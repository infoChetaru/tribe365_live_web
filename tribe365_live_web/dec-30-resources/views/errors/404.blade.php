@section('title', 'Page Not Found')
@include('layouts.adminHeader')
  <main class="main-content">
  <div class="add-fild-section ">
    <div class="container">
        <div class="error-section">
          <div class="error-content">
            <h1>4<span>0</span>4</h1>
              <p> Sorry - File not Found!</p>
              <div class="sub">
              <p><a href="javascript:history.back()" class="btn"> Back to Home</a></p>
          </div>
				</div>
		  </div>
        </div>
        </div>
      </main>
<script type="text/javascript">
$(document).ready(function() {
  function setHeight() {
  var windowHeight = $(window).height();
  var header = $("header").outerHeight(true);
  var footer = $("footer").outerHeight(true);
  var result = windowHeight - header - footer;
  $(".content-section").css("min-height", result);
  }
    setHeight();
  
  $(window).resize(function() {
     setHeight();
  });
    
});
</script>

@include('layouts.adminFooter')