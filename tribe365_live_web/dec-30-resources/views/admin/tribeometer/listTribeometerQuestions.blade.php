@section('title', 'List Tribeometer Questions')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                
								{{session('message')}}
							</div>
							@endif
						</div>
						<h2> Tribeometer Questions </h2>	

						<div class="value-list menual_fun_lens_wrap">
							<table>
								<tr><th> Question </th><th>Category</th><th>Action</th></tr>								
								@foreach($questionTbl as $value)	

								{{ Form::model($value, ['method' => 'PUT', 'route' => array('tribeometer.update', base64_encode($value->id)),'id'=>'form']) }}
								<tr class="table-class"><td><textarea name="question">{{$value->question}}</textarea></td>	
									<td>{{$value->title}}</td>
									<td>
										<div class="editable">
											<button class="editbtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
											<button style="display: none;" class="submitbtn" type="submit">submit</button>
										</div>
									</td>			
								</tr>
								{{Form::close()}}
								@endforeach							
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="organ-page-nav">
			{{$questionTbl->links('layouts.pagination')}}
		</div>
	</div>
</main>
<script type="text/javascript">
	$(document).ready(function() {
		$('.submitbtn').hide();
		$(".table-class").find("textarea").prop("disabled", true);		
	});
</script>
<script type="text/javascript">
	$('.editbtn').on('click',function(){
		$(this).closest('.editable').find('.submitbtn').show();
		$(this).closest('.editable').find('.editbtn').hide();	
		$(this).closest('.table-class').find('textarea').prop('disabled',false);		
	})
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$("textarea").each(function(){
			var scroll_height = $(this).get(0).scrollHeight;
			$(this).css('height', scroll_height + 'px');
		});		
	});
</script>
@include('layouts.adminFooter')