<!-- header -->
@section('title', 'Edit Theme')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">

			<div class="dot-section">
				<div class="message-cls" align="center">                            
					@if(session('message'))
					<div class="alert alert-success" role="alert">
						{{session('message')}}
					</div>
					@endif
					@if(session('error'))
					<div class="alert alert-danger" role="alert">
						{{session('error')}}
					</div>
					@endif
				</div> 
				<h2> Edit Theme </h2>
				
				<div class="form-section">
					<form id="add-theme-form" action="{{route('update-theme')}}" method="post">
						{{csrf_field()}}	
						<input type="hidden" name="themeId" value="{{base64_encode($themeTbl->id)}}">					
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">									
									<input class="charCount date" id="date-opened"  type="text" name="date_opened" placeholder="Date Opened" autocomplete="off" value="{{date('d-m-Y',strtotime($themeTbl->dateOpened))}}">
								</div>									
							</div>

							<div class="col-md-6">
								<div class="form-group">									
									<select id="organisation" name="organisation">
										<option value="" disabled="" selected=""> All organisation </option>
										@foreach($organisations as $oValue)
										<option {{($oValue->id==$themeTbl->orgId)?'selected':''}} value="{{$oValue->id}}">{{ucfirst($oValue->organisation)}}</option>
										@endforeach
									</select>
								</div>									
							</div>														
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input class="charCount" id="title" type="text" name="title" placeholder="Title" value="{{ucfirst($themeTbl->title)}}">
								</div>									
							</div>		

							<div class="col-md-6">
								<div class="form-group">									
									<div class="select-mnth">						
										<select id="theme" class="linked-action" name="linked_action[]" multiple="multiple">					
											@foreach($actionList as $action)
											<option {{(in_array($action->id, $selectedActionArr))?'selected':''}} value="{{$action->id}}">{{ucfirst($action->id)}}</option>
											@endforeach()
										</select>							
									</div>
								</div>									
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">									
									<input class="charCount" id="description"  type="text" name="description" placeholder="Description" value="{{ucfirst($themeTbl->description)}}">
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group">									
									<select id="status" name="status">
										<option value="" disabled="" selected=""> Select status </option>
										<option {{($themeTbl->themeStatus=='Open')?'selected':''}} value="open"> Open </option>
										<option {{($themeTbl->themeStatus=='Closed')?'selected':''}} value="Closed"> Closed </option>
									</select>
								</div>									
							</div>														
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">	
									<select id="initial-likelihood" name="initial_likelihood">
										<option value="" disabled="" selected=""> Select initial likelihood </option>
										<option {{($themeTbl->initialLikelihood=='0')?'selected':''}} value="0"> 0 </option>
										<option {{($themeTbl->initialLikelihood=='1')?'selected':''}} value="1"> 1 </option>
										<option {{($themeTbl->initialLikelihood=='2')?'selected':''}} value="2"> 2 </option>
										<option {{($themeTbl->initialLikelihood=='3')?'selected':''}} value="3"> 3 </option>
										<option {{($themeTbl->initialLikelihood=='4')?'selected':''}} value="4"> 4 </option>
										<option {{($themeTbl->initialLikelihood=='5')?'selected':''}} value="5"> 5 </option>
									</select>
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<select id="initial-consequence" name="initial_consequence">
										<option value="" disabled="" selected=""> Select initial consequence </option>
										<option {{($themeTbl->initialConsequence=='0')?'selected':''}} value="0"> 0 </option>
										<option {{($themeTbl->initialConsequence=='1')?'selected':''}} value="1"> 1 </option>
										<option {{($themeTbl->initialConsequence=='2')?'selected':''}} value="2"> 2 </option>
										<option {{($themeTbl->initialConsequence=='3')?'selected':''}} value="3"> 3 </option>
										<option {{($themeTbl->initialConsequence=='4')?'selected':''}} value="4"> 4 </option>
										<option {{($themeTbl->initialConsequence=='5')?'selected':''}} value="5"> 5 </option>
									</select>
								</div>									
							</div>														
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">	
									<select id="current-likelihood" name="current_likelihood">
										<option value="" disabled="" selected=""> Select current likelihood </option>
										<option {{($themeTbl->currentLikelihood=='0')?'selected':''}} value="0"> 0 </option>
										<option {{($themeTbl->currentLikelihood=='1')?'selected':''}} value="1"> 1 </option>
										<option {{($themeTbl->currentLikelihood=='2')?'selected':''}} value="2"> 2 </option>
										<option {{($themeTbl->currentLikelihood=='3')?'selected':''}} value="3"> 3 </option>
										<option {{($themeTbl->currentLikelihood=='4')?'selected':''}} value="4"> 4 </option>
										<option {{($themeTbl->currentLikelihood=='5')?'selected':''}} value="5"> 5 </option>
									</select>
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group">									
									<select id="current-consequence" name="current_consequence">
										<option value="" disabled="" selected=""> Select current likelihood </option>
										<option {{($themeTbl->currentConsequence=='0')?'selected':''}} value="0"> 0 </option>
										<option {{($themeTbl->currentConsequence=='1')?'selected':''}} value="1"> 1 </option>
										<option {{($themeTbl->currentConsequence=='2')?'selected':''}} value="2"> 2 </option>
										<option {{($themeTbl->currentConsequence=='3')?'selected':''}} value="3"> 3 </option>
										<option {{($themeTbl->currentConsequence=='4')?'selected':''}} value="4"> 4 </option>
										<option {{($themeTbl->currentConsequence=='5')?'selected':''}} value="5"> 5 </option>
									</select>
								</div>									
							</div>														
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">									
									<select id="type" name="type">
										@foreach($types as $tValue)
										<option {{($tValue->id==$themeTbl->type)?'selected':''}} value="{{$tValue->id}}">{{ucfirst($tValue->title)}}</option>
										@endforeach
									</select>
								</div>									
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="Create-btn-cont">
									<button type="button" onclick="validation()" class="btn save"> Update </button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="error-message" style="display: none;">
					<span id="resp"></span>
				</div> 
			</div>
		</div>
	</main>

	@include('layouts.adminFooter')

	<script type="text/javascript">
		$(function () {
			$('#theme').multiselect({
				includeSelectAllOption: true,
				enableFiltering:false,
				nonSelectedText: 'Select action',
			});			
		});
	</script>
	<script type="text/javascript">
		$('#organisation').on('change', function() {

			$('#theme').html(" ");
			
			$('#theme').multiselect('refresh');
			$('.multiselect-selected-text').html('Select action');

			var orgId = $('#organisation').val();
			
			$.ajax({
				type: "POST",
				url: "{{route('get-actions-by-ajax')}}",
				data: {orgId:orgId, "_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					var obj = JSON.parse(response);
					var htmlContainer = obj.htmlContainer;
					var htmlOption    = obj.htmlOption

					$('#new-theme-input').hide();
					$('#theme').html('');
					$('.multiselect-container').html('');
					$('#theme').append(htmlOption);
					$('.multiselect-container').append(htmlContainer);			
				}
			});
		});
	</script>
	<script type="text/javascript">
		function validation()
		{

			$('.error-message').show();

			var date_opened    	= $('#date-opened').val();
			var title           = $('#title').val();	
			var description		= $('#description').val();
			var type 		    = $('#type').val();
			var organisation 	= $('#organisation').val();
			var status 		    = $('#status').val();
			var initial_likelihood  = $('#initial-likelihood').val();
			var initial_consequence = $('#initial-consequence').val();
			var current_likelihood  = $('#current-likelihood').val();
			var current_consequence = $('#current-consequence').val();
			var linked_action 		= $('.linked-action').val();
			
			if($.trim(date_opened) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please select date.');
			}
			else if($.trim(title) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please enter title.');
			}
			else if($.trim(description) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please enter description.');
			}
			else if($.trim(type) == '' )
			{
				$('#resp').html('');
				$('#resp').html('Please select type.');
			}
			else if($.trim(organisation) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please select organisation.');
			}
			else if($.trim(status) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please select status.');
			}
			else if($.trim(initial_likelihood) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please enter initial likelihood.');
			}
			else if($.trim(initial_consequence) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please enter initial consequence.');
			}
			else if($.trim(current_likelihood) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please enter current likelihood.');
			}
			else if($.trim(current_consequence) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please enter current consequence.');
			}
			else if($.trim(linked_action) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please enter linked action.');
			}
			else
			{
				$('.save').prop("disabled", true);
				$('#add-theme-form').submit();
			}
		}
	</script>

	<script type="text/javascript">
		$(document).on('focus', '.date',function(){
			$(this).datepicker({
				todayHighlight:true,
				dateFormat: 'dd-mm-yy',
				minDate:'0',
				autoclose:true
			});
		});
	</script>