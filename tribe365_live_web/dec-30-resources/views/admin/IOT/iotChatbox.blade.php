<!-- header -->
@section('title', 'IOT-Chat')
@include('layouts.adminHeader')
<div class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="chat-link-section chat-section">
				<table>
					<tr>
						<td>
							<table>
								<tr>
									<th> ID </th>
									<th> Date Of <br/> Submission </th>
									<th> Organisation </th>
									<th> Office </th>
									<th> Department </th>
									<th> Initial Text </th>
									<th> Reporter Email </th>
									<th> Theme/Trends/Risks </th>
								</tr>
								@foreach($feedbackList as $feedback)		

								<tr>
									<td> {{$feedback->id}} </td>
									<td> {{date('d-m-Y',strtotime($feedback->created_at))}} </td>
									<td> {{$feedback->organisation}} </td>
									<td> {{$feedback->office}} </td>
									<td> {{$feedback->department}} </td>
									<td> {{$feedback->message}} </td>
									<td> {{$feedback->email}} </td>
									<td> {{implode(', ', $feedback->themes)}} </td>
								</tr>

								@endforeach
							</table>
						</td>
					</tr>
					@if($feedback->status !='Completed')
					<tr>
						<td>
							<table class="chat-button">
								<tr>
									<td>
				<a href="{{url::to('admin/complete-feedback/'.base64_encode($feedback->id))}}" onclick="return confirm('Are you sure?')">
											<button> Complete </button>
										</a>
									</td>
								</tr>
							</table> 
						</td>
					</tr>	
					@endif
				</table>
			</div>
			<div class="loader-img" style="display: none;width: 70px; top: 23px;"><img src="{{asset('public/images/loader.gif')}}"></div>
			<div class="chat-box">
				<div class="content demo-y mCustomScrollbar ">
					<div class="messages">
						<ul id="ajax-msg">

							@foreach($messages as $msg)
							@if($msg->sendFrom !=1)

							<li class="sent">
								<div class="user-img"> <img src="{{asset('public/uploads/user_profile/'.$msg->imageUrl)}}"/></div>
								<p>
									<span>{{$msg->message}}</span>
									<time>{{date('H:i a',strtotime($msg->created_at))}}</time>
								</p>
								
							</li>

							@if($msg->file)

							<li class="sent">								
								<a href="{{url('public/uploads/iot_files/'.$msg->file)}}" target="blank">	
									<img src="{{asset('public/uploads/iot_files/'.$msg->file)}}"/>
								</a>
							</li>

							@endif

							@elseif($msg->sendFrom == 1)

							<li class="replies">
								<div class="user-img"><img src="{{asset('public/uploads/user_profile/'.$msg->imageUrl)}}"/></div>
								<p>
									<span>{{$msg->message}}</span>
									<time>{{date('H:i a',strtotime($msg->created_at))}}</time>
								</p>
							</li>

							@if($msg->file)

							<li class="replies">								
								<a href="{{url('public/uploads/iot_files/'.$msg->file)}}" target="blank">	
									<img class="comn-img" src="{{asset('public/uploads/iot_files/'.$msg->file)}}"/>
								</a>
							</li>

							@endif

							@endif
							@endforeach			

						</ul>
					</div>
				</div>
			</div>
			<div class="type-chat">
				<div class="chat-form">

					<form id="msg-form">
						
						{{csrf_field()}}
						<input id="message" type="text" name="message" placeholder="Type Something...">
						<div class="file-upload">
							<i>
								<img style="width: 25px" src="{{asset('public/images/paperclip.png')}}">
								<input id="file" type="file" name="file">
							</i>
						</div>
						<div class="upld-chat-img">
							<img style="display: none;" width="50" height="50" id="blah" src="img" class="mCS_img_loaded hide-img">
						</div>

						<?php $user = Auth::user(); ?>
						<input type="hidden" name="sendFrom" value="{{$user->id}}">
						<input type="hidden" name="sendTo" value="{{$feedback->userId}}">
						<input type="hidden" name="feedbackId" value="{{$feedback->id}}">
						<button id="send-btn" onclick="validation();" type="button">Send</button>

					</form>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- footer -->
@include('layouts.adminFooter')
<script type="text/javascript">
	function validation()
	{		
		$.ajax({
			type: "POST",
			url: "{{route('send-chat-msg')}}",
			data:new FormData($("#msg-form")[0]),
			contentType: false,
			cache: false,
			processData: false,
			success: function(response)
			{
				$('#ajax-msg').html('')
				$('#message').val('')
				$('#ajax-msg').html(response)

				$('#file').val('');
				$('#blah').attr('src', '');

				console.log('gek '+response);
			}
		});		
	}
</script>
<!-- call function after a time and set in msg list -->
<script type="text/javascript">

	setInterval(function () { 

		var feedbackId = "{{$feedback->id}}";

		$.ajax({
			type: "POST",
			url: "{{route('get-chat-messages-by-ajax')}}",
			data: {feedbackId:feedbackId, "_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				$('#ajax-msg').html('')
				$('#ajax-msg').html(response)

				console.log('ajax')				
			}
		});		

	},2000);
</script>

<script type="text/javascript">
	// when enter key pressed
	$(function(){

		// $('input[name="message"]').click(function(){
		// 	$('#send-btn').click();
		// });

		$('#message').keypress(function (e) {
			var key = e.which;
			if(key == 13)
			{	
				$('#send-btn').click();			
				return false;  
			}
		});

	});
</script>

<script type="text/javascript">
	function readURL(input) 
	{
		$('#blah').show();
		
		if (input.files && input.files[0]) 
		{
			var reader = new FileReader();
			
			reader.onload = function (e) 
			{
				$('#blah').attr('src', e.target.result);
			}
			
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	$("#file").change(function()
	{
		readURL(this);
	});
</script>


