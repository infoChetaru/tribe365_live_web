<!-- header -->
@section('title', 'IOT')
@include('layouts.adminHeader')
<div class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="performance">
				<div class="loader-img" style="display: none;width: 70px; top: 10px;"><img src="{{asset('public/images/loader.gif')}}"></div>
				
				<div class="improvement">
					<form id="form-id" action="#" method="POST">					
						<div class="performance-option">
							<ul>
								<li> 
									<select name="orgId"> 
										<option value="">All Organisation</option>									
										<option value="">Chetaru India</option>
										<option value="">Chetaru UK</option>									
									</select>
								</li>
								<li> 
									<select id="reportType" name="reportType"> 				
										<option value="">UK</option>
									</select>
								</li>														
								<li> 
									<button onclick="validation();" type="button" class="search-btn">Search</button>
								</li>							
							</ul>
						</div>					
					</form>
					<div class="improvement-list">
						<div class="row">
							<h2> Submissions</h2>
							<div class="col-md-2">
								<div class="improvement-box">
									<h3> New </h3>
									<strong> 4 </strong>
								</div>
							</div>
							<div class="col-md-2">
								<div class="improvement-box">
									<h3> On Hold </h3>
									<strong> 4 </strong>
								</div>
							</div>
							<div class="col-md-2">
								<div class="improvement-box">
									<h3> New </h3>
									<strong> 4 </strong>
								</div>
							</div>

						</div>
						<div class="row">
							<h2> Live Conversations</h2>
							<div class="col-md-2">
								<div class="improvement-box">
									<h3> Awaiting Responses </h3>
									<strong> 4 </strong>
								</div>
							</div>
						</div>
						<div class="row">
							<h2> Themes</h2>
							<div class="col-md-2">
								<div class="improvement-box">
									<h3> Strength </h3>
									<strong> 4 </strong>
								</div>
							</div>
							<div class="col-md-2">
								<div class="improvement-box">
									<h3> Weakness </h3>
									<strong> 4 </strong>
								</div>
							</div>
							<div class="col-md-2">
								<div class="improvement-box">
									<h3> Opportunities </h3>
									<strong> 4 </strong>
								</div>
							</div>
							<div class="col-md-2">
								<div class="improvement-box">
									<h3> Theats </h3>
									<strong> 4 </strong>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
<!-- footer -->
@include('layouts.adminFooter')