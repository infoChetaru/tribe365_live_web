<!-- header -->
@section('title', 'IOT')
@include('layouts.adminHeader')
<div class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="performance">
				<div class="loader-img" style="display: none;width: 70px; top: 10px;"><img src="{{asset('public/images/loader.gif')}}"></div>				
				<div class="improvement">
					<form id="form-id" action="{{url::to('admin/iot-dashboard',base64_encode($orgId))}}" method="POST">
						{{csrf_field()}}					
						<div class="performance-option">
							<ul>
								@if(!empty($fromOrg===true))
								<li> 
									<select id="organisation" name="orgId"> 
										@foreach($organisations as $oValues)
											<option {{($oValues->id==$orgId)?'selected':''}} value="{{base64_encode($oValues->id)}}">{{$oValues->organisation}}</option>
										@endforeach									
									</select>

									<select id="office" name="officeId">
										<option value="" selected=""> All </option>
										@foreach($offices as $office)
										<option {{($office->id==$officeId)?'selected':''}} value="{{$office->id}}"> {{$office->office}} </option>
										@endforeach
									</select>										
								</li>

								@else

								<li> 
									<select id="organisation" name="orgId"> 
										<option value="">All Organisation</option>							@foreach($organisations as $oValues)
										<option {{($oValues->id==$orgId)?'selected':''}} value="{{base64_encode($oValues->id)}}">{{$oValues->organisation}}</option>
										@endforeach									
									</select>
								</li>

								@endif
								
								<li> 
									@if($officeId)									
									<select id="office" name="officeId">
										@foreach($offices as $office)
										<option {{($office->id==$officeId)?'selected':''}} value="{{$office->id}}"> {{$office->office}} </option>
										@endforeach
									</select>
									@else
									<select style="display: none;" id="office" name="officeId"></select>
									@endif									
								</li>	

								<li> 
									<button onclick="validation();" type="button" class="search-btn">Search</button>
								</li>							
							</ul>
						</div>							
					</form>

					<div class="improvement-list">

						<div class="row">

							<h2> Submissions</h2>							
							<div class="col-md-2">
								<a href="{{url::to('admin/feedback-list'.'/'.base64_encode($orgId).'/'.base64_encode('new').'/'.base64_encode($officeId))}}">
									<div class="improvement-box">
										<h3> New </h3>
										<strong> {{count($iotNewFeedback)}} </strong>
									</div>
								</a>
							</div>
							
							<div class="col-md-2">
								<a href="{{url::to('admin/feedback-list'.'/'.base64_encode($orgId).'/'.base64_encode('on_hold').'/'.base64_encode($officeId))}}">
									<div class="improvement-box">
										<h3> On Hold </h3>
										<strong> {{$iotOnHoldFeedbackArray}} </strong>
									</div>
								</a>
							</div>

							<div class="col-md-2">
								<a href="{{url::to('admin/feedback-list'.'/'.base64_encode($orgId).'/'.base64_encode('completed').'/'.base64_encode($officeId))}}">
									<div class="improvement-box">
										<h3> Complete </h3>
										<strong> {{count($iotCompletedFeedback)}} </strong>
									</div>
								</a>
							</div>

						</div>

						<div class="row">
							<h2> Live Conversations</h2>
							<div class="col-md-2">
								<a href="{{url::to('admin/feedback-list'.'/'.base64_encode($orgId).'/'.base64_encode('awaiting').'/'.base64_encode($officeId))}}">
									<div class="improvement-box">
										<h3> Awaiting Responses </h3>
										<strong> {{$avaitingResponseArray}} </strong>			
										</div>
								</a>
							</div>
						</div>

						<div class="row">
							<h2> Themes </h2>

							@foreach($themeList as $tvalue)
							<div class="col-md-2">
								<a href="{{url::to('admin/theme-list/'.base64_encode($orgId).'/'.base64_encode($tvalue['id']))}}">
									<div class="improvement-box">
										<h3> {{ucfirst($tvalue['title'])}} </h3>
										<strong> {{$tvalue['themeCount']}} </strong>
									</div>
								</a>
							</div>							
							@endforeach

						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
<!-- footer -->
@include('layouts.adminFooter')

<script type="text/javascript">
	$('#organisation').on('change', function() {
		$('#office option').remove();

		var orgId1 = $('#organisation').val();

		var orgId = atob(orgId1);

		if(orgId) 
		{
			$('#office').show();
		}
		else
		{
			$('#office').hide();
		}

		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getOfficesByOrgId')!!}",				
			data: {orgId:orgId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{					
				$('#office').append(response);
			}
		});
	});
</script>
<script type="text/javascript">
	function validation()
	{
		$('.loader-img').show();
		var orgId = $('#organisation').val();

		$('#form-id').submit();
	}
</script>