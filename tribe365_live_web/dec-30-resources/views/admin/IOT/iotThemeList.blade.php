<!-- header -->
@section('title', 'IOT')
@include('layouts.adminHeader')
<div class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="chat-link-section">
				<div class="container">
					<div class="row">

						@foreach($themeListTbl as $value)
						<div class="col-md-4">
							<div class="chat-link"> 
								<div class="dt-tim-section">
									<div class="dt">
										<strong> {{ucfirst($value->title)}}</strong>
										<span>  </span>
									</div>									
								</div>
								<ul>
									<li>
										<strong> Date Opened</strong>
										<span> {{date('d-m-Y',strtotime($value->dateOpened))}}</span>										
									</li>
									<li>
										<strong> Description </strong>
										<div class="toltip-section">
											<span class="hide-text">{{ucfirst($value->description)}}</span>
											<div class="toltip-box">{{ucfirst($value->description)}}</div>
										</div>
									</li>
									<li>
										<strong> Type </strong>
										<span>{{ucfirst($value->categoryTitle)}}</span>
									</li>
									<li>
										<strong> Organisation</strong>
										<span>{{ucfirst($value->organisation)}}</span>
									</li>
									<li>
										<strong> Status</strong>
										<span>{{ucfirst($value->themeStatus)}}</span>
									</li>
									<li>
										<strong> Submissions</strong>
										<span>{{implode(', ',$value->submission)}}</span>
									</li>
									<li>
										<strong> Initial likelihood</strong>
										<span>{{($value->initialLikelihood)}}</span>
									</li>
									<li>
										<strong> Initial consequence</strong>
										<span>{{($value->initialConsequence)}}</span>
									</li>
									<li>
										<strong> Current likelihood</strong>
										<span>{{($value->currentLikelihood)}}</span>
									</li>
									<li>
										<strong> Current consequence</strong>
										<span>{{($value->currentConsequence)}}</span>
									</li>
									<li>
										<strong> Linked action</strong>
										<div class="toltip-section">
											<span class="hide-text">{{ucfirst($value->actions)}}</span>
											<div class="toltip-box">{{ucfirst($value->actions)}}</div>
										</div>
									</li>									
								</ul>
								<div class="cht-lnk-btn">
									<a href="{{url::to('admin/edit-theme'.'/'.($value->orgId).'/'.base64_encode($value->id))}}" data-toggle="tooltip" title="Edit theme"><button class="btn chat-btn">Edit</button></a>
									<button class="btn chat-btn" onclick="deleteThemes('{{base64_encode($value->id)}}','{{base64_encode($value->orgId)}}')"> Delete </button>
									<!-- <button class="btn lnk-btn"> Link </button> -->
								</div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
			<div class="add-plus-icon">
				<div class="add-plus-upper">
					<a href="{{route('add-theme')}}"><img src="{{asset('public/images/plus-icon.png')}}">
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="organ-page-nav">
	{!! $themeListTblpagi->links('layouts.pagination') !!}                        
</div>

<script type="text/javascript">
	function deleteThemes(id,orgId) {

		if(confirm('Are you sure you want to delete?') == true){
			$.ajax({
				url: "{{URL::to('admin/delete-themes')}}",
				type: "POST",
				dataType: "JSON",
				data: {orgId:orgId,themeId:id,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					//swal("Deleted!", "Your imaginary file has been deleted.", "success");
					if (response.status == 200) {
						//window.location.href = '';
						location.reload();
					}
				}
			});
		}	
	}
</script>

<!-- footer -->
@include('layouts.adminFooter')