<!-- header -->
@section('title', 'IOT')
@include('layouts.adminHeader')
<div class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="chat-link-section">
				<table>
					<tr>
						<td>
							<table>
								<tr>
									<th>
										ID
									</th>
									<th>
										Date Of <br/> Submission 
									</th>
									<th>
										Organisation
									</th>
									<th>
										Office
									</th>
									<th>
										Department
									</th>
									<th>
										Initial Text
									</th>
									<th>
										Reporter Email
									</th>
									<th>
										Theme/Trends/Risks
									</th>
								</tr>
							</table>
							<table>
								<tr>
									<td>
										<table>
											<tr>
												<td> 12345 </td>
												<td> 15/7/2019 </td>
												<td> Chetaru </td>
												<td> UK</td>
												<td> Business Dev.  </td>
												<td> Rubbish Day </td>
												<td> <a href=""> oliver@chetaru.com </a> </td>
												<td> Poor Service <br/> Poor Communication  </td>
											</tr>
										</table>
										<table class="chat-button">
											<tr>
												<td>
													<button> Chat </button>
													<button> Link </button>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table>
											<tr>
												<td> 12345 </td>
												<td> 15/7/2019 </td>
												<td> Chetaru </td>
												<td> UK</td>
												<td> Business Dev.  </td>
												<td> Rubbish Day </td>
												<td> <a href=""> oliver@chetaru.com </a> </td>
												<td> Poor Service <br/> Poor Communication  </td>
											</tr>
										</table>
										<table class="chat-button">
											<tr>
												<td>
													<button> Chat </button>
													<button> Link </button>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table>
											<tr>
												<td> 12345 </td>
												<td> 15/7/2019 </td>
												<td> Chetaru </td>
												<td> UK</td>
												<td> Business Dev.  </td>
												<td> Rubbish Day </td>
												<td> <a href=""> oliver@chetaru.com </a> </td>
												<td> Poor Service <br/> Poor Communication  </td>
											</tr>
										</table>
										<table class="chat-button">
											<tr>
												<td>
													<button> Chat </button>
													<button> Link </button>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>


<!-- footer -->
@include('layouts.adminFooter')