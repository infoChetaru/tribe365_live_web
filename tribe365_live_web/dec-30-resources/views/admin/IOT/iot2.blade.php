<!-- header -->
@section('title', 'IOT')
@include('layouts.adminHeader')
<div class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="chat-link-section chat-section">
				<table>
					<tr>
						<td>
							<table>
								<tr>
									<th>
										ID
									</th>
									<th>
										Date Of <br/> Submission 
									</th>
									<th>
										Organisation
									</th>
									<th>
										Office
									</th>
									<th>
										Department
									</th>
									<th>
										Initial Text
									</th>
									<th>
										Reporter Email
									</th>
									<th>
										Theme/Trends/Risks
									</th>
								</tr>
							</table>
							<table>
								<tr>
									<td>
										<table>
											<tr>
												<td> 12345 </td>
												<td> 15/7/2019 </td>
												<td> Chetaru </td>
												<td> UK</td>
												<td> Business Dev.  </td>
												<td> Rubbish Day </td>
												<td> <a href=""> oliver@chetaru.com </a> </td>
												<td> Poor Service <br/> Poor Communication  </td>
											</tr>
										</table>
										
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<div class="chat-box">
			<div class="content demo-y mCustomScrollbar ">
				<div class="messages">
					<ul>
						<li class="sent">
							<img src="http://emilcarlsson.se/assets/mikeross.png" alt="" />
							<p>How the hell am I supposed to get a jury to believe you when I am not even sure that I do?!</p>
						</li>
						<li class="replies">
							<img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
							<p>When you're backed against the wall, break the god damn thing down.</p>
						</li>
						<li class="replies">
							<img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
							<p>Excuses don't win championships.</p>
						</li>
						<li class="sent">
							<img src="http://emilcarlsson.se/assets/mikeross.png" alt="" />
							<p>Oh yeah, did Michael Jordan tell you that?</p>
						</li>
						<li class="replies">
							<img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
							<p>No, I told him that.</p>
						</li>
						<li class="replies">
							<img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
							<p>What are your choices when someone puts a gun to your head?</p>
						</li>
						<li class="sent">
							<img src="http://emilcarlsson.se/assets/mikeross.png" alt="" />
							<p>What are you talking about? You do what they say or they shoot you.</p>
						</li>
						<li class="replies">
							<img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
							<p>Wrong. You take the gun, or you pull out a bigger one. Or, you call their bluff. Or, you do any one of a hundred and forty six other things.</p>
						</li>
					</ul>
				</div>
				</div>
			</div>
			<div class="type-chat">
				<div class="chat-form">
					<form>
						<input type="text" name="" placeholder="Type Something...">
						<div class="file-upload">
							<i class="fa fa-paperclip" aria-hidden="true"></i>
						</div>
						<input type="submit" name="" value="Send">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- footer -->
@include('layouts.adminFooter')