<!-- header -->
@section('title', 'Dashboard')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="title-stats-section">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<a href="{{ url('/admin/admin-organisation') }}">
						<div class="title-stats-cont">
							<img src="{{ asset('public/images/org-bg-icon.png') }}">
							<h3> Organisations </h3>
							<span class="org-number">@if(!empty($data['organisation']))
								{{$data['organisation']}}
								@endif </span>
							</div>
						</a>
					</div>
					<div class="col-md-3">
						<a href="{{route('departments.index')}}">
							<div class="title-stats-cont user-box">
								<img src="{{ asset('public/images/user-bg-icon.png') }}">
								<h3> Departments </h3>
								<span class="org-number">
									@if(!empty($data['departmentCount']))
									{{ $data['departmentCount']}}
									@endif
								</span>
							</div>
						</a>					
					</div>

					
					<div class="col-md-3">

						<?php if(Session::get('improvementPassword')=='done'){ ?>

						<a href="{{url::to('admin/iot-dashboard',base64_encode(0))}}">
							<div class="title-stats-cont per-box">
								<img src="{{asset('public/images/improvement.png')}}">
								<h3> Improvement </h3>
								<span class="org-number">
									@if(!empty($data['iotFeedbackCount']))
									{{ $data['iotFeedbackCount']}} New
									@endif
								</span>
							</div>
						</a>	

						<?php }else{ ?>	
							<a href="javascript:void(0);" onclick="improvementOpnPoup('<?php echo url('admin/iot-dashboard/'.base64_encode(0)); ?>');"> 
							<div class="title-stats-cont per-box">
								<img src="{{asset('public/images/improvement.png')}}">
								<h3> Improvement </h3>
								<span class="org-number">
									@if(!empty($data['iotFeedbackCount']))
									{{ $data['iotFeedbackCount']}} New
									@endif
								</span>
							</div>
						</a>	
						<?php } ?>

					</div>


					<div class="col-md-3">
						<a href="{{route('performace-detail')}}">
							<div class="title-stats-cont user-box">
								<img src="{{ asset('public/images/line-chart.png') }}">
								<h3> Performance </h3>
								<span class="org-number">

								</span>
							</div>
						</a>					
					</div>


					
				</div>
			</div>
		</div>
	</main>
	<!-- footer -->
	@include('layouts.adminFooter')
