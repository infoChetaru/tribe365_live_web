<!-- header -->
@section('title', 'List User')
@include('layouts.adminHeader')

<div class="add-fild-section">
	<div class="container">
		<div class="ragistration-section">
			<div class="row">
				<div class="col-md-12">
					<div class="loader-img" style="display: none;width: 70px; top: -57px;"><img src="{{asset('public/images/loader.gif')}}"></div>
					<div class="search-section">
						<form>
							<input id="search-key" type="search" placeholder="Search...">
						</form>                        
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                                    
								{{session('message')}}
							</div>
							@endif
						</div>                        
					</div>
					<h2> Staff </h2>					
					<div class="value-list">
						<table class="search-sec">
							<tr>
								<th> Name         </th>
								<th> Email        </th>								
								<th> Office       </th>
								<th> Department   </th>
								<th> Action </th>
							</tr>							
							@foreach($users as $value)
							<tr>
								<td> {{ucfirst($value->name)}}         </td>
								<td> {{$value->email}}                 </td>
								
								<td> {{ucfirst($value->office)}}       </td>	
								<td> {{ucfirst($value->department)}}   </td>	

								<td>
									<div class="editable">
										<a href="{!!route('admin-user.edit',['id'=>base64_encode($value->id)])!!}">
											<button type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
										</a>

										{{ Form::open(['method'  => 'delete', 'route' => [ 'admin-user.destroy', base64_encode($value->id) ] ]) }}

										{{ Form::button('', ['type'=>'submit','onclick'=>'return confirm("Are you sure you want to delete ?")']) }}

										{{ Form::close() }}
									</div>

								</td>									
							</tr>
							@endforeach							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="organ-page-nav">
		{!! $users->links('layouts.pagination') !!}
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#search-key").keyup(function(){
			$('.search-sec').html('');			
			$('.organ-page-nav').html('');
			$('.loader-img').show();

			var staff = $('#search-key').val();
			var orgId  = '<?php echo base64_decode(Request::segment(3)); ?>';
			if(staff==''){
				location.reload();
			}
			$.ajax({
				type: "POST",
				url: "{!!URL::to('admin/searchStaff')!!}",             
				data: {staff:staff,orgId:orgId,"_token":'<?php echo csrf_token()?>'},
				success: function(response) {
					
					if(response.length >= 1){

						$('.search-sec').html('');				
						$('.search-sec').append(response);             
						$('.loader-img').hide();
					}else{
						
						$('.search-sec').html('');
						$('.value-list').html('');				
						$('.value-list').append('<div align="center" class="text-danger" role="alert">No result found.</div>');
						$('.loader-img').hide();
					}
				}
			}); 		
		});
	});
</script>
@include('layouts.adminFooter')