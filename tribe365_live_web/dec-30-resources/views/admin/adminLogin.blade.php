<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" >
<head>
	<title>Tribe 365 </title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/responsive.css') }}">
	<script type="text/javascript" src="{{ asset('public/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/jquery.min.js') }}"></script>
	<style type="text/css">

	section.loging-section {
		background: url({{ asset('public/images/bg-img.jpg') }})no-repeat center center; 
}
.login-img {
	background:url({{ asset('public/images/round-img.png') }})no-repeat right center,url({{ asset('public/images/round-bg-img.jpg') }})no-repeat center;
}
.sidebar-menu ul li a {	
	background: url({{ asset('public/images/nav-arrow.png') }}) no-repeat 88% center;
}
.profile-user-list span {
	background: url({{ asset('public/images/user-drop-icon.png') }}) no-repeat right 6px;
}

</style>
</head>
<body>
	<section class="loging-section">
		<!-- <div class="number-top">
			<a href="#">+44(0)191 Enquiries@tribe365</a>
		</div> -->
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="login-content">
						<div class="login-img">
						</div>
						<div class="loing-form">
						<div class="loing-box">
							<div class="logo-img">
								<img src="{{ asset('public/images/login-logo.png') }}">
							</div>
							
							<div class="form">

								<form method="POST" action="{{ route('admin.auth') }}">
									{{ csrf_field() }}
									<div class="form-group">					
										<input class="form-control" placeholder="E-Mail Address" type="email" name="email" value="{{ old('email') }}" required>
										<span class="login-icon-img"> <img src="http://production.chetaru.co.uk/tribe365/public/images/use-icon.png"> </span>
									</div>
									<div class="form-group">
										<input class="form-control" placeholder="Password" type="password" name="password" required><span class="login-icon-img"> <img src="http://production.chetaru.co.uk/tribe365/public/images/key-icon.png"> </span>
									</div>
									<button type="submit" class="btn btn-primary">Login</button>
									<a href="{{URL::to('forgot-password')}}"><p  class="forget-pass" style="color:#ef4950;">Forgot Password?</p></a>

									@if ($errors->has('email'))
									<span class="alert-danger">
										{{ $errors->first('email') }}
									</span>
									@endif

									@if ($errors->has('password'))
									<span class="alert-danger">
										{{ $errors->first('password') }}
									</span>
									@endif

								</form>

							</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>