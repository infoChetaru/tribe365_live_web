<!-- header -->
@section('title', 'Add User')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="dot-section">
				<h2> Add Office </h2>
				<div class="form-section">
					<form id="add-office-form" action="{{ URL::to('admin/create-office/') }}" method="post">
						{{csrf_field()}}

						<input type="hidden" name="orgId" value="{{$orgId}}">
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">									
									<input class="form-control charCount" id="name"  type="text" name="name" placeholder="Office Name"  maxlength="50">
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input class="form-control charCount numberControl" id="nemp" type="text" name="nemp" placeholder="Number of Employee" autocomplete="off" maxlength="50">
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group"><input name="office_address" id="office_address" type="text" class="form-control office_validation checkOrgCount"  placeholder="Address"  maxlength="150">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group checkOrgCount"><input name="office_city" id="office_city" type="text" class="form-control office_validation checkOrgCount"  placeholder="City"  maxlength="100"></div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<!-- <input name="office_country" id="office_country"  type="text" class="form-control office_validation checkOrgCount"  placeholder="Country"  maxlength="150"> -->
									<select name="office_country" id="office_country" style="background:url(http://chetaru.gottadesigner.com/tribe365/public/images/down-arrow.png)no-repeat 95% center;">
										<option value="">Country</option>
										@foreach($countryList as $country)
										<option value="{{$country->id}}">{{ucfirst($country->nicename)}}</option>
										@endforeach					
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group"><input name="office_phone" id="office_phone" type="text" class="numberControl form-control office_validation charCount"  placeholder="Phone" maxlength="20"></div>
							</div>
							
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="Create-btn-cont">
									<button type="button" onclick="validation()" class="btn save">Add</button>
								</div>

							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="error-message" style="display: none;">
				<span id="resp"></span>
			</div>
		</div>
	</div>
</main>

<script type="text/javascript">

	$(".numberControl").on("keypress keyup blur",function (event) {    
		$(this).val($(this).val().replace(/[^\d].+/, ""));
		if ((event.which < 48 || event.which > 57)) {
			event.preventDefault();
		}
	});

	function validation() {

		$('.error-message').show();

		var name    	   = $('#name').val();
		var nemp           = $('#nemp').val();
		var office_address = $('#office_address').val();
		var office_city    = $('#office_city').val();
		var office_country = $('#office_country').val();
		var office_phone   = $('#office_phone').val();
		

		var empty = false;
		$('.charCount').each(function() {
			if ($(this).val().length > 50){
				empty = true;
			}
		});


		var checkOrgCount = false;
		$('.checkOrgCount').each(function() {
			if ($(this).val().length > 150){
				checkOrgCount = true;
			}
		});

		if (checkOrgCount){

			$('#resp').html('');
			$('#resp').html('All input fields should be less than 150 characters.');
		}else if (empty){

			$('#resp').html('');
			$('#resp').html('All input fields should be less than 50 characters.');
		}else if($.trim(name)==''){

			$('#resp').html('');
			$('#resp').html('Please enter Office Name.');
		}else if($.trim(nemp)==''){

			$('#resp').html('');
			$('#resp').html('Please enter Number of Employee.');
		}else if($.trim(office_address)==''){

			$('#resp').html('');
			$('#resp').html('Please enter Office Address.');
		}else if($.trim(office_city)==''){

			$('#resp').html('');
			$('#resp').html('Please enter Office City.');
		}else if($.trim(office_country)==''){

			$('#resp').html('');
			$('#resp').html('Please enter Office Country.');
		}else if($.trim(office_phone)==''){

			$('#resp').html('');
			$('#resp').html('Please enter Office Phone.');
		}else{

			$('.save').prop("disabled", true);

			$('#add-office-form').submit();      	
		}
	}


</script>

@include('layouts.adminFooter')