<!-- header -->
@section('title', 'Edit DOT')
@include('layouts.adminHeader')
<main class="main-content">
  <div class="add-fild-section dot">
    <div class="container">    
      <div class="dot-section Belfs-valus-section Reports">
        <div class="form-section">
          <div class="Belfs-valus">
            <div class="Reports-tital">
              <div align="center">              
                @if(session('error'))
                <div class="alert alert-danger" role="alert">                                    
                  {{session('error')}}
                </div>
                @endif
                
                <div class="loader-img" style="display: none;width: 70px;"><img src="{{asset('public/images/loader.gif')}}"></div>
              </div> 

              <div class="prof-acount-section report-belief">
                <div class="compy-logo" style="padding-bottom: 15px;">
                  @if(!empty($organisations->ImageURL))
                  <img src="{{asset('public/uploads/org_images\/').$organisations->ImageURL}}" class="mCS_img_loaded">
                  @endif
                  <h2>
                    @if(!empty($organisations->organisation))
                    {{ucfirst($organisations->organisation)}}
                    @endif
                  </h2>
                </div>

                <div class="search-cot">
                 <a href="{{URL::to('admin/report-belief-value-rating-list/'.base64_encode($orgId))}}"><img src="{{asset('public/images/menu.png')}}"></a>            
                 <select id="belief" name="beliefId">
                  <option value="" selected>All Belief</option> 
                  @foreach($beliefList as $bValue)        
                  <option {{(session('searchBeliefId')==$bValue->id)?'selected':''}} value="{{$bValue->id}}">{{ucfirst($bValue->name)}}</option>
                  @endforeach
                </select>               
              </div> 
            </div>
          </div>  
          <div class="Belfs-valus-repeat">
            <div class="edit-belief-btn">                 
            </div>


            <div class="row">
              @foreach($dotValuesArray as $belief)
              <div class="col-md-4">
                <div class="form-group">
                  <label> Belief: {{$belief['beliefName']}} </label>
                  <div class="col-md-12">
                    <div id='beliefChart{{$belief['beliefId']}}'></div>

                  </div>
                </div>
                <div class="row">
                 <label> Values</label>   
                 @foreach($belief['beliefValues'] as $bValues)
                 <div class="col-md-12">
                  <div id='valueChart{{$bValues['valueId']}}'></div>
                </div>
                @endforeach
              </div>
            </div>
            @endforeach  
          </div>  

        </div>
      </div>
    </div>
  </div>
</div>
</div>  
</main>

<script>
//get new value rating charts
@foreach($dotValuesArray as $belief)

var myConfig = {
  type: "gauge",
  globals: {
    fontSize: 10
  },
  plotarea: {
    marginTop: 10
  },
  plot: {
    size: '100%',
    valueBox: {
      placement: 'center',
          text: '%v', //default ucfirst($bValues['beliefName'])
          fontSize: 10,
          rules: [{
            rule: '%v >= 0',
            text: '{{ucfirst($belief['beliefName'])}}<br>%v'
          }]
        }
      },
      tooltip: {
        borderRadius: 5
      },
      scaleR: {
        aperture:300,
        minValue: 0,
        maxValue: 5,
        step: 1,
        center: {
          visible: false
        },
        tick: {
          visible: false
        },
        
        labels: ['0','1', '2', '3', '4', '5'],
        ring: {
          size: 20,

          <?php $beliefRatings = $belief['beliefRatings']; ?>
          
          <?php
          if($beliefRatings >=0 && $beliefRatings <1)
          {
            ?>

            rules: [{
              rule: '%v >= 0 && %v < 1',
              backgroundColor: '#d41111'
            }, {
              rule: '%v >= 1 && %v < 2',
              backgroundColor: '#ffe9c5'
            }, {
              rule: '%v >= 2 && %v < 3',
              backgroundColor: '#fff7c3'
            }, {
              rule: '%v >= 3 && %v < 4',
              backgroundColor: '#e8ffaf'
            }, {
              rule: '%v >= 4 && %v < 5',
              backgroundColor: '#c4ffb5'
            }]
            <?php
          }
          elseif ($beliefRatings >=1 && $beliefRatings <2)
          {
            ?>


            rules: [{
              rule: '%v >= 0 && %v < 1',
              backgroundColor: '#ffcfcf'
            }, {
              rule: '%v >= 1 && %v < 2',
              backgroundColor: '#ed9407'
            }, {
              rule: '%v >= 2 && %v < 3',
              backgroundColor: '#fff7c3'
            }, {
              rule: '%v >= 3 && %v < 4',
              backgroundColor: '#e8ffaf'
            }, {
              rule: '%v >= 4 && %v < 5',
              backgroundColor: '#c4ffb5'
            }]
            <?php
          }
          elseif ($beliefRatings >=2 && $beliefRatings <3)
          {
            ?>


            rules: [{
              rule: '%v >= 0 && %v < 1',
              backgroundColor: '#ffcfcf'
            }, {
              rule: '%v >= 1 && %v < 2',
              backgroundColor: '#ffe9c5'
            }, {
              rule: '%v >= 2 && %v < 3',
              backgroundColor: '#f1d621'
            }, {
              rule: '%v >= 3 && %v < 4',
              backgroundColor: '#e8ffaf'
            }, {
              rule: '%v >= 4 && %v < 5',
              backgroundColor: '#c4ffb5'
            }]
            <?php
          }
          elseif ($beliefRatings >=3 && $beliefRatings <4)
          {
            ?>


            rules: [{
              rule: '%v >= 0 && %v < 1',
              backgroundColor: '#ffcfcf'
            }, {
              rule: '%v >= 1 && %v < 2',
              backgroundColor: '#ffe9c5'
            }, {
              rule: '%v >= 2 && %v < 3',
              backgroundColor: '#fff7c3'
            }, {
              rule: '%v >= 3 && %v < 4',
              backgroundColor: '#91bc29'
            }, {
              rule: '%v >= 4 && %v < 5',
              backgroundColor: '#c4ffb5'
            }]
            <?php
          }
          elseif ($beliefRatings >=4 && $beliefRatings <=5)
          {
            ?>

            rules: [{
              rule: '%v >= 0 && %v < 1',
              backgroundColor: '#ffcfcf'
            }, {
              rule: '%v >= 1 && %v < 2',
              backgroundColor: '#ffe9c5'
            }, {
              rule: '%v >= 2 && %v < 3',
              backgroundColor: '#fff7c3'
            }, {
              rule: '%v >= 3 && %v < 4',
              backgroundColor: '#e8ffaf'
            }, {
              rule: '%v >= 4 && %v < 5',
              backgroundColor: '#208406'
            }]

            <?php
          }
          ?>

        }
      },

      series: [{
        values: [<?php echo number_format($belief['beliefRatings'],2); ?>],
        backgroundColor: 'black',
        indicator: [5,1,1,50,0.75],
        animation: {
          effect: 2,
          method: 1,
          sequence: 4,
          speed: 900
        },
      }]
    };

    zingchart.render({
      title:'hello',
      id: 'beliefChart{{$belief['beliefId']}}',
      data: myConfig,
      height: 300

    });


//for belief values
@foreach($belief['beliefValues'] as $bValues)

var myConfig1 = {
  type: "gauge",

  globals: {
    fontSize: 10
  },
  plotarea: {
    marginTop: 5
  },
  plot: {
    size: '100%',
    valueBox: {
      placement: 'center',
      text: '%v',
      fontSize: 10,
      rules: [{
        rule: '%v >= 0',
        text: '{{$bValues['valueName']}}<br>%v'
      }]
    }
  },
  tooltip: {
    borderRadius: 5
  },
  scaleR: {
    aperture:300,
    values:"0:5:1",
    center: {
      visible: false
    },
    tick: {
      visible: false
    },

    labels: ['0','1', '2', '3', '4', '5'],
    ring: {
      size: 20,

      <?php $valueRatings = $bValues['valueRatings']; ?>

      <?php
      if($valueRatings >=0 && $valueRatings <1)
      {
        ?>


        rules: [{
          rule: '%v >= 0 && %v < 1',
          backgroundColor: '#d41111'
        }, {
          rule: '%v >= 1 && %v < 2',
          backgroundColor: '#ffe9c5'
        }, {
          rule: '%v >= 2 && %v < 3',
          backgroundColor: '#fff7c3'
        }, {
          rule: '%v >= 3 && %v < 4',
          backgroundColor: '#e8ffaf'
        }, {
          rule: '%v >= 4 && %v < 5',
          backgroundColor: '#c4ffb5'
        }]
        <?php
      }
      elseif ($valueRatings >=1 && $valueRatings <2)
      {
        ?>


        rules: [{
          rule: '%v >= 0 && %v < 1',
          backgroundColor: '#ffcfcf'
        }, {
          rule: '%v >= 1 && %v < 2',
          backgroundColor: '#ed9407'
        }, {
          rule: '%v >= 2 && %v < 3',
          backgroundColor: '#fff7c3'
        }, {
          rule: '%v >= 3 && %v < 4',
          backgroundColor: '#e8ffaf'
        }, {
          rule: '%v >= 4 && %v < 5',
          backgroundColor: '#c4ffb5'
        }]
        <?php
      }
      elseif ($valueRatings >=2 && $valueRatings <3)
      {
        ?>


        rules: [{
          rule: '%v >= 0 && %v < 1',
          backgroundColor: '#ffcfcf'
        }, {
          rule: '%v >= 1 && %v < 2',
          backgroundColor: '#ffe9c5'
        }, {
          rule: '%v >= 2 && %v < 3',
          backgroundColor: '#f1d621'
        }, {
          rule: '%v >= 3 && %v < 4',
          backgroundColor: '#e8ffaf'
        }, {
          rule: '%v >= 4 && %v < 5',
          backgroundColor: '#c4ffb5'
        }]
        <?php
      }
      elseif ($valueRatings >=3 && $valueRatings <4)
      {
        ?>


        rules: [{
          rule: '%v >= 0 && %v < 1',
          backgroundColor: '#ffcfcf'
        }, {
          rule: '%v >= 1 && %v < 2',
          backgroundColor: '#ffe9c5'
        }, {
          rule: '%v >= 2 && %v < 3',
          backgroundColor: '#fff7c3'
        }, {
          rule: '%v >= 3 && %v < 4',
          backgroundColor: '#91bc29'
        }, {
          rule: '%v >= 4 && %v < 5',
          backgroundColor: '#c4ffb5'
        }]
        <?php
      }
      elseif ($valueRatings >=4 && $valueRatings <=5)
      {
        ?>

        rules: [{
          rule: '%v >= 0 && %v < 1',
          backgroundColor: '#ffcfcf'
        }, {
          rule: '%v >= 1 && %v < 2',
          backgroundColor: '#ffe9c5'
        }, {
          rule: '%v >= 2 && %v < 3',
          backgroundColor: '#fff7c3'
        }, {
          rule: '%v >= 3 && %v < 4',
          backgroundColor: '#e8ffaf'
        }, {
          rule: '%v >= 4 && %v < 5',
          backgroundColor: '#208406'
        }]

        <?php
      }
      ?>

    }
  },

  series: [{
    values: [<?php echo number_format($bValues['valueRatings'],2); ?>],
    backgroundColor: 'black',
    indicator: [5,1,10,20,0.75],

  }]
};

zingchart.render({
  id: 'valueChart{{$bValues['valueId']}}',
  data: myConfig1,
  height: 300

});

@endforeach //for belief value

@endforeach
</script>
<script type="text/javascript">
  $('#belief').on('change', function() {
    $('.loader-img').show();
    var beliefId = $('#belief').val();

    $.ajax({
      type: "POST",
      url: "{!!URL::to('admin/dot-reports/'.base64_encode($orgId))!!}",   
      data: {beliefId:beliefId,"_token":'<?php echo csrf_token()?>'},
      success: function(response) 
      {
          // console.log(response)
          $("body").html(response);
        }
      });
  });

  (function($){
    $(window).on("load",function(){

      $("#content-1").mCustomScrollbar({
        axis:"x",
        advanced:{
          autoExpandHorizontalScroll:true
        }
      });
    });
  });
</script>
@include('layouts.adminFooter')