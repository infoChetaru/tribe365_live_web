<div class="organistiondetail-section">
	<div class="row">
		<div class="col-md-12">	
			<div class="search-cot">				
				<form id="cot-team-role-form" action="#">
					{{csrf_field()}}					
					<select id="office" name="officeId">
						<option value="" selected>All Offices</option>
						@foreach($offices as $office)
						<option {{($officeId==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
						@endforeach
					</select>
					<select id="department" name="departmentId">
						@if($departments && !empty($officeId))
						<option value="">All Department</option>
						@foreach($departments as $cdept)
						<option {{($departmentId==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
						@endforeach
						@elseif($all_department)
						<option value="">All Department</option>
						@foreach($all_department as $dept)
						<option {{($departmentId==$dept->id)?'selected':''}} value="{{$dept->id}}">{{$dept->department}}</option>
						@endforeach	
						@endif
					</select>		
					<button class="cot-team-role-map" type="button">Search</button>
				</form> 
			</div>	
		</div>
	</div>
	<div class="cot-tab-detail-section">
		<div class="tab-pane" id="individual" class="active">
			<div class="individual-content team-content">
				<div class="individual-cate">
					<div class="cate-left">
						<div class="cat-left-tital">
							<h4> Organisation</h4>
							<span class="shaper">  {{ucfirst($cotRoleMapOptions[0]->maper)}} </span>
							<span class="coordinator"> {{ucfirst($cotRoleMapOptions[1]->maper)}} </span>
						</div>
						<div class="cat-left-tital">
							<h4> Controllers</h4>
							<span class="implementer">  {{ucfirst($cotRoleMapOptions[2]->maper)}} </span>
							<span class="completerFinisher"> {{ucfirst($cotRoleMapOptions[3]->maper)}} </span>
						</div>
						<div class="cat-left-tital">
							<h4> Advisors</h4>
							<span class="monitorEvaluator"> {{ucfirst($cotRoleMapOptions[4]->maper)}} </span>
							<span class="teamworker"> {{ucfirst($cotRoleMapOptions[5]->maper)}} </span>
						</div>
						<div class="cat-left-tital">
							<h4> Explorers</h4>
							<span class="plant">  {{ucfirst($cotRoleMapOptions[6]->maper)}} </span>
							<span class="resourceInvestigator"> {{ucfirst($cotRoleMapOptions[7]->maper)}} </span>
						</div>
					</div>
					<div class="cate-right">

						<div class="cate-user-section">
							{{--	@foreach($cotTeamRoleMapGraphPercentage as $value)
								<div class="cate-user-tital">									
									<span class="">{{print_r($value)}}%</span>
								</div>		
								@endforeach	--}}
							</div>


							@foreach($cotTeamRoleMapGraphPercentage as $value)
							
							<div class="cate-user-section">
								
								<div class="cate-user-tital">								
									<span class="{{$value['shaper']}}">{{$value['shaper']}}%</span>
									<span class="{{$value['coordinator']}}">{{$value['coordinator']}}%</span>
								</div>

								<div class="cate-user-tital">
									<span class="{{$value['implementer']}}">{{$value['implementer']}}%</span>											
									<span class="{{$value['completerFinisher']}}">{{$value['completerFinisher']}}%</span>
								</div>

								<div class="cate-user-tital">									
									<span class="{{$value['monitorEvaluator']}}">{{$value['monitorEvaluator']}}%</span>									
									<span class="{{$value['teamworker']}}">{{$value['teamworker']}}%</span>
								</div>

								<div class="cate-user-tital">											
									<span class="{{$value['plant']}}">{{$value['plant']}}%</span>
									<span class="{{$value['resourceInvestigator']}}">{{$value['resourceInvestigator']}}%</span>
								</div>											
							</div>
							
							@endforeach
							
						</div>
					</div>
				</div>
			</div>		
		</div>

		<script type="text/javascript">

			$('.cot-team-role-map').on('click',function(){
				$('.loader-img').show();			
			// get form values 
			var form_values   = $("#cot-team-role-form").serializeArray();
			
			var officeId      = form_values[1]['value'];
			var departmentId  = form_values[2]['value'];
			var orgId = "{{base64_encode($orgId)}}";
			

			$.ajax({
				type: "POST",
				url: "{{URL::to('admin/getCOTteamRoleMapGraph')}}",
				data: {orgId:orgId,officeId:officeId,departmentId:departmentId,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					$('.loader-img').hide();
					$('#cot2').html('');
					$('#cot5').html('');
					$('#cot3').html('');	
					$('#cot2').append(response);
				}
			});		

		});

	</script>

	<script type="text/javascript">
		$('#office').on('change', function() {
			$('#department option').remove();
			var officeId = $('#office').val();

			$.ajax({
				type: "POST",
				url: "{!!URL::to('admin/getDepartmentByOfficecIdforGraph')!!}",				
				data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
				success: function(response) 
				{					
					$('#department').append('<option value="" selected>All Department</option>');
					$('#department').append(response);
				}
			});
		});
	</script>
