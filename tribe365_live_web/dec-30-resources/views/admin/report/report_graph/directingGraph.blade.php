<div>
<ul class="nav nav-tabs">
	<li><a class="active show" data-toggle="tab" href="#other">List</a></li>
	<li><a class="" data-toggle="tab" href="#graph">Graph</a></li>	
</ul>
<div class="tab-content">

	<div id="other" class="tab-pane fade active show">

		<div class="search-cot">
			<a href="{{URL::to('admin/report-belief-value-rating-list/'.base64_encode($org->id))}}"><img src="{{asset('public/images/menu.png')}}"></a>            
			<select id="belief" name="beliefId">
				<option value="" selected>All Belief</option> 
				@foreach($beliefList as $bValue)        
				<option {{(session('searchBeliefId')==$bValue->id)?'selected':''}} value="{{$bValue->id}}">{{ucfirst($bValue->name)}}</option>
				@endforeach
			</select>  

			<select id="office" name="officeId">
				<option value="" selected>All Office</option> 
				@foreach($offices as $ovalue)        
				<option {{($officeId==$ovalue->id)?'selected':''}} value="{{$ovalue->id}}">{{ucfirst($ovalue->office)}}</option>
				@endforeach
			</select>                
		</div>
		
		<div class="dot-belief-value">
			<table>
				<tr>
					@foreach($dotValuesArray as $belief1)
					<td> 
						<div class="dot-belief <?php 

						$beliefRatings = $belief1['beliefRatings'];

						if($beliefRatings >=0 && $beliefRatings <1)
						{
							echo ("red_bg");
						}						                    
						elseif($beliefRatings >=1 && $beliefRatings <2)
						{
							echo("orange_bg");
						}
						elseif($beliefRatings >=2 && $beliefRatings <3)
						{
							echo("yellow_bg");
						}
						elseif($beliefRatings >=3 && $beliefRatings <4)
						{
							echo("lightgreen_bg");
						}
						elseif($beliefRatings >=4 && $beliefRatings <5)
						{
							echo("green_bg");
						}
						?>">
						<h3> {{$belief1['beliefName']}} </h3>
						<strong> {{round($belief1['beliefRatings'],2)}} </strong>
					</div>
					<div class="dot-value">
						@foreach($belief1['beliefValues'] as $bValues1)
						<div class="dot-value-inner <?php 

						$beliefRatings = $bValues1['valueRatings'];

						if($beliefRatings >=0 && $beliefRatings <1)
						{
							echo ("red_bg");
						}						                    
						elseif($beliefRatings >=1 && $beliefRatings <2)
						{
							echo("orange_bg");
						}
						elseif($beliefRatings >=2 && $beliefRatings <3)
						{
							echo("yellow_bg");
						}
						elseif($beliefRatings >=3 && $beliefRatings <4)
						{
							echo("lightgreen_bg");
						}
						elseif($beliefRatings >=4 && $beliefRatings <=5)
						{
							echo("green_bg");
						}
						?>">
						<h4>{{$bValues1['valueName']}}</h4>
						<strong> {{round($bValues1['valueRatings'],2)}} </strong>
					</div>
					@endforeach

				</div>
			</td>					                    
			@endforeach
		</tr>					                    
	</table>
</div>
</div>	


<!-- dot graph meters -->
<div id="graph" class="tab-pane fade">

<!-- <div class="search-cot">
	<a href="{{URL::to('admin/report-belief-value-rating-list/'.base64_encode($org->id))}}"><img src="{{asset('public/images/menu.png')}}"></a>            
	<select id="belief" name="beliefId">
		<option value="" selected>All Belief</option> 
		@foreach($beliefList as $bValue)        
		<option {{(session('searchBeliefId')==$bValue->id)?'selected':''}} value="{{$bValue->id}}">{{ucfirst($bValue->name)}}</option>
		@endforeach
	</select>  

	<select id="office" name="officeId">
		<option value="" selected>All Office</option> 
		@foreach($offices as $ovalue)        
		<option {{($officeId==$ovalue->id)?'selected':''}} value="{{$ovalue->id}}">{{ucfirst($ovalue->office)}}</option>
		@endforeach
	</select>                
</div> -->

<div class="belif-value Belfs-valus-repeat">
	<div class="row">
		@foreach($dotValuesArray as $belief)
		<div class="col-md-4">
			<div class="form-group">
				<label> Belief: {{$belief['beliefName']}} </label>
				<div class="col-md-12">
					<div id='beliefChart{{$belief['beliefId']}}'></div>
				</div>
			</div>
			<div class="row">
				<label> Values</label>   
				@foreach($belief['beliefValues'] as $bValues)
				<div class="col-md-12">
					<div id='valueChart{{$bValues['valueId']}}'></div>
				</div>
				@endforeach
			</div>
		</div>
		@endforeach  
	</div>
</div> 
</div>


<script type="text/javascript">
$('#belief').on('change', function() {
	$('.loader-img').show();
	var orgId = "{{base64_encode($org->id)}}";		
	var beliefId = $('#belief').val();
	var start_date = $('#start_date').val().trim();
	var end_date   = $('#end_date').val().trim();

	$.ajax({
		type: "POST",
		url: "{{URL::to('admin/dotTabData')}}",   
		data: {orgId:orgId,beliefId:beliefId,startDate:start_date,endDate:end_date,"_token":'<?php echo csrf_token()?>'},
		success: function(response) 
		{
			$('.loader-img').hide();
			$('#cot19').html('');	
			$('#cot19').html(response);
		}
	});
});

(function($){
	$(window).on("load",function(){

		$("#content-1").mCustomScrollbar({
			axis:"x",
			advanced:{
				autoExpandHorizontalScroll:true
			}
		});
	});
});


$('#office').on('change', function() {
		$('.loader-img').show();
		var orgId = "{{base64_encode($org->id)}}";		
		var officeId = $('#office').val();
		var start_date = $('#start_date').val().trim();
		var end_date   = $('#end_date').val().trim();

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/dotTabData')}}",   
			data: {orgId:orgId,officeId:officeId,startDate:start_date,endDate:end_date,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{	
				$('.loader-img').hide();
				$('#cot19').html('');	
				$('#cot19').html(response);
				$('#start_date').val(start_date);
				$('#end_date').val(end_date);
			}
		});
	});

	(function($){
		$(window).on("load",function(){

			$("#content-1").mCustomScrollbar({
				axis:"x",
				advanced:{
					autoExpandHorizontalScroll:true
				}
			});
		});
	});
</script>

<script>
//get new value rating charts
@foreach($dotValuesArray as $belief)

var myConfig = {
	type: "gauge",
	globals: {
		fontSize: 10
	},
	plotarea: {
		marginTop: 10
	},
	plot: {
		size: '100%',
		valueBox: {
			placement: 'center',
          text: '%v', //default ucfirst($bValues['beliefName'])
          fontSize: 10,
          rules: [{
          	rule: '%v >= 0',
          	text: '{{ucfirst($belief['beliefName'])}}<br>%v'
          }]
      }
  },
  tooltip: {
  	borderRadius: 5
  },
  scaleR: {
  	aperture:300,
  	minValue: 0,
  	maxValue: 5,
  	step: 1,
  	center: {
  		visible: false
  	},
  	tick: {
  		visible: false
  	},

  	labels: ['0','1', '2', '3', '4', '5'],
  	ring: {
  		size: 20,

  		<?php $beliefRatings = $belief['beliefRatings']; ?>

  		<?php
  		if($beliefRatings >=0 && $beliefRatings <1)
  		{
  			?>

  			rules: [{
  				rule: '%v >= 0 && %v < 1',
  				backgroundColor: '#b2b2b2'
  			}, {
  				rule: '%v >= 1 && %v < 2',
  				backgroundColor: '#ffa3a6'
  			}, {
  				rule: '%v >= 2 && %v < 3',
  				backgroundColor: '#ff8085'
  			}, {
  				rule: '%v >= 3 && %v < 4',
  				backgroundColor: '#ff454b'
  			}, {
  				rule: '%v >= 4 && %v < 5',
  				backgroundColor: '#eb1c24'
  			}]
  			<?php
  		}
  		elseif ($beliefRatings >=1 && $beliefRatings <2)
  		{
  			?>


  			rules: [{
  				rule: '%v >= 0 && %v < 1',
  				backgroundColor: '#b2b2b2'
  			}, {
  				rule: '%v >= 1 && %v < 2',
  				backgroundColor: '#ffa3a6'
  			}, {
  				rule: '%v >= 2 && %v < 3',
  				backgroundColor: '#ff8085'
  			}, {
  				rule: '%v >= 3 && %v < 4',
  				backgroundColor: '#ff454b'
  			}, {
  				rule: '%v >= 4 && %v < 5',
  				backgroundColor: '#eb1c24'
  			}]
  			<?php
  		}
  		elseif ($beliefRatings >=2 && $beliefRatings <3)
  		{
  			?>


  			rules: [{
  				rule: '%v >= 0 && %v < 1',
  				backgroundColor: '#b2b2b2'
  			}, {
  				rule: '%v >= 1 && %v < 2',
  				backgroundColor: '#ffa3a6'
  			}, {
  				rule: '%v >= 2 && %v < 3',
  				backgroundColor: '#ff8085'
  			}, {
  				rule: '%v >= 3 && %v < 4',
  				backgroundColor: '#ff454b'
  			}, {
  				rule: '%v >= 4 && %v < 5',
  				backgroundColor: '#eb1c24'
  			}]
  			<?php
  		}
  		elseif ($beliefRatings >=3 && $beliefRatings <4)
  		{
  			?>


  			rules: [{
  				rule: '%v >= 0 && %v < 1',
  				backgroundColor: '#b2b2b2'
  			}, {
  				rule: '%v >= 1 && %v < 2',
  				backgroundColor: '#ffa3a6'
  			}, {
  				rule: '%v >= 2 && %v < 3',
  				backgroundColor: '#ff8085'
  			}, {
  				rule: '%v >= 3 && %v < 4',
  				backgroundColor: '#ff454b'
  			}, {
  				rule: '%v >= 4 && %v < 5',
  				backgroundColor: '#eb1c24'
  			}]
  			<?php
  		}
  		elseif ($beliefRatings >=4 && $beliefRatings <=5)
  		{
  			?>

  			rules: [{
  				rule: '%v >= 0 && %v < 1',
  				backgroundColor: '#b2b2b2'
  			}, {
  				rule: '%v >= 1 && %v < 2',
  				backgroundColor: '#ffa3a6'
  			}, {
  				rule: '%v >= 2 && %v < 3',
  				backgroundColor: '#ff8085'
  			}, {
  				rule: '%v >= 3 && %v < 4',
  				backgroundColor: '#ff454b'
  			}, {
  				rule: '%v >= 4 && %v < 5',
  				backgroundColor: '#eb1c24'
  			}]

  			<?php
  		}
  		?>

  	}
  },

  series: [{
  	values: [<?php echo number_format($belief['beliefRatings'],2); ?>],
  	backgroundColor: 'black',
  	indicator: [5,1,1,50,0.75],
  	animation: {
  		effect: 2,
  		method: 1,
  		sequence: 4,
  		speed: 900
  	},
  }]
};

zingchart.render({
	title:'hello',
	id: 'beliefChart{{$belief['beliefId']}}',
	data: myConfig,
	height: 300

});


//for belief values
@foreach($belief['beliefValues'] as $bValues)

var myConfig1 = {
	type: "gauge",

	globals: {
		fontSize: 10
	},
	plotarea: {
		marginTop: 5
	},
	plot: {
		size: '100%',
		valueBox: {
			placement: 'center',
			text: '%v',
			fontSize: 10,
			rules: [{
				rule: '%v >= 0',
				text: '{{$bValues['valueName']}}<br>%v'
			}]
		}
	},
	tooltip: {
		borderRadius: 5
	},
	scaleR: {
		aperture:300,
		values:"0:5:1",
		center: {
			visible: false
		},
		tick: {
			visible: false
		},

		labels: ['0','1', '2', '3', '4', '5'],
		ring: {
			size: 20,

			<?php $valueRatings = $bValues['valueRatings']; ?>

			<?php
			if($valueRatings >=0 && $valueRatings <1)
			{
				?>


				rules: [{
					rule: '%v >= 0 && %v < 1',
					backgroundColor: '#b2b2b2'
				}, {
					rule: '%v >= 1 && %v < 2',
					backgroundColor: '#ffa3a6'
				}, {
					rule: '%v >= 2 && %v < 3',
					backgroundColor: '#ff8085'
				}, {
					rule: '%v >= 3 && %v < 4',
					backgroundColor: '#ff454b'
				}, {
					rule: '%v >= 4 && %v < 5',
					backgroundColor: '#eb1c24'
				}]
				<?php
			}
			elseif ($valueRatings >=1 && $valueRatings <2)
			{
				?>


				rules: [{
					rule: '%v >= 0 && %v < 1',
					backgroundColor: '#b2b2b2'
				}, {
					rule: '%v >= 1 && %v < 2',
					backgroundColor: '#ffa3a6'
				}, {
					rule: '%v >= 2 && %v < 3',
					backgroundColor: '#ff8085'
				}, {
					rule: '%v >= 3 && %v < 4',
					backgroundColor: '#ff454b'
				}, {
					rule: '%v >= 4 && %v < 5',
					backgroundColor: '#eb1c24'
				}]
				<?php
			}
			elseif ($valueRatings >=2 && $valueRatings <3)
			{
				?>


				rules: [{
					rule: '%v >= 0 && %v < 1',
					backgroundColor: '#b2b2b2'
				}, {
					rule: '%v >= 1 && %v < 2',
					backgroundColor: '#ffa3a6'
				}, {
					rule: '%v >= 2 && %v < 3',
					backgroundColor: '#ff8085'
				}, {
					rule: '%v >= 3 && %v < 4',
					backgroundColor: '#ff454b'
				}, {
					rule: '%v >= 4 && %v < 5',
					backgroundColor: '#eb1c24'
				}]
				<?php
			}
			elseif ($valueRatings >=3 && $valueRatings <4)
			{
				?>


				rules: [{
					rule: '%v >= 0 && %v < 1',
					backgroundColor: '#b2b2b2'
				}, {
					rule: '%v >= 1 && %v < 2',
					backgroundColor: '#ffa3a6'
				}, {
					rule: '%v >= 2 && %v < 3',
					backgroundColor: '#ff8085'
				}, {
					rule: '%v >= 3 && %v < 4',
					backgroundColor: '#ff454b'
				}, {
					rule: '%v >= 4 && %v < 5',
					backgroundColor: '#eb1c24'
				}]
				<?php
			}
			elseif ($valueRatings >=4 && $valueRatings <=5)
			{
				?>

				rules: [{
					rule: '%v >= 0 && %v < 1',
					backgroundColor: '#b2b2b2'
				}, {
					rule: '%v >= 1 && %v < 2',
					backgroundColor: '#ffa3a6'
				}, {
					rule: '%v >= 2 && %v < 3',
					backgroundColor: '#ff8085'
				}, {
					rule: '%v >= 3 && %v < 4',
					backgroundColor: '#ff454b'
				}, {
					rule: '%v >= 4 && %v < 5',
					backgroundColor: '#eb1c24'
				}]

				<?php
			}
			?>

		}
	},

	series: [{
		values: [<?php echo number_format($bValues['valueRatings'],2); ?>],
		backgroundColor: 'black',
		indicator: [5,1,10,20,0.75],

	}]
};

zingchart.render({
	id: 'valueChart{{$bValues['valueId']}}',
	data: myConfig1,
	height: 300

});

@endforeach //for belief value

@endforeach
</script>