<div class="search-cot">				
	<form id="report-diagnostic-form" action="#">
		{{csrf_field()}}					
		<select id="diagnostic-office" name="officeId">
			<option value="" selected>All Offices</option>
			@foreach($offices as $office)
			<option {{($officeId==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
			@endforeach
		</select>
		<select id="diagnostic-department" name="departmentId">			
			@if($departments && !empty($officeId))
			<option value="">All Department</option>
			@foreach($departments as $cdept)
			<option {{($departmentId==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
			@endforeach
			@elseif($all_department)
			<option value="">All Department</option>
			@foreach($all_department as $dept)
			<option {{($departmentId==$dept->id)?'selected':''}} value="{{$dept->id}}">{{$dept->department}}</option>
			@endforeach	
			@endif
		</select>		
		<button class="report-diagnostic-form" type="button">Search</button>
	</form> 
</div>							
<canvas style="margin-top: 20px;" id="myChartDiagnostic" width="1000" height="400"></canvas>

<script type="text/javascript">
	$('.report-diagnostic-form').on('click',function(){
		$('.loader-img').show();			

		var form_values   = $("#report-diagnostic-form").serializeArray();

		var officeId      = form_values[1]['value'];
		var departmentId  = form_values[2]['value'];
		var orgId         = "{{base64_encode($orgId)}}";
		var start_date = $('#start_date').val().trim();
		var end_date   = $('#end_date').val().trim();

		console.log(form_values)
		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getReportDiagnosticGraph')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,startDate:start_date,endDate:end_date,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				$('.loader-img').hide();
				$('#cot2').html('');
				$('#cot5').html('');
				$('#cot3').html('');
				$('#cot6').html('');	
				$('#cot6').append(response);
			}
		});		
	});
</script>

<?php 

$maxValueArray = array();
foreach($diagnosticResultArray as $value1)
{
	array_push($maxValueArray, $value1['percentage']);
}

$max=10;
if(!empty($maxValueArray))
{
	$max = max($maxValueArray);

	$max = ceil($max / 10) * 10;

	if($max>=100)
	{
		$max=100;
	}
}

?>

<!-- DIAGNOSTIC GRAPH -->
<script  type="text/javascript">

	var canvas = document.getElementById("myChartDiagnostic");
	var ctx = canvas.getContext("2d");
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: [@foreach($diagnosticResultArray as $value)"{{$value['title']}}",@endforeach],
			datasets: [{
				label: 'Percentage',
				data: [@foreach($diagnosticResultArray as $value1){{$value1['percentage']}},@endforeach],
				backgroundColor: [
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)', 
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)'
				],
				borderColor: [
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)'
				],
				borderWidth: 5
			}]
		},
		options: {			
			legend: { display: false},			
			scales: {
				xAxes: [{
					categoryPercentage: 0.25,
					ticks: {
						fontSize: 15
					},
					gridLines: {
						display: false
					}

				}],
				yAxes: [{					
					ticks: {
						beginAtZero:true,
						step:10,
						max:{{$max}},
					},
					gridLines: {
						display: false
					}
				}]
			}
		}

	});
//get onclick event
canvas.onclick = function(evt) {
	var activePoints = myChart.getElementsAtEvent(evt);
	if(activePoints[0])
	{

		var form_values   = $("#report-diagnostic-form").serializeArray();
		var officeId      = form_values[1]['value'];
		var departmentId  = form_values[2]['value'];

		var chartData = activePoints[0]['_chart'].config.data;
		var idx = activePoints[0]['_index'];
		var label = chartData.labels[idx];
		var value = chartData.datasets[0].data[idx];
		var orgId         = "{{$orgId}}";
		console.log(idx)

		var barIndex = idx;

		$.ajax({
			type: "POST",
			url: "{{route('getDiagnsticSubGraph')}}",				
			data: {barIndex:barIndex,orgId:orgId,officeId:officeId,departmentId:departmentId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{					
				$('.loader-img').hide();
				$('#cot2').html('');
				$('#cot5').html('');
				$('#cot3').html('');
				$('#cot6').html('');	
				$('#cot6').append(response);
			}
		});
	}
};


</script>

<script type="text/javascript">
	$('#diagnostic-office').on('change', function() {
		$('#diagnostic-department option').remove();
		var officeId = $('#diagnostic-office').val();

		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getDepartmentByOfficecIdforGraph')!!}",				
			data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{					
				$('#diagnostic-department').append('<option value="" selected>All Department</option>');
				$('#diagnostic-department').append(response);
			}
		});
	});
</script>