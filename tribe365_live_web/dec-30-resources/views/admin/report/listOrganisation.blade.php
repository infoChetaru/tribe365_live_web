@section('title', 'List Organisation Report')
@include('layouts.adminHeader')
<main class="main-content">
  <div class="add-fild-section organization-fild">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="loader-img" style="display: none;width: 70px; top: -57px;"><img src="{{asset('public/images/loader.gif')}}"></div>
          <div class="search-section">  
            <form>
              <input id="search-key" type="search" placeholder="Search...">
            </form>                                             
            <div align="center">                            
              @if(session('message'))
              <div class="alert alert-success" role="alert">                                    
                {{session('message')}}
              </div>
              @endif
              @if(session('error'))
              <div class="alert alert-danger" role="alert">                                    
                {{session('error')}}
              </div>
              @endif
            </div>                        
          </div>
        </div>
      </div>
      <div class="row">
        <div class="organization-section  manage-reports ">
          @foreach($organisation as $value)
          <div class="col-md-3">
            <div class="company-section">
              <div class="company-detale">   
                <a href="{{route('reports.show',base64_encode($value->id))}}">
                  @if(!empty($value->ImageURL))
                  <img src="{{ asset('public/uploads/org_images').'/'.$value->ImageURL }}">
                  @else 
                  <img src="{{ asset('public/images/no-image.png')}}">
                  @endif
                </a>
                <h2> {{ucfirst($value->organisation)}} </h2>
              </div>
              <div class="company-ovel-text">
                <span>generate report </span>
              </div>
            </div>
          </div>  
          @endforeach             
        </div>
      </div>
    </div>
  </div>
  <div class="organ-page-nav" >
    {!! $organisation->links('layouts.pagination') !!}                               
  </div>
</div>
</main>

<script type="text/javascript">
 $(document).ready(function(){
  $("#search-key").keyup(function(){
    $('.organization-section').html('');
    $('.organ-page-nav').html('');
    $('.loader-img').show();

    var orgName = $('#search-key').val();
    if(orgName==''){
      location.reload();
    }
    $.ajax({
      type: "POST",
      url: "{!!URL::to('admin/searchReportOrganisations')!!}",             
      data: {orgName:orgName,"_token":'<?php echo csrf_token()?>'},
      success: function(response) {

        console.log(response)

        if(response.length > 0){
          $('.organization-section').html('');
          $('.organization-section').append(response);
          $('.loader-img').hide();

        }else{

          $('.organization-section').html('');                                 
          $('.organization-section').append('<div align="center" style="width:100%" class="text-danger" role="alert">No result found.</div>');
          $('.loader-img').hide();
        }
      }
    }); 

  });
});
</script>

@include('layouts.adminFooter')