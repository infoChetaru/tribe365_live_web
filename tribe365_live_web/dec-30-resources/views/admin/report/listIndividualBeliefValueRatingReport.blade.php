<!-- header -->
@section('title', 'List Belief individual Value Rating')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                                    
								{{session('message')}}
							</div>
							@endif
						</div>
						 <div class="loader-img" style="display: none;width: 70px;"><img src="{{asset('public/images/loader.gif')}}"></div>
              
						<div class="prof-acount-section report-belief">
							<div class="compy-logo" style="padding-bottom: 15px;">								
								<img src="{{asset('public/uploads/org_images\/').$beliefs->ImageURL}}" class="mCS_img_loaded">								
								<h2>{{ucfirst($beliefs->organisation)}}</h2>
							</div>

							<div class="search-cot ">						
								<select id="custValue" name="beliefId">
									<option value="" selected>All Values</option>	
									@foreach($custValueList as $custValue)				
									<option {{(session('customValueId')==$custValue->id)?'selected':''}} value="{{$custValue->id}}">{{ucfirst($custValue->name)}}</option>
									@endforeach
								</select>	
								<a href="{{URL::to('admin/report-belief-value-rating-list/'.base64_encode($beliefs->orgId))}}" class="back-button"><button >Back</button></a>						
							</div>
						</div>

						<div class="value-list">
							

								@foreach($valueRatingArray as $bValue)
								<table>		
								<tr><td style="width: 175px;">Value</td><td class="value-tital" colspan="3">{{ucfirst($bValue->name)}} | {{$bValue->valueRating}}</td></tr>
								@if($bValue->ratingArray->isNotEmpty())
								<tr><th>User Rating</th><th style="width: 764px;">Description</th><th style="width: 200px;">User Name</th><th style="width: 200px;">Date & Time</th></tr>
								@endif
								@foreach($bValue->ratingArray as $bValuesRatings)
								<tr>
									<td>{{$bValuesRatings->ratings}}</td>
									<td>

										<?php 

										if ($bValuesRatings->ratings==4) {
											echo "I understand what it means to be ".ucfirst($bValue->name).", I am ".ucfirst($bValue->name)." every day at work, colleagues and clients would describe me as ".ucfirst($bValue->name);
										}elseif ($bValuesRatings->ratings==3) {
											echo "I understand what it means to be ".ucfirst($bValue->name).", I am ".ucfirst($bValue->name)." every day at work";
										}elseif ($bValuesRatings->ratings==2) {
											echo "I understand what it means to be ".ucfirst($bValue->name)." , I am ".ucfirst($bValue->name)." when I have to be ".ucfirst($bValue->name);
										}elseif ($bValuesRatings->ratings==1) {
											echo "I understand what it means to be ".ucfirst($bValue->name).",  I am never/rarely ".ucfirst($bValue->name);
										}elseif ($bValuesRatings->ratings==0) {
											echo "I do not understand what it means to be ".ucfirst($bValue->name);
										}else{
											echo "I understand what it means to be ".ucfirst($bValue->name).", I am ".ucfirst($bValue->name)." every day at work, colleagues and clients would describe me as ".ucfirst($bValue->name).", I help others become ".ucfirst($bValue->name);
										}

										?>
									</td>
									<td>{{ucfirst($bValuesRatings->name)}}</td>
									<td>{{date("Y-m-d | H:i:s", strtotime($bValuesRatings->created_at))}}</td>
								</tr>
								@endforeach

</table>
								@endforeach	 

							
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</main>
<script type="text/javascript">
	$('#custValue').on('change', function() {

		var custValueId = $('#custValue').val();
		$('.loader-img').show();
		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/report-belief-individual-ratings/'.base64_encode($beliefs->id))!!}",		
			data: {custValueId:custValueId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{
				// console.log(response)
				$("body").html(response);

			}
		});
	});
</script>

@include('layouts.adminFooter')