<!-- header -->
@section('title', 'Edit DOT')
@include('layouts.adminHeader')
<main class="main-content">
  <div class="add-fild-section dot">
    <div class="container">
     <a href="{{URL::to('admin/report-belief-value-rating-list/'.base64_encode($orgId))}}"><img src="{{asset('public/images/menu.png')}}"></a>
     <div class="dot-section">
      <h2> Reports </h2>
      <div class="form-section">        
        <div align="center">                            
          @if(session('message'))
          <div class="alert alert-success" role="alert">                                    
            {{session('message')}}
          </div>
          @endif
          @if(session('error'))
          <div class="alert alert-danger" role="alert">                                    
            {{session('error')}}
          </div>
          @endif
        </div>        
      </div>
    </div>     
    <div class="dot-section Belfs-valus-section">
      <div class="form-section">
        <div class="Belfs-valus">

         @foreach($dotValuesArray as $belief)

         <div class="Belfs-valus-repeat">
          <div class="edit-belief-btn">                 
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label> Beliefs: {{$belief['beliefName']}} </label>
                <div class="col-md-12">
                  <div id='beliefChart{{$belief['beliefId']}}'></div>
                </div>
              </div>
            </div>
            <div class="col-md-8">
              <label> Values</label>               
              <div class="row toggleOpen">

                @foreach($belief['beliefValues'] as $bValues)
                <div class="col-md-4">
                  <div id='valueChart{{$bValues['valueId']}}'></div>
                </div>
                @endforeach

              </div>    
            </div>
          </div>                
        </div>

        @endforeach  

      </div>
    </div>
  </div>
</div>
</div>  
</main>

<script>
//get new value rating charts
@foreach($dotValuesArray as $belief)

var myConfig = {
  type: "gauge",
  globals: {
    fontSize: 10
  },
  plotarea: {
    marginTop: 10
  },
  plot: {
    size: '100%',
    valueBox: {
      placement: 'center',
          text: '%v', //default
          fontSize: 15,
          rules: [{
            rule: '%v >= 5 && %v > 4',
            text: '%v'
          }, {
            rule: '%v <= 4 && %v > 3',
            text: '%v'
          }, {
            rule: '%v <= 3 && %v > 2',
            text: '%v'
          }, {
            rule: '%v <= 2 && %v > 1',
            text: '%v'
          }, {
            rule: '%v <= 1 && %v >= 0',
            text: '%v'
          }]
        }
      },
      tooltip: {
        borderRadius: 5
      },
      scaleR: {
        aperture:300,
        minValue: 0,
        maxValue: 5,
        step: 1,
        center: {
          visible: false
        },
        tick: {
          visible: false
        },
        
        labels: ['0','1', '2', '3', '4', '5'],
        ring: {
          size: 10,
          rules: [{
            rule: '%v >= 0 && %v < 1',
            backgroundColor: '#d41111'
          }, {
            rule: '%v >= 1 && %v < 2',
            backgroundColor: '#ed9407'
          }, {
            rule: '%v >= 2 && %v < 3',
            backgroundColor: '#f1d621'
          }, {
            rule: '%v >= 3 && %v < 4',
            backgroundColor: '#91bc29'
          }, {
            rule: '%v >= 4 && %v < 5',
            backgroundColor: '#208406'
          }]
        }
      },
      
      series: [{
        values: [<?php echo number_format($belief['beliefRatings'],2); ?>],
        backgroundColor: 'black',
        indicator: [5,1,1,50,0.75],
        animation: {
          effect: 2,
          method: 1,
          sequence: 4,
          speed: 900
        },
      }]
    };

    zingchart.render({
      title:'hello',
      id: 'beliefChart{{$belief['beliefId']}}',
      data: myConfig,
      height: 300,
      width: '100%'
    });


    //for belief values
    @foreach($belief['beliefValues'] as $bValues)

    var myConfig1 = {
      type: "gauge",
      
      globals: {
        fontSize: 10
      },
      plotarea: {
        marginTop: 5
      },
      plot: {
        size: '100%',
        valueBox: {
          placement: 'center',
          text: '%v',
          fontSize: 10,
          rules: [{
            rule: '%v >= 5 && %v > 4',
            text: '%v<br>{{$bValues['valueName']}}'
          }, {
            rule: '%v <= 4 && %v > 3',
            text: '%v<br>{{$bValues['valueName']}}'
          }, {
            rule: '%v <= 3 && %v > 2',
            text: '%v<br>{{$bValues['valueName']}}'
          }, {
            rule: '%v <= 2 && %v > 1',
            text: '%v<br>{{$bValues['valueName']}}'
          }, {
            rule: '%v <= 1 && %v >= 0',
            text: '%v<br>{{$bValues['valueName']}}'
          }]
        }
      },
      tooltip: {
        borderRadius: 5
      },
      scaleR: {
        aperture:300,
        values:"0:5:1",
        center: {
          visible: false
        },
        tick: {
          visible: false
        },
        
        labels: ['0','1', '2', '3', '4', '5'],
        ring: {
          size: 10,
          rules: [{
            rule: '%v >= 0 && %v < 1',
            backgroundColor: '#d41111'
          }, {
            rule: '%v >= 1 && %v < 2',
            backgroundColor: '#ed9407'
          }, {
            rule: '%v >= 2 && %v < 3',
            backgroundColor: '#f1d621'
          }, {
            rule: '%v >= 3 && %v < 4',
            backgroundColor: '#91bc29'
          }, {
            rule: '%v >= 4 && %v < 5',
            backgroundColor: '#208406'
          }]
        }
      },
      
      series: [{
        values: [<?php echo number_format($bValues['valueRatings'],2); ?>],
        backgroundColor: 'black',
        indicator: [5,1,10,20,0.75],
        
      }]
    };

    zingchart.render({
      id: 'valueChart{{$bValues['valueId']}}',
      data: myConfig1,
      height: 200,
      width: '100%'
    });

    @endforeach //for belief value

    @endforeach
  </script>

  @include('layouts.adminFooter')