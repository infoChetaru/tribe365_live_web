@section('title', 'Diagnostic Report')
@include('layouts.adminHeader')

<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                
								{{session('message')}}
							</div>
							@endif
						</div>
						<h2> Diagnostic Reports- <?php if(!empty($orgName->organisation))
						{ echo(ucfirst($orgName->organisation)); }
						?>
					</h2>	

					<canvas style="margin-top: 20px;" id="myChart" width="1000" height="400"></canvas>

				</div>
			</div>
		</div>
	</div>		
</div>
</main>


<!-- google charts -->

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>

<!-- bar graph -->

<script  type="text/javascript">
	var ctx = document.getElementById("myChart");

	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: [
			@foreach($resultArray as $value)						
			"{{$value['title']}}",
			@endforeach
			],
			datasets: [{
				label: 'Percentage',
				data: [
				@foreach($resultArray as $value1)
				{{$value1['percentage']}},
				@endforeach
				],
				backgroundColor: [
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)', 
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)'
				],
				borderColor: [
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)',
				'rgb(51, 102, 204)'
				],
				borderWidth: 5
			}]
		},
		options: {
			legend: { display: false},
			scales: {
				xAxes: [{
					ticks: {
						fontSize: 15
					},
					gridLines: {
						display: false
					}
				}],
				yAxes: [{
					ticks: {
						beginAtZero:true,
						step:10,
						max:100
					},
					gridLines: {
						display: false
					},

				}]
			},

		}
	});

</script>
@include('layouts.adminFooter')