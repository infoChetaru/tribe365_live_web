@section('title', 'List Organisation Report')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section dot cot-functional">
		<div class="container">  
			<div class="Reports-tital">
				<div align="center">                              
					<div class="loader-img" style="display: none;width: 70px;"><img src="{{asset('public/images/loader.gif')}}"></div>
				</div> 				
				<div class="prof-acount-section report-belief">
					<div class="compy-logo">
						<img src="{{asset('public/uploads/org_images\/').$org->ImageURL}}" class="mCS_img_loaded">
						<h5> {{ucfirst($org->organisation)}} </h5>
					</div>					
				</div>
				<div class="generate-pdf-btn">
					<a target="_blank" href="{{URL::to('admin/pdf\/').base64_encode($org->id)}}">
						<button type="submit" class="btn"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</button>
					</a>					
				</div>

				<div class="cot-tab-section">					
					<ul class="nav nav-tabs">		
						<li><a class="active show" data-toggle="tab" href="#cot1">DOT</a></li>
						<li class ="cot-team-role-map"><a data-toggle="tab" href="#cot2" class="">COT: Team Roles</a></li>	
						<li class="report-functinal-lens"><a data-toggle="tab" href="#cot7" class="">COT: Personality Type</a></li>	
						<li class="report-culture"><a data-toggle="tab" href="#cot4" class="">SOT: Culture Structure</a></li>	
						<li class="report-motivation"><a data-toggle="tab" href="#cot15" class="">SOT: Motivation</a></li>	
						<li class="report-diagnostic"><a data-toggle="tab" href="#cot6" class="">Diagnostics</a></li>
						<li class="report-tribometer"><a data-toggle="tab" href="#cot8" class="">Tribeometer</a></li>	
						<!-- <li><a data-toggle="tab" href="#" class="">Customised</a></li>		 -->
					</ul>					
					<div class="tab-content">

						<div id="cot15" class="tab-pane fade"></div>

						<div id="cot2" class="tab-pane fade"></div>

						<div id="cot3" class="tab-pane fade"></div>

						<div id="cot4" class="tab-pane fade"></div>		

						<div id="cot6" class="tab-pane fade"></div>			

						<div id="cot7" class="tab-pane fade"></div>
						<!-- tribeometer graph -->
						<div id="cot8" class="tab-pane fade"></div>

						<div id="cot1" class="tab-pane fade active show">
							<ul class="nav nav-tabs">
								<li><a class="active show" data-toggle="tab" href="#graph">Graph</a></li>
								<li><a class="" data-toggle="tab" href="#other">List</a></li>	
							</ul>
							<div class="tab-content">
								<div id="other" class="tab-pane fade">
									<div class="dot-belief-value">
										<table>
											<tr>
												@foreach($dotValuesArray as $belief1)
												<td> 
													<div class="dot-belief <?php 

													$beliefRatings = $belief1['beliefRatings'];

													if($beliefRatings >=0 && $beliefRatings <1)
													{
														echo ("red_bg");
													}						                    
													elseif($beliefRatings >=1 && $beliefRatings <2)
													{
														echo("orange_bg");
													}
													elseif($beliefRatings >=2 && $beliefRatings <3)
													{
														echo("yellow_bg");
													}
													elseif($beliefRatings >=3 && $beliefRatings <4)
													{
														echo("lightgreen_bg");
													}
													elseif($beliefRatings >=4 && $beliefRatings <5)
													{
														echo("green_bg");
													}
													?>">
													<h3> {{$belief1['beliefName']}} </h3>
													<strong> {{round($belief1['beliefRatings'],2)}} </strong>
												</div>
												<div class="dot-value">
													@foreach($belief1['beliefValues'] as $bValues1)
													<div class="dot-value-inner <?php 

													$beliefRatings = $bValues1['valueRatings'];

													if($beliefRatings >=0 && $beliefRatings <1)
													{
														echo ("red_bg");
													}						                    
													elseif($beliefRatings >=1 && $beliefRatings <2)
													{
														echo("orange_bg");
													}
													elseif($beliefRatings >=2 && $beliefRatings <3)
													{
														echo("yellow_bg");
													}
													elseif($beliefRatings >=3 && $beliefRatings <4)
													{
														echo("lightgreen_bg");
													}
													elseif($beliefRatings >=4 && $beliefRatings <5)
													{
														echo("green_bg");
													}
													?>">
													<h4>{{$bValues1['valueName']}}</h4>
													<strong> {{round($bValues1['valueRatings'],2)}} </strong>
												</div>
												@endforeach

											</div>
										</td>					                    
										@endforeach
									</tr>					                    
								</table>
							</div>
						</div>	
						<div id="graph" class="tab-pane fade active show">
							<div class="search-cot">
								<a href="{{URL::to('admin/report-belief-value-rating-list/'.base64_encode($org->id))}}"><img src="{{asset('public/images/menu.png')}}"></a>            
								<select id="belief" name="beliefId">
									<option value="" selected>All Belief</option> 
									@foreach($beliefList as $bValue)        
									<option {{(session('searchBeliefId')==$bValue->id)?'selected':''}} value="{{$bValue->id}}">{{ucfirst($bValue->name)}}</option>
									@endforeach
								</select>  

								<select id="office" name="officeId">
									<option value="" selected>All Office</option> 
									@foreach($offices as $ovalue)        
									<option {{($officeId==$ovalue->id)?'selected':''}} value="{{$ovalue->id}}">{{ucfirst($ovalue->office)}}</option>
									@endforeach
								</select>                
							</div>
							<div class="belif-value Belfs-valus-repeat">
								<div class="row">
									@foreach($dotValuesArray as $belief)
									<div class="col-md-4">
										<div class="form-group">
											<label> Belief: {{$belief['beliefName']}} </label>
											<div class="col-md-12">
												<div id='beliefChart{{$belief['beliefId']}}'></div>
											</div>
										</div>
										<div class="row">
											<label> Values</label>   
											@foreach($belief['beliefValues'] as $bValues)
											<div class="col-md-12">
												<div id='valueChart{{$bValues['valueId']}}'></div>
											</div>
											@endforeach
										</div>
									</div>
									@endforeach  
								</div>
							</div> 
						</div>
					</div>


				</div>					

			</div>
		</div>
	</div>
</div>
</div>
</main>
<script type="text/javascript">	
	// COT Team Role mape
	$('.cot-team-role-map').on('click',function(){
		$('.loader-img').show();
		var orgId = "{{base64_encode($org->id)}}";
		
		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getCOTteamRoleMapGraph')}}",
			data: {orgId:orgId,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				$('.loader-img').hide();
				$('#cot2').html('');
				$('#cot5').html('');
				$('#cot3').html('');	
				$('#cot2').append(response);
			}
		});		
	});

// tribeometer
$('.report-tribometer').on('click',function(){
	$('.loader-img').show();
	var orgId = "{{base64_encode($org->id)}}";

	$.ajax({
		type: "POST",
		url: "{{URL::to('admin/getReportTribeometerGraph')}}",
		data: {orgId:orgId,"_token":'<?php echo csrf_token()?>'},
		success: function(response)
		{
			$('.loader-img').hide();
			$('#cot2').html('');
			$('#cot5').html('');
			$('#cot3').html('');
			$('#cot8').html('');	
			$('#cot8').append(response);
		}
	});		
});

// Diagnostic 
$('.report-diagnostic').on('click',function(){
	$('.loader-img').show();
	var orgId = "{{base64_encode($org->id)}}";

	$.ajax({
		type: "POST",
		url: "{{URL::to('admin/getReportDiagnosticGraph')}}",
		data: {orgId:orgId,"_token":'<?php echo csrf_token()?>'},
		success: function(response)
		{
			$('.loader-img').hide();
			$('#cot2').html('');
			$('#cot5').html('');
			$('#cot3').html('');
			$('#cot6').html('');	
			$('#cot6').append(response);
		}
	});		
});

// Report-Motivation
$('.report-motivation').on('click',function(){
	$('.loader-img').show();
	var orgId = "{{base64_encode($org->id)}}";

	$.ajax({
		type: "POST",
		url: "{{URL::to('admin/getReportMotivationalGraph')}}",
		data: {orgId:orgId,"_token":'<?php echo csrf_token()?>'},
		success: function(response)
		{
			$('.loader-img').hide();
			$('#cot2').html('');
			$('#cot5').html('');
			$('#cot3').html('');
			$('#cot15').html('');	
			$('#cot15').append(response);
		}
	});		
});

// Report-culture structure
$('.report-culture').on('click',function(){
	$('.loader-img').show();
	var orgId = "{{base64_encode($org->id)}}";

	$.ajax({
		type: "POST",
		url: "{{URL::to('admin/getReportCultureGraph')}}",
		data: {orgId:orgId,"_token":'<?php echo csrf_token()?>'},
		success: function(response)
		{
			$('.loader-img').hide();
			$('#cot2').html('');
			$('#cot5').html('');
			$('#cot3').html('');
			$('#cot4').html('');	
			$('#cot4').append(response);
		}
	});		
});

// Report-functinal-lens
$('.report-functinal-lens').on('click',function(){
	$('.loader-img').show();
	var orgId = "{{base64_encode($org->id)}}";

	$.ajax({
		type: "POST",
		url: "{{URL::to('admin/getReportFunctionalGraph')}}",
		data: {orgId:orgId,"_token":'<?php echo csrf_token()?>'},
		success: function(response)
		{
			$('.loader-img').hide();
			$('#cot2').html('');
			$('#cot5').html('');
			$('#cot3').html('');
			$('#cot7').html('');	
			$('#cot7').append(response);
		}
	});		
});
</script>

<!-- google charts -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>

<script>
//get new value rating charts
@foreach($dotValuesArray as $belief)

var myConfig = {
	type: "gauge",
	globals: {
		fontSize: 10
	},
	plotarea: {
		marginTop: 10
	},
	plot: {
		size: '100%',
		valueBox: {
			placement: 'center',
          text: '%v', //default ucfirst($bValues['beliefName'])
          fontSize: 10,
          rules: [{
          	rule: '%v >= 0',
          	text: '{{ucfirst($belief['beliefName'])}}<br>%v'
          }]
      }
  },
  tooltip: {
  	borderRadius: 5
  },
  scaleR: {
  	aperture:300,
  	minValue: 0,
  	maxValue: 5,
  	step: 1,
  	center: {
  		visible: false
  	},
  	tick: {
  		visible: false
  	},

  	labels: ['0','1', '2', '3', '4', '5'],
  	ring: {
  		size: 20,

  		<?php $beliefRatings = $belief['beliefRatings']; ?>

  		<?php
  		if($beliefRatings >=0 && $beliefRatings <1)
  		{
  			?>

  			rules: [{
  				rule: '%v >= 0 && %v < 1',
  				backgroundColor: '#d41111'
  			}, {
  				rule: '%v >= 1 && %v < 2',
  				backgroundColor: '#ffe9c5'
  			}, {
  				rule: '%v >= 2 && %v < 3',
  				backgroundColor: '#fff7c3'
  			}, {
  				rule: '%v >= 3 && %v < 4',
  				backgroundColor: '#e8ffaf'
  			}, {
  				rule: '%v >= 4 && %v < 5',
  				backgroundColor: '#c4ffb5'
  			}]
  			<?php
  		}
  		elseif ($beliefRatings >=1 && $beliefRatings <2)
  		{
  			?>


  			rules: [{
  				rule: '%v >= 0 && %v < 1',
  				backgroundColor: '#ffcfcf'
  			}, {
  				rule: '%v >= 1 && %v < 2',
  				backgroundColor: '#ed9407'
  			}, {
  				rule: '%v >= 2 && %v < 3',
  				backgroundColor: '#fff7c3'
  			}, {
  				rule: '%v >= 3 && %v < 4',
  				backgroundColor: '#e8ffaf'
  			}, {
  				rule: '%v >= 4 && %v < 5',
  				backgroundColor: '#c4ffb5'
  			}]
  			<?php
  		}
  		elseif ($beliefRatings >=2 && $beliefRatings <3)
  		{
  			?>


  			rules: [{
  				rule: '%v >= 0 && %v < 1',
  				backgroundColor: '#ffcfcf'
  			}, {
  				rule: '%v >= 1 && %v < 2',
  				backgroundColor: '#ffe9c5'
  			}, {
  				rule: '%v >= 2 && %v < 3',
  				backgroundColor: '#f1d621'
  			}, {
  				rule: '%v >= 3 && %v < 4',
  				backgroundColor: '#e8ffaf'
  			}, {
  				rule: '%v >= 4 && %v < 5',
  				backgroundColor: '#c4ffb5'
  			}]
  			<?php
  		}
  		elseif ($beliefRatings >=3 && $beliefRatings <4)
  		{
  			?>


  			rules: [{
  				rule: '%v >= 0 && %v < 1',
  				backgroundColor: '#ffcfcf'
  			}, {
  				rule: '%v >= 1 && %v < 2',
  				backgroundColor: '#ffe9c5'
  			}, {
  				rule: '%v >= 2 && %v < 3',
  				backgroundColor: '#fff7c3'
  			}, {
  				rule: '%v >= 3 && %v < 4',
  				backgroundColor: '#91bc29'
  			}, {
  				rule: '%v >= 4 && %v < 5',
  				backgroundColor: '#c4ffb5'
  			}]
  			<?php
  		}
  		elseif ($beliefRatings >=4 && $beliefRatings <=5)
  		{
  			?>

  			rules: [{
  				rule: '%v >= 0 && %v < 1',
  				backgroundColor: '#ffcfcf'
  			}, {
  				rule: '%v >= 1 && %v < 2',
  				backgroundColor: '#ffe9c5'
  			}, {
  				rule: '%v >= 2 && %v < 3',
  				backgroundColor: '#fff7c3'
  			}, {
  				rule: '%v >= 3 && %v < 4',
  				backgroundColor: '#e8ffaf'
  			}, {
  				rule: '%v >= 4 && %v < 5',
  				backgroundColor: '#208406'
  			}]

  			<?php
  		}
  		?>

  	}
  },

  series: [{
  	values: [<?php echo number_format($belief['beliefRatings'],2); ?>],
  	backgroundColor: 'black',
  	indicator: [5,1,1,50,0.75],
  	animation: {
  		effect: 2,
  		method: 1,
  		sequence: 4,
  		speed: 900
  	},
  }]
};

zingchart.render({
	title:'hello',
	id: 'beliefChart{{$belief['beliefId']}}',
	data: myConfig,
	height: 300

});


//for belief values
@foreach($belief['beliefValues'] as $bValues)

var myConfig1 = {
	type: "gauge",

	globals: {
		fontSize: 10
	},
	plotarea: {
		marginTop: 5
	},
	plot: {
		size: '100%',
		valueBox: {
			placement: 'center',
			text: '%v',
			fontSize: 10,
			rules: [{
				rule: '%v >= 0',
				text: '{{$bValues['valueName']}}<br>%v'
			}]
		}
	},
	tooltip: {
		borderRadius: 5
	},
	scaleR: {
		aperture:300,
		values:"0:5:1",
		center: {
			visible: false
		},
		tick: {
			visible: false
		},

		labels: ['0','1', '2', '3', '4', '5'],
		ring: {
			size: 20,

			<?php $valueRatings = $bValues['valueRatings']; ?>

			<?php
			if($valueRatings >=0 && $valueRatings <1)
			{
				?>


				rules: [{
					rule: '%v >= 0 && %v < 1',
					backgroundColor: '#d41111'
				}, {
					rule: '%v >= 1 && %v < 2',
					backgroundColor: '#ffe9c5'
				}, {
					rule: '%v >= 2 && %v < 3',
					backgroundColor: '#fff7c3'
				}, {
					rule: '%v >= 3 && %v < 4',
					backgroundColor: '#e8ffaf'
				}, {
					rule: '%v >= 4 && %v < 5',
					backgroundColor: '#c4ffb5'
				}]
				<?php
			}
			elseif ($valueRatings >=1 && $valueRatings <2)
			{
				?>


				rules: [{
					rule: '%v >= 0 && %v < 1',
					backgroundColor: '#ffcfcf'
				}, {
					rule: '%v >= 1 && %v < 2',
					backgroundColor: '#ed9407'
				}, {
					rule: '%v >= 2 && %v < 3',
					backgroundColor: '#fff7c3'
				}, {
					rule: '%v >= 3 && %v < 4',
					backgroundColor: '#e8ffaf'
				}, {
					rule: '%v >= 4 && %v < 5',
					backgroundColor: '#c4ffb5'
				}]
				<?php
			}
			elseif ($valueRatings >=2 && $valueRatings <3)
			{
				?>


				rules: [{
					rule: '%v >= 0 && %v < 1',
					backgroundColor: '#ffcfcf'
				}, {
					rule: '%v >= 1 && %v < 2',
					backgroundColor: '#ffe9c5'
				}, {
					rule: '%v >= 2 && %v < 3',
					backgroundColor: '#f1d621'
				}, {
					rule: '%v >= 3 && %v < 4',
					backgroundColor: '#e8ffaf'
				}, {
					rule: '%v >= 4 && %v < 5',
					backgroundColor: '#c4ffb5'
				}]
				<?php
			}
			elseif ($valueRatings >=3 && $valueRatings <4)
			{
				?>


				rules: [{
					rule: '%v >= 0 && %v < 1',
					backgroundColor: '#ffcfcf'
				}, {
					rule: '%v >= 1 && %v < 2',
					backgroundColor: '#ffe9c5'
				}, {
					rule: '%v >= 2 && %v < 3',
					backgroundColor: '#fff7c3'
				}, {
					rule: '%v >= 3 && %v < 4',
					backgroundColor: '#91bc29'
				}, {
					rule: '%v >= 4 && %v < 5',
					backgroundColor: '#c4ffb5'
				}]
				<?php
			}
			elseif ($valueRatings >=4 && $valueRatings <=5)
			{
				?>

				rules: [{
					rule: '%v >= 0 && %v < 1',
					backgroundColor: '#ffcfcf'
				}, {
					rule: '%v >= 1 && %v < 2',
					backgroundColor: '#ffe9c5'
				}, {
					rule: '%v >= 2 && %v < 3',
					backgroundColor: '#fff7c3'
				}, {
					rule: '%v >= 3 && %v < 4',
					backgroundColor: '#e8ffaf'
				}, {
					rule: '%v >= 4 && %v < 5',
					backgroundColor: '#208406'
				}]

				<?php
			}
			?>

		}
	},

	series: [{
		values: [<?php echo number_format($bValues['valueRatings'],2); ?>],
		backgroundColor: 'black',
		indicator: [5,1,10,20,0.75],

	}]
};

zingchart.render({
	id: 'valueChart{{$bValues['valueId']}}',
	data: myConfig1,
	height: 300

});

@endforeach //for belief value

@endforeach
</script>
<script type="text/javascript">
	$('#belief').on('change', function() {
		$('.loader-img').show();
		var beliefId = $('#belief').val();

		$.ajax({
			type: "GET",
			url: "{{route('reports.show',base64_encode($org->id))}}",   
			data: {beliefId:beliefId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{
				$("body").html(response);
			}
		});
	});

	(function($){
		$(window).on("load",function(){

			$("#content-1").mCustomScrollbar({
				axis:"x",
				advanced:{
					autoExpandHorizontalScroll:true
				}
			});
		});
	});
</script>

<script type="text/javascript">
	$('#office').on('change', function() {
		$('.loader-img').show();
		var officeId = $('#office').val();

		$.ajax({
			type: "GET",
			url: "{{route('reports.show',base64_encode($org->id))}}",   
			data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{
				$("body").html(response);
			}
		});
	});

	(function($){
		$(window).on("load",function(){

			$("#content-1").mCustomScrollbar({
				axis:"x",
				advanced:{
					autoExpandHorizontalScroll:true
				}
			});
		});
	});
</script>

@include('layouts.adminFooter')