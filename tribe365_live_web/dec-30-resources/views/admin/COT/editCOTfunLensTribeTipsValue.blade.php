@section('title', 'List COT Personality Type tribe tips')
@include('layouts.adminHeader')
<main class="main-content cot_inner_pages">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section edit_lense_sec">
				<div class="row">
					<div class="col-md-12">						
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                                    
								{{session('message')}}
							</div>
							@endif
						</div>

						<div class="prof-acount-section report-belief">
							<div class="compy-logo">
								<h5> {{ucfirst($valueType)}} </h5>
							</div>
						</div>
						<div class="value-list parent-ul ">

							<ul style="list-style: none outside none;">
								<li style="float: left;display: block;width: 500px;height: 40px;">Values </li>
								<li style="float: left;display: block;width: 500px;height: 40px;">Action </li>
							</ul>

							<?php //print_r($tribeTipsValueId); die(); ?>
							<?php $counter=2; ?>
							@foreach($seekPersuadeList as $value)

							<form action="{{URL::to('admin/update-cot-fun-tribe-tips-value')}}" method="post">
								{{csrf_field()}}

								<input type="hidden" name="seekPersuadeValueId" value="{{base64_encode($value->id)}}">
								
								<ul class="table-class <?php if($counter % 2 == 0) echo 'grey-css'; ?>" style="list-style: none outside none;">
									<li style="float: left;display: block;width: 400px;height: 40px;">
										<input type="text" name="seekPersuadeValue" value="{{ucfirst($value->value)}}">
									</li>

									<li style="float: left;display: block;width: 400px;height: 40px;">
										<div class="editable">
											<button class="editbtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>

											<a href="{{URL::to('admin/delete-cot-fun-tribe-tips-value/'.base64_encode($value->id))}}"><button class="" type="button" onclick="return confirm('Are you sure you want to delete ?')"><i class="fa fa-trash" aria-hidden="true"></i></button></a>

											<button class="submitbtn" type="submit">submit</button>
										</div>
									</li>								
								</ul>
							</form>	
							<?php $counter++; ?>							
							@endforeach 

							<form class="" id="formNew" action="{{URL::to('admin/update-cot-fun-tribe-tips-value')}}" method="post">
								{{csrf_field()}}
								
								<input type="hidden" name="tribeTipsValueId" value="{{$tribeTipsValueId}}">
								<input type="hidden" name="valueType" value="{{$valueType}}">

								<ul class="add-value-input-row" style="list-style: none outside none;"> 

									<li style="float: left;display: block;width: 500px;height: 40px;">
										<input type="text" name="newValue[]" placeholder="Enter new value">
									</li>

									<li style="float: left;display: block;width: 300px;height: 40px;">
										<div class="editable">
											<button class="addInput" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
											<button class="submitbtn" type="submit">submit</button>
										</div>	
									</li>	
								</ul>

							</form>								
						</div>
					</div>
				
					<div class="col-md-12">
						<div class="Create-btn-cont">
							<button type="button" class="btn submitnewbtn">Add</button>
						</div>
					</div>
										
				</div>
			</div>
		</div>
	</div>
</main>

<script type="text/javascript">	
	$('.submitnewbtn').on('click',function(){
		$('#formNew').submit();
	})
</script>

<script type="text/javascript">

	$('.addInput').on('click',function(e){

		e.preventDefault();

		$('<ul style="list-style: none outside none;"><li style="float: left;display: block;width: 500px;height: 40px;"><input type="text" name="newValue[]" placeholder="Enter new value"></li><li style="float: left;display: block;width: 300px;height: 40px;"><div class="editable"><button class="removeInput" type="button"><i class="fa fa-minus" aria-hidden="true"></i></button></div></li></ul>').insertAfter('.add-value-input-row');


		$('.parent-ul').on('click','.removeInput',function(){
			console.log($(this).parent('.editable'))
			$(this).parent('.editable').parent('li').parent('ul').remove();
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {

		$('.submitbtn').hide();
		$(".table-class").find("input,select").prop("disabled", true);		

	});
</script>
<script type="text/javascript">
	$('.editbtn').on('click',function(){

		$(this).closest('.editable').find('.submitbtn').show();
		$(this).closest('.editable').find('.editbtn').hide();	
		$(this).closest('.table-class').find('input,select').prop('disabled',false);	
	})
</script>
@include('layouts.adminFooter')