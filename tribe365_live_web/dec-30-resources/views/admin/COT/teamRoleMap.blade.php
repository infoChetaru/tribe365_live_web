<!-- header -->
@section('title', 'team role mape')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="organistiondetail-section">
				<div class="row">
					<div class="col-md-12">
						@if($organisations->has_cot)
						<div class="search-cot">
							<form action="{{URL::to('admin/cot-team-role-map/'.base64_encode($organisations->id))}}" method="POST">
								{{csrf_field()}}
								<input type="search" name="searchKey" value="{{session('searchKey')}}" placeholder="Search User">

								<select id="office" name="officeId">
									<option value="" selected>All Offices</option>
									@foreach($offices as $office)
									<option {{(session('officeId')==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
									@endforeach
								</select>

								<select id="department" name="departmentId">
									@if(session('customDept'))
									<option value="">All Department</option>
									@foreach(session('customDept') as $cdept)
									<option {{(session('departmentId')==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
									@endforeach
									@else
									<option value="" selected>All Department</option>
									@endif
									
								</select>

								<select id="maper" name="maperKey">
									<option value="" selected>All Category</option>
									@foreach($mapers as $maper)
									<option {{(session('maperKey')==$maper->maper_key)?'selected':''}} value="{{$maper->maper_key}}">{{ucfirst($maper->maper)}}</option>
									@endforeach
								</select>
								@if(!empty(session('maperKey')))
								<select id="preferenceKey" name="preferenceKey" >
									@else
									<select id="preferenceKey" name="preferenceKey" style="display: none;">
										@endif
										<option value="" selected>All Preference</option>					
										<option {{(session('preferenceKey')=='primary')?'selected':''}} value="primary">Primary</option>
										<option {{(session('preferenceKey')=='secondary')?'selected':''}} value="secondary">Secondary</option>
										<option {{(session('preferenceKey')=='tertiary')?'selected':''}} value="tertiary">Tertiary</option>
									</select>

									<button type="submit">Search</button>

								</form> 
							</div>

							@endif
							<div class="prof-acount-section report-belief">
								<div class="compy-logo" style="padding-bottom: 15px; float: left;">
									<img src="{{asset('public/uploads/org_images\/').$organisations->ImageURL}}" class="mCS_img_loaded">
									<h5> {{ucfirst($organisations->organisation)}} </h5>
								</div>


							</div>

						</div>
					</div>

					@if(empty($organisations->has_cot))
					<div class="cot-tab">
						<h2> Team Role Map </h2>
						<ul class="nav nav-tabs" role="tablist">						
							<li class="" style="background-color: rgb(235, 28, 36);">
								<a href="{{URL::to('admin/cot-team-role-map-update-has-cot\/').base64_encode($organisations->id)}}">
									<img class="wite-icon mCS_img_loaded" src="{{asset('public/images/tem-icon1.png')}}">
									<span style="color: white;"> Distribute Questions </span>
								</a>
							</li>        
						</ul>						
					</div>
					@else

					<!-- tab-content -->
					<div class="cot-tab-detail-section">
						<div class="tab-pane" id="individual" class="active">
							<div class="individual-content team-content">

								<div class="individual-cate">

									<div class="cate-left">
										<div class="cat-left-tital">
											<h4> Organisation</h4>
											<span class="shaper">{{ucfirst($cotRoleMapOptions[0]->maper)}} </span>
											<span class="coordinator"> {{ucfirst($cotRoleMapOptions[1]->maper)}} </span>
										</div>
										<div class="cat-left-tital">
											<h4> Controllers</h4>
											<span class="implementer">{{ucfirst($cotRoleMapOptions[2]->maper)}} </span>
											<span class="completerFinisher">{{ucfirst($cotRoleMapOptions[3]->maper)}} </span>
										</div>
										<div class="cat-left-tital">
											<h4> Advisors</h4>
											<span class="monitorEvaluator">{{ucfirst($cotRoleMapOptions[4]->maper)}} </span>
											<span class="teamworker">{{ucfirst($cotRoleMapOptions[5]->maper)}} </span>
										</div>
										<div class="cat-left-tital">
											<h4> Explorers</h4>
											<span class="plant">{{ucfirst($cotRoleMapOptions[6]->maper)}} </span>
											<span class="resourceInvestigator"> {{ucfirst($cotRoleMapOptions[7]->maper)}} </span>
										</div>
										
									</div>

									<div class="cate-right">
										
										@foreach($usersArray as $user)

										<div class="cate-user-section">
											<div class="cate-user-tital">
												<h5 title="{{ucfirst($user->name)}}"> {{ucfirst($user->name)}}</h5>

												<span class="{{$user->new[$cotRoleMapOptions[0]->maper_key]}}">{{$user->new[$cotRoleMapOptions[0]->maper_key]}}</span>

												<span class="{{$user->new[$cotRoleMapOptions[1]->maper_key]}}">{{$user->new[$cotRoleMapOptions[1]->maper_key]}}</span>
											</div>

											<div class="cate-user-tital">
												<span class="{{$user->new[$cotRoleMapOptions[2]->maper_key]}}">{{$user->new[$cotRoleMapOptions[2]->maper_key]}}</span>	

												<span class="{{$user->new[$cotRoleMapOptions[3]->maper_key]}}">{{$user->new[$cotRoleMapOptions[3]->maper_key]}}</span>
											</div>

											<div class="cate-user-tital">	
												<span class="{{$user->new[$cotRoleMapOptions[4]->maper_key]}}">{{$user->new[$cotRoleMapOptions[4]->maper_key]}}</span>									
												<span class="{{$user->new[$cotRoleMapOptions[5]->maper_key]}}">{{$user->new[$cotRoleMapOptions[5]->maper_key]}}</span>
											</div>

											<div class="cate-user-tital">
												<span class="{{$user->new[$cotRoleMapOptions[6]->maper_key]}}">{{$user->new[$cotRoleMapOptions[6]->maper_key]}}</span>

												<span class="{{$user->new[$cotRoleMapOptions[7]->maper_key]}}">{{$user->new[$cotRoleMapOptions[7]->maper_key]}}</span>
											</div>	
										</div>

										@endforeach

									</div>

								</div>


								<div class="individual-cate key-value">
									<div class="cate-left">
										<h4> Key </h4>
									</div>
									<div class="key-box">
										<span class="btn green"> 1st Preference </span>
										<span class="btn yallow"> 2nd Preference </span>
										<span  class="btn blue"> 3rd Preference </span>
										<span class="btn orange"> 4th Preference </span>
										<span class="btn red"> 5th Preference </span>
										<!-- <span class="btn black"> Overrepresented </span>							 -->
									</div>
								</div>
								<div class="view-content-section">
									<div class="plan-view">
										<table>
											<tr class="green">
												<th>Plan</th>
												<th><span class="count" style="color:#6aa84f;"> 01 </span>Goals</th>
												<th><span class="count" style="color:#6aa84f;"> 02 </span>Ideas</th>
												<th><span class="count" style="color:#6aa84f;"> 03 </span>Plan</th>
											</tr>
											<tr>
												<td> Direction</td>
												<td> shaper</td>
												<td> Plant</td>
												<td> Monitor <br> Evaluator</td>
											</tr>
											<tr>
												<td> Connection</td>
												<td> Coordinator</td>
												<td> Resource <br> Investigator</td>
												<td> Implementer </td>
											</tr>
										</table>
									</div>
									<div class="do-view">
										<table>
											<tr class="orange">
												<th>DO</th>
												<th> <span class="count" style="color:#ff9900;"> 04 </span> Contacts</th>
												<th> <span class="count" style="color:#ff9900;"> 05 </span> Organise</th>
											</tr>
											<tr>
												<td> Direction</td>
												<td> </td>
												<td> Implementer </td>

											</tr>
											<tr>
												<td> Connection</td>
												<td> Resource Investigator <br> Team-Worker</td>
												<td> Coordinator </td>
											</tr>
										</table>
									</div>
									<div class="finish-view">
										<table>
											<tr class="red">
												<th>Finish</th>
												<th><span class="count" style="color:#ff0000;" > 06 </span> Follow Through</th>
											</tr>
											<tr>
												<td> Direction</td>
												<td> Completer </td>

											</tr>
											<tr>
												<td> Connection</td>
												<td> Implementer </td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

					@endif

				</div>
			</div>
		</div>
	</main>
	<script type="text/javascript">
		$(document).ready(function(){
			$.ajax({
				type: "GET",		
				url: "{{URL::to('admin/getCOTmaperkey')}}",
				data: {orgId:<?php echo $organisations->id; ?>},
				success: function(response) {
					console.log(response)
					var maperCountArray = [];				
					$.each(response.data, function(key, value) {						
						if(value=='underrepresented'){
							maperCountArray.push('underrepresented');					
						}
					});

					if(maperCountArray.length < 8)
					{

						$.each(response.data, function(key, value) {
							$('.'+key).addClass(value);
						});
					}
				}
			});
		});
	</script>

	<script type="text/javascript">
		$('#office').on('change', function() {
			$('#department option').remove();
			var officeId = $('#office').val();

			$.ajax({
				type: "POST",
				url: "{!!URL::to('admin/getDepartmentByOfficecId')!!}",				
				data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
				success: function(response) 
				{
					console.log(response)
					$('#department').append('<option value="" selected>All Department</option>');
					$('#department').append(response);
				}
			});
		});
	</script>
	<script type="text/javascript">
		$('#maper').on('change',function(){
			if($('#maper').val()=="")
			{
				$('#preferenceKey').hide();

			}
			else
			{
				$('#preferenceKey').show();	
			}
		})
	</script>

	@include('layouts.adminFooter')