<!-- header -->
@section('title', 'graphic table ')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="organistiondetail-section">
				<div class="row">
					<div class="col-md-12">
						<div class="prof-acount-section">
							<div class="compy-logo">
								<img src="http://production.chetaru.co.uk/tribe365/public/images/infosys-logo.png" class="mCS_img_loaded">
							</div>
							<div class="edit-box">
								<div class="profile-btn">
									<a href="#"> <img src="http://production.chetaru.co.uk/tribe365/public/images/profile-icon.png"> Profile </a>
								</div>
								<div class="account-btn">
									<a href="#"> <img src="http://production.chetaru.co.uk/tribe365/public/images/account-icon.png" class="mCS_img_loaded"> Account </a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<h2> Connecting-Team Role Map Questions </h2>
				<form id="form" action="{!!route('cot-question.store')!!}" method="post">
					{{csrf_field()}}
					<div class="row">
						
						<div class="col-md-1">
							<label><strong>Question</strong></label>
						</div>
						<div class="col-md-11">
							<div class="form-group">									
								<input  class="form-control validate" type="text" name="question" placeholder="Enter Question">
							</div>
						</div>
						<div class="col-md-1">
							<label>Options 1</label>
						</div>
						<div class="col-md-11">
							<div class="form-group">									
								<input class="form-control validate" type="text" name="option[]" placeholder="Enter Option">
							</div>
						</div>
						<div class="col-md-1">
							<label>Options 2</label>
						</div>
						<div class="col-md-11">
							<div class="form-group">									
								<input class="form-control validate" type="text" name="option[]" placeholder="Enter Option">
							</div>
						</div>
						<div class="col-md-1">
							<label>Options 3</label>
						</div>
						<div class="col-md-11">
							<div class="form-group">									
								<input class="form-control validate" type="text" name="option[]" placeholder="Enter Option">
							</div>
						</div>
						<div class="col-md-1">
							<label>Options 4</label>
						</div>
						<div class="col-md-11">
							<div class="form-group">									
								<input class="form-control validate" type="text" name="option[]" placeholder="Enter Option">
							</div>
						</div>
						<div class="col-md-1">
							<label>Options 5</label>
						</div>
						<div class="col-md-11">
							<div class="form-group">									
								<input class="form-control validate" type="text" name="option[]" placeholder="Enter Option">
							</div>
						</div>
						<div class="col-md-1">
							<label>Options 6</label>
						</div>
						<div class="col-md-11">
							<div class="form-group">									
								<input class="form-control validate" type="text" name="option[]" placeholder="Enter Option">
							</div>
						</div>
						<div class="col-md-1">
							<label>Options 7</label>
						</div>
						<div class="col-md-11">
							<div class="form-group">									
								<input class="form-control validate" type="text" name="option[]" placeholder="Enter Option">
							</div>
						</div>
						<div class="col-md-1">
							<label>Options 8</label>
						</div>
						<div class="col-md-11">
							<div class="form-group">									
								<input class="form-control validate" type="text" name="option[]" placeholder="Enter Option">
							</div>
						</div>
						<div class="Create-btn-cont">								
							<button type="button" onclick="validation();" class="btn save">Add</button>
						</div>
						<div class="error-message " style="display: none;">
							<span id="resp"></span>
						</div>	
					</div>
				</form>	
			</div>
		</div>
	</div>
</main>
<script type="text/javascript">
	
	function validation()
	{
		$('.error-message').show();
		$('.save').prop("disabled", true);

		var empty = false;
		$('.validate').each(function() {
			if ($(this).val().trim().length == 0) {
				empty = true;
			}
		});

		if (empty){
			$('.save').prop("disabled", false);
			$('#resp').html('');
			$('#resp').html('All the fields are mandatory.');
			
		}else{
			$('.save').prop("disabled", true);
			$('#form').submit();
		}

	}

</script>
@include('layouts.adminFooter')