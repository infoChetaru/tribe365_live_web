<!-- header -->
@section('title', 'COT Questions ')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="organistiondetail-section">
				<div class="row">
					<div class="col-md-12">
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                                    
								{{session('message')}}
							</div>
							@endif
						</div>
						<div class="prof-acount-section">
							<!-- <div class="compy-logo">
								<img src="http://production.chetaru.co.uk/tribe365/public/images/infosys-logo.png" class="mCS_img_loaded">
							</div> -->
							<!-- <div class="edit-box">
								<div class="profile-btn">
									<a href="#"> <img src="http://production.chetaru.co.uk/tribe365/public/images/profile-icon.png"> Profile </a>
								</div>
								<div class="account-btn">
									<a href="#"> <img src="http://production.chetaru.co.uk/tribe365/public/images/account-icon.png" class="mCS_img_loaded"> Account </a>
								</div>
							</div> -->
						</div>
					</div>
				</div>
				<h2 style="font-size: 1.6em;"> Connecting-Team Role Map Questions </h2>
				{{Form::model($cotQuestions, ['method' => 'PUT', 'route' => array('cot-question.update', base64_encode($cotQuestions[0]->cot_question_id)),'id'=>'form'])}}

				<div class="row">

					<div class="col-md-1">
						<label><strong>Question</strong></label>
					</div>
					<div class="col-md-11">
						<div class="form-group">									
							<input value="{{(!empty($cotQuestions[0]->question))?ucfirst($cotQuestions[0]->question):''}}" class="form-control validate" type="text" name="question" placeholder="Enter Question">
						</div>
					</div>
					@php $count=1; @endphp 
					@foreach($cotQuestions as $value)

					<input type="hidden" name="optionId[]" value="{{$value->id}}">

					<div class="col-md-1">
						<label>Option {{$count}}</label>
					</div>
					<div class="col-md-9">
						<div class="form-group">									
							<input value="{{$value->option_name}}" class="form-control validate" type="text" name="option[]" placeholder="Enter Option">
						</div>
					</div>
					<div class="col-md-2">
						
						<span style="color: #eb1c24">{{$value->maper}}</span>
						
					</div>
					@php $count++; @endphp 
					@endforeach

					<div class="Create-btn-cont">								
						<button type="button" onclick="validation();" class="btn">Save</button>
					</div>
					<div class="error-message " style="display: none;">
						<span id="resp"></span>
					</div>	
				</div>
			</form>	
		</div>
	</div>
</div>
</main>
<script type="text/javascript">
	
	function validation()
	{
		$('.error-message').show();

		var empty = false;
		$('.validate').each(function() {
			if ($(this).val().trim().length == 0) {
				empty = true;
			}
		});

		if (empty){
			
			$('#resp').html('');
			$('#resp').html('All the fields are mandatory.');
			
		}else{
			$('#form').submit();
		}

	}

</script>
@include('layouts.adminFooter')