<!-- header -->
@section('title', 'COT Questions ')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="organistiondetail-section">
				<div class="row">
					<div class="col-md-12">
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">               				{{session('message')}}
							</div>
							@endif
						</div>						
					</div>
				</div>
				<h2 style="font-size: 1.6em;"> Connecting- Personality Type Questions </h2>
				<div class="row">
					<div class="col-md-1">
						<label><strong>Question</strong></label>
					</div>
					<div class="col-md-11">
						<div class="form-group">									
							<input value="{{(!empty($cotFuncQuestionsOpt[0]->question))?ucfirst($cotFuncQuestionsOpt[0]->question):''}}" class="form-control validate" type="text" name="question" placeholder="Enter Question" readonly="">
						</div>
					</div>
					
					@php $count=1; @endphp 
					@foreach($cotFuncQuestionsOpt as $value)

					<input type="hidden" name="optionId[]" value="{{$value->id}}">

					<div class="col-md-1">
						<label>Option {{$count}}</label>
					</div>
					<div class="col-md-9">
						<div class="form-group">									
							<input value="{{ucfirst($value->option_name)}}" class="form-control validate" type="text" name="option[]" placeholder="Enter Option" readonly="">
						</div>
					</div>	
					<div class="col-md-2">
						<div class="form-group">									
							<input value="{{ucfirst($value->initial)}}" class="form-control validate" type="text" name="initial[]" placeholder="Enter Initial Value" readonly="">
						</div>
					</div>	

					@php $count++; @endphp 
					@endforeach
					<!-- <div class="Create-btn-cont">								
						<button type="button" onclick="validation();" class="btn">Save</button>
					</div> -->
					<div class="error-message " style="display: none;">
						<span id="resp"></span>
					</div>	
				</div>
			</form>	
		</div>
	</div>
</div>
</main>
@include('layouts.adminFooter')