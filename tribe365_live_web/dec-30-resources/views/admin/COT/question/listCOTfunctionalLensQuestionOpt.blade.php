<!-- header -->
@section('title', 'COT Questions ')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="organistiondetail-section">
				<div class="row">
					<div class="col-md-12">
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">               				{{session('message')}}
							</div>
							@endif
						</div>						
					</div>
				</div>
				<h2 style="font-size: 1.6em;"> Connecting-Personality Type Questions </h2>
				<form id="form" action="{{URL::to('admin/update-cot-fun-que-opt')}}" method="post">
					{{csrf_field()}}
					<div class="row">
						<div class="col-md-1">							
							<label><strong>Question{{$cotFuncQuestionsOpt[0]->question_id}}</strong></label>
						</div>
						<div class="col-md-11">
							<div class="form-group">		
								<input value="{{(!empty($cotFuncQuestionsOpt[0]->question_id))?$cotFuncQuestionsOpt[0]->question_id:''}}" type="hidden" name="questionId">							
								<input value="{{(!empty($cotFuncQuestionsOpt[0]->question))?ucfirst($cotFuncQuestionsOpt[0]->question):''}}" class="form-control validate" type="text" name="question" placeholder="Enter Question">
							</div>
						</div>
						@php $count=1; @endphp 
						@foreach($cotFuncQuestionsOpt as $value)

						<input type="hidden" name="optionId[]" value="{{$value->id}}">

						<div class="col-md-1">
							<label>Option {{$count}}</label>
						</div>
						<div class="col-md-9">
							<div class="form-group">									
								<input value="{{ucfirst($value->option_name)}}" class="form-control validate" type="text" name="optionName[]" placeholder="Enter Option">
							</div>
						</div>	
						<div class="col-md-2">
							<div class="form-group">									
								<input value="{{ucfirst($value->value_name)}}" class="form-control validate" type="text" name="valueName[]" placeholder="Enter Initial Value" readonly="">
							</div>
						</div>	

						@php $count++; @endphp 
						@endforeach
						<div class="Create-btn-cont">								
							<button type="button" onclick="validation();" class="btn">Save</button>
						</div>
						<div class="error-message " style="display: none;">
							<span id="resp"></span>
						</div>	
					</div>
				</form>	
			</div>
		</div>
	</div>
</main>
<script type="text/javascript">	
	function validation()
	{
		$('.error-message').show();

		var empty = false;
		$('.validate').each(function() {
			if ($(this).val().trim().length == 0) {
				empty = true;
			}
		});

		if (empty){
			
			$('#resp').html('');
			$('#resp').html('All the fields are mandatory.');
			
		}else{
			console.log('hoi')
			$('#form').submit();
		}

	}

</script>
@include('layouts.adminFooter')