@section('title', 'List COT Personality Type')
@include('layouts.adminHeader')
<main class="main-content cot_inner_pages">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">						
						<div id='success-msg' align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                                    
								{{session('message')}}
							</div>
							@endif
						</div>

						<div class="loader-img" style="display: none;width: 70px; padding-top: 39px"><img src="{{asset('public/images/loader.gif')}}"></div>
						
						<div class="prof-acount-section report-belief">
							<div class="compy-logo">
								<img src="{{asset('public/uploads/org_images\/').$organisations->ImageURL}}" class="mCS_img_loaded">
								<h5> {{ucfirst($organisations->organisation)}} </h5>
							</div>
							
							<div class="search-cot">								
								<select id="office">
									<option value="" >All Office</option>
									@foreach($offices as $office)
									<option {{(session('officeId')==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
									@endforeach
								</select>
								<select id="department" name="departmentId">
									@if(session('customDept'))
									<option value="">All Department</option>
									@foreach(session('customDept') as $cdept)
									<option {{(session('departmentId')==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
									@endforeach
									@else
									<option value="" selected>All Department</option>
									@endif									
								</select>							
							</div>

							<div class="Fun_lens_date"><h5>Personality Type</h5><div class="Fun_lens_date_edit"><input id="fun_start_date" type="text" name="" value="<?php if($organisations->COT_fun_lens_start_date){ echo date('d-M-Y', strtotime($organisations->COT_fun_lens_start_date)); }else{  echo date('d-M-Y'); } ?>" readonly=""></div></div>
						</div>

						<div class="value-list scrol-class lens_tbl">
							<table>
								<tr>
									<th>All Staff</th>
									<th>All Office</th>
									<th>All department</th>

									@foreach($keyNameArray as $fvalue)								
									<th>{{$fvalue['value1']}}/{{$fvalue['value2']}}</th>
									<th>Score</th>									
									@endforeach()
									
									<!-- <th>E/I</th>
									<th>Score</th>
									<th>S/N</th>
									<th>Score</th>
									<th>T/F</th>
									<th>Score</th>
									<th>R/F</th>
									<th>Score</th>	 -->								
								</tr>

								<?php $counter=2; ?>
								@foreach($userArray as $value)

								<tr class="table-class <?php if($counter % 2 == 0) echo 'grey-css'; ?>">
									<td>{{ucfirst($value->name)}}</td>
									<td>{{ucfirst($value->office)}}</td>
									<td>{{ucfirst($value->department)}}</td>
									
									@foreach($value->scoreArray as  $sValue)

									<td>{{$sValue['key']}}</td>
									<td>{{$sValue['score']}}</td>		

									@endforeach
								</tr>
								<?php $counter++; ?>
								@endforeach
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="organ-page-nav">
			{{$userTbl->links('layouts.pagination')}}
		</div>
	</div>
	<div class="error-message" style="display: none;">
		<span id="resp"></span>
	</div>
</main>
<script type="text/javascript">
	$(document).ready(function() {
		$('.submitbtn').hide();
		$(".table-class").find("input,select").prop("disabled", true);	
	});
</script>
<script type="text/javascript">
	$('.editbtn').on('click',function(){
		$(this).closest('.editable').find('.submitbtn').show();
		$(this).closest('.editable').find('.editbtn').hide();	
		$(this).closest('.table-class').find('input,select').prop('disabled',false);	
	})
</script>
<script type="text/javascript">
	$(".numberControl").on("keypress keyup blur",function (event) {    
		$(this).val($(this).val().replace(/[^\d].+/, ""));
		if ((event.which < 48 || event.which > 57)) {
			event.preventDefault();
		}
	});
</script>

<script type="text/javascript">
	$('#office').on('change', function() {
		$('.loader-img').show();
		$('#department option').remove();

		var officeId = $('#office').val();

		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getDepartmentByOfficecId')!!}",				
			data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{	
				$('#department').append('<option value="">Select Department</option>');
				$('#department').append(response);
			}
		});

		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/cot-functional-lens/'.base64_encode($organisations->id))!!}",				
			data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{	
				$('body').html(response);
			}
		});
	});
</script>
<script type="text/javascript">

	$('#department').on('change', function() {
		$('.loader-img').show();
		var departmentId = $('#department').val();
		var officeId     = $('#office').val();

		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/cot-functional-lens/'.base64_encode($organisations->id))!!}",				
			data: {departmentId:departmentId,officeId:officeId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{	
				$('body').html(response);
			}
		});
	});
</script>
<script type="text/javascript">
	$(function(){

		$("#fun_start_date").datepicker({
			showAnim: "fold",
			dateFormat: "dd-M-yy",
			showOn: "button",
			buttonImage: "{{asset('public/images/edit_date.png')}}",
			buttonImageOnly: true,
			buttonText: "Select date",
			onSelect: function(dateText) {

				var orgId = '<?php echo($organisations->id); ?>';
				$.ajax({
					type: "POST",
					url: "{!!URL::to('admin/updateCOTfuncLensStartingDate')!!}",				
					data: {cotDate:dateText,orgId:orgId,"_token":'<?php echo csrf_token()?>'},
					success: function(response) 
					{							
						$('div').remove('.alert');						
						$('#success-msg').append('<div class="alert alert-success" role="alert">Date updated successfully.</div>');
					}

				});				
			}
		});

	});
</script>
@include('layouts.adminFooter')