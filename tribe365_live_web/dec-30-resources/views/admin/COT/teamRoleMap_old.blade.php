<!-- header -->
@section('title', 'team role mape')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="organistiondetail-section">
				<div class="row">
					<div class="col-md-12">
						<div class="">
							<form action="{{URL::to('admin/cot-team-role-map/'.base64_encode($organisations->id))}}" method="POST">
								{{csrf_field()}}
								<input type="search" name="searchKey" placeholder="Search users">

								<select id="office" name="officeId">
									<option disabled="" selected="">Select office</option>
									@foreach($offices as $office)
									<option value="{{$office->id}}">{{$office->office}}</option>
									@endforeach
								</select>

								<select id="department" name="departmentId">
									<option disabled="" selected="">Select Department</option>
								</select>

								<select id="maper" name="maperKey">
									<option disabled="" selected="">Select Maper</option>
									@foreach($mapers as $maper)
									<option value="{{$maper->maper}}">{{ucfirst($maper->maper)}}</option>
									@endforeach
								</select>

								<select id="maper" name="preferenceKey">
									<option disabled="" selected="">Select Preference</option>					
									<option value="primary">Primary</option>
									<option value="secondary">Secondary</option>
									<option value="tertiary">Tertiary</option>
								</select>

								<button type="submit">search</button>

							</form> 
						</div>
						<div class="prof-acount-section">
							<div class="compy-logo" style="padding-bottom: 15px;">
								<img src="{{asset('public/uploads/org_images\/').$organisations->ImageURL}}" class="mCS_img_loaded">
							</div>

						</div>

					</div>
				</div>

				@if(empty($organisations->has_cot))
				<div class="cot-tab">
					<h2> Team Role Map </h2>

					<ul class="nav nav-tabs" role="tablist">
						<!-- <li class="active">
							<a href="#individual" class="active"  role="tab" data-toggle="tab">
								<img class="wite-icon mCS_img_loaded" src="http://production.chetaru.co.uk/tribe365/public/images/individual-icon.png">
								<span> Individual</span>								
							</a>
						</li> -->

						<li class="" style="background-color: rgb(235, 28, 36);">
							<a href="{{URL::to('admin/cot-team-role-map-update-has-cot\/').base64_encode($organisations->id)}}">
								<img class="wite-icon mCS_img_loaded" src="{{asset('public/images/tem-icon1.png')}}">
								<span style="color: white;"> Distribute Questions </span>
							</a>
						</li>        
					</ul>
					
				</div>
				@else
				
				<!-- tab-content -->
				<div class="cot-tab-detail-section">
					<div class="tab-pane" id="individual" class="active">
						<div class="individual-content team-content">

							<div class="individual-cate">
								<div class="cate-left">
									<div class="cat-left-tital">
										<h4> Organisation</h4>
										<span class="shaper">  Shaper </span>
										<span class="coordinator"> Coordinator </span>
									</div>
									<div class="cat-left-tital">
										<h4> Controllers</h4>
										<span class="implementer">  Implementer </span>
										<span class="completerFinisher"> Completer </span>
									</div>
									<div class="cat-left-tital">
										<h4> Advisors</h4>
										<span class="monitorEvaluator">  Monitor Evaluator </span>
										<span class="teamworker"> Team worker </span>
									</div>
									<div class="cat-left-tital">
										<h4> Explorers</h4>
										<span class="plant">  Plant </span>
										<span class="resourceInvestigator"> Resource Investigator </span>
									</div>
								</div>

								<div class="cate-right">


									<?php //print_r($usersArray); ?>
									
									@foreach($usersArray as $user)
									
									<div class="cate-user-section">

										<div class="cate-user-tital">
											<h5 title="{{ucfirst($user->name)}}"> {{ucfirst($user->name)}}</h5>
											
											@if($user->new['shaper']==4 || $user->new['shaper']==5)
											
											<span class="orange">R</span>
											@else
											<span class="{{$user->new['shaper']}}">{{$user->new['shaper']}}</span>
											@endif

											@if($user->new['coordinator']==4 || $user->new['coordinator']==5)
											<span class="orange">R</span>
											@else
											<span class="{{$user->new['coordinator']}}">{{$user->new['coordinator']}}</span>
											@endif
										</div>

										<div class="cate-user-tital">
											@if($user->new['implementer']==4 || $user->new['implementer']==5)
											<span class="orange">R</span>
											@else
											<span class="{{$user->new['implementer']}}">{{$user->new['implementer']}}</span>
											@endif

											@if($user->new['completerFinisher']==4 || $user->new['completerFinisher']==5)
											<span class="orange">R</span>
											@else
											<span class="{{$user->new['completerFinisher']}}">{{$user->new['completerFinisher']}}</span>
											@endif
										</div>

										<div class="cate-user-tital">
											@if($user->new['monitorEvaluator']==4 || $user->new['monitorEvaluator']==5)
											<span class="orange">R</span>
											@else
											<span class="{{$user->new['monitorEvaluator']}}">{{$user->new['monitorEvaluator']}}</span>
											@endif

											@if($user->new['teamworker']==4 || $user->new['teamworker']==5)
											<span class="orange">R</span>
											@else
											<span class="{{$user->new['teamworker']}}">{{$user->new['teamworker']}}</span>
											@endif
										</div>

										<div class="cate-user-tital">
											@if($user->new['plant']==4 || $user->new['plant']==5)
											<span class="orange">R</span>
											@else
											<span class="{{$user->new['plant']}}">{{$user->new['plant']}}</span>
											@endif

											@if($user->new['resourceInvestigator']==4 || $user->new['resourceInvestigator']==5)
											<span class="orange">R</span>
											@else
											<span class="{{$user->new['resourceInvestigator']}}">{{$user->new['resourceInvestigator']}}</span>
											@endif
										</div>											

									</div>

									@endforeach

								</div>

							</div>


							<div class="individual-cate key-value">
								<div class="cate-left">
									<h4> Key </h4>
								</div>
								<div class="key-box">
									<span class="btn green"> 1st Preference </span>
									<span class="btn yallow"> 2nd Preference </span>
									<span  class="btn blue"> 3rd Preference </span>
									<span class="btn orange"> R = Reserve Role </span>
									<span class="btn red"> Underrepresented </span>
									<span class="btn black"> Overrepresented </span>							
								</div>
							</div>
							<div class="view-content-section">
								<div class="plan-view">
									<table>
										<tr class="green">
											<th>Plan</th>
											<th><span class="count" style="color:#6aa84f;"> 01 </span>Goals</th>
											<th><span class="count" style="color:#6aa84f;"> 02 </span>Ideas</th>
											<th><span class="count" style="color:#6aa84f;"> 03 </span>Plan</th>
										</tr>
										<tr>
											<td> Direction</td>
											<td> shaper</td>
											<td> Plant</td>
											<td> Monitor <br> Evaluator</td>
										</tr>
										<tr>
											<td> Connection</td>
											<td> Coordinator</td>
											<td> Resource <br> Investigator</td>
											<td> Implementer </td>
										</tr>
									</table>
								</div>
								<div class="do-view">
									<table>
										<tr class="orange">
											<th>DO</th>
											<th> <span class="count" style="color:#ff9900;"> 04 </span> Contacts</th>
											<th> <span class="count" style="color:#ff9900;"> 05 </span> Organise</th>
										</tr>
										<tr>
											<td> Direction</td>
											<td> </td>
											<td> Implementer </td>

										</tr>
										<tr>
											<td> Connection</td>
											<td> Resource Investigator <br> Team-Worker</td>
											<td> Coordinator </td>
										</tr>
									</table>
								</div>
								<div class="finish-view">
									<table>
										<tr class="red">
											<th>Finish</th>
											<th><span class="count" style="color:#ff0000;" > 06 </span> Follow Through</th>
										</tr>
										<tr>
											<td> Direction</td>
											<td> Completer </td>

										</tr>
										<tr>
											<td> Connection</td>
											<td> Implementer </td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

				@endif

			</div>
		</div>
	</div>
</main>
<script type="text/javascript">
	$(document).ready(function(){
		$.ajax({
			type: "GET",		
			url: "{{URL::to('admin/getCOTmaperkey')}}",
			data: {orgId:<?php echo $organisations->id; ?>},
			success: function(response) {

				var maperCountArray = [];				
				$.each(response.data, function(key, value) {						
					if(value=='underrepresented'){
						maperCountArray.push('underrepresented');					
					}
				});

				if(maperCountArray.length < 8)
				{
					$.each(response.data, function(key, value) {
						$('.'+key).addClass(value);
					});
				}
			}
		});
	});
</script>

<script type="text/javascript">
	$('#office').on('change', function() {
		$('#department option').remove();
		var officeId = $('#office').val();
		
		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getDepartmentByOfficecId')!!}",				
			data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{
				console.log(response)
				$('#department').append(response);
			}
		});
	});
</script>
@include('layouts.adminFooter')

