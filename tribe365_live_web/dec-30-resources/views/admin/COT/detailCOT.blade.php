<!-- header -->
@section('title', 'graphic table ')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="organistiondetail-section">
				<div class="row">
					<div class="col-md-12">
						<div class="prof-acount-section">
							<div class="compy-logo">
								<img src="{{asset('public/uploads/org_images\/').$organisations->ImageURL}}" class="mCS_img_loaded">
							</div>
							<!-- <div class="edit-box">
								<div class="profile-btn">
									<a href="{{route('admin.profile')}}"> <img src="{{asset('public/images/profile-icon.png')}}"> Profile </a>
								</div>
								<div class="account-btn">
									<a href="{{URL::to('admin/admin-organisation/'. base64_encode($organisations->id) .'/edit')}}"><img src="{{asset('public/images/account-icon.png')}}" class="mCS_img_loaded"> Account </a>
								</div>
							</div> -->
						</div>
					</div>
				</div>
				<div class="team-role-map">
					<h1> Connecting</h1>
					<div class="row">
						<div class="col-md-6">
							<a href="{{URL::to('admin/cot-team-role-map/'.base64_encode($organisations->id))}}">
								<div class="team-map-box">
									<img src="{{asset('public/images/temp-map-icon.png')}}">
									<h4> Team Role Map </h4>
								</div>
							</a>
						</div>
						<div class="col-md-6">
							<!-- <a href="#"> -->
								<a href="{{URL::to('admin/cot-functional-lens/'.base64_encode($organisations->id))}}">
									<div class="team-map-box">
										<img src="{{asset('public/images/funtion-icon.png')}}">
										<h4> Personality Type  </h4>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
	@include('layouts.adminFooter')