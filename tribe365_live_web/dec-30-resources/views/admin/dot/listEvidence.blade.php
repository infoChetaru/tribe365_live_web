@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="dot-section evidence-list">
				<h2>Evidence</h2>
				<h4>  {{(!empty($type))?ucfirst($type):''}} - <?php print_r($sectionName->name);?></h4>
				<div class="row">

					@if(!empty(count($evidences)))
					@foreach($evidences as $value)
					<div class="col-md-6">
						<div class="evidence-list-box">
							@if(!empty($value->fileURL))
							<div class="evidence-img">
								<a href="{{asset('public/uploads/evidence_images').'/'.$value->fileURL}}" target="_blank">	<img src="{{asset('public/images/preview-icon.png')}}"></a>
							</div>
							@else
							<div class="evidence-img">
								<a href="#" onclick="alert('Evidence file not available.')"><img src="{{asset('public/images/preview-icon.png')}}"></a>
							</div>
							@endif
							<div class="evidence-content">
								<p class="dat-time">
									<span> {{date('d-m-Y', strtotime($value->created_at))}} </span>
									<span> | {{date('H:i:s', strtotime($value->created_at))}} </span>									
								</p>
								<p>{{ucfirst($value->description)}}</p>
							</div>
						</div>
					</div>
					@endforeach

					@else

					<div class="col-md-6"><font color="red">No Record Found.</font></div>

					@endif

				</div>
				<a href="{{URL::to('admin/dot-edit/'. $dotId.'/edit') }}"><button class="savebtn" type="button"> Back </button></a>				
				<div class="organ-page-nav">
					{!! $evidences->links('layouts.pagination') !!}
				</div>
			</div>
		</div>
	</div>
</main>
@include('layouts.adminFooter')