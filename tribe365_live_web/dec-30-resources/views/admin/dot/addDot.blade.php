<!-- header -->
@section('title', 'Add DOT')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section ">
		<div class="container">
			<div class="dot-section">
				<h2> Directing </h2>
				<div class="form-section">

					<form id="dot_form" action="{!!route('admin-dot.store')!!}" method="post">
						{{csrf_field()}}

						<input type="hidden" name="orgId" value="{{ !empty($orgId)?$orgId:''}}">

						<input type="hidden" class="belief_count" name="belief_count" value="1">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label> DOT Start Date </label>
									<input id="dot_date" type="text" name="dot_date" placeholder="Enter date of D.O.T" readonly="">
								</div>

								<div class="form-group">
									<label> Vision </label>
									<input class="checkOrgCount" id="vision" type="text" name="vision" placeholder="Enter Vision" maxlength="250">			
								</div>
								<div class="form-group">
									<label> Mission </label>
									<input class="checkOrgCount" id="mission" type="text" name="mission" placeholder="Enter Mission"  maxlength="250">			
								</div>
								<div class="form-group">
									<label> Focus </label>
									<input class="checkOrgCount" id="focus" type="text" name="focus" placeholder="Enter Focus"  maxlength="250">
								</div>
							</div>
						</div>

						<div class="Belfs-valus">
							<!-- add belief here -->
							<!-- <div class="add-belief-here"></div> -->

							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label> Beliefs </label>
										<input type="text" name="belief_name0" placeholder="Enter Belief" class="belief_validation charCount" maxlength="100">	

									</div>
								</div>
								<div class="col-md-9">
									<label> Value </label>
									<div class="form-group">
										<div class="row addValueHere0 toggleOpen">
											<div class="col-md-12">
												<button type="button" class="down">down</button>
												<!-- <select class="belief_validation addDotValuesOpt" name ="value_name0[]" style="background:url({{asset('public/images/down-arrow.png')}})no-repeat 95% center;">										
												</select> -->
												<ul class="value-checkbox">
													@foreach($dotValuesList as $dotValuesList)

													<li><input type="checkbox" name="value_name0[]" value="{{$dotValuesList->id}}"><label>{{$dotValuesList->name}}</label></li>
													@endforeach
												</ul>		
											</div>
											
											<!-- <button id="add_value0" class="add_value0 belief" type="button"> Add Value </button> -->
										</div>					
									</div>
								</div>

							</div>

							<div class="">
								<div class="repeat-row">
									<div class="add-belief-here"></div>	
								</div>
								<button type="button" class="belief addBelief">Add More Belief</button>
							</div>

							<div class="Create-btn-cont">
								<button type="button" onclick="validation();" class="btn save">Add DOT</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>	
	<div class="error-message" style="display: none;">
		<span id="resp"></span>
	</div>	
</main>

<script type="text/javascript">

	$(function() {
		$("#dot_date").datepicker({
			showAnim: "fold",
			dateFormat: "dd-mm-yy"
		});
	});
</script>

<script type="text/javascript">

	function validation(){
		$('.error-message').show();

		var ddot    = $('#dot_date').val();
		var vision  = $('#vision').val();
		var mission = $('#mission').val();
		var focus   = $('#focus').val();



		
		var empty = false;
		$('.charCount').each(function() {
			if ($(this).val().length > 50){
				empty = true;
			}
		});


		var checkOrgCount = false;
		$('.checkOrgCount').each(function() {
			if ($(this).val().length > 150){
				checkOrgCount = true;
			}
		});

		if (checkOrgCount){

			$('#resp').html('');
			$('#resp').html('All input fields should be less than 150 characters.');
		}else if (empty){

			$('#resp').html('');
			$('#resp').html('All input fields should be less than 50 characters.');
		}else if($.trim(ddot) == ''){

			$('#resp').html('');
			$('#resp').html('Please select Date.');
		}else if($.trim(vision) ==""){

			$('#resp').html('');
			$('#resp').html('Please enter Vision.');
		}else if($.trim(mission) ==""){

			$('#resp').html('');
			$('#resp').html('Please enter Mission.');
		}else if($.trim(focus) ==""){

			$('#resp').html('');
			$('#resp').html('Please enter Focus.');

		}else{

			$('.save').prop("disabled", true);

			var flag = 0;
			$(".belief_validation").each(function(index){

				if($.trim($(this).val()) == ''){
					$('.save').prop("disabled", false);
					$('#resp').html('Please enter Belief Detail.');

					flag++;
				} 
			});
			if(flag == 0)
			{
				$('.save').prop("disabled", true);
				$('#dot_form').submit();
			}
		}
	}
</script>

<script type="text/javascript">

	var next = 1;

// add belief on click				
$('.Belfs-valus').on('click','.addBelief',function(e){			
	e.preventDefault();

	$('.add-belief-here').append('<div class="row"><div class="col-md-3"><div class="form-group"><label> Beliefs </label><input class="belief_validation charCount" type="text" name="belief_name'+next+'" maxlength="100" placeholder="Enter Belief "></div></div><div class="col-md-9"><label> Value </label><div class="form-group"><div class="row addValueHere'+next+' toggleOpen"></div>');

	getCutomDotValues(next);


// // add values in belief				
// $('.Belfs-valus').on('click','.add_value'+next,function(e){
// 	e.preventDefault();

	//var fieldNum = this.id.charAt(this.id.length-1);
	// var addValueHere  = ".addValueHere" + fieldNum;

	// $(addValueHere).append('<div class="col-md-4">'+getCutomDotValues(next)+'</div>');
	
// });


//remove remove beliefs (add dots)
$('.Belfs-valus').on('click','.removeBelief',function(){

	$(this).parent('div').remove();
});

//remove remove beliefs (add dots)
$('.Belfs-valus').on('click','.removeValue',function(){

	$(this).parent('div').remove();
});

next++;

$(".belief_count").val(next);

});	

// $('.Belfs-valus').on('click','.add_value0',function(e){
// 	e.preventDefault();

// 	var fieldNum = this.id.charAt(this.id.length-1);
// 	var addValueHere  = ".addValueHere0";

// 	$(addValueHere).append('<div class="col-md-4"><select name ="value_name0[]" style="background:url({{asset("public/images/down-arrow.png")}})no-repeat 95% center;" class="">'+getCutomDotValues()+'</select><button class="removeValue0" type="button">X</button></div>');
// });
// //remove remove beliefs (add dots)
// $('.Belfs-valus').on('click','.removeValue0',function(){

// 	$(this).parent('div').remove();
// });
</script>

<script type="text/javascript">
	$(document).ready(function(){

		$('.addDotValuesOpt option').remove();
		$('.addDotValuesOpt').append(getCutomDotValues());

	});

	function getCutomDotValues(next){
		
		var dot_values_arr = new Array();
		form_values = {name: "_token", value: "<?php echo csrf_token()?>",variable:next};

		$.ajax({
			type: "GET",
			url: "{{url('admin/get-custom-dot-list')}}",
			data: form_values,
			dataType: 'json',
			success: function(response) {	

				toString = response.toString();

				resCheckbox = toString.replace(/,/g , '');
				
				var addValueHere  = ".addValueHere" + next;

				$(addValueHere).append('<div class="col-md-12">	<button type="button" class="down">down</button><ul class="value-checkbox">'+resCheckbox+'</ul></div>');
				//return dot_values_arr;
			}
		});


	}
</script>

@include('layouts.adminFooter')