<!-- header -->
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="dot-section">
				<h2> D.O.T </h2>
				<div class="form-section">

					{!! Form::model($dots, ['method' => 'PUT','id'=>'dot_form', 'route' => array('admin-dot.update', $dots->id)]) !!}

					{{csrf_field()}}

					<input type="hidden" class="belief_count" name="belief_count" value="1">

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label> Date of D.O.T </label>
								<input id="dot_date" type="text" value="{{!empty($dots->dot_date)?date_format(date_create($dots->dot_date),'d-m-Y'):''}}" name="dot_date" placeholder="Enter date of D.O.T" readonly="">
							</div>
							<div class="form-group">
								<label> Vision </label>
								<input id="vision" value="{{!empty($dots->vision)?$dots-> vision:''}}" type="text" name="vision" placeholder="Enter Vision">
							</div>
							<div class="form-group">
								<label> Mission </label>
								<input id="mission" value="{{!empty($dots->mission)?$dots->mission:''}}" type="text" name="mission" placeholder="Enter Mission">	
							</div>
							<div class="form-group">
								<label> Focus </label>
								<input id="focus" value="{{!empty($dots->focus)?$dots->focus:''}}" type="text" name="focus" placeholder="Enter Focus">
							</div>
						</div>
					</div>

					<div class="Belfs-valus">
						<!-- add belief here -->
						@php $counter=1 @endphp

						@foreach($beliefs as $belief)


						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<label> Beliefs </label>
									<input type="text" name="belief_name{{$counter}}[]" value="{{ !empty($belief['name'])?$belief['name']:''}}" placeholder="belief {{$counter}}" class="belief_validation">	
								</div>
							</div>
							<div class="col-md-9">
								<label> Value </label>
								<div class="form-group">
									<div class="row addValueHere0">
										
										@foreach($belief['belief_value'] as $bValue)

										<div class="col-md-4">
											<select class="belief_validation" name ="value_name{{$counter}}[]" style="background:url({{asset('public/images/down-arrow.png')}}no-repeat 95% center;">
												@foreach($dotValues as $value)

												<option {{ ($bValue->name==$value->name)?'selected':''}}  value="{{$value->id}}">{{$value->name}} </option>
												
												@endforeach()
											</select>	
										</div>

										@endforeach()
										<button id="add_value0" class="add_value0 belief" type="button"> Add Value </button>
									</div>					
								</div>
							</div>
						</div>
						
						@php $counter++ @endphp

						@endforeach()


						<div class="">
							<div class="repeat-row">
								<div class="add-belief-here"></div>	
							</div>
							<button type="button" class="belief addBelief">Add Belief</button>
						</div>

						<!-- <div class="Create-btn-cont">
							<button type="button" onclick="validation();" class="btn">Add DOT</button>
						</div> -->

						<div class="error-message">
							<span id="resp"></span>
						</div>	


					</div>

					{!! Form::close() !!}

				</div>
			</div>
		</div>
	</div>
</main>

<script type="text/javascript">

	$(function() {
		$("#dot_date").datepicker({
			showAnim: "fold",
			dateFormat: "dd-mm-yy"
		});
	});
</script>

<script type="text/javascript">

	function validation(){

		var ddot    = $('#dot_date').val();
		var vision  = $('#vision').val();
		var mission = $('#mission').val();
		var focus   = $('#focus').val();

		if(ddot ==""){

			$('#resp').html('');
			$('#resp').html('Please select Date.');

		} else if(vision ==""){

			$('#resp').html('');
			$('#resp').html('Please enter Vision.');

		} else if(mission ==""){

			$('#resp').html('');
			$('#resp').html('Please enter Mission.');

		} else if(focus ==""){

			$('#resp').html('');
			$('#resp').html('Please enter Focus.');

		} else {

			var flag = 0;
			$(".belief_validation").each(function(index) {

				if($(this).val()==''){

					$('#resp').html('');
					$('#resp').html('Please enter Belief Detail.');

					flag++;
				} 

			});
			if(flag == 0)
			{
				$('#dot_form').submit();
			}
		}
	}

</script>

<script type="text/javascript">

	var x = {{count($beliefs)}};
	var next = x + 1;
	console.log(next);
// add belief on click				
$('.Belfs-valus').on('click','.addBelief',function(e){			
	e.preventDefault ();

	$('.add-belief-here').append('<div class="row"><div class="col-md-3"><div class="form-group"><label> Beliefs </label><input class="belief_validation" type="text" name="belief_name'+next+'" placeholder=" belief '+next+'"></div></div><div class="col-md-9"><label> Value </label><div class="form-group"><div class="row addValueHere'+next+'"><div class="col-md-4"><select class="belief_validation addDotValuesOpt" name="value_name'+next+'[]" style="background:url({{asset("public/images/down-arrow.png")}}no-repeat 95% center;"></select></div><button id="add_value'+next+'" class="add_value'+next+' belief" type="button"> Add Value </button></div></div></div><button class="removeBelief" type="button">X</button></div>');


// // add values in belief				
$('.Belfs-valus').on('click','.add_value'+next,function(e){
	e.preventDefault();

	var fieldNum = this.id.charAt(this.id.length-1);
	var addValueHere  = ".addValueHere" + fieldNum;

	$(addValueHere).append('<div class="col-md-4"><select class="belief_validation addDotValuesOpt" name ="value_name'+fieldNum+'[]" style="background:url({{asset("public/images/down-arrow.png")}}no-repeat 95% center;"></select><button class="removeValue" type="button">X</button></div>');
});


//remove remove beliefs (add dots)
$('.Belfs-valus').on('click','.removeBelief',function(){

	$(this).parent('div').remove();
});

//remove remove beliefs (add dots)
$('.Belfs-valus').on('click','.removeValue',function(){

	$(this).parent('div').remove();
});

next++;

$(".belief_count").val(next);
console.log(next);
});	

</script>

@include('layouts.adminFooter')