<main class="main-content">
	<div class="modal fade" id="myModal1" role="dialog">
		<div class="modal-dialog add-operater evidence-modal">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="ragistration-detele add-fild-section">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<label><strong>{{(!empty($belief->name))?ucfirst($belief->name):''}}</strong></label>
									<input id="searchKey" class="form-control" type="search" name="searchItem" placeholder="search..">
									<div class="form-group">
										<div class="row toggleOpen">
											<div class="col-md-12">
												<ul class="value-checkbox add-search-values-data">
													@foreach($dotValuesList as $dotValuesList)
													<li><a href="{{URL::to('admin/get-evidence-list\/').base64_encode($dotId).'/'.base64_encode('value').'/'.base64_encode($dotValuesList->id)}}"><label>{{$dotValuesList->name}}</label></a></li>
													@endforeach
												</ul>		
											</div>											
										</div>					
									</div>
								</div>
							</div>
						</div>		
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<script type="text/javascript">

	$('#searchKey').on('keyup',function(){

		var search_key = $('#searchKey').val();	
		var beliefId = '<?php echo $belief->id; ?>';
		var dotId   = '<?php echo($dotId); ?>';

		$.ajax({
			type: "get",
			url: "{{URL::to('admin/search-dot-values')}}",
			data: {name: "_token", value: "<?php echo csrf_token()?>",search_key:search_key,beliefId:beliefId,dotId:dotId},
			success: function(response) {
            // console.log(response);
            $('.add-search-values-data').html(response);

            // console.log('hello '+response)
        }
    });

	});
</script>

