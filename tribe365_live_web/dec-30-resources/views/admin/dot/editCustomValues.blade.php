<!-- header -->
@section('title', 'Edit DOT Values')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="dot-section">
				<h2> Edit Value </h2>
				<div class="form-section">
					<form id="form" action="{{URL::to('admin/update-custom-value')}}" method="post">
						{{csrf_field()}}						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">									
									<input id="value_name" type="text" name="name" placeholder="Enter Value Name" value="{{$dotValue->name}}" maxlength="100">

									<input type="hidden" name="dotValueId" value="{{base64_encode($dotValue->id)}}">
								</div>
								<div class="error" style="color:red;">
									{{$errors->first()}}
									{{session('message')}}
								</div>
							</div>
							<div class="Create-btn-cont">
								<button type="submit" onclick="validation();" class="btn">Save</button>
							</div>							
						</form>
					</div>
				</div>
			</div>
		</div>
	</main>
	
	@include('layouts.adminFooter')