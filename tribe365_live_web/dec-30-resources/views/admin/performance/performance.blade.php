<!-- header -->
@section('title', 'Performance')
@include('layouts.adminHeader')

<!-- working area -->


<div class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="performance">

				<div class="loader-img" style="display: none;width: 70px; top: 10px;"><img src="{{asset('public/images/loader.gif')}}"></div>

				<form id="form-id" action="{{route('performace-detail')}}" method="POST">
					{{csrf_field()}}
					<div class="performance-option">
						<ul>
							<li> 
								<select name="orgId"> 
									<option value="">All Organisation</option>
									@foreach($organisations as $value)
									<option {{($orgId==$value->id)?'Selected':'' }} value="{{$value->id}}">{{$value->organisation}}</option>
									@endforeach()
								</select>
							</li>
							<li> 
								<!-- <select id="reportType" name="reportType" class="reportType"> 
									<option value="" selected>All</option>
									<option {{($reportType=='daily')?'Selected':'' }} value="daily">Daily</option>
									<option {{($reportType=='monthly')?'Selected':'' }} value="monthly" class="monthly" value="">Monthly</option>
								</select> -->
								<select id="reportType" name="reportType" class="reportType"> 
									<option value="" selected>All time</option>
									<option {{($reportType=='lastMonths12')?'Selected':'' }} value="lastMonths12">Last 12 months</option>
									<option {{($reportType=='lastMonths6')?'Selected':'' }} value="lastMonths6" class="monthly" value="">Last 6 months</option>
									<option {{($reportType=='lastMonth')?'Selected':'' }} value="lastMonth" class="monthly" value="">Last month</option>
									<option {{($reportType=='thisMonth')?'Selected':'' }} value="thisMonth" class="monthly" value="">This month</option>
									<option {{($reportType=='lastWeek')?'Selected':'' }} value="lastWeek" class="monthly" value="">Last week</option>
									<option {{($reportType=='thisWeek')?'Selected':'' }} value="thisWeek" class="monthly" value="">This week</option>
									<option {{($reportType=='yesterday')?'Selected':'' }} value="yesterday" class="monthly" value="">Yesterday</option>
									<option {{($reportType=='today')?'Selected':'' }} value="today" class="monthly" value="">Today</option>
								</select>
							</li>
							<!-- @if(empty($date))
							<li> 
								<input name="date" class="date date_field numberControl" type="text" autocomplete="false"/>
							</li>
							@else
							<li> 
								<input value="{{$date}}" name="date" class="date date_field numberControl" type="text" autocomplete="false"/>
							</li>
							@endif -->
							
							<li> 
								<button type="submit" class="search-btn">Search</button>
								<!-- <button onclick="validation();" type="button" class="search-btn">Search</button> -->
							</li>
							<div class="error-message" style="display: none;">
								<span id="resp"></span>
							</div> 
						</ul>
					</div>					
				</form>

				<div class="performance-list">
					<div class="row">
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['thumbCompleted']}}</h2></div>
										@if($reportType == 'thisMonth')
											<div class="text_mamm">
												<p class="mm">M/M
													@if($resultArray['bubbleRating']['MM'] > 0)
														<span class="green_up">+{{$resultArray['bubbleRating']['MM']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['bubbleRating']['MM'] < 0)
														<span class="red_down">{{$resultArray['bubbleRating']['MM']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
													@elseif($resultArray['bubbleRating']['MM'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
												<p class="ma">M.A
													@if($resultArray['bubbleRating']['MA'] > 0)
														<span class="green_up">+{{$resultArray['bubbleRating']['MA']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['bubbleRating']['MA'] < 0)
														<span class="red_down">{{$resultArray['bubbleRating']['MA']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
													@elseif($resultArray['bubbleRating']['MA'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
											</div>
										@endif
									</div>
									<div class="text_bottm">
										<strong>Kudos /person</strong>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['improvements']}}</h2></div>
										@if($reportType == 'thisMonth')
											<div class="text_mamm">
												<p class="mm">M/M
													@if($resultArray['improvementsMAMM']['MM'] > 0)
														<span class="green_up">+{{$resultArray['improvementsMAMM']['MM']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['improvementsMAMM']['MM'] < 0)
														<span class="red_down">{{$resultArray['improvementsMAMM']['MM']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
													@elseif($resultArray['improvementsMAMM']['MM'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
												<p class="ma">M.A
													@if($resultArray['improvementsMAMM']['MA'] > 0)
														<span class="green_up">+{{$resultArray['improvementsMAMM']['MA']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['improvementsMAMM']['MA'] < 0)
														<span class="red_down">{{$resultArray['improvementsMAMM']['MA']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
													@elseif($resultArray['improvementsMAMM']['MA'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
											</div>
										@endif
									</div>
									<div class="text_bottm">
										<strong>Improvements sent/person</strong>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['dotCompleted']}}%</h2></div>
										@if($reportType == 'thisMonth')
											<div class="text_mamm">
												<p class="mm">M/M
													@if($resultArray['dotRatingCompleted']['MM'] > 0)
														<span class="green_up">+{{$resultArray['dotRatingCompleted']['MM']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['dotRatingCompleted']['MM'] < 0)
														<span class="red_down">{{$resultArray['dotRatingCompleted']['MM']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
													@elseif($resultArray['dotRatingCompleted']['MM'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
												<p class="ma">M.A
													@if($resultArray['dotRatingCompleted']['MA'] > 0)
														<span class="green_up">+{{$resultArray['dotRatingCompleted']['MA']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['dotRatingCompleted']['MA'] < 0)
														<span class="red_down">{{$resultArray['dotRatingCompleted']['MA']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
													@elseif($resultArray['dotRatingCompleted']['MA'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
											</div>
										@endif
									</div>
									<div class="text_bottm">
										<strong>DOT Values Complete</strong>
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['dotUpdated']}}%</h2></div>
										
									</div>
									<div class="text_bottm">
										<strong>DOT Values Updated</strong>
									</div>
								</div>
							</div>
						</div> -->
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['tealRoleMapCompleted']}}%</h2></div>
										@if($reportType == 'thisMonth')
											<div class="text_mamm">
												<p class="mm">M/M
													@if($resultArray['teamRoleCompleted']['MM'] > 0)
														<span class="green_up">+{{$resultArray['teamRoleCompleted']['MM']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['teamRoleCompleted']['MM'] < 0)
														<span class="red_down">{{$resultArray['teamRoleCompleted']['MM']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
													@elseif($resultArray['teamRoleCompleted']['MM'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
												<p class="ma">M.A
													@if($resultArray['teamRoleCompleted']['MA'] > 0)
														<span class="green_up">+{{$resultArray['teamRoleCompleted']['MA']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['teamRoleCompleted']['MA'] < 0)
														<span class="red_down">{{$resultArray['teamRoleCompleted']['MA']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
													@elseif($resultArray['teamRoleCompleted']['MA'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
											</div>
										@endif
									</div>
									<div class="text_bottm">
										<strong>Team Roles Complete</strong>
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['tealRoleMapUpdated']}}%</h2></div>
										<div class="text_mamm">
											<p class="mm">M/M
												@if($resultArray['teamRoleUpdated']['MM'] > 0)
													<span class="green_up">+{{$resultArray['teamRoleUpdated']['MM']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
												@elseif($resultArray['teamRoleUpdated']['MM'] < 0)
													<span class="red_down">{{$resultArray['teamRoleUpdated']['MM']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
												@elseif(empty($resultArray['teamRoleUpdated']['MM']))
													<span class="gray_stable">{{$resultArray['teamRoleUpdated']['MM']}}%</span>
												@endif
											</p>
											<p class="ma">M.A
												@if($resultArray['teamRoleUpdated']['MA'] > 0)
													<span class="green_up">+{{$resultArray['teamRoleUpdated']['MA']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
												@elseif($resultArray['teamRoleUpdated']['MA'] < 0)
													<span class="red_down">{{$resultArray['teamRoleUpdated']['MA']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
												@elseif(empty($resultArray['teamRoleUpdated']['MA']))
													<span class="gray_stable">{{$resultArray['teamRoleUpdated']['MA']}}%</span>
												@endif
											</p>
										</div>
									</div>
									<div class="text_bottm">
										<strong>Team Roles Updated</strong>
									</div>
								</div>
							</div>
						</div> -->
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['personalityTypeCompleted']}}%</h2></div>
										@if($reportType == 'thisMonth')
											<div class="text_mamm">
												<p class="mm">M/M
													@if($resultArray['personalityTypeCompletedMAMM']['MM'] > 0)
														<span class="green_up">+{{$resultArray['personalityTypeCompletedMAMM']['MM']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['personalityTypeCompletedMAMM']['MM'] < 0)
														<span class="red_down">{{$resultArray['personalityTypeCompletedMAMM']['MM']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
													@elseif($resultArray['personalityTypeCompletedMAMM']['MM'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
												<p class="ma">M.A
													@if($resultArray['personalityTypeCompletedMAMM']['MA'] > 0)
														<span class="green_up">+{{$resultArray['personalityTypeCompletedMAMM']['MA']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['personalityTypeCompletedMAMM']['MA'] < 0)
														<span class="red_down">{{$resultArray['personalityTypeCompletedMAMM']['MA']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
													@elseif($resultArray['personalityTypeCompletedMAMM']['MA'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
											</div>
										@endif
									</div>
									<div class="text_bottm">
										<strong>Personality Types Complete</strong>
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['personalityTypeUpdated']}}%</h2></div>
										<div class="text_mamm">
											<p class="mm">M/M
												@if($resultArray['personalityTypeUpdatedMAMM']['MM'] > 0)
													<span class="green_up">+{{$resultArray['personalityTypeUpdatedMAMM']['MM']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
												@elseif($resultArray['personalityTypeUpdatedMAMM']['MM'] < 0)
													<span class="red_down">{{$resultArray['personalityTypeUpdatedMAMM']['MM']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
												@elseif(empty($resultArray['personalityTypeUpdatedMAMM']['MM']))
													<span class="gray_stable">{{$resultArray['personalityTypeUpdatedMAMM']['MM']}}%</span>
												@endif
											</p>
											<p class="ma">M.A
												@if($resultArray['personalityTypeUpdatedMAMM']['MA'] > 0)
													<span class="green_up">+{{$resultArray['personalityTypeUpdatedMAMM']['MA']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
												@elseif($resultArray['personalityTypeUpdatedMAMM']['MA'] < 0)
													<span class="red_down">{{$resultArray['personalityTypeUpdatedMAMM']['MA']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
												@elseif(empty($resultArray['personalityTypeUpdatedMAMM']['MA']))
													<span class="gray_stable">{{$resultArray['personalityTypeUpdatedMAMM']['MA']}}%</span>
												@endif
											</p>
										</div>
									</div>
									<div class="text_bottm">
										<strong>Personality Types Updated</strong>
									</div>
								</div>
							</div>
						</div> -->
					<!-- 	<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['cultureStructureCompleted']}}%</h2></div>
										<div class="text_mamm">
											<p class="mm">M/M
												@if($resultArray['cultureStructureCompletedMAMM']['MM'] > 0)
													<span class="green_up">+{{$resultArray['cultureStructureCompletedMAMM']['MM']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
												@elseif($resultArray['cultureStructureCompletedMAMM']['MM'] < 0)
													<span class="red_down">{{$resultArray['cultureStructureCompletedMAMM']['MM']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
												@elseif(empty($resultArray['cultureStructureCompletedMAMM']['MM']))
													<span class="gray_stable">{{$resultArray['cultureStructureCompletedMAMM']['MM']}}%</span>
												@endif
											</p>
											<p class="ma">M.A
												@if($resultArray['cultureStructureCompletedMAMM']['MA'] > 0)
													<span class="green_up">+{{$resultArray['cultureStructureCompletedMAMM']['MA']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
												@elseif($resultArray['cultureStructureCompletedMAMM']['MA'] < 0)
													<span class="red_down">{{$resultArray['cultureStructureCompletedMAMM']['MA']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
												@elseif(empty($resultArray['cultureStructureCompletedMAMM']['MA']))
													<span class="gray_stable">{{$resultArray['cultureStructureCompletedMAMM']['MA']}}%</span>
												@endif
											</p>
										</div>
									</div>
									<div class="text_bottm">
										<strong>Organisation Structure Complete</strong>
									</div>
								</div>
							</div>
						</div> -->
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['cultureStructureUpdated']}}%</h2></div>
										@if($reportType == 'thisMonth')
											<div class="text_mamm">
												<p class="mm">M/M
													@if($resultArray['teamRoleCompleted']['MM'] > 0)
														<span class="green_up">+{{$resultArray['cultureStructureUpdatedMAMM']['MM']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['cultureStructureUpdatedMAMM']['MM'] < 0)
														<span class="red_down">{{$resultArray['cultureStructureUpdatedMAMM']['MM']}}%</span><img src="{{asset('public/images/downdown.png')}}" alt="" />
													@elseif($resultArray['cultureStructureUpdatedMAMM']['MM'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
												<p class="ma">M.A
													@if($resultArray['cultureStructureUpdatedMAMM']['MA'] > 0)
														<span class="green_up">+{{$resultArray['cultureStructureUpdatedMAMM']['MA']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['cultureStructureUpdatedMAMM']['MA'] < 0)
														<span class="red_down">{{$resultArray['cultureStructureUpdatedMAMM']['MA']}}%</span><img src="{{asset('public/images/downdown.png')}}" alt="" />
													@elseif($resultArray['cultureStructureUpdatedMAMM']['MA'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
											</div>
										@endif
									</div>
									<div class="text_bottm">
										<strong>Organisation Structure Updated</strong>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['motivationCompleted']}}%</h2></div>
										@if($reportType == 'thisMonth')
											<div class="text_mamm">
												<p class="mm">M/M
													@if($resultArray['motivationCompletedMAMM']['MM'] > 0)
														<span class="green_up">+{{$resultArray['motivationCompletedMAMM']['MM']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['motivationCompletedMAMM']['MM'] < 0)
														<span class="red_down">{{$resultArray['motivationCompletedMAMM']['MM']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
													@elseif($resultArray['motivationCompletedMAMM']['MM'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
												<p class="ma">M.A
													@if($resultArray['motivationCompletedMAMM']['MA'] > 0)
														<span class="green_up">+{{$resultArray['motivationCompletedMAMM']['MA']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['motivationCompletedMAMM']['MA'] < 0)
														<span class="red_down">{{$resultArray['motivationCompletedMAMM']['MA']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
													@elseif($resultArray['motivationCompletedMAMM']['MA'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
											</div>
										@endif
									</div>
									<div class="text_bottm">
										<strong>Motivation Complete</strong>
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['motivationCountUpdated']}}%</h2></div>
										<div class="text_mamm">
											<p class="mm">M/M
												@if($resultArray['motivationUpdatedMAMM']['MM'] > 0)
													<span class="green_up">+{{$resultArray['motivationUpdatedMAMM']['MM']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
												@elseif($resultArray['motivationUpdatedMAMM']['MM'] < 0)
													<span class="red_down">{{$resultArray['motivationUpdatedMAMM']['MM']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
												@elseif(empty($resultArray['motivationUpdatedMAMM']['MM']))
													<span class="gray_stable">{{$resultArray['motivationUpdatedMAMM']['MM']}}%</span>
												@endif
											</p>
											<p class="ma">M.A
												@if($resultArray['motivationUpdatedMAMM']['MA'] > 0)
													<span class="green_up">+{{$resultArray['motivationUpdatedMAMM']['MA']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
												@elseif($resultArray['motivationUpdatedMAMM']['MA'] < 0)
													<span class="red_down">{{$resultArray['motivationUpdatedMAMM']['MA']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
												@elseif(empty($resultArray['motivationUpdatedMAMM']['MA']))
													<span class="gray_stable">{{$resultArray['motivationUpdatedMAMM']['MA']}}%</span>
												@endif
											</p>
										</div>
									</div>
									<div class="text_bottm">
										<strong>Motivation Updated</strong>
									</div>
								</div>
							</div>
						</div> -->
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['diagnosticUpdated']}}%</h2></div>
										@if($reportType == 'thisMonth')
											<div class="text_mamm">
												<p class="mm">M/M
													@if($resultArray['diagnosticUpdatedMAMM']['MM'] > 0)
														<span class="green_up">+{{$resultArray['diagnosticUpdatedMAMM']['MM']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['diagnosticUpdatedMAMM']['MM'] < 0)
														<span class="red_down">{{$resultArray['diagnosticUpdatedMAMM']['MM']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
													@elseif($resultArray['diagnosticUpdatedMAMM']['MM'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
												<p class="ma">M.A
													@if($resultArray['diagnosticUpdatedMAMM']['MA'] > 0)
														<span class="green_up">+{{$resultArray['diagnosticUpdatedMAMM']['MA']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['diagnosticUpdatedMAMM']['MA'] < 0)
														<span class="red_down">{{$resultArray['diagnosticUpdatedMAMM']['MA']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
													@elseif($resultArray['diagnosticUpdatedMAMM']['MA'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
											</div>
										@endif
									</div>
									<div class="text_bottm">
										<strong>Diagnostic Updated</strong>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['tribeometerUpdated']}}%</h2></div>
										@if($reportType == 'thisMonth')
											<div class="text_mamm">
												<p class="mm">M/M
													@if($resultArray['tribeometerUpdatedMAMM']['MM'] > 0)
														<span class="green_up">+{{$resultArray['tribeometerUpdatedMAMM']['MM']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['tribeometerUpdatedMAMM']['MM'] < 0)
														<span class="red_down">{{$resultArray['tribeometerUpdatedMAMM']['MM']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
													@elseif($resultArray['tribeometerUpdatedMAMM']['MM'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
												<p class="ma">M.A
													@if($resultArray['tribeometerUpdatedMAMM']['MA'] > 0)
														<span class="green_up">+{{$resultArray['tribeometerUpdatedMAMM']['MA']}}%</span><img src="{{asset('public/images/up.png')}}" alt="" />
													@elseif($resultArray['tribeometerUpdatedMAMM']['MA'] < 0)
														<span class="red_down">{{$resultArray['tribeometerUpdatedMAMM']['MA']}}%</span><img src="{{asset('public/images/down.png')}}" alt="" />
													@elseif($resultArray['tribeometerUpdatedMAMM']['MA'] == 0)
														<span class="gray_stable">0%</span>
													@endif
												</p>
											</div>
										@endif
									</div>
									<div class="text_bottm">
										<strong>Tribeometer Updated</strong>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
<!-- footer -->
@include('layouts.adminFooter')

<script type="text/javascript">
	// function validation()
	// {
		
	// 	$('.error-message').show();

	// 	var reportType = $('#reportType').val();
	// 	var date = $('.date').val();
	// 	console.log(reportType)
	// 	console.log(date)

	// 	if(reportType=='monthly')
	// 	{
	// 		if(!date)
	// 		{
	// 			$('.date').css('border-color', 'red');
	// 			$('#resp').html('');
	// 			$('#resp').html('Please select month.');
	// 		}
	// 		else
	// 		{
	// 			$('.loader-img').show();
	// 			$('.search-btn').prop("disabled", true);
	// 			$('#form-id').submit();
	// 		}
	// 	}
	// 	else
	// 	{
	// 		$('.loader-img').show();
	// 		$('.search-btn').prop("disabled", true);
	// 		$('#form-id').submit();
	// 	}
	// }
</script>
<script type="text/javascript">
	// var currentTime = new Date(); 
	// var minDate = new Date(currentTime.getYear(), currentTime.getMonth()-1); 
	// var maxDate =  new Date(currentTime.getFullYear(),currentTime.getMonth()-1); 
	// $(".date_field").datepicker({
	// 	changeMonth: true,
	// 	changeYear: true,
	// 	todayHighlight:true,
	// 	dateFormat: 'MM-yy',
	// 	autoclose:true,			
	// 	minDate: minDate, 
	// 	maxDate: maxDate, 
	// });
</script>

<script type="text/javascript">
	//$('.date_field').hide();

	// <?php 
	// if($reportType=='monthly')
	// {		
	// 	echo "$('.date_field').show();";
	// }
	// ?>
	// $(document).on('change','.reportType',function(){

	// 	if ($(this).val() == 'monthly')
	// 	{
	// 		$('.date_field').val('')			
	// 		$(".date_field").show();
	// 	}
	// 	else if ($(this).val() == 'daily')
	// 	{	
	// 		$('.date_field').hide();
	// 	}
	// 	else if ($(this).val() == '')
	// 	{	
	// 		$('.date_field').hide();
	// 	}

	// });
</script>

<script type="text/javascript">
	$(document).on("keypress keyup blur",".numberControl",function (event) {    
		$(this).val($(this).val().replace(/[^\d].+/, ""));
		if ((event.which < 48 || event.which > 57)){
			event.preventDefault();
		}
	});
</script>
