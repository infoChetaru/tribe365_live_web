<!-- header -->
@section('title', 'graphic table ')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="organistiondetail-section">
				<div class="row">
					<div class="col-md-12">
						<div class="prof-acount-section">
							<div class="compy-logo">
								<img src="{{asset('public/uploads/org_images\/').$organisations->ImageURL}}" class="mCS_img_loaded">
							</div>							
						</div>
					</div>
				</div>
				<div class="team-role-map">
					<h1> Supercharging </h1>
					<div class="row">
						<div class="col-md-6">
							<a href="{{route('sot.show',base64_encode($organisations->id))}}">
								<div class="team-map-box">
									<img src="{{asset('public/images/Culture Structure.png')}}">
									<h4> Culture Structure </h4>
								</div>
							</a>
						</div>
						<div class="col-md-6">
							<a href="{{URl::to('admin/sot-motivation-users/'.base64_encode($organisations->id))}}">
								<div class="team-map-box">
									<img src="{{asset('public/images/Motivation.png')}}">
									<h4> Motivation </h4>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
@include('layouts.adminFooter')