@section('title', 'List SOT Motivation values')
@include('layouts.adminHeader')
<main class="main-content cot_inner_pages">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">						
						<div id='success-msg' align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                                    
								{{session('message')}}
							</div>
							@endif
						</div>
						<div class="loader-img" style="display: none;width: 70px; padding-top: 39px"><img src="{{asset('public/images/loader.gif')}}">
						</div>
						<div class="prof-acount-section report-belief">
							<div class="compy-logo">
								<h5> Supercharging- Motivation Values </h5>
							</div>		
							<div class="search-cot">              
								<a href="{{URL::to('admin/admin-dashboard')}}" class="back-button"><button >Back</button></a>                       
							</div>

						</div>

						<div class="value-list menual_fun_lens_wrap">
							<table>
								<tr>									
									<th>Title</th>
									<th>Description</th>		
									<th>Action</th>													
								</tr>
								<?php $counter=2; ?>
								@foreach($motivationValues as $value)
								<form id="form" action="{{URL::to('admin/update-sot-motivation-value')}}" method="post">
									{{csrf_field()}}
									<input type="hidden" name="motValueId" value="{{base64_encode($value->id)}}">
									<tr class="table-class <?php if($counter % 2 == 0) echo 'grey-css'; ?>">
										<td><textarea name="motValTitle">{{$value->title}}</textarea></td>
										<td><textarea name="description">{{$value->description}}</textarea></td>	
										<td>
											<div class="editable">
												<button class="editbtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
												<button style="display: none;" class="submitbtn" type="submit">submit</button>
											</div>
										</td>								
									</tr>
								</form>
								<?php $counter++; ?>
								@endforeach				
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</main>
<script type="text/javascript">
	$(document).ready(function() {
		$('.submitbtn').hide();
		$(".table-class").find("textarea").prop("disabled", true);		
	});
</script>
<script type="text/javascript">
	$('.editbtn').on('click',function(){
		$(this).closest('.editable').find('.submitbtn').show();
		$(this).closest('.editable').find('.editbtn').hide();	
		$(this).closest('.table-class').find('textarea').prop('disabled',false);		
	})
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$("textarea").each(function(){
			var scroll_height = $(this).get(0).scrollHeight;
			$(this).css('height', scroll_height + 'px');
		});		
	});
</script>
@include('layouts.adminFooter')