@section('title', 'List SOT Detail')
@include('layouts.adminHeader')
<main class="main-content">
    <div class="add-fild-section organization-fild sot-results">
        <div class="container">
            <div class="organistiondetail-section">
                <div class="row">
                    <div class="col-md-12">

                        <div class="search-section">                                          
                            <div align="center">                            
                                @if(session('message'))
                                <div class="alert alert-success" role="alert">                                    
                                    {{session('message')}}
                                </div>
                                @endif
                            </div>   
                            <div class="prof-acount-section report-belief">
                            <div class="compy-logo" style="padding-bottom: 15px; float: left;">
                                <img src="{{asset('public/uploads/org_images\/').$organisations->ImageURL}}" class="mCS_img_loaded">
                                <h5> {{ucfirst($organisations->organisation)}} </h5>
                            </div>


                        </div>                     
                        </div>
                    </div>
                </div>
                <div class="row sot-value">
                    @foreach($sotCountArray as $value)
                    <div class="col-md-3">
                        <div class="sot-value-inner">
                            <div class="heading-text

                            <?php
                            if($value->title=='Power')
                            {
                                echo('blue-color');
                            }
                            elseif($value->title=='Role')
                            {
                                echo('yallow-color');
                            }
                            elseif($value->title=='Tribe')
                            {
                             echo('green-color');
                         }
                         ?>
                         "> 
                         <h2> {{$value->title}} </h2>
                     </div>
                     <div class="sot-value-text">
                        <strong> {{!empty($value->SOTCount)?$value->SOTCount:0}} </strong>
                    </div>
                </div>
            </div> 
            @endforeach
        </div>

        @if(empty($sotStrDetailArr[0]->SOTCount))

        <span class="empty-data">Culture Structure Questionnaire are not submitted by any members of organisation yet.</span>

        @endif

        @if(!empty($sotStrDetailArr[0]->SOTCount)) 

        <div class="row sot-content"> 
            <div class="col-md-3 image-sot">               
                <img width="100px" height="100px" src="{{asset('public/uploads/sot\/').$sotStrDetailArr[0]->imgUrl}}">        
            </div>
            <div class="col-md-9">
                <ul>
                    @foreach($sotStrDetailArr[0]->summary as $summValue)
                    <li>{{$summValue->summary}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
    </div>
</div>
</div>

</main>

@include('layouts.adminFooter')