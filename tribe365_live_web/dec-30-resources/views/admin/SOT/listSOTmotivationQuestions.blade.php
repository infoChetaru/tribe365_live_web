@section('title', 'List SOT Motivational Questions')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                
								{{session('message')}}
							</div>
							@endif
						</div>
						<h2> Supercharging- Motivation Questions </h2>						
						<div class="value-list">
							<table>
								<tr><th> Question </th></tr>								
								@foreach($sotQuestions as $value)
								<tr>									
									<td>{{ucfirst($value->question)}}
										<div class="editable">
											<a href="{!!URL::to('admin/sot-mot-que-option/'.base64_encode($value->id))!!}">
												<button type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
											</a>											
										</div>
									</td>
								</tr>
								@endforeach							
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="organ-page-nav">
			{{$sotQuestions->links('layouts.pagination')}}
		</div>
	</div>
</main>
@include('layouts.adminFooter')