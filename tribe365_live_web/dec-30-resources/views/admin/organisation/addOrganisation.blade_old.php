<!-- header -->
@section('title', 'Add Organisation')
@include('layouts.adminHeader')
<main class="main-content">
	<form id="org_form" action="{!!route('admin-organisation.store')!!}" method="post" enctype="multipart/form-data">

		{{csrf_field()}}
		<input type="hidden" name="office_count" class="office_count" id="count" value="0"/>
		<input type="hidden" name="additional_user_count" class="additional_user_count" id="additional_user_count" value="0" />
		<div class="add-fild-section">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="fild-stats-cont">
							<h2> Add Organisations </h2>
							<div class="add-content">

								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<input id="org_name" name="org_name" type="text" class="form-control"  placeholder="Name of Organisation">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input id="org_industry" name="org_industry" type="text" class="form-control" placeholder="Industry">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input id="org_turnover" name="org_turnover" type="text" class="form-control numberControl" placeholder="Turn Over">
										</div>
									</div>
								</div>

							</div>
							<div class="add-logo-section">
								<div class="add-logo-box">
									<img src="{{ asset('public/images/add-icon.png')}}">
									<h4> Add Logo </h4>
									<input id="org_logo" name="org_logo" type='file' value="" />

								</div>
								<div class="add-img-box"> <img id="blah" src="#" alt="your image" /> </div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="fild-stats-cont contact-cont">
							<h2> Contact </h2>
							<div class="add-content">

								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<input id="org_add1" name="org_add1" type="text" class="form-control"  placeholder="Address1">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input id="org_add2" name="org_add2" type="text" class="form-control" placeholder="Address2">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input id="org_phone" name="org_phone" type="phone" class="form-control numberControl" placeholder="Phone">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input id="org_email" name="email" type="email" class="form-control" placeholder="E-Mail">
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="fild-stats-cont Create-cont">
							<h2> Create Account </h2>
							<div class="add-content">

								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<input id="user_name" name="user_name" type="text" class="form-control"  placeholder="Name">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<input id="user_password" name="user_password" type="password" class="form-control"  placeholder="Password">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<input id="user_confirm_password" name="user_confirm_password" type="password" class="form-control" placeholder="Confirm Password ">
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>

				<!-- add office here -->
				<div class="row add-office-row"> </div>

				<!-- add user here -->
				<div class="row add-user-row"></div>


				<div class="add-plus-icon">
					<div class="add-plus-upper">
						<a href="#"><img src="{{ asset('public/images/plus-icon.png')}}"></a>
						<div class="add-section-btn">
							<a href="javascript:void(0);" class="add-office-btn addoffice">
								<span class="office-text"> Add Offices </span>
								<span class="office-icon"> <img src="{{asset('public/images/add-office-icon.png')}}"> </span>
							</a>
							<br>
							<a href="javascript:void(0);" class="add-user-btn addUserSection">
								<span class="user-text"> Add User </span>
								<span class="user-icon"> <img src="{{asset('public/images/add-user-icon.png')}}"> </span>
							</a>
						</div>
					</div>
				</div>

				<div class="Create-btn-cont">								
					<button type="button" onclick="validation();" class="btn">Add ORGANISATION</button>
				</div>
				<div class="error-message">
					<span id="resp"></span>
				</div>								
			</div>
		</div>
	</form>
</main>
<!-- footer -->

<script type="text/javascript">
	function validation() {

		var org_name     = $('#org_name').val();
		var org_industry = $('#org_industry').val();
		var org_turnover = $('#org_turnover').val();
		var org_add1     = $('#org_add1').val();
		var org_add2     = $('#org_add2').val();
		var org_email    = $('#org_email').val();
		var org_phone    = $('#org_phone').val();
		var user_name    = $('#user_name').val();
		var user_password= $('#user_password').val();
		var user_confirm_password= $('#user_confirm_password').val();

		var org_logo_count = document.getElementById("org_logo").files.length;

					//check email type
					is_email = isEmail(org_email);
					
					if(org_name ==""){

						$('#resp').html('');
						$('#resp').html('Please enter organisation.');
					} else if(org_industry ==""){

						$('#resp').html('');
						$('#resp').html('Please enter industry.');
					} else if(org_turnover ==""){

						$('#resp').html('');
						$('#resp').html('Please enter turnover.');
					} else if(org_logo_count== 0){

						$('#resp').html('');
						$('#resp').html('Please enter image');
					} else if(org_add1 ==""){

						$('#resp').html('');
						$('#resp').html('Please enter address1.');
					} else if(org_add2 ==""){

						$('#resp').html('');
						$('#resp').html('Please enter address2.');
					} else if (org_add1==org_add2) {

						$('#resp').html('');
						$('#resp').html('address1 and address2 same !.');
					} else if(org_phone ==""){

						$('#resp').html('');
						$('#resp').html('Please enter phone number.');
					} else if(org_email =="" || !is_email){

						$('#resp').html('');
						$('#resp').html('Please enter valid email.');
					} else if(user_name==""){

						$('#resp').html('');
						$('#resp').html('Please enter name.');
					} else if (user_password=="") {

						$('#resp').html('');
						$('#resp').html('Please enter user password.');
					} else if (user_confirm_password=="") {

						$('#resp').html('');
						$('#resp').html('Please enter user confirm password.');
					} else if (user_password !=user_confirm_password) {

						$('#resp').html('');
						$('#resp').html('Password and confirm-password not matched');
					} else {

						var flag = 0;
						$(".office_validation").each(function(index) {

							if($(this).val()==''){

								$('#resp').html('Please enter office detail');

								flag++;
							} 

						});

						if(flag == 0) {
							//check validation for aaddtional user input fields
							var user_flag = 0;
							$(".add_user_validation").each(function(index) {

								if($(this).val()=='') {

									$('#resp').html('Please enter user detail');

									user_flag++;
								} 

							});

							if (user_flag == 0) {

								$('#org_form').submit();
							}
						}
					} 
				}				
			</script>

			<script type="text/javascript">

				function isEmail(email){
					var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
					return regex.test(email);
				}

			</script>
			@include('layouts.adminFooter')