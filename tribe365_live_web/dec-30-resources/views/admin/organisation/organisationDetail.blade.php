<!-- header -->
@section('title', 'Detail Organisation')
@include('layouts.adminHeader')

<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="organistiondetail-section">
				<div class="row">
					<div class="col-md-4">
						<div class="prof-acount-section">
							<div class="compy-logo">
							
								@if(!empty($record->ImageURL))
								<img src="{{asset('public/uploads/org_images').'/'.$organisations->ImageURL}}">
								@else 
								<img src="{{ asset('public/images/no-image.jpg')}}">

								@endif

							</div>
							<div class="edit-box">
								<div class="profile-btn">
									<a href="#"> <img src="{{ asset('public/images/profile-icon.png')}}"> Profile </a>
								</div>
								<div class="account-btn">
									<a href="{{ URL::to('admin/admin-organisation/'. $organisations->id .'/edit') }}"> <img src="{{ asset('public/images/account-icon.png')}}"> Account </a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="detail-btn">
							<div class="adit-btn">
								<button type="text"> Add Staff </button>
							</div>
							<div class="adit-btn">
								<button type="text"> View Staff </button>
							</div>
							<div class="adit-btn">
								<button type="text"> View Reports </button>
							</div>

						</div>
					</div>
				</div>
				<div class="row">
					<div class="organistiondetail-box">
						<div class="col-md-4">
							<div class="detail-box">
								<div class="detail-content">
									<h2> DOT </h2>
									<span> Directing of Tribe </span>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="detail-box">
								<div class="detail-content">
									<h2> COT </h2>
									<span> Connecting of Tribe  </span>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="detail-box">
								<div class="detail-content">
									<h2> SOT </h2>
									<span> Supercharging of Tribe </span>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="detail-box">
								<div class="detail-content">
									<h2> IYT </h2>
									<span> Improve your Tribe  </span>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="detail-box">
								<div class="detail-content">
									<h2> Actions </h2>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="detail-box">
								<div class="detail-content">
									<h2> Messages </h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

@include('layouts.adminFooter')