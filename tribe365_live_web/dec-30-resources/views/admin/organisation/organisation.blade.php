<!-- header -->
@section('title', 'List Organisation')
@include('layouts.adminHeader')
<main class="main-content">
    <div class="add-fild-section organization-fild">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="loader-img" style="display: none;width: 70px; top: -57px;"><img src="{{asset('public/images/loader.gif')}}"></div>
                    <div class="search-section">
                     <form>
                        <input id="search-key" type="search" placeholder="Search...">
                    </form>                         
                    <div align="center">                            
                        @if(session('message'))
                        <div class="alert alert-success" role="alert">                                    
                            {{session('message')}}
                        </div>
                        @endif
                    </div>                        
                </div>
            </div>
        </div>
        <div class="row">
            <div class="organization-section">

                @foreach($organisation as $key => $value)

                <div class="col-md-6">
                    <div class="company-section">
                        <div class="company-logo">

                           <a href="{{URL::to('admin/organisation-detail/'.base64_encode($value->id))}}">

                            @if(!empty($value->ImageURL))
                            <img src="{{ asset('public/uploads/org_images').'/'.$value->ImageURL }}">
                            @else 
                            <img src="{{ asset('public/images/no-image.png')}}">

                            @endif
                        </a>


                    </div>
                    <div class="company-detale">

                        <a href="{{URL::to('admin/organisation-detail/'.base64_encode($value->id))}}">
                            <h2> {{ucfirst($value->organisation)}} </h2>
                        </a>                                                      

                        <div class="company-category">
                            <ul>
                                <li>
                                    <span class="category-tital">Industry Type</span>
                                    <span class="category-number"> {{ucfirst($value->industry)}} </span>
                                </li>
                                <li>
                                    <span class="category-tital">Offices</span>
                                    <span class="category-number">{{$value->numberOfOffices}}</span>
                                </li>
                                <li>
                                    <span class="category-tital">Departments</span>
                                    <span class="category-number">{{$value->numberOfDepartments}} </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>    

            @endforeach 
            
        </div>
    </div>


    <div class="add-plus-icon">
        <div class="add-plus-upper">
            <a href="{!!route('admin-organisation.create')!!}"><img src="{{ asset('public/images/plus-icon.png')}}"></a>

        </div>
    </div>
</div>

<div class="organ-page-nav" >
    <!-- <ul class="pagination"> -->

        {!! $paginations->links('layouts.pagination') !!}

        <!--     </ul>         -->                           
    </div>
</div>
</main>

<script type="text/javascript">
 $(document).ready(function(){
    $("#search-key").keyup(function(){
        $('.organization-section').html('');
        $('.organ-page-nav').html('');
        $('.loader-img').show();

        var orgName = $('#search-key').val();
        if(orgName==''){
            location.reload();
        }
        $.ajax({
            type: "POST",
            url: "{!!URL::to('admin/searchOrganisations')!!}",             
            data: {orgName:orgName,"_token":'<?php echo csrf_token()?>'},
            success: function(response) {

                if(response.length > 0){
                    $('.organization-section').html('');
                    $('.organization-section').append(response);
                    $('.loader-img').hide();

                }else{

                    $('.organization-section').html('');                                 
                    $('.organization-section').append('<div align="center" style="width:100%" class="text-danger" role="alert">No result found.</div>');
                    $('.loader-img').hide();
                }
            }
        }); 

    });
});
</script>

@include('layouts.adminFooter')