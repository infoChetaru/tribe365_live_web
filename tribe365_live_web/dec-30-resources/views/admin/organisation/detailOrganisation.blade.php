<!-- header -->
@section('title', 'Detail Organisation')
@include('layouts.adminHeader')

<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="organistiondetail-section">
				<div align="center">
					<div align="center">                            
						@if(session('message'))
						<div class="alert alert-success" role="alert">                                    
							{{session('message')}}
						</div>
						@endif
						@if(session('error'))
						<div class="alert alert-danger" role="alert">                                    
							{{session('error')}}
						</div>
						@endif
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="prof-acount-section">
							<div class="compy-logo">
								
								@if(!empty($organisations->ImageURL))
								<img src="{{asset('public/uploads/org_images').'/'.$organisations->ImageURL}}">
								@else 
								<img src="{{ asset('public/images/no-image.png')}}">

								@endif

							</div>
							<div class="edit-box">								
								<div class="account-btn">
									<h5>{{ucfirst($organisations->organisation)}}</h5>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="detail-btn">
							<div class="adit-btn">
								<a href="{{URL::to('admin/admin-organisation/'. base64_encode($organisations->id) .'/edit')}}">
									<button type="button"> Account </button>
								</a>							
							</div>
							<div class="adit-btn">
								<!-- <a href="{{URL::to('admin/dot-reports/'.base64_encode($organisations->id))}}"> -->
									<a href="{{route('reports.show',base64_encode($organisations->id))}}">
										<button type="button"> View Reports </button>
									</a>							
								</div>
								<div class="adit-btn">
									<a href="{{ URL::to('admin/add-office/'. base64_encode($organisations->id)) }}">
										<button type="button"> Add Office </button>
									</a>							
								</div>
								<div class="adit-btn">
									<a href="{{ URL::to('admin/list-office/'. base64_encode($organisations->id)) }}">
										<button type="button"> View Office </button>
									</a>									
									
								</div>
								
								<div class="adit-btn">
									<a href="{{ URL::to('admin/add-staff/'. base64_encode($organisations->id)) }}">
										<button type="button"> Add Staff </button>
									</a>									
									
								</div>

								<div class="adit-btn">
									<a href="{{ URL::to('admin/view-staff/'. base64_encode($organisations->id)) }}"" >
										<button type="button"> View Staff </button>
									</a>
								</div>


							<!-- <div class="adit-btn">
								<button type="text"> View Reports </button>
							</div> -->

						</div>
					</div>
				</div>
				<div class="organistiondetail-box">
					<div class="row">	
						<div class="col-md-4">
							@if(!empty($dots))
							<a href="{{URL::to('admin/dot-edit/'. base64_encode($dots->id).'/edit') }}">
								<div class="detail-box">
									<div class="detail-content">
										<h2> Directing </h2>									
										<!-- <span> Directing Our Tribe </span> -->
									</div>
								</div>
							</a>

							@else 

							<a href="{{URL::to('admin/dot-create/'.base64_encode($organisations->id))}}">					
								<div class="detail-box">
									<div class="detail-content">
										<h2> Directing </h2>
										<!-- <span> Directing Our Tribe </span> -->
									</div>
								</div>
							</a>						
							@endif
						</div>
						
						<div class="col-md-4">
							<a href="{{URL::to('admin/cot-detail/'.base64_encode($organisations->id))}}">
								<div class="detail-box">
									<div class="detail-content">
										<h2> Connecting </h2>
										<!-- <span> Connecting Our Tribe  </span> -->
									</div>
								</div>
							</a>
						</div>
						
						<div class="col-md-4">
							<a href="{{URL::to('admin/sot-detail',base64_encode($organisations->id))}}">
								<div class="detail-box">
									<div class="detail-content">
										<h2> Supercharging </h2>
										<!-- <span> Supercharging Our Tribe </span> -->
									</div>
								</div>
							</a>
						</div>
						
						<div class="col-md-4">

							<?php if(Session::get('improvementPassword')=='done'){ ?>

							<a href="{{URL::to('admin/iot-dashboard',base64_encode($organisations->id))}}">
								<div class="detail-box">
									<div class="detail-content">
										<h2> Improving </h2>
										
									</div>
								</div>
							</a>
							<?php }else{ ?>
								<a href="javascript:void(0);" onclick="improvementOpnPoup('<?php echo url('admin/iot-dashboard/'.base64_encode($organisations->id)); ?>');">
								<div class="detail-box">
									<div class="detail-content">
										<h2> Improving </h2>										
									</div>
								</div>
							</a>
							<?php } ?>

						</div>
						<div class="col-md-4">
							<a href="{{ URL::to('admin/action-list/').'/'.base64_encode($organisations->id)}}">	
								<div class="detail-box">
									<div class="detail-content">
										<h2> Actions </h2>
									</div>
								</div>
							</a>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

@include('layouts.adminFooter')