<!-- header -->
@section('title', 'Edit Department')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="dot-section">
				<h2> Edit Department </h2>
				<div class="form-section">

					{{ Form::model($departments, ['method' => 'PUT', 'route' => array('departments.update', base64_encode($departments->id)),'id'=>'form','enctype'=>'multipart/form-data']) }}
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">									
								<input type="text" name="department" placeholder="Enter Department" value="{{$departments->department}}" maxlength="100">
							</div>

							<div class="error" style="color:red;">
								{{$errors->first()}}
								{{session('message')}}
							</div>

						</div>

						<div class="Create-btn-cont">
							<button type="submit" class="btn">Save</button>
						</div>
					</div>

				</form>

			</div>
		</div>
	</div>
</div>
</main>

@include('layouts.adminFooter')