<!-- header -->
@section('title', 'List Department')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">
						<div class="loader-img" style="display: none;width: 70px; top: -57px;"><img src="{{asset('public/images/loader.gif')}}"></div>
						<div class="search-section" style="width: 80%;">
							<form>
								<input id="search-key" type="search" placeholder="Search...">
							</form>                        
							<div align="center">                            
								@if(session('message'))
								<div class="alert alert-success" role="alert">                                    
									{{session('message')}}
								</div>
								@endif
							</div>                        
						</div>
						<h2> Departments </h2>
						<div class="adit-btn">
							<a href="{!!route('departments.create')!!}"><button class="savebtn" type="button" > Add </button></a>
						</div>
						<div class="value-list">
							<table class="search-sec">
								<tr> 
									<th> Name </th>									
								</tr>
								@foreach($departments as $value)
								<tr>								
									<td>
										{{ucfirst($value->department)}}<div class="editable">

											<a href="{!!route('departments.edit',['id'=>base64_encode($value->id)])!!}">
												<button type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
											</a>

											{{ Form::open(['method'  => 'delete', 'route' => [ 'departments.destroy', base64_encode($value->id) ] ]) }}

											{{ Form::button('', ['type'=>'submit','onclick'=>'return confirm("Are you sure you want to delete ?")']) }}

											{{ Form::close() }}
										</div>
									</td>
									
								</tr>
								@endforeach()
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="organ-page-nav">
			{!! $departments->links('layouts.pagination') !!}
		</div>
	</div>
</main>

<script type="text/javascript">
	$(document).ready(function(){
		$("#search-key").keyup(function(){
			$('.search-sec').html('');			
			$('.organ-page-nav').html('');
			$('.loader-img').show();

			var department = $('#search-key').val();
			if(department==''){
				location.reload();
			}
			$.ajax({
				type: "POST",
				url: "{!!URL::to('admin/searchDepartment')!!}",             
				data: {department:department,"_token":'<?php echo csrf_token()?>'},
				success: function(response) {
					
					if(response.length > 1){

						$('.search-sec').html('');				
						$('.search-sec').append(response);             
						$('.loader-img').hide();
					}else{
						
						$('.search-sec').html('');
						$('.value-list').html('');				
						$('.value-list').append('<div align="center" class="text-danger" role="alert">No result found.</div>');
						$('.loader-img').hide();
					}
				}
			}); 		
		});
	});
</script>
@include('layouts.adminFooter')