<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		@import url('https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700');
		body {margin: 0;font-family: 'Roboto', sans-serif;}
		h1 {font-size:4.5em; color: #fff; text-shadow: 4px 0 2px #eb1c24;}
		.comeigsoon-section {display: flex; display:-webkit-flex; display:-ms-flex; align-items: center; -webkit-align-items: center; -ms-flex-align: center; justify-content: center; -webkit-justify-content: center; -ms-flex-pack: center; background-size: cover !important;
    height: 100vh;
    text-align: center; position: relative; }
    .comeigsoon-section:before {
    position: absolute;
    content: "";
    height: 100%;
    width: 100%;
    background-color: rgba(255,255,255,0.2);
    top: 0;
    left: 0;
}
.container {position: relative; z-index: 9;}
	</style>

</head>
<body>
	<div class="comeigsoon-section" style="background:url(http://chetaru.gottadesigner.com/tribe365/public/images/bg-img.jpg)no-repeat center center;">
		<div class="container">
				<img src="{{asset('public/images/login-logo.png')}}">
			<h1> Coming Soon </h1>
		</div>
	</div>
</body>
</html>