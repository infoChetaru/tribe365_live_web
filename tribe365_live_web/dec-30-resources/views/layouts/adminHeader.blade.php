					<!DOCTYPE html>
					<html>
					<head>						
						<title>Tribe 365 - @yield('title') </title>
						<meta name="viewport" content="width=device-width, initial-scale=1.0">
						<link rel="shortcut icon" href="{{asset('public/images/Favicon-icon.png')}}" type="image/x-icon"/>
						<link rel="stylesheet" type="text/css" href="{{asset('public/css/bootstrap.min.css')}}">
						<link rel="stylesheet" type="text/css" href="{{asset('public/css/style.css')}}">
						<link rel="stylesheet" type="text/css" href="{{asset('public/css/responsive.css')}}">
						<link rel="stylesheet" href="{{asset('public/css/jquery.mCustomScrollbar.css')}}">	
						<link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.teal-blue.min.css">
						<link rel="stylesheet" href="{{asset('public/css/scrollert.min.css')}}">
						<link rel="stylesheet" href="{{asset('public/css/scrollert.css')}}">
						<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
						
						<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet"> -->
						<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" rel="stylesheet">
						<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">

						<!-- Sweet alert CDN -->
						<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css" rel="stylesheet">

						<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js" type="text/javascript"></script>	

						<!-- gauge chart -->
						<script src="https://cdn.zingchart.com/zingchart.min.js"></script>
						<script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
						
						<script>
							zingchart.MODULESDIR = "https://cdn.zingchart.com/modules/";
							ZC.LICENSE = ["569d52cefae586f634c54f86dc99e6a9", "ee6b7db5b51705a13dc2339db3edaf6d"];
						</script>

						<style>
							html,
							#myChart {height: 100%;width: 100%;min-height: 150px;}
							.zc-ref {display: none;}

							#resetPassDiv .close, #forgotPassDiv .close, #impPasswordDiv .close {
							    position: absolute;
							    right: 10px;
							    top: 6px;

							}
							#resetPassDiv h4, #forgotPassDiv h4, #impPasswordDiv h4 {
						    width: 100%;
						    text-align: center;
						}
						.modalDivSty {
							    padding: 0 15px;
							}
						.modalDivSty .modal-body {
						    padding-top: 2px;
						}
						.spanDiv111 a:hover {
						    color: #000;
						}
						.spanDiv111 {
						    text-align: center;
						    display: inline-block;
						    width: 100%;
						}
						.spanDiv111 a {
						    color: #f00e0e;
						    display: inline-flex;
						    padding: 0 3px;
						    float: left;
						}
						.spanDiv111 a:last-child {float: right;}
						.check-right img {
						    border-radius: 2px;
						}
						.check-right {
						    position: absolute;
						    right: 7px;
						    top: 6px;
						    width: 32px;
						    display: inline-block;
						}
						.improvementFrm .form-group {position: relative;} 

						.text-muted {
							color: #da2c43 !important; 
						}
						.modal-header .close:hover {
						    background-color: transparent;
						}
						</style>

						<!-- for multi selection in IOT select -->
						<script src="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js" type="text/javascript"></script>						
					</head>
					<body>
						<div class="wrapper">

							<nav class="">
								<div class="logo-box">
									<a href="{{ url('/admin') }}"><img src="{{ asset('public/images/logo.png')}}"></a>

									<img class="active-logo" src="{{ asset('public/images/Favicon-icon.png') }}">

								</div>
								<div class="sidebar-menu">
									<div class="slid-nav">
										<ul>
											<li {{ request()->is('admin/admin-dashboard') ? ' class=active' : '' }}>
												<a href="{{ url('/admin') }}">
													<span class="icon-img"><img src="{{ asset('public/images/desh-icon.png')}}"> </span>
													<span class="menu-tital"> Dashboard </span>
												</a> 
											</li>
											<li {{ request()->is('admin/admin-organisation') ? ' class=active' : '' }}>
												<a href="{{ url('/admin/admin-organisation') }}">
													<span class="icon-img"><img src="{{ asset('public/images/org-icon.png')}}"> </span>
													<span class="menu-tital"> Organisations</span>
												</a> 
											</li>



											<li {{ request()->is('admin/dot-custom-values') ? ' class=active' : '' }}>
												<a href="{{ url('/admin/dot-custom-values') }}">
													<span class="icon-img"><img src="{{ asset('public/images/Manage-DOT-Values.png') }}"> </span>
													<span class="menu-tital"> Manage Directing Values </span>
												</a> 
											</li>
											<li  {{ request()->is('admin/departments') ? ' class=active' : '' }}>
												<a href="{{route('departments.index')}}">
													<span class="icon-img"><img src="{{ asset('public/images/Manage-Departments.png') }}"> </span>
													<span class="menu-tital"> Manage Departments </span>					
												</a> 
											</li>

											<li {{ request()->is('admin/cot-question') ? ' class=active' : '' }} {{ request()->is('admin/cot-fun-questions') ? ' class=active' : '' }} {{ request()->is('admin/manual-functional-lens-detail') ? ' class=active' : '' }}>
												<a href="#" data-toggle="dropdown">
													<span class="icon-img"><img src="{{ asset('public/images/COT.png') }}"></span>
													<span class="menu-tital"> Manage Connecting </span>
													<span class="menu-arrow"><img src="{{ asset('public/images/nav-arrow.png')}}"></span>
												</a> 
												<ul class="dropdown-menu">
													<li {{ request()->is('admin/cot-question') ? ' class=active' : '' }}>
														<a href="{{route('cot-question.index')}}">
															<span class="menu-tital">Team Role Map Questions </span>
														</a> 
													</li>
													<li {{ request()->is('admin/cot-fun-questions') ? ' class=active' : '' }}>
														<a href="{{URL::to('admin/cot-fun-questions')}}">
															<span class="menu-tital">Personality Type Questions</span>
														</a> 
													</li> 
													<li {{ request()->is('admin/manual-functional-lens-detail') ? ' class=active' : '' }}>
														<a href="{{URL::to('admin/manual-functional-lens-detail')}}">
															<span class="menu-tital">Personality Type Values</span>					
														</a> 
													</li>      
													<li {{ request()->is('admin/cot-team-role-map-description') ? ' class=active' : '' }}>
														<a href="{{URL::to('admin/cot-team-role-map-description')}}">
															<span class="menu-tital">Team Role Map Description</span>					
														</a> 
													</li>      
												</ul>
											</li>

											<li {{ request()->is('admin/sot-culture-questionnaire-list') ? ' class=active' : '' }} {{ request()->is('admin/sot-motivation-values') ? ' class=active' : '' }}>
												<a href="#" data-toggle="dropdown">
													<span class="icon-img"><img src="{{ asset('public/images/SOT.png') }}"></span>
													<span class="menu-tital"> Manage Supercharging </span>
													<span class="menu-arrow"> <img src="{{ asset('public/images/nav-arrow.png')}}"></span>
												</a>
												<ul class="dropdown-menu">
													<li {{ request()->is('admin/sot-culture-questionnaire-list') ? ' class=active' : '' }} >
														<a href="{{URL::to('admin/sot-culture-questionnaire-list')}}">
															<span class="menu-tital">Cultural Structure Questions</span>	
														</a>
													</li>
													<li {{ request()->is('admin/sot-motivation-questions') ? ' class=active' : '' }}>
														<a href="{{URL::to('admin/sot-motivation-questions')}}">	
															<span class="menu-tital">Motivation Questions</span>
														</a> 
													</li>
													<li {{ request()->is('admin/sot-motivation-values') ? ' class=active' : '' }}>
														<a href="{{URL::to('admin/sot-motivation-values')}}">	
															<span class="menu-tital">Motivation Values</span>
														</a> 
													</li>     
												</ul>
											</li>	

											<li {{ request()->is('admin/diagnostic') ? ' class=active' : '' }}>
												<a href="#" data-toggle="dropdown">
													<span class="icon-img"><img src="{{ asset('public/images/DIAGNOSTIC.png') }}"></span>
													<span class="menu-tital"> Manage Diagnostic </span>
													<span class="menu-arrow"> <img src="{{ asset('public/images/nav-arrow.png')}}"></span>
												</a>
												<ul class="dropdown-menu">
													<li {{ request()->is('admin/diagnostic') ? ' class=active' : '' }} >
														<a href="{{route('diagnostic.index')}}">
															<span class="menu-tital">Diagnostic Questions</span>	
														</a>
													</li>
													<li {{ request()->is('admin/diagnostic-option') ? ' class=active' : '' }} >
														<a href="{{URL::to('admin/diagnostic-option')}}">
															<span class="menu-tital">Diagnostic Options</span>	
														</a>
													</li>  
													<li {{ request()->is('admin/diagnostinc-cat-values') ? ' class=active' : '' }} >
														<a href="{{URL::to('admin/diagnostinc-cat-values')}}">
															<span class="menu-tital">Diagnostic Values</span>
														</a>
													</li> 
												</ul>
											</li>	

											<li {{ request()->is('admin/tribeometer') ? ' class=active' : '' }}>
												<a href="#" data-toggle="dropdown">
													<span class="icon-img"><img src="{{ asset('public/images/tribeometer.png') }}"></span>
													<span class="menu-tital"> Manage Tribeometer </span>
													<span class="menu-arrow"> <img src="{{ asset('public/images/nav-arrow.png')}}"></span>
												</a>
												<ul class="dropdown-menu">
													<li {{ request()->is('admin/tribeometer') ? ' class=active' : '' }}>
														<a href="{{route('tribeometer.index')}}">	
															<span class="menu-tital">Tribeometer Questions</span>
														</a> 
													</li> 
													<li {{ request()->is('admin/tribeometer-option') ? ' class=active' : '' }} >
														<a href="{{URL::to('admin/tribeometer-option')}}">
															<span class="menu-tital">Tribeometer Options</span>	
														</a>
													</li>   
													<li {{ request()->is('admin/tribeometer-cat-values') ? ' class=active' : '' }} >
														<a href="{{URL::to('admin/tribeometer-cat-values')}}">
															<span class="menu-tital">Tribeometer Values</span>
														</a>
													</li> 
												</ul>
											</li>

											<li {{ request()->is('admin/reports') ? ' class=active' : '' }}{{ request()->is('admin/diagnostic-report-org') ? ' class=active' : '' }}{{ request()->is('admin/tribeometer-report-org') ? ' class=active' : '' }}>

												<a href="{{route('reports.index')}}">
													<span class="icon-img"><img src="{{ asset('public/images/Manage-Records.png') }}"></span>
													<span class="menu-tital"> Manage Reports </span>
												</a>
											<!-- <a href="#" data-toggle="dropdown">
												<span class="icon-img"><img src="{{ asset('public/images/Manage-Records.png') }}"></span>
												<span class="menu-tital"> Manage Reports </span>
												<span class="menu-arrow"> <img src="{{ asset('public/images/nav-arrow.png')}}"></span>
											</a> -->


											<!-- <ul class="dropdown-menu">
												<li {{ request()->is('admin/reports') ? ' class=active' : '' }}>
													<a href="{{route('reports.index')}}">	
														<span class="menu-tital"> All Reports </span>
													</a> 
												</li> 
												<li  {{ request()->is('admin/diagnostic-report-org') ? ' class=active' : '' }}>
													<a href="{{URL::to('admin/diagnostic-report-org')}}">
														<span class="menu-tital">Diagnostic</span>	
													</a>
												</li> 
												<li {{ request()->is('admin/tribeometer-report-org') ? ' class=active' : '' }}>
													<a href="{{URL::to('admin/tribeometer-report-org')}}">
														<span class="menu-tital">Tribeometer</span>	
													</a>
												</li>   

												<li>
													<a href="{{URL::to('admin/manage-report')}}">
														<span class="menu-tital">Manage Report</span>	
													</a>
												</li> 
											</ul> -->
										</li>
										

									</ul>
								</div>
							</div>
						</nav>
						<div id="examples">
							<div class="content demo-y">
								<div class="app-body">
									<header>
										<div class="tob-nve-btn">
											<img src="{{ asset('public/images/menu-icon.png') }}">
										</div>	

										<?php
										if(!empty($org_detail))
										{
											$org_detail = explode('-', $org_detail);

											$orgId    = $org_detail[0];
											$org_logo = $org_detail[1];

											?>	
											<div class="org-logo">

												<?php if(Request::segment(2)=='feedback-list') { ?>

												<a href="javascript:goBackBtn();"><img src="{{ asset('public/uploads/org_images/'.$org_logo) }}"></a>

												<?php }else{ ?>		

												<a href="{{ url('/admin/organisation-detail/'.$orgId) }}"><img src="{{ asset('public/uploads/org_images/'.$org_logo) }}"></a>
												<?php } ?>


											</div>

											<?php
										}
										?>							
										<div class="profile-box">
											<div class="profile-img">
												<img src="{{ asset('public/uploads/user_profile\/').Auth::user()->imageUrl}}">
											</div>
											<div class="profile-user-list dropdown"> 
												<span class="dropdown-toggle" data-toggle="dropdown" style="background: url('{{ asset('public/images/user-drop-icon.png') }}')no-repeat;">{{ucfirst(Auth::user()->name)}} </span>
												<ul class="dropdown-menu">
													<li>
														<a href="{{ route('admin.profile') }}"><i class="fa fa-user" aria-hidden="true"></i> Profile </a>
													</li>
													<li>
														<a href="{{URL::to('admin/profile-password-edit')}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Change Password </a>
													</li>
													<li>
														<a href="{{ route('admin.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a>
													</li>
													<form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
														{{ csrf_field() }}
													</form>


												</ul>
											</div>
										</div>
									</header>



<!-- Modal -->
<div class="modal fade" id="ImprovementsModal" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content modalDivSty" id="impPasswordDiv">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h6 class="modal-title">Please enter password in order to access  improvements.</h6>
    </div>

    <div class="alert alert-success" role="fixed" id="showSucceesMsg" style="display: none;">
 	 	<strong>Success!</strong> Email sent successfully.
	</div>

    <div class="modal-body" id="mainContecntModal">
      <p>
      	<form name="improvementFrm" id="improvementFrm" class="improvementFrm" method="GET"> 

      		<div class="form-group">
		    <input type="password" name="improvementPassword" id="improvementPassword"  class="form-control" placeholder="Password">
		    <span class="glyphicon glyphicon-ok"></span>
		    <small id="passErrorDiv" class="form-text text-muted" style="display: none;">Please enter password.</small>

		    <small id="passErrorSorryDiv" class="form-text text-muted" style="display: none;">Sorry! Your password did not match.</small>	    
		    

		    <a href="#"  href="javascript:void(0);" onclick="improvementChkPass();" class="check-right"><img src="{{asset('public/images/rightClick.png')}}"></a>

		  </div>
       </form>   		
      </p>

      <span style="display: none;width: 70px;" id="spanImg111">
      	<img src="{{asset('public/images/loader.gif')}}">
      </span>

        <p id="spanDiv111" class="spanDiv111">
      		<a href="javascript:void();" onclick="forgotPassClick();" class="">Forgot Password</a>
      		<a href="javascript:void();" onclick="resetPassClick();" class="">Reset Password</a>
      	</p>
    </div>

  </div>  



   <!-- Modal content-->
   <!--
  <div class="modal-content modalDivSty" id="forgotPassDiv" style="display: none;">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h5 class="modal-title">Forgot Password</h5>
    </div>
    <div class="modal-body">
      <p>
      	<form name="improvementFrm1" id="improvementFrm2" method="POST"> 	      	

	      <div class="form-group">
		    <label for="exampleInputEmail1">Enter Email:  </label>
		    <input type="text" name="improvementEmail" id="improvementEmail"  class="form-control">
		    <small id="passErrorDiv1" class="form-text text-muted" style="display: none;">Please enter email.</small>
		    <small id="passErrorDiv2" class="form-text text-muted" style="display: none;">Invalid Email.</small>		    
		  </div>

		  <div class="modal-footer d-flex justify-content-center">
		  	<span id="spanSub">
		  		<a href="javascript:void(0);" onclick="forgotPassSubmit();" class="btn btn-primary">Submit</a>
		  	</span>
		  	<span style="display: none;width: 70px;" id="spanImg"><img src="{{asset('public/images/loader.gif')}}"></span>
		  </div>

       </form>
      </p>
    </div>
    <div class="modal-footer">
      <a href="javascript:void(0);" class="btn btn-primary" onclick="gobackImp();">Back</a>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>  
-->



   <div class="modal-content modalDivSty" id="resetPassDiv" style="display: none;">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h6 class="modal-title">Reset Password</h6>
    </div>

    <div class="alert alert-success" role="fixed" id="showSucceesMsg1" style="display: none;">
 	 	<strong>Success!</strong> Password changed successfully.
	</div>


    <div class="modal-body" id="mainContecntModal1">
      <p>
      	<form name="improvementFrm3" id="improvementFrm3" method="GET"> 
			  <div class="form-group">
			    <input type="password" class="form-control" id="currentPassImp" name="currentPassImp" placeholder="Current password">

			    <small id="resetPassf1" class="form-text text-muted" style="display: none;">Please enter current password.</small>

			     <small id="sorryMsgError" class="form-text text-muted" style="display: none;">Sorry! Your password did not match.</small>
			  </div>

			   <div class="form-group">
			    <input type="password" class="form-control" id="newPassImp" name="newPassImp" placeholder="New password">

			    <small id="resetPassf2" class="form-text text-muted" style="display: none;">Please enter new password.</small>
			  </div>

			   <div class="form-group">
			    <input type="password" class="form-control" id="confirmPassImp" name="confirmPassImp" placeholder="Confirm password">

			    <small id="resetPassf3" class="form-text text-muted" style="display: none;">Please enter confirm password.</small>
			    <small id="resetPassf4" class="form-text text-muted" style="display: none;">Confirm password does not match.</small>
			    
			  </div>

			 <div class="modal-footer d-flex justify-content-center">

			 	<span id="spanSub1">
			 	 <a href="javascript:void(0);" onclick="resetPassSubmit();" class="btn btn-danger">
			 	 	<strong>Submit</strong>
			 	</a>
			 	</span>

			 	 <span style="display: none;width: 70px;" id="spanImg1"><img src="{{asset('public/images/loader.gif')}}"></span>

			</div>
		</form>
      </p>
    </div>
  </div>  


</div>
</div>

<input type="hidden" name="improvementUrl" id="improvementUrl">

<script type="text/javascript">
function improvementOpnPoup(url){
	$('#improvementUrl').val(url);
	$('#improvementPassword').val('');
	$('#improvementEmail').val('');
	$('#currentPassImp').val('');
	$('#newPassImp').val('');
	$('#confirmPassImp').val('');

	$('#passErrorDiv').hide();
	$('#passErrorSorryDiv').hide();
	$('#resetPassf1').hide();
	$('#sorryMsgError').hide();
	$('#resetPassf2').hide();
	$('#resetPassf3').hide();
	$('#resetPassf4').hide();

	$('#impPasswordDiv').show();
	$('#resetPassDiv').hide();

	$('#mainContecntModal').show();
	$('#showSucceesMsg').hide();
	$('#showSucceesMsg1').hide();

	$('#spanSub1').show();
	$('#spanImg1').hide();

	$('#spanSub').show();
	$('#spanImg').hide();
	
	$('#passErrorDiv').hide();




	$('#ImprovementsModal').modal('toggle');
}

function gobackImp(){
	$('#impPasswordDiv').show();
	$('#forgotPassDiv').hide();
	$('#resetPassDiv').hide();
}


function resetPassSubmit(){
	$('#passErrorSorryDiv').hide();
	$('#resetPassf1').hide();
	$('#sorryMsgError').hide();
	$('#resetPassf2').hide();
	$('#resetPassf3').hide();
	$('#resetPassf4').hide();

	var currentPassImp 	= $('#currentPassImp').val();
	var newPassImp 		= $('#newPassImp').val();
	var confirmPassImp 	= $('#confirmPassImp').val();
	if(currentPassImp==''){
		$('#resetPassf1').show();
		return false;
	}else if(newPassImp==''){
		$('#resetPassf2').show();
		return false;
	}else if(confirmPassImp==''){
		$('#resetPassf3').show();
		return false;
	}else if(newPassImp!=confirmPassImp){
		$('#resetPassf3').hide();
		$('#resetPassf4').show();
		return false;
	}

	$('#spanSub1').hide();		
	$('#spanImg1').show();


	$.ajax({
		url: "{{URL::to('admin/resetPasswordImprovementSubmit')}}",
		type: "POST",
		dataType: "JSON",
		data: {currentPassImp:currentPassImp,newPassImp:newPassImp,"_token":'<?php echo csrf_token()?>'},
		success: function(response){
			//console.log(response);
			//swal("Deleted!", "Your imaginary file has been deleted.", "success");
			
			if (response.status == 200 && response.passwordChk == 1) {		

				$('#showSucceesMsg1').show();		
				$('#mainContecntModal1').hide();

				//$('#ImprovementsModal').modal('toggle');
				//swal("Password!", "Password has been changed..", "success");
			}else{
				$('#spanSub1').show();		
				$('#spanImg1').hide();
				//swal("Password!", "Sorry! your password not match.", "error");
				$('#resetPassf1').hide();
				$('#sorryMsgError').show();
				$('#resetPassf2').hide();	
				$('#resetPassf3').hide();	
				$('#resetPassf4').hide();	
				$('#passErrorDiv').hide();	
				$('#passErrorSorryDiv').hide();	
			}			
		}
	});	

}


function improvementChkPass(){	
	var improvementPassword = $('#improvementPassword').val();
	if(improvementPassword==''){
		//swal("Password!", "Please enter your password.", "error");	
		$('#passErrorDiv').show();
		return false;
	}
	$.ajax({
		url: "{{URL::to('admin/improvementChkPass')}}",
		type: "POST",
		dataType: "JSON",
		data: {improvementPassword:improvementPassword,"_token":'<?php echo csrf_token()?>'},
		success: function(response){
			//console.log(response);
			//swal("Deleted!", "Your imaginary file has been deleted.", "success");
			
			var url = $('#improvementUrl').val();
			if (response.status == 200 && response.passwordChk == 1) {
				location.href = url;
			}else{
				//swal("Password!", "Sorry! your password not match.", "error");	
				$('#passErrorSorryDiv').show();
				$('#passErrorDiv').hide();

				$('#resetPassf1').hide();
				$('#sorryMsgError').hide();
				$('#resetPassf2').hide();	
				$('#resetPassf3').hide();	
				$('#resetPassf4').hide();	
				$('#passErrorDiv').hide();	

			}
			
		}
	});	
}

function forgotPassClick(){
	forgotPassSubmit();
	//$('#impPasswordDiv').hide();
	//$('#resetPassDiv').hide();
	//$('#forgotPassDiv').show();
	//$('#spanSub').show();		
	//$('#spanImg').hide();
}

function resetPassClick(){
	$('#impPasswordDiv').hide();
	$('#resetPassDiv').show();
	$('#forgotPassDiv').hide();
	$('#sorryMsgError').hide();

	$('#showSucceesMsg').hide();		
	$('#mainContecntModal').show();

	$('#showSucceesMsg1').hide();		
	$('#mainContecntModal1').show();

	
}

function forgotPassSubmit(){
	//var improvementEmail = $('#improvementEmail').val();
	//if(improvementEmail==''){	
		//$('#passErrorDiv1').show();		
		//return false;
	//}
	//Check email format
	//var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    //if (reg.test(improvementEmail) == false) {
        //$('#passErrorDiv1').hide();		
        //$('#passErrorDiv2').show();		
        //return false;
    //}

	$('#showSucceesMsg').show();		
	$('#mainContecntModal').hide();

	$.ajax({
		url: "{{URL::to('admin/improvementForgotPassword')}}",
		type: "POST",
		dataType: "JSON",
		data: {"_token":'<?php echo csrf_token()?>'},
		success: function(response){
			//console.log(response);
			//swal("Deleted!", "Your imaginary file has been deleted.", "success");
			
			if (response.status == 200 && response.passwordChk == 1) {		
				$('#ImprovementsModal').modal('toggle');
				$('#spanImg111').hide();		
				$('#spanDiv111').show();

				//swal("Email!", "Email sent.", "success");
			}else{
				$('#spanSub').show();		
				$('#spanImg').hide();
				//swal("Password!", "Sorry! your email not match.", "error");	
			}			
		}
	});	
}
</script>

<script>
function goBackBtn() {
  window.history.back();
}
</script>