<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('thumsup', function () {
	return view('admin.report.report_graph.dotThumbsuplist');

});

Route::get('iot1', function () {
	return view('admin.IOT.iot1');

});

Route::get('/iot2', function () {
	return view('admin.IOT.iot2');
});

Route::get('/iot3', function () {
	return view('admin.IOT.iot3');
});

Route::get('/iot4', function () {
	return view('admin.IOT.iot4');
});


// Route::get('/', function () {
// 	return view('layouts.comingsoon');
// });


/*SET NEW USER PASSWORD**/ 
Route::get('setUserPassword/{token}', 'API\ApiOrganisationController@setUserPassword')->name('setUserPassword');

//update new password of user
Route::post('updateNewUserpassword/{confirmationCode}','API\ApiOrganisationController@updateNewUserpassword');

// User Authentication
Route::get('/','User\UserLoginController@index');
Route::post('/user/login',['as'=>'user.auth','uses'=>'User\UserLoginController@login']);
Route::post('/user/logout',['as'=>'user.logout','uses'=>'User\UserLoginController@logout']);

/*USER FUNCTIONALITY*/
Route::group(['prefix'=>'user'],function ()
{	
	//User Dashboard
	Route::get('/user-dashboard','User\UserDashboardController@index');
	//user profile
	// Route::get('user-profile','Admin\UserLoginController@getProfile');
	/*user profile*/
	Route::get('profile',['as'=>'user.profile','uses'=>'User\UserLoginController@getProfile']);
	/*user profile update*/
	Route::post('profile-update',['as'=>'user.profile.update','uses'=>'User\UserLoginController@updateProfile']);
	/*user profile password*/
	Route::get('profile-password-edit','User\UserLoginController@getEditProfilePassword');
	/*user profile update password*/
	Route::post('profile-password-update','User\UserLoginController@updateProfilePassword');
});


/*----------FORGOT PASSWORD---------------*/

Route::get('resetPassword/{confirmationCode}', [
	'as' => 'confirmation_path',
	'uses' => 'API\ApiLoginController@resetPassword'
]);
Route::post('updatepassword/{confirmationCode}','API\ApiLoginController@updatePassword');


/*web*/
/*get forgot page*/
Route::get('forgot-password/{userRole}','Admin\AdminLoginController@getForgotPassword');
/*update forgot password*/
Route::post('forgotPassword','Admin\AdminLoginController@forgotPassword');

/*----------FORGOT PASSWORD----------------*/



//Ram added
//improvement password
Route::post('admin/improvementChkPass','Admin\AdminLoginController@improvementChkPass');
Route::post('admin/improvementForgotPassword','Admin\AdminLoginController@improvementForgotPassword');

Route::get('resetPasswordImprovement/{confirmationCode}', [
	'as' => 'confirmation_path',
	'uses' => 'API\ApiLoginController@resetPasswordImprovement'
]);
Route::post('updatepasswordImprovement/{confirmationCode}','API\ApiLoginController@updatePasswordImprovement');
Route::post('admin/resetPasswordImprovementSubmit','Admin\AdminLoginController@resetPasswordImprovementSubmit');

//CRON URL's
//For culture Index & engagement index
Route::get('admin/getIndexesOfAllOrganization/{date?}','Admin\AdminReportController@getIndexesOfAllOrganization');
//By past dates
Route::get('admin/getIndexesOfAllOrganizationByPastDate','Admin\AdminReportController@getIndexesOfAllOrganizationByPastDate');
//For Diagnostic Index
Route::get('admin/getDiagnosticIndexReport','Admin\AdminReportController@getDiagnosticIndexReport');
//By past dates
Route::get('admin/getDiagnosticIndexReportByPastDate','Admin\AdminReportController@getDiagnosticIndexReportByPastDate');

//For Tribeometer Graph
Route::get('admin/getTribeometerIndexReport','Admin\AdminReportController@getTribeometerIndexReport');
//By past dates
Route::get('admin/getTribeometerIndexReportByPastDate','Admin\AdminReportController@getTribeometerIndexReportByPastDate');

//For Culture Structure Graph
Route::get('admin/getCultureStructureIndexReport','Admin\AdminReportController@getCultureStructureIndexReport');
//By past dates
Route::get('admin/getCultureStructureIndexReportByPastDate','Admin\AdminReportController@getCultureStructureIndexReportByPastDate');

//For Team Role Graph
Route::get('admin/getTeamRoleIndexReport','Admin\AdminReportController@getTeamRoleIndexReport');
//By past dates
Route::get('admin/getTeamRoleIndexReportByPastDate','Admin\AdminReportController@getTeamRoleIndexReportByPastDate');

//For Motivation Graph
Route::get('admin/getMotivationIndexReport','Admin\AdminReportController@getMotivationIndexReport');
//By past dates
Route::get('admin/getMotivationIndexReportByPastDate','Admin\AdminReportController@getMotivationIndexReportByPastDate');

//For Directing Graph
Route::get('admin/getDirectingIndexReport','Admin\AdminReportController@getDirectingIndexReport');
//By past dates
Route::get('admin/getDirectingIndexReportByPastDate','Admin\AdminReportController@getDirectingIndexReportByPastDate');

//For Personality Type Graph
Route::get('admin/getPersonalityTypeIndexReport','Admin\AdminReportController@getPersonalityTypeIndexReport');
//By past dates
Route::get('admin/getPersonalityTypeIndexReportByPastDate','Admin\AdminReportController@getPersonalityTypeIndexReportByPastDate');

// For happy index graph
Route::get('admin/getHappyIndexReport','Admin\AdminReportController@getHappyIndexReport');
//By past dates
Route::get('admin/getHappyIndexReportByPastDate','Admin\AdminReportController@getHappyIndexReportByPastDate');

//For Happy index daily count
Route::get('admin/getHappyIndexDailyCountReport','Admin\AdminReportController@getHappyIndexDailyCountReport');
//By past dates
Route::get('admin/getHappyIndexDailyCountReportByPastDate','Admin\AdminReportController@getHappyIndexDailyCountReportByPastDate');

//For users count daily
Route::get('admin/getUsersCountReport','Admin\AdminReportController@getUsersCountReport');
//By past dates
Route::get('admin/getUsersCountReportByPastDate','Admin\AdminReportController@getUsersCountReportByPastDate');

//For Kudos count beliefs and values monthly
Route::get('admin/getKudosCountReport','Admin\AdminReportController@getKudosCountReport');
//By past dates
Route::get('admin/getKudosCountReportByPastDate','Admin\AdminReportController@getKudosCountReportByPastDate');

//For Kudos champ leaderboard monthly
Route::get('admin/getKudosChampLeaderboard','Admin\AdminReportController@getKudosChampLeaderboard');
//By past dates
Route::get('admin/getKudosChampLeaderboardByPastDate','Admin\AdminReportController@getKudosChampLeaderboardByPastDate');


//For Personality Type Graph OLD
Route::get('admin/getPersonalityTypeIndexReport1','Admin\AdminReportController@getPersonalityTypeIndexReport1');

//Kudos Notification
Route::get('admin/kudosNotification','Admin\AdminDashboardController@kudosNotification');

/*Admin Authentication*/
Route::get('/admin','Admin\AdminLoginController@index');
Route::post('/admin/login',['as'=>'admin.auth','uses'=>'Admin\AdminLoginController@login']);
Route::post('/admin/logout',['as'=>'admin.logout','uses'=>'Admin\AdminLoginController@logout']);

/*ADMIN FUNCTIONALITY*/
Route::group(['prefix'=>'admin','middleware'=>['admin']],function ()
{	

	//Send notification manually
	Route::get('sendNotificationManual','Admin\AdminDashboardController@sendNotificationManual');

	/*ADMIN ADD FUNCTIONALITY*/
	//Add new admin  view
	Route::get('add-admin-view','Admin\AdminLoginController@addAdminView');
	/*Add new admin*/
	Route::post('add-admin','Admin\AdminLoginController@addAdmin');
	// Admin List
	Route::get('admin-list','Admin\AdminLoginController@adminList');
	// Delete Admin
	Route::post('delete-admin','Admin\AdminLoginController@deleteAdmin');
	/*search admin by name and email*/
	Route::post('searchAdmin','Admin\CommonController@searchAdmin');


	/*ADMIN PROFILE FUNCTIONALITY*/

	/*admin profile*/
	Route::get('profile',['as'=>'admin.profile','uses'=>'Admin\AdminLoginController@getProfile']);
	/*admin profile update*/
	Route::post('profile-update',['as'=>'admin.profile.update','uses'=>'Admin\AdminLoginController@updateProfile']);
	/*admin profile password*/
	Route::get('profile-password-edit','Admin\AdminLoginController@getEditProfilePassword');
	/*admin profile update password*/
	Route::post('profile-password-update','Admin\AdminLoginController@updateProfilePassword');
	/*Dashboard FUNCTIONALITY*/
	Route::get('/admin-dashboard','Admin\AdminDashboardController@index');
	// Get motivation categories graph
	Route::post('getMotivationCat','Admin\AdminDashboardController@getMotivationCat');
	// Get tribeometer categories graph
	Route::post('getTribeometerCat','Admin\AdminDashboardController@getTribeometerCat');
	// Get culture structure categories graph
	Route::post('getCultureStructureCat','Admin\AdminDashboardController@getCultureStructureCat');
	// Get team role categories graph
	Route::post('getTeamRoleCat','Admin\AdminDashboardController@getTeamRoleCat');
	// Get Diagnostics categories graph
	Route::post('getDiagnosticCat','Admin\AdminDashboardController@getDiagnosticCat');
	// Get Diagnostics categories graph
	Route::post('getDiagnosticSubCat','Admin\AdminDashboardController@getDiagnosticSubCat');
	// Get dot beliefs graph
	Route::post('getDotBeliefCat','Admin\AdminDashboardController@getDotBeliefCat');
	// Get dot value graph
	Route::post('getDotValueCat','Admin\AdminDashboardController@getDotValueCat');
	// Get Thumbsp beliefs graph
	Route::post('getThumbsupBeliefCat','Admin\AdminDashboardController@getThumbsupBeliefCat');
	// Get Thumbsp values category graph
	Route::post('getThumbsupValuesCat','Admin\AdminDashboardController@getThumbsupValuesCat');
	// Get Personality type category graph
	Route::post('getPerTypeCat','Admin\AdminDashboardController@getPerTypeCat');
	// Get Happy index mood values graph
	Route::post('getHappyIndexCat','Admin\AdminDashboardController@getHappyIndexCat');
	// Get Happy index week graph
	Route::post('getHappyIndexWeekGraph','Admin\AdminDashboardController@getHappyIndexWeekGraph');
	// Get Happy index week category graph
	Route::post('getHappyIndexWeekCat','Admin\AdminDashboardController@getHappyIndexWeekCat');
	// Get Happy index days graph
	Route::post('getHappyIndexDaysGraph','Admin\AdminDashboardController@getHappyIndexDaysGraph');
	// Get Happy index days category graph
	Route::post('getHappyIndexDaysCat','Admin\AdminDashboardController@getHappyIndexDaysCat');
	// Get Personality type subcategory graph
	Route::post('getDashboardSubPerTypeGraph','Admin\AdminDashboardController@getDashboardSubPerTypeGraph');
	// Get Personality type single sub category graph
	Route::post('getPerTypeSubCat','Admin\AdminDashboardController@getPerTypeSubCat');
	//Get happy index reponders month graph for single category
	Route::post('getHappyIndexResCat','Admin\AdminDashboardController@getHappyIndexResCat');
	//Get happy index reponders week graph
	Route::post('getHappyIndexRespWeekGraph','Admin\AdminDashboardController@getHappyIndexRespWeekGraph');
	//Get happy index reponders week graph for single category
	Route::post('getHappyIndexRespWeekCat','Admin\AdminDashboardController@getHappyIndexRespWeekCat');
	//Get happy index reponders days graph
	Route::post('getHappyIndexRespDaysGraph','Admin\AdminDashboardController@getHappyIndexRespDaysGraph');
	//update tribeometer questionnaire from dashboard
	Route::post('updateTribeometerQues','Admin\AdminDashboardController@updateTribeometerQues');
	//update diagnostic questionnaire from dashboard
	Route::post('updateDiagnosticQues','Admin\AdminDashboardController@updateDiagnosticQues');
	//update culture structure questionnaire from dashboard
	Route::post('updateCultureStructureQues','Admin\AdminDashboardController@updateCultureStructureQues');
	//get organisation name by id
	Route::post('getOrgNameById','Admin\CommonController@getOrgNameById');

	/*Kudos Leaderboard FUNCTIONALITY*/
	// Kudos Leaderboard 
	Route::get('kudos-Leaderboard','Admin\AdminDashboardController@kudosLeaderboard');
	//get Months By Year
	Route::post('getMonthsByYear','Admin\CommonController@getMonthsByYear');
	//Kudo champ
	Route::get('kudosChamp','Admin\AdminOrganisationController@kudosChamp');


	/*Organisation FUNCTIONALITY*/
	Route::resource('admin-organisation','Admin\AdminOrganisationController');

	Route::get('organisationPdf/{id}','Admin\AdminOrganisationController@organisation_pdf');
	Route::post('organisation_pdf_search/{id}','Admin\AdminOrganisationController@organisation_pdf_search');

	/*UPDATE organisation*/
	Route::post('organisation-update','Admin\AdminOrganisationController@organisation_update')->name('organisation-update');

	Route::post('organisation-update1','Admin\AdminOrganisationController@organisation_update1')->name('organisation-update1');

	Route::post('organisation-update2','Admin\AdminOrganisationController@organisation_update2')->name('organisation-update2');

	Route::post('organisation-update3','Admin\AdminOrganisationController@organisation_update3')->name('organisation-update3');	
	Route::post('organisation-update4','Admin\AdminOrganisationController@organisation_update4')->name('organisation-update4');	
	/*Inactive organisation*/
	Route::post('organisation-delete','Admin\AdminOrganisationController@deleteOrganisation');
	/*delete organisation*/
	Route::post('delete-oragnisation','Admin\AdminOrganisationController@deleteWholeOrganisation');
	
	/*DOT FUNCTIONALITY*/	

	Route::resource('admin-dot','Admin\AdminDotController');
	/*show add page of custom adding page*/
	Route::get('custome-value','Admin\AdminDotController@createCustomValues')->name('custome-value');
	/*store custom value*/
	Route::post('store-custom-value','Admin\AdminDotController@storeCustomValues')->name('store-custom-value');
	/*get edit page of DOT*/
	Route::resource('dot-edit','Admin\AdminDotController');
	/*add DOT*/
	Route::get('dot-create/{id}','Admin\AdminDotController@addDot');
	/*UPDATE DOT*/
	Route::post('dot-update','Admin\AdminDotController@updateDotDetail')->name('dot-update');
	/*delete dot*/
	Route::post('dot-value-delete','Admin\AdminDotController@deleteDot');
	/* delete belief */
	Route::post('dot-belief-delete','Admin\AdminDotController@deleteBelief');
	/*UPDATE belief*/
	Route::post('dot-belief-update','Admin\AdminDotController@updateDotBeliefDetail')->name('dot-belief-update');
	/*ADD belief in edit section*/
	Route::post('dot-add-belief','Admin\AdminDotController@addBelief')->name('dot-add-belief');
	/*show add page of custom adding page*/
	Route::get('custome-value','Admin\AdminDotController@createCustomValues')->name('custome-value');
	/*store custom value*/
	Route::post('store-custom-value','Admin\AdminDotController@storeCustomValues')->name('store-custom-value');
	/*get custom values list*/
	Route::get('dot-custom-values','Admin\AdminDotController@beliefCustomeValues')->name('dot-custom-values');
	/*edit custom DOT value*/
	Route::get('edit-custom-value/{id}','Admin\AdminDotController@editCustomValue')->name('edit-custom-value');
	/*update dot custom values*/
	Route::post('update-custom-value','Admin\AdminDotController@updateCustomValues')->name('update-custom-value');
	/*delet dot custom values*/
	Route::get('delete-custom-value/{id}','Admin\AdminDotController@deleteCustomValue')->name('delete-custom-value');
	/*get evidence list*/ 
	Route::get('get-evidence-list/{dotId}/{type}/{id}','Admin\AdminDotController@getEvidenceList')->name('get-evidence-list');
	/*get custom dot values modal */
	Route::post('get-custom-value-evidence-modal','Admin\AdminDotController@getDotCustomValueList')->name('get-custom-value-evidence-modal');




	/*USER FUNCTIONALITY*/

	Route::resource('admin-user','Admin\AdminUserController');	

	Route::get('add-staff/{id}','Admin\AdminUserController@addStaff');
	/*get user /staff list*/
	Route::get('view-staff/{id}','Admin\AdminUserController@viewStaffList');
	/*add user by csv file*/
	Route::post('add-user-csv','Admin\AdminUserController@addUserCSV')->name('add-user-csv');
	/*export user list csv*/
	Route::get('user-list-export/{id}','Admin\AdminUserController@exportUserList');
	// delete user
	Route::post('delete-user','Admin\AdminUserController@destroy');


	/*ORGANISATION FUNCTIONALITY*/

	/*get organisation detail DOT COT org. SOT etc*/
	Route::get('organisation-detail/{id}','Admin\AdminOrganisationController@showOrganisationDetail');	
	/*get department create page */
	Route::get('add-office/{id}','Admin\AdminOrganisationController@addOffice');

	Route::get('list-office/{id}','Admin\AdminOrganisationController@listOffice');
	
	Route::post('create-office','Admin\AdminOrganisationController@createOffice');

	/*DEPARTMENT FUNCTIONALITY*/

	/*get all departments list*/
	Route::resource('departments','Admin\AdminDepartmentController');
	
	
	/*ACTION FUNCTIONALITY*/

	/*Dot action lits*/
	Route::get('action-list/{id}/{index?}/{feedthemeId?}','Admin\AdminActionController@getActionList');
	// Route::get('action-list/{id}','Admin\AdminActionController@getActionList');
	/*add DOT action */
	Route::get('add-action/{id}','Admin\AdminActionController@addAction');
	/*store active detail*/
	Route::resource('action','Admin\AdminActionController');
	/*get actions comments*/
	Route::get('action-comments/{id}','Admin\AdminActionController@getCommentsList');
	/*add new comments*/
	Route::post('action-add-comment','Admin\AdminActionController@addComment');
	/*updated actione status*/
	Route::post('action-status-update','Admin\AdminActionController@updateActionStatus');
	/*get responsible user list*/
	Route::POST('get-responsible-users','Admin\AdminActionController@getResUser');
	/*delete action*/
	Route::get('delete-action/{id}','Admin\AdminActionController@deleteAction');
	/*get action theme modal response */
	Route::post('get-action-theme-modal','Admin\AdminActionController@getActionThemeModal')->name('get-action-theme-modal');
	/*add new action theme by ajax when modal is open*/
	Route::post('add-new-action-theme-by-ajax','Admin\AdminActionController@addNewActionThemeByAjax')->name('add-new-action-theme-by-ajax');

	

	/*COMMON FUNCTIONALITY*/

	/*DEPARTMENT functionality*/
	Route::post('admin-add-department','Admin\CommonController@addNewDepartment')->name('admin-add-department');
	/*Common controller*/
	Route::get('get-department','Admin\CommonController@getAllDepartments');	
	/*get dot custom values list*/
	Route::get('get-custom-dot-list','Admin\CommonController@getCustomDotValues');
	/*check email is exists in table*/
	Route::post('search-email','Admin\CommonController@getEmail');	
	/*get offices list*/
	Route::post('get-offices','Admin\CommonController@getOfficeList');
	/*get departments list*/
	Route::post('get-department','Admin\CommonController@getDepartmentList');
	/*get user list*/
	Route::post('get-users','Admin\CommonController@getUsers');

	Route::get('inArray','Admin\CommonController@inArray');
	/*get org name*/
	Route::post('get-org-name','Admin\CommonController@getOrgName')->name('get-org-name');
	/*get search data of the values */
	Route::get('search-dot-values','Admin\CommonController@getDotValueSearchData')->name('search-dot-values');
	// /*send notification mail */
	Route::get('sendNotificationMail','Admin\CommonController@sendNotificationMail')->name('sendNotificationMail');
	/*get offices by organisation*/
	Route::post('getOfficesByOrgId','Admin\CommonController@getOfficesByOrgId');	
	/*get departments by office*/
	Route::post('getDepartmentByOfficecId','Admin\CommonController@getDepartmentByOfficecId');
	/*search organisation functionality in list organisation*/
	Route::post('searchOrganisations','Admin\CommonController@searchOrganisations');
	/*get office search result list*/
	Route::post('searchOffice','Admin\CommonController@searchOffice');
	/*get office search result list*/
	Route::post('searchStaff','Admin\CommonController@searchStaff');
	/*get departments search result list*/
	Route::post('searchDepartment','Admin\CommonController@searchDepartment'); 
	/*get search reports organisation result list*/
	Route::post('searchReportOrganisations','Admin\CommonController@searchReportOrganisations');
	/*get department by office id this is used in report graph section*/
	Route::post('getDepartmentByOfficecIdforGraph','Admin\CommonController@getDepartmentByOfficecIdforGraph');
	/*get office by organisation id this is used in dashboard graph section*/
	Route::post('getOfficeByOrgIdforGraph','Admin\CommonController@getOfficeByOrgIdforGraph');
	/*get department by office id in kudos leaderboard*/
	Route::post('getDepartmentByOfficecIdforKudos','Admin\CommonController@getDepartmentByOfficecIdforKudos');
	/*get office by organisation id in kudos leaderboard*/
	Route::post('getOfficeByOrgIdforKudos','Admin\CommonController@getOfficeByOrgIdforKudos');


	/*COT QUESTIONS FUNCTIONALITY*/

	Route::resource('cot-question','Admin\AdminCOTquestionController');
	/*get functional lens question list*/
	Route::get('cot-fun-questions','Admin\AdminCOTquestionController@getCOTfunctionalLensQuestionList');
	/*get cot functional lens questions option list*/
	Route::get('cot-fun-que-option/{id}','Admin\AdminCOTquestionController@getCOTfunctionalLensQuestionOptionList');
	/*update functional lens question and option*/
	Route::post('update-cot-fun-que-opt','Admin\AdminCOTquestionController@updateFunctionalLensQueOpt');


	
	/*COT FUNCTIONALITY*/

	Route::resource('cot','Admin\AdminCOTController');

	/*get cot detail page*/
	Route::get('cot-detail/{id}','Admin\AdminCOTController@getCotDetail');

	/*get team role map view*/
	Route::match(['get', 'post'],'cot-team-role-map/{id}','Admin\AdminCOTController@getTeamRoleMap');
	/*update organisaton has_cot status*/
	Route::get('cot-team-role-map-update-has-cot/{id}','Admin\AdminCOTController@updateCOTstatus');
	/*get COt maper rank DATA FROM API*/
	Route::get('getCOTmaperkey','API\ApiCOTController@getCOTmaperkey');
	/*get list of functional lens list*/
	Route::match(['get', 'post'],'cot-functional-lens/{id}','Admin\AdminCOTController@getFunctinalLensList');
	/*get functional lens manage page */
	Route::get('manual-functional-lens-detail','Admin\AdminCOTController@manageFunctionalLensList');
	/*update cot functional initial values*/
	Route::post('update-cot-fun-initial-value','Admin\AdminCOTController@updateCOTinitiatialValue');
	/*update cot functional insight values*/
	Route::post('update-cot-fun-strength-value','Admin\AdminCOTController@updateCOTfunctionalLensInsightForStrenghtValue');
	//edit page of COT functional lens tribe tips seek and persuade values
	Route::get('edit-cot-fun-tribe-tips-value/{id}/{type}','Admin\AdminCOTController@editCOTfunTribeTipsSeekPersuadeValues');
	/*update cot functional lens tribe tips seek and persuade values*/
	Route::post('update-cot-fun-tribe-tips-value','Admin\AdminCOTController@updateCOTtribeSeekPersuadevalues');
	/*delete cot functional lense tribe tips value */
	Route::get('delete-cot-fun-tribe-tips-value/{id}','Admin\AdminCOTController@deleteCOTfunLensTribeTipsvalue');
	/*update cot functional lens starting date*/
	Route::post('updateCOTfuncLensStartingDate','Admin\AdminCOTController@updateCOTfuncLensStartingDate');
	/*update cot functional lens tribe tips summay*/
	Route::post('update-cot-tribe-tips-summary','Admin\AdminCOTController@updateTribeTipsSummary');
	/*get COT team role map description list*/
	Route::get('cot-team-role-map-description','Admin\AdminCOTController@getTeamRoleMapDescriptionList');
	/*update cot team role map description values*/
	Route::post('update-cot-team-role-description','Admin\AdminCOTController@updateCOTteamroleMapDescription');
	
	
	/*REPORT FUNCTIONALITY*/

	Route::resource('reports','Admin\AdminReportController');
	/*get */
	Route::match(['get', 'post'],'dot-reports/{id}','Admin\AdminReportController@getDOTreports');
	/*get rating list of belief and values */
	Route::match(['get', 'post'],'report-belief-value-rating-list/{id}','Admin\AdminReportController@getBeliefValueRatingList')->name('report-belief-value-rating-list');
	/*get belief indivisual ratings*/
	Route::match(['get', 'post'],'report-belief-individual-ratings/{id}','Admin\AdminReportController@getIndividualBeliefValueRatingList')->name('report-belief-individual-ratings');
	/*get diagnostic reports organisations list*/
	Route::get('diagnostic-report-org','Admin\AdminReportController@getDiagOrgList');
	/*get diagnostic resport graph*/
	Route::get('diagnostic-report-graph/{id}','Admin\AdminReportController@getDiagGraph'); 
	/*get diagnostic reports organisations list*/
	Route::get('tribeometer-report-org','Admin\AdminReportController@getTribeoOrgList');
	/*get tribeometer resport graph*/
	Route::get('tribeometer-report-graph/{id}','Admin\AdminReportController@getTribeoGraph');
	Route::get('manage-report','Admin\AdminReportController@manageReport');
	/*get cot functional lens */
	Route::post('getCOTFunctionalLensGraph','Admin\AdminReportController@getCOTFunctionalLensGraph');
	/*get sot motivation data for graph*/
	Route::post('getSOTmotivationGraph','Admin\AdminReportController@getSOTmotivationGraph'); 
	/*get COT team role mape for report graph*/
	Route::post('getCOTteamRoleMapGraph','Admin\AdminReportController@getCOTteamRoleMapGraph'); 
	/*get tribeometer report graph*/
	Route::post('getReportTribeometerGraph','Admin\AdminReportController@getReportTribeometerGraph'); 
	/*get doagnostic report graph*/
	Route::post('getReportDiagnosticGraph','Admin\AdminReportController@getReportDiagnosticGraph'); 
	/*get motivational report graph*/
	Route::post('getReportMotivationalGraph','Admin\AdminReportController@getReportMotivationalGraph'); 
	/*get culture report graph*/
	Route::post('getReportCultureGraph','Admin\AdminReportController@getReportCultureGraph');
	/*get culture report graph*/
	Route::post('getReportFunctionalGraph','Admin\AdminReportController@getReportFunctionalGraph'); 

	/*get diagnostic sub report of graphs*/
	Route::post('getDiagnsticSubGraph','Admin\AdminReportController@getDiagnsticSubGraph')->name('getDiagnsticSubGraph'); 

	//Report sub diagnostic graph
	Route::post('getReportSubDiagGraph','Admin\AdminReportController@getReportSubDiagGraph')->name('getReportSubDiagGraph');

	//dashboard sub diagnostic graph
	Route::post('getDashboardSubDiagGraph','Admin\AdminDashboardController@getDashboardSubDiagGraph')->name('getDashboardSubDiagGraph');

	//dashboard DOT values graph
	Route::post('getDashboardDotValues','Admin\AdminDashboardController@getDashboardDotValues')->name('getDashboardDotValues');

	//dashboard thumbsup count graph
	Route::post('getDashboardThumbsupValues','Admin\AdminDashboardController@getDashboardThumbsupValues')->name('getDashboardThumbsupValues');

	/*get DOT bubble list for report*/
	Route::post('getDotThumbsUpBubbleList','Admin\AdminReportController@getDotThumbsUpBubbleList')->name('getDotThumbsUpBubbleList');

	// get happy index report yearly
	Route::post('getReportHappyIndexGraphYear','Admin\AdminReportController@getReportHappyIndexGraphYear'); 
	// get happy index report Monthly
	Route::post('getReportHappyIndexGraphMonth','Admin\AdminReportController@getReportHappyIndexGraphMonth'); 
	// get happy index report Weekly
	Route::post('getReportHappyIndexGraphWeek','Admin\AdminReportController@getReportHappyIndexGraphWeek'); 
	// get happy index report daily
	Route::post('getReportHappyIndexGraphDay','Admin\AdminReportController@getReportHappyIndexGraphDay'); 


	//Report index
	Route::post('dotTabData','Admin\AdminReportController@dotTabData');

	
	/*IOT FUNCTIONALITY*/

	
	Route::resource('iot','Admin\AdminIOTController');
	/*get iot dashboard*/
	Route::match(['get','post'],'iot-dashboard/{orgId}','Admin\AdminIOTController@getIotDeshboard');
	/*get feedback list*/
	Route::get('feedback-list/{orgId}/{status}/{officeId}','Admin\AdminIOTController@getFeedbackList');
	/*get feedback list from theme list */
	Route::get('feedback-list-from-theme-list/{orgId}/{themeId}','Admin\AdminIOTController@getFeedbackListFromThemeList');
	/*get create page of theme*/
	Route::get('add-theme','Admin\AdminIOTController@createTheme')->name('add-theme');
	/*get custom theme list*/
	Route::get('theme-list/{orgId}/{id}','Admin\AdminIOTController@getThemeList')->name('theme-list');
	/*get custom theme list from feedback list redirection*/
	Route::get('theme-list-from-feedback-list/{orgId}/{feedbackId}','Admin\AdminIOTController@getThemeListFromFeedbackList')->name('theme-list-from-feedback-list');
	/*store custom theme*/
	Route::post('store-theme','Admin\AdminIOTController@storeCustomTheme')->name('store-theme');
	/*get edit page of cutome theme*/
	Route::get('edit-theme/{orgId}/{id}','Admin\AdminIOTController@editCustomTheme')->name('edit-theme');
	/*update theme*/
	Route::post('update-theme','Admin\AdminIOTController@updateCustomTheme')->name('update-theme');
	/*get chat screen*/
	Route::get('get-chat/{orgId}/{feedbackId}','Admin\AdminIOTController@getChatMessages')->name('get-chat');
	/*send msg*/
	Route::post('send-chat-msg','Admin\AdminIOTController@sendChatMessages')->name('send-chat-msg');
	/*get msgs by ajax call*/
	Route::post('get-chat-messages-by-ajax','Admin\AdminIOTController@getChatMessagesByAjax')->name('get-chat-messages-by-ajax');
	/*update theme to feedback*/
	Route::post('update-feedback-theme','Admin\AdminIOTController@updateFeedbackTheme')->name('update-feedback-theme');
	/*get theme modal response */
	Route::post('get-theme-modal','Admin\AdminIOTController@getThemeModal')->name('get-theme-modal');
	/*get theme modal response */
	Route::get('complete-feedback/{id}','Admin\AdminIOTController@completeFeedback')->name('complete-feedback');
	/*add new theme by ajax when modal is open*/
	Route::post('add-new-theme-by-ajax','Admin\AdminIOTController@addNewThemeByAjax')->name('add-new-theme-by-ajax');
	/*get IOT actions*/
	Route::post('getActionsByAjax','Admin\AdminIOTController@getActionsByAjax')->name('get-actions-by-ajax');
	/*get IOT actions edit*/
	Route::post('getActionsByAjaxEdit','Admin\AdminIOTController@getActionsByAjaxEdit')->name('get-actions-by-ajax-edit');
	// Delete themes
	Route::post('delete-themes','Admin\AdminIOTController@deleteThemes');
	//Delete Feedbacks
	Route::post('delete-feedbacks','Admin\AdminIOTController@deleteFeedbacks');
	/*get theme from SWOT */
	Route::post('getThemesFromSWOT','Admin\AdminIOTController@getThemesFromSWOT');
	//Update Improvements
	Route::post('update-improvements','Admin\AdminIOTController@updateImprovements');
	//Risk register completed
	Route::get('risk-register','Admin\AdminIOTController@showRiskRegister');
	//Update Risk register
	Route::post('update-risk-register','Admin\AdminIOTController@updateRiskRegister');
	//add actions from feedback list
	Route::post('add-action-from-feedback','Admin\AdminIOTController@addActionFromFeedback');
	//Show submission feedback of themes
	Route::get('submission-feedback/{id}/{orgId}','Admin\AdminIOTController@showSubmissionFeedback');
	//add actions from Add theme page
	Route::post('add-action-from-add-theme','Admin\AdminIOTController@addActionFromAddTheme');
	//Responsible [person  list
	Route::post('responsiblePersonList','Admin\AdminIOTController@responsiblePersonList');
	//add risks from feedback list
	Route::post('add-risks-from-feedback','Admin\AdminIOTController@addRisksFromFeedback');
	
	
	/*Personality Type FUNCTIONALITY*/
	//Personality type questions
	Route::match(['get', 'post'],'personality-type-question','Admin\AdminPersonalityTypeController@index');
	//Personality type options
	Route::get('personality-type-option','Admin\AdminPersonalityTypeController@personalityTypeOption');
	//Personality type values
	Route::get('personality-type-value','Admin\AdminPersonalityTypeController@personalityTypeValue');
	//Add Personality type category
	Route::post('add-personality-type-category','Admin\AdminPersonalityTypeController@addPersonalityTypeValue');
	//Update Personality type category
	Route::post('update-personality-type-category','Admin\AdminPersonalityTypeController@updatePerTypeCategories');
	//Delete Personality type category
	Route::post('delete-personality-type-category','Admin\AdminPersonalityTypeController@deletePerTypeCategory');
	//Add Personality type option
	Route::post('add-personality-type-option','Admin\AdminPersonalityTypeController@addPersonalityTypeOption');
	//Delete Personality type option
	Route::post('delete-personality-type-option','Admin\AdminPersonalityTypeController@deletePersonalityTypeOption');
	//Update Personality type option
	Route::post('update-personality-type-option','Admin\AdminPersonalityTypeController@updatePersonalityTypeOption');
	//Add Personality type questions
	Route::post('add-personality-type-question','Admin\AdminPersonalityTypeController@addPersonalityTypeQuestion');
	//Update Personality Type Question
	Route::post('update-personality-type-question','Admin\AdminPersonalityTypeController@updatePerTypeQuestions');
	//Delete Personality Type Question
	Route::post('delete-personality-type-question','Admin\AdminPersonalityTypeController@deletePerTypeQuestions');


	/*SOT FUNCTIONALITY*/
	Route::resource('sot','Admin\AdminSOTController');
	/*get sot culture questions list */
	Route::get('sot-culture-questionnaire-list','Admin\AdminSOTController@getSOTquestionnaireList');
	/*sot detail page*/
	Route::get('sot-detail/{id}','Admin\AdminSOTController@sotHomeDetail');
	/*update sot-culture information question*/
	Route::post('update-sot-culture-question','Admin\AdminSOTController@updateCultureInfoQuestion');
	/*update sot-culture-structure-summary-value*/
	Route::post('update-sot-culture-structure-summary','Admin\AdminSOTController@updateCultureSummaryValue');
	/*sott culture structure image update*/
	Route::post('update-sot-culture-img','Admin\AdminSOTController@updateCultureStructureImag');

	/*list SOT Motivation users list*/
	Route::match(['get', 'post'],'sot-motivation-users/{id}','Admin\AdminSOTController@getSOTmotivationUserList');
	//update sot motivation detail
	Route::post('sot-update-motivation-users','Admin\AdminSOTController@updateSOTmotivationDetail');
	/*update SOT motivation starting date*/
	Route::post('updateSOTmotivationStartingDate','Admin\AdminSOTController@updateSOTmotivationStartingDate');
	/*sot Motivation  value list*/
	Route::get('sot-motivation-values','Admin\AdminSOTController@getSOTmotivationValuesList');
	/*update sot-motivation-values*/
	Route::post('update-sot-motivation-value','Admin\AdminSOTController@updateSOTmotivationValue');	
	/*get sot motivation questions list*/
	Route::get('sot-motivation-questions','Admin\AdminSOTController@getSOTmotivationQuestionnaireList');
	/*get sot motivation question option*/
	Route::get('sot-mot-que-option/{id}','Admin\AdminSOTController@getSOTmotivationQuesOptionList');
	/*update sot motivation questions option*/
	Route::post('update-sot-mot-que-opt','Admin\AdminSOTController@updateSOTmotivationQueOpt');
	/*update Culture Structure Title*/
	Route::post('update-sot-culture-structure-title','Admin\AdminSOTController@updateCultureStructureTitle');
	// Edit Sot questions
	Route::get('editSotQuestions/{id}','Admin\AdminSOTController@editSotQuestions');
	// Edit culture structure questions
	Route::post('editCultureStructureQuestions','Admin\AdminSOTController@editCultureStructureQuestions');
	// Add culture structure questions
	Route::post('addCultureStructureQuestions','Admin\AdminSOTController@addCultureStructureQuestions');
	//Add culture structure question title
	//Route::post('add-culture-structure-question-title','Admin\AdminSOTController@addCultureStructureQuestionTitle');
	//Delete Culture Structure Questionnaire
	Route::post('delete-culture-structure-questionnaire','Admin\AdminSOTController@deleteCultureStructureQuestionnaire');
	//Show page of add organisation Culture Structure 
	Route::get('showAddCultureStructureType','Admin\AdminSOTController@showAddCultureStructureType');
	//Crop Culture structure type image
	Route::post('cropCSTypeImg','Admin\AdminSOTController@cropCSTypeImg');
	//Add culture structure type summary and image
	Route::post('addCultureStructureTypeSummary','Admin\AdminSOTController@addCultureStructureTypeSummary');
	//Delete Culture Structure Summary
	Route::post('delete-culture-structure-summary','Admin\AdminSOTController@deleteCultureStructureSummary');
	//Add Culture Structure Summary
	Route::post('add-culture-structure-summary','Admin\AdminSOTController@addCultureStructureSummary');
	//Delete Culture Structure Type
	Route::post('delete-culture-structure-type','Admin\AdminSOTController@deleteCultureStructureType');
	//Delete Culture Structure Option
	Route::post('delete-culture-structure-option','Admin\AdminSOTController@deleteCultureStructureOption');
	//Delete Culture Structure Option Finally
	Route::post('delete-culture-structure-option-final','Admin\AdminSOTController@deleteCultureStructureOptionfinal');
	// Add Sot questions
	Route::get('addSotQuestions','Admin\AdminSOTController@addSotQuestions');
	//Show page of Edit Culture Structure Type
	Route::get('showEditCulturestructureType/{id}','Admin\AdminSOTController@showEditCulturestructureType');
	//Update culture structure type summary and image
	Route::post('updateCultureStructureTypeSummary','Admin\AdminSOTController@updateCultureStructureTypeSummary');


	/*DIAGNOSTIC FUNCTIONALITY*/
	Route::resource('diagnostic','Admin\AdminDiagnosticController');
	/*get diagnostic option list*/
	Route::get('diagnostic-option','Admin\AdminDiagnosticController@getDiagnosticOptionList');
	/*update diagnostic options*/
	Route::post('update-dia-opt','Admin\AdminDiagnosticController@upadateDiagnosticOption');
	/*get diagnostinc categories list*/
	Route::get('diagnostinc-cat-values','Admin\AdminDiagnosticController@getDiagnosticValuesList');
	/*update diagnostic category values */
	Route::post('updateDiagnosticCategoryValues','Admin\AdminDiagnosticController@updateDiagnosticCategoryValues');


	/*TRIBEOMETER FUNCTIONALITY*/
	Route::resource('tribeometer','Admin\AdminTribeometerController');
	/*get tribeometer option list*/
	Route::get('tribeometer-option','Admin\AdminTribeometerController@getTribeometerOptionList');
	/*update tribeometer options*/
	Route::post('update-tri-opt','Admin\AdminTribeometerController@upadateTribeometerOption');
	/*get tribeometer categories list*/
	Route::get('tribeometer-cat-values','Admin\AdminTribeometerController@getTribeometerValuesList');
	/*update tribeometer category values */
	Route::post('updateTribeometerCategoryValues','Admin\AdminTribeometerController@updateTribeometerCategoryValues');

	Route::get('pdf/{id}','Admin\AdminReportController@showPdfReport');

	Route::post('exportPdf','Admin\AdminReportController@exportPdf');
	Route::get('exportPdf','Admin\AdminReportController@exportPdf');

	/*PERFORMANCE FUNCTIONALITY*/	
	Route::resource('performance','Admin\AdminPerformanceController');
	/*get performace*/
	Route::match(['get','post'],'performace-detail','Admin\AdminPerformanceController@getPerformance')->name('performace-detail');

	//crop image
	Route::post('cropOrganizationImg','Admin\AdminOrganisationController@cropOrganizationImg');
});