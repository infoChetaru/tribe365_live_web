<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" >
<head>
  <title>Tribe 365 </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/images/Favicon-icon.png')}}"/>
  <link rel="stylesheet" type="text/css" href="{{asset('public/css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{asset('public/css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{asset('public/css/responsive.css') }}">
  <script type="text/javascript" src="{{ asset('public/js/bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('public/js/jquery.min.js') }}"></script>
  <style type="text/css">

  section.loging-section {
    background: url({{ asset('public/images/bg-img.jpg') }})no-repeat center center; 
}
.login-img {
  background:url({{ asset('public/images/round-img.png') }})no-repeat right center,url({{ asset('public/images/round-bg-img.jpg') }})no-repeat center;
}
.loing-form .form input[type="email"]{
  background:url({{ asset('public/images/use-icon.png') }})no-repeat 34px center;
}
.loing-form .form input[type="password"]{
  background:url({{ asset('public/images/key-icon.png') }})no-repeat 34px center;
}
.sidebar-menu ul li a { 
  background: url({{ asset('public/images/nav-arrow.png') }}) no-repeat 88% center;
}
.profile-user-list span {
  background: url({{ asset('public/images/user-drop-icon.png') }}) no-repeat right 6px;
}

</style>
</head>
<body>
  <section class="loging-section">    
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="login-content">
            <div class="login-img">
            </div>
            <div class="loing-form Reset-pass">
              <div class="loing-box">
                <div class="logo-img">
                  <img src="{{ asset('public/images/login-logo.png') }}">
                </div>
                <div class="forget-text">
                  <h2>Reset Password</h2>
                  <p>Create new password by entering a new password below.</p>
                </div>
                <div class="form">

                 {!!Form::model($data,['method' => 'POST','url' =>  $data['token'], 'files' => true, 'class'=>'form-horizontal form-label-left', ])!!}

                 @if (count($errors) > 0)
                 <div class="alert alert-danger">           
                  @foreach ($errors->all() as $error)
                  <strong>{{ $error }}</strong>
                  @endforeach                
                </div>
                @endif

                @if(session('message'))                
                <div class="alert alert-success">         
                  <strong>{{session('message')}}</strong>
                </div>
                @endif

                <div class="form-group">  
                 {!!Form::hidden('email',$data['email'],['class'=>'form-control col-md-12 col-xs-12','required'=>'required','readonly'=>'readonly'])!!}
               </div>
               <div class="form-group"> 
                {!!Form::password('password',['class'=>'validate form-control col-md-12 col-xs-12','required'=>'required','placeholder'=>'Enter new password'])!!}
              </div>
              <div class="form-group">  
               {!!Form::password('confirm_password',['class'=>'validate form-control col-md-12 col-xs-12','required'=>'required','placeholder'=>'Re-enter new password'])!!}
             </div>
             <button type="submit" class="btn btn-primary">Reset Password</button>

             {!! Form::close() !!}
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</section>
<script type="text/javascript">
  $(function() {
    $('.validate').on('keypress', function(e) {
      if (e.which == 32)
        return false;
    });
  });
</script>
</body>
</html>