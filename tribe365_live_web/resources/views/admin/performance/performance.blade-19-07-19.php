<!-- header -->
@section('title', 'Performance')
@include('layouts.adminHeader')

<!-- working area -->

<div class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="performance">

				<form id="form-id" action="{{route('performace-detail')}}" method="POST">
					{{csrf_field()}}
					<div class="performance-option">
						<ul>
							<li> 
								<select name="orgId"> 
									<option value="">All Organisation</option>
									@foreach($organisations as $value)
									<option {{($orgId==$value->id)?'Selected':'' }} value="{{$value->id}}">{{$value->organisation}}</option>
									@endforeach()
								</select>
							</li>
							<li> 
								<select id="reportType" name="reportType" class="reportType"> 
									<option value="" selected>All</option>
									<option {{($reportType=='daily')?'Selected':'' }} value="daily">Daily</option>
									<option {{($reportType=='monthly')?'Selected':'' }} value="monthly" class="monthly" value="">Monthly</option>
								</select>
							</li>
							@if(empty($date))
							<li> 
								<input name="date" class="date date_field numberControl" type="text" autocomplete="off"/>
							</li>
							@else
							<li> 
								<input value="{{$date}}" name="date" class="date date_field numberControl" type="text" autocomplete="off"/>
							</li>
							@endif
							
							<li> 
								<button onclick="validation();" type="button" class="search-btn">Search</button>
							</li>
							<div class="error-message" style="display: none;">
								<span id="resp"></span>
							</div>
						</ul>
					</div>					
				</form>

				<div class="performance-list">
					<div class="row">
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<h2> {{$resultArray['thumbCompleted']}} </h2>
									<strong>Thumps Up/Person </strong>
								</div>
								<a href="#" class="notification">
									Send Notification
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">								
									<h2> {{$resultArray['improvements']}} </h2>				
									<strong>Improvements Sent/Person</strong>
								</div>
								<a href="#" class="notification">
									Send Notification
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<h2> {{$resultArray['dotCompleted']}}% </h2>
									<strong>DOT Values Complete</strong>
								</div>
								<a href="#" class="notification">
									Send Notification
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<h2> {{$resultArray['dotUpdated']}}% </h2>
									<strong>DOT Values Updated</strong>
								</div>
								<a href="#" class="notification">
									Send Notification
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<h2> {{$resultArray['tealRoleMapCompleted']}}% </h2>
									<strong>Team Roles Complete</strong>
								</div>
								<a href="#" class="notification">
									Send Notification
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<h2> {{$resultArray['tealRoleMapUpdated']}}% </h2>
									<strong>Team Roles Updated</strong>
								</div>
								<a href="#" class="notification">
									Send Notification
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<h2> {{$resultArray['personalityTypeCompleted']}}% </h2>
									<strong>Personality Types Complete</strong>
								</div>
								<a href="#" class="notification">
									Send Notification
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<h2> {{$resultArray['personalityTypeUpdated']}}% </h2>
									<strong>Personality Types Updated</strong>
								</div>
								<a href="#" class="notification">
									Send Notification
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<h2> {{$resultArray['cultureStructureCompleted']}}% </h2>
									<strong>Organisation Structure Complete</strong>
								</div>
								<a href="#" class="notification">
									Send Notification
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">									
									<h2> {{$resultArray['cultureStructureUpdated']}}% </h2>		
									<strong>Organisation Structure Updated</strong>
								</div>
								<a href="#" class="notification">
									Send Notification
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">									
									<h2> {{$resultArray['motivationCompleted']}}% </h2>		
									<strong>Motivation Complete</strong>
								</div>
								<a href="#" class="notification">
									Send Notification
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">									
									<h2> {{$resultArray['motivationCountUpdated']}}% </h2>	
									<strong>Motivation Updated</strong>
								</div>
								<a href="#" class="notification">
									Send Notification
								</a>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
<!-- footer -->
@include('layouts.adminFooter')

<script type="text/javascript">
	function validation()
	{

		var reportType = $('#reportType').val();
		var date = $('.date').val();
		console.log(reportType)
		console.log(date)

		if(reportType=='monthly')
		{
			if(!date)
			{
				$('.error-message').show();
				$('.date').css('border-color', 'red');
				$('#resp').html('');
				$('#resp').html('Please select month.');
			}
			else
			{
				$('.search-btn').prop("disabled", true);
				$('#form-id').submit();
			}
		}
		else
		{
			$('.search-btn').prop("disabled", true);
			$('#form-id').submit();
		}
	}
</script>

<script type="text/javascript">
	var currentTime = new Date(); 
	var minDate = new Date(currentTime.getYear(), currentTime.getMonth()-1); 
	var maxDate =  new Date(currentTime.getFullYear(),currentTime.getMonth()-1); 
	$(".date_field").datepicker({
		changeMonth: true,
		changeYear: true,
		todayHighlight:true,
		dateFormat: 'MM-yy',
		autoclose:true,			
		minDate: minDate, 
		maxDate: maxDate, 
	});
</script>

<script type="text/javascript">
	$('.date_field').hide();

	<?php 
	if($reportType=='monthly')
	{		
		echo "$('.date_field').show();";
	}
	?>
	$(document).on('change','.reportType',function(){

		if ($(this).val() == 'monthly')
		{
			$('.date_field').val('')			
			$(".date_field").show();
		}
		else if ($(this).val() == 'daily')
		{	
			$('.date_field').hide();
		}
		else if ($(this).val() == '')
		{	
			$('.date_field').hide();
		}

	});
</script>

<script type="text/javascript">
	$(document).on("keypress keyup blur",".numberControl",function (event) {    
		$(this).val($(this).val().replace(/[^\d].+/, ""));
		if ((event.which < 48 || event.which > 57)){
			event.preventDefault();
		}
	});
</script>