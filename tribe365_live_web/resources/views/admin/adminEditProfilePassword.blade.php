@section('title', 'Admin Profile')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">

			<div class="dot-section">
				<form id="form" action="{{URL::to('admin/profile-password-update')}}" method="post" enctype="multipart/form-data">
					{{csrf_field()}}
					
					<h2> Change Password </h2>
					<div class="form-section">
						<div class="row">	

							<input  type="hidden" name="email" value="{{$user->email}}">

							<div class="col-md-6">	

								<div class="form-group">
									<input class="" id="currentpsw" type="password" name="currentpsw" placeholder="Current password" autocomplete="off" maxlength="50" >
								</div>

								<div class="form-group">
									<input class="validate" id="newpsw" type="password" name="newpsw" placeholder="New password" autocomplete="off" maxlength="50">
								</div>

								<div class="form-group">
									<input class="validate" id="confirmPsw" type="password" name="confirmPsw" placeholder="Confirm password" autocomplete="off" maxlength="50">
								</div>

							</div>	


						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="Create-btn-cont">
									<button type="button" onclick="validation()" class="btn">Save</button>
								</div>
							</div>
						</div>						
					</form>
					
				</div>
			</div>			
			<div class="error-message" style="display: none;">
				<span id="resp"></span>
			</div>		
			@if(count($errors)>0)
			<div class="error-message message">
				<span id="resp">{{$errors}}</span>
			</div>
			@endif

			@if(session('message'))
			<div class="success-message message">
				<span id="resp">{{session('message')}}</span>
			</div>
			@endif

		</div>
	</div>
</main>

<script type="text/javascript">
	function validation()
	{
		$('.error-message').show();

		if(!$('#currentpsw').val()){

			$('#resp').html('');
			$('#resp').html('Please enter Current Password.');
		}else if(!$('#newpsw').val()){

			$('#resp').html('');
			$('#resp').html('Please enter New Password.');
		}else if(!$('#confirmPsw').val()){

			$('#resp').html('');
			$('#resp').html('Please enter Confirm Password.');
		}else if($('#newpsw').val()!=$('#confirmPsw').val()){

			$('#resp').html('');
			$('#resp').html('New and Current password are not same.');
		}else{
			$('#form').submit();
		}

	}
</script>
<script type="text/javascript">
	$(function() {
		$('.validate').on('keypress', function(e) {
			if (e.which == 32)				
			return false;
		});
	});
</script>
@include('layouts.adminFooter')

