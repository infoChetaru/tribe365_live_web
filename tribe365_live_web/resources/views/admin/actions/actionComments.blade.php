<!-- header -->
@section('title', 'Action Comment')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section organization-fild view-coment">
		<div class="container">		
			<div class="row">
				<div class="organization-section">
					@foreach($actions as $action)
					<div class="col-md-6">
						<div class="company-section">
							<strong class="id-text">{{$action->id}}</strong>
							<div class="view-user">							
								<h2>
									@php 

									if(!empty($action->office))
									{
										echo $action->office;
									}
									else if(!empty($action->department))
									{
										echo $action->department;
									}
									else
									{
										echo $action->cname." ".$action->clastName;
									}
									@endphp
								</h2>
							</div>
							<div class="comments-text">
								<div class="toltip-section">
									<span class="hide-text">{{ucfirst($action->description)}}</span>
									<div class="toltip-box">{{ucfirst($action->description)}}</div>
								</div>
							</div>
							<div class="status-box">
								<div class="date-box">
									<span class="date-tital"> Start Date : </span>
									<span class="date"> {{date('d-m-Y', strtotime($action->startedDate))}} </span>
								</div>
								<div class="date-box">
									<span class="date-tital"> Due Date : </span>
									<span class="date"> {{date('d-m-Y', strtotime($action->dueDate))}} </span>
								</div>
								<div class="status">
									<span class="status-tital"> Status : </span>
									@if($action->orgStatus=='Not Started')
									<div class="view-status not-started">
										Not Started 
									</div>
									@elseif($action->orgStatus=='Started')
									<div class="view-status started">
										Started 
									</div>
									@elseif($action->orgStatus=='Completed')
									<div class="view-status complete">
										Completed 
									</div>
									@endif
								</div>

								<div class="date-box">
									<span class="status-tital"> Responsible :</span>
									<span class="date">{{ucfirst($action->rname)." ".ucfirst($action->rlastName)}}  </span>
								</div>
								<div class="date-box">
									<span class="date-tital"> Risks : </span>	
									<span class="date"> {{$action->themeTitle}}</span>
								</div>
								<div class="date-box">
									<span class="date-tital"> Submissions : </span>
									<span class="date">{{$action->feedbackTitle}}</span>
								</div>
							</div>
						</div>							
					</div>
				</div>	
				@endforeach				
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="conment-serch">

						<form id="form" action="{{URL::to('admin/action-add-comment')}}" method="POST">
							{{csrf_field()}}
							<input type="hidden" name="actionId" value="{{$action->id}}">
							<input class="charCount" id="comment" type="text" name="comment" placeholder="Type your comments">
							<button type="button" onclick="validation();" class="btn save"> Send </button>
							<div class="error-message" style="display: none;">
								<span id="resp"></span>
							</div>
						</form>						
						<h2>Comments </h2>
					</div>
					<div class="organization-section coment-list">
						<div class="company-section">
							@foreach($actionsComments as $value)
							<div class="comments-box">
								<div class="view-user">									
									<h2> {{ucfirst($value->name)}} </h2>
								</div>
								<div class="date-time">
									<span class="date">{{date('M-d-Y', strtotime($value->created_at))}} </span>
									<span class="time">{{date('H:i:A', strtotime($value->created_at))}} </span>
								</div>
								<div class="comments-text">
									<p> {{ucfirst($value->comment)}}</p>
								</div>
							</div>
							@endforeach
						</div>						
					</div>
				</div>
			</div>
		</div>

	</div>
</main>

<script type="text/javascript">
	function validation()
	{
		$('.error-message').show();
		$('.save').prop("disabled", true);

		var comment = $('#comment').val();
		
		var empty = false;
		$('.charCount').each(function() {
			if ($(this).val().length > 250){
				empty = true;
			}
		});

		if (empty){

			$('.save').prop("disabled", false);
			$('#resp').html('');
			$('#resp').html('All input fields should be less than 250 characters.');
		}else if($.trim($('#comment').val()) == ''){

			$('.save').prop("disabled", false);
			$('#resp').html();
			$('#resp').html('Please enter Comment.');
		}else{
			
			$('.save').prop("disabled", true);
			$('#form').submit();
		}
	}
</script>
@include('layouts.adminFooter')