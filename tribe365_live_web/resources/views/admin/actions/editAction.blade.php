<!-- header -->
@section('title', 'Edit Action')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="fild-stats-cont contact-cont dot-action">
						<h2> Edit Action</h2>
						<div class="add-content">
							
							{{Form::model($actions, ['method' => 'PUT', 'route' => array('action.update', base64_encode($actions->id)),'id'=>'form'])}}

							<input type="hidden" name="orgId" value="{{base64_encode($actions->orgId)}}">
							
							<div class="row ">
								<div class="col-md-6">
									<div class="form-group">
										<select id="responsible" name="responiblityValue" class="form-control" required>
											<option value="" disabled="" selected="">Action Tier</option>
											@foreach($actionResponsibles as $value)
											<option {{($actions->responsibleId==$value->id)?'selected':''}} value="{{$value->id}}">{{$value->name}}</option>
											@endforeach
										</select>
										<span class="select-arrow"><img src="{{asset('public/images/select-arrow.png')}}"></span>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<select id="status" name="status" type="" class="form-control required">
											<option value="" disabled="" selected="">Status</option>
											<option {{($actions->orgStatus=='Not Started')?'selected':''}} value="Not Started">Not Started</option>
											<option {{($actions->orgStatus=='Started')?'selected':''}} value="Started">Started</option>
											<option {{($actions->orgStatus=='Completed')?'selected':''}} value="Completed">Completed</option>
										</select>
										<span class="select-arrow"><img src="{{asset('public/images/select-arrow.png')}}"></span>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<input id="start_date" name="start_date" type="text" class="form-control" value="{{date('m/d/Y', strtotime($actions->startedDate))}}" placeholder="Start Date" readonly="">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<input id="end_date" name="end_date" type="text" class="form-control" value="{{date('m/d/Y', strtotime($actions->dueDate))}}" placeholder="Due Date" readonly="">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">								
										<div class="select-mnth">						
											<select id="theme" name="themeId[]" multiple="multiple">
												@foreach($themeList as $themes)
												<option {{(in_array($themes->id, $selectedThemeArr))?'selected':''}} value="{{$themes->id}}">{{ucfirst($themes->title)}}</option>
												@endforeach()
											</select>							
										</div>
										<input type="hidden" name="themeUpdate" value="1">	
										<form id="new-theme-form">				
											<div id="new-theme-input" style="display: none;">
												<input type="text" id="theme-title" name="theme_title"> 	
												<button id="add-new-theme" type="button"> Add </button>
											</div>
										</form>
										
										<div class="add-selected-themes-here"></div>
									</div>
								</div>
								<div class="col-md-6 resUsers" >
									<div class="form-group">
										<select id="resUsers" name="resUsers" class="form-control" required>
											<option value="" selected="" disabled>Responsible Person</option>
											@foreach($resUsers as $rValue)
											<option {{($actions->responsibleUserId== $rValue->id)?'selected':''}} value="{{$rValue->id}}">{{ucfirst($rValue->name)." ".ucfirst($rValue->lastName)}}</option>
											@endforeach
										</select>
										<span class="select-arrow"><img src="{{asset('public/images/select-arrow.png')}}"></span>
									</div>
								</div>

								<div class="col-md-6 add-ofc-here"></div>
								<div class="col-md-6 new-resUsers"></div>

								<div class="col-md-6">
									<div class="form-group">								
										<div class="select-mnth">						
											<select id="feedback" name="feedbackId[]" multiple="multiple">
												@foreach($feedbackList as $feedbacks)
												<option {{(in_array($feedbacks->id, $selectedFeedbackArr))?'selected':''}} value="{{$feedbacks->id}}">{{ucfirst($feedbacks->id)}}</option>
												@endforeach()
											</select>							
										</div>
									</div>
								</div>

								<div class="form-group description">
									<textarea id="description" placeholder="Description" name="description" class="form-control">{{ucfirst($actions->description)}}</textarea>
								</div>
							</div>

							<div id="modal-data"></div>

							<button type="button" onclick="validation();" class="btn"> Submit </button>
							
							{{Form::close()}}

						</div>
					</div>
				</div>
			</div>				
		</div>
	</div>

	<div class="error-message success" style="display: none;">
		<span id="resp"></span>
	</div>
</main>

@include('layouts.adminFooter')

<script type="text/javascript">
	$(function () {
		$('#theme').multiselect({
			includeSelectAllOption: true,
			enableFiltering:false,
			nonSelectedText: 'Select risks',
		});		
		$('#feedback').multiselect({
			includeSelectAllOption: true,
			enableFiltering:false,
			nonSelectedText: 'Select submissions',
		});			
	});
</script>

<!-- add new theme by ajax when modal is open -->
<script type="text/javascript">
	$('#add-new-theme-show').on('click',function(){
		$('#new-theme-input').show();
	});
</script>

<script type="text/javascript">
	$('#add-new-theme').on('click',function(){
		
		var orgId      = "{{$actions->orgId}}";
		var themeTitle = $('#theme-title').val();

		$.ajax({
			type: "POST",
			url: "{{route('add-new-action-theme-by-ajax')}}",
			data: {orgId:orgId,themeTitle:themeTitle,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				var obj           = JSON.parse(response);
				var htmlContainer = obj.htmlContainer;
				var htmlOption    = obj.htmlOption
				
				$('#new-theme-input').hide();
				$('#theme').html('');
				$('.multiselect-container').html('');
				$('#theme').append(htmlOption);
				$('.multiselect-container').append(htmlContainer);			
			}
		});
	});
</script>

<!-- modal button  -->
<button style="display: none;" type="button" class="modal-btn" data-toggle="modal" data-target="#myModal"></button>

<script type="text/javascript">
	$(document).ready(function(){
		$('.create-btn').click(function(){     
			$('#modal-data').html('');    
			var orgId = $(this).data('id');			
			$('#orgId').val(orgId);

			var selectedThemeArr = <?php print_r(json_encode($selectedThemeArr)); ?>;

			$.ajax({
				type: "POST",
				url: "{{route('get-action-theme-modal')}}",
				data: {orgId:orgId,selectedThemeArr:selectedThemeArr,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					$('#modal-data').append(response);
					$('.modal-btn').click();
				}
			});
		});
	});
</script>

<script type="text/javascript">
	$(function () {
		$("#start_date").datepicker({

			onSelect: function (selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate());
				$("#end_date").datepicker("option", "minDate", dt);
			}
		});

		$("#end_date").datepicker({			
			
			onSelect: function (selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate());
				$("#start_date").datepicker("option", "maxDate", dt);
			}
		});
	});
</script>

<script type="text/javascript">
	function validation()
	{
		$('.error-message').show();

		var responsible  = $('#responsible').val();
		var start_date   = $('#start_date').val();
		var end_date     = $('#end_date').val();				
		var description  = $('#description').val();
		var status       = $('#status').val();
		var resUsers     = $('#resUsers').val();

		if(responsible=='' || responsible==null)
		{
			$('#resp').html();
			$('#resp').html('Please select Action Tier.');
		}
		else if(status=='' || status==null)
		{
			$('#resp').html();
			$('#resp').html('Please select Action Status.');
		}
		else if(start_date =="" || status==null)
		{
			$('#resp').html();
			$('#resp').html('Please select Start Date.');
		}
		else if(end_date =="" || end_date==null)
		{
			$('#resp').html();
			$('#resp').html('Please select End Date.');
		}
		else if(!resUsers || resUsers==null)
		{
			$('#resp').html();
			$('#resp').html('Please select Responsible Person.');
		} 		
		else if(description =="" || description==null)
		{
			$('#resp').html();
			$('#resp').html('Please enter Description.');
		} 		
		else
		{
			$('.error-message').hide();
			$('#form').submit();
		}
	}
</script>

<script type="text/javascript">

	$('#responsible').on('change', function() {
		$('#remove').remove();
		$('#remove1').remove();
		var tierId = $('#responsible').val();
		$('.resUsers').remove();
		$('.add-ofc-here').show();

		if(tierId == 4)
		{			
			var urlTag ="{!!URL::to('admin/get-offices')!!}";
		}
		else if(tierId==5)
		{			
			var urlTag ="{!!URL::to('admin/get-department')!!}";
		}		
		else if(tierId==1 || tierId==2 || tierId==3 || tierId==6)
		{
			$('.add-ofc-here').hide();
			var urlTag ="{!!URL::to('admin/get-users')!!}";

			$.ajax({
				type: "POST",
				url: "{!!URL::to('admin/get-responsible-users')!!}",				
				data: {section:'',id:'',orgId:'<?php echo $id ?>',"_token":'<?php echo csrf_token()?>'},
				success: function(response) {
					
					$('.new-resUsers').append('<div id="remove1" class="form-group">'+response+'<span class="select-arrow"><img src="{{asset("public/images/select-arrow.png")}}"></span></div>');
				}
			});
			return false;
		}
		
		$.ajax({
			type: "POST",
			url: urlTag,				
			data: {orgId:'<?php echo $id ?>',"_token":'<?php echo csrf_token()?>'},
			success: function(response) {
				
				$('.add-ofc-here').append('<div id="remove" class="form-group">'+response+'<span class="select-arrow"><img src="{{asset("public/images/select-arrow.png")}}"></span></div>');
			}
		});

	});


	$('#form').on('change','#office', function() {
		$('#remove1').remove();
		var officeId = $('#office').val();

		
		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/get-responsible-users')!!}",				
			data: {section:'office',id:officeId,orgId:'<?php echo $id ?>',"_token":'<?php echo csrf_token()?>'},
			success: function(response) {
				// console.log(response);
				
				$('.new-resUsers').append('<div id="remove1" class="form-group">'+response+'<span class="select-arrow"><img src="{{asset("public/images/select-arrow.png")}}"></span></div>');
			}
		});
	});

	$('#form').on('change','#department', function() {
		$('#remove1').remove();
		var departmentId = $('#department').val();
		
		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/get-responsible-users')!!}",				
			data: {section:'department',id:departmentId,orgId:'<?php echo $id ?>',"_token":'<?php echo csrf_token()?>'},
			success: function(response) {
				// console.log(response);
				
				$('.new-resUsers').append('<div id="remove1" class="form-group">'+response+'<span class="select-arrow"><img src="{{asset("public/images/select-arrow.png")}}"></span></div>');
			}
		});
	});
</script>