<!-- header -->
@section('title', 'List Action')
@include('layouts.adminHeader')

<main class="main-content">
	<div class="add-fild-section organization-fild view-action">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="search-section">						
						<select id="tier-filter" name="tier-filter" class="form-control" onchange="filter_tier();" style="width: 20%; float: right;background:url({{asset('public/images/down-arrow.png')}})no-repeat 95% center;">
							<option value="">Select All Tiers</option>
							<option value="Primary">Primary</option>
							<option value="Secondary">Secondary</option>
							<option value="Tertiary">Tertiary</option>
							<option value="Office">Office</option>
							<option value="Department">Department</option>
							<option value="Individual">Individual</option>	
						</select>
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">
								{{session('message')}}
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="organization-section">
					@php $count=1; @endphp
					@foreach($actions as $action)			

					<div class="col-md-4 {{$action->tier}} alltier">
						<div class="company-section">
							
							<strong class="id-text">{{$action->id}}</strong>
							
							<div class="view-user">								
								<h2>
									@php 
									if(!empty($action->office) && $action->responsibleId==4)
									{
										echo $action->office;
									}
									else if(!empty($action->department) && $action->responsibleId==5)
									{
										echo $action->department;
									}
									else
									{
										echo $action->cname." ".$action->clastName;
									}
									@endphp
								</h2>

							</div>
							<div class="cmt-box">								
								<span class="date"><a href="{{URL::to('admin/action-comments')}}/{{base64_encode($action->id)}}"><img src="{{asset('public/images/comment.png')}}"></a></span>			
							</div>
							<div class="info-icon">
								<a href="{!!route('action.edit',['id'=>base64_encode($action->id)])!!}"><i data-toggle="modal" data-toggle="tooltip" title="Edit" class="fa fa-edit" style="font-size:22px;color:red"></i></a>
							</div>

							<div class="delete-icon">
								<a onclick="return confirm('Are you sure you want to delete ?')" href="{{URL::to('admin/delete-action/'.base64_encode($action->id))}}"><i data-toggle="modal" data-toggle="tooltip" title="Delete" class="fa fa-trash" style="font-size:22px;color:red"></i></a>
							</div>
							
							<div class="comments-text">
								<div class="toltip-section">
									<span class="hide-text">{{ucfirst($action->description)}}</span>
									<div class="toltip-box">{{ucfirst($action->description)}}</div>
								</div>
							</div>
							<div class="status-box">
								<div class="date-box">
									<span class="date-tital"> Start Date : </span>
									<span class="date"> {{date('d-m-Y', strtotime($action->startedDate))}} </span>
								</div>
								<div class="date-box">
									<span class="date-tital"> Due Date : </span>
									<span class="date"> {{date('d-m-Y', strtotime($action->dueDate))}} </span>
								</div>

								<div class="date-box">
									<span class="status-tital"> Status : </span>
									<input id="actionId{{$count}}" type="hidden" name="actionId" value="{{$action->id}}">
									@if(Auth::user()->id==$action->userId)
									<select class="form-control" id="status{{$count}}" name="status" style="margin-left:54px;width:179px;background:url({{asset('public/images/down-arrow.png')}})no-repeat 95% center;">
										<option class="view-status not-started" {{($action->orgStatus=='Not Started')?'selected':''}} value="Not Started" >Not Started</option>
										<option class="view-status started" {{($action->orgStatus=='Started')?'selected':''}} value="Started" >Started</option>
										<option class="view-status complete" {{($action->orgStatus=='Completed')?'selected':''}} value="Completed" >Completed</option>
									</select>
									@else

									@if($action->orgStatus=='Not Started')
									<div class="view-status not-started">
										Not Started
									</div>

									@elseif($action->orgStatus=='Started')
									<div class="view-status started">
										Started
									</div>
									@elseif($action->orgStatus=='Completed')
									<div class="view-status complete">
										Complete
									</div>
									@endif
									@endif
								</div>								
								<div class="date-box">
									<span class="date-tital"> Responsible : </span>
									<span class="date">{{ucfirst($action->rname)." ".ucfirst($action->rlastName)}}  </span>
								</div>
								<div class="date-box">
									<span class="date-tital"> Risks : </span>
									<span class="date">{{$action->themeTitle}}</span>
								</div>
								<div class="date-box">
									<span class="date-tital"> Submissions : </span>
									<span class="date">{{$action->feedbackTitle}}</span>
								</div>
								<!-- <div class="date-box">
									<span class="date-tital"> Risks : </span>
									<div class="toltip-section">
										<span class="hide-text"> {{$action->themeTitle}}</span>
										<div class="toltip-box"> {{$action->themeTitle}}</div>
									</div>
								</div> -->
								<!-- <div class="date-box">
									<span class="date-tital"> Submissions : </span>
									<div class="toltip-section">
										<span class="hide-text"> {{$action->feedbackTitle}}</span>
										<div class="toltip-box"> {{$action->feedbackTitle}}</div>
									</div>
								</div> -->

							</div>
						</div>
					</div>	

					@php $count++ @endphp
					@endforeach	

				</div>
				<div id="resp" align="center" style="width:100%" class="text-danger" role="alert"></div>
			</div>
		</div>

		
	</div>

	<div class="add-plus-icon" style="    position: fixed;">
		<div class="add-plus-upper">
			<a href="{{URL::to('admin/add-action').'/'.$id}}"><img src="{{asset('public/images/plus-icon.png')}}">
			</a>						
		</div>
	</div>
</main>
@include('layouts.adminFooter')

@php $count=1; @endphp
@foreach($actions as $value)

<script type="text/javascript">
	$('#status{{$count}}').on('change', function() {

		var status   = $('#status{{$count}}').val();
		var actionId = $('#actionId{{$count}}').val();

		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/action-status-update')!!}",				
			data: {status:status,actionId:actionId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{
				location.reload();
				console.log(response);
			}
		});
	});

	function filter_tier()
	{
		var val   = $('#tier-filter').val();
		$('#resp').html('')

		if(val == "")
		{
			$('.alltier').show();
		}
		else
		{						
			var hasClass = $('.'+val).hasClass(val).toString();

			if(hasClass=='false')
			{
				$('#resp').html('No result found.');
				$('.alltier').hide();
			}
			else
			{				
				$('.alltier').hide();
				$('.'+val).show();
			}
		}
	}
</script>

@php $count++ ;@endphp
@endforeach