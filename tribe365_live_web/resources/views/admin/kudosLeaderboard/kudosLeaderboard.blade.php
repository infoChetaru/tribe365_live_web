@section('title', 'Kudos Leaderboard')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section dot cot-functional">
		<div class="container">  
			<div class="Reports-tital">
				<div class="search-cot">				
					<form id="kudos-leaderboard-form" action="#" method="POST">
						{{csrf_field()}}					
						<select id="kudos-org" name="orgId"> 
							@foreach($organisations as $value)
								<option {{($orgId==$value->id)?'Selected':'' }} value="{{$value->id}}">{{$value->organisation}}</option>
							@endforeach()
						</select>
						<select id="kudos-office" name="officeId">
							<option value="" selected>All Offices</option>
							@foreach($offices as $office)
								<option {{($officeId==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
							@endforeach
						</select>
						<select id="kudos-department" name="departmentId">			
							@if($departments && !empty($officeId))
								<option value="">All Department</option>
								@foreach($departments as $cdept)
									<option {{($departmentId==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
								@endforeach
								@elseif($all_department)
									<option value="">All Department</option>
								@foreach($all_department as $dept)
									<option {{($departmentId==$dept->id)?'selected':''}} value="{{$dept->id}}">{{$dept->department}}</option>
								@endforeach	
							@endif
						</select>
						<select id="kudos-year" name="year">
							@foreach($years as $yearVal)
								<option {{($year==$yearVal)?'selected':''}} value="{{$yearVal}}">{{$yearVal}}</option>
							@endforeach	
						
						</select>
						<select id="kudos-month" name="month">
							@foreach($months as $monthKey => $monthVal)
								<option {{($month==$monthKey)?'selected':''}} value="{{$monthKey}}">{{$monthVal}}</option>
							@endforeach		
						</select>
						<button type="button" class="kudos-leaderboard-form" style="width: auto;">Search</button>
					</form> 
				</div>
				<div class="loader-img dashboard-loader" style="display:none; width: 70px;">
					<img src="{{asset('public/images/loader.gif')}}">
				</div>
				<div class="chart_group">
				<div class="kudos-leader">
				<div class="kudos-leader-inner">
					<h2> Kudo Leaderboard</h2>
					@if(!empty($kudosFinal))
						<ul>
							@foreach($kudosFinal as $user)
								@if($kudosFinal[0]['count'] == $user['count'])
									<li class="active">
									<span class="crown-icon">
										<img src="{{asset('public/images/crown-kudos.png')}}">
									</span>
								@else
									<li>
								@endif
								<div class="leader-img">
									@if(!empty($user['userImage']))
										<img src="{{asset('public/uploads/user_profile/'.$user['userImage'])}}">
									@else
										<img src="{{asset('public/images/no-user.png')}}">
									@endif
								</div>
								<h3> {{$user['userName']}}</h3>
								<span class="leader-number"> {{$user['count']}}</span>
								</li>
							@endforeach
						</ul>
					@else
						<div class="no-record">No record found.</div>
					@endif
				</div>
			</div>
			</div>
			
		</div>
	</div>
</main>

<script type="text/javascript">
	$('#kudos-org').on('change', function() {
		$('#kudos-office option').remove();
		$('#kudos-department option').remove();
		var orgId = $('#kudos-org').val();
		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getOfficeByOrgIdforGraph')!!}",				
			data: {orgId:orgId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{				
				$('.loader-img').hide();	
				// console.log(response.office);
				$('#kudos-office').append('<option value="" selected>All office</option>');
				$('#kudos-office').append(response.office);

				$('#kudos-department').append('<option value="" selected>All Department</option>');
				$('#kudos-department').append(response.department);

			}
		});
	});
</script>
<script type="text/javascript">
	$('#kudos-office').on('change', function() {
		$('#kudos-department option').remove();
		var officeId = $('#kudos-office').val();
		var orgId = $('#kudos-org').val();

		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getDepartmentByOfficecIdforGraph')!!}",				
			data: {officeId:officeId,orgId:orgId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{					
				$('.loader-img').hide();
				$('#kudos-department').append('<option value="" selected>All Department</option>');
				$('#kudos-department').append(response);
			}
		});
	});
</script>
<script type="text/javascript">
	$('#kudos-year').on('change', function() {
		$('#kudos-month option').remove();
		var year = $('#kudos-year').val();

		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getMonthsByYear')!!}",				
			data: {year:year,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{					
				$('.loader-img').hide();
				$('#kudos-month').append(response);
			}
		});
	});
</script>
<script type="text/javascript">
	$('.kudos-leaderboard-form').on('click',function(){
		$('.loader-img').show();			

		var form_values   = $("#kudos-leaderboard-form").serializeArray();

		var orgId      		= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];
		var year  			= form_values[4]['value'];
		var month  			= form_values[5]['value'];
		
		$.ajax({
			type: "GET",
			url: "{{URL::to('admin/kudos-Leaderboard')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,year:year,month:month,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				$('.loader-img').hide();
				$('body').html('');	
				$('body').append(response);
			}
		});		
	});
</script>

@include('layouts.adminFooter')


