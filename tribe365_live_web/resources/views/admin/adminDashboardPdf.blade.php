<!DOCTYPE html>
<html>
<head>
	<title>Tribe 365 - PDF </title>
	<link rel="shortcut icon" href="{{asset('public/images/Favicon-icon.png')}}" type="image/x-icon"/>
	<script
	src="<?php echo url('/public/extranal_js_css/jquery-3.3.1.js'); ?>" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/jsapi'); ?>"></script>
	<!-- google charts -->
	<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/loader.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/Chart.js'); ?>"></script>

	<style>
		html,
		#myChart {height: 100%;width: 100%;min-height: 150px;}
		.zc-ref {display: none;}
	</style>

	<style type="text/css">

		.dot-belief.lightgreen_bg {
			border-color: #ff454b;
			border: 2px solid #ff454b;
			border-top-left-radius: 12px;
			border-bottom: 8px solid #ff454b;
			border-top-right-radius: 12px;
		}
		.dot-belief {
			border-color: #eb1c24;
			border: 2px solid #eb1c24;
			border-top-left-radius: 12px;
			border-bottom: 8px solid #eb1c24;
			border-top-right-radius: 12px;
		}
		.dot-belief.red_bg {
			border-color:#b2b2b2;
			border: 2px solid #b2b2b2;
			border-top-left-radius: 12px;
			border-bottom: 8px solid #b2b2b2;
			border-top-right-radius: 12px;
		}
		.dot-belief.yellow_bg {
			border-color: #ff8085;
			border: 2px solid #ff8085;
			border-top-left-radius: 12px;
			border-bottom: 8px solid #ff8085;
			border-top-right-radius: 12px;
		}
		.dot-belief.orange_bg {
			border-color: #ffa3a6;
			border: 2px solid #ffa3a6;
			border-top-left-radius: 12px;
			border-bottom: 8px solid #ffa3a6;
			border-top-right-radius: 12px;
		}
		.dot-value-inner strong {background-color: #eb1c24;}
		.dot-value-inner.lightgreen_bg strong {background-color: #ff454b;}
		.dot-value-inner.red_bg strong {background-color: #b2b2b2; }
		.dot-value-inner.yellow_bg strong {background-color: #ff8085; }
		.dot-value-inner.orange_bg strong {background-color: #ffa3a6; }
		h4,h2 { font-family: "Helvetica","Arial",sans-serif; text-align: left;}
		h3 {
			font-size: 24px;
			font-family: "Helvetica","Arial",sans-serif;
		}
		@media print {
			body {
				-webkit-print-color-adjust: exact;
			}
		}
		form#make_pdf {
			max-width: 1450px;
			margin: 0 auto;
			text-align: right;
			position: fixed;
			top: 32px;
			right: 0;
			left: 0;
		}
		button#create_pdf {
			background-color: #eb1c24;
			font-size: 1rem;
			border: none;
			padding: 9px 20px 8px;
			color: #fff;
			border-radius: 5px;
			font-weight: 400;
		}
		.loader-img {
			position: fixed;
			left: 0;
			-ms-transform: translateX(-50%);
			width: 100% !important;
			background-color: rgba(239, 239, 239, 0.9);
			height: 100%;
			top: 0 !important;
			z-index: 9;
		}
		.loader-img img {
			position: absolute;
			top: 50%;
			transform: translateY(-50%);
			-webkit-transform: translateY(-50%);
			-ms-transform: translateY(-50%);
			left: 50%;
			transform: translateX(-50%);
			width: 97px;
		}
		@media print {
			.printPageButton {
				display: none;
			}
		}
		li:first-child {

    padding-left: 0 !important;

}
</style>
<style type="text/css">
	.pie_chart_div svg>g:nth-child(3) {transform: translate(30px, 80px);}
</style>
</head>
<body onload="init()">

	<div class="loader-img dashboard-loader" style="display:none; width: 70px;">
		<img src="{{asset('public/images/loader.gif')}}">
	</div>

	<div  id="covertIntoPDF" class="pdf-section" style="width: 1210px; margin: 0 auto; text-align: center;">
		

	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content">
		<div class="tital-name" style="width: 100%; display: inline-block;  clear: both;">
			<table style="width: 100%;">
				<tr>
					<td><img style="max-width: 120px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$organisationDetails->ImageURL))}}"></td>
					<td valign="top" style="padding-top: 55px; display: none;" class="pdfHead"><img style="max-width: 82px; float: right; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/logo.png'))}}"></td>
				</tr>
			</table>
		</div>

		<table width="100%" cellspacing="0" style="margin-top: 180px;text-align: center;">
			<tr>
				<td>
				<h1 style="margin-top: 20px;text-align: center;"> 
					Tribe365<sup>TM</sup> Report
				</h1>
				</td>
			</tr>
			<tr>
				<td>
				<h1 style="margin-top: 20px;text-align: center;">
					<?php echo date('F Y'); ?>
				</h1>						
				</td>
			</tr>
			<tr>
				<td>
					<h1 style="margin-top: 20px;text-align: center;"> 
					<?php echo $organisationDetails->organisation; ?>
					</h1>		
				</td>
			</tr>
		</table>					
	</div>	


	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content">
		<div class="tital-name" style="width: 100%; display: inline-block;  clear: both;">

			<table style="width: 100%;">
				<tr>
					<td valign="middle" style="display: none;" class="pdfHead"><img style="max-width: 120px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$organisationDetails->ImageURL))}}"></td>
					<td valign="top" style="padding-top: 55px; display: none;" class="pdfHead"><img style="max-width: 82px; float: right; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/logo.png'))}}"></td>
				</tr>
			</table>


			<h5 style="margin-top: 20px;text-align: left; clear: both; font-size: 25px;"> One Time(All time stats) </h5>

			<div style="width: 100%; clear: both;">
				<textarea rows="5" placeholder="Enter Your Text..." name="oneTimeAllStatesField" id="oneTimeAllStatesField" style="width: 100%;"></textarea>
				<span id="oneTimeAllStatesText"></span>
			</div>
		</div>


		<div style="">
			<table width="100%" cellspacing="0">
				<tr>
				<td width="25%"><ul style=" margin: 0;padding: 0;">
				<li style="list-style: none;max-width: 100%;margin-top: 20px;width: 100%;">
				<div style="border: solid 1px #d8d8d8;margin-top: 20px;padding: 20px;">
					<div style="border-bottom: solid 1px #eb1c24;padding: 0 0 9px;clear: both;"><h2> {{$resultArray['dotComplete']}}%</h2></div>
					<div style="text-align: left; padding: 15px 0;">
						
					<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">
					@if($resultArray['dotRatingCount']['MM'] > 0)
						MM: +{{$resultArray['dotRatingCount']['MM']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
					@elseif($resultArray['dotRatingCount']['MM'] < 0)
						MM: {{$resultArray['dotRatingCount']['MM']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
					@elseif($resultArray['dotRatingCount']['MM'] == 0)
						MM: 0%
					@endif
					</p>

					<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">
					@if($resultArray['dotRatingCount']['MA'] > 0)
						MA: +{{$resultArray['dotRatingCount']['MA']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
					@elseif($resultArray['dotRatingCount']['MA'] < 0)
						MA: {{$resultArray['dotRatingCount']['MA']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
					@elseif($resultArray['dotRatingCount']['MA'] == 0)
						MA: 0%
					@endif
					</p>

					<strong style="font-size: 12px;display: inline-block;width: 100%;">DOT Values Complete</strong></div>
				</div>					
			</li>				
			</ul>
			</td>
			
			<td width="25%">
				<ul style=" margin: 0;padding: 0;">
				<li style="list-style: none;max-width: 100%;margin-top: 20px;width: 100%;">
				<div style="border: solid 1px #d8d8d8;margin-top: 20px;padding: 20px;">
					<div style="border-bottom: solid 1px #eb1c24;padding: 0 0 9px;clear: both;"><h2> {{$resultArray['teamRoleComplete']}}%</h2></div>
					<div style="text-align: left; padding: 15px 0;">
					<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['teamRoleCompletedMMMA']['MM'] > 0)
						MM: +{{$resultArray['teamRoleCompletedMMMA']['MM']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
					@elseif($resultArray['teamRoleCompletedMMMA']['MM'] < 0)
						MM: {{$resultArray['teamRoleCompletedMMMA']['MM']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
					@elseif($resultArray['teamRoleCompletedMMMA']['MM'] == 0)
						MM: 0%
					@endif</p>

					<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['teamRoleCompletedMMMA']['MA'] > 0)
						MA: +{{$resultArray['teamRoleCompletedMMMA']['MA']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
					@elseif($resultArray['teamRoleCompletedMMMA']['MA'] < 0)
						MA: {{$resultArray['teamRoleCompletedMMMA']['MA']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
					@elseif($resultArray['teamRoleCompletedMMMA']['MA'] == 0)
						MA: 0%
					@endif</p>
					<strong style="font-size: 12px;display: inline-block;width: 100%;">Team Roles Complete</strong></div>
				</div>				
			</li>				
			</ul>
			</td>

			<td width="25%">
				<ul style=" margin: 0;padding: 0;">
				<li style="list-style: none;max-width: 100%;margin-top: 20px;width: 100%;">
				<div style="border: solid 1px #d8d8d8;margin-top: 20px;padding: 20px;">
					<div style="border-bottom: solid 1px #eb1c24;padding: 0 0 9px;clear: both;"><h2> {{$resultArray['personalityTypeComplete']}}%</h2></div>
					<div style="text-align: left; padding: 15px 0;">
					<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['personalityTypeCompletedMAMM']['MM'] > 0)
						MM: +{{$resultArray['personalityTypeCompletedMAMM']['MM']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
					@elseif($resultArray['personalityTypeCompletedMAMM']['MM'] < 0)
						MM: {{$resultArray['personalityTypeCompletedMAMM']['MM']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
					@elseif($resultArray['personalityTypeCompletedMAMM']['MM'] == 0)
						MM: 0%
					@endif</p>


					<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['personalityTypeCompletedMAMM']['MA'] > 0)
						MA: +{{$resultArray['personalityTypeCompletedMAMM']['MA']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
					@elseif($resultArray['personalityTypeCompletedMAMM']['MA'] < 0)
						MA: {{$resultArray['personalityTypeCompletedMAMM']['MA']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
					@elseif($resultArray['personalityTypeCompletedMAMM']['MA'] == 0)
						MA: 0%
					@endif</p>
					<strong style="font-size: 12px;display: inline-block;width: 100%;">Personality Types Complete</strong></div>
				</div>					
			</li>				
			</ul>
			</td>

			<td width="25%">
				<ul style=" margin: 0;padding: 0;">
				<li style="list-style: none;max-width: 100%;margin-top: 20px;width: 100%;">
				<div style="border: solid 1px #d8d8d8;margin-top: 20px;padding: 20px;">
					<div style="border-bottom: solid 1px #eb1c24;padding: 0 0 9px;clear: both;"><h2> {{$resultArray['motivationComplete']}}%</h2></div>
					<div style="text-align: left; padding: 15px 0;">
						<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['motivationCompletedMAMM']['MM'] > 0)
							MM: +{{$resultArray['motivationCompletedMAMM']['MM']}}%
							<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
						@elseif($resultArray['motivationCompletedMAMM']['MM'] < 0)
							MM: {{$resultArray['motivationCompletedMAMM']['MM']}}%
							<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
						@elseif($resultArray['motivationCompletedMAMM']['MM'] == 0)
							MM: 0%
						@endif</p>

						<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['motivationCompletedMAMM']['MA'] > 0)
							MA: +{{$resultArray['motivationCompletedMAMM']['MA']}}%
							<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
						@elseif($resultArray['motivationCompletedMAMM']['MA'] < 0)
							MA: {{$resultArray['motivationCompletedMAMM']['MA']}}%
							<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
						@elseif($resultArray['motivationCompletedMAMM']['MA'] == 0)
							MA: 0%
						@endif</p>
						<strong style="font-size: 12px;display: inline-block;width: 100%;">Motivation Complete</strong></div>
					</div>					
				</li>				
				</ul>
			</td>
		</tr>
		</table>			
		</div>
	</div>	



	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content" >
		<div class="tital-name" style="width: 100%; display: inline-block;  clear: both;">

			<table style="width: 100%;">
				<tr>
					<td valign="middle" style="display: none;" class="pdfHead"><img style="max-width: 120px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$organisationDetails->ImageURL))}}"></td>
					<td valign="top" style="padding-top: 55px; display: none;" class="pdfHead"><img style="max-width: 82px; float: right; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/logo.png'))}}"></td>
				</tr>
			</table>


			<h5 style="margin-top: 20px;text-align: left; clear: both; font-size: 25px;"> Daily's(Display yesterday's stats) </h5>

			<div style="width: 100%; clear: both;">
			<textarea rows="5" placeholder="Enter Your Text..." name="dailyDisplayStatesField" id="dailyDisplayStatesField" style="width: 100%;"></textarea>
			</div>

			<span id="dailyDisplayStatesText"></span>

		</div>


		<div style="">
			<table width="50%" cellspacing="0">
				<tr>
				<td width="25%">
				<ul style=" margin: 0;padding: 0;">
				<li style="list-style: none;max-width: 100%;margin-top: 20px;width: 100%;">
				<div style="border: solid 1px #d8d8d8;margin-top: 20px;padding: 20px;">
					<div style="border-bottom: solid 1px #eb1c24;padding: 0 0 9px;clear: both;"><h2> {{$resultArray['kudosComplete']}}%</h2></div>
					<div style="text-align: left; padding: 15px 0;">
					<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['bubbleRating']['DD'] > 0)
						DD: +{{$resultArray['bubbleRating']['DD']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
					@elseif($resultArray['bubbleRating']['DD'] < 0)
						DD: {{$resultArray['bubbleRating']['DD']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
					@elseif($resultArray['bubbleRating']['DD'] == 0)
						DD: 0%
					@endif</p>

					<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['bubbleRating']['MA'] > 0)
						MA: +{{$resultArray['bubbleRating']['MA']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
					@elseif($resultArray['bubbleRating']['MA'] < 0)
						MA: {{$resultArray['bubbleRating']['MA']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
					@elseif($resultArray['bubbleRating']['MA'] == 0)
						MA: 0%
					@endif</p>
					<strong style="font-size: 12px;display: inline-block;width: 100%;">Kudos/Person</strong></div>
				</div>					
				</li>				
			</ul>
			</td>

			<td width="25%">
				<ul style=" margin: 0;padding: 0;">
				<li style="list-style: none;max-width: 100%;margin-top: 20px;width: 100%;">
				<div style="border: solid 1px #d8d8d8;margin-top: 20px;padding: 20px;">
					<div style="border-bottom: solid 1px #eb1c24;padding: 0 0 9px;clear: both;"><h2> {{$resultArray['happyIndexComplete']}}%</h2></div>
					<div style="text-align: left; padding: 15px 0;">
					<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['happyIndexDDMA']['DD'] > 0)
						DD: +{{$resultArray['happyIndexDDMA']['DD']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
					@elseif($resultArray['happyIndexDDMA']['DD'] < 0)
						DD: {{$resultArray['happyIndexDDMA']['DD']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
					@elseif($resultArray['happyIndexDDMA']['DD'] == 0)
						DD: 0%
					@endif</p>

					<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['happyIndexDDMA']['MA'] > 0)
						MA: +{{$resultArray['happyIndexDDMA']['MA']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
					@elseif($resultArray['happyIndexDDMA']['MA'] < 0)
						MA: {{$resultArray['happyIndexDDMA']['MA']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
					@elseif($resultArray['happyIndexDDMA']['MA'] == 0)
						MA: 0%
					@endif</p>
					<strong style="font-size: 12px;display: inline-block;width: 100%;">Good/Bad Day Index Completed Yesterday</strong></div>
				</div>					
				</li>				
			</ul>
			</td>
			</tr>
			</table>			
		</div>
	</div>	


	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content" >
		<div class="tital-name" style="width: 100%; display: inline-block;  clear: both;">


			<table style="width: 100%;">
				<tr>
					<td valign="middle" style="display: none;" class="pdfHead"><img style="max-width: 120px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$organisationDetails->ImageURL))}}"></td>
					<td valign="top" style="padding-top: 55px; display: none;" class="pdfHead"><img style="max-width: 82px; float: right; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/logo.png'))}}"></td>
				</tr>
			</table>


			<h5 style="margin-top: 20px;text-align: left; clear: both; font-size: 25px;"> Monthly's(Display last month's stats) </h5>

			<div style="width: 100%; clear: both;">
			<textarea rows="5" placeholder="Enter Your Text..." name="montheltDisplayStatesField" id="montheltDisplayStatesField" style="width: 100%;"></textarea>
			</div>

			<span id="montheltDisplayStatesText"></span>

		</div>


		<div style="">
			<table width="100%" cellspacing="0">
				<tr>
				<td width="25%">
				<ul style=" margin: 0;padding: 0;">
				<li style="list-style: none;max-width: 100%;margin-top: 20px;width: 100%;">
				<div style="border: solid 1px #d8d8d8;margin-top: 20px;padding: 20px;">
					<div style="border-bottom: solid 1px #eb1c24;padding: 0 0 9px;clear: both;"><h2> {{$resultArray['improvements']}}%</h2></div>
					<div style="text-align: left; padding: 15px 0;">
					<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['improvementsMAMM']['MM'] > 0)
						MM: +{{$resultArray['improvementsMAMM']['MM']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
					@elseif($resultArray['improvementsMAMM']['MM'] < 0)
						MM: {{$resultArray['improvementsMAMM']['MM']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
					@elseif($resultArray['improvementsMAMM']['MM'] == 0)
						MM: 0%
					@endif</p>

					<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['improvementsMAMM']['MA'] > 0)
						MA: +{{$resultArray['improvementsMAMM']['MA']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
					@elseif($resultArray['improvementsMAMM']['MA'] < 0)
						MA: {{$resultArray['improvementsMAMM']['MA']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
					@elseif($resultArray['improvementsMAMM']['MA'] == 0)
						MA: 0%
					@endif</p>
					<strong style="font-size: 12px;display: inline-block;width: 100%;">Improvements sent/person</strong></div>
				</div>					
				</li>				
			</ul>
			</td>

			<td width="25%">
				<ul style=" margin: 0;padding: 0;">
				<li style="list-style: none;max-width: 100%;margin-top: 20px;width: 100%;">
					<div style="border: solid 1px #d8d8d8;margin-top: 20px;padding: 20px;">
					<div style="border-bottom: solid 1px #eb1c24;padding: 0 0 9px;clear: both;"><h2> {{$resultArray['cultureStructure']}}%</h2></div>
					<div style="text-align: left; padding: 15px 0;">
						<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['cultureStructureCompletedMAMM']['MM'] > 0)
							MM: +{{$resultArray['cultureStructureCompletedMAMM']['MM']}}%
							<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
						@elseif($resultArray['cultureStructureCompletedMAMM']['MM'] < 0)
							MM: {{$resultArray['cultureStructureCompletedMAMM']['MM']}}%
							<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
						@elseif($resultArray['cultureStructureCompletedMAMM']['MM'] == 0)
							MM: 0%
						@endif</p>

						<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['cultureStructureCompletedMAMM']['MA'] > 0)
							MA: +{{$resultArray['cultureStructureCompletedMAMM']['MA']}}%
							<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
						@elseif($resultArray['cultureStructureCompletedMAMM']['MA'] < 0)
							MA: {{$resultArray['cultureStructureCompletedMAMM']['MA']}}%
							<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
						@elseif($resultArray['cultureStructureCompletedMAMM']['MA'] == 0)
							MA: 0%
						@endif</p>
						<strong style="font-size: 12px;display: inline-block;width: 100%;">Organisation Structure Updated</strong></div>
					</div>					
				</li>				
			</ul>
			</td>

			<td width="25%">
				<ul style=" margin: 0;padding: 0;">
				<li style="list-style: none;max-width: 100%;margin-top: 20px;width: 100%;">
					<div style="border: solid 1px #d8d8d8;margin-top: 20px;padding: 20px;">
					<div style="border-bottom: solid 1px #eb1c24;padding: 0 0 9px;clear: both;"><h2> {{$resultArray['tribeometer']}}%</h2></div>
					<div style="text-align: left; padding: 15px 0;">
					<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['tribeometerUpdatedMAMM']['MM'] > 0)
						MM: +{{$resultArray['tribeometerUpdatedMAMM']['MM']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
					@elseif($resultArray['tribeometerUpdatedMAMM']['MM'] < 0)
						MM: {{$resultArray['tribeometerUpdatedMAMM']['MM']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
					@elseif($resultArray['tribeometerUpdatedMAMM']['MM'] == 0)
						MM: 0%
					@endif</p>

					<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['tribeometerUpdatedMAMM']['MA'] > 0)
						MA: +{{$resultArray['tribeometerUpdatedMAMM']['MA']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
					@elseif($resultArray['tribeometerUpdatedMAMM']['MA'] < 0)
						MA: {{$resultArray['tribeometerUpdatedMAMM']['MA']}}%
						<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
					@elseif($resultArray['tribeometerUpdatedMAMM']['MA'] == 0)
						MA: 0%
					@endif</p>
					<strong style="font-size: 12px;display: inline-block;width: 100%;">Tribeometer Updated</strong></div>
					</div>					
				</li>				
			</ul>
			</td>

			<td width="25%">
				<ul style=" margin: 0;padding: 0;">
				<li style="list-style: none;max-width: 100%;margin-top: 20px;width: 100%;">
					<div style="border: solid 1px #d8d8d8;margin-top: 20px;padding: 20px;">
					<div style="border-bottom: solid 1px #eb1c24;padding: 0 0 9px;clear: both;"><h2> {{$resultArray['diagnostic']}}%</h2></div>
					<div style="text-align: left; padding: 15px 0;">
						<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['diagnosticUpdatedMAMM']['MM'] > 0)
							MM: +{{$resultArray['diagnosticUpdatedMAMM']['MM']}}%
							<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
						@elseif($resultArray['diagnosticUpdatedMAMM']['MM'] < 0)
							MM: {{$resultArray['diagnosticUpdatedMAMM']['MM']}}%
							<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
						@elseif($resultArray['diagnosticUpdatedMAMM']['MM'] == 0)
							MM: 0%
						@endif</p>

						<p style="font-size: 12px;color: #474747;margin: 0; line-height: 24px;">@if($resultArray['diagnosticUpdatedMAMM']['MA'] > 0)
							MA: +{{$resultArray['diagnosticUpdatedMAMM']['MA']}}%
							<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/up.png'))}}">
						@elseif($resultArray['diagnosticUpdatedMAMM']['MA'] < 0)
							MA: {{$resultArray['diagnosticUpdatedMAMM']['MA']}}%
							<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/down.png'))}}">
						@elseif($resultArray['diagnosticUpdatedMAMM']['MA'] == 0)
							MA: 0%
						@endif</p>
						<strong style="font-size: 12px;display: inline-block;width: 100%;">Diagnostic Updated</strong></div>
					</div>					
				</li>				
			</ul>
			</td>
			</tr>
			</table>
		</div>
	</div>	

	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content" >
		<div class="tital-name" style="width: 100%; display: inline-block;  clear: both;">

			<table style="width: 100%;">
				<tr>
					<td valign="middle" style="display: none;" class="pdfHead"><img style="max-width: 120px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$organisationDetails->ImageURL))}}"></td>
					<td valign="top" style="padding-top: 55px; display: none;" class="pdfHead"><img style="max-width: 82px; float: right; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/logo.png'))}}"></td>
				</tr>
			</table>


			<h5 style="margin-top: 20px;text-align: left; clear: both; font-size: 25px;"> Culture Index </h5>

			<div style="width: 100%; clear: both;">
			<textarea rows="5"  placeholder="Enter Your Text..." name="cultureIndexField" id="cultureIndexField" style="width: 100%;"></textarea>
			</div>

			<span id="cultureIndexText"></span>

		</div>


		<div style="clear: both; min-width: 1200px;">
			<div id="cultureIndex"></div>
			<div style="width: 1000px;">
			<ul style="text-align: center;">
				<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;background-color: red"></span>Culture Index</a></li>
			</ul>
			</div>
		</div>
	</div>	

	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content" >
		<div class="tital-name" style="width: 100%; display: inline-block;  clear: both;">

			<table style="width: 100%;">
				<tr>
					<td valign="middle" style="display: none;" class="pdfHead"><img style="max-width: 120px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$organisationDetails->ImageURL))}}"></td>
					<td valign="top" style="padding-top: 55px; display: none;" class="pdfHead"><img style="max-width: 82px; float: right; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/logo.png'))}}"></td>
				</tr>
			</table>


			<h5 style="margin-top: 20px;text-align: left; clear: both; font-size: 25px;"> Engagement Index </h5>

			<div style="width: 100%; clear: both;">
			<textarea rows="5" placeholder="Enter Your Text..." name="engagementIndexField" id="engagementIndexField" style="width: 100%;"></textarea>
			</div>

			<span id="engagementIndexText"></span>

		</div>


		<div style="clear: both; min-width: 1200px;">
			<div id="engagementIndex"></div>
			<div style="width: 1000px;">
			<ul style="text-align: center;">
				<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;background-color: red"></span>Engagement Index</a></li>
			</ul>
			</div>
		</div>
	</div>	


	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content" >
		<div class="tital-name" style="width: 100%; display: inline-block;  clear: both;">


			<table style="width: 100%;">
				<tr>
					<td valign="middle" style="display: none;" class="pdfHead"><img style="max-width: 120px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$organisationDetails->ImageURL))}}"></td>
					<td valign="top" style="padding-top: 55px; display: none;" class="pdfHead"><img style="max-width: 82px; float: right; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/logo.png'))}}"></td>
				</tr>
			</table>


			<h5 style="margin-top: 20px;text-align: left; clear: both; font-size: 25px;"> Good/Bad Day Index </h5>

			<div style="width: 100%; clear: both;">
			<textarea rows="5"  placeholder="Enter Your Text..." name="happyIndexField" id="happyIndexField" style="width: 100%;"></textarea>
			</div>

			<span id="happyIndexText"></span>

		</div>


		<div style="clear: both; min-width: 1200px;">
			<div id="happyIndex"></div>
			<div style="width: 1000px;">
			<ul style="text-align: center;">
				<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="background-color: red; position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;"></span>All</a></li>
				<?php 
				 $catArray = $resultArray['happyIndexArray']['arrMain'][0]; 
				 if($catArray){
				 	$c=0;
				 	$catClr = array('','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327');
				 	foreach($catArray as $key=>$catArrayVal){
				 		if($c>0){
				 			$columColor = $catClr[$c];
				 		?>
				 			<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;background-color: <?php echo $columColor;?>"></span><?php echo $catArrayVal;?></a></li>
				 		<?php 
				 		}
				 		$c++;
				 	}
				 }
				?>
			</ul>
			</div>
		</div>
	</div>	


	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content" >
		<div class="tital-name" style="width: 100%; display: inline-block;  clear: both;">


			<table style="width: 100%;">
				<tr>
					<td valign="middle" style="display: none;" class="pdfHead"><img style="max-width: 120px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$organisationDetails->ImageURL))}}"></td>
					<td valign="top" style="padding-top: 55px; display: none;" class="pdfHead"><img style="max-width: 82px; float: right; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/logo.png'))}}"></td>
				</tr>
			</table>


			<h5 style="margin-top: 20px;text-align: left; clear: both; font-size: 25px;"> Good/Bad Day Index Responders </h5>

			<div style="width: 100%; clear: both;">
			<textarea rows="5"  placeholder="Enter Your Text..." name="happyIndexRespondersField" id="happyIndexRespondersField" style="width: 100%;"></textarea>
			</div>

			<span id="happyIndexRespondersText"></span>

		</div>


		<div style="clear: both; min-width: 1200px;">
			<div id="happyIndexResponders"></div>
			<div style="width: 1000px;">
			<ul style="text-align: center;">
				<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="background-color: red; position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;"></span>All</a></li>
				<?php 
				 $catArray = $resultArray['happyIndexResArray']['arrMain'][0]; 
				 if($catArray){
				 	$c=0;
				 	$catClr = array('','#000','#eb1c24','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327');
				 	foreach($catArray as $key=>$catArrayVal){
				 		if($c>0){
				 			$columColor = $catClr[$c];
				 		?>
				 			<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;background-color: <?php echo $columColor;?>"></span><?php echo $catArrayVal;?></a></li>
				 		<?php 
				 		}
				 		$c++;
				 	}
				 }
				?>
			</ul>
			</div>
		</div>
	</div>


	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content" >
		<div class="tital-name" style="width: 100%; display: inline-block;  clear: both;">

			<table style="width: 100%;">
				<tr>
					<td valign="middle" style="display: none;" class="pdfHead"><img style="max-width: 120px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$organisationDetails->ImageURL))}}"></td>
					<td valign="top" style="padding-top: 55px; display: none;" class="pdfHead"><img style="max-width: 82px; float: right; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/logo.png'))}}"></td>
				</tr>
			</table>

			<h5 style="margin-top: 20px;text-align: left; clear: both; font-size: 25px;"> Diagnostics </h5>
			
			<div style="width: 100%; clear: both;">
			<textarea rows="5" placeholder="Enter Your Text..." name="DiagnosticField" id="DiagnosticField" style="width: 100%;"></textarea>
			</div>

			<span id="DiagnosticText"></span>
		</div>


		<div style="clear: both; min-width: 1200px;">
			<div id="diagnostic"></div>
			<div style="width: 1000px;">
			<ul style="text-align: center;">
				<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="background-color: red; position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;"></span>All</a></li>
				<?php 
				 $catArray = $resultArray['diagnosticArray']['arrMain'][0]; 
				 if($catArray){
				 	$c=0;
				 	$catClr = array('','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327');
				 	foreach($catArray as $key=>$catArrayVal){
				 		if($c>0){
				 			$columColor = $catClr[$c];
				 		?>
				 			<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;background-color: <?php echo $columColor;?>"></span><?php echo $catArrayVal;?></a></li>
				 		<?php 
				 		}
				 		$c++;
				 	}
				 }
				?>
			</ul>
		</div>
		</div>

	</div>	


	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content" >
		<div class="tital-name" style="width: 100%; display: inline-block;  clear: both;">

			<table style="width: 100%;">
				<tr>
					<td valign="middle" style="display: none;" class="pdfHead"><img style="max-width: 120px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$organisationDetails->ImageURL))}}"></td>
					<td valign="top" style="padding-top: 55px; display: none;" class="pdfHead"><img style="max-width: 82px; float: right; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/logo.png'))}}"></td>
				</tr>
			</table>


			<h5 style="margin-top: 20px;text-align: left; clear: both; font-size: 25px;"> Tribeometer </h5>

			<div style="width: 100%; clear: both;">
			<textarea rows="5" placeholder="Enter Your Text..." name="tribeometerField" id="tribeometerField" style="width: 100%;"></textarea>
			</div>

			<span id="tribeometerText"></span>

		</div>


		<div style="clear: both; min-width: 1200px;">
			<div id="tribeometer"></div>
			<div style="width: 1000px;">
				<ul style="text-align: center;">
					<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="background-color: red; position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;"></span>All</a></li>
					<?php 
					 $catArray = $resultArray['tribeometerArray']['arrMain'][0]; 
					 if($catArray){
					 	$c=0;
					 	$catClr = array('','#000000','#eb1c24','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327');
					 	foreach($catArray as $key=>$catArrayVal){
					 		if($c>0){
					 			$columColor = $catClr[$c];
					 		?>
					 			<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;background-color: <?php echo $columColor;?>"></span><?php echo $catArrayVal;?></a></li>
					 		<?php 
					 		}
					 		$c++;
					 	}
					 }
					?>
				</ul>
			</div>
			</div>
	</div>	


	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content" >
		<div class="tital-name" style="width: 100%; display: inline-block;  clear: both;">

			<table style="width: 100%;">
				<tr>
					<td valign="middle" style="display: none;" class="pdfHead"><img style="max-width: 120px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$organisationDetails->ImageURL))}}"></td>
					<td valign="top" style="padding-top: 55px; display: none;" class="pdfHead"><img style="max-width: 82px; float: right; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/logo.png'))}}"></td>
				</tr>
			</table>


			<h5 style="margin-top: 20px;text-align: left; clear: both; font-size: 25px;"> Culture Structure </h5>

			<div style="width: 100%; clear: both;">
			<textarea rows="5"  placeholder="Enter Your Text..." name="cultureStructureField" id="cultureStructureField" style="width: 100%"></textarea>
			</div>

			<span id="cultureStructureText"></span>

		</div>


		<div style="clear: both; min-width: 1200px;">
			<div id="cultureStructure"></div>
			<div style="width: 1000px;">
				<ul style="text-align: center;">
					<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="background-color: red; position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;"></span>All</a></li>
					<?php 
					 $catArray = $resultArray['cultureStructureArray']['arrMain'][0]; 
					 //print_r($catMotivation);die;
					 if($catArray){
					 	$c=0;
					 	$catClr = array('','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327');
					 	foreach($catArray as $key=>$catArrayVal){
					 		if($c>0){
					 			$columColor = $catClr[$c];
					 		?>
					 			<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;background-color: <?php echo $columColor;?>"></span><?php echo $catArrayVal;?></a></li>
					 		<?php 
					 		}
					 		$c++;
					 	}
					 }
					?>
				</ul>
			</div>
		</div>
	</div>	


	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content" >
		<div class="tital-name" style="width: 100%; display: inline-block;  clear: both;">

			<table style="width: 100%;">
				<tr>
					<td valign="middle" style="display: none;" class="pdfHead"><img style="max-width: 120px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$organisationDetails->ImageURL))}}"></td>
					<td valign="top" style="padding-top: 55px; display: none;" class="pdfHead"><img style="max-width: 82px; float: right; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/logo.png'))}}"></td>
				</tr>
			</table>


			<h5 style="margin-top: 20px;text-align: left; clear: both; font-size: 25px;"> Team Role </h5>

			<div style="width: 100%; clear: both;">
			<textarea rows="5" placeholder="Enter Your Text..." name="teamRoleField" id="teamRoleField" style="width: 100%;"></textarea>
			</div>

			<span id="teamRoleText"></span>

		</div>


		<div style="clear: both; min-width: 1200px;">
			<div id="teamRole"></div>
			<div style="width: 1000px;">
			<ul style="text-align: center;">
				<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="background-color: red; position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;"></span>All</a></li>
				<?php 
				 $catArray = $resultArray['teamRoleArray']['arrMain'][0]; 
				 if($catArray){
				 	$c=0;
				 	$catClr = array('','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327');
				 	foreach($catArray as $key=>$catArrayVal){
				 		if($c>0){
				 			$columColor = $catClr[$c];
				 		?>
				 			<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;background-color: <?php echo $columColor;?>"></span><?php echo $catArrayVal;?></a></li>
				 		<?php 
				 		}
				 		$c++;
				 	}
				 }
				?>
			</ul>
		</div>
		</div>
	</div>	


	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content" >
		<div class="tital-name" style="width: 100%; display: inline-block;  clear: both;">

			<table style="width: 100%;">
				<tr>
					<td valign="middle" style="display: none;" class="pdfHead"><img style="max-width: 120px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$organisationDetails->ImageURL))}}"></td>
					<td valign="top" style="padding-top: 55px; display: none;" class="pdfHead"><img style="max-width: 82px; float: right; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/logo.png'))}}"></td>
				</tr>
			</table>


			<h5 style="margin-top: 20px;text-align: left; clear: both; font-size: 25px;"> Motivation </h5>

			<div style="width: 100%; clear: both;">
			<textarea rows="5" placeholder="Enter Your Text..." name="motivationField" id="motivationField" style="width: 100%;"></textarea>
			</div>

			<span id="motivationText"></span>

		</div>


		<div style="clear: both; min-width: 1200px;">
			<div id="motivation"></div>
			<div style="width: 1000px;">
			<ul style="text-align: center;">
				<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="background-color: red; position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;"></span>All</a></li>
				<?php 
				 $catArray = $resultArray['motivationArray']['arrMain'][0]; 
				 if($catArray){
				 	$c=0;
				 	$catClr = array('','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327');
				 	foreach($catArray as $key=>$catArrayVal){
				 		if($c>0){
				 			$columColor = $catClr[$c];
				 		?>
				 			<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;background-color: <?php echo $columColor;?>"></span><?php echo $catArrayVal;?></a></li>
				 		<?php 
				 		if($c==8){
				 			echo "<br />";
				 		}
				 		}
				 		$c++;
				 	}
				 }
				?>
			</ul>
			</div>
		</div>
	</div>	


	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content" >
		<div class="tital-name" style="width: 100%; display: inline-block;  clear: both;">

			<table style="width: 100%;">
				<tr>
					<td valign="middle" style="display: none;" class="pdfHead"><img style="max-width: 120px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$organisationDetails->ImageURL))}}"></td>
					<td valign="top" style="padding-top: 55px; display: none;" class="pdfHead"><img style="max-width: 82px; float: right; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/logo.png'))}}"></td>
				</tr>
			</table>


			<h5 style="margin-top: 20px;text-align: left; clear: both; font-size: 25px;"> Personality Type </h5>

			<div style="width: 100%; clear: both;">
			<textarea rows="5" placeholder="Enter Your Text..." name="personalityTypeField" id="personalityTypeField" style="width: 100%;"></textarea>
			</div>

			<span id="personalityTypeText"></span>

		</div>


		<div style="clear: both; min-width: 1200px;">
			<div id="personalityType"></div>
			<div style="width: 1000px;">

				<ul style="text-align: center;">
				<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="background-color: red; position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;"></span>All</a></li>
				<?php 
				 $catArray = $resultArray['personalityTypeArray'][0]; 
				 if($catArray){
				 	$c=0;
				 	$catClr = array('#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327');
				 	foreach($catArray as $key=>$catArrayVal){
				 		if($c>0){
				 			$columColor = $catClr[$c];
				 		?>
				 			<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;background-color: <?php echo $columColor;?>"></span><?php echo $catArrayVal;?></a></li>
				 		<?php 
				 		}
				 		$c++;
				 	}
				 }
				?>
				</ul>
			</div>
		</div>
	</div>	


	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content" >
		<div class="tital-name" style="width: 100%; display: inline-block;  clear: both;">			

			<table style="width: 100%;">
				<tr>
					<td valign="middle" style="display: none;" class="pdfHead"><img style="max-width: 120px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$organisationDetails->ImageURL))}}"></td>
					<td valign="top" style="padding-top: 55px; display: none;" class="pdfHead"><img style="max-width: 82px; float: right; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/logo.png'))}}"></td>
				</tr>
			</table>

			<h5 style="margin-top: 20px;text-align: left; clear: both; font-size: 25px;"> Beliefs </h5>

			<div style="width: 100%; clear: both;">
			<textarea rows="5" placeholder="Enter Your Text..." name="directingField" id="directingField" style="width: 100%;"></textarea>
			</div>

			<span id="directingText"></span>
			

		</div>


		<div style="clear: both; min-width: 1200px;">
			<div id="directing"></div>
			<div style="width: 1000px;">
			<ul style="text-align: center;">
				<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="background-color: red; position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;"></span>All</a></li>
				<?php 
				 $catArray = $resultArray['dotArray']['beliefId'];
				 if($catArray){
				 	$c=0;
				 	$catClr = array('#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327');
				 	foreach($catArray as $key=>$catArrayVal){
				 		//if($c>0){
				 			$columColor = $catClr[$c];
				 		?>
				 			<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;background-color: <?php echo $columColor;?>"></span><?php echo $catArrayVal;?></a></li>
				 		<?php 
				 		//}
				 		$c++;
				 	}
				 }
				?>
			</ul>
			</div>
		</div>
	</div>	


	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content" >
		<div class="tital-name" style="width: 100%; display: inline-block;  clear: both;">

			<table style="width: 100%;">
				<tr>
					<td valign="middle" style="display: none;" class="pdfHead"><img style="max-width: 120px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$organisationDetails->ImageURL))}}"></td>
					<td valign="top" style="padding-top: 55px; display: none;" class="pdfHead"><img style="max-width: 82px; float: right; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/images/logo.png'))}}"></td>
				</tr>
			</table>

			<h5 style="margin-top: 20px;text-align: left; clear: both; font-size: 25px;"> Kudos </h5>

			<div style="width: 100%; clear: both;">
			<textarea rows="5"  placeholder="Enter Your Text..." name="thumbsupField" id="thumbsupField" style="width: 100%;"></textarea>
			</div>

			<span id="thumbsupText"></span>
		</div>


		<div style="clear: both; min-width: 1200px;">
			<div id="thumbsup"></div>
			<div style="width: 1000px;">
			<ul style="text-align: center;">
				<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="background-color: red; position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;"></span>All</a></li>
				<?php 
				 $catArray = $resultArray['thumbsupArray']['beliefsName'];
				 if($catArray){
				 	$c=0;
				 	$catClr = array('#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327');
				 	foreach($catArray as $key=>$catArrayVal){
				 		//if($c>0){
				 			$columColor = $catClr[$c];
				 		?>
				 			<li style="display: inline-block;list-style-type: none;line-height: normal;padding-right: 10px;"><a onclick="#" style="display: inline-block;position: relative;padding-left: 12px;font-size: 12px;color: #000;line-height: normal;"><span style="position: absolute;display: block;width: 8px;height: 8px;left: 0;top: 2px;background-color: <?php echo $columColor;?>"></span><?php echo $catArrayVal;?></a></li>
				 		<?php 
				 		//}
				 		$c++;
				 	}
				 }
				?>
			</ul>
			</div>
		</div>
	</div>	


</div>		


<form action="{{URL::to('admin/exportPdf')}}" id="make_pdf" method="post">
	{{csrf_field()}}
	<input type="hidden" name="hidden_html" id="hidden_html">
	<input type="hidden" name="orgId" value="{{base64_encode($orgId)}}">	

	<?php if($ispdf==1){ ?>
	<button style="cursor: pointer;" class="printPageButton" type="button" name="create_pdf" id="create_pdf">Export</button>
	<?php } ?>
</form>


<style>
	.raphael-group-88-dataset-Label-group{
		display: none !important;
	}
	.raphael-group-78-common-elems-group path{
		fill: #eb1c24 !important;
	}
	.raphael-group-fpJqCkwO{
		display: none !important;
	}
</style>



<script type="text/javascript">
	$(document).ready(function(){
		$('.pdfHead').hide();
		$('#create_pdf').click(function(){

			var DiagnosticField = $('#DiagnosticField').val();
			$('#DiagnosticText').html(DiagnosticField);
			$('#DiagnosticField').hide();


			var tribeometerField = $('#tribeometerField').val();
			$('#tribeometerText').html(tribeometerField);
			$('#tribeometerField').hide();


			var cultureStructureField = $('#cultureStructureField').val();
			$('#cultureStructureText').html(cultureStructureField);
			$('#cultureStructureField').hide();


			var cultureIndexField = $('#cultureIndexField').val();
			$('#cultureIndexText').html(cultureIndexField);
			$('#cultureIndexField').hide();


			var engagementIndexField = $('#engagementIndexField').val();
			$('#engagementIndexText').html(engagementIndexField);
			$('#engagementIndexField').hide();


			var happyIndexField = $('#happyIndexField').val();
			$('#happyIndexText').html(happyIndexField);
			$('#happyIndexField').hide();

			var happyIndexRespondersField = $('#happyIndexRespondersField').val();
			$('#happyIndexRespondersText').html(happyIndexRespondersField);
			$('#happyIndexRespondersField').hide();

			var teamRoleField = $('#teamRoleField').val();
			$('#teamRoleText').html(teamRoleField);
			$('#teamRoleField').hide();


			var motivationField = $('#motivationField').val();
			$('#motivationText').html(motivationField);
			$('#motivationField').hide();


			var personalityTypeField = $('#personalityTypeField').val();
			$('#personalityTypeText').html(personalityTypeField);
			$('#personalityTypeField').hide();


			var directingField = $('#directingField').val();
			$('#directingText').html(directingField);
			$('#directingField').hide();


			var thumbsupField = $('#thumbsupField').val();
			$('#thumbsupText').html(thumbsupField);
			$('#thumbsupField').hide();

			var oneTimeAllStatesField = $('#oneTimeAllStatesField').val();
			$('#oneTimeAllStatesText').html(oneTimeAllStatesField);
			$('#oneTimeAllStatesField').hide();


			var dailyDisplayStatesField = $('#dailyDisplayStatesField').val();
			$('#dailyDisplayStatesText').html(dailyDisplayStatesField);
			$('#dailyDisplayStatesField').hide();

			var montheltDisplayStatesField = $('#montheltDisplayStatesField').val();
			$('#montheltDisplayStatesText').html(montheltDisplayStatesField);
			$('#montheltDisplayStatesField').hide();
						
			
			$('.pdfHead').show();

			$('#hidden_html').val($('#covertIntoPDF').html());			
			$('#make_pdf').submit();
			$('.loader-img').show();
		});
	});
</script>
<script type="text/javascript">
	function init() {
		google.load("visualization", "1.1", { packages:["corechart"], callback: 'drawCharts' });
	}
	function drawCharts() {
		drawDiagnostic('diagnostic');
		drawTribeometer('tribeometer');
		drawCultureStructure('cultureStructure');
		drawCultureIndex('cultureIndex');
		drawEngagementIndex('engagementIndex');
		drawHappyIndex('happyIndex');
		drawTeamRole('teamRole');
		drawMotivation('motivation');
		drawPersonalityType('personalityType');		
		drawDirecting('directing');
		drawThumbsup('thumbsup');	
		drawHappyIndexResponders('happyIndexResponders');
		$('.loader-img').hide();
	}	
</script>


<script type="text/javascript">

function drawDiagnostic(containerId) {
	var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['diagnosticArray']['arrMain']);?>);
	var options = {
		width: '100%',
		height: '100%',
		colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
		hAxis: {
			titleTextStyle: {
				color: '#333'
			}
		},
		vAxis: {
			minValue: 0,
			viewWindow: {
				@if($resultArray['diagnosticArray']['maxCount'] <= 0)
					max: 100
				@endif
				// min: 0,
				//max: 100
			}

			/*
			,
			gridlines: {
	          count: 15,
	        }
	        */
		},
		// curveType: 'function',
		chartArea: {
				top: 30,
				left: 30
			},
		legend: 'none',
		pointSize: 5
		/*
		legend: { position: 'bottom', maxLines:5,textStyle:{ color: 'black',fontSize: 11},alignment:'center' },
		*/
	};
     
    var chart_id = document.getElementById(containerId);
	var chart    = new google.visualization.LineChart(chart_id);
  	google.visualization.events.addListener(chart, 'ready', function(){
		chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
	});
  	chart.draw(data, options);

}
</script>

<script type="text/javascript">
	function drawTribeometer(containerId) {
		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['tribeometerArray']['arrMain']);?>);
		var options = {
			width: '100%',
			height: '100%',
			// colors: ['#eb1c24','#000'],
			colors: ['#000000','#eb1c24','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			hAxis: {
				titleTextStyle: {
					color: '#333'
				}
			},
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if($resultArray['tribeometerArray']['maxCount'] <= 0)
						max: 100
					@endif
					// min: 0,
					//max: 110
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 30
			},
			legend: 'none',
			pointSize: 5
			/*
			legend: { position: 'bottom', maxLines:5,textStyle:{ color: 'black',fontSize: 11},alignment:'center' },
			*/
		};

		var chart_id = document.getElementById(containerId);
		var chart    = new google.visualization.LineChart(chart_id);
	  	google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
	  	chart.draw(data, options);
	}
</script>


<script type="text/javascript">
	function drawCultureStructure(containerId) {
		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['cultureStructureArray']['arrMain']);?>);
		var options = {
			width: '100%',
			height: '100%',
			// colors: ['#eb1c24','#ffb4b7','#000','#939598'],
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			hAxis: {
				titleTextStyle: {
					color: '#333'
				}
			},
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if($resultArray['cultureStructureArray']['maxCount'] <= 0)
						max: 100
					@endif
					// min: 0,
					//max: 100
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 30
			},
			legend: 'none',
			pointSize: 5
			/*
			legend: { position: 'bottom', maxLines:5,textStyle:{ color: 'black',fontSize: 11},alignment:'center' },
			*/
		};
		var chart_id = document.getElementById(containerId);
		var chart    = new google.visualization.LineChart(chart_id);
	  	google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
	  	chart.draw(data, options);
	}


	function drawCultureIndex(containerId) {
		var data = google.visualization.arrayToDataTable([
			['', 'Culture Index'],
			@foreach($resultArray['cultureIndexArray']['indexArray'] as $value)
			["{{$value['monthName']}}",{{$value['data']}}],
			@endforeach

			]);
		var options = {
			colors: ['#ff454b'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if($resultArray['cultureIndexArray']['maxCount'] <= 0)
						max: 100
					@endif
					// min: 0,
					//max: 1000
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 30
			},
			legend: 'none',
			pointSize: 5
			/*
			legend: { position: 'bottom', maxLines:5,textStyle:{ color: 'black',fontSize: 11},alignment:'center' },
			*/
		};
		var chart_id = document.getElementById(containerId);
		var chart    = new google.visualization.LineChart(chart_id);
	  	google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
	  	chart.draw(data, options);
	}


	function drawEngagementIndex(containerId) {
			var data = google.visualization.arrayToDataTable([
			['', 'Engagement Index'],
			@foreach($resultArray['engagementIndexArray']['indexArray'] as $value)
			["{{$value['monthName']}}",{{$value['data']}}],
			@endforeach

			]);

			var options = {
				colors: ['#ff454b'],
				hAxis: { title: '',  titleTextStyle: { color: '#333' } },
				vAxis: {
					minValue: 0,
					viewWindow: {
						@if($resultArray['engagementIndexArray']['maxCount'] <= 0)
							max: 100
						@endif
						// min: 0,
						//max: 1000
					}
				},
				// curveType: 'function',
				chartArea: {
					top: 30,
					left: 30
				},
				legend: 'none',
				pointSize: 5
				/*
				legend: { position: 'bottom', maxLines:5,textStyle:{ color: 'black',fontSize: 11},alignment:'center' },
				*/
		};
		var chart_id = document.getElementById(containerId);
		var chart    = new google.visualization.LineChart(chart_id);
	  	google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
	  	chart.draw(data, options);
	}


	function drawHappyIndex(containerId) {
		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['happyIndexArray']['arrMain']);?>);
		var options = {
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if($resultArray['happyIndexArray']['maxCount'] <= 0)
						max: 100
					@endif
					// min: 0,
					// max: 100
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
			},
			legend: 'none',
			pointSize: 5
			// legend: { position: 'bottom',
			// 	textStyle: {bold:true},
			// }
		};
		var chart_id = document.getElementById(containerId);
		var chart    = new google.visualization.LineChart(chart_id);
	  	google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
	  	chart.draw(data, options);

	}


	function drawHappyIndexResponders(containerId) {
		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['happyIndexResArray']['arrMain']);?>);
		var options = {
			colors: ['#000','#eb1c24'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if($resultArray['happyIndexResArray']['maxCount'] <= 0)
						max: 100
					@endif
					// min: 0,
					// max: 100
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
			},
			legend: 'none',
			pointSize: 5
			// legend: { position: 'bottom',
			// 	textStyle: {bold:true},
			// }
		};
		var chart_id = document.getElementById(containerId);
		var chart    = new google.visualization.LineChart(chart_id);
	  	google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
	  	chart.draw(data, options);

	}
	

	function drawTeamRole(containerId) {
		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['teamRoleArray']['arrMain']);?>);
      	var options = {
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			// colors: ['#ff454b'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if($resultArray['teamRoleArray']['maxCount'] <= 0)
						max: 100
					@endif
					// min: 0,
					//max: 100
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 30
			},
			legend: 'none',
			pointSize: 5
			/*
			legend: { position: 'bottom', maxLines:5,textStyle:{ color: 'black',fontSize: 11},alignment:'center' },
			*/
		};
		var chart_id = document.getElementById(containerId);
		var chart    = new google.visualization.LineChart(chart_id);
	  	google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
	  	chart.draw(data, options);
	}


	function drawMotivation(containerId) {
		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['motivationArray']['arrMain']);?>);
	    var options = {
			// colors: ['#ff454b'],
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if($resultArray['motivationArray']['maxCount'] <= 0)
						max: 100
					@endif
					// min: 0,
					//max: 100
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 30
			},
			legend: 'none',
			pointSize: 5
			/*
			legend: { position: 'bottom', maxLines:5,textStyle:{ color: 'black',fontSize: 11},alignment:'center',padding:0},
			*/
		};
		var chart_id = document.getElementById(containerId);
		var chart    = new google.visualization.LineChart(chart_id);
	  	google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
	  	chart.draw(data, options);
	}


	function drawPersonalityType(containerId) {
		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['personalityTypeArray']);?>);
	    var options = {
			width: '100%',
			height: '100%',
			// colors: ['#ff454b'],
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					min: 0,
					max: 100
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 30
			},
			legend: 'none',
			pointSize: 5
			/*
			legend: { position: 'bottom', maxLines:5,textStyle:{ color: 'black',fontSize: 11},alignment:'center' },
			*/
		};
		var chart_id = document.getElementById(containerId);
		var chart    = new google.visualization.LineChart(chart_id);
	  	google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
	  	chart.draw(data, options);
	}
	


	function drawDirecting(containerId) {
		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['dotArray']['arrMain']);?>);
		var options = {
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			// colors: ['#ff454b'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if ($resultArray['dotArray']['maxCount'] <= 0) 
						max: 100
					@endif
					// min: 0,
					// max: 7
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 30
			},
			legend: 'none',
			pointSize: 5
			/*
			legend: { position: 'bottom', maxLines:5,textStyle:{ color: 'black',fontSize: 11},alignment:'center' },
			*/
		};
		var chart_id = document.getElementById(containerId);
		var chart    = new google.visualization.LineChart(chart_id);
	  	google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
	  	chart.draw(data, options);
	}


	function drawThumbsup(containerId) {
		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['thumbsupArray']['arrMain']);?>);
		var options = {
			// colors: ['#ff454b'],
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					min: 0,
					@if ($resultArray['thumbsupArray']['maxCount']==0) 
						max: 100
					@endif
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 30
			},
			legend: 'none',
			pointSize: 5
			/*
			legend: { position: 'bottom', maxLines:5,textStyle:{ color: 'black',fontSize: 11},alignment:'center' },
			*/
		};
		var chart_id = document.getElementById(containerId);
		var chart    = new google.visualization.LineChart(chart_id);
	  	google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
	  	chart.draw(data, options);
	}
</script>


</body>
</html>