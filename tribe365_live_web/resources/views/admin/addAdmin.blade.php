@section('title', 'Add Admin')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">

			<div class="dot-section">
				<div class="message-cls" align="center">                            
					@if(session('message'))
					<div class="alert alert-success" role="alert">
						{{session('message')}}
					</div>
					@endif
					@if(session('error'))
					<div class="alert alert-danger" role="alert">
						{{session('error')}}
					</div>
					@endif
				</div> 
				<form id="form" action="{{URL::to('admin/add-admin')}}" method="post" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="edit-profile-section">
						<div class="edit-profile-img">
							<img id="blah" src="{{asset('public/images/no-user.png')}}" alt="logo preview" class="mCS_img_loaded">
						</div> 
						<div class="edit-profile-btn">	
							<div id="form1" runat="server">
								<input type='file' id="imgInp" name="profileImg">
								<i class="fa fa-pencil" aria-hidden="true"></i>
							</div>
						</div>
					</div>
					<h2> Add New Admin </h2>
					<div class="form-section">
						<div class="row">
							<div class="col-md-6">
								<label>First Name</label>
								<div class="form-group">									
									<input class="charCount" id="name"  type="text" name="name" placeholder="Name" maxlength="50" value="">
								</div>									
							</div>
							<div class="col-md-6">
								<label>Last Name</label>
								<div class="form-group">									
									<input class="charCount" id="lastName"  type="text" name="lastName" placeholder="Last Name" maxlength="50" value="">
								</div>									
							</div>
							<div class="col-md-6">
								<label>Email</label>
								<div class="form-group">
									<input class="charCount" id="email" type="email" name="email" placeholder="Email" autocomplete="off" maxlength="50" value="">
								</div>									
							</div>	
							<div class="col-md-6">
								<label>Contact</label>
								<div class="form-group">
									<input class="charCount numberControl" id="contact" type="phone" name="contact" placeholder="Contact" autocomplete="off" maxlength="50" value="">
								</div>									
							</div>	
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="Create-btn-cont">
									<button type="button" onclick="validation()" class="btn">Save</button>
								</div>
							</div>
						</div>						
					</form>
					
				</div>
			</div>			
			<div class="error-message" style="display: none;">
				<span id="resp"></span>
			</div>		
			@if(count($errors)>0)
			<div class="success-message message">
				<span id="resp">{{$errors}}</span>
			</div>
			@endif

		</div>
	</div>
</main>

<script type="text/javascript">
	function validation() {
		$('.error-message').show();

		var name    		= $('#name').val();
		var lastName   		= $('#lastName').val();
		var email           = $('#email').val();

		is_email = isEmail(email);

		if($.trim(name) == ''){
			$('#resp').html('');
			$('#resp').html('Please enter First Name.');
		}else if($.trim(lastName) == ''){
			$('#resp').html('');
			$('#resp').html('Please enter Last Name.');
		}else if($.trim(email) == '' || !is_email){

			$('#resp').html('');
			$('#resp').html('Please enter Valid Email.');
		}else{
			$('#form').submit();
		}
	}

	function isEmail(email) {
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	   	return regex.test(email);
   	}

	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			
			reader.onload = function (e) {
				$('#blah').attr('src', e.target.result);
			}
			
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	$("#imgInp").change(function(){
		readURL(this);
	});
</script>

<script type="text/javascript">
	$(".numberControl").on("keypress keyup blur",function (event) {  
		$(this).val($(this).val().replace(/[^\d].+/, ""));
		if ((event.which < 48 || event.which > 57)) {
			event.preventDefault();
		}
	});
</script>
@include('layouts.adminFooter')

