@section('title', 'List Diagnostic Option')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                
								{{session('message')}}
							</div>
							@endif
						</div>
						<h2> Diagnostic Options </h2>	

						<div class="value-list menual_fun_lens_wrap">
							<table>
								<tr><th> Options </th><th>Action</th></tr>								
								@foreach($diaOptTbl as $value)	

								<form action="{{URL::to('admin/update-dia-opt')}}" method="post">
									{{csrf_field()}}
									<input type="hidden" name="optionId" value="{{base64_encode($value->id)}}">

									<tr class="table-class"><td><textarea name="option">{{$value->option_name}}</textarea></td>	
										<td>
											<div class="editable">
												<button class="editbtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
												<button style="display: none;" class="submitbtn" type="submit">submit</button>
											</div>
										</td>			
									</tr>
								</form>
								@endforeach							
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>		
	</div>
</main>
<script type="text/javascript">
	$(document).ready(function(){
		$('.submitbtn').hide();
		$(".table-class").find("textarea").prop("disabled", true);		
	});
</script>
<script type="text/javascript">
	$('.editbtn').on('click',function(){
		$(this).closest('.editable').find('.submitbtn').show();
		$(this).closest('.editable').find('.editbtn').hide();	
		$(this).closest('.table-class').find('textarea').prop('disabled',false);		
	})
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$("textarea").each(function(){
			var scroll_height = $(this).get(0).scrollHeight;
			$(this).css('height', scroll_height + 'px');
		});		
	});
</script>
@include('layouts.adminFooter')