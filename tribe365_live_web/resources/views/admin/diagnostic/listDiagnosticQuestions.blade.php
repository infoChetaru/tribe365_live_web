@section('title', 'List Diagnostic Questions')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">

			<div class="loader-img" style="display: none;width: 70px; top: 10px;"><img src="{{asset('public/images/loader.gif')}}"></div>

			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                
								{{session('message')}}
							</div>
							@endif
						</div>
						<div class="search-dia-question">
							<h2> Diagnostic Questions </h2>	

							<form id="diagnostic-form" action="{{route('diagnostic.index')}}" method="GET" class="diagnostic">

								<select name="categoryId"> 
									<option value=""> All </option>	
									@foreach($diaQueCategoryList as $cValue)
									<option value="{{$cValue->id}}" {{($categoryId==$cValue->id)?'Selected':''}}> {{$cValue->title}} </option>
									@endforeach()   
								</select>
								<button onclick="validation();" type="button">Search</button>

							</form>
						</div>

						<div class="value-list menual_fun_lens_wrap">
							<table>
								<tr>
									<th> Question </th>
									<th> Category </th>
									<th> Measure </th>
									<th> Action </th>
								</tr>								
								@foreach($questionTbl as $value)	

								{{ Form::model($value, ['method' => 'PUT', 'route' => array('diagnostic.update', base64_encode($value->id)),'id'=>'form']) }}
								
								<tr class="table-class">
									<td>
										<textarea name="question">{{$value->question}}</textarea>
									</td>	
									
									<td>
										<select name="diaCategoryId"> 
											@foreach($diaQueCategoryList as $cValue)
											<option value="{{$cValue->id}}" {{($value->category_id==$cValue->id)?'Selected':''}}> {{$cValue->title}} </option>
											@endforeach()
										</select>
									</td>
									<td>
										<textarea name="measure">{{ucfirst($value->measure)}}</textarea>
									</td>
									<td>
										<div class="editable">
											<button class="editbtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
											<button style="display: none;" class="submitbtn" type="submit">submit</button>
										</div>
									</td>			
								</tr>
								{{Form::close()}}
								@endforeach							
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="organ-page-nav">
			{{$questionTbl->links('layouts.pagination')}}
		</div>
	</div>
</main>
@include('layouts.adminFooter')
<script type="text/javascript">

	function validation()
	{
		$('.loader-img').show();
		$('#diagnostic-form').submit();
	}
	
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.submitbtn').hide();
		$(".table-class").find("textarea,select").prop("disabled", true);		
	});
</script>
<script type="text/javascript">
	$('.editbtn').on('click',function(){
		$(this).closest('.editable').find('.submitbtn').show();
		$(this).closest('.editable').find('.editbtn').hide();	
		$(this).closest('.table-class').find('textarea,select').prop('disabled',false);		
	})
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$("textarea").each(function(){
			var scroll_height = $(this).get(0).scrollHeight;
			$(this).css('height', scroll_height + 'px');
		});		
	});
</script>
