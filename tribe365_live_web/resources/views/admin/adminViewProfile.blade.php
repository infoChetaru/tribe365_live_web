@section('title', 'Admin Profile')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">

			<div class="dot-section">
				<form id="form" action="{!!route('admin.profile.update')!!}" method="post" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="edit-profile-section">
						<div class="edit-profile-img">
							@if(isset(Auth::user()->imageUrl) && !empty(Auth::user()->imageUrl))
								<img id="blah" src="{{asset('public/uploads/user_profile\/').$user->imageUrl}}" alt="logo preview" class="mCS_img_loaded">
							@else
								<img id="blah" src="{{asset('public/images/no-user.png')}}" alt="logo preview" class="mCS_img_loaded">
							@endif
							
							<!-- <img id="blah" src="img" alt="Preview Image" class="mCS_img_loaded hide-img">	 -->
						</div> 
						<div class="edit-profile-btn">	
							<div id="form1" runat="server">
								<input type='file' id="imgInp" name="profileImg">
								<i class="fa fa-pencil" aria-hidden="true"></i>
							</div>
						</div>
					</div>
					<h2> Account Information </h2>
					<div class="form-section">
						<div class="row">
							<div class="col-md-6">
								<label>First Name</label>
								<div class="form-group">									
									<input class="charCount" id="name"  type="text" name="name" placeholder="First Name" maxlength="50" value="{{$user->name}}">
								</div>									
							</div>
							<div class="col-md-6">
								<label>Last Name</label>
								<div class="form-group">									
									<input class="charCount" id="lastName"  type="text" name="lastName" placeholder="Last Name" maxlength="50" value="{{$user->lastName}}">
								</div>									
							</div>
							<div class="col-md-6">
								<label>Email</label>
								<div class="form-group">
									<input class="charCount" id="email" type="email" name="email" placeholder="Email" autocomplete="off" maxlength="50" value="{{$user->email}}" readonly="">
								</div>									
							</div>	
							<div class="col-md-6">
								<label>Contact</label>
								<div class="form-group">
									<input class="charCount numberControl" id="contact" type="phone" name="contact" placeholder="Contact" autocomplete="off" maxlength="50" value="{{$user->contact}}">
								</div>									
							</div>	
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="Create-btn-cont">
									<button type="button" onclick="validation()" class="btn">Save</button>
								</div>
							</div>
						</div>						
					</form>
					
				</div>
			</div>			
			<div class="error-message" style="display: none;">
				<span id="resp"></span>
			</div>		
			@if(count($errors)>0)
			<div class="success-message message">
				<span id="resp">{{$errors}}</span>
			</div>
			@endif

		</div>
	</div>
</main>

<script type="text/javascript">
	function validation()
	{
		$('.error-message').show();

		if(!$('#name').val()){

			$('#resp').html('');
			$('#resp').html('Please enter First Name.');
		}if(!$('#lastName').val()){

			$('#resp').html('');
			$('#resp').html('Please enter Last Name.');
		}else if(!$('#contact').val()){

			$('#resp').html('');
			$('#resp').html('Please enter Contact.');
		}else{
			$('#form').submit();
		}

	}

	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			
			reader.onload = function (e) {
				$('#blah').attr('src', e.target.result);
			}
			
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	$("#imgInp").change(function(){
		readURL(this);
	});
</script>

<script type="text/javascript">
	$(".numberControl").on("keypress keyup blur",function (event) {  
		$(this).val($(this).val().replace(/[^\d].+/, ""));
		if ((event.which < 48 || event.which > 57)) {
			event.preventDefault();
		}
	});
</script>
@include('layouts.adminFooter')

