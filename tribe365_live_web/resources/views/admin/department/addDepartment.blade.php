<!-- header -->
@section('title', 'Add Department')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="dot-section">
				<h2> Department </h2>
				<div class="form-section">

					<form id="form" action="{!!route('departments.store')!!}" method="post">
						{{csrf_field()}}
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">									
									<input type="text" name="department" placeholder="Enter Department" maxlength="100">
								</div>

								<div class="error" style="color:red;">
									{{$errors->first()}}
									{{session('message')}}
								</div>

							</div>

							<div class="Create-btn-cont">
								<button type="submit" class="btn">Add</button>
							</div>
						</div>
						
					</form>

				</div>
			</div>
		</div>
	</div>
</main>

@include('layouts.adminFooter')