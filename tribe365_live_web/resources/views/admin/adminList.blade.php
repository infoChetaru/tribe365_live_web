<!-- header -->
@section('title', 'List Admin')
@include('layouts.adminHeader')

<div class="add-fild-section">
	<div class="container">
		<div class="ragistration-section">
			<div class="row">
				<div class="col-md-12">
					<div class="loader-img" style="display: none;width: 70px; top: -57px;"><img src="{{asset('public/images/loader.gif')}}"></div>
					<div class="search-section">
						<form>
							<input id="search-key" type="search" placeholder="Search...">
						</form>                        
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                                    
								{{session('message')}}
							</div>
							@endif
						</div>                        
					</div>
					<h2> Admins </h2>					
					<div class="value-list">
						<table class="search-sec">
							<tr>
								<th> Name  </th>
								<th> Email </th>								
								<th> Action </th>
							</tr>							
							@foreach($users as $value)
							<tr>
								<td> {{ucfirst($value->name)." ".ucfirst($value->lastName)}} </td>
								<td> {{$value->email}} </td>
								<td>
									<div class="">
										<button class="deletebtn" type="button" onclick="deleteAdmin('{{$value->id}}');"><i class="fa fa-trash" aria-hidden="true"></i></button>
									</div>
									<!-- <div class="editable">
										<a href="{!!route('admin-user.edit',['id'=>base64_encode($value->id)])!!}">
											<button type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
										</a>

										{{ Form::open(['method'  => 'delete', 'route' => [ 'admin-user.destroy', base64_encode($value->id) ] ]) }}

										{{ Form::button('', ['type'=>'submit','onclick'=>'return confirm("Are you sure you want to delete ?")']) }}

										{{ Form::close() }}
									</div> -->

								</td>									
							</tr>
							@endforeach							
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="add-plus-icon">
	        <div class="add-plus-upper">
	            <a href="{{URL::to('admin/add-admin-view')}}"><img src="{{ asset('public/images/plus-icon.png')}}"></a>

	        </div>
	    </div>
	</div>
	<div class="organ-page-nav">
		{!! $users->links('layouts.pagination') !!}
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#search-key").keyup(function(){
			$('.search-sec').html('');			
			$('.organ-page-nav').html('');
			$('.loader-img').show();

			var staff = $('#search-key').val();
			// if(staff==''){
			// 	location.reload();
			// }
			$.ajax({
				type: "POST",
				url: "{!!URL::to('admin/searchAdmin')!!}",             
				data: {staff:staff,"_token":'<?php echo csrf_token()?>'},
				success: function(response) {
					
					// if(response.length >= 1){

						$('.search-sec').html('');				
						$('.search-sec').append(response);             
						$('.loader-img').hide();
					// }else{
						
					// 	$('.search-sec').html('');
					// 	$('.value-list').html('');				
					// 	$('.value-list').append('<div align="center" class="text-danger" role="alert">No result found.</div>');
					// 	$('.loader-img').hide();
					// }
				}
			}); 		
		});
	});
</script>
<script type="text/javascript">
	function deleteAdmin(userId) {
		swal({
			title: "Are you sure?",
			text: "You want to delete ?",
			type: null,
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes",
			closeOnConfirm: false,
			cancelButtonText: "No"
		},
		function(){
			$.ajax({
				url: "{{URL::to('admin/delete-admin')}}",
				type: "POST",
				dataType: "JSON",
				data: {userId:userId,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					if (response.status == 200) {
						location.href = "{{URL::to('admin/admin-list')}}";
					}else if (response.status == 400) {
						location.href = "{{URL::to('admin/admin-list')}}";
					}
				}
			});	
		});
	}
</script>
@include('layouts.adminFooter')