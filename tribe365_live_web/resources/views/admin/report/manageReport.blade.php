@section('title', 'List Organisation Report')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section dot cot-functional">
		<div class="container">  
			<div class="Reports-tital">
				<div align="center">                              
					<div class="loader-img" style="display: none;width: 70px;"><img src="{{asset('public/images/loader.gif')}}"></div>
				</div> 				
				<div class="prof-acount-section report-belief">
					<div class="compy-logo">
						<img src="{{asset('public/uploads/org_images\/').$org->ImageURL}}" class="mCS_img_loaded">
						<h5> {{ucfirst($org->organisation)}} </h5>
					</div>
				</div>
				
				<div class="generate-pdf-btn">
					
					<a target="_blank" href="{{URL::to('admin/pdf\/').base64_encode($org->id)}}">
						<button type="submit" class="btn"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</button>
					</a>
										
				</div>
				<div style="display: none;" class="date-from-to">
					<ul>
						<li> <strong> From: </strong> <input id="start_date" class="date" type="text" name="start_date" value=""> <span class="date-icon"><i class="fa fa-calendar"></i> </span></li>
						<li> <strong> To: </strong> <input id="end_date" class="date" type="text" name="end_date" value="">	 <span class="date-icon"> <i class="fa fa-calendar"></i> </span></li>
					</ul>								
				</div>

				<div class="cot-tab-section">					
					<ul class="nav nav-tabs">
						<li><a  class="active show" data-toggle="tab" href="#cot18">Culture Index</a></li>	

						<li class ="dot-tab"><a data-toggle="tab" href="#cot19" class="">Directing</a></li>	

						<li class ="cot-team-role-map1"><a data-toggle="tab" href="#cot2" class="">Connecting: Team Roles</a></li>	

						<li class="report-functinal-lens"><a data-toggle="tab" href="#cot7" class="">Connecting: Personality Type</a></li>	

						<li class="report-culture"><a data-toggle="tab" href="#cot4" class="">Supercharging: Culture Structure</a></li>	

						<li class="report-motivation"><a data-toggle="tab" href="#cot15" class="">Supercharging: Motivation</a></li>	

						<li class="report-diagnostic"><a data-toggle="tab" href="#cot6" class="">Diagnostics</a></li>

						<li class="report-tribometer"><a data-toggle="tab" href="#cot8" class="">Tribeometer</a></li>	

						<li class="dot-thumbsup-bubble-ratings-list"><a data-toggle="tab" href="#cot16" class="">Thumbs up</a></li>	

						<li class="report-happy-index"><a data-toggle="tab" href="#cot17" class="">Happy Index</a></li>	
					</ul>					
					<div class="tab-content">

						<div id="cot19" class="tab-pane fade"></div>

						<div id="cot15" class="tab-pane fade"></div>

						<div id="cot16" class="tab-pane fade"></div>

						<div id="cot2" class="tab-pane fade"></div>

						<div id="cot3" class="tab-pane fade"></div>

						<div id="cot4" class="tab-pane fade"></div>		

						<div id="cot6" class="tab-pane fade"></div>			

						<div id="cot7" class="tab-pane fade"></div>
						<!-- tribeometer graph -->
						<div id="cot8" class="tab-pane fade"></div>
						<!-- Happy index graph -->
						<div id="cot17" class="tab-pane fade"></div>
						<!-- Index -->
						<div id="cot18" class="tab-pane fade active show">

							
							<div align="center">                              
								<div class="loader-img-graph" style="display: block;width: 70px; margin-top: -80px;"><img src="{{asset('public/images/loader.gif')}}"></div>
							</div> 	


							<div class="search-cot">				
								<form id="report-index-form" action="#">
									{{csrf_field()}}					
									<select id="index-office" name="indexOfficeId">
										<option value="" selected>All Offices</option>
										@foreach($offices as $office)
											<option {{($officeId==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
										@endforeach
									</select>
									<select id="index-department" name="indexDepartmentId">			
									@if($departments && !empty($officeId))
										<option value="">All Department</option>
										@foreach($departments as $cdept)
											<option {{($departmentId==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
										@endforeach
									@elseif($all_department)
										<option value="">All Department</option>
										@foreach($all_department as $dept)
											<option {{($departmentId==$dept->id)?'selected':''}} value="{{$dept->id}}">{{$dept->department}}</option>
										@endforeach	
									@endif
									</select>		
									<button class="report-index-form" type="button">Search</button>

									
									<select name="selectYear" id="selectYear" onchange="showHideMonthSelect(this.value);">
										<option value="">All Years</option>
										<?php 
										if($yearUsed){
										foreach($yearUsed as $yearUsedVal){ ?>
										<option <?php if($currentYear && $currentYear==$yearUsedVal->usingyear) { echo 'selected'; } ?> value="<?php echo $yearUsedVal->usingyear; ?>"><?php echo $yearUsedVal->usingyear; ?></option>
										<?php } } ?>
									</select>
									
									<span <?php if($currentYear=='') { ?>style="display: none;" <?php } ?> id="monthSpan">
									<select name="currentMonth" id="currentMonth">
										<option value="">All Month</option>
										<option value="01" <?php if($currentMonth && $currentMonth=='1') { echo 'selected';} ?>>January</option>
										<option value="02" <?php if($currentMonth && $currentMonth=='2') { echo 'selected';} ?>>February</option>
										<option value="03" <?php if($currentMonth && $currentMonth=='3') { echo 'selected';} ?>>March</option>
										<option value="04" <?php if($currentMonth && $currentMonth=='4') { echo 'selected';} ?>>April</option>
										<option value="05" <?php if($currentMonth && $currentMonth=='5') { echo 'selected';} ?>>May</option>
										<option value="06" <?php if($currentMonth && $currentMonth=='6') { echo 'selected';} ?>>June</option>
										<option value="07" <?php if($currentMonth && $currentMonth=='7') { echo 'selected';} ?>>July</option>
										<option value="08" <?php if($currentMonth && $currentMonth=='8') { echo 'selected';} ?>>August</option>
										<option value="09" <?php if($currentMonth && $currentMonth=='9') { echo 'selected';} ?>>September</option>
										<option value="10" <?php if($currentMonth && $currentMonth=='10') { echo 'selected';} ?>>October</option>
										<option value="11" <?php if($currentMonth && $currentMonth=='11') { echo 'selected';} ?>>November</option>
										<option value="12" <?php if($currentMonth && $currentMonth=='12') { echo 'selected';} ?>>December</option>
									</select>
									</span>
								</form> 
							</div>

								

							<div style="clear: both; min-width: 1200px;">
								<div id="curve_chart"></div>
								<!--
								<ul>
									<li> <strong> Index</strong></li>
									<li> <span>{{$indexOrg}}</span></li>
								</ul>
								-->

							</div>
						</div>
					</div>


				</div>					

			</div>
		</div>
	</div>
</div>
</div>
</main>


<!-- google charts -->
<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/loader.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/Chart.js'); ?>"></script>


<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
	$('.loader-img-graph').hide();
	<?php 
	//Days wise  OR  //Year wise
	if($showGraph==2 || $showGraph==3){ ?>
		 var data = google.visualization.arrayToDataTable(
	        [
	          ["Index","Index"]
	            <?php 
	            $c    = 1;
	            $tArr = count($graphVal);
	            foreach($graphVal as $key=>$value) {
	              if($tArr>=$c){ ?>,<?php } 
	            ?>
	              ["<?php echo $key; ?>",<?php echo (int)$value; ?>]
	            <?php $c++; } ?>
	        ]
	      );
    <?php  
    //Month wise
	} else if($showGraph==1){ ?>

    	//var data = google.visualization.arrayToDataTable(
	        //<?php //echo $graphVal; ?>
	    //);



	      var data = google.visualization.arrayToDataTable([
	      	["Index","Index"],
		    <?php 
		    $c    = 1;
		    $tArr = count($graphVal);
		    if($graphVal) {
		    	foreach($graphVal as $graph){ ?>
		    	["<?php echo $graph[0]; ?>", <?php echo (int)$graph[1]; ?>]
		    	<?php if($tArr>=$c){ ?>,<?php }  ?>
		    <?php  $c++; } } ?>
		  ]);


    <?php } ?>



/*		
  var data = google.visualization.arrayToDataTable([
    ['Month', 'Index'],
    ['Jan',  1000],
    ['Feb',  1170],
    ['March',  660],
    ['Apr',  1030],
    ['May',  1030],
    ['Jun',  1030],
    ['July',  1030],
    ['Aug',  1030]
  ]);
*/
	

/*	
  var options = {
	    title: 'Total Index',
	    curveType: 'function',
	    legend: { position: 'bottom' },
	    colors: ['#e2431e'],
	    vAxis: {
            viewWindow: {
                min: 0 
            },
            textStyle : {
	            fontSize: 11
	        }
	    },
	    hAxis : { 
	        textStyle : {
	            fontSize: 11
	        }
    	}
  };
*/


	var options = {
		//width: 1000,
		//height: 400,
		colors: ['#eb1c24'],
		hAxis: {
			title: '',
			titleTextStyle: {
				color: '#eb1c24'
			},
			textStyle : {
	            fontSize: 11
	        }
			
		},
		vAxis: { 
			minValue: 0,
			viewWindow: {
		        min: 0,
		        <?php
		        	if (count(array_filter($graphVal)) == 0) {
		        ?>
		        max : 10
		        <?php } ?>
		        //max: 1000
		    },
		    textStyle : {
	            fontSize: 11
	        }
		},
		curveType: 'function',
		chartArea: {
			top: 40,
			left: 40,
			//height: '100%',
			//width: '100%'
		},
		legend: 'none'
	};


  var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
  chart.draw(data, options);
}
</script>


<script type="text/javascript">	
	// COT Team Role mape
	$('.cot-team-role-map1').on('click',function(){
		$('.loader-img').show();
		var orgId = "{{base64_encode($org->id)}}";		
		var start_date = $('#start_date').val().trim();
		var end_date   = $('#end_date').val().trim();

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getCOTteamRoleMapGraph')}}",
			data: {orgId:orgId,startDate:start_date,endDate:end_date,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				$('.loader-img').hide();
				$('#cot2').html('');
				$('#cot5').html('');
				$('#cot3').html('');	
				$('#cot2').append(response);
			}
		});		
	});

// tribeometer
$('.report-tribometer').on('click',function(){
	$('.loader-img').show();
	var orgId = "{{base64_encode($org->id)}}";
	var start_date = $('#start_date').val().trim();
	var end_date   = $('#end_date').val().trim();

	$.ajax({
		type: "POST",
		url: "{{URL::to('admin/getReportTribeometerGraph')}}",
		data: {orgId:orgId,startDate:start_date,endDate:end_date,"_token":'<?php echo csrf_token()?>'},
		success: function(response)
		{
			$('.loader-img').hide();
			$('#cot2').html('');
			$('#cot5').html('');
			$('#cot3').html('');
			$('#cot8').html('');	
			$('#cot8').append(response);
		}
	});		
});

// Diagnostic 
$('.report-diagnostic').on('click',function(){
	$('.loader-img').show();
	var orgId = "{{base64_encode($org->id)}}";
	var start_date = $('#start_date').val().trim();
	var end_date   = $('#end_date').val().trim();

	$.ajax({
		type: "POST",
		url: "{{URL::to('admin/getReportDiagnosticGraph')}}",
		data: {orgId:orgId,startDate:start_date,endDate:end_date,"_token":'<?php echo csrf_token()?>'},
		success: function(response)
		{
			$('.loader-img').hide();
			$('#cot2').html('');
			$('#cot5').html('');
			$('#cot3').html('');
			$('#cot6').html('');	
			$('#cot6').append(response);
		}
	});		
});

// Report-Motivation
$('.report-motivation').on('click',function(){
	$('.loader-img').show();
	var orgId = "{{base64_encode($org->id)}}";
	var start_date = $('#start_date').val().trim();
	var end_date   = $('#end_date').val().trim();

	$.ajax({
		type: "POST",
		url: "{{URL::to('admin/getReportMotivationalGraph')}}",
		data: {orgId:orgId,startDate:start_date,endDate:end_date,"_token":'<?php echo csrf_token()?>'},
		success: function(response)
		{
			$('.loader-img').hide();
			$('#cot2').html('');
			$('#cot5').html('');
			$('#cot3').html('');
			$('#cot15').html('');	
			$('#cot15').append(response);
		}
	});		
});

// Report-culture structure
$('.report-culture').on('click',function(){
	$('.loader-img').show();
	var orgId = "{{base64_encode($org->id)}}";
	var start_date = $('#start_date').val().trim();
	var end_date   = $('#end_date').val().trim();

	$.ajax({
		type: "POST",
		url: "{{URL::to('admin/getReportCultureGraph')}}",
		data: {orgId:orgId,startDate:start_date,endDate:end_date,"_token":'<?php echo csrf_token()?>'},
		success: function(response)
		{
			$('.loader-img').hide();
			$('#cot2').html('');
			$('#cot5').html('');
			$('#cot3').html('');
			$('#cot4').html('');	
			$('#cot4').append(response);
		}
	});		
});

// Report-functinal-lens (Personality type)
$('.report-functinal-lens').on('click',function(){
	$('.loader-img').show();
	var orgId = "{{base64_encode($org->id)}}";
	var start_date = $('#start_date').val().trim();
	var end_date   = $('#end_date').val().trim();

	$.ajax({
		type: "POST",
		url: "{{URL::to('admin/getReportFunctionalGraph')}}",
		data: {orgId:orgId,startDate:start_date,endDate:end_date,"_token":'<?php echo csrf_token()?>'},
		success:function(response)
		{
			$('.loader-img').hide();
			$('#cot2').html('');
			$('#cot5').html('');
			$('#cot3').html('');
			$('#cot7').html('');	
			$('#cot7').append(response);
		}
	});		
});


// DOT bubble thumbsup list for report
$('.dot-thumbsup-bubble-ratings-list').on('click',function(){
	$('.loader-img').show();
	var orgId = "{{base64_encode($org->id)}}";	

	$.ajax({
		type: "POST",
		url: "{{URL::to('admin/getDotThumbsUpBubbleList')}}",
		data: {orgId:orgId,"_token":'<?php echo csrf_token()?>'},
		success: function(response)
		{

			$('.loader-img').hide();
			$('#cot2').html('');
			$('#cot3').html('');
			$('#cot4').html('');
			$('#cot5').html('');
			$('#cot6').html('');			
			$('#cot7').html('');
			$('#cot8').html('');
			$('#cot16').html('');	
			$('#cot16').append(response);
		}
	});		
});

//Happy Index Report

$('.report-happy-index').on('click',function(){
	$('.loader-img').show();
	var orgId = "{{base64_encode($org->id)}}";
	var start_date = $('#start_date').val().trim();
	var end_date   = $('#end_date').val().trim();

	$.ajax({
		type: "POST",
		url: "{{URL::to('admin/getReportHappyIndexGraphYear')}}",
		data: {orgId:orgId,startDate:start_date,endDate:end_date,"_token":'<?php echo csrf_token()?>'},
		success: function(response)
		{
			$('.loader-img').hide();
			$('#cot2').html('');
			$('#cot5').html('');
			$('#cot3').html('');
			//$('#cot6').html('');	
			$('#cot17').html('');	
			$('#cot17').append(response);
		}
	});		
});

//Report for index
$('.report-index-form').on('click',function(){
	$('.loader-img-graph').show();			

	var form_values   = $("#report-index-form").serializeArray();

	var officeId      = form_values[1]['value'];
	var departmentId  = form_values[2]['value'];
	var orgId         = "{{base64_encode($orgId)}}";
	var currentMonth  = $('#currentMonth').val().trim();
	var selectYear  = $('#selectYear').val().trim();


	console.log(form_values)
	$.ajax({
		type: "GET",
		//url: "{{URL::to('admin/getReportDiagnosticGraph')}}",
		url: "{{route('reports.show',base64_encode($orgId))}}",
		data: {selectYear:selectYear,currentMonth:currentMonth,orgId:orgId,officeId:officeId,departmentId:departmentId,"_token":'<?php echo csrf_token()?>'},
		success: function(response)
		{
			 $('.loader-img-graph').hide();
			// $('#cot2').html('');
			// $('#cot5').html('');
			// $('#cot3').html('');
			// $('#cot6').html('');	
			// $('#cot6').append(response);
			$("body").html(response);
		}
	});		
});

$('#index-office').on('change', function() {
		$('#index-department option').remove();
		var officeId = $('#index-office').val();

		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getDepartmentByOfficecIdforGraph')!!}",				
			data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{					
				$('#index-department').append('<option value="" selected>All Department</option>');
				$('#index-department').append(response);
			}
		});
	});
</script>



<script type="text/javascript">
//
$('#end_date').on('change', function() {


});

(function($){
	$(window).on("load",function(){

		$("#content-1").mCustomScrollbar({
			axis:"x",
			advanced:{
				autoExpandHorizontalScroll:true
			}
		});
	});
});

</script>

<script type="text/javascript">
	// Dot
	$('.dot-tab').on('click',function(){
		$('.loader-img').show();
		var orgId = "{{base64_encode($org->id)}}";		
		var start_date = $('#start_date').val().trim();
		var end_date   = $('#end_date').val().trim();

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/dotTabData')}}",  
			data: {orgId:orgId,startDate:start_date,endDate:end_date,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				$('.loader-img').hide();
				//$('#cot2').html('');
				//$('#cot5').html('');
				//$('#cot1').html('');
				$('#cot19').html('');	
				$('#cot19').html(response);
			}
		});		
	});
</script>

<script type="text/javascript">
	$(document).on('focus', '.date',function(){
		$(this).datepicker({
			todayHighlight:true,
			dateFormat: 'dd-mm-yy',
			autoclose:true,
			onSelect: function(dateText) {

				console.log('date '+dateText)				
				
				var beliefId   = $('#belief').val();
				var start_date = $('#start_date').val().trim();
				var end_date   = $('#end_date').val().trim();

				console.log('startDate '+start_date+' '+'endDate '+end_date)
				if(end_date)
				{
					$("#cot1").click();
					$('.loader-img').show();

					$.ajax({
						type: "GET",
						url: "{{route('reports.show',base64_encode($org->id))}}",   
						data: {beliefId:beliefId,startDate:start_date,endDate:end_date,"_token":'<?php echo csrf_token()?>'},
						success: function(response) 
						{
							$("body").html(response);
							$('#start_date').val(start_date);
							$('#end_date').val(end_date);
						}
					});

				}
			}
		})
	});
</script>

<!-- refresh page after 1 hour -->
<script type="text/javascript">
	setInterval(function () { 
		location.reload(true);
	},3600000);
</script>


<script type="text/javascript">
	function showHideMonthSelect(YearVal){
		//alert(YearVal);
		if(YearVal==''){
			$('#monthSpan').hide();
		}else{
			$('#monthSpan').show();
		}
	}
</script>

<!--
<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/fusioncharts.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/fusioncharts.theme.fusion.js'); ?>"></script>
<style>
	.raphael-group-207-dataset-Label-group{
		display: none !important;
	}
	.raphael-group-85-dataset-Label-group{
		display: none !important;
	}
	.raphael-group-75-common-elems-group path{
		fill: #eb1c24 !important;
	}
	.raphael-group-197-common-elems-group path{
		fill: #eb1c24 !important;
	}
</style>
-->

@include('layouts.adminFooter')