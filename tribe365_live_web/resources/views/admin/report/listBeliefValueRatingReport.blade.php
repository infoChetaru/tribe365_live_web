<!-- header -->
@section('title', 'List Belief Value Rating')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                                    
								{{session('message')}}
							</div>
							@endif

						</div>
						 <div class="loader-img" style="display: none;width: 70px;"><img src="{{asset('public/images/loader.gif')}}"></div>
              

						<div class="prof-acount-section report-belief">
							<div class="compy-logo" style="padding-bottom: 15px;">
								@if(!empty($organisations->ImageURL))
								<img src="{{asset('public/uploads/org_images\/').$organisations->ImageURL}}" class="mCS_img_loaded">
								@endif
								<h2>
									@if(!empty($organisations->organisation))
									{{ucfirst($organisations->organisation)}}
									@endif
								</h2>
							</div>
							<div class="search-cot">
								<a href="{{route('reports.show',base64_encode($organisations->id))}}"><img src="{{asset('public/images/chart-bar-solid.png')}}"></a> 

								<select id="belief" name="beliefId">
									<option value="" selected>All Belief</option>	
									@foreach($beliefList as $bValue)				
									<option {{(session('searchBeliefId')==$bValue->id)?'selected':''}} value="{{$bValue->id}}">{{ucfirst($bValue->name)}}</option>
									@endforeach
								</select>				
							</div>
						</div>
						<div class="value-list">


							@foreach($dotValuesArray as $belief)
							<table>		
								<tr><td style="width: 175px;">Belief</td><td class="value-tital" colspan="2">{{ucfirst($belief['beliefName'])}}<a href="{{URL::to('admin/report-belief-individual-ratings/'.base64_encode($belief['beliefId']))}}"><img class="view-list" src="{{asset('public/images/eye.png')}}"></a></td></tr>
								<tr><th style="width: 175px;">Value</th><th style="width: 135px;">Avg. Rating</th><th>Description</th></tr>
								@foreach($belief['beliefValues'] as $bValues)
								<tr>
									<td>{{ucfirst($bValues['valueName'])}}</td>
									<td>{{number_format($bValues['valueRatings'],2)}}</td>
									<td>
										<?php 

										if ($bValues['valueRatings']<5 && $bValues['valueRatings'] >=4) {
											echo "I understand what it means to be ".ucfirst($bValues['valueName']).", I am ".ucfirst($bValues['valueName'])." every day at work, colleagues and clients would describe me as ".ucfirst($bValues['valueName']);
										}elseif ($bValues['valueRatings']<4 && $bValues['valueRatings']>=3) {
											echo "I understand what it means to be ".ucfirst($bValues['valueName']).", I am ".ucfirst($bValues['valueName'])." every day at work";
										}elseif ($bValues['valueRatings']<3 && $bValues['valueRatings']>=2) {
											echo "I understand what it means to be ".ucfirst($bValues['valueName'])." , I am ".ucfirst($bValues['valueName'])." when I have to be ".ucfirst($bValues['valueName']);
										}elseif ($bValues['valueRatings']<2 && $bValues['valueRatings']>=1) {
											echo "I understand what it means to be ".ucfirst($bValues['valueName']).",  I am never/rarely ".ucfirst($bValues['valueName']);
										}elseif ($bValues['valueRatings']<1 && $bValues['valueRatings']>=0) {
											echo "I do not understand what it means to be ".ucfirst($bValues['valueName']);
										}else{
											echo "I understand what it means to be ".ucfirst($bValues['valueName']).", I am ".ucfirst($bValues['valueName'])." every day at work, colleagues and clients would describe me as ".ucfirst($bValues['valueName']).", I help others become ".ucfirst($bValues['valueName']);
										}

										?>
									</td>
								</tr>
								@endforeach
							</table>
							@endforeach

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="organ-page-nav">
			{{--$dotValuesArray->links('layouts.pagination')--}}
		</div>
	</div>
</main>
<script type="text/javascript">
	$('#belief').on('change', function() {
		$('.loader-img').show();
		var beliefId = $('#belief').val();
		
		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/report-belief-value-rating-list/'.base64_encode($organisations->id))!!}",		
			data: {beliefId:beliefId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{
				console.log(response)
				$("body").html(response);

			}
		});
	});
</script>
@include('layouts.adminFooter')