@section('title', 'List Organisation Report')
@include('layouts.adminHeader')
<main class="main-content">
    <div class="add-fild-section organization-fild">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="search-section">                                          
                        <div align="center">                            
                            @if(session('message'))
                            <div class="alert alert-success" role="alert">                                    
                                {{session('message')}}
                            </div>
                            @endif
                            @if(session('error'))
                            <div class="alert alert-danger" role="alert">                                    
                              {{session('error')}}
                          </div>
                          @endif
                      </div>                        
                  </div>
              </div>
          </div>
          <div class="row">
            <div class="organization-section  manage-reports ">

                @foreach($organisation as $value)

                <div class="col-md-3">
                    <div class="company-section">
                        <div class="company-detale">   
                         <a href="{{URL::to('admin/dot-reports/'.base64_encode($value->id))}}">
                            @if(!empty($value->ImageURL))
                            <img src="{{ asset('public/uploads/org_images').'/'.$value->ImageURL }}">
                            @else 
                            <img src="{{ asset('public/images/no-image.png')}}">
                            @endif
                        </a>
                        <h2> {{ucfirst($value->organisation)}} </h2>
                    </div>
                    <div class="company-ovel-text">
                        <span>generate report </span>
                    </div>

                       <!--  <div class="company-category">
                            <ul>
                                <li>
                                    <span class="category-tital">Reports</span>                                    
                                </li>                               
                            </ul>
                        </div> -->
                    </div>
                </div>  
                @endforeach 
                
            </div>
        </div>


        <!-- <div class="add-plus-icon">
            <div class="add-plus-upper">
                <a href="{!!route('admin-organisation.create')!!}"><img src="{{ asset('public/images/plus-icon.png')}}"></a>

            </div> -->
        </div>
    </div>

    <div class="organ-page-nav" >
        {!! $organisation->links('layouts.pagination') !!}                               
    </div>
</div>
</main>

@include('layouts.adminFooter')