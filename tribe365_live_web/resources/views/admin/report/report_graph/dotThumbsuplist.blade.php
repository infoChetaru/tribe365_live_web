<div class="search-cot">				
	<form id="report-dot-bubble-thumbsup-form" action="#">
		{{csrf_field()}}					
		<select id="users" name="userId">
			<option value="" selected>All Users</option>
			@foreach($userList as $uValue)		
			<option value="{{$uValue->id}}" {{($userId==$uValue->id)?'selected':''}}>{{$uValue->name}}</option>
			@endforeach()	
		</select>

		<input style="width: calc(18% - -81px);" name="date" class="date date_field numberControl" type="hidden" value="{{$date}}" autocomplete="off"/>

		<button class="report-dot-bubble-thumbsup-btn" type="button">Search</button>
	</form> 
</div>	

<div class="thumbs-section">
	@foreach($beliefArr as $bValue)
	<div class="thumbs-section-inner">
		<h3> {{$bValue['name']}} </h3>
		<div class="thumbs-listing">
			<ul>
				@foreach($bValue['beliefValueArr'] as $bvValue)
				<li>
					<span class="list-tital">{{$bvValue['name']}} {{$bvValue['ratings']}} </span>
					<span class="list-icon"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
					<span class="list-no"> {{$bvValue['upVotes']}} </span>
				</li>
				@endforeach()
			</ul>
		</div>
	</div>
	@endforeach()
</div>

<script type="text/javascript">
	// var currentTime = new Date(); 
	// var minDate = new Date(currentTime.getYear(), currentTime.getMonth()-1); 
	// var maxDate =  new Date(currentTime.getFullYear(),currentTime.getMonth()-1); 
	// $(".date_field").datepicker({
	// 	changeMonth: true,
	// 	changeYear: true,
	// 	todayHighlight:true,
	// 	dateFormat: 'MM-yy',
	// 	autoclose:true,			
	// 	minDate: minDate, 
	// 	maxDate: maxDate, 
	// });

	$(function() {
		var currentTime = new Date(); 
		var minDate = new Date(currentTime.getYear(), currentTime.getMonth()-1); 
		var maxDate = new Date(currentTime.getFullYear(),currentTime.getMonth()-1); 
		$(".date_field").datepicker({
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			todayHighlight:false,
			dateFormat: 'MM-yy',
			autoclose:true,			
			minDate: minDate, 
			maxDate: maxDate, 
			onClose: function(dateText, inst) { 
				$(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
			}
		});
	});
</script>

<script type="text/javascript">
	
	$('.report-dot-bubble-thumbsup-btn').on('click',function(){
		$('.loader-img').show();
		
		var form_values   = $("#report-dot-bubble-thumbsup-form").serializeArray();

		var userId        = form_values[1]['value'];
		var date          = form_values[2]['value'];
		var orgId         = "{{base64_encode($orgId)}}"; 
		
		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getDotThumbsUpBubbleList')}}",
			data: {orgId:orgId,date:date,userId:userId,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{

				$('.loader-img').hide();
				$('#cot2').html('');
				$('#cot3').html('');
				$('#cot4').html('');
				$('#cot5').html('');
				$('#cot6').html('');			
				$('#cot7').html('');
				$('#cot8').html('');
				$('#cot16').html('');	
				$('#cot16').append(response);
			}
		});		
	});
</script>
<script type="text/javascript">
	$(document).on("keypress keyup blur",".numberControl",function (event) {    
		$(this).val($(this).val().replace(/[^\d].+/, ""));
		if ((event.which < 48 || event.which > 57)){
			event.preventDefault();
		}
	});
</script>



