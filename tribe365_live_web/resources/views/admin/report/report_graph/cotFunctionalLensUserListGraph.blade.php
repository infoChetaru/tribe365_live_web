<div class="parent-search-cot">
	<div class="search-cot">				
		<form id="report-functional-form" action="#">
			{{csrf_field()}}					
			<select id="functional-office" name="officeId">
				<option value="" selected>All Offices</option>
				@foreach($offices as $office)
				<option {{($officeId==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
				@endforeach
			</select>

			<select id="functional-department" name="departmentId">
				@if($departments && !empty($officeId))
				<option value="">All Department</option>
				@foreach($departments as $cdept)
				<option {{($departmentId==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
				@endforeach				
				@elseif($all_department)
				<option value="">All Department</option>
				@foreach($all_department as $dept)
				<option {{($departmentId==$dept->id)?'selected':''}} value="{{$dept->id}}">{{$dept->department}}</option>
				@endforeach	
				@endif
			</select>
			<button class="report-functional-form" type="button">Search</button>
		</form> 
	</div>
</div>
<div class="cot-content">
	<div class="cot-content-box">

		<div class="cot-inner-box sky-blue">
			<ul>
				<li> <strong> <?php if(!empty($keyNameArray[0][0]) && !empty($keyNameArray[0][1])) print_r($keyNameArray[0][0]);print_r($keyNameArray[0][1]); ?></strong></li>
				<li> <span>
					@if(!empty($funcLensPercentageArray['st']))
					{{$funcLensPercentageArray['st']}}%
					@else
					0%
					@endif
				</span> 
			</li>
		</ul>
	</div>	

	<div class="cot-inner-box yallow">
		<ul>
			<li> <strong><?php if(!empty($keyNameArray[3][0]) && !empty($keyNameArray[3][1])) print_r($keyNameArray[3][0]);print_r($keyNameArray[3][1]); ?></strong></li>
			<li> <span> @if(!empty($funcLensPercentageArray['sf']))
				{{$funcLensPercentageArray['sf']}}%
				@else
				0%
			@endif </span> 
		</li>
	</ul>
</div>

<div class="cot-inner-box red">
	<ul>
		<li> <strong> <?php if(!empty($keyNameArray[1][0]) && !empty($keyNameArray[1][1])) print_r($keyNameArray[1][0]);print_r($keyNameArray[1][1]); ?> </strong></li>
		<li> <span> @if(!empty($funcLensPercentageArray['nt']))
			{{$funcLensPercentageArray['nt']}}%
			@else
			0%
		@endif </span>
	</li>
</ul>
</div>

<div class="cot-inner-box light-red">
	<ul>
		<li> <strong> <?php if(!empty($keyNameArray[2][0]) && !empty($keyNameArray[2][1])) print_r($keyNameArray[2][0]);print_r($keyNameArray[2][1]); ?> </strong></li>
		<li> <span> @if(!empty($funcLensPercentageArray['nf']))
			{{$funcLensPercentageArray['nf']}}%
			@else
			0%
		@endif </span>
	</li>
</ul>
</div>

</div>
</div>

<script type="text/javascript">
	$('.report-functional-form').on('click',function(){
		$('.loader-img').show();			
			// get form values 
			var form_values   = $("#report-functional-form").serializeArray();
			
			var officeId      = form_values[1]['value'];
			var departmentId  = form_values[2]['value'];
			var orgId = "{{base64_encode($orgId)}}";

			var start_date = $('#start_date').val().trim();
			var end_date   = $('#end_date').val().trim();
			
			// console.log(form_values)
			$.ajax({
				type: "POST",
				url: "{{URL::to('admin/getReportFunctionalGraph')}}",
				data: {orgId:orgId,officeId:officeId,departmentId:departmentId,startDate:start_date,endDate:end_date,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					$('.loader-img').hide();
					$('#cot2').html('');
					$('#cot5').html('');
					$('#cot3').html('');
					$('#cot7').html('');	
					$('#cot7').append(response);
				}
			});		

		});
	</script>

	<script type="text/javascript">
		$('#functional-office').on('change', function() {
			$('#functional-department option').remove();
			var officeId = $('#functional-office').val();

			$.ajax({
				type: "POST",
				url: "{!!URL::to('admin/getDepartmentByOfficecIdforGraph')!!}",				
				data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
				success: function(response) 
				{					
					$('#functional-department').append('<option value="" selected>All Department</option>');
					$('#functional-department').append(response);
				}
			});
		});
	</script>