<div class="search-cot">				
	<form id="report-motivation-form" action="#">
		{{csrf_field()}}					
		<select id="motivation-office" name="officeId">
			<option value="" selected>All Offices</option>
			@foreach($offices as $office)
			<option {{($officeId==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
			@endforeach
		</select>
		<select id="motivation-department" name="departmentId">			
			@if($departments && !empty($officeId))
			<option value="">All Department</option>
			@foreach($departments as $cdept)
			<option {{($departmentId==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
			@endforeach
			@elseif($all_department)
			<option value="">All Department</option>
			@foreach($all_department as $dept)
			<option {{($departmentId==$dept->id)?'selected':''}} value="{{$dept->id}}">{{$dept->department}}</option>
			@endforeach	
			@endif
		</select>		
		<button class="report-motivation-form" type="button">Search</button>
	</form> 
</div>
<canvas style="margin-top: 20px;" id="chartMotivation" width="1000" height="400"></canvas>

<script type="text/javascript">
	$('.report-motivation-form').on('click',function(){
		$('.loader-img').show();			
		var form_values   = $("#report-motivation-form").serializeArray();

		var officeId      = form_values[1]['value'];
		var departmentId  = form_values[2]['value'];
		var orgId      = "{{base64_encode($orgId)}}";
		var start_date = $('#start_date').val().trim();
		var end_date   = $('#end_date').val().trim();
		
		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getReportMotivationalGraph')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,startDate:start_date,endDate:end_date,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				$('.loader-img').hide();
				$('#cot2').html('');
				$('#cot5').html('');
				$('#cot3').html('');
				$('#cot15').html('');	
				$('#cot15').append(response);
			}
		});		
	});
</script>

<script type="text/javascript">
	$('#motivation-office').on('change', function() {
		$('#motivation-department option').remove();
		var officeId = $('#motivation-office').val();

		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getDepartmentByOfficecIdforGraph')!!}",				
			data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{					
				$('#motivation-department').append('<option value="" selected>All Department</option>');
				$('#motivation-department').append(response);
			}
		});
	});
</script>

<?php 

$maxValueArray = array();
foreach($SOTmotivationResultArray as $value1)
{
	array_push($maxValueArray, $value1['percentage']);
}

$max=10;
if(!empty($maxValueArray))
{
	$max = max($maxValueArray);

	$max = ceil($max / 10) * 10;

	if($max>=100)
	{
		$max=100;
	}
}
?>

<!-- MOTIVATION CHART -->
<script  type="text/javascript">
	var ctx = document.getElementById("chartMotivation");

	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: [@foreach($SOTmotivationResultArray as $value)"<?php print_r(html_entity_decode($value['title']));?>",@endforeach],
			datasets: [{
				label: 'Percentage',
				data: [@foreach($SOTmotivationResultArray as $value1){{$value1['percentage']}},@endforeach],
				backgroundColor: [
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)', 
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)'
				],
				borderColor: [
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)'
				],
				borderWidth: 5
			}]
		},
		options: {
			legend: { display: false},			
			scales: {
				xAxes: [{
					categoryPercentage: 0.25,
					ticks: {
						fontSize: 15
					},
					gridLines: {
						display: false
					}

				}],
				yAxes: [{					
					ticks: {
						beginAtZero:true,
						step:10,
						max:{{$max}},
					},
					gridLines: {
						display: false
					}
				}]
			}
		}
	});
</script>
