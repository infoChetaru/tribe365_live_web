<!DOCTYPE html>
<html id="testing">
<head>
	<title>Tribe 365 - PDF </title>
	<link rel="shortcut icon" href="{{asset('public/images/Favicon-icon.png')}}" type="image/x-icon"/>
	<script
	src="<?php echo url('/public/extranal_js_css/jquery-3.3.1.js'); ?>" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/jsapi'); ?>"></script>
	<!-- google charts -->
	<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/loader.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/Chart.js'); ?>"></script>
	<style>
		html,
		#myChart {height: 100%;width: 100%;min-height: 150px;}
		.zc-ref {display: none;}
	</style>

	<style type="text/css">

		.dot-belief.lightgreen_bg {
			border-color: #ff454b;
			border: 2px solid #ff454b;
			border-top-left-radius: 12px;
			border-bottom: 8px solid #ff454b;
			border-top-right-radius: 12px;
		}
		.dot-belief {
			border-color: #eb1c24;
			border: 2px solid #eb1c24;
			border-top-left-radius: 12px;
			border-bottom: 8px solid #eb1c24;
			border-top-right-radius: 12px;
		}
		.dot-belief.red_bg {
			border-color:#b2b2b2;
			border: 2px solid #b2b2b2;
			border-top-left-radius: 12px;
			border-bottom: 8px solid #b2b2b2;
			border-top-right-radius: 12px;
		}
		.dot-belief.yellow_bg {
			border-color: #ff8085;
			border: 2px solid #ff8085;
			border-top-left-radius: 12px;
			border-bottom: 8px solid #ff8085;
			border-top-right-radius: 12px;
		}
		.dot-belief.orange_bg {
			border-color: #ffa3a6;
			border: 2px solid #ffa3a6;
			border-top-left-radius: 12px;
			border-bottom: 8px solid #ffa3a6;
			border-top-right-radius: 12px;
		}
		.dot-value-inner strong {background-color: #eb1c24;}
		.dot-value-inner.lightgreen_bg strong {background-color: #ff454b;}
		.dot-value-inner.red_bg strong {background-color: #b2b2b2; }
		.dot-value-inner.yellow_bg strong {background-color: #ff8085; }
		.dot-value-inner.orange_bg strong {background-color: #ffa3a6; }
		h4,h2 { font-family: "Helvetica","Arial",sans-serif; text-align: left;}
		h3 {
			font-size: 24px;
			font-family: "Helvetica","Arial",sans-serif;
		}
		@media print {
			body {
				-webkit-print-color-adjust: exact;
			}
		}
		form#make_pdf {
			max-width: 1189px;
			margin: 0 auto;
			text-align: right;
			position: fixed;
			top: 32px;
			right: 0;
			left: 0;
		}
		button#create_pdf {
			background-color: #eb1c24;
			font-size: 1rem;
			border: none;
			padding: 9px 20px 8px;
			color: #fff;
			border-radius: 5px;
			font-weight: 400;
		}
		.loader-img {
			position: fixed;
			left: 0;
			-ms-transform: translateX(-50%);
			width: 100% !important;
			background-color: rgba(239, 239, 239, 0.9);
			height: 100%;
			top: 0 !important;
			z-index: 9;
		}
		.loader-img img {
			position: absolute;
			top: 50%;
			transform: translateY(-50%);
			-webkit-transform: translateY(-50%);
			-ms-transform: translateY(-50%);
			left: 50%;
			transform: translateX(-50%);
			width: 97px;
		}
		@media print {
			.printPageButton {
				display: none;
			}
		}
	</style>
	<style type="text/css">
		.pie_chart_div svg>g:nth-child(3) {transform: translate(30px, 80px);}
	</style>
</head>
<body onload="init()">
	<button style="display: none;" id="test_export">Export Chart</button>
	<div class="pdf-section" style="width: 1000px; margin: 0 auto; text-align: center;">
		<div class="pdf-head" style="width: 100%; display: inline-block;margin-bottom: 40px;">
			<div class="pdf-img" style="width: 100%; display:inline-block; text-align: center;">		
				<img style="max-width: 82px; float: left; " src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/org_images'.'/'.$org->ImageURL))}}">
				<h2 style="float: left; padding-left: 20px;"> {{$org->organisation}} </h2>
			</div>	
		</div>
		<div class="loader-img printPageButton" style="display: none;width: 70px; top: 106px;"><img src="{{asset('public/images/loader.gif')}}"></div>




	<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content" >
		<div class="tital-name" style="width: 100%; display: inline-block; border-bottom: solid 1px #e6e6e6; clear: both;">
			<h3 style="margin-top: 20px;text-align: left;"> Index </h3>
		</div>


		<div style="clear: both; min-width: 1200px;">
			<div id="curve_chart"></div>
		</div>

	</div>	




		<div style="page-break-after:always; width: 100%; clear: both;" class="pdf-content" >
			<div class="tital-name" style="width: 100%; display: inline-block; border-bottom: solid 1px #e6e6e6; clear: both;">
				<h3 style="margin-top: 20px;text-align: left;"> Directing </h3>
			</div>
			<?php
			$numItems = count($dotValuesArray);
			$i = 0;
			?>
			@foreach($dotValuesArray as $belief1)
			@php 

			$beliefValuesCount = ceil(count($belief1['beliefValues'])/6);
			
			@endphp	
			<div class="dot-belief-value">
				<div class="belif-girap">
					<div class="belif-girap-inner">
						<div class="dot-belief <?php 

						$beliefRatings = $belief1['beliefRatings'];

						if($beliefRatings >=0 && $beliefRatings <1)
						{
							echo ("red_bg");
						}						                    
						elseif($beliefRatings >=1 && $beliefRatings <2)
						{
							echo("orange_bg");
						}
						elseif($beliefRatings >=2 && $beliefRatings <3)
						{
							echo("yellow_bg");
						}
						elseif($beliefRatings >=3 && $beliefRatings <4)
						{
							echo("lightgreen_bg");
						}
						elseif($beliefRatings >=4 && $beliefRatings <5)
						{
							echo("green_bg");
						}
						?>" style="width: 140px;padding: 2px 15px;margin-bottom: 20px;text-align: center; margin-top: 40px;">
						<h3 style="font-size: 14px;text-transform: uppercase;border-bottom: 2px solid #afafaf;padding: 0 0 15px 0;margin-top: 6px;margin-bottom: 15px; text-align: center; font-family: 'Helvetica','Arial',sans-serif;"> {{$belief1['beliefName']}} </h3>
						<strong style="padding-bottom: 14px;display: block;font-size: 13px;font-family: 'Helvetica','Arial',sans-serif;"> {{round($belief1['beliefRatings'],2)}} </strong>
					</div>	
					
				</div>
			</div>
			<div class="values-giraf">
				<div class="values-inner" style="width: 100%; display: inline-block; clear: both; margin-top: 40px;">
					<ul style="padding-left: 0; margin-top: 0;">
						@for($j=0; $j<$beliefValuesCount; $j++)	
						<?php 
						$counter =$j*6;
						

						?>
						<li style="display: inline-block; width: 100%; clear: both;">
							@for($k=$counter; $k < 6+$counter; $k++)	
							@if(!empty($belief1['beliefValues'][$k]['valueId']))
							<div class="dot-value-inner <?php 

							$beliefRatings = $belief1['beliefValues'][$k]['valueRatings'];

							if($beliefRatings >=0 && $beliefRatings <1)
							{
								echo ("red_bg");
							}						                    
							elseif($beliefRatings >=1 && $beliefRatings <2)
							{
								echo("orange_bg");
							}
							elseif($beliefRatings >=2 && $beliefRatings <3)
							{
								echo("yellow_bg");
							}
							elseif($beliefRatings >=3 && $beliefRatings <4)
							{
								echo("lightgreen_bg");
							}
							elseif($beliefRatings >=4 && $beliefRatings <5)
							{
								echo("green_bg");
							}
							?>" style="background-color: #f2f2f2;width: 140px;padding: 16px 0 0;border-radius: 10px;height: auto;margin-bottom: 20px;text-align: center; float: left; margin-right: 25px; margin-bottom: 20px;">
							<h4 style="font-size: 12px; text-transform: capitalize;padding: 0 0 15px 0;margin-bottom: 0;margin-top: 0;font-weight: 500;text-align: center; font-family: 'Helvetica','Arial',sans-serif;">{{$belief1['beliefValues'][$k]['valueName']}}</h4>
							<strong style="color: #fff;display: block;width: 100%;padding: 12px 0;border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;font-size: 12px;font-family: 'Helvetica','Arial',sans-serif;"> {{round($belief1['beliefValues'][$k]['valueRatings'],2)}} </strong>
						</div>
						@endif
						@endfor
					</li>
					@endfor		
				</ul>
			</div>
		</div>				
		</div>	
		<?php 

		if(++$i != $numItems)
		{
			echo '<div style="page-break-after:always;"></div>';
		}			
		?> 
		@endforeach
	</div>		

<div style="page-break-after:always;" class="pdf-content">
	<div class="tital-name" style="width: 100%;display: inline-block;border-bottom: solid 1px #e6e6e6;clear: both; margin-bottom: 10px;">
		<h3 style="text-align: left; margin-top: 0px;"> Connecting: Team Role</h3>
	</div>
	<div class="team-roles-content">
		<div style="display: flex;">
			@if(!empty($cotGraphConditionArr))
				<div class="motivation" id="chart_div"></div>
				<!-- <div class="motivation" id="chart-container"></div> -->
			@else
				<div class="motivation">No record found.</div>
			@endif
			
			<!-- <div id="chart_div"></div>
			<div id="chart-container"></div> -->
			<!-- <canvas style="margin-top: 20px;" id="myRadarChart" width="1500" height="400" style="margin-left: -20em;"></canvas> -->
		</div>
		<!-- <div class="cat-left-tital">
			<div class="team-roles-tital">
				<h4 style="margin-bottom: 5px; margin-top: 8px;">Organisation</h4>
				<span style="display: block; width:55px; height: 2px; background-color:#eb1c24; margin-bottom: 10px;">  </span>
			</div>
			<table style="width: 100%;">
				<tr>
					<td style="width: 90%;text-align: left;padding: 0;">
						<span style="border: solid 2px #272525;display: block;padding:12px 20px;border-radius: 3px; font-family:'Helvetica','Arial',sans-serif;"> {{$cotRoleMapOptions[0]->maper}} </span>
						<span style=" background-color: #e6e6e6;display: block;padding:12px 20px;border-radius: 3px;font-family:'Helvetica','Arial',sans-serif;"> {{$cotRoleMapOptions[1]->maper}} </span>
					</td>
					<td>
						<span style="border: solid 2px #272525;display: block;padding:12px 20px;border-radius: 3px;font-family:'Helvetica','Arial',sans-serif; width: 60px;" >{{$cotTeamRoleMapGraphPercentage['shaper']}}%</span>
						<span style=" background-color: #e6e6e6;display: block;padding:12px 20px;border-radius: 3px;font-family:'Helvetica','Arial',sans-serif; width: 60px;">{{$cotTeamRoleMapGraphPercentage['coordinator']}}%</span>
					</td>
				</tr>
			</table>
		</div>
		<div class="cat-left-tital">
			<div class="team-roles-tital">
				<h4 style="margin-bottom: 5px;margin-top: 8px;">Controllers</h4>
				<span style="display: block; width: 55px; height: 2px; background-color:#eb1c24; margin-bottom: 10px;">  </span>
			</div>
			<table style="width: 100%;">
				<tr>
					<td style="width: 90%;text-align: left;padding: 0;">
						<span style="border: solid 2px #272525;display: block;padding:12px 20px;border-radius: 3px; font-family:'Helvetica','Arial',sans-serif;"> {{$cotRoleMapOptions[2]->maper}} </span>
						<span style=" background-color: #e6e6e6;display: block;padding:12px 20px;border-radius: 3px;font-family:'Helvetica','Arial',sans-serif;"> {{$cotRoleMapOptions[3]->maper}} </span>
					</td>
					<td>
						<span style="border: solid 2px #272525;display: block;padding: 12px 20px;border-radius: 3px;font-family:'Helvetica','Arial',sans-serif; width: 60px;">{{$cotTeamRoleMapGraphPercentage['implementer']}}%</span>
						<span style=" background-color: #e6e6e6;display: block;padding:12px 20px;border-radius: 3px;font-family:'Helvetica','Arial',sans-serif; width: 60px;">{{$cotTeamRoleMapGraphPercentage['completerFinisher']}}%</span>
					</td>
				</tr>
			</table>
		</div>
		<div class="cat-left-tital">
			<div class="team-roles-tital">
				<h4 style="margin-bottom: 5px;margin-top: 8px;">Advisors</h4>
				<span style="display: block; width: 55px; height: 2px; background-color:#eb1c24; margin-bottom: 10px; ">  </span>
			</div>
			<table style="width: 100%;">
				<tr>
					<td style="width: 90%;text-align: left;padding: 0;">
						<span style="border: solid 2px #272525;display: block;padding: 12px 20px;border-radius: 3px; font-family:'Helvetica','Arial',sans-serif;"> {{$cotRoleMapOptions[4]->maper}} </span>
						<span style=" background-color: #e6e6e6;display: block;padding: 12px 20px;border-radius: 3px;font-family:'Helvetica','Arial',sans-serif;"> {{$cotRoleMapOptions[5]->maper}} </span>
					</td>
					<td>
						<span style="border: solid 2px #272525;display: block;padding:12px 20px;border-radius: 3px;font-family:'Helvetica','Arial',sans-serif; width: 60px;">{{$cotTeamRoleMapGraphPercentage['monitorEvaluator']}}%</span>
						<span style=" background-color: #e6e6e6;display: block;padding: 12px 20px;border-radius: 3px;font-family:'Helvetica','Arial',sans-serif; width: 60px;">{{$cotTeamRoleMapGraphPercentage['teamworker']}}%</span>
					</td>
				</tr>
			</table>
		</div>
		<div class="cat-left-tital">
			<div class="team-roles-tital">
				<h4 style="margin-bottom: 5px;margin-top: 8px;">Explorers</h4>
				<span style="display: block; width: 55px; height: 2px; background-color:#eb1c24; margin-bottom: 10px; ">  </span>
			</div>
			<table style="width: 100%;">
				<tr>
					<td style="width: 90%;text-align: left;padding: 0;">
						<span style="border: solid 2px #272525;display: block;padding: 12px 20px;border-radius: 3px; font-family:'Helvetica','Arial',sans-serif;"> {{$cotRoleMapOptions[6]->maper}} </span>
						<span style=" background-color: #e6e6e6;display: block;padding: 12px 20px;border-radius: 3px;font-family:'Helvetica','Arial',sans-serif;"> {{$cotRoleMapOptions[7]->maper}} </span>
					</td>
					<td>
						<span style="border: solid 2px #272525;display: block;padding:12px 20px;border-radius: 3px;font-family:'Helvetica','Arial',sans-serif; width: 60px;">{{$cotTeamRoleMapGraphPercentage['plant']}}%</span>
						<span style=" background-color: #e6e6e6;display: block;padding: 12px 20px;border-radius: 3px;font-family:'Helvetica','Arial',sans-serif; width: 60px;">{{$cotTeamRoleMapGraphPercentage['resourceInvestigator']}}%</span>
					</td>
				</tr>
			</table>
		</div> -->
	</div>
</div>

<div style="page-break-after:always;" class="pdf-content">
	<div class="tital-name" style="width: 100%;display: inline-block;border-bottom: solid 1px #e6e6e6;clear: both; margin-bottom: 20px;">
		<h3 style="text-align: left;">Connecting: Personality Type</h3>
	</div>
	<div class="cot-content-box">
		<table style="width: 100%;">
			<tr>
				<td style="padding: 0;width: 25%;">
					<div style="background-color: #ff8085 !important;margin: 0 10px;padding: 15px 15px 30px;border-bottom-left-radius: 0;position: relative;">
						<ul style="margin: 0;padding: 0;">
							<li style="list-style-type: none;text-align: center;font-weight: 200;color: #fff;padding: 16px 0;background: url({{asset('public/images/cot-b.png')}})no-repeat center bottom;margin-bottom: 10px;    background-size: 192px;"> <strong style="font-family:'Helvetica','Arial',sans-serif;font-size: 20px;"> <?php if(!empty($keyNameArray[0][0]) && !empty($keyNameArray[0][1])) print_r($keyNameArray[0][0]);print_r($keyNameArray[0][1]); ?> </strong></li>
							<li style="list-style-type: none;text-align: center;font-weight: 200;color: #fff;padding: 16px 0;background: url({{asset('public/images/cot-b.png')}})no-repeat center bottom;margin-bottom: 10px;    background-size: 192px;"> <span style="font-family:'Helvetica','Arial',sans-serif;font-size: 20px;">@if(!empty($funcLensPercentageArray['st']))
								{{$funcLensPercentageArray['st']}}%
								@else
								0%
							@endif</span></li>
						</ul>
					</div>
				</td>
				<td style="padding: 0;width: 25%;">
					<div style="background-color:#ff454b;margin: 0 10px;padding: 15px 15px 30px;border-bottom-left-radius: 0;position: relative;">
						<ul style="margin: 0;padding: 0;">
							<li style="list-style-type: none;text-align: center;font-weight: 200;color: #fff;padding: 16px 0;background: url({{asset('public/images/cot-b.png')}})no-repeat center bottom;margin-bottom: 10px;    background-size: 192px;"> <strong style="font-family:'Helvetica','Arial',sans-serif;font-size: 20px;"> <?php if(!empty($keyNameArray[3][0]) && !empty($keyNameArray[3][1])) print_r($keyNameArray[3][0]);print_r($keyNameArray[3][1]); ?> </strong></li>
							<li style="list-style-type: none;text-align: center;font-weight: 200;color: #fff;padding: 16px 0;background: url({{asset('public/images/cot-b.png')}})no-repeat center bottom;margin-bottom: 10px;    background-size: 192px;"> <span style="font-family:'Helvetica','Arial',sans-serif;font-size: 20px;">
								@if(!empty($funcLensPercentageArray['sf']))
								{{$funcLensPercentageArray['sf']}}%
								@else
								0%
							@endif</span></li>
						</ul>
					</div>
				</td>
				<td style="padding: 0;width: 25%;">
					<div style="background-color:#ff8085;margin: 0 10px;padding: 15px 15px 30px;border-bottom-left-radius: 0;position: relative;">
						<ul style="margin: 0;padding: 0;">
							<li style="list-style-type: none;text-align: center;font-weight: 200;color: #fff;padding: 16px 0;background: url({{asset('public/images/cot-b.png')}})no-repeat center bottom;margin-bottom: 10px;    background-size: 192px;"> <strong style="font-family:'Helvetica','Arial',sans-serif;font-size: 20px;"> <?php if(!empty($keyNameArray[1][0]) && !empty($keyNameArray[1][1])) print_r($keyNameArray[1][0]);print_r($keyNameArray[1][1]); ?> </strong></li>
							<li style="list-style-type: none;text-align: center;font-weight: 200;color: #fff;padding: 16px 0;background: url({{asset('public/images/cot-b.png')}})no-repeat center bottom;margin-bottom: 10px;    background-size: 192px;"> <span style="font-family:'Helvetica','Arial',sans-serif;font-size: 20px;">@if(!empty($funcLensPercentageArray['nt']))
								{{$funcLensPercentageArray['nt']}}%
								@else
								0%
							@endif</span></li>
						</ul>
					</div>
				</td>
				<td style="padding: 0;width: 25%;">
					<div style="background-color:#ff454b;margin: 0 10px;padding: 15px 15px 30px;border-bottom-left-radius: 0;position: relative;">
						<ul style="margin: 0;padding: 0;">
							<li style="list-style-type: none;text-align: center;font-weight: 200;color: #fff;padding: 16px 0;background: url({{asset('public/images/cot-b.png')}})no-repeat center bottom;margin-bottom: 10px;    background-size: 192px;"> <strong style="font-family:'Helvetica','Arial',sans-serif; font-size: 20px;"> <?php if(!empty($keyNameArray[2][0]) && !empty($keyNameArray[2][1])) print_r($keyNameArray[2][0]);print_r($keyNameArray[2][1]); ?> </strong></li>
							<li style="list-style-type: none;text-align: center;font-weight: 200;color: #fff;padding: 16px 0;background: url({{asset('public/images/cot-b.png')}})no-repeat center bottom;margin-bottom: 10px;    background-size: 192px;"> <span style="font-family:'Helvetica','Arial',sans-serif;font-size: 20px;">@if(!empty($funcLensPercentageArray['nf']))
								{{$funcLensPercentageArray['nf']}}%
								@else
								0%
							@endif</span></li>
						</ul>
					</div>
				</td>
			</tr>
		</table>
	</div>
</div>

<div style="page-break-after:always;" class="pdf-content">
	<div class="tital-name"  style="width: 100%;display: inline-block;border-bottom: solid 1px #e6e6e6;clear: both;margin-bottom: 20px;">
		<h3 style="text-align: left;"> Supercharging: Culture Structure </h3>
	</div>
	<div class="sot-value">
		<table style="width:100%;">
			<tr> 
				
				@if(!empty($sotStrDetailArr[0]->SOTCount))
				@foreach($sotCountArray as $value)
				<td style="width: 25%;padding-left: 0;">
					<div style="border:solid 1px #dedddd;">
						<h3 style="font-size: 20px;text-transform: uppercase;text-align: center; margin: 0;color: #fff;<?php
						if($value->title=="Person")
						{
							echo("background-color:#eb1c24;");
						}
						elseif($value->title=="Power")
						{
							echo("background-color:#eb1c24;");
						}
						elseif($value->title=="Role")
						{
							echo("background-color:#eb1c24;");
						}
						elseif($value->title=="Tribe")
						{
							echo("background-color:#eb1c24;");
						}
						?>font-family:'Helvetica','Arial',sans-serif; font-weight: 200; padding: 13px;"> {{$value->title}} </h3>
						<strong style="font-size: 20px;font-family:'Helvetica','Arial',sans-serif; padding: 40px 0; display: block; text-align: center;">  {{!empty($value->SOTCount)?$value->SOTCount:0}}  </strong>
					</div>
				</td>
				@endforeach
				@endif
			</tr>
		</table>
	</div>
	@if(!empty($sotStrDetailArr[0]->SOTCount)) 
	<div class="sot-content" style="margin-top: 30px;">
		<table style="border: solid 1px #ececec;">
			<tr>
				<td style="width: 30%;border-right: solid 1px #ececec; padding-top: 50px; text-align: center;" valign="middle">
					<img src="data:image/png;base64, {{base64_encode(file_get_contents('public/uploads/sot'.'/'.$sotStrDetailArr[0]->imgUrl))}}">							
				</td>
				<td style="width: 70%;">
					<ul style="text-align: left;">								
						@foreach($sotStrDetailArr[0]->summary as $summValue)
						<li style="font-family:'Helvetica','Arial',sans-serif; font-weight:200; padding-bottom: 10px;">{{$summValue->summary}}</li>
						@endforeach								
					</ul>
				</td>
			</tr>
		</table>
	</div>
	@else
	<div class="sot-content">No record found.</div>
	@endif
</div>
<div style="page-break-after:always;" class="pdf-content">
	<div class="tital-name"  style="width: 100%;display: inline-block;border-bottom: solid 1px #e6e6e6;clear: both; margin-bottom: 20px;">
		<h3 style="text-align: left;"> Supercharging: Motivation </h3>
	</div>	
	@if(!empty(count($SOTmotivationResultArray)))
	<div class="motivation" id="chartMotivation"></div>
	@else
	<div class="motivation">No record found.</div>
	@endif
</div>
<div style="page-break-after:always;" class="pdf-content">
	<div class="tital-name" style="width: 100%;display: inline-block;border-bottom: solid 1px #e6e6e6;clear: both; margin-bottom: 20px;">
		<h3 style="text-align: left;"> Diagnostics </h3>
	</div>
	<!-- @if(!empty(count($diagnosticResultArray)))
	<div class="motivation" id="myChartDiagnostic"></div>
	@else
	<div class="motivation">No record found.</div>
	@endif -->
	@if($diagnosticQueCatTbl)
		@foreach($diagnosticQueCatTbl as $cate)
		<div>
			<h4 style="">{{$cate->title}}</h4>
			<div style="display: flex;">
				<!-- <div id="chart_div{{$cate->id}}" class="chart_div"></div>
				<div id="pie_chart_div{{$cate->id}}" class="pie_chart_div"></div>				 -->

				<table style="width: 100%;">
					<tr>
						<td style="width: 50%;">
							<div id="chart_div{{$cate->id}}" class="chart_div"></div>
						</td>
						<td style="width: 50%;">
							<table style="width: 110%;">
							<tr>
							<td style="width: 110%;">
							<div id="pie_chart_div{{$cate->id}}" class="pie_chart_div"></div>	
							</td>
							</tr>
							</table>
						</td>
					</tr>
				</table>	
			</div>
		</div>
		@endforeach()
	@endif
</div>

<div  class="pdf-content">
	<div class="tital-name" style="width: 100%;display: inline-block;border-bottom: solid 1px #e6e6e6;clear: both; margin-bottom: 20px;">
		<h3 style="text-align: left;"> Tribeometer </h3>
	</div>
	@if(!empty(count($tribeometerResultArray)))
	<div class="motivation" id="myChartTribeometer"></div>
	@else
	<div class="motivation">No record found.</div>
	@endif
</div>

<div style="page-break-after:always;" class="pdf-content">
	<div class="tital-name" style="width: 100%;display: inline-block;border-bottom: solid 1px #e6e6e6;clear: both; margin-bottom: 20px;">
		<h3 style="text-align: left;"> Happy Index </h3>
	</div>
	@if(!empty(count($happyIndexCountYear)))
	<div class="motivation" id="myChartHappyIndex"></div>
	@else
	<div class="motivation">No record found.</div>
	@endif
</div>
</div>

<form action="{{URL::to('admin/exportPdf')}}" id="make_pdf" method="post">
	{{csrf_field()}}
	<input type="hidden" name="hidden_html" id="hidden_html">
	<input type="hidden" name="orgId" value="{{base64_encode($org->id)}}">	

	@if(Request::segment(2)=='pdfview')
	@else
	<button style="cursor: pointer;" class="printPageButton" type="button" name="create_pdf" id="create_pdf">Export</button>
	@endif
</form>

<style>
	.raphael-group-88-dataset-Label-group{
		display: none !important;
	}
	.raphael-group-78-common-elems-group path{
		fill: #eb1c24 !important;
	}
	.raphael-group-fpJqCkwO{
		display: none !important;
	}
</style>


<!--
<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/fusioncharts.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/fusioncharts.theme.fusion.js'); ?>"></script>
-->


<script type="text/javascript">
	$(document).ready(function(){
		$('#create_pdf').click(function(){
			$('#create_pdf').hide();
			$('#valueChart710').remove();
			$('#valueChart705').remove();
			$('#valueChart1477').remove();
			$('#valueChart1054').remove();
			$('#valueChart1524').remove();
			$('#hidden_html').val($('#testing').html());			
			$('#make_pdf').submit();
			$('.loader-img').show();
		});
	});
</script>
<script type="text/javascript">
	$(function(){
		$('#test_export').click();
	});	
	function init() {
		google.load("visualization", "1.1", { packages:["corechart"], callback: 'drawCharts' });
	}
	function drawCharts() {
		drawTribeometer('myChartTribeometer');
		drawDiagnosticsLineGraph();
		drawDiagnosticsPieChart();
		drawMotivation('chartMotivation');
		drawHappyIndex('myChartHappyIndex');
		drawChart('curve_chart');
		drawMultSeries('chart_div');
	}	
</script>


<script type="text/javascript">


// google.charts.load('current', {packages: ['corechart', 'bar']});
// google.charts.setOnLoadCallback(drawMultSeries);

function drawMultSeries(containerId) {
      var data = google.visualization.arrayToDataTable([
  		['', '%'],
  		// ['', '%',{ role: 'style' }],
		@foreach($cotGraphArr as $key => $value)
		
		["{{$value['option']}}",{{ucfirst($value['perArr'])}}],
		// ["{{$value['option']}}",{{ucfirst($value['perArr'])}},'{{$value['color']}}'],
		@endforeach
      ]);
      var options = {
			width: 1000,
			height: 400,
			colors: ['#eb1c24'],
			hAxis: {
				titleTextStyle: {
					color: '#333'
				}
			},
			vAxis: {
				minValue: 0,
				viewWindow: {
			        min: 0,
			        max: 300
			    }
			},
			curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				width: '80%'
			},
			legend: 'none'
		};
		var chart_id = document.getElementById(containerId);
		var chart    = new google.visualization.ColumnChart(chart_id);
      	// var chart = new google.visualization.ColumnChart(document.getElementById(chart_id));

      	google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
      	chart.draw(data, options);
    }


</script>



<!-- 
<script type="text/javascript">
	FusionCharts.ready(function(){
		var chartObj = new FusionCharts({
			type: 'radar',
			renderAt: 'chart-container',
			width: '600',
			height: '350',
			dataFormat: 'json',
		    scale: {
		        angleLines: {
		            display: false
		        },
		    },
			dataSource: {
				"chart": {
					"caption": "",
					"subCaption": "",
					//"numberPreffix": "$",
					"theme": "fusion",
					"radarfillcolor": "#ffffff",
				},
				"categories": [{
					"category": [
						@foreach($cotRadarChartArr as $key => $value)
							{"label" : "{{ucfirst($value['option'])}}"},
						@endforeach
					]
				}],
				"dataset": [{
					"seriesname": "",
					"data": [{radius: 0},
						@foreach($cotRadarChartArr as $key => $value)
							{"value" : "{{ucfirst($value['perArr'])}}"},
						@endforeach
					]
				}]
			}
		});
		chartObj.render();
	});
</script> -->


<script type="text/javascript">

function drawDiagnosticsLineGraph() {

	<?php $a = 1;?>
	@foreach($diagnosticFinalArray as $diagFinalArray)
		var data = google.visualization.arrayToDataTable([
			['', '(%)'],
			@foreach($diagFinalArray as $diagArray)
				@if (!empty($diagArray['data'])) 	
					@if(!empty($organisation))
						@if($organisation->include_weekend == 1)
							["{{$diagArray['monthName']}}",{{$diagArray['data']->with_weekend}}],
						@elseif($organisation->include_weekend != 1)
							["{{$diagArray['monthName']}}",{{$diagArray['data']->without_weekend}}],
						@endif
					@endif
				@else
					["{{$diagArray['monthName']}}",0],
				@endif
			@endforeach
		]);
		

		var options = {
			 width: '100%',
			 height: '100%',
			colors: ['#eb1c24'],
			hAxis: {
				titleTextStyle: {
					color: '#333'
				}
			},
			vAxis: {
				minValue: 0,
				viewWindow: {
					min: 0,
					max: 100
				}
			},
			curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				width: '100%'
			},
			legend: 'none'
		};

		var chart_id = document.getElementById('chart_div{{$a}}');
		var chart    = new google.visualization.LineChart(chart_id);

		// var chart = new google.visualization.LineChart(document.getElementById('chart_div{{$a}}'));

		google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
		chart.draw(data, options);

		<?php $a++;?>
	@endforeach
}

function drawDiagnosticsPieChart() {

	<?php $b = 1;?>
	@foreach($subDiagFinalArray as $subDiagArray)
		var data = google.visualization.arrayToDataTable([
			['', ''],
			@foreach($subDiagArray as $subdiag)
				<?php
				$measure = $subdiag['measure'];
				$measure = str_replace("(%)", "", $measure);
				$measure = $measure." (".floatval($subdiag['data'])." %)";
				?>
				["{{ucfirst($measure)}}",{{$subdiag['data']}}],
			@endforeach
		]);
		var options = {
			pieSliceText: 'value',
			//legend: 'none',
			width: '100%',
			height: '100%',
			tooltip: {
	            text: 'value'
	        },
	        slices: {
	            0: { color: '#ec2127' },
	            1: { color: '#f16465' },
	            2: { color: '#939598' },
	            3: { color: '#bfbfbf' },
	            4: { color: '#27af57' },
	            5: { color: '#7ecc9c' },
          	},
			is3D: true

          //title: 'My Daily Activities'
        };
		var chart_id = document.getElementById('pie_chart_div{{$b}}');
		var chart    = new google.visualization.PieChart(chart_id);
		// var chart = new google.visualization.PieChart(document.getElementById('pie_chart_div{{$b}}'));
		google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
		chart.draw(data, options);

		<?php $b++;?>
	@endforeach
}


</script>

<!-- <script  type="text/javascript">
@if(!empty($cotGraphConditionArr))
	var canvas = document.getElementById("myRadarChart");
	var ctx = canvas.getContext("2d");
	var myChart = new Chart(ctx, {
		type: 'radar',
		data: {
			labels: [@foreach($cotRadarChartArr as $value)"{{$value['option']}}",@endforeach],
			datasets: [{
				label: 'Percentage',
				data: [@foreach($cotRadarChartArr as $value1){{ucfirst($value1['perArr'])}},@endforeach],
				backgroundColor: '#eb1c24',
				borderColor: '#eb1c24',
				//borderWidth: 5
			}]
		},
		options: {			
			legend: { display: false},	
		}

	});
@endif
</script> -->


<?php 

$maxValueArray = array();
foreach($SOTmotivationResultArray as $value1)
{
	array_push($maxValueArray, $value1['percentage']);
}

$max=10;
if(!empty($maxValueArray))
{
	$max = max($maxValueArray);

	$max = ceil($max / 10) * 10;

	if($max>=100)
	{
		$max=100;
	}
}
?>

<!-- MOTIVATION CHART -->
<script  type="text/javascript">
	function drawMotivation(containerId) {
		var data = google.visualization.arrayToDataTable([
			['', ''],
			@foreach($SOTmotivationResultArray as $value)
			["{{$value['title']}}",{{$value['percentage']}}],
			@endforeach

			]);
		var options = {
			width: 700,
			height: 400,
			colors: ['#ff454b'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: { minValue: 0 },
			curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				height: '70%',
				width: '100%'
			},
			legend: 'none'
		};
		/*var chart = new google.visualization.ColumnChart(document.getElementById(containerId));
		chart.draw(data, options);
		*/

		var chart_id = document.getElementById(containerId);
		var chart    = new google.visualization.ColumnChart(chart_id);
		
		google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
		chart.draw(data, options);
	}

</script>

<?php 

$maxValueArray = array();
foreach($diagnosticResultArray as $value1)
{
	array_push($maxValueArray, $value1['percentage']);
}

$max=10;
if(!empty($maxValueArray))
{
	$max = max($maxValueArray);

	$max = ceil($max / 10) * 10;

	if($max>=100)
	{
		$max=100;
	}
}

?>

<!-- DIAGNOSTIC GRAPH -->
<!-- <script  type="text/javascript">
	function drawDiagnostics(containerId) {
		var data = google.visualization.arrayToDataTable([
			['', ''],
			@foreach($diagnosticResultArray as $value)
			["{{$value['title']}}",{{$value['percentage']}}],
			@endforeach

			]);
		var options = {
			width: 700,
			height: 400,
			colors: ['#ff454b'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: { minValue: 0 },
			curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				height: '70%',
				width: '100%'
			},
			legend: 'none'
		};
		/*var chart = new google.visualization.ColumnChart(document.getElementById(containerId));
		chart.draw(data, options);*/


		var chart_id = document.getElementById(containerId);
		var chart    = new google.visualization.ColumnChart(chart_id);
		
		google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
		chart.draw(data, options);
	}
</script> -->


<?php 

$maxValueArray = array();
foreach($tribeometerResultArray as $value1)
{
	array_push($maxValueArray, $value1['percentage']);
}

$max=10;
if(!empty($maxValueArray))
{
	$max = max($maxValueArray);

	$max = ceil($max / 10) * 10;

	if($max>=100)
	{
		$max=100;
	}
}
?>

<!-- TRIBEOMETER GRAPH -->
<script type="text/javascript">


	function drawTribeometer(containerId) {
		var data = google.visualization.arrayToDataTable([
			['', ''],
			@foreach($tribeometerResultArray as $value)
			["{{$value['title']}}",{{$value['percentage']}}],
			@endforeach

			]);
		var options = {
			width: 700,
			height: 400,
			colors: ['#ff454b'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: { minValue: 0 },
			curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				height: '70%',
				width: '100%'
			},
			legend: 'none'
		};
		/*var chart = new google.visualization.ColumnChart(document.getElementById(containerId));
		google.visualization.events.addListener(chart, 'ready', function(){
			containerId.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
		chart.draw(data, options);*/


		var chart_id = document.getElementById(containerId);
		var chart    = new google.visualization.ColumnChart(chart_id);

		google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
		chart.draw(data, options);

	}
</script>

<!-- Happy Index Graph -->
<script type="text/javascript">
	function drawHappyIndex(containerId) {
		var data = google.visualization.arrayToDataTable([
      		['', 'Sad(%)','Average(%)', 'Happy(%)'],
			@foreach($happyIndexCountYear as $value)
				["{{$value['year']}}",{{$value['indexCount'][0]}},{{$value['indexCount'][1]}},{{$value['indexCount'][2]}}],
			@endforeach
      	]);
      	
		var options = {
			width: 700,
			height: 400,
			colors: ['#ffb4b7','#ff6469','#eb1c24'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: { minValue: 0 },
			curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				height: '70%',
				width: '70%'
			},
			//legend: 'none'
		};
		
		var chart_id = document.getElementById(containerId);
		var chart    = new google.visualization.ColumnChart(chart_id);

		google.visualization.events.addListener(chart, 'ready', function(){
			chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
		});
		chart.draw(data, options);

	}
</script>

<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
	$('.loader-img-graph').hide();
	var data = google.visualization.arrayToDataTable(
    [
      ["Index","Index"]
        <?php 
        $c    = 1;
        $tArr = count($graphVal);
        foreach($graphVal as $key=>$value) {
          if($tArr>=$c){ ?>,<?php } 
        ?>
          ["<?php echo $key; ?>",<?php echo (int)$value; ?>]
        <?php $c++; } ?>
    ]
  );

	var options = {
		//width: 1000,
		//height: 400,
		colors: ['#eb1c24'],
		hAxis: {
			title: '',
			titleTextStyle: {
				color: '#eb1c24'
			},
			textStyle : {
	            fontSize: 11
	        }
			
		},
		vAxis: { 
			minValue: 0,
			viewWindow: {
		        min: 0,
		        <?php
		        	if (count(array_filter($graphVal)) == 0) {
		        ?>
		        max : 10
		        <?php } ?>
		        //max: 1000
		    },
		    textStyle : {
	            fontSize: 11
	        }
		},
		curveType: 'function',
		chartArea: {
			top: 40,
			left: 40,
			//height: '100%',
			//width: '100%'
		},
		legend: 'none'
	};


  var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
  chart.draw(data, options);

  	var containerId='curve_chart';
	var chart_id = document.getElementById(containerId);
	var chart    = new google.visualization.LineChart(chart_id);

	google.visualization.events.addListener(chart, 'ready', function(){
		chart_id.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
	});
	chart.draw(data, options);

}
</script>

</body>
</html>