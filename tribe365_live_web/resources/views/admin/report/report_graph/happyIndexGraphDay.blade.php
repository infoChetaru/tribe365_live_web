
					
<!-- <canvas style="margin-top: 20px;" id="chart_div" width="1000" height="400"></canvas> -->
<style type="text/css">
	.search-cot{
		float: none!important;
	}
</style>
<div class="search-cot">				
	<form id="report-happy-index-form" action="#">
		{{csrf_field()}}					
		<select id="happy-index-office" name="officeId">
			<option value="" selected>All Offices</option>
			@foreach($offices as $office)
			<option {{($officeId==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
			@endforeach
		</select>
		<select id="happy-index-department" name="departmentId">			
			@if($departments && !empty($officeId))
			<option value="">All Department</option>
			@foreach($departments as $cdept)
			<option {{($departmentId==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
			@endforeach
			@elseif($all_department)
			<option value="">All Department</option>
			@foreach($all_department as $dept)
			<option {{($departmentId==$dept->id)?'selected':''}} value="{{$dept->id}}">{{$dept->department}}</option>
			@endforeach	
			@endif
		</select>	
		<select id="happyIndexWeek" name="happyIndexWeek">
			<option value="">All</option>			
			@foreach($weekList as $weeks)
			<option {{($week==$weeks)?'selected':''}} value="{{$weeks}}">{{$weeks}}</option>
			@endforeach
		</select>	
		
		<button class="report-happy-index-form" type="button">Search</button>
	</form> 
</div>
  <div id="chart_div"></div>
  <div id="error"></div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawMultSeries);
@php
	if (!empty($happyIndexCountDay)) { 
@endphp

function drawMultSeries() {
      var data = google.visualization.arrayToDataTable([
      		['', 'Sad(%)','Average(%)', 'Happy(%)'],
			@foreach($happyIndexCountDay as $value)
				["{{$value['dayName']}}",{{$value['dayCount'][0]}},{{$value['dayCount'][1]}},{{$value['dayCount'][2]}}],
			@endforeach
      ]);

       var options = {
			width: 1000,
			height: 400,
			colors: ['#ffb4b7','#ff6469','#eb1c24'],
			hAxis: {
				title: '',
				titleTextStyle: {
					color: '#333'
				}
			},
			vAxis: { minValue: 0,
				viewWindow: {
			        min: 0,
			        max: 100
			    }
			},
			curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				//height: '100%',
				width: '80%'
			},
			//legend: 'none'
		};

      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }
@php
}
else
{ @endphp
 $('#chart_div').css('display','none');
$('#error').html("<span>No record found.</span>");

// $('#cot17').html("<span>No record found.</span>");

@php } @endphp
</script>

<script type="text/javascript">
	$('.report-happy-index-form').on('click',function(){
		$('.loader-img').show();			

		var form_values   = $("#report-happy-index-form").serializeArray();

		var officeId      = form_values[1]['value'];
		var departmentId  = form_values[2]['value'];
		var weeks  		  = form_values[3]['value'];
		var orgId         = "{{base64_encode($orgId)}}";
		var year = {{$year}};
		var monthName = "{{$monthName}}";
		var month = {{$month}};
		
		console.log(form_values);
		if (weeks) {			
			$.ajax({
					type: "POST",
					url: "{{URL::to('admin/getReportHappyIndexGraphDay')}}",
					data: {orgId:orgId,month:month,year:year,officeId:officeId,departmentId:departmentId,week:weeks,"_token":'<?php echo csrf_token()?>'},
					success: function(response)
					{
						console.log(response);
						$('.loader-img').hide();
						$('#cot2').html('');
						$('#cot5').html('');
						$('#cot3').html('');
						//$('#cot6').html('');	
						$('#cot17').html('');	
						$('#cot17').append(response);
					}
				});
		}
		else
		{
			// $('.report-happy-index').click();
			$.ajax({
				type: "POST",
				url: "{{URL::to('admin/getReportHappyIndexGraphWeek')}}",
				data: {orgId:orgId,month:monthName,year:year,officeId:officeId,departmentId:departmentId,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					console.log(response);
					$('.loader-img').hide();
					$('#cot2').html('');
					$('#cot5').html('');
					$('#cot3').html('');
					//$('#cot6').html('');	
					$('#cot17').html('');
					$('#cot17').append(response);
				}
			});
		}
	});
</script>


<script type="text/javascript">
	$('#happy-index-office').on('change', function() {
		$('#happy-index-department option').remove();
		var officeId = $('#happy-index-office').val();

		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getDepartmentByOfficecIdforGraph')!!}",				
			data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{					
				$('#happy-index-department').append('<option value="" selected>All Department</option>');
				$('#happy-index-department').append(response);
			}
		});
	});
</script>