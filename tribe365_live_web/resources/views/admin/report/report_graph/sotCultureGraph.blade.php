<div class="parent-search-cot">	
	<div class="search-cot">				
		<form id="report-culture-form" action="#">
			{{csrf_field()}}					
			<select id="culture-office" name="officeId">
				<option value="" selected>All Offices</option>
				@foreach($offices as $office)
				<option {{($officeId==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
				@endforeach
			</select>
			<select id="culture-department" name="departmentId">				
				@if($departments && !empty($officeId))
				<option value="">All Department</option>
				@foreach($departments as $cdept)
				<option {{($departmentId==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
				@endforeach
				@elseif($all_department)
				<option value="">All Department</option>
				@foreach($all_department as $dept)
				<option {{($departmentId==$dept->id)?'selected':''}} value="{{$dept->id}}">{{$dept->department}}</option>
				@endforeach	
				@endif
			</select>		
			<button class="report-culture-form" type="button">Search</button>
		</form> 
	</div>
</div>
<div class="row sot-value">
	@if(!empty($sotStrDetailArr[0]->SOTCount))
	@foreach($sotCountArray as $value)
	<div class="col-md-3">
		<div class="sot-value-inner">
			<div class="heading-text
			<?php
			if($value->title=='Power')
			{
				echo('blue-color');
			}
			elseif($value->title=='Role')
			{
				echo('yallow-color');
			}
			elseif($value->title=='Tribe')
			{
				echo('green-color');
			}
			?>
			"> 
			<h2> {{$value->title}} </h2>
		</div>
		<div class="sot-value-text">
			<strong> {{!empty($value->SOTCount)?$value->SOTCount:0}} </strong>
		</div>
	</div>
</div> 
@endforeach
@endif
</div>

@if(empty($sotStrDetailArr[0]->SOTCount))

<span class="empty-data">Culture Structure Questionnaire are not submitted by any members of organisation yet.</span>

@endif

@if(!empty($sotStrDetailArr[0]->SOTCount)) 

<div class="row sot-content"> 
	<div class="col-md-3 image-sot">               
		<img width="100px" height="100px" src="{{asset('public/uploads/sot\/').$sotStrDetailArr[0]->imgUrl}}">        
	</div>
	<div class="col-md-9">
		<ul>
			@foreach($sotStrDetailArr[0]->summary as $summValue)
			<li>{{$summValue->summary}}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif	

<script type="text/javascript">

	$('.report-culture-form').on('click',function(){
		$('.loader-img').show();			
			// get form values 
			var form_values   = $("#report-culture-form").serializeArray();
			
			var officeId      = form_values[1]['value'];
			var departmentId  = form_values[2]['value'];
			var orgId = "{{base64_encode($orgId)}}";

			var start_date = $('#start_date').val().trim();
			var end_date   = $('#end_date').val().trim();
			
			$.ajax({
				type: "POST",
				url: "{{URL::to('admin/getReportCultureGraph')}}",
				data: {orgId:orgId,officeId:officeId,departmentId:departmentId,startDate:start_date,endDate:end_date,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					$('.loader-img').hide();
					$('#cot2').html('');
					$('#cot5').html('');
					$('#cot3').html('');
					$('#cot4').html('');	
					$('#cot4').append(response);
				}
			});
		});

	</script>

	<script type="text/javascript">
		$('#culture-office').on('change', function() {
			$('#culture-department option').remove();
			var officeId = $('#culture-office').val();

			$.ajax({
				type: "POST",
				url: "{!!URL::to('admin/getDepartmentByOfficecIdforGraph')!!}",				
				data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
				success: function(response) 
				{					
					$('#culture-department').append('<option value="" selected>All Department</option>');
					$('#culture-department').append(response);
				}
			});
		});
	</script>