
					
<!-- <canvas style="margin-top: 20px;" id="chart_div" width="1000" height="400"></canvas> -->

<style type="text/css">
	.search-cot{
		float: none!important;
	}
</style>

<div class="search-cot">				
	<form id="report-happy-index-form" action="#">
		{{csrf_field()}}					
		<select id="happy-index-office" name="officeId">
			<option value="" selected>All Offices</option>
			@foreach($offices as $office)
			<option {{($officeId==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
			@endforeach
		</select>

		<select id="happy-index-department" name="departmentId">			
			@if($departments && !empty($officeId))
			<option value="">All Department</option>
			@foreach($departments as $cdept)
			<option {{($departmentId==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
			@endforeach
			@elseif($all_department)
			<option value="">All Department</option>
			@foreach($all_department as $dept)
			<option {{($departmentId==$dept->id)?'selected':''}} value="{{$dept->id}}">{{$dept->department}}</option>
			@endforeach	
			@endif
		</select>

		<button class="report-happy-index-form" type="button">Search</button>
	</form> 
</div>	
  <div id="chart_div"></div>
  <div id="error"></div>

<!-- google charts -->
<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/loader.js'); ?>"></script>



<script type="text/javascript">


google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawMultSeries);
@php

if (!empty($happyIndexCountYear)) {
@endphp
function drawMultSeries() {
      var data = google.visualization.arrayToDataTable([
      		['', 'Sad(%)','Average(%)', 'Happy(%)'],
			@foreach($happyIndexCountYear as $value)
				["{{$value['year']}}",{{$value['indexCount'][0]}},{{$value['indexCount'][1]}},{{$value['indexCount'][2]}}],
			@endforeach
      ]);
      var options = {
			width: 1000,
			height: 400,
			colors: ['#ffb4b7','#ff6469','#eb1c24'],
			hAxis: {
				titleTextStyle: {
					color: '#333'
				}
			},
			vAxis: {
				minValue: 0,
				viewWindow: {
			        min: 0,
			        max: 100
			    }
			},
			curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				width: '80%'
			},
			//legend: 'none'
		};

      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
      chart.draw(data, options);

      google.visualization.events.addListener(chart, 'select', selectHandler);

    function selectHandler() {

      	var selection = chart.getSelection();
		var message = '';
		for (var i = 0; i < selection.length; i++) {
			var item = selection[i];
			if (item.row != null && item.column != null) {
				var str = data.getFormattedValue(item.row, item.column);
				var year = data.getValue(chart.getSelection()[0].row, 0);

				//console.log(year);

				var form_values   = $("#report-happy-index-form").serializeArray();
				var officeId      = form_values[1]['value'];
				var departmentId  = form_values[2]['value'];

				$('.loader-img').show();
				var orgId = "{{base64_encode($orgId)}}";
				
				$.ajax({
					type: "POST",
					url: "{{URL::to('admin/getReportHappyIndexGraphMonth')}}",
					data: {orgId:orgId,year:year,officeId:officeId,departmentId:departmentId,"_token":'<?php echo csrf_token()?>'},
					success: function(response)
					{
						$('.loader-img').hide();
						$('#cot2').html('');
						$('#cot5').html('');
						$('#cot3').html('');
						//$('#cot6').html('');	
						$('#cot17').html('');
						$('#cot17').append(response);
					}
				});		
			}
		}
	}

    }

@php
}
else
{ @endphp

function drawMultSeries() {
	$('#chart_div').css('display','none');
	$('.search-cot').css('display','none');
	$('#error').html('<span style="font-size:1em;color:#8c8c8c;margin:10px;">No Record Found.</span>');
}
// $('#chart_div').css('display','none');
// $('#error').html("<span>No record found.</span>");

@php } @endphp


</script>

<script type="text/javascript">
	$('.report-happy-index-form').on('click',function(){
		$('.loader-img').show();			

		var form_values   = $("#report-happy-index-form").serializeArray();

		var officeId      = form_values[1]['value'];
		var departmentId  = form_values[2]['value'];
		var orgId         = "{{base64_encode($orgId)}}";
		
		console.log(form_values)
		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getReportHappyIndexGraphYear')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				$('.loader-img').hide();
				$('#cot2').html('');
				$('#cot5').html('');
				$('#cot3').html('');
				//$('#cot6').html('');	
				$('#cot17').html('');	
				$('#cot17').append(response);
			}
		});		
	});
</script>

<script type="text/javascript">
	$('#happy-index-office').on('change', function() {
		$('#happy-index-department option').remove();
		var officeId = $('#happy-index-office').val();

		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getDepartmentByOfficecIdforGraph')!!}",				
			data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{					
				$('#happy-index-department').append('<option value="" selected>All Department</option>');
				$('#happy-index-department').append(response);
			}
		});
	});
</script>

