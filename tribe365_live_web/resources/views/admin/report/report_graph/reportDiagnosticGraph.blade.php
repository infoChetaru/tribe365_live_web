<div  id="diagnosticGraph">
<div class="row">
	<div class="col-md-12">	
		<div class="search-cot">				
			<form id="report-diagnostic-form" action="#">
				{{csrf_field()}}					
				<select id="diagnostic-office" name="officeId">
					<option value="" selected>All Offices</option>
					@foreach($offices as $office)
					<option {{($officeId==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
					@endforeach
				</select>
				<select id="diagnostic-department" name="departmentId">			
					@if($departments && !empty($officeId))
					<option value="">All Department</option>
					@foreach($departments as $cdept)
					<option {{($departmentId==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
					@endforeach
					@elseif($all_department)
					<option value="">All Department</option>
					@foreach($all_department as $dept)
					<option {{($departmentId==$dept->id)?'selected':''}} value="{{$dept->id}}">{{$dept->department}}</option>
					@endforeach	
					@endif
				</select>
				<select id="diagnostic-year" name="year">
					<!-- <option value="" selected>All Year</option> -->
					@foreach($getYears as $years)
					<option {{($year==$years->year)?'selected':''}} value="{{$years->year}}">{{$years->year}}</option>
					@endforeach
				</select>	
				<button class="report-diagnostic-form" type="button" onclick="clickDignBtn('');">Search</button>
			</form> 
		</div>
	</div>
</div>

<!--
<ul class="nav nav-tabs" id="teamRoleTabs">
	@if($diagnosticQueCatTbl)
		@foreach($diagnosticQueCatTbl as $cate)
			@if($cate->id == 1)
				@if($chartId == 'diagCateId'.$cate->id || $chartId == '')
					<li><a class="active show" onclick="clickDignBtn('<?php echo 'diagCateId'.$cate->id; ?>');" data-toggle="tab" href="" id="diagCateId{{$cate->id}}">{{$cate->title}}</a></li>
				@else
					<li><a class="" data-toggle="tab" onclick="clickDignBtn('<?php echo 'diagCateId'.$cate->id; ?>');" href="" id="diagCateId{{$cate->id}}">{{$cate->title}}</a></li>
				@endif
			@elseif($cate->id != 1)
				@if(($chartId == 'diagCateId'.$cate->id))
					<li><a class="active show" onclick="clickDignBtn('<?php echo 'diagCateId'.$cate->id; ?>');" data-toggle="tab" href="" id="diagCateId{{$cate->id}}">{{$cate->title}}</a></li>
				@else
					<li><a class="" data-toggle="tab" onclick="clickDignBtn('<?php echo 'diagCateId'.$cate->id; ?>');" href="" id="diagCateId{{$cate->id}}">{{$cate->title}}</a></li>
				@endif
			@endif
		@endforeach()
	@endif
</ul>
-->

<div class="tab-content">
	@if($diagnosticQueCatTbl)
		@foreach($diagnosticQueCatTbl as $cate)
			@if($cate->id == 1)
				@if($chartId == 'diagCateId'.$cate->id || $chartId == '')
					<div id="diagCateGraphId{{$cate->id}}">
				@else
					<div id="diagCateGraphId{{$cate->id}}">
				@endif
				<!-- <div id="diagCateGraphId{{$cate->id}}" class="tab-pane fade active show"> -->
					<div class="organistiondetail-section">
						<h5>{{$cate->title}}</h5>
						<!-- <div style="display: flex;"> -->
							<div class="cot-tab-detail-section">
								<div class="tab-pane" id="individual" class="active">
									<div class="individual-content team-content">
										<div class="individual-cate">
											<div id="chart_div{{$cate->id}}" class="chart_div"></div>
											<!-- <hr style="border-width: 2px;border-color: darkgray;"> -->
											<div id="pie_chart_div{{$cate->id}}" class="pie_chart_div"></div>
										</div>
									</div>
								</div>		
							</div>
						<!-- </div> -->
						<br />
					</div>
				</div>
			@elseif($cate->id != 1)
				@if(($chartId == 'diagCateId'.$cate->id))
					<div id="diagCateGraphId{{$cate->id}}">
				@else
					<div id="diagCateGraphId{{$cate->id}}">
				@endif
				<!-- <div id="diagCateGraphId{{$cate->id}}" class="tab-pane fade"> -->
					<div class="organistiondetail-section">
						<h5>{{$cate->title}}</h5>
						<!-- <div style="display: flex;"> -->
							<div class="cot-tab-detail-section">
								<div class="tab-pane" id="individual" class="active">
									<div class="individual-content team-content">
										<div class="individual-cate">
											<div id="chart_div{{$cate->id}}" class="chart_div"></div>
											<!-- <hr style="border-width: 2px;border-color: darkgray;"> -->
											<div id="pie_chart_div{{$cate->id}}" class="pie_chart_div"></div>
										</div>
									</div>
								</div>		
							</div>
						<!-- </div> -->
						<br />
					</div>
				</div>
			@endif
		@endforeach()
	@endif
	<!-- dot graph meters -->
	
	<!-- <div id="radarChart" class="tab-pane fade">
		<div class="organistiondetail-section">
			<div class="cot-tab-detail-section">
				<div class="tab-pane" id="individual" class="active">
					<div class="individual-content team-content">
						<div class="individual-cate">
							<div id="chart-container">FusionCharts XT will load here!</div>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div> -->
</div>
</div>



<!-- <div class="search-cot">				
	<form id="report-diagnostic-form" action="#">
		{{csrf_field()}}					
		<select id="diagnostic-office" name="officeId">
			<option value="" selected>All Offices</option>
			@foreach($offices as $office)
			<option {{($officeId==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
			@endforeach
		</select>
		<select id="diagnostic-department" name="departmentId">			
			@if($departments && !empty($officeId))
			<option value="">All Department</option>
			@foreach($departments as $cdept)
			<option {{($departmentId==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
			@endforeach
			@elseif($all_department)
			<option value="">All Department</option>
			@foreach($all_department as $dept)
			<option {{($departmentId==$dept->id)?'selected':''}} value="{{$dept->id}}">{{$dept->department}}</option>
			@endforeach	
			@endif
		</select>		
		<button class="report-diagnostic-form" type="button">Search</button>
	</form> 
</div>	


<canvas style="margin-top: 20px;" id="myChartDiagnostic" width="1000" height="400"></canvas> -->

 <!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->

<script type="text/javascript">

google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawMultSeries);

function drawMultSeries() {

	<?php $a = 1;?>
	@foreach($diagnosticFinalArray as $diagFinalArray)
		var data = google.visualization.arrayToDataTable([
			['', '(%)'],
			@foreach($diagFinalArray as $diagArray)
				@if (!empty($diagArray['data'])) 	
					@if(!empty($organisation))
						@if($organisation->include_weekend == 1)
							["{{$diagArray['monthName']}}",{{$diagArray['data']->with_weekend}}],
						@elseif($organisation->include_weekend != 1)
							["{{$diagArray['monthName']}}",{{$diagArray['data']->without_weekend}}],
						@endif
					@endif
				@else
					["{{$diagArray['monthName']}}",0],
				@endif
			@endforeach
		]);
		

		var options = {
			width: '100%',
			height: '100%',
			colors: ['#eb1c24'],
			hAxis: {
				titleTextStyle: {
					color: '#333'
				}
			},
			vAxis: {
				minValue: 0,
				viewWindow: {
					min: 0,
					max: 100
				}
			},
			curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				width: '100%'
			},
			legend: 'none'
		};

		var chart = new google.visualization.LineChart(document.getElementById('chart_div{{$a}}'));
		chart.draw(data, options);

		<?php $a++;?>
	@endforeach
}


google.charts.load('current', {packages: ['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

	<?php $b = 1;?>
	@foreach($subDiagFinalArray as $subDiagArray)
		var data = google.visualization.arrayToDataTable([
			['', ''],
			@foreach($subDiagArray as $subdiag)
				<?php
				$measure = $subdiag['measure'];
				$measure = str_replace("(%)", "", $measure);
				$measure = $measure." (".floatval($subdiag['data'])." %)";
				?>
				["{{ucfirst($measure)}}",{{$subdiag['data']}}],
			@endforeach
		]);
		var options = {
			pieSliceText: 'value',
			//legend: 'none',
			width: '100%',
			height: '100%',
			tooltip: {
	            text: 'value',
	            textStyle:'bold'
	        },
	        slices: {
	            0: { color: '#ec2127' },
	            1: { color: '#f16465' },
	            2: { color: '#939598' },
	            3: { color: '#bfbfbf' },
	            4: { color: '#27af57' },
	            5: { color: '#7ecc9c' },
          	},
			is3D: true

          //title: 'My Daily Activities'
        };
		
		var chart = new google.visualization.PieChart(document.getElementById('pie_chart_div{{$b}}'));
		chart.draw(data, options);

		<?php $b++;?>
	@endforeach
}
</script>




<script type="text/javascript">
//$('.report-diagnostic-form').on('click',function(){
function clickDignBtn(whichTab){	
	//alert(whichTab);
	$('.loader-img').show();			
	var form_values   = $("#report-diagnostic-form").serializeArray();
	var officeId      = form_values[1]['value'];
	var departmentId  = form_values[2]['value'];
	var orgId         = "{{base64_encode($orgId)}}";
	var year  		  = form_values[3]['value'];
	if(whichTab!=''){
		var chartId = whichTab;
	}else{
		var chartId = $("#diagnosticGraph").find(".active").attr('id');
	}

	console.log(form_values)
	$.ajax({
		type: "POST",
		url: "{{URL::to('admin/getReportDiagnosticGraph')}}",
		data: {orgId:orgId,officeId:officeId,departmentId:departmentId,year:year,chartId:chartId,"_token":'<?php echo csrf_token()?>'},
		success: function(response)
		{
			$('.loader-img').hide();
			$('#cot2').html('');
			$('#cot5').html('');
			$('#cot3').html('');
			$('#cot6').html('');	
			$('#cot6').append(response);
		}
	});	
}	
//});
</script>



<!-- <?php 

//$maxValueArray = array();
//foreach($diagnosticResultArray as $value1)
{
//	array_push($maxValueArray, $value1['percentage']);
}

//$max=10;
//if(!empty($maxValueArray))
{
//	$max = max($maxValueArray);

//	$max = ceil($max / 10) * 10;

//	if($max>=100)
	{
//		$max=100;
	}
}

?>
 -->
<!-- DIAGNOSTIC GRAPH -->
<!-- <script  type="text/javascript">

	var canvas = document.getElementById("myChartDiagnostic");
	var ctx = canvas.getContext("2d");
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: [@foreach($diagnosticResultArray as $value)"{{$value['title']}}",@endforeach],
			datasets: [{
				label: 'Percentage',
				data: [@foreach($diagnosticResultArray as $value1){{$value1['percentage']}},@endforeach],
				backgroundColor: [
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)', 
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)'
				],
				borderColor: [
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)',
				'rgb(255, 69, 75)'
				],
				borderWidth: 5
			}]
		},
		options: {			
			legend: { display: false},			
			scales: {
				xAxes: [{
					categoryPercentage: 0.25,
					ticks: {
						fontSize: 15
					},
					gridLines: {
						display: false
					}

				}],
				yAxes: [{					
					ticks: {
						beginAtZero:true,
						step:10,
						max:0,
					},
					gridLines: {
						display: false
					}
				}]
			}
		}

	});
//get onclick event
canvas.onclick = function(evt) {
	var activePoints = myChart.getElementsAtEvent(evt);
	if(activePoints[0])
	{

		var form_values   = $("#report-diagnostic-form").serializeArray();
		var officeId      = form_values[1]['value'];
		var departmentId  = form_values[2]['value'];

		var chartData = activePoints[0]['_chart'].config.data;
		var idx = activePoints[0]['_index'];
		var label = chartData.labels[idx];
		var value = chartData.datasets[0].data[idx];
		var orgId         = "{{$orgId}}";
		console.log(idx)

		var barIndex = idx;

		$.ajax({
			type: "POST",
			url: "{{route('getDiagnsticSubGraph')}}",				
			data: {barIndex:barIndex,orgId:orgId,officeId:officeId,departmentId:departmentId,"_token":'<?php //echo csrf_token()?>'},
			success: function(response) 
			{					
				$('.loader-img').hide();
				$('#cot2').html('');
				$('#cot5').html('');
				$('#cot3').html('');
				$('#cot6').html('');	
				$('#cot6').append(response);
			}
		});
	}
};


</script> -->

<script type="text/javascript">
	$('#diagnostic-office').on('change', function() {
		$('#diagnostic-department option').remove();
		var officeId = $('#diagnostic-office').val();

		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getDepartmentByOfficecIdforGraph')!!}",				
			data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{					
				$('#diagnostic-department').append('<option value="" selected>All Department</option>');
				$('#diagnostic-department').append(response);
			}
		});
	});
</script>

<style type="text/css">
	/*
	.pie_chart_div svg>g:nth-child(3) 
	{transform: translate(30px, 80px);}
	.pie_chart_div svg>g:nth-child(4)>text, 
	.pie_chart_div svg g:nth-child(5)>text, 
	.pie_chart_div svg>g:nth-child(6)>text, 
	.pie_chart_div svg>g:nth-child(7)>text 
	{transform: translateX(-17px);}
	*/
	div.chart_div,div.pie_chart_div {width: 50%; display: flex; justify-content: center; }
</style>