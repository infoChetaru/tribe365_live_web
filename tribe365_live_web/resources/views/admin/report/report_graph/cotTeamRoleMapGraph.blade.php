<div id="teamRoleMapGraphs">
 	<div class="row">
		<div class="col-md-12">	
			<div class="search-cot">				
				<form id="cot-team-role-form" action="#">
					{{csrf_field()}}					
					<select id="office" name="officeId">
						<option value="" selected>All Offices</option>
						@foreach($offices as $office)
						<option {{($officeId==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
						@endforeach
					</select>
					<select id="department" name="departmentId">
						@if($departments && !empty($officeId))
						<option value="">All Department</option>
						@foreach($departments as $cdept)
						<option {{($departmentId==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
						@endforeach
						@elseif($all_department)
						<option value="">All Department</option>
						@foreach($all_department as $dept)
						<option {{($departmentId==$dept->id)?'selected':''}} value="{{$dept->id}}">{{$dept->department}}</option>
						@endforeach	
						@endif
					</select>							
					<button class="cot-team-role-map" onclick="clickSearchBtn('');" type="button">Search</button>
				</form> 
			</div>	
		</div>
	</div>
	<ul class="nav nav-tabs" id="teamRoleTabs">
		@if($chartId == 'graphCotTab')
			<li><a class="active show" data-toggle="tab" href="#graph" id="graphCotTab">Graph</a></li>	
			<li><a class="" data-toggle="tab" href="#radarChart" id="radatCotTab">Radar Chart</a></li>
		@elseif($chartId == 'radatCotTab')
			<li><a class="" data-toggle="tab" href="#graph" id="graphCotTab">Graph</a></li>	
			<li><a class="active show" data-toggle="tab" href="#radarChart" id="radatCotTab">Radar Chart</a></li>
		@elseif($chartId == '')
			<li><a class="active show" data-toggle="tab" href="#graph" id="graphCotTab">Graph</a></li>	
			<li><a class="" data-toggle="tab" href="#radarChart" id="radatCotTab">Radar Chart</a></li>
		@endif
	</ul>
	<div class="tab-content">
		@if($chartId == 'graphCotTab')
			<div id="graph" class="tab-pane fade active show">
		@elseif($chartId == 'radatCotTab')
			<div id="graph" class="tab-pane fade">
		@elseif($chartId == '')
			<div id="graph" class="tab-pane fade active show">
		@endif
		<!-- <div id="graph" class="tab-pane fade active show"> -->
			<div class="organistiondetail-section">
				<div style="display: flex;">
					<div class="cot-tab-detail-section">
					<div class="tab-pane" id="individual" class="active">
						<div class="individual-content team-content">
							<div class="individual-cate">
								<div id="chart_div"></div>
								</div>
							</div>
						</div>		
					</div>
					<!-- <div class="allCat" style="margin-top: 2em;">
						@foreach($cotCategory as $value)
							@if($value->id == 1) 
								<p class="black-square">{{$value->category_name}}</p>
							@elseif($value->id == 2) 
								<p class="orange-square">{{$value->category_name}}</p>
							@elseif($value->id == 3) 
								<p class="red-square">{{$value->category_name}}</p>
							@elseif($value->id == 4)
								<p class="gray-square">{{$value->category_name}}</p>
							@endif
						@endforeach
					</div> -->
				</div>
			</div>
		</div>
		<!-- dot graph meters -->
		@if($chartId == 'graphCotTab')
			<div id="radarChart" class="tab-pane fade">
		@elseif($chartId == 'radatCotTab')
			<div id="radarChart" class="tab-pane fade active show">
		@elseif($chartId == '')
			<div id="radarChart" class="tab-pane fade">
		@endif
		<!-- <div id="radarChart" class="tab-pane fade"> -->
			<div class="organistiondetail-section">
				<div class="cot-tab-detail-section">
					<div class="tab-pane" id="individual" class="active">
						<div class="individual-content team-content">
							<div class="individual-cate">
								<!-- <div id="chart-container">Chart will load here!</div> -->
								@if(!empty($cotGraphConditionArr))
									<canvas id="myRadarChart" width="1500" height="400" style="margin-left: -20em;"></canvas>
								@else
									<p>No record found</p>
								@endif
							</div>
						</div>
					</div>		
				</div>
			</div>
		</div>
	</div>
</div>


<!--
<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
<style>
	.raphael-group-207-dataset-Label-group{
		display: none !important;
	}
	.raphael-group-85-dataset-Label-group{
		display: none !important;
	}
	.raphael-group-75-common-elems-group path{
		fill: #eb1c24 !important;
	}
	.raphael-group-197-common-elems-group path{
		fill: #eb1c24 !important;
	}
</style>
-->

@if(empty($cotGraphConditionArr))
	<style>
		.raphael-group-84-dataset-Label-group{
			display: none !important;
		}
	</style>
@endif

<script  type="text/javascript">
@if(!empty($cotGraphConditionArr))
	var canvas = document.getElementById("myRadarChart");
	var ctx = canvas.getContext("2d");
	var myChart = new Chart(ctx, {
		type: 'radar',
		data: {
			labels: [@foreach($cotRadarChartArr as $value)"{{$value['option']}}",@endforeach],
			datasets: [{
				label: 'Percentage',
				data: [@foreach($cotRadarChartArr as $value1){{ucfirst($value1['perArr'])}},@endforeach],
				backgroundColor: '#eb1c24',
				borderColor: '#eb1c24',
				//borderWidth: 5
			}]
		},
		options: {			
			legend: { display: false},	
		}

	});
@endif
</script>

<!-- 
<script type="text/javascript">
	FusionCharts.ready(function(){
		var chartObj = new FusionCharts({
			type: 'radar',
			renderAt: 'chart-container',
			width: '600',
			height: '350',
			dataFormat: 'json',
		    scale: {
		        angleLines: {
		            display: false
		        },
		    },
			dataSource: {
				"chart": {
					"caption": "",
					"subCaption": "",
					//"numberPreffix": "$",
					"theme": "fusion",
					"radarfillcolor": "#ffffff",
				},
				"categories": [{
					"category": [

					@foreach($cotRadarChartArr as $key => $value)
						{"label" : "{{ucfirst($value['option'])}}"},
					@endforeach

					// {
					// 	"label": "{{ucfirst($cotRoleMapOptions[0]->maper)}}"
					// }, {
					// 	"label": "{{ucfirst($cotRoleMapOptions[1]->maper)}}"
					// }, {
					// 	"label": "{{ucfirst($cotRoleMapOptions[2]->maper)}}"
					// }, {
					// 	"label": "{{ucfirst($cotRoleMapOptions[3]->maper)}}"
					// }, {
					// 	"label": "{{ucfirst($cotRoleMapOptions[4]->maper)}}"
					// }, {
					// 	"label": "{{ucfirst($cotRoleMapOptions[5]->maper)}}"
					// }, {
					// 	"label": "{{ucfirst($cotRoleMapOptions[6]->maper)}}"
					// }, {
					// 	"label": "{{ucfirst($cotRoleMapOptions[7]->maper)}}"
					// }
					]
				}],
				"dataset": [{
					"seriesname": "",
					"data": [{radius: 0},
					@foreach($cotRadarChartArr as $key => $value)
						{"value" : "{{ucfirst($value['perArr'])}}"},
					@endforeach
					// {
					// 	"value": "{{$cotTeamRoleMapGraphPercentage['data']['shaper']}}"
					// }, {
					// 	"value": "{{$cotTeamRoleMapGraphPercentage['data']['coordinator']}}"
					// }, {
					// 	"value": "{{$cotTeamRoleMapGraphPercentage['data']['implementer']}}"
					// }, {
					// 	"value": "{{$cotTeamRoleMapGraphPercentage['data']['completerFinisher']}}"
					// }, {
					// 	"value": "{{$cotTeamRoleMapGraphPercentage['data']['monitorEvaluator']}}"
					// }, {
					// 	"value": "{{$cotTeamRoleMapGraphPercentage['data']['teamworker']}}"
					// }, {
					// 	"value": "{{$cotTeamRoleMapGraphPercentage['data']['plant']}}"
					// }, {
					// 	"value": "{{$cotTeamRoleMapGraphPercentage['data']['resourceInvestigator']}}"
					// }
					]
				}]
			}
		});
		chartObj.render();
	});
</script> -->




<script type="text/javascript">
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawMultSeries);

function drawMultSeries() {
      var data = google.visualization.arrayToDataTable([
  		['', '%'],
  		// ['', '%',{ role: 'style' }],
		@foreach($cotGraphArr as $key => $value)
		
		["{{$value['option']}}",{{ucfirst($value['perArr'])}}],
		// ["{{$value['option']}}",{{ucfirst($value['perArr'])}},'{{$value['color']}}'],
		@endforeach
      ]);
      var options = {
			width: 1000,
			height: 400,
			colors: ['#eb1c24'],
			hAxis: {
				titleTextStyle: {
					color: '#333'
				}
			},
			vAxis: {
				minValue: 0,
				viewWindow: {
			        min: 0,
			        max: 300
			    }
			},
			curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				width: '80%'
			},
			legend: 'none'
		};
      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }


</script>
<script type="text/javascript">
//$('.cot-team-role-map').on('click',function(){
function clickSearchBtn(istabOne){
	$('.loader-img').show();			
	// get form values 
	var form_values   = $("#cot-team-role-form").serializeArray();
	var start_date 	  = $('#start_date').val().trim();
	var end_date   	  = $('#end_date').val().trim();
	var officeId      = form_values[1]['value'];
	var departmentId  = form_values[2]['value'];
	var orgId         = "{{base64_encode($orgId)}}";	
	if(istabOne==1){
		var chartId  = 'graphCotTab';
	}else{
		var chartId  = $("#teamRoleMapGraphs").find(".active").attr('id');
	}

	$.ajax({
		type: "POST",
		url: "{{URL::to('admin/getCOTteamRoleMapGraph')}}",
		data: {orgId:orgId,officeId:officeId,departmentId:departmentId,startDate:start_date,endDate:end_date,chartId:chartId,"_token":'<?php echo csrf_token()?>'},
		success: function(response)
		{
			$('.loader-img').hide();
			$('#cot2').html('');
			$('#cot5').html('');
			$('#cot3').html('');	
			$('#cot2').append(response);
		}
	});	
}
//});


//Show y indexes
$('#graphCotTab').on('click',function(){
	clickSearchBtn('1');
});
	
</script>

<script type="text/javascript">
$('#office').on('change', function() {
	$('#department option').remove();
	var officeId = $('#office').val();

	$.ajax({
		type: "POST",
		url: "{!!URL::to('admin/getDepartmentByOfficecIdforGraph')!!}",				
		data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
		success: function(response) 
		{					
			$('#department').append('<option value="" selected>All Department</option>');
			$('#department').append(response);
		}
	});
});
</script>



<!-- <div class="organistiondetail-section">
	<div class="row">
		<div class="col-md-12">	
			<div class="search-cot">				
				<form id="cot-team-role-form" action="#">
					{{csrf_field()}}					
					<select id="office" name="officeId">
						<option value="" selected>All Offices</option>
						@foreach($offices as $office)
						<option {{($officeId==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
						@endforeach
					</select>
					<select id="department" name="departmentId">
						@if($departments && !empty($officeId))
						<option value="">All Department</option>
						@foreach($departments as $cdept)
						<option {{($departmentId==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
						@endforeach
						@elseif($all_department)
						<option value="">All Department</option>
						@foreach($all_department as $dept)
						<option {{($departmentId==$dept->id)?'selected':''}} value="{{$dept->id}}">{{$dept->department}}</option>
						@endforeach	
						@endif
					</select>							
					<button class="cot-team-role-map" type="button">Search</button>
				</form> 
			</div>	
		</div>
	</div>
	<div class="cot-tab-detail-section">
		<div class="tab-pane" id="individual" class="active">
			<div class="individual-content team-content">
				<div class="individual-cate">
					<div class="cate-left">
						<div class="cat-left-tital">
							<h4> Organisation</h4>
							<span class="shaper">  {{ucfirst($cotRoleMapOptions[0]->maper)}} </span>
							<span class="coordinator"> {{ucfirst($cotRoleMapOptions[1]->maper)}} </span>
						</div>
						<div class="cat-left-tital">
							<h4> Controllers</h4>
							<span class="implementer">  {{ucfirst($cotRoleMapOptions[2]->maper)}} </span>
							<span class="completerFinisher"> {{ucfirst($cotRoleMapOptions[3]->maper)}} </span>
						</div>
						<div class="cat-left-tital">
							<h4> Advisors</h4>
							<span class="monitorEvaluator"> {{ucfirst($cotRoleMapOptions[4]->maper)}} </span>
							<span class="teamworker"> {{ucfirst($cotRoleMapOptions[5]->maper)}} </span>
						</div>
						<div class="cat-left-tital">
							<h4> Explorers</h4>
							<span class="plant">  {{ucfirst($cotRoleMapOptions[6]->maper)}} </span>
							<span class="resourceInvestigator"> {{ucfirst($cotRoleMapOptions[7]->maper)}} </span>
						</div>
					</div>
					<div class="cate-right">

						<div class="cate-user-section">
							{{--	@foreach($cotTeamRoleMapGraphPercentage as $value)
								<div class="cate-user-tital">									
									<span class="">{{print_r($value)}}%</span>
								</div>		
								@endforeach	--}}
							</div>


							@foreach($cotTeamRoleMapGraphPercentage as $value)
							
							<div class="cate-user-section">
								
								<div class="cate-user-tital">								
									<span class="{{$value['shaper']}}">{{$value['shaper']}}%</span>
									<span class="{{$value['coordinator']}}">{{$value['coordinator']}}%</span>
								</div>

								<div class="cate-user-tital">
									<span class="{{$value['implementer']}}">{{$value['implementer']}}%</span>											
									<span class="{{$value['completerFinisher']}}">{{$value['completerFinisher']}}%</span>
								</div>

								<div class="cate-user-tital">									
									<span class="{{$value['monitorEvaluator']}}">{{$value['monitorEvaluator']}}%</span>									
									<span class="{{$value['teamworker']}}">{{$value['teamworker']}}%</span>
								</div>

								<div class="cate-user-tital">											
									<span class="{{$value['plant']}}">{{$value['plant']}}%</span>
									<span class="{{$value['resourceInvestigator']}}">{{$value['resourceInvestigator']}}%</span>
								</div>											
							</div>
							
							@endforeach
							
						</div>
					</div>
				</div>
			</div>		
		</div>

		<script type="text/javascript">

			$('.cot-team-role-map').on('click',function(){
				$('.loader-img').show();			
			// get form values 
			var form_values   = $("#cot-team-role-form").serializeArray();

			var start_date = $('#start_date').val().trim();
			var end_date   = $('#end_date').val().trim();
			
			var officeId      = form_values[1]['value'];
			var departmentId  = form_values[2]['value'];
			var orgId = "{{base64_encode($orgId)}}";
			
			$.ajax({
				type: "POST",
				url: "{{URL::to('admin/getCOTteamRoleMapGraph')}}",
				data: {orgId:orgId,officeId:officeId,departmentId:departmentId,startDate:start_date,endDate:end_date,"_token":'<?php //echo csrf_token()?>'},
				success: function(response)
				{
					$('.loader-img').hide();
					$('#cot2').html('');
					$('#cot5').html('');
					$('#cot3').html('');	
					$('#cot2').append(response);
				}
			});	
		});
	</script>

	<script type="text/javascript">
		$('#office').on('change', function() {
			$('#department option').remove();
			var officeId = $('#office').val();

			$.ajax({
				type: "POST",
				url: "{!!URL::to('admin/getDepartmentByOfficecIdforGraph')!!}",				
				data: {officeId:officeId,"_token":'<?php //echo csrf_token()?>'},
				success: function(response) 
				{					
					$('#department').append('<option value="" selected>All Department</option>');
					$('#department').append(response);
				}
			});
		});
	</script>
 -->


 <style>
	.allCat p:before{
		content: '';
	    display: inline-block;
	    height: 8px;
	    width: 8px;
	    vertical-align: 1px;
	    margin-right: 5px;
	}
	.allCat p.black-square:before{
		 background: #000;
	}
	.allCat p.orange-square:before{
		background: #e28b13;
	}
	.allCat p.red-square:before{
		background: #eb1c24;
	}
	.allCat p.gray-square:before{
		background: lightgray;
	}

</style>