<div class="search-cot">				
	<form id="report-tribeometer-form" action="#">
		{{csrf_field()}}					
		<select id="tribeometer-office" name="officeId">
			<option value="" selected>All Offices</option>
			@foreach($offices as $office)
			<option {{($officeId==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
			@endforeach
		</select>
		<select id="tribeometer-department" name="departmentId">			
			@if($departments && !empty($officeId))
			<option value="">All Department</option>
			@foreach($departments as $cdept)
			<option {{($departmentId==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
			@endforeach
			@elseif($all_department)
			<option value="">All Department</option>
			@foreach($all_department as $dept)
			<option {{($departmentId==$dept->id)?'selected':''}} value="{{$dept->id}}">{{$dept->department}}</option>
			@endforeach	
			@endif
		</select>		
		<button class="report-tribeometer-form" type="button">Search</button>
	</form> 
</div>							
<canvas style="margin-top: 20px;" id="myChartTribeometer" width="1000" height="400"></canvas>

<script type="text/javascript">

	$('.report-tribeometer-form').on('click',function(){
		$('.loader-img').show();			
			// get form values 
			var form_values   = $("#report-tribeometer-form").serializeArray();
			
			var officeId      = form_values[1]['value'];
			var departmentId  = form_values[2]['value'];
			var orgId = "{{base64_encode($orgId)}}";
			var start_date = $('#start_date').val().trim();
			var end_date   = $('#end_date').val().trim();
			
			console.log(form_values)
			$.ajax({
				type: "POST",
				url: "{{URL::to('admin/getReportTribeometerGraph')}}",
				data: {orgId:orgId,officeId:officeId,departmentId:departmentId,startDate:start_date,endDate:end_date,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					$('.loader-img').hide();
					$('#cot2').html('');
					$('#cot5').html('');
					$('#cot3').html('');
					$('#cot8').html('');	
					$('#cot8').append(response);
				}
			});		

		});

	</script>


	<?php 

	$maxValueArray = array();
	foreach($tribeometerResultArray as $value1)
	{
		array_push($maxValueArray, $value1['percentage']);
	}

	$max=10;
	if(!empty($maxValueArray))
	{
		$max = max($maxValueArray);

		$max = ceil($max / 10) * 10;

		if($max>=100)
		{
			$max=100;
		}
	}

	?>

	<!-- TRIBEOMETER GRAPH -->
	<script  type="text/javascript">
		var ctx = document.getElementById("myChartTribeometer");

		var myChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: [@foreach($tribeometerResultArray as $value)"{{$value['title']}}",@endforeach],
				datasets: [{
					label: 'Percentage',
					data: [@foreach($tribeometerResultArray as $value1){{$value1['percentage']}},@endforeach],
					backgroundColor: ['rgb(255, 69, 75)','rgb(255, 69, 75)'],
					borderColor: ['rgb(255, 69, 75)','rgb(255, 69, 75)'],
					borderWidth: 1
				}]
			},
			options: {
				legend: { display: false},			
				scales: {
					xAxes: [{
						categoryPercentage: 0.25,
						ticks: {
							fontSize: 15
						},
						gridLines: {
							display: false
						}

					}],
					yAxes: [{					
						ticks: {
							beginAtZero:true,
							step:10,
							max:{{$max}},
						},
						gridLines: {
							display: false
						}
					}]
				}
			}
		});
	</script>

	<script type="text/javascript">
		$('#tribeometer-office').on('change', function() {
			$('#tribeometer-department option').remove();
			var officeId = $('#tribeometer-office').val();

			$.ajax({
				type: "POST",
				url: "{!!URL::to('admin/getDepartmentByOfficecIdforGraph')!!}",				
				data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
				success: function(response) 
				{					
					$('#tribeometer-department').append('<option value="" selected>All Department</option>');
					$('#tribeometer-department').append(response);
				}
			});
		});
	</script>