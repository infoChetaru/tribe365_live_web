@section('title', 'List Organisation Report')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section dot cot-functional">
		<div class="container">  
			<div class="Reports-tital">
				<!-- <div class="cot-tab-section">					 -->
					<!--<div class="tab-content">-->
					<!-- <div class=""> -->
						<div class="search-cot">				
							<form id="dashboard-report-form" action="#" method="POST">
								{{csrf_field()}}					
								<select id="dashboard-org" name="orgId"> 
									<!--<option value="">All Organisation</option>-->
									@foreach($organisations as $value)
									<option {{($orgId==$value->id)?'Selected':'' }} value="{{$value->id}}">{{$value->organisation}}</option>
									@endforeach()
								</select>
								<select id="dashboard-office" name="officeId">
									<option value="" selected>All Offices</option>
									@foreach($offices as $office)
									<option {{($officeId==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
									@endforeach
								</select>
								<select id="dashboard-department" name="departmentId">			
									@if($departments && !empty($officeId))
									<option value="">All Department</option>
									@foreach($departments as $cdept)
									<option {{($departmentId==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
									@endforeach
									@elseif($all_department)
									<option value="">All Department</option>
									@foreach($all_department as $dept)
									<option {{($departmentId==$dept->id)?'selected':''}} value="{{$dept->id}}">{{$dept->department}}</option>
									@endforeach	
									@endif
								</select>	
								<button type="button" class="dashboard-report-form" style="width: auto;">Search</button>
								<input type="hidden" name="ispdf" id="ispdf">
							</form> 
						</div>

						<div class="generate-pdf-btn">					
							<a target="_BLANK" href="{{URL::to('admin/admin-dashboard/')}}?orgId={{$orgId}}&ispdf=1" style="color:red;">
									Generate Report								
							</a>								
						</div>

					
<!-- 						<div class="performance-list">
							<div class="row">
								<div class="col-md-4">
									<div class="performance-box">
										<div class="performance-text">
											<div>Culture Index</div>
											<div id="cultureIndex"></div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="performance-box">
										<div class="performance-text">
											<div>Engagement Index</div>
											<div id="engagementIndex"></div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="performance-box">
										<div class="performance-text">
											<div>Happy Index</div>
											<div id="happyIndex"></div>
										</div>
									</div>
								</div>
							</div>
						</div>		 -->			
					<!-- </div> -->
				<!-- </div> -->
				<div class="loader-img dashboard-loader" style="display:none; width: 70px;">
					<img src="{{asset('public/images/loader.gif')}}">
				</div>
				<div class="chart_group">
					<!-- <div class="chartRows">
						<div class="row">
							<div class="col-md-4">
								<h6 class="">Culture Index</h6>
								<div id="cultureIndex"></div>
							</div>
							<div class="col-md-4">
								<h6 class="">Engagement Index</h6>
								<div id="engagementIndex"></div>
							</div>
							<div class="col-md-4">
								<h6 class="">Happy Index</h6>
								<div id="happyIndex"></div>
							</div>
						</div>
					</div>
					<div class="chartRows">
						<div class="row">
							<div class="col-md-4">
								<h6 class="">Diagnostics</h6>
								<div id="diagnostic"></div>
								<div class="text-center" id="diagBackBtn" style="display: none;">
									<button class="graphBackButton">Back</button>
								</div>
							</div>
							<div class="col-md-4">
								<h6 class="">Directing</h6>
								<div id="directing"></div>
								<div class="text-center" id="directingBackBtn" style="display: none;">
									<button class="graphBackButton">Back</button>
								</div>
							</div>
							<div class="col-md-4">
								<h6 class="">Thumbs up</h6>
								<div id="thumbsup"></div>
								<div class="text-center" id="thumbsupBackBtn" style="display: none;">
									<button class="graphBackButton">Back</button>
								</div>
							</div>
						</div>
					</div>
					<div class="chartRows">
						<div class="row">
							<div class="col-md-4">
								<h6 class="">Tribeometer</h6>
								<div id="tribeometer"></div>
							</div>
							<div class="col-md-4">
								<h6 class="">Connecting: Team Role</h6>
								<div id="teamRole"></div>
							</div>
							<div class="col-md-4">
								<h6 class="">Supercharging: Motivation</h6>
								<div id="motivation"></div>
							</div>
						</div>
					</div>
					<div class="chartRows">
						<div class="row">
							<div class="col-md-4">
								<h6 class="">Connecting: Personality Type</h6>
								<canvas id="personalityType"></canvas>
								<div id="ptError">No Record Found.</div>
							</div>
							<div class="col-md-4">
								<h6 class="">Supercharging: Culture Structure</h6>
								<canvas id="cultureStructure"></canvas>
								<div id="csError">No Record Found.</div>
							</div>
						</div>
					</div> -->

					<div class="lead-indicators">
						<h5 class="ml-2">Lead Indicators</h5>
						<div class="row">
							<div class="col-md-4">
								<h6 class="ml-2">Culture Index</h6>
								<div id="cultureIndex"></div>
								<div class="motCat">
									<ul>
										<li><a><span style="background-color: red;"></span>Culture Index</a></li>
									</ul>
								</div>
							</div>
							<div class="col-md-4">
								<h6 class="ml-3">Good/Bad Day Index</h6>
								<div id="happyIndex"></div>
								<div id="happyIndexCate"></div>
								<div class="text-center hide-show">
									<button class="graphBackButton" id="happyIndexWeekBackBtn" style="visibility:hidden;">Back</button>
									<button class="graphBackButton" id="happyIndexDaysBackBtn" style="visibility:hidden;">Back</button>
									<input type="hidden" id="monthNum">
								</div>
								<!-- <div class="motCat text-center">
									<ul>
										<li><a><span style="background-color: red;"></span>Happy Index(%)</a></li>
									</ul>
								</div> -->
							</div>
						</div>
						<div class="row mt-4">
							<div class="col-md-4">
								<h6 class="ml-2">Engagement Index</h6>
								<div id="engagementIndex"></div>
								<div class="motCat">
									<ul>
										<li><a><span style="background-color: red;"></span>Engagement Index</a></li>
									</ul>
								</div>
							</div>
							<div class="col-md-4">
								<h6 class="ml-3">Good/Bad Day Index Responders</h6>
								<div id="happyIndexResponders"></div>
								<div id="happyIndexResCate"></div>
								<div class="text-center hide-show">
									<button class="graphBackButton" id="happyIndexResWeekBackBtn" style="visibility:hidden;">Back</button>
									<button class="graphBackButton" id="happyIndexResDaysBackBtn" style="visibility:hidden;">Back</button>
									<input type="hidden" id="monthNumHI">
								</div>
							</div>
						</div>
						<div class="row mt-4">
							<div class="col-md-4">
								<h6 class="ml-3">Diagnostics</h6>
								<div id="diagnostic"></div>
								<div id="diagnosticCate"></div>
								<div class="text-center">
									<button class="graphBackButton" id="diagBackBtn" style="visibility:hidden;">Back</button>
								</div>
							</div>
							<div class="col-md-4">
								<h6 class="ml-3">Tribeometer</h6>
								<div id="tribeometer"></div>
								<div class="motCat">
									<ul>
										<li><a onclick="getTribeometerCat('','')"><span style="background-color: red;"></span>All</a></li>
										<?php 
										 $catTribeometer = $resultArray['tribeometerArray']['arrMain'][0]; 
										 //print_r($catMotivation);die;
										 if($catTribeometer){
										 	$c=0;
										 	$catClr = array('','#000000','#eb1c24','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327');
										 	foreach($catTribeometer as $key=>$catTribeometerVal){
										 		if($c>0){
										 			$columColor = $catClr[$c];
										 		?>
										 			<li><a onclick="getTribeometerCat('<?php echo $key;?>','<?php echo $columColor;?>')"><span style="background-color: <?php echo $columColor;?>"></span><?php echo $catTribeometerVal;?></a></li>
										 		<?php 
										 		}
										 		$c++;
										 	}
										 }
										?>
									</ul>
								</div>
							</div>
							<div class="col-md-4">
								<h6 class="ml-3">Culture Structure</h6>
								<!-- <div id="personalityType">Chart will load here!</div> -->
								<!-- <canvas id="cultureStructure"></canvas> -->
								<div id="cultureStructure"></div>
								<div class="motCat">
									<ul>
										<li><a onclick="getCultureStructureCat('','')"><span style="background-color: red;"></span>All</a></li>
										<?php 
										 $catCultureStructure = $resultArray['cultureStructureArray']['arrMain'][0]; 
										 //print_r($catMotivation);die;
										 if($catCultureStructure){
										 	$c=0;
										 	$catClr = array('','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327');
										 	foreach($catCultureStructure as $key=>$catCultureStructureVal){
										 		if($c>0){
										 			$columColor = $catClr[$c];
										 		?>
										 			<li><a onclick="getCultureStructureCat('<?php echo $key;?>','<?php echo $columColor;?>')"><span style="background-color: <?php echo $columColor;?>"></span><?php echo $catCultureStructureVal;?></a></li>
										 		<?php 
										 		}
										 		$c++;
										 	}
										 }
										?>
									</ul>
								</div>
								<!-- <div id="csError">No Record Found.</div> -->
							</div>
						</div>
					</div>
					<div class="lead-indicators">
						<h5 class="ml-3">Dynamics</h5>
						<div class="row">						
							<div class="col-md-4">
								<h6 class="ml-3">Team Role</h6>
								<div id="teamRole"></div>
								<div class="motCat">
									<ul>
										<li><a onclick="getTeamRoleCat('','')"><span style="background-color: red;"></span>All</a></li>
										<?php 
										 $catTeamRole = $resultArray['teamRoleArray']['arrMain'][0]; 
										 //print_r($catMotivation);die;
										 if($catTeamRole){
										 	$c=0;
										 	$catClr = array('','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327');
										 	foreach($catTeamRole as $key=>$catTeamRoleVal){
										 		if($c>0){
										 			$columColor = $catClr[$c];
										 		?>
										 			<li><a onclick="getTeamRoleCat('<?php echo $key;?>','<?php echo $columColor;?>')"><span style="background-color: <?php echo $columColor;?>"></span><?php echo $catTeamRoleVal;?></a></li>
										 		<?php 
										 		}
										 		$c++;
										 	}
										 }
										?>
									</ul>
								</div>
							</div>
							<div class="col-md-4">
								<h6 class="ml-3">Motivation</h6>
								<div id="motivation"></div>
								<div class="motCat">
									<ul>
										<li><a onclick="getMotivationCat('','')"><span style="background-color: red;"></span>All</a></li>
										<?php 
										 $catMotivation = $resultArray['motivationArray']['arrMain'][0]; 
										 //print_r($catMotivation);die;
										 if($catMotivation){
										 	$c=0;
										 	$catClr = array('','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327');
										 	foreach($catMotivation as $key=>$catMotivationVal){
										 		if($c>0){
										 			$columColor = $catClr[$c];
										 		?>
										 			<li><a onclick="getMotivationCat('<?php echo $key;?>','<?php echo $columColor;?>')"><span style="background-color: <?php echo $columColor;?>"></span><?php echo $catMotivationVal;?></a></li>
										 		<?php 
										 		}
										 		$c++;
										 	}
										 }
										?>
									</ul>
								</div>
							</div>
							<div class="col-md-4">
								<h6 class="ml-3">Personality Type</h6>
								<!-- <div id="personalityType">Chart will load here!</div> -->
								<!-- <canvas id="personalityType"></canvas> -->
								<div id="personalityType"></div>
								<div class="motCat">
									<ul>
										<li><a onclick="getPerTypeCat('','')"><span style="background-color: red;"></span>All</a></li>
										<?php 
										 $catPerType = $resultArray['personalityTypeArray'][0]; 
										 //print_r($catMotivation);die;
										 if($catPerType){
										 	$c=0;
										 	$catClr = array('','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327');
										 	foreach($catPerType as $key=>$catPerTypeVal){
										 		if($c>0){
										 			$columColor = $catClr[$c];
										 		?>
										 			<li><a onclick="getPerTypeCat('<?php echo $key;?>','<?php echo $columColor;?>')"><span style="background-color: <?php echo $columColor;?>"></span><?php echo $catPerTypeVal;?></a></li>
										 		<?php 
										 		}
										 		$c++;
										 	}
										 }
										?>
									</ul>
								</div>
								<!-- <div class="motCat text-center">
									<ul>
										<li><a><span style="background-color: red;"></span>Personality Type(%)</a></li>
									</ul>
								</div> -->
								<!-- <div id="ptError">No Record Found.</div> -->
							</div>
						</div>
					</div>
					<div class="lead-indicators mt-3">
						<h5 class="ml-3">Behaviours</h5>
						<div class="row">
							<div class="col-md-4">
								<h6 class="ml-3">Beliefs</h6>
								<div id="directing"></div>
								<div id="beliefCate"></div>
								<div class="text-center">
									<button class="graphBackButton" id="directingBackBtn" style="visibility:hidden;">Back</button>
								</div>
							</div>
							<div class="col-md-4">
								<h6 class="ml-3">Kudos</h6>
								<div id="thumbsup"></div>
								<div id="thumbsupCate"></div>
								<div class="text-center">
									<button class="graphBackButton" id="thumbsupBackBtn" style="visibility:hidden;">Back</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="blockRows ml-3 mt-4">
					<h5 class="mb-0">One Time</h5>
					<div class="row">
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['dotComplete']}}%</h2></div>
									</div>
									<div class="text_bottm">
										@if($resultArray['dotRatingCount']['MM'] > 0)
											<span class="green_up">MM: +{{$resultArray['dotRatingCount']['MM']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
										@elseif($resultArray['dotRatingCount']['MM'] < 0)
											<span class="red_down">MM: {{$resultArray['dotRatingCount']['MM']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
										@elseif($resultArray['dotRatingCount']['MM'] == 0)
											<span class="gray_stable">MM: 0%</span>
										@endif
										@if($resultArray['dotRatingCount']['MA'] > 0)
											<span class="green_up">MA: +{{$resultArray['dotRatingCount']['MA']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
										@elseif($resultArray['dotRatingCount']['MA'] < 0)
											<span class="red_down">MA: {{$resultArray['dotRatingCount']['MA']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
										@elseif($resultArray['dotRatingCount']['MA'] == 0)
											<span class="gray_stable">MA: 0%</span>
										@endif
										<strong>DOT Values Complete</strong>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['teamRoleComplete']}}%</h2></div>
									</div>
									<div class="text_bottm">
										@if($resultArray['teamRoleCompletedMMMA']['MM'] > 0)
											<span class="green_up">MM: +{{$resultArray['teamRoleCompletedMMMA']['MM']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
										@elseif($resultArray['teamRoleCompletedMMMA']['MM'] < 0)
											<span class="red_down">MM: {{$resultArray['teamRoleCompletedMMMA']['MM']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
										@elseif($resultArray['teamRoleCompletedMMMA']['MM'] == 0)
											<span class="gray_stable">MM: 0%</span>
										@endif
										@if($resultArray['teamRoleCompletedMMMA']['MA'] > 0)
											<span class="green_up">MA: +{{$resultArray['teamRoleCompletedMMMA']['MA']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
										@elseif($resultArray['teamRoleCompletedMMMA']['MA'] < 0)
											<span class="red_down">MA: {{$resultArray['teamRoleCompletedMMMA']['MA']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
										@elseif($resultArray['teamRoleCompletedMMMA']['MA'] == 0)
											<span class="gray_stable">MA: 0%</span>
										@endif
										<!-- <span>MM: {{$resultArray['teamRoleCompletedMMMA']['MM']}}%</span>
										<span>MA: {{$resultArray['teamRoleCompletedMMMA']['MA']}}%</span> -->
										<strong>Team Roles Complete</strong>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['personalityTypeComplete']}}%</h2></div>
									</div>
									<div class="text_bottm">
										@if($resultArray['personalityTypeCompletedMAMM']['MM'] > 0)
											<span class="green_up">MM: +{{$resultArray['personalityTypeCompletedMAMM']['MM']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
										@elseif($resultArray['personalityTypeCompletedMAMM']['MM'] < 0)
											<span class="red_down">MM: {{$resultArray['personalityTypeCompletedMAMM']['MM']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
										@elseif($resultArray['personalityTypeCompletedMAMM']['MM'] == 0)
											<span class="gray_stable">MM: 0%</span>
										@endif
										@if($resultArray['personalityTypeCompletedMAMM']['MA'] > 0)
											<span class="green_up">MA: +{{$resultArray['personalityTypeCompletedMAMM']['MA']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
										@elseif($resultArray['personalityTypeCompletedMAMM']['MA'] < 0)
											<span class="red_down">MA: {{$resultArray['personalityTypeCompletedMAMM']['MA']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
										@elseif($resultArray['personalityTypeCompletedMAMM']['MA'] == 0)
											<span class="gray_stable">MA: 0%</span>
										@endif
										<!-- <span>MM: {{$resultArray['personalityTypeCompletedMAMM']['MM']}}%</span>
										<span>MA: {{$resultArray['personalityTypeCompletedMAMM']['MA']}}%</span> -->
										<strong>Personality Types Complete</strong>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['motivationComplete']}}%</h2></div>
									</div>
									<div class="text_bottm">
										@if($resultArray['motivationCompletedMAMM']['MM'] > 0)
											<span class="green_up">MM: +{{$resultArray['motivationCompletedMAMM']['MM']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
										@elseif($resultArray['motivationCompletedMAMM']['MM'] < 0)
											<span class="red_down">MM: {{$resultArray['motivationCompletedMAMM']['MM']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
										@elseif($resultArray['motivationCompletedMAMM']['MM'] == 0)
											<span class="gray_stable">MM: 0%</span>
										@endif
										@if($resultArray['motivationCompletedMAMM']['MA'] > 0)
											<span class="green_up">MA: +{{$resultArray['motivationCompletedMAMM']['MA']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
										@elseif($resultArray['motivationCompletedMAMM']['MA'] < 0)
											<span class="red_down">MA: {{$resultArray['motivationCompletedMAMM']['MA']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
										@elseif($resultArray['motivationCompletedMAMM']['MA'] == 0)
											<span class="gray_stable">MA: 0%</span>
										@endif
										<!-- <span>MM: {{$resultArray['motivationCompletedMAMM']['MM']}}%</span>
										<span>MA: {{$resultArray['motivationCompletedMAMM']['MA']}}%</span> -->
										<strong>Motivation Complete</strong>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="blockRows ml-3" style="margin-top: 4em;">
					<h5 class="mb-0">Daily's</h5>
					<div class="row">
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['kudosComplete']}}</h2></div>
									</div>
									<div class="text_bottm">
										@if($resultArray['bubbleRating']['DD'] > 0)
											<span class="green_up">DD: +{{$resultArray['bubbleRating']['DD']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
										@elseif($resultArray['bubbleRating']['DD'] < 0)
											<span class="red_down">DD: {{$resultArray['bubbleRating']['DD']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
										@elseif($resultArray['bubbleRating']['DD'] == 0)
											<span class="gray_stable">DD: 0%</span>
										@endif
										@if($resultArray['bubbleRating']['MA'] > 0)
											<span class="green_up">MA: +{{$resultArray['bubbleRating']['MA']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
										@elseif($resultArray['bubbleRating']['MA'] < 0)
											<span class="red_down">MA: {{$resultArray['bubbleRating']['MA']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
										@elseif($resultArray['bubbleRating']['MA'] == 0)
											<span class="gray_stable">MA: 0%</span>
										@endif
										<!-- <span>DD: {{$resultArray['bubbleRating']['DD']}}%</span>
										<span>MA: {{$resultArray['bubbleRating']['MA']}}%</span> -->
										<strong>Kudos/Person</strong>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="performance-box">
								<div class="performance-text">
									<div class="text_top">
										<div class="text_prstg"><h2> {{$resultArray['happyIndexComplete']}}%</h2></div>
									</div>
									<div class="text_bottm">
										@if($resultArray['happyIndexDDMA']['DD'] > 0)
											<span class="green_up">DD: +{{$resultArray['happyIndexDDMA']['DD']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
										@elseif($resultArray['happyIndexDDMA']['DD'] < 0)
											<span class="red_down">DD: {{$resultArray['happyIndexDDMA']['DD']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
										@elseif($resultArray['happyIndexDDMA']['DD'] == 0)
											<span class="gray_stable">DD: 0%</span>
										@endif
										@if($resultArray['happyIndexDDMA']['MA'] > 0)
											<span class="green_up">MA: +{{$resultArray['happyIndexDDMA']['MA']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
										@elseif($resultArray['happyIndexDDMA']['MA'] < 0)
											<span class="red_down">MA: {{$resultArray['happyIndexDDMA']['MA']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
										@elseif($resultArray['happyIndexDDMA']['MA'] == 0)
											<span class="gray_stable">MA: 0%</span>
										@endif
										<!-- <span>DD: {{$resultArray['happyIndexDDMA']['DD']}}%</span>
										<span>MA: {{$resultArray['happyIndexDDMA']['MA']}}%</span> -->
										<strong>Good/Bad Day Index Completed</strong>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="blockRows ml-3" style="margin-top: 4em;">
						<h5 class="mb-0">Monthly's</h5>
						<div class="row">
							<div class="col-md-3">
								<div class="performance-box">
									<div class="performance-text">
										<div class="text_top">
											<div class="text_prstg"><h2> {{$resultArray['improvements']}}</h2></div>
										</div>
										<div class="text_bottm">
											@if($resultArray['improvementsMAMM']['MM'] > 0)
												<span class="green_up">MM: +{{$resultArray['improvementsMAMM']['MM']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
											@elseif($resultArray['improvementsMAMM']['MM'] < 0)
												<span class="red_down">MM: {{$resultArray['improvementsMAMM']['MM']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
											@elseif($resultArray['improvementsMAMM']['MM'] == 0)
												<span class="gray_stable">MM: 0%</span>
											@endif
											@if($resultArray['improvementsMAMM']['MA'] > 0)
												<span class="green_up">MA: +{{$resultArray['improvementsMAMM']['MA']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
											@elseif($resultArray['improvementsMAMM']['MA'] < 0)
												<span class="red_down">MA: {{$resultArray['improvementsMAMM']['MA']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
											@elseif($resultArray['improvementsMAMM']['MA'] == 0)
												<span class="gray_stable">MA: 0%</span>
											@endif
											<!-- <span>MM: {{$resultArray['improvementsMAMM']['MM']}}%</span>
											<span>MA: {{$resultArray['improvementsMAMM']['MA']}}%</span> -->
											<strong>Improvements sent/person</strong>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="performance-box">
									<div class="performance-text">
										<div class="text_top">
											<div class="text_prstg"><h2> {{$resultArray['cultureStructure']}}%</h2></div>
										</div>
										<div class="text_bottm">
											@if($resultArray['cultureStructureCompletedMAMM']['MM'] > 0)
												<span class="green_up">MM: +{{$resultArray['cultureStructureCompletedMAMM']['MM']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
											@elseif($resultArray['cultureStructureCompletedMAMM']['MM'] < 0)
												<span class="red_down">MM: {{$resultArray['cultureStructureCompletedMAMM']['MM']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
											@elseif($resultArray['cultureStructureCompletedMAMM']['MM'] == 0)
												<span class="gray_stable">MM: 0%</span>
											@endif
											@if($resultArray['cultureStructureCompletedMAMM']['MA'] > 0)
												<span class="green_up">MA: +{{$resultArray['cultureStructureCompletedMAMM']['MA']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
											@elseif($resultArray['cultureStructureCompletedMAMM']['MA'] < 0)
												<span class="red_down">MA: {{$resultArray['cultureStructureCompletedMAMM']['MA']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
											@elseif($resultArray['cultureStructureCompletedMAMM']['MA'] == 0)
												<span class="gray_stable">MA: 0%</span>
											@endif
											<!-- <span>MM: {{$resultArray['cultureStructureCompletedMAMM']['MM']}}%</span>
											<span>MA: {{$resultArray['cultureStructureCompletedMAMM']['MA']}}%</span> -->
											<strong>Organisation Structure Updated</strong>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="performance-box">
									<div class="performance-text">
										<div class="text_top">
											<div class="text_prstg"><h2> {{$resultArray['tribeometer']}}%</h2></div>
										</div>
										<div class="text_bottm">
											@if($resultArray['tribeometerUpdatedMAMM']['MM'] > 0)
												<span class="green_up">MM: +{{$resultArray['tribeometerUpdatedMAMM']['MM']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
											@elseif($resultArray['tribeometerUpdatedMAMM']['MM'] < 0)
												<span class="red_down">MM: {{$resultArray['tribeometerUpdatedMAMM']['MM']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
											@elseif($resultArray['tribeometerUpdatedMAMM']['MM'] == 0)
												<span class="gray_stable">MM: 0%</span>
											@endif
											@if($resultArray['tribeometerUpdatedMAMM']['MA'] > 0)
												<span class="green_up">MA: +{{$resultArray['tribeometerUpdatedMAMM']['MA']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
											@elseif($resultArray['tribeometerUpdatedMAMM']['MA'] < 0)
												<span class="red_down">MA: {{$resultArray['tribeometerUpdatedMAMM']['MA']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
											@elseif($resultArray['tribeometerUpdatedMAMM']['MA'] == 0)
												<span class="gray_stable">MA: 0%</span>
											@endif
											<!-- <span>MM: {{$resultArray['tribeometerUpdatedMAMM']['MM']}}%</span>
											<span>MA: {{$resultArray['tribeometerUpdatedMAMM']['MA']}}%</span> -->
											<strong>Tribeometer Updated</strong>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="performance-box">
									<div class="performance-text">
										<div class="text_top">
											<div class="text_prstg"><h2> {{$resultArray['diagnostic']}}%</h2></div>
										</div>
										<div class="text_bottm">
											@if($resultArray['diagnosticUpdatedMAMM']['MM'] > 0)
												<span class="green_up">MM: +{{$resultArray['diagnosticUpdatedMAMM']['MM']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
											@elseif($resultArray['diagnosticUpdatedMAMM']['MM'] < 0)
												<span class="red_down">MM: {{$resultArray['diagnosticUpdatedMAMM']['MM']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
											@elseif($resultArray['diagnosticUpdatedMAMM']['MM'] == 0)
												<span class="gray_stable">MM: 0%</span>
											@endif
											@if($resultArray['diagnosticUpdatedMAMM']['MA'] > 0)
												<span class="green_up">MA: +{{$resultArray['diagnosticUpdatedMAMM']['MA']}}%<img src="{{asset('public/images/up.png')}}" alt="" /></span>
											@elseif($resultArray['diagnosticUpdatedMAMM']['MA'] < 0)
												<span class="red_down">MA: {{$resultArray['diagnosticUpdatedMAMM']['MA']}}%<img src="{{asset('public/images/down.png')}}" alt="" /></span>
											@elseif($resultArray['diagnosticUpdatedMAMM']['MA'] == 0)
												<span class="gray_stable">MA: 0%</span>
											@endif
											<!-- <span>MM: {{$resultArray['diagnosticUpdatedMAMM']['MM']}}%</span>
											<span>MA: {{$resultArray['diagnosticUpdatedMAMM']['MA']}}%</span> -->
											<strong>Diagnostic Updated</strong>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>
</div>
</div>
</main>


<script type="text/javascript">
	$('.dashboard-report-form').on('click',function(){
		$('.loader-img').show();			

		var form_values   = $("#dashboard-report-form").serializeArray();

		var orgId      = form_values[1]['value'];
		var officeId      = form_values[2]['value'];
		var departmentId  = form_values[3]['value'];

		console.log(orgId);
		console.log(officeId);
		console.log(departmentId);

		// return false;
		// var orgId         = "{{base64_encode($orgId)}}";
		
		console.log(form_values)
		$.ajax({
			type: "GET",
			url: "{{URL::to('admin/admin-dashboard')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				$('.loader-img').hide();
				// $('#cot2').html('');
				// $('#cot5').html('');
				// $('#cot3').html('');
				// //$('#cot6').html('');	
				$('body').html('');	
				$('body').append(response);
			}
		});		
	});
</script>

<script type="text/javascript">
	$('#dashboard-org').on('change', function() {
		$('#dashboard-office option').remove();
		$('#dashboard-department option').remove();
		var orgId = $('#dashboard-org').val();
		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getOfficeByOrgIdforGraph')!!}",				
			data: {orgId:orgId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{				
				$('.loader-img').hide();	
				// console.log(response.office);
				$('#dashboard-office').append('<option value="" selected>All office</option>');
				$('#dashboard-office').append(response.office);

				$('#dashboard-department').append('<option value="" selected>All Department</option>');
				$('#dashboard-department').append(response.department);

			}
		});
	});
</script>

<script type="text/javascript">
	$('#dashboard-office').on('change', function() {
		$('#dashboard-department option').remove();
		var officeId = $('#dashboard-office').val();
		var orgId = $('#dashboard-org').val();

		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getDepartmentByOfficecIdforGraph')!!}",				
			data: {officeId:officeId,orgId:orgId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{					
				$('.loader-img').hide();
				$('#dashboard-department').append('<option value="" selected>All Department</option>');
				$('#dashboard-department').append(response);
			}
		});
	});
</script>



<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/loader.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/Chart.js'); ?>"></script>

<!-- <script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script> -->

<!-- <script type="text/javascript" src="<?php //echo url('/public/extranal_js_css/fusioncharts.js'); ?>"></script>
<script type="text/javascript" src="<?php //echo url('/public/extranal_js_css/fusioncharts.theme.fusion.js'); ?>"></script> -->


<script type="text/javascript">
	$('.loader-img').show();
	google.charts.load('current', {'packages':['line', 'corechart']});
	google.charts.setOnLoadCallback(drawCharts);

	function drawCharts() {
		drawCultureIndex('cultureIndex');
		drawEngagementIndex('engagementIndex');
		drawHappyIndex('happyIndex');
		drawDiagnostic('diagnostic');
		drawDirecting('directing');
		drawThumbsup('thumbsup');
		drawTribeometer('tribeometer');
		drawTeamRole('teamRole');
		drawMotivation('motivation');
		drawPersonalityType('personalityType');
		drawCultureStructure('cultureStructure');
		drawHappyIndexResponders('happyIndexResponders');
		$('.loader-img').hide();
	}	
</script>

<script type="text/javascript">
	function drawCultureIndex(containerId) {
		var data = google.visualization.arrayToDataTable([
			['', 'Culture Index'],
			@foreach($resultArray['cultureIndexArray']['indexArray'] as $value)
			["{{$value['monthName']}}",{{$value['data']}}],
			@endforeach

			]);
		var options = {
			width: '100%',
			height: '100%',
			colors: ['#ff454b'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if($resultArray['cultureIndexArray']['maxCount'] <= 0)
						max: 100
					@endif
					// min: 0,
					// max: 1000
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				// height: '50%',
				width: '100%'
			},
			legend: 'none',
			pointSize: 5
			// legend: { position: 'bottom',
			// 	textStyle: {bold:true},
			// }
		};
		var chart = new google.visualization.LineChart(document.getElementById(containerId));
		// var chart = new google.charts.Line(cultureIndex);
		chart.draw(data, options);
	}
</script>

<script type="text/javascript">
	function drawEngagementIndex(containerId) {
		var data = google.visualization.arrayToDataTable([
			['', 'Engagement Index'],
			@foreach($resultArray['engagementIndexArray']['indexArray'] as $value)
			["{{$value['monthName']}}",{{$value['data']}}],
			@endforeach

			]);
		var options = {
			width: '100%',
			height: '100%',
			colors: ['#ff454b'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if($resultArray['engagementIndexArray']['maxCount'] <= 0)
						max: 100
					@endif
					// min: 0,
					// max: 1000
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				// height: '50%',
				width: '100%'
			},
			legend: 'none',
			pointSize: 5
			// legend: { position: 'bottom',
			// 	textStyle: {bold:true},
			// }
		};
		var chart = new google.visualization.LineChart(document.getElementById(containerId));
		chart.draw(data, options);
	}
</script>

<script type="text/javascript">
	function drawHappyIndex(containerId) {

		//Happy index month graph
		getHappyIndexMonthGraph(<?php echo json_encode($resultArray['happyIndexArray']['arrMain'][0]);?>,<?php echo json_encode($resultArray['happyIndexArray']['arrMain']);?>,containerId);

		// var happyIndexCategory = '';

		// happyIndexCategory = '<div class="motCat">'+
		// '<ul>'+
		// '<li>'+
		// '<a onclick="getHappyIndexCat();">'+
		// '<span style="background-color: red;"></span>'+
		// 'All</a>'+
		// '</li>';

		// var catArr = <?php //echo json_encode($resultArray['happyIndexArray']['arrMain'][0]);?>;

		// if (catArr) {
		// 	var c=0;
		// 	var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
		// 	//Loop Array
		// 	$.each(catArr,function(key,value){
		// 		if(c>0){
		//  			var columColor = catClr[c];
		//  			happyIndexCategory += '<li><a onclick="getHappyIndexCat('+key+',\'' + columColor + '\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		//  		}
		//  		c++;
		// 	});
		// }
		// happyIndexCategory += '</ul></div>';
		
		// $('#happyIndexCate').append(happyIndexCategory);

		// var data = google.visualization.arrayToDataTable(<?php //echo json_encode($resultArray['happyIndexArray']['arrMain']);?>);
		// var options = {
		// 	width: '100%',
		// 	height: '100%',
		// 	colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
		// 	hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 	vAxis: {
		// 		minValue: 0,
		// 		viewWindow: {
		// 			@if($resultArray['happyIndexArray']['maxCount'] <= 0)
		// 				max: 100
		// 			@endif
		// 			// min: 0,
		// 			// max: 100
		// 		}
		// 	},
		// 	// curveType: 'function',
		// 	chartArea: {
		// 		top: 30,
		// 		left: 50,
		// 		// height: '50%',
		// 		width: '100%'
		// 	},
		// 	legend: 'none',
		// 	pointSize: 5
		// 	// legend: { position: 'bottom',
		// 	// 	textStyle: {bold:true},
		// 	// }
		// };
		// var chart = new google.visualization.LineChart(document.getElementById(containerId));
		// chart.draw(data, options);

		// google.visualization.events.addListener(chart, 'select', selectHandler);

  //      	function selectHandler() {

		// 	var selectedItem = chart.getSelection();

		// 	if (selectedItem[0]) {
		// 	    // if (selectedItem[0]) {
		// 		// if (selectedItem[0].row) {
		// 			var monthNum 	 = selectedItem[0].row;
		// 			var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
		// 		// }else{
		// 		// 	var monthNum 	 = 0;
		// 		// 	var value = 0;
		// 		// }
		// 	    // }
		// 	}else{
		// 		var monthNum 	 = 0;
		// 		var value = 0;
		// 	}
		// 	// console.log(monthNum);
		// 	// console.log(value);
		// 	// return false;
		//     if (value>0) {
		//     	console.log(monthNum);

		//     	$('#monthNum').val(monthNum);

		// 		var form_values   = $("#dashboard-report-form").serializeArray();
		// 		var orgId  			= form_values[1]['value'];
		// 		var officeId      	= form_values[2]['value'];
		// 		var departmentId  	= form_values[3]['value'];


		// 		//Happy index week graph
		// 		$.ajax({
		// 			type: "POST",
		// 			//dataType:"json",
		// 			url: "{{URL::to('admin/getHappyIndexWeekGraph')}}",
		// 			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,monthNum:monthNum,"_token":'<?php echo csrf_token()?>'},
		// 			success: function(response)
		// 			{
		// 				$('#happyIndexCate').html('');

		// 				var happyIndexCategory = '';

		// 				happyIndexCategory = '<div class="motCat">'+
		// 				'<ul>'+
		// 				'<li>'+
		// 				'<a onclick="getHappyIndexWeekCat(\'\',\'\',\''+monthNum+'\');">'+
		// 				'<span style="background-color: red;"></span>'+
		// 				'All</a>'+
		// 				'</li>';

		// 				var catArr = response.arrMain[0];

		// 				if (catArr) {
		// 					var c=0;
		// 					var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
		// 					//Loop Array
		// 					$.each(catArr,function(key,value){
		// 						if(c>0){
		// 				 			var columColor = catClr[c];
		// 				 			happyIndexCategory += '<li><a onclick="getHappyIndexWeekCat('+key+',\'' + columColor + '\',\''+monthNum+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		// 				 		}
		// 				 		c++;
		// 					});
		// 				}
		// 				happyIndexCategory += '</ul></div>';
						
		// 				$('#happyIndexCate').append(happyIndexCategory);

		// 				$('#happyIndexWeekBackBtn').css('visibility','visible');

		// 				var data = google.visualization.arrayToDataTable(response.arrMain);
		// 			    var options = {
		// 					width: '100%',
		// 					height: '100%',
		// 					colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
		// 					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 					vAxis: {
		// 						minValue: 0,
		// 						viewWindow: {
		// 							// min: 0,
		// 							// max: 100
		// 						}
		// 					},
		// 					// curveType: 'function',
		// 					chartArea: {
		// 						top: 30,
		// 						left: 50,
		// 						// height: '50%',
		// 						width: '100%'
		// 					},
		// 					legend: 'none',
		// 					pointSize: 5
		// 				};
		// 				var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
		// 				chart.draw(data, options);

		// 				google.visualization.events.addListener(chart, 'select', selectHandler1);

		// 		       	function selectHandler1() {

		// 		       		var yearMonth = response.yearMonth;

		// 					var selectedItem = chart.getSelection();

		// 					if (selectedItem[0]) {
		// 					    // if (selectedItem[0]) {
		// 						// if (selectedItem[0].row) {
		// 							var weekNum 	 = selectedItem[0].row;
		// 							var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
		// 						// }else{
		// 						// 	var weekNum 	 = 0;
		// 						// 	var value = 0;
		// 						// }
		// 					    // }
		// 					}else{
		// 						var weekNum 	 = 0;
		// 						var value = 0;
		// 					}
		// 				    if (value>0) {
		// 						var form_values   = $("#dashboard-report-form").serializeArray();
		// 						var orgId  			= form_values[1]['value'];
		// 						var officeId      	= form_values[2]['value'];
		// 						var departmentId  	= form_values[3]['value'];

		// 						//happy index days graph
		// 						$.ajax({
		// 							type: "POST",
		// 							//dataType:"json",
		// 							url: "{{URL::to('admin/getHappyIndexDaysGraph')}}",
		// 							data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
		// 							success: function(response)
		// 							{
		// 								$('#happyIndexCate').html('');

		// 								var happyIndexCategory = '';

		// 								happyIndexCategory = '<div class="motCat">'+
		// 								'<ul>'+
		// 								'<li>'+
		// 								'<a onclick="getHappyIndexDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
		// 								'<span style="background-color: red;"></span>'+
		// 								'All</a>'+
		// 								'</li>';

		// 								var catArr = response.arrMain[0];

		// 								if (catArr) {
		// 									var c=0;
		// 									var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
		// 									//Loop Array
		// 									$.each(catArr,function(key,value){
		// 										if(c>0){
		// 								 			var columColor = catClr[c];
		// 								 			happyIndexCategory += '<li><a onclick="getHappyIndexDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		// 								 		}
		// 								 		c++;
		// 									});
		// 								}
		// 								happyIndexCategory += '</ul></div>';
										
		// 								$('#happyIndexCate').append(happyIndexCategory);

		// 								$('#happyIndexWeekBackBtn').css('visibility','hidden');
		// 								$('#happyIndexDaysBackBtn').css('visibility','visible');

		// 								var data = google.visualization.arrayToDataTable(response.arrMain);
		// 							    var options = {
		// 									width: '100%',
		// 									height: '100%',
		// 									colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
		// 									hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 									vAxis: {
		// 										minValue: 0,
		// 										viewWindow: {
		// 											// min: 0,
		// 											// max: 100
		// 										}
		// 									},
		// 									// curveType: 'function',
		// 									chartArea: {
		// 										top: 30,
		// 										left: 50,
		// 										// height: '50%',
		// 										width: '100%'
		// 									},
		// 									legend: 'none',
		// 									pointSize: 5
		// 								};
		// 								var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
		// 								chart.draw(data, options);
		// 							}
		// 						});	

		// 						//happy index reponders days graph
		// 						$.ajax({
		// 							type: "POST",
		// 							//dataType:"json",
		// 							url: "{{URL::to('admin/getHappyIndexRespDaysGraph')}}",
		// 							data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
		// 							success: function(response)
		// 							{
		// 								$('#happyIndexResCate').html('');

		// 								var happyIndexCategory = '';

		// 								happyIndexCategory = '<div class="motCat">'+
		// 								'<ul>'+
		// 								'<li>'+
		// 								'<a onclick="getHappyIndexRespDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
		// 								'<span style="background-color: red;"></span>'+
		// 								'All</a>'+
		// 								'</li>';

		// 								var catArr = response.arrMain[0];

		// 								if (catArr) {
		// 									var c=0;
		// 									var catClr = ['','#000','#eb1c24'];
		// 									//Loop Array
		// 									$.each(catArr,function(key,value){
		// 										if(c>0){
		// 								 			var columColor = catClr[c];
		// 								 			happyIndexCategory += '<li><a onclick="getHappyIndexRespDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		// 								 		}
		// 								 		c++;
		// 									});
		// 								}
		// 								happyIndexCategory += '</ul></div>';
										
		// 								$('#happyIndexResCate').append(happyIndexCategory);

		// 								$('#happyIndexResWeekBackBtn').css('visibility','hidden');
		// 								$('#happyIndexResDaysBackBtn').css('visibility','visible');

		// 								var data = google.visualization.arrayToDataTable(response.arrMain);
		// 							    var options = {
		// 									width: '100%',
		// 									height: '100%',
		// 									colors: ['#000','#eb1c24'],
		// 									hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 									vAxis: {
		// 										minValue: 0,
		// 										viewWindow: {
		// 											// min: 0,
		// 											// max: 100
		// 										}
		// 									},
		// 									// curveType: 'function',
		// 									chartArea: {
		// 										top: 30,
		// 										left: 50,
		// 										// height: '50%',
		// 										width: '100%'
		// 									},
		// 									legend: 'none',
		// 									pointSize: 5
		// 								};
		// 								var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
		// 								chart.draw(data, options);
		// 							}
		// 						});
		// 					}
		// 				}
		// 			}
		// 		});
				
		// 		//Happy index responders week graph
		// 		getHappyIndexRespWeekGraph(orgId,officeId,departmentId,monthNum);
		// 	}
		// }
	}
</script>

<script type="text/javascript">
	$('#happyIndexWeekBackBtn').on('click',function(){

		$('#happyIndexWeekBackBtn').css('visibility','hidden');
		$('#happyIndexResWeekBackBtn').css('visibility','hidden');

		//Happy index month graph
		getHappyIndexMonthGraph(<?php echo json_encode($resultArray['happyIndexArray']['arrMain'][0]);?>,<?php echo json_encode($resultArray['happyIndexArray']['arrMain']);?>,'happyIndex');

		//Happy index responders month graph
		getHappyIndexRespMonthGraph(<?php echo json_encode($resultArray['happyIndexResArray']['arrMain'][0]);?>,<?php echo json_encode($resultArray['happyIndexResArray']['arrMain']);?>,'happyIndexResponders');

		// $('#happyIndexCate').html('');

		// var happyIndexCategory = '';
		// happyIndexCategory = '<div class="motCat">'+
		// '<ul>'+
		// '<li>'+
		// '<a onclick="getHappyIndexCat();">'+
		// '<span style="background-color: red;"></span>'+
		// 'All</a>'+
		// '</li>';

		// var catArr = <?php //echo json_encode($resultArray['happyIndexArray']['arrMain'][0]);?>;

		// if (catArr) {
		// 	var c=0;
		// 	var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
		// 	//Loop Array
		// 	$.each(catArr,function(key,value){
		// 		if(c>0){
		//  			var columColor = catClr[c];
		//  			happyIndexCategory += '<li><a onclick="getHappyIndexCat('+key+',\'' + columColor + '\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		//  		}
		//  		c++;
		// 	});
		// }
		// happyIndexCategory += '</ul></div>';
		
		// $('#happyIndexCate').append(happyIndexCategory);

		// var data = google.visualization.arrayToDataTable(<?php //echo json_encode($resultArray['happyIndexArray']['arrMain']);?>);
		// var options = {
		// 	width: '100%',
		// 	height: '100%',
		// 	colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
		// 	hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 	vAxis: {
		// 		minValue: 0,
		// 		viewWindow: {
		// 			@if($resultArray['happyIndexArray']['maxCount'] <= 0)
		// 				max: 100
		// 			@endif
		// 			// min: 0,
		// 			// max: 100
		// 		}
		// 	},
		// 	// curveType: 'function',
		// 	chartArea: {
		// 		top: 30,
		// 		left: 50,
		// 		// height: '50%',
		// 		width: '100%'
		// 	},
		// 	legend: 'none',
		// 	pointSize: 5
		// 	// legend: { position: 'bottom',
		// 	// 	textStyle: {bold:true},
		// 	// }
		// };
		// var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
		// chart.draw(data, options);

		// google.visualization.events.addListener(chart, 'select', selectHandler);

  //      	function selectHandler() {

		// 	var selectedItem = chart.getSelection();
		// 	if (selectedItem[0]) {
		// 		if (selectedItem[0].row) {
		// 			var monthNum 	 = selectedItem[0].row;
		// 			var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
		// 		}else{
		// 			var monthNum 	 = 0;
		// 			var value = 0;
		// 		}
		// 	}else{
		// 		var monthNum 	 = 0;
		// 		var value = 0;
		// 	}
		//     if (value>0) {
		//     	console.log(monthNum);
		//     	$('#monthNum').val(monthNum);

		// 		var form_values   = $("#dashboard-report-form").serializeArray();
		// 		var orgId  			= form_values[1]['value'];
		// 		var officeId      	= form_values[2]['value'];
		// 		var departmentId  	= form_values[3]['value'];

		// 		$.ajax({
		// 			type: "POST",
		// 			//dataType:"json",
		// 			url: "{{URL::to('admin/getHappyIndexWeekGraph')}}",
		// 			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,monthNum:monthNum,"_token":'<?php echo csrf_token()?>'},
		// 			success: function(response)
		// 			{
		// 				$('#happyIndexCate').html('');

		// 				var happyIndexCategory = '';

		// 				happyIndexCategory = '<div class="motCat">'+
		// 				'<ul>'+
		// 				'<li>'+
		// 				'<a onclick="getHappyIndexWeekCat(\'\',\'\',\''+monthNum+'\');">'+
		// 				'<span style="background-color: red;"></span>'+
		// 				'All</a>'+
		// 				'</li>';

		// 				var catArr = response.arrMain[0];

		// 				if (catArr) {
		// 					var c=0;
		// 					var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
		// 					//Loop Array
		// 					$.each(catArr,function(key,value){
		// 						if(c>0){
		// 				 			var columColor = catClr[c];
		// 				 			happyIndexCategory += '<li><a onclick="getHappyIndexWeekCat('+key+',\'' + columColor + '\',\''+monthNum+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		// 				 		}
		// 				 		c++;
		// 					});
		// 				}
		// 				happyIndexCategory += '</ul></div>';
						
		// 				$('#happyIndexCate').append(happyIndexCategory);

		// 				$('#happyIndexWeekBackBtn').css('visibility','visible');

		// 				var data = google.visualization.arrayToDataTable(response.arrMain);
		// 			    var options = {
		// 					width: '100%',
		// 					height: '100%',
		// 					colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
		// 					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 					vAxis: {
		// 						minValue: 0,
		// 						viewWindow: {
		// 							// min: 0,
		// 							// max: 100
		// 						}
		// 					},
		// 					// curveType: 'function',
		// 					chartArea: {
		// 						top: 30,
		// 						left: 50,
		// 						// height: '50%',
		// 						width: '100%'
		// 					},
		// 					legend: 'none',
		// 					pointSize: 5
		// 				};
		// 				var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
		// 				chart.draw(data, options);

		// 				google.visualization.events.addListener(chart, 'select', selectHandler1);

		// 		       	function selectHandler1() {

		// 		       		var yearMonth = response.yearMonth;

		// 					var selectedItem = chart.getSelection();

		// 					if (selectedItem[0]) {
		// 					    // if (selectedItem[0]) {
		// 						// if (selectedItem[0].row) {
		// 							var weekNum 	 = selectedItem[0].row;
		// 							var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
		// 						// }else{
		// 						// 	var weekNum 	 = 0;
		// 						// 	var value = 0;
		// 						// }
		// 					    // }
		// 					}else{
		// 						var weekNum 	 = 0;
		// 						var value = 0;
		// 					}
		// 				    if (value>0) {
		// 						var form_values   = $("#dashboard-report-form").serializeArray();
		// 						var orgId  			= form_values[1]['value'];
		// 						var officeId      	= form_values[2]['value'];
		// 						var departmentId  	= form_values[3]['value'];

		// 						$.ajax({
		// 							type: "POST",
		// 							//dataType:"json",
		// 							url: "{{URL::to('admin/getHappyIndexDaysGraph')}}",
		// 							data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
		// 							success: function(response)
		// 							{
		// 								$('#happyIndexCate').html('');

		// 								var happyIndexCategory = '';

		// 								happyIndexCategory = '<div class="motCat">'+
		// 								'<ul>'+
		// 								'<li>'+
		// 								'<a onclick="getHappyIndexDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
		// 								'<span style="background-color: red;"></span>'+
		// 								'All</a>'+
		// 								'</li>';

		// 								var catArr = response.arrMain[0];

		// 								if (catArr) {
		// 									var c=0;
		// 									var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
		// 									//Loop Array
		// 									$.each(catArr,function(key,value){
		// 										if(c>0){
		// 								 			var columColor = catClr[c];
		// 								 			happyIndexCategory += '<li><a onclick="getHappyIndexDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		// 								 		}
		// 								 		c++;
		// 									});
		// 								}
		// 								happyIndexCategory += '</ul></div>';
										
		// 								$('#happyIndexCate').append(happyIndexCategory);

		// 								$('#happyIndexWeekBackBtn').css('visibility','hidden');
		// 								$('#happyIndexDaysBackBtn').css('visibility','visible');

		// 								var data = google.visualization.arrayToDataTable(response.arrMain);
		// 							    var options = {
		// 									width: '100%',
		// 									height: '100%',
		// 									colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
		// 									hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 									vAxis: {
		// 										minValue: 0,
		// 										viewWindow: {
		// 											// min: 0,
		// 											// max: 100
		// 										}
		// 									},
		// 									// curveType: 'function',
		// 									chartArea: {
		// 										top: 30,
		// 										left: 50,
		// 										// height: '50%',
		// 										width: '100%'
		// 									},
		// 									legend: 'none',
		// 									pointSize: 5
		// 								};
		// 								var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
		// 								chart.draw(data, options);
		// 							}
		// 						});	
		// 					}
		// 				}
		// 			}
		// 		});	
		// 	}
		// }
	});
</script>

<script type="text/javascript">
	$('#happyIndexDaysBackBtn').on('click',function(){

		var monthNum = $('#monthNum').val();

		var form_values   = $("#dashboard-report-form").serializeArray();
		var orgId  			= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		//Happy index week graph
		getHappyIndexWeekGraph(orgId,officeId,departmentId,monthNum);

		//Happy index responders week graph
		getHappyIndexRespWeekGraph(orgId,officeId,departmentId,monthNum);

		// $.ajax({
		// 	type: "POST",
		// 	//dataType:"json",
		// 	url: "{{URL::to('admin/getHappyIndexWeekGraph')}}",
		// 	data: {orgId:orgId,officeId:officeId,departmentId:departmentId,monthNum:monthNum,"_token":'<?php echo csrf_token()?>'},
		// 	success: function(response)
		// 	{
		// 		$('#happyIndexCate').html('');

		// 		var happyIndexCategory = '';

		// 		happyIndexCategory = '<div class="motCat">'+
		// 		'<ul>'+
		// 		'<li>'+
		// 		'<a onclick="getHappyIndexWeekCat(\'\',\'\',\''+monthNum+'\');">'+
		// 		'<span style="background-color: red;"></span>'+
		// 		'All</a>'+
		// 		'</li>';

		// 		var catArr = response.arrMain[0];

		// 		if (catArr) {
		// 			var c=0;
		// 			var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
		// 			//Loop Array
		// 			$.each(catArr,function(key,value){
		// 				if(c>0){
		// 		 			var columColor = catClr[c];
		// 		 			happyIndexCategory += '<li><a onclick="getHappyIndexWeekCat('+key+',\'' + columColor + '\',\''+monthNum+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		// 		 		}
		// 		 		c++;
		// 			});
		// 		}
		// 		happyIndexCategory += '</ul></div>';
				
		// 		$('#happyIndexCate').append(happyIndexCategory);

		// 		$('#happyIndexDaysBackBtn').css('visibility','hidden');
		// 		$('#happyIndexWeekBackBtn').css('visibility','visible');

		// 		var data = google.visualization.arrayToDataTable(response.arrMain);
		// 	    var options = {
		// 			width: '100%',
		// 			height: '100%',
		// 			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
		// 			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 			vAxis: {
		// 				minValue: 0,
		// 				viewWindow: {
		// 					// min: 0,
		// 					// max: 100
		// 				}
		// 			},
		// 			// curveType: 'function',
		// 			chartArea: {
		// 				top: 30,
		// 				left: 50,
		// 				// height: '50%',
		// 				width: '100%'
		// 			},
		// 			legend: 'none',
		// 			pointSize: 5
		// 		};
		// 		var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
		// 		chart.draw(data, options);

		// 		google.visualization.events.addListener(chart, 'select', selectHandler1);

		//        	function selectHandler1() {

		//        		var yearMonth = response.yearMonth;

		// 			var selectedItem = chart.getSelection();

		// 			if (selectedItem[0]) {
		// 			    // if (selectedItem[0]) {
		// 				// if (selectedItem[0].row) {
		// 					var weekNum 	 = selectedItem[0].row;
		// 					var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
		// 				// }else{
		// 				// 	var weekNum 	 = 0;
		// 				// 	var value = 0;
		// 				// }
		// 			    // }
		// 			}else{
		// 				var weekNum 	 = 0;
		// 				var value = 0;
		// 			}
		// 		    if (value>0) {
		// 				var form_values   = $("#dashboard-report-form").serializeArray();
		// 				var orgId  			= form_values[1]['value'];
		// 				var officeId      	= form_values[2]['value'];
		// 				var departmentId  	= form_values[3]['value'];

		// 				$.ajax({
		// 					type: "POST",
		// 					//dataType:"json",
		// 					url: "{{URL::to('admin/getHappyIndexDaysGraph')}}",
		// 					data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
		// 					success: function(response)
		// 					{
		// 						$('#happyIndexCate').html('');

		// 						var happyIndexCategory = '';

		// 						happyIndexCategory = '<div class="motCat">'+
		// 						'<ul>'+
		// 						'<li>'+
		// 						'<a onclick="getHappyIndexDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
		// 						'<span style="background-color: red;"></span>'+
		// 						'All</a>'+
		// 						'</li>';

		// 						var catArr = response.arrMain[0];

		// 						if (catArr) {
		// 							var c=0;
		// 							var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
		// 							//Loop Array
		// 							$.each(catArr,function(key,value){
		// 								if(c>0){
		// 						 			var columColor = catClr[c];
		// 						 			happyIndexCategory += '<li><a onclick="getHappyIndexDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		// 						 		}
		// 						 		c++;
		// 							});
		// 						}
		// 						happyIndexCategory += '</ul></div>';
								
		// 						$('#happyIndexCate').append(happyIndexCategory);

		// 						$('#happyIndexWeekBackBtn').css('visibility','hidden');
		// 						$('#happyIndexDaysBackBtn').css('visibility','visible');

		// 						var data = google.visualization.arrayToDataTable(response.arrMain);
		// 					    var options = {
		// 							width: '100%',
		// 							height: '100%',
		// 							colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
		// 							hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 							vAxis: {
		// 								minValue: 0,
		// 								viewWindow: {
		// 									// min: 0,
		// 									// max: 100
		// 								}
		// 							},
		// 							// curveType: 'function',
		// 							chartArea: {
		// 								top: 30,
		// 								left: 50,
		// 								// height: '50%',
		// 								width: '100%'
		// 							},
		// 							legend: 'none',
		// 							pointSize: 5
		// 						};
		// 						var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
		// 						chart.draw(data, options);
		// 					}
		// 				});	
		// 			}
		// 		}
		// 	}
		// });	
	});
</script>

<script type="text/javascript">
	function getHappyIndexCat(moodId=null,columnColor=null) {
		
		var form_values   	= $("#dashboard-report-form").serializeArray();

		var orgId      		= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getHappyIndexCat')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,moodId:moodId,columnColor:columnColor,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				if (response.columnColor==null) {
					var lineColors = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
				}else {
					var lineColors = [columnColor];
				}

				if (response.maxCount <= 0) {
					var maxCount = 100;
				}

				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					// colors: ['#ff454b'],
					colors: lineColors,
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							max: maxCount
							// min: 0,
							// max: 100
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
				};
				var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
				chart.draw(data, options);

				google.visualization.events.addListener(chart, 'select', selectHandler);

		       	function selectHandler() {

					var selectedItem = chart.getSelection();
					if (selectedItem[0]) {
						if (selectedItem[0].row) {
							var monthNum = selectedItem[0].row;
							var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
						}else{
							var monthNum = 0;
							var value = 0;
						}
					}else{
						var monthNum = 0;
						var value = 0;
					}
				    if (value>0) {
				    	console.log(monthNum);
				    	$('#monthNum').val(monthNum);
				    	$('#monthNumHI').val(monthNum);

						var form_values   	= $("#dashboard-report-form").serializeArray();
						var orgId  			= form_values[1]['value'];
						var officeId      	= form_values[2]['value'];
						var departmentId  	= form_values[3]['value'];

						//Happy index week graph
						getHappyIndexWeekGraph(orgId,officeId,departmentId,monthNum);

						//Happy index responders week graph
						getHappyIndexRespWeekGraph(orgId,officeId,departmentId,monthNum);

						// $.ajax({
						// 	type: "POST",
						// 	//dataType:"json",
						// 	url: "{{URL::to('admin/getHappyIndexWeekGraph')}}",
						// 	data: {orgId:orgId,officeId:officeId,departmentId:departmentId,monthNum:monthNum,"_token":'<?php echo csrf_token()?>'},
						// 	success: function(response)
						// 	{
						// 		$('#happyIndexCate').html('');

						// 		var happyIndexCategory = '';

						// 		happyIndexCategory = '<div class="motCat">'+
						// 		'<ul>'+
						// 		'<li>'+
						// 		'<a onclick="getHappyIndexWeekCat(\'\',\'\',\''+monthNum+'\');">'+
						// 		'<span style="background-color: red;"></span>'+
						// 		'All</a>'+
						// 		'</li>';

						// 		var catArr = response.arrMain[0];

						// 		if (catArr) {
						// 			var c=0;
						// 			var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
						// 			//Loop Array
						// 			$.each(catArr,function(key,value){
						// 				if(c>0){
						// 		 			var columColor = catClr[c];
						// 		 			happyIndexCategory += '<li><a onclick="getHappyIndexWeekCat('+key+',\'' + columColor + '\',\''+monthNum+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
						// 		 		}
						// 		 		c++;
						// 			});
						// 		}
						// 		happyIndexCategory += '</ul></div>';
								
						// 		$('#happyIndexCate').append(happyIndexCategory);

						// 		$('#happyIndexWeekBackBtn').css('visibility','visible');

						// 		var data = google.visualization.arrayToDataTable(response.arrMain);
						// 	    var options = {
						// 			width: '100%',
						// 			height: '100%',
						// 			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
						// 			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
						// 			vAxis: {
						// 				minValue: 0,
						// 				viewWindow: {
						// 					// min: 0,
						// 					// max: 100
						// 				}
						// 			},
						// 			// curveType: 'function',
						// 			chartArea: {
						// 				top: 30,
						// 				left: 50,
						// 				// height: '50%',
						// 				width: '100%'
						// 			},
						// 			legend: 'none',
						// 			pointSize: 5
						// 		};
						// 		var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
						// 		chart.draw(data, options);

						// 		google.visualization.events.addListener(chart, 'select', selectHandler1);

						//        	function selectHandler1() {

						//        		var yearMonth = response.yearMonth;

						// 			var selectedItem = chart.getSelection();
						// 			if (selectedItem[0]) {
						// 			    // if (selectedItem[0]) {
						// 				// if (selectedItem[0].row) {
						// 					var weekNum 	 = selectedItem[0].row;
						// 					var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
						// 				// }else{
						// 				// 	var weekNum 	 = 0;
						// 				// 	var value = 0;
						// 				// }
						// 			    // }
						// 			}else{
						// 				var weekNum 	 = 0;
						// 				var value = 0;
						// 			}
						// 		    if (value>0) {
						// 				var form_values   = $("#dashboard-report-form").serializeArray();
						// 				var orgId  			= form_values[1]['value'];
						// 				var officeId      	= form_values[2]['value'];
						// 				var departmentId  	= form_values[3]['value'];

						// 				$.ajax({
						// 					type: "POST",
						// 					//dataType:"json",
						// 					url: "{{URL::to('admin/getHappyIndexDaysGraph')}}",
						// 					data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
						// 					success: function(response)
						// 					{
						// 						$('#happyIndexCate').html('');

						// 						var happyIndexCategory = '';

						// 						happyIndexCategory = '<div class="motCat">'+
						// 						'<ul>'+
						// 						'<li>'+
						// 						'<a onclick="getHappyIndexDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
						// 						'<span style="background-color: red;"></span>'+
						// 						'All</a>'+
						// 						'</li>';

						// 						var catArr = response.arrMain[0];

						// 						if (catArr) {
						// 							var c=0;
						// 							var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
						// 							//Loop Array
						// 							$.each(catArr,function(key,value){
						// 								if(c>0){
						// 						 			var columColor = catClr[c];
						// 						 			happyIndexCategory += '<li><a onclick="getHappyIndexDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
						// 						 		}
						// 						 		c++;
						// 							});
						// 						}
						// 						happyIndexCategory += '</ul></div>';
												
						// 						$('#happyIndexCate').append(happyIndexCategory);

						// 						$('#happyIndexWeekBackBtn').css('visibility','hidden');
						// 						$('#happyIndexDaysBackBtn').css('visibility','visible');

						// 						var data = google.visualization.arrayToDataTable(response.arrMain);
						// 					    var options = {
						// 							width: '100%',
						// 							height: '100%',
						// 							colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
						// 							hAxis: { title: '',  titleTextStyle: { color: '#333' } },
						// 							vAxis: {
						// 								minValue: 0,
						// 								viewWindow: {
						// 									// min: 0,
						// 									// max: 100
						// 								}
						// 							},
						// 							// curveType: 'function',
						// 							chartArea: {
						// 								top: 30,
						// 								left: 50,
						// 								// height: '50%',
						// 								width: '100%'
						// 							},
						// 							legend: 'none',
						// 							pointSize: 5
						// 						};
						// 						var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
						// 						chart.draw(data, options);
						// 					}
						// 				});	
						// 			}
						// 		}
						// 	}
						// });	
					}
				}
			}
		});	
	}
</script>

<script type="text/javascript">
	function getHappyIndexWeekCat(moodId=null,columnColor=null,monthNum) {

		var form_values   	= $("#dashboard-report-form").serializeArray();
		var orgId      		= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getHappyIndexWeekCat')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,moodId:moodId,columnColor:columnColor,monthNum:monthNum,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				console.log(response.columnColor);
				// return false
				if (response.columnColor==null) {
					var lineColors = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
				}else {
					var lineColors = [columnColor];
				}

				if (response.maxCount <= 0) {
					var maxCount = 100;
				}

				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					// colors: ['#ff454b'],
					colors: lineColors,
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							// min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
				};
				var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
				chart.draw(data, options);

				google.visualization.events.addListener(chart, 'select', selectHandler);

		       	function selectHandler() {

		       		var yearMonth = response.yearMonth;

					var selectedItem = chart.getSelection();

					if (selectedItem[0]) {
					    // if (selectedItem[0]) {
						// if (selectedItem[0].row) {
							var weekNum 	 = selectedItem[0].row;
							var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
						// }else{
						// 	var weekNum 	 = 0;
						// 	var value = 0;
						// }
					    // }
					}else{
						var weekNum 	 = 0;
						var value = 0;
					}
				    if (value>0) {
						var form_values   = $("#dashboard-report-form").serializeArray();
						var orgId  			= form_values[1]['value'];
						var officeId      	= form_values[2]['value'];
						var departmentId  	= form_values[3]['value'];

						//Happy index days graph
						getHappyIndexDaysGraph(orgId,officeId,departmentId,weekNum,yearMonth);

						//happy index reponders days graph
						getHappyIndexRespDaysGraph(orgId,officeId,departmentId,weekNum,yearMonth);

						// $.ajax({
						// 	type: "POST",
						// 	//dataType:"json",
						// 	url: "{{URL::to('admin/getHappyIndexDaysGraph')}}",
						// 	data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
						// 	success: function(response)
						// 	{
						// 		$('#happyIndexCate').html('');

						// 		var happyIndexCategory = '';

						// 		happyIndexCategory = '<div class="motCat">'+
						// 		'<ul>'+
						// 		'<li>'+
						// 		'<a onclick="getHappyIndexDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
						// 		'<span style="background-color: red;"></span>'+
						// 		'All</a>'+
						// 		'</li>';

						// 		var catArr = response.arrMain[0];

						// 		if (catArr) {
						// 			var c=0;
						// 			var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
						// 			//Loop Array
						// 			$.each(catArr,function(key,value){
						// 				if(c>0){
						// 		 			var columColor = catClr[c];
						// 		 			happyIndexCategory += '<li><a onclick="getHappyIndexDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
						// 		 		}
						// 		 		c++;
						// 			});
						// 		}
						// 		happyIndexCategory += '</ul></div>';
								
						// 		$('#happyIndexCate').append(happyIndexCategory);

						// 		$('#happyIndexWeekBackBtn').css('visibility','hidden');
						// 		$('#happyIndexDaysBackBtn').css('visibility','visible');

						// 		var data = google.visualization.arrayToDataTable(response.arrMain);
						// 	    var options = {
						// 			width: '100%',
						// 			height: '100%',
						// 			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
						// 			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
						// 			vAxis: {
						// 				minValue: 0,
						// 				viewWindow: {
						// 					// min: 0,
						// 					// max: 100
						// 				}
						// 			},
						// 			// curveType: 'function',
						// 			chartArea: {
						// 				top: 30,
						// 				left: 50,
						// 				// height: '50%',
						// 				width: '100%'
						// 			},
						// 			legend: 'none',
						// 			pointSize: 5
						// 		};
						// 		var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
						// 		chart.draw(data, options);
						// 	}
						// });	
					}
				}
			}
		});	
	}
</script>

<script type="text/javascript">
	function getHappyIndexDaysCat(moodId=null,columnColor=null,weekNum,yearMonth) {
		
		var form_values   	= $("#dashboard-report-form").serializeArray();
		var orgId      		= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getHappyIndexDaysGraph')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,moodId:moodId,columnColor:columnColor,weekNum:weekNum,yearMonth:yearMonth,index:2,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				console.log(response.columnColor);
				// return false
				if (response.columnColor==null) {
					var lineColors = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
				}else {
					var lineColors = [columnColor];
				}
				
				if (response.maxCount <= 0) {
					var maxCount = 100;
				}

				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					// colors: ['#ff454b'],
					colors: lineColors,
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							// min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
				};
				var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
				chart.draw(data, options);
			}
		});	
	}
</script>

<script type="text/javascript">
	function getHappyIndexMonthGraph(category,mainResponse,containerId) {

		$('#happyIndexCate').html('');

		var happyIndexCategory = '';

		happyIndexCategory = '<div class="motCat">'+
		'<ul>'+
		'<li>'+
		'<a onclick="getHappyIndexCat();">'+
		'<span style="background-color: red;"></span>'+
		'All</a>'+
		'</li>';

		var catArr = category;

		if (catArr) {
			var c=0;
			var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
			//Loop Array
			$.each(catArr,function(key,value){
				if(c>0){
		 			var columColor = catClr[c];
		 			happyIndexCategory += '<li><a onclick="getHappyIndexCat('+key+',\'' + columColor + '\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		 		}
		 		c++;
			});
		}
		happyIndexCategory += '</ul></div>';
		
		$('#happyIndexCate').append(happyIndexCategory);

		var data = google.visualization.arrayToDataTable(mainResponse);
		var options = {
			width: '100%',
			height: '100%',
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if($resultArray['happyIndexArray']['maxCount'] <= 0)
						max: 100
					@endif
					// min: 0,
					// max: 100
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				// height: '50%',
				width: '100%'
			},
			legend: 'none',
			pointSize: 5
			// legend: { position: 'bottom',
			// 	textStyle: {bold:true},
			// }
		};
		var chart = new google.visualization.LineChart(document.getElementById(containerId));
		chart.draw(data, options);

		google.visualization.events.addListener(chart, 'select', selectHandler);

       	function selectHandler() {

			var selectedItem = chart.getSelection();

			if (selectedItem[0]) {
			    // if (selectedItem[0]) {
				// if (selectedItem[0].row) {
					var monthNum 	 = selectedItem[0].row;
					var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
				// }else{
				// 	var monthNum 	 = 0;
				// 	var value = 0;
				// }
			    // }
			}else{
				var monthNum 	 = 0;
				var value = 0;
			}
			// console.log(monthNum);
			// console.log(value);
			// return false;
		    if (value>0) {
		    	console.log(monthNum);

		    	$('#monthNum').val(monthNum);
		    	$('#monthNumHI').val(monthNum);

				var form_values   = $("#dashboard-report-form").serializeArray();
				var orgId  			= form_values[1]['value'];
				var officeId      	= form_values[2]['value'];
				var departmentId  	= form_values[3]['value'];


				//Happy index week graph
				getHappyIndexWeekGraph(orgId,officeId,departmentId,monthNum);

				//Happy index responders week graph
				getHappyIndexRespWeekGraph(orgId,officeId,departmentId,monthNum);

				// $.ajax({
				// 	type: "POST",
				// 	//dataType:"json",
				// 	url: "{{URL::to('admin/getHappyIndexWeekGraph')}}",
				// 	data: {orgId:orgId,officeId:officeId,departmentId:departmentId,monthNum:monthNum,"_token":'<?php echo csrf_token()?>'},
				// 	success: function(response)
				// 	{
				// 		$('#happyIndexCate').html('');

				// 		var happyIndexCategory = '';

				// 		happyIndexCategory = '<div class="motCat">'+
				// 		'<ul>'+
				// 		'<li>'+
				// 		'<a onclick="getHappyIndexWeekCat(\'\',\'\',\''+monthNum+'\');">'+
				// 		'<span style="background-color: red;"></span>'+
				// 		'All</a>'+
				// 		'</li>';

				// 		var catArr = response.arrMain[0];

				// 		if (catArr) {
				// 			var c=0;
				// 			var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
				// 			//Loop Array
				// 			$.each(catArr,function(key,value){
				// 				if(c>0){
				// 		 			var columColor = catClr[c];
				// 		 			happyIndexCategory += '<li><a onclick="getHappyIndexWeekCat('+key+',\'' + columColor + '\',\''+monthNum+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
				// 		 		}
				// 		 		c++;
				// 			});
				// 		}
				// 		happyIndexCategory += '</ul></div>';
						
				// 		$('#happyIndexCate').append(happyIndexCategory);

				// 		$('#happyIndexWeekBackBtn').css('visibility','visible');

				// 		var data = google.visualization.arrayToDataTable(response.arrMain);
				// 	    var options = {
				// 			width: '100%',
				// 			height: '100%',
				// 			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
				// 			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
				// 			vAxis: {
				// 				minValue: 0,
				// 				viewWindow: {
				// 					// min: 0,
				// 					// max: 100
				// 				}
				// 			},
				// 			// curveType: 'function',
				// 			chartArea: {
				// 				top: 30,
				// 				left: 50,
				// 				// height: '50%',
				// 				width: '100%'
				// 			},
				// 			legend: 'none',
				// 			pointSize: 5
				// 		};
				// 		var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
				// 		chart.draw(data, options);

				// 		google.visualization.events.addListener(chart, 'select', selectHandler1);

				//        	function selectHandler1() {

				//        		var yearMonth = response.yearMonth;

				// 			var selectedItem = chart.getSelection();

				// 			if (selectedItem[0]) {
				// 			    // if (selectedItem[0]) {
				// 				// if (selectedItem[0].row) {
				// 					var weekNum 	 = selectedItem[0].row;
				// 					var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
				// 				// }else{
				// 				// 	var weekNum 	 = 0;
				// 				// 	var value = 0;
				// 				// }
				// 			    // }
				// 			}else{
				// 				var weekNum 	 = 0;
				// 				var value = 0;
				// 			}
				// 		    if (value>0) {
				// 				var form_values   = $("#dashboard-report-form").serializeArray();
				// 				var orgId  			= form_values[1]['value'];
				// 				var officeId      	= form_values[2]['value'];
				// 				var departmentId  	= form_values[3]['value'];

				// 				//happy index days graph
				// 				$.ajax({
				// 					type: "POST",
				// 					//dataType:"json",
				// 					url: "{{URL::to('admin/getHappyIndexDaysGraph')}}",
				// 					data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
				// 					success: function(response)
				// 					{
				// 						$('#happyIndexCate').html('');

				// 						var happyIndexCategory = '';

				// 						happyIndexCategory = '<div class="motCat">'+
				// 						'<ul>'+
				// 						'<li>'+
				// 						'<a onclick="getHappyIndexDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
				// 						'<span style="background-color: red;"></span>'+
				// 						'All</a>'+
				// 						'</li>';

				// 						var catArr = response.arrMain[0];

				// 						if (catArr) {
				// 							var c=0;
				// 							var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
				// 							//Loop Array
				// 							$.each(catArr,function(key,value){
				// 								if(c>0){
				// 						 			var columColor = catClr[c];
				// 						 			happyIndexCategory += '<li><a onclick="getHappyIndexDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
				// 						 		}
				// 						 		c++;
				// 							});
				// 						}
				// 						happyIndexCategory += '</ul></div>';
										
				// 						$('#happyIndexCate').append(happyIndexCategory);

				// 						$('#happyIndexWeekBackBtn').css('visibility','hidden');
				// 						$('#happyIndexDaysBackBtn').css('visibility','visible');

				// 						var data = google.visualization.arrayToDataTable(response.arrMain);
				// 					    var options = {
				// 							width: '100%',
				// 							height: '100%',
				// 							colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
				// 							hAxis: { title: '',  titleTextStyle: { color: '#333' } },
				// 							vAxis: {
				// 								minValue: 0,
				// 								viewWindow: {
				// 									// min: 0,
				// 									// max: 100
				// 								}
				// 							},
				// 							// curveType: 'function',
				// 							chartArea: {
				// 								top: 30,
				// 								left: 50,
				// 								// height: '50%',
				// 								width: '100%'
				// 							},
				// 							legend: 'none',
				// 							pointSize: 5
				// 						};
				// 						var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
				// 						chart.draw(data, options);
				// 					}
				// 				});	

				// 				//happy index reponders days graph
				// 				$.ajax({
				// 					type: "POST",
				// 					//dataType:"json",
				// 					url: "{{URL::to('admin/getHappyIndexRespDaysGraph')}}",
				// 					data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
				// 					success: function(response)
				// 					{
				// 						$('#happyIndexResCate').html('');

				// 						var happyIndexCategory = '';

				// 						happyIndexCategory = '<div class="motCat">'+
				// 						'<ul>'+
				// 						'<li>'+
				// 						'<a onclick="getHappyIndexRespDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
				// 						'<span style="background-color: red;"></span>'+
				// 						'All</a>'+
				// 						'</li>';

				// 						var catArr = response.arrMain[0];

				// 						if (catArr) {
				// 							var c=0;
				// 							var catClr = ['','#000','#eb1c24'];
				// 							//Loop Array
				// 							$.each(catArr,function(key,value){
				// 								if(c>0){
				// 						 			var columColor = catClr[c];
				// 						 			happyIndexCategory += '<li><a onclick="getHappyIndexRespDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
				// 						 		}
				// 						 		c++;
				// 							});
				// 						}
				// 						happyIndexCategory += '</ul></div>';
										
				// 						$('#happyIndexResCate').append(happyIndexCategory);

				// 						$('#happyIndexResWeekBackBtn').css('visibility','hidden');
				// 						$('#happyIndexResDaysBackBtn').css('visibility','visible');

				// 						var data = google.visualization.arrayToDataTable(response.arrMain);
				// 					    var options = {
				// 							width: '100%',
				// 							height: '100%',
				// 							colors: ['#000','#eb1c24'],
				// 							hAxis: { title: '',  titleTextStyle: { color: '#333' } },
				// 							vAxis: {
				// 								minValue: 0,
				// 								viewWindow: {
				// 									// min: 0,
				// 									// max: 100
				// 								}
				// 							},
				// 							// curveType: 'function',
				// 							chartArea: {
				// 								top: 30,
				// 								left: 50,
				// 								// height: '50%',
				// 								width: '100%'
				// 							},
				// 							legend: 'none',
				// 							pointSize: 5
				// 						};
				// 						var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
				// 						chart.draw(data, options);
				// 					}
				// 				});
				// 			}
				// 		}
				// 	}
				// });
				
				//Happy index responders week graph
				// getHappyIndexRespWeekGraph(orgId,officeId,departmentId,monthNum);
			}
		}
	}
</script>

<script type="text/javascript">
	function getHappyIndexWeekGraph(orgId,officeId,departmentId,monthNum) {
		$.ajax({
			type: "POST",
			//dataType:"json",
			url: "{{URL::to('admin/getHappyIndexWeekGraph')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,monthNum:monthNum,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				$('#happyIndexCate').html('');

				var happyIndexCategory = '';

				happyIndexCategory = '<div class="motCat">'+
				'<ul>'+
				'<li>'+
				'<a onclick="getHappyIndexWeekCat(\'\',\'\',\''+monthNum+'\');">'+
				'<span style="background-color: red;"></span>'+
				'All</a>'+
				'</li>';

				var catArr = response.arrMain[0];

				if (catArr) {
					var c=0;
					var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
					//Loop Array
					$.each(catArr,function(key,value){
						if(c>0){
				 			var columColor = catClr[c];
				 			happyIndexCategory += '<li><a onclick="getHappyIndexWeekCat('+key+',\'' + columColor + '\',\''+monthNum+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
				 		}
				 		c++;
					});
				}
				happyIndexCategory += '</ul></div>';
				
				$('#happyIndexCate').append(happyIndexCategory);

				$('#happyIndexDaysBackBtn').css('visibility','hidden');
				$('#happyIndexWeekBackBtn').css('visibility','visible');

				if (response.maxCount <= 0) {
					var maxCount = 100;
				}

				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							// min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
				};
				var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
				chart.draw(data, options);

				google.visualization.events.addListener(chart, 'select', selectHandler1);

		       	function selectHandler1() {

		       		var yearMonth = response.yearMonth;

					var selectedItem = chart.getSelection();

					if (selectedItem[0]) {
					    // if (selectedItem[0]) {
						// if (selectedItem[0].row) {
							var weekNum 	 = selectedItem[0].row;
							var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
						// }else{
						// 	var weekNum 	 = 0;
						// 	var value = 0;
						// }
					    // }
					}else{
						var weekNum 	 = 0;
						var value = 0;
					}
				    if (value>0) {
						var form_values   = $("#dashboard-report-form").serializeArray();
						var orgId  			= form_values[1]['value'];
						var officeId      	= form_values[2]['value'];
						var departmentId  	= form_values[3]['value'];

						//Happy index days graph
						getHappyIndexDaysGraph(orgId,officeId,departmentId,weekNum,yearMonth);

						//happy index reponders days graph
						getHappyIndexRespDaysGraph(orgId,officeId,departmentId,weekNum,yearMonth);

						// $.ajax({
						// 	type: "POST",
						// 	//dataType:"json",
						// 	url: "{{URL::to('admin/getHappyIndexRespDaysGraph')}}",
						// 	data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
						// 	success: function(response)
						// 	{
						// 		$('#happyIndexResCate').html('');

						// 		var happyIndexCategory = '';

						// 		happyIndexCategory = '<div class="motCat">'+
						// 		'<ul>'+
						// 		'<li>'+
						// 		'<a onclick="getHappyIndexRespDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
						// 		'<span style="background-color: red;"></span>'+
						// 		'All</a>'+
						// 		'</li>';

						// 		var catArr = response.arrMain[0];

						// 		if (catArr) {
						// 			var c=0;
						// 			var catClr = ['','#000','#eb1c24'];
						// 			//Loop Array
						// 			$.each(catArr,function(key,value){
						// 				if(c>0){
						// 		 			var columColor = catClr[c];
						// 		 			happyIndexCategory += '<li><a onclick="getHappyIndexRespDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
						// 		 		}
						// 		 		c++;
						// 			});
						// 		}
						// 		happyIndexCategory += '</ul></div>';
								
						// 		$('#happyIndexResCate').append(happyIndexCategory);

						// 		$('#happyIndexResWeekBackBtn').css('visibility','hidden');
						// 		$('#happyIndexResDaysBackBtn').css('visibility','visible');

						// 		var data = google.visualization.arrayToDataTable(response.arrMain);
						// 	    var options = {
						// 			width: '100%',
						// 			height: '100%',
						// 			colors: ['#000','#eb1c24'],
						// 			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
						// 			vAxis: {
						// 				minValue: 0,
						// 				viewWindow: {
						// 					// min: 0,
						// 					// max: 100
						// 				}
						// 			},
						// 			// curveType: 'function',
						// 			chartArea: {
						// 				top: 30,
						// 				left: 50,
						// 				// height: '50%',
						// 				width: '100%'
						// 			},
						// 			legend: 'none',
						// 			pointSize: 5
						// 		};
						// 		var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
						// 		chart.draw(data, options);
						// 	}
						// });
					}
				}
			}
		});
	}
</script>

<script type="text/javascript">
	function getHappyIndexDaysGraph(orgId,officeId,departmentId,weekNum,yearMonth) {
		$.ajax({
			type: "POST",
			//dataType:"json",
			url: "{{URL::to('admin/getHappyIndexDaysGraph')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				$('#happyIndexCate').html('');

				var happyIndexCategory = '';

				happyIndexCategory = '<div class="motCat">'+
				'<ul>'+
				'<li>'+
				'<a onclick="getHappyIndexDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
				'<span style="background-color: red;"></span>'+
				'All</a>'+
				'</li>';

				var catArr = response.arrMain[0];

				if (catArr) {
					var c=0;
					var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
					//Loop Array
					$.each(catArr,function(key,value){
						if(c>0){
				 			var columColor = catClr[c];
				 			happyIndexCategory += '<li><a onclick="getHappyIndexDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
				 		}
				 		c++;
					});
				}
				happyIndexCategory += '</ul></div>';
				
				$('#happyIndexCate').append(happyIndexCategory);

				$('#happyIndexWeekBackBtn').css('visibility','hidden');
				$('#happyIndexDaysBackBtn').css('visibility','visible');

				if (response.maxCount <= 0) {
					var maxCount = 100;
				}

				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							// min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
				};
				var chart = new google.visualization.LineChart(document.getElementById('happyIndex'));
				chart.draw(data, options);
			}
		});	
	}
</script>

<script type="text/javascript">
	function drawHappyIndexResponders(containerId) {

		//Happy index responders month graph
		getHappyIndexRespMonthGraph(<?php echo json_encode($resultArray['happyIndexResArray']['arrMain'][0]);?>,<?php echo json_encode($resultArray['happyIndexResArray']['arrMain']);?>,containerId);

		// var happyIndexCategory = '';
		// happyIndexCategory = '<div class="motCat">'+
		// '<ul>'+
		// '<li>'+
		// '<a onclick="getHappyIndexResCat();">'+
		// '<span style="background-color: red;"></span>'+
		// 'All</a>'+
		// '</li>';

		// var catArr = <?php //echo json_encode($resultArray['happyIndexResArray']['arrMain'][0]);?>;
		// if (catArr) {
		// 	var c=0;
		// 	var catClr = ['','#000','#eb1c24'];
		// 	//Loop Array
		// 	$.each(catArr,function(key,value){
		// 		if(c>0){
		//  			var columColor = catClr[c];
		//  			happyIndexCategory += '<li><a onclick="getHappyIndexResCat('+key+',\'' + columColor + '\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		//  		}
		//  		c++;
		// 	});
		// }
		// happyIndexCategory += '</ul></div>';
		
		// $('#happyIndexResCate').append(happyIndexCategory);

		// var data = google.visualization.arrayToDataTable(<?php //echo json_encode($resultArray['happyIndexResArray']['arrMain']);?>);
		// var options = {
		// 	width: '100%',
		// 	height: '100%',
		// 	colors: ['#000','#eb1c24'],
		// 	hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 	vAxis: {
		// 		minValue: 0,
		// 		viewWindow: {
		// 			@if($resultArray['happyIndexResArray']['maxCount'] <= 0)
		// 				max: 100
		// 			@endif
		// 			// min: 0,
		// 			// max: 100
		// 		}
		// 	},
		// 	// curveType: 'function',
		// 	chartArea: {
		// 		top: 30,
		// 		left: 50,
		// 		// height: '50%',
		// 		width: '100%'
		// 	},
		// 	legend: 'none',
		// 	pointSize: 5
		// };
		// var chart = new google.visualization.LineChart(document.getElementById(containerId));
		// chart.draw(data, options);

		// google.visualization.events.addListener(chart, 'select', selectHandler);

  //      	function selectHandler() {
		// 	var selectedItem = chart.getSelection();
		// 	if (selectedItem[0]) {
		// 		var monthNum 	 = selectedItem[0].row;
		// 		var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
		// 	}else{
		// 		var monthNum 	 = 0;
		// 		var value = 0;
		// 	}
		// 	// console.log(monthNum);
		// 	// console.log(value);
		// 	// return false;
		//     if (value>0) {
		//     	$('#monthNumHI').val(monthNum);

		// 		var form_values   = $("#dashboard-report-form").serializeArray();
		// 		var orgId  			= form_values[1]['value'];
		// 		var officeId      	= form_values[2]['value'];
		// 		var departmentId  	= form_values[3]['value'];

		// 		//Happy index responders week graph
		// 		getHappyIndexRespWeekGraph(orgId,officeId,departmentId,monthNum);

		// 		// $.ajax({
		// 		// 	type: "POST",
		// 		// 	//dataType:"json",
		// 		// 	url: "{{URL::to('admin/getHappyIndexRespWeekGraph')}}",
		// 		// 	data: {orgId:orgId,officeId:officeId,departmentId:departmentId,monthNum:monthNum,"_token":'<?php echo csrf_token()?>'},
		// 		// 	success: function(response)
		// 		// 	{
		// 		// 		$('#happyIndexResCate').html('');

		// 		// 		var happyIndexCategory = '';

		// 		// 		happyIndexCategory = '<div class="motCat">'+
		// 		// 		'<ul>'+
		// 		// 		'<li>'+
		// 		// 		'<a onclick="getHappyIndexRespWeekCat(\'\',\'\',\''+monthNum+'\');">'+
		// 		// 		'<span style="background-color: red;"></span>'+
		// 		// 		'All</a>'+
		// 		// 		'</li>';

		// 		// 		var catArr = response.arrMain[0];

		// 		// 		if (catArr) {
		// 		// 			var c=0;
		// 		// 			var catClr = ['','#000','#eb1c24'];
		// 		// 			//Loop Array
		// 		// 			$.each(catArr,function(key,value){
		// 		// 				if(c>0){
		// 		// 		 			var columColor = catClr[c];
		// 		// 		 			happyIndexCategory += '<li><a onclick="getHappyIndexRespWeekCat('+key+',\'' + columColor + '\',\''+monthNum+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		// 		// 		 		}
		// 		// 		 		c++;
		// 		// 			});
		// 		// 		}
		// 		// 		happyIndexCategory += '</ul></div>';
						
		// 		// 		$('#happyIndexResCate').append(happyIndexCategory);

		// 		// 		$('#happyIndexResWeekBackBtn').css('visibility','visible');

		// 		// 		var data = google.visualization.arrayToDataTable(response.arrMain);
		// 		// 	    var options = {
		// 		// 			width: '100%',
		// 		// 			height: '100%',
		// 		// 			colors: ['#000','#eb1c24'],
		// 		// 			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 		// 			vAxis: {
		// 		// 				minValue: 0,
		// 		// 				viewWindow: {
		// 		// 					// min: 0,
		// 		// 					// max: 100
		// 		// 				}
		// 		// 			},
		// 		// 			// curveType: 'function',
		// 		// 			chartArea: {
		// 		// 				top: 30,
		// 		// 				left: 50,
		// 		// 				// height: '50%',
		// 		// 				width: '100%'
		// 		// 			},
		// 		// 			legend: 'none',
		// 		// 			pointSize: 5
		// 		// 		};
		// 		// 		var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
		// 		// 		chart.draw(data, options);

		// 		// 		google.visualization.events.addListener(chart, 'select', selectHandler1);

		// 		//        	function selectHandler1() {
		// 		//        		var yearMonth = response.yearMonth;
		// 		// 			var selectedItem = chart.getSelection();
		// 		// 			if (selectedItem[0]) {
		// 		// 				var weekNum 	 = selectedItem[0].row;
		// 		// 				var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
		// 		// 			}else{
		// 		// 				var weekNum	= 0;
		// 		// 				var value = 0;
		// 		// 			}
		// 		// 		    if (value>0) {
		// 		// 				var form_values   = $("#dashboard-report-form").serializeArray();
		// 		// 				var orgId  			= form_values[1]['value'];
		// 		// 				var officeId      	= form_values[2]['value'];
		// 		// 				var departmentId  	= form_values[3]['value'];

		// 		// 				$.ajax({
		// 		// 					type: "POST",
		// 		// 					//dataType:"json",
		// 		// 					url: "{{URL::to('admin/getHappyIndexRespDaysGraph')}}",
		// 		// 					data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
		// 		// 					success: function(response)
		// 		// 					{
		// 		// 						$('#happyIndexResCate').html('');

		// 		// 						var happyIndexCategory = '';

		// 		// 						happyIndexCategory = '<div class="motCat">'+
		// 		// 						'<ul>'+
		// 		// 						'<li>'+
		// 		// 						'<a onclick="getHappyIndexRespDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
		// 		// 						'<span style="background-color: red;"></span>'+
		// 		// 						'All</a>'+
		// 		// 						'</li>';

		// 		// 						var catArr = response.arrMain[0];

		// 		// 						if (catArr) {
		// 		// 							var c=0;
		// 		// 							var catClr = ['','#000','#eb1c24'];
		// 		// 							//Loop Array
		// 		// 							$.each(catArr,function(key,value){
		// 		// 								if(c>0){
		// 		// 						 			var columColor = catClr[c];
		// 		// 						 			happyIndexCategory += '<li><a onclick="getHappyIndexRespDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		// 		// 						 		}
		// 		// 						 		c++;
		// 		// 							});
		// 		// 						}
		// 		// 						happyIndexCategory += '</ul></div>';
										
		// 		// 						$('#happyIndexResCate').append(happyIndexCategory);

		// 		// 						$('#happyIndexResWeekBackBtn').css('visibility','hidden');
		// 		// 						$('#happyIndexResDaysBackBtn').css('visibility','visible');

		// 		// 						var data = google.visualization.arrayToDataTable(response.arrMain);
		// 		// 					    var options = {
		// 		// 							width: '100%',
		// 		// 							height: '100%',
		// 		// 							colors: ['#000','#eb1c24'],
		// 		// 							hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 		// 							vAxis: {
		// 		// 								minValue: 0,
		// 		// 								viewWindow: {
		// 		// 									// min: 0,
		// 		// 									// max: 100
		// 		// 								}
		// 		// 							},
		// 		// 							// curveType: 'function',
		// 		// 							chartArea: {
		// 		// 								top: 30,
		// 		// 								left: 50,
		// 		// 								// height: '50%',
		// 		// 								width: '100%'
		// 		// 							},
		// 		// 							legend: 'none',
		// 		// 							pointSize: 5
		// 		// 						};
		// 		// 						var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
		// 		// 						chart.draw(data, options);
		// 		// 					}
		// 		// 				});	
		// 		// 			}
		// 		// 		}
		// 		// 	}
		// 		// });	
		// 	}
		// }
	}
</script>

<script type="text/javascript">
	$('#happyIndexResWeekBackBtn').on('click',function(){
		$('#happyIndexResWeekBackBtn').css('visibility','hidden');
		$('#happyIndexWeekBackBtn').css('visibility','hidden');

		//Happy index responders month graph
		getHappyIndexRespMonthGraph(<?php echo json_encode($resultArray['happyIndexResArray']['arrMain'][0]);?>,<?php echo json_encode($resultArray['happyIndexResArray']['arrMain']);?>,'happyIndexResponders');

		//Happy index month graph
		getHappyIndexMonthGraph(<?php echo json_encode($resultArray['happyIndexArray']['arrMain'][0]);?>,<?php echo json_encode($resultArray['happyIndexArray']['arrMain']);?>,'happyIndex');

		// $('#happyIndexResCate').html('');

		// var happyIndexCategory = '';
		// happyIndexCategory = '<div class="motCat">'+
		// '<ul>'+
		// '<li>'+
		// '<a onclick="getHappyIndexResCat();">'+
		// '<span style="background-color: red;"></span>'+
		// 'All</a>'+
		// '</li>';

		// var catArr = <?php //echo json_encode($resultArray['happyIndexResArray']['arrMain'][0]);?>;
		// if (catArr) {
		// 	var c=0;
		// 	var catClr = ['','#000','#eb1c24'];
		// 	//Loop Array
		// 	$.each(catArr,function(key,value){
		// 		if(c>0){
		//  			var columColor = catClr[c];
		//  			happyIndexCategory += '<li><a onclick="getHappyIndexResCat('+key+',\'' + columColor + '\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		//  		}
		//  		c++;
		// 	});
		// }
		// happyIndexCategory += '</ul></div>';
		
		// $('#happyIndexResCate').append(happyIndexCategory);

		// var data = google.visualization.arrayToDataTable(<?php //echo json_encode($resultArray['happyIndexResArray']['arrMain']);?>);
		// var options = {
		// 	width: '100%',
		// 	height: '100%',
		// 	colors: ['#000','#eb1c24'],
		// 	hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 	vAxis: {
		// 		minValue: 0,
		// 		viewWindow: {
		// 			@if($resultArray['happyIndexResArray']['maxCount'] <= 0)
		// 				max: 100
		// 			@endif
		// 			// min: 0,
		// 			// max: 100
		// 		}
		// 	},
		// 	// curveType: 'function',
		// 	chartArea: {
		// 		top: 30,
		// 		left: 50,
		// 		// height: '50%',
		// 		width: '100%'
		// 	},
		// 	legend: 'none',
		// 	pointSize: 5
		// };
		// var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
		// chart.draw(data, options);

		// google.visualization.events.addListener(chart, 'select', selectHandler);

  //      	function selectHandler() {
		// 	var selectedItem = chart.getSelection();
		// 	if (selectedItem[0]) {
		// 		var monthNum 	 = selectedItem[0].row;
		// 		var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
		// 	}else{
		// 		var monthNum 	 = 0;
		// 		var value = 0;
		// 	}
		//     if (value>0) {
		//     	$('#monthNumHI').val(monthNum);

		// 		var form_values   = $("#dashboard-report-form").serializeArray();
		// 		var orgId  			= form_values[1]['value'];
		// 		var officeId      	= form_values[2]['value'];
		// 		var departmentId  	= form_values[3]['value'];

		// 		//Happy index responders week graph
		// 		getHappyIndexRespWeekGraph(orgId,officeId,departmentId,monthNum);

		// 		// $.ajax({
		// 		// 	type: "POST",
		// 		// 	//dataType:"json",
		// 		// 	url: "{{URL::to('admin/getHappyIndexRespWeekGraph')}}",
		// 		// 	data: {orgId:orgId,officeId:officeId,departmentId:departmentId,monthNum:monthNum,"_token":'<?php echo csrf_token()?>'},
		// 		// 	success: function(response)
		// 		// 	{
		// 		// 		$('#happyIndexResCate').html('');

		// 		// 		var happyIndexCategory = '';

		// 		// 		happyIndexCategory = '<div class="motCat">'+
		// 		// 		'<ul>'+
		// 		// 		'<li>'+
		// 		// 		'<a onclick="getHappyIndexRespWeekCat(\'\',\'\',\''+monthNum+'\');">'+
		// 		// 		'<span style="background-color: red;"></span>'+
		// 		// 		'All</a>'+
		// 		// 		'</li>';

		// 		// 		var catArr = response.arrMain[0];

		// 		// 		if (catArr) {
		// 		// 			var c=0;
		// 		// 			var catClr = ['','#000','#eb1c24'];
		// 		// 			//Loop Array
		// 		// 			$.each(catArr,function(key,value){
		// 		// 				if(c>0){
		// 		// 		 			var columColor = catClr[c];
		// 		// 		 			happyIndexCategory += '<li><a onclick="getHappyIndexRespWeekCat('+key+',\'' + columColor + '\',\''+monthNum+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		// 		// 		 		}
		// 		// 		 		c++;
		// 		// 			});
		// 		// 		}
		// 		// 		happyIndexCategory += '</ul></div>';
						
		// 		// 		$('#happyIndexResCate').append(happyIndexCategory);

		// 		// 		$('#happyIndexResWeekBackBtn').css('visibility','visible');

		// 		// 		var data = google.visualization.arrayToDataTable(response.arrMain);
		// 		// 	    var options = {
		// 		// 			width: '100%',
		// 		// 			height: '100%',
		// 		// 			colors: ['#000','#eb1c24'],
		// 		// 			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 		// 			vAxis: {
		// 		// 				minValue: 0,
		// 		// 				viewWindow: {
		// 		// 					// min: 0,
		// 		// 					// max: 100
		// 		// 				}
		// 		// 			},
		// 		// 			// curveType: 'function',
		// 		// 			chartArea: {
		// 		// 				top: 30,
		// 		// 				left: 50,
		// 		// 				// height: '50%',
		// 		// 				width: '100%'
		// 		// 			},
		// 		// 			legend: 'none',
		// 		// 			pointSize: 5
		// 		// 		};
		// 		// 		var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
		// 		// 		chart.draw(data, options);

		// 		// 		google.visualization.events.addListener(chart, 'select', selectHandler1);

		// 		//        	function selectHandler1() {
		// 		//        		var yearMonth = response.yearMonth;
		// 		// 			var selectedItem = chart.getSelection();
		// 		// 			if (selectedItem[0]) {
		// 		// 				var weekNum 	 = selectedItem[0].row;
		// 		// 				var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
		// 		// 			}else{
		// 		// 				var weekNum	= 0;
		// 		// 				var value = 0;
		// 		// 			}
		// 		// 		    if (value>0) {
		// 		// 				var form_values   = $("#dashboard-report-form").serializeArray();
		// 		// 				var orgId  			= form_values[1]['value'];
		// 		// 				var officeId      	= form_values[2]['value'];
		// 		// 				var departmentId  	= form_values[3]['value'];

		// 		// 				$.ajax({
		// 		// 					type: "POST",
		// 		// 					//dataType:"json",
		// 		// 					url: "{{URL::to('admin/getHappyIndexRespDaysGraph')}}",
		// 		// 					data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
		// 		// 					success: function(response)
		// 		// 					{
		// 		// 						$('#happyIndexResCate').html('');

		// 		// 						var happyIndexCategory = '';

		// 		// 						happyIndexCategory = '<div class="motCat">'+
		// 		// 						'<ul>'+
		// 		// 						'<li>'+
		// 		// 						'<a onclick="getHappyIndexRespDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
		// 		// 						'<span style="background-color: red;"></span>'+
		// 		// 						'All</a>'+
		// 		// 						'</li>';

		// 		// 						var catArr = response.arrMain[0];

		// 		// 						if (catArr) {
		// 		// 							var c=0;
		// 		// 							var catClr = ['','#000','#eb1c24'];
		// 		// 							//Loop Array
		// 		// 							$.each(catArr,function(key,value){
		// 		// 								if(c>0){
		// 		// 						 			var columColor = catClr[c];
		// 		// 						 			happyIndexCategory += '<li><a onclick="getHappyIndexRespDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		// 		// 						 		}
		// 		// 						 		c++;
		// 		// 							});
		// 		// 						}
		// 		// 						happyIndexCategory += '</ul></div>';
										
		// 		// 						$('#happyIndexResCate').append(happyIndexCategory);

		// 		// 						$('#happyIndexResWeekBackBtn').css('visibility','hidden');
		// 		// 						$('#happyIndexResDaysBackBtn').css('visibility','visible');

		// 		// 						var data = google.visualization.arrayToDataTable(response.arrMain);
		// 		// 					    var options = {
		// 		// 							width: '100%',
		// 		// 							height: '100%',
		// 		// 							colors: ['#000','#eb1c24'],
		// 		// 							hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 		// 							vAxis: {
		// 		// 								minValue: 0,
		// 		// 								viewWindow: {
		// 		// 									// min: 0,
		// 		// 									// max: 100
		// 		// 								}
		// 		// 							},
		// 		// 							// curveType: 'function',
		// 		// 							chartArea: {
		// 		// 								top: 30,
		// 		// 								left: 50,
		// 		// 								// height: '50%',
		// 		// 								width: '100%'
		// 		// 							},
		// 		// 							legend: 'none',
		// 		// 							pointSize: 5
		// 		// 						};
		// 		// 						var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
		// 		// 						chart.draw(data, options);
		// 		// 					}
		// 		// 				});	
		// 		// 			}
		// 		// 		}
		// 		// 	}
		// 		// });	
		// 	}
		// }
	});
</script>

<script type="text/javascript">
	$('#happyIndexResDaysBackBtn').on('click',function(){

		var monthNum = $('#monthNumHI').val();

		var form_values   = $("#dashboard-report-form").serializeArray();
		var orgId  			= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		//Happy index responders week graph
		getHappyIndexRespWeekGraph(orgId,officeId,departmentId,monthNum);

		//Happy index week graph
		getHappyIndexWeekGraph(orgId,officeId,departmentId,monthNum);

		// $.ajax({
		// 	type: "POST",
		// 	//dataType:"json",
		// 	url: "{{URL::to('admin/getHappyIndexRespWeekGraph')}}",
		// 	data: {orgId:orgId,officeId:officeId,departmentId:departmentId,monthNum:monthNum,"_token":'<?php echo csrf_token()?>'},
		// 	success: function(response)
		// 	{
		// 		$('#happyIndexResCate').html('');

		// 		var happyIndexCategory = '';

		// 		happyIndexCategory = '<div class="motCat">'+
		// 		'<ul>'+
		// 		'<li>'+
		// 		'<a onclick="getHappyIndexRespWeekCat(\'\',\'\',\''+monthNum+'\');">'+
		// 		'<span style="background-color: red;"></span>'+
		// 		'All</a>'+
		// 		'</li>';

		// 		var catArr = response.arrMain[0];

		// 		if (catArr) {
		// 			var c=0;
		// 			var catClr = ['','#000','#eb1c24'];
		// 			//Loop Array
		// 			$.each(catArr,function(key,value){
		// 				if(c>0){
		// 		 			var columColor = catClr[c];
		// 		 			happyIndexCategory += '<li><a onclick="getHappyIndexRespWeekCat('+key+',\'' + columColor + '\',\''+monthNum+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		// 		 		}
		// 		 		c++;
		// 			});
		// 		}
		// 		happyIndexCategory += '</ul></div>';
				
		// 		$('#happyIndexResCate').append(happyIndexCategory);

		// 		$('#happyIndexResDaysBackBtn').css('visibility','hidden');
		// 		$('#happyIndexResWeekBackBtn').css('visibility','visible');

		// 		var data = google.visualization.arrayToDataTable(response.arrMain);
		// 	    var options = {
		// 			width: '100%',
		// 			height: '100%',
		// 			colors: ['#000','#eb1c24'],
		// 			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 			vAxis: {
		// 				minValue: 0,
		// 				viewWindow: {
		// 					// min: 0,
		// 					// max: 100
		// 				}
		// 			},
		// 			// curveType: 'function',
		// 			chartArea: {
		// 				top: 30,
		// 				left: 50,
		// 				// height: '50%',
		// 				width: '100%'
		// 			},
		// 			legend: 'none',
		// 			pointSize: 5
		// 		};
		// 		var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
		// 		chart.draw(data, options);

		// 		google.visualization.events.addListener(chart, 'select', selectHandler1);

		//        	function selectHandler1() {

		//        		var yearMonth = response.yearMonth;

		// 			var selectedItem = chart.getSelection();

		// 			if (selectedItem[0]) {
		// 			    // if (selectedItem[0]) {
		// 				// if (selectedItem[0].row) {
		// 					var weekNum 	 = selectedItem[0].row;
		// 					var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
		// 				// }else{
		// 				// 	var weekNum 	 = 0;
		// 				// 	var value = 0;
		// 				// }
		// 			    // }
		// 			}else{
		// 				var weekNum 	 = 0;
		// 				var value = 0;
		// 			}
		// 		    if (value>0) {
		// 				var form_values   = $("#dashboard-report-form").serializeArray();
		// 				var orgId  			= form_values[1]['value'];
		// 				var officeId      	= form_values[2]['value'];
		// 				var departmentId  	= form_values[3]['value'];

		// 				$.ajax({
		// 					type: "POST",
		// 					//dataType:"json",
		// 					url: "{{URL::to('admin/getHappyIndexRespDaysGraph')}}",
		// 					data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
		// 					success: function(response)
		// 					{
		// 						$('#happyIndexResCate').html('');

		// 						var happyIndexCategory = '';

		// 						happyIndexCategory = '<div class="motCat">'+
		// 						'<ul>'+
		// 						'<li>'+
		// 						'<a onclick="getHappyIndexRespDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
		// 						'<span style="background-color: red;"></span>'+
		// 						'All</a>'+
		// 						'</li>';

		// 						var catArr = response.arrMain[0];

		// 						if (catArr) {
		// 							var c=0;
		// 							var catClr = ['','#000','#eb1c24'];
		// 							//Loop Array
		// 							$.each(catArr,function(key,value){
		// 								if(c>0){
		// 						 			var columColor = catClr[c];
		// 						 			happyIndexCategory += '<li><a onclick="getHappyIndexRespDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		// 						 		}
		// 						 		c++;
		// 							});
		// 						}
		// 						happyIndexCategory += '</ul></div>';
								
		// 						$('#happyIndexResCate').append(happyIndexCategory);

		// 						$('#happyIndexResWeekBackBtn').css('visibility','hidden');
		// 						$('#happyIndexResDaysBackBtn').css('visibility','visible');

		// 						var data = google.visualization.arrayToDataTable(response.arrMain);
		// 					    var options = {
		// 							width: '100%',
		// 							height: '100%',
		// 							colors: ['#000','#eb1c24'],
		// 							hAxis: { title: '',  titleTextStyle: { color: '#333' } },
		// 							vAxis: {
		// 								minValue: 0,
		// 								viewWindow: {
		// 									// min: 0,
		// 									// max: 100
		// 								}
		// 							},
		// 							// curveType: 'function',
		// 							chartArea: {
		// 								top: 30,
		// 								left: 50,
		// 								// height: '50%',
		// 								width: '100%'
		// 							},
		// 							legend: 'none',
		// 							pointSize: 5
		// 						};
		// 						var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
		// 						chart.draw(data, options);
		// 					}
		// 				});	
		// 			}
		// 		}
		// 	}
		// });	
	});
</script>

<script type="text/javascript">
	function getHappyIndexResCat(moodId=null,columnColor=null) {

		var form_values   	= $("#dashboard-report-form").serializeArray();

		var orgId      		= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getHappyIndexResCat')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,moodId:moodId,columnColor:columnColor,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				if (response.columnColor==null) {
					var lineColors = ['#000','#eb1c24'];
				}else {
					var lineColors = [columnColor];
				}
				if (response.maxCount <= 0) {
					var maxCount = 100;
				}
				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					// colors: ['#ff454b'],
					colors: lineColors,
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							// min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
				};
				var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
				chart.draw(data, options);

				google.visualization.events.addListener(chart, 'select', selectHandler);

		       	function selectHandler() {

					var selectedItem = chart.getSelection();
					if (selectedItem[0]) {
						if (selectedItem[0].row) {
							var monthNum = selectedItem[0].row;
							var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
						}else{
							var monthNum = 0;
							var value = 0;
						}
					}else{
						var monthNum = 0;
						var value = 0;
					}
				    if (value>0) {
				    	console.log(monthNum);
				    	$('#monthNumHI').val(monthNum);
				    	$('#monthNum').val(monthNum);

						var form_values   	= $("#dashboard-report-form").serializeArray();
						var orgId  			= form_values[1]['value'];
						var officeId      	= form_values[2]['value'];
						var departmentId  	= form_values[3]['value'];

						//Happy index responders week graph
						getHappyIndexRespWeekGraph(orgId,officeId,departmentId,monthNum);

						//Happy index week graph
						getHappyIndexWeekGraph(orgId,officeId,departmentId,monthNum);

						// $.ajax({
						// 	type: "POST",
						// 	//dataType:"json",
						// 	url: "{{URL::to('admin/getHappyIndexRespWeekGraph')}}",
						// 	data: {orgId:orgId,officeId:officeId,departmentId:departmentId,monthNum:monthNum,"_token":'<?php echo csrf_token()?>'},
						// 	success: function(response)
						// 	{
						// 		$('#happyIndexResCate').html('');

						// 		var happyIndexCategory = '';

						// 		happyIndexCategory = '<div class="motCat">'+
						// 		'<ul>'+
						// 		'<li>'+
						// 		'<a onclick="getHappyIndexRespWeekCat(\'\',\'\',\''+monthNum+'\');">'+
						// 		'<span style="background-color: red;"></span>'+
						// 		'All</a>'+
						// 		'</li>';

						// 		var catArr = response.arrMain[0];

						// 		if (catArr) {
						// 			var c=0;
						// 			var catClr = ['','#000','#eb1c24'];
						// 			//Loop Array
						// 			$.each(catArr,function(key,value){
						// 				if(c>0){
						// 		 			var columColor = catClr[c];
						// 		 			happyIndexCategory += '<li><a onclick="getHappyIndexRespWeekCat('+key+',\'' + columColor + '\',\''+monthNum+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
						// 		 		}
						// 		 		c++;
						// 			});
						// 		}
						// 		happyIndexCategory += '</ul></div>';
								
						// 		$('#happyIndexResCate').append(happyIndexCategory);

						// 		$('#happyIndexResWeekBackBtn').css('visibility','visible');

						// 		var data = google.visualization.arrayToDataTable(response.arrMain);
						// 	    var options = {
						// 			width: '100%',
						// 			height: '100%',
						// 			colors: ['#000','#eb1c24'],
						// 			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
						// 			vAxis: {
						// 				minValue: 0,
						// 				viewWindow: {
						// 					// min: 0,
						// 					// max: 100
						// 				}
						// 			},
						// 			// curveType: 'function',
						// 			chartArea: {
						// 				top: 30,
						// 				left: 50,
						// 				// height: '50%',
						// 				width: '100%'
						// 			},
						// 			legend: 'none',
						// 			pointSize: 5
						// 		};
						// 		var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
						// 		chart.draw(data, options);

						// 		google.visualization.events.addListener(chart, 'select', selectHandler1);

						//        	function selectHandler1() {

						//        		var yearMonth = response.yearMonth;

						// 			var selectedItem = chart.getSelection();
						// 			if (selectedItem[0]) {
						// 			    // if (selectedItem[0]) {
						// 				// if (selectedItem[0].row) {
						// 					var weekNum 	 = selectedItem[0].row;
						// 					var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
						// 				// }else{
						// 				// 	var weekNum 	 = 0;
						// 				// 	var value = 0;
						// 				// }
						// 			    // }
						// 			}else{
						// 				var weekNum 	 = 0;
						// 				var value = 0;
						// 			}
						// 		    if (value>0) {
						// 				var form_values   = $("#dashboard-report-form").serializeArray();
						// 				var orgId  			= form_values[1]['value'];
						// 				var officeId      	= form_values[2]['value'];
						// 				var departmentId  	= form_values[3]['value'];

						// 				$.ajax({
						// 					type: "POST",
						// 					//dataType:"json",
						// 					url: "{{URL::to('admin/getHappyIndexRespDaysGraph')}}",
						// 					data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
						// 					success: function(response)
						// 					{
						// 						$('#happyIndexResCate').html('');

						// 						var happyIndexCategory = '';

						// 						happyIndexCategory = '<div class="motCat">'+
						// 						'<ul>'+
						// 						'<li>'+
						// 						'<a onclick="getHappyIndexRespDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
						// 						'<span style="background-color: red;"></span>'+
						// 						'All</a>'+
						// 						'</li>';

						// 						var catArr = response.arrMain[0];

						// 						if (catArr) {
						// 							var c=0;
						// 							var catClr = ['','#000','#eb1c24'];
						// 							//Loop Array
						// 							$.each(catArr,function(key,value){
						// 								if(c>0){
						// 						 			var columColor = catClr[c];
						// 						 			happyIndexCategory += '<li><a onclick="getHappyIndexRespDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
						// 						 		}
						// 						 		c++;
						// 							});
						// 						}
						// 						happyIndexCategory += '</ul></div>';
												
						// 						$('#happyIndexResCate').append(happyIndexCategory);

						// 						$('#happyIndexResWeekBackBtn').css('visibility','hidden');
						// 						$('#happyIndexResDaysBackBtn').css('visibility','visible');

						// 						var data = google.visualization.arrayToDataTable(response.arrMain);
						// 					    var options = {
						// 							width: '100%',
						// 							height: '100%',
						// 							colors: ['#000','#eb1c24'],
						// 							hAxis: { title: '',  titleTextStyle: { color: '#333' } },
						// 							vAxis: {
						// 								minValue: 0,
						// 								viewWindow: {
						// 									// min: 0,
						// 									// max: 100
						// 								}
						// 							},
						// 							// curveType: 'function',
						// 							chartArea: {
						// 								top: 30,
						// 								left: 50,
						// 								// height: '50%',
						// 								width: '100%'
						// 							},
						// 							legend: 'none',
						// 							pointSize: 5
						// 						};
						// 						var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
						// 						chart.draw(data, options);
						// 					}
						// 				});	
						// 			}
						// 		}
						// 	}
						// });	
					}
				}
			}
		});	
	}
</script>

<script type="text/javascript">
	function getHappyIndexRespWeekCat(moodId=null,columnColor=null,monthNum) {

		var form_values   	= $("#dashboard-report-form").serializeArray();
		var orgId      		= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getHappyIndexRespWeekCat')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,moodId:moodId,columnColor:columnColor,monthNum:monthNum,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				console.log(response.columnColor);
				// return false
				if (response.columnColor==null) {
					var lineColors = ['#000','#eb1c24'];
				}else {
					var lineColors = [columnColor];
				}
				if (response.maxCount <= 0) {
					var maxCount = 100;
				}
				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					// colors: ['#ff454b'],
					colors: lineColors,
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							// min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
				};
				var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
				chart.draw(data, options);

				google.visualization.events.addListener(chart, 'select', selectHandler);

		       	function selectHandler() {

		       		var yearMonth = response.yearMonth;

					var selectedItem = chart.getSelection();

					if (selectedItem[0]) {
					    // if (selectedItem[0]) {
						// if (selectedItem[0].row) {
							var weekNum 	 = selectedItem[0].row;
							var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
						// }else{
						// 	var weekNum 	 = 0;
						// 	var value = 0;
						// }
					    // }
					}else{
						var weekNum 	 = 0;
						var value = 0;
					}
				    if (value>0) {
						var form_values   = $("#dashboard-report-form").serializeArray();
						var orgId  			= form_values[1]['value'];
						var officeId      	= form_values[2]['value'];
						var departmentId  	= form_values[3]['value'];

						//Happy index responders days graph
						getHappyIndexRespDaysGraph(orgId,officeId,departmentId,weekNum,yearMonth);

						//Happy index days graph
						getHappyIndexDaysGraph(orgId,officeId,departmentId,weekNum,yearMonth);

						// $.ajax({
						// 	type: "POST",
						// 	//dataType:"json",
						// 	url: "{{URL::to('admin/getHappyIndexRespDaysGraph')}}",
						// 	data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
						// 	success: function(response)
						// 	{
						// 		$('#happyIndexResCate').html('');

						// 		var happyIndexCategory = '';

						// 		happyIndexCategory = '<div class="motCat">'+
						// 		'<ul>'+
						// 		'<li>'+
						// 		'<a onclick="getHappyIndexRespDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
						// 		'<span style="background-color: red;"></span>'+
						// 		'All</a>'+
						// 		'</li>';

						// 		var catArr = response.arrMain[0];

						// 		if (catArr) {
						// 			var c=0;
						// 			var catClr = ['','#000','#eb1c24'];
						// 			//Loop Array
						// 			$.each(catArr,function(key,value){
						// 				if(c>0){
						// 		 			var columColor = catClr[c];
						// 		 			happyIndexCategory += '<li><a onclick="getHappyIndexRespDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
						// 		 		}
						// 		 		c++;
						// 			});
						// 		}
						// 		happyIndexCategory += '</ul></div>';
								
						// 		$('#happyIndexResCate').append(happyIndexCategory);

						// 		$('#happyIndexResWeekBackBtn').css('visibility','hidden');
						// 		$('#happyIndexResDaysBackBtn').css('visibility','visible');

						// 		var data = google.visualization.arrayToDataTable(response.arrMain);
						// 	    var options = {
						// 			width: '100%',
						// 			height: '100%',
						// 			colors: ['#000','#eb1c24'],
						// 			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
						// 			vAxis: {
						// 				minValue: 0,
						// 				viewWindow: {
						// 					// min: 0,
						// 					// max: 100
						// 				}
						// 			},
						// 			// curveType: 'function',
						// 			chartArea: {
						// 				top: 30,
						// 				left: 50,
						// 				// height: '50%',
						// 				width: '100%'
						// 			},
						// 			legend: 'none',
						// 			pointSize: 5
						// 		};
						// 		var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
						// 		chart.draw(data, options);
						// 	}
						// });	
					}
				}
			}
		});	
	}
</script>

<script type="text/javascript">
	function getHappyIndexRespDaysCat(moodId=null,columnColor=null,weekNum,yearMonth) {
		
		var form_values   	= $("#dashboard-report-form").serializeArray();
		var orgId      		= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getHappyIndexRespDaysGraph')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,moodId:moodId,columnColor:columnColor,weekNum:weekNum,yearMonth:yearMonth,index:2,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				if (response.columnColor==null) {
					var lineColors = ['#000','#eb1c24'];
				}else {
					var lineColors = [columnColor];
				}
				if (response.maxCount <= 0) {
					var maxCount = 100;
				}
				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					// colors: ['#ff454b'],
					colors: lineColors,
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							// min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
				};
				var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
				chart.draw(data, options);
			}
		});	
	}
</script>

<script type="text/javascript">
	function getHappyIndexRespMonthGraph(category,mainResponse,containerId) {
		
		$('#happyIndexResCate').html('');

		var happyIndexCategory = '';
		happyIndexCategory = '<div class="motCat">'+
		'<ul>'+
		'<li>'+
		'<a onclick="getHappyIndexResCat();">'+
		'<span style="background-color: red;"></span>'+
		'All</a>'+
		'</li>';

		var catArr = category;
		if (catArr) {
			var c=0;
			var catClr = ['','#000','#eb1c24'];
			//Loop Array
			$.each(catArr,function(key,value){
				if(c>0){
		 			var columColor = catClr[c];
		 			happyIndexCategory += '<li><a onclick="getHappyIndexResCat('+key+',\'' + columColor + '\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		 		}
		 		c++;
			});
		}
		happyIndexCategory += '</ul></div>';
		
		$('#happyIndexResCate').append(happyIndexCategory);

		var data = google.visualization.arrayToDataTable(mainResponse);
		var options = {
			width: '100%',
			height: '100%',
			colors: ['#000','#eb1c24'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if($resultArray['happyIndexResArray']['maxCount'] <= 0)
						max: 100
					@endif
					// min: 0,
					// max: 100
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				// height: '50%',
				width: '100%'
			},
			legend: 'none',
			pointSize: 5
		};
		var chart = new google.visualization.LineChart(document.getElementById(containerId));
		chart.draw(data, options);

		google.visualization.events.addListener(chart, 'select', selectHandler);

       	function selectHandler() {
			var selectedItem = chart.getSelection();
			if (selectedItem[0]) {
				var monthNum 	 = selectedItem[0].row;
				var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
			}else{
				var monthNum 	 = 0;
				var value = 0;
			}
			// console.log(monthNum);
			// console.log(value);
			// return false;
		    if (value>0) {
		    	$('#monthNumHI').val(monthNum);
		    	$('#monthNum').val(monthNum);

				var form_values   = $("#dashboard-report-form").serializeArray();
				var orgId  			= form_values[1]['value'];
				var officeId      	= form_values[2]['value'];
				var departmentId  	= form_values[3]['value'];


				//Happy index responders week graph
				getHappyIndexRespWeekGraph(orgId,officeId,departmentId,monthNum);

				//Happy index week graph
				getHappyIndexWeekGraph(orgId,officeId,departmentId,monthNum);

				// $.ajax({
				// 	type: "POST",
				// 	//dataType:"json",
				// 	url: "{{URL::to('admin/getHappyIndexRespWeekGraph')}}",
				// 	data: {orgId:orgId,officeId:officeId,departmentId:departmentId,monthNum:monthNum,"_token":'<?php echo csrf_token()?>'},
				// 	success: function(response)
				// 	{
				// 		$('#happyIndexResCate').html('');

				// 		var happyIndexCategory = '';

				// 		happyIndexCategory = '<div class="motCat">'+
				// 		'<ul>'+
				// 		'<li>'+
				// 		'<a onclick="getHappyIndexRespWeekCat(\'\',\'\',\''+monthNum+'\');">'+
				// 		'<span style="background-color: red;"></span>'+
				// 		'All</a>'+
				// 		'</li>';

				// 		var catArr = response.arrMain[0];

				// 		if (catArr) {
				// 			var c=0;
				// 			var catClr = ['','#000','#eb1c24'];
				// 			//Loop Array
				// 			$.each(catArr,function(key,value){
				// 				if(c>0){
				// 		 			var columColor = catClr[c];
				// 		 			happyIndexCategory += '<li><a onclick="getHappyIndexRespWeekCat('+key+',\'' + columColor + '\',\''+monthNum+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
				// 		 		}
				// 		 		c++;
				// 			});
				// 		}
				// 		happyIndexCategory += '</ul></div>';
						
				// 		$('#happyIndexResCate').append(happyIndexCategory);

				// 		$('#happyIndexResWeekBackBtn').css('visibility','visible');

				// 		var data = google.visualization.arrayToDataTable(response.arrMain);
				// 	    var options = {
				// 			width: '100%',
				// 			height: '100%',
				// 			colors: ['#000','#eb1c24'],
				// 			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
				// 			vAxis: {
				// 				minValue: 0,
				// 				viewWindow: {
				// 					// min: 0,
				// 					// max: 100
				// 				}
				// 			},
				// 			// curveType: 'function',
				// 			chartArea: {
				// 				top: 30,
				// 				left: 50,
				// 				// height: '50%',
				// 				width: '100%'
				// 			},
				// 			legend: 'none',
				// 			pointSize: 5
				// 		};
				// 		var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
				// 		chart.draw(data, options);

				// 		google.visualization.events.addListener(chart, 'select', selectHandler1);

				//        	function selectHandler1() {
				//        		var yearMonth = response.yearMonth;
				// 			var selectedItem = chart.getSelection();
				// 			if (selectedItem[0]) {
				// 				var weekNum 	 = selectedItem[0].row;
				// 				var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
				// 			}else{
				// 				var weekNum	= 0;
				// 				var value = 0;
				// 			}
				// 		    if (value>0) {
				// 				var form_values   = $("#dashboard-report-form").serializeArray();
				// 				var orgId  			= form_values[1]['value'];
				// 				var officeId      	= form_values[2]['value'];
				// 				var departmentId  	= form_values[3]['value'];

				// 				$.ajax({
				// 					type: "POST",
				// 					//dataType:"json",
				// 					url: "{{URL::to('admin/getHappyIndexRespDaysGraph')}}",
				// 					data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
				// 					success: function(response)
				// 					{
				// 						$('#happyIndexResCate').html('');

				// 						var happyIndexCategory = '';

				// 						happyIndexCategory = '<div class="motCat">'+
				// 						'<ul>'+
				// 						'<li>'+
				// 						'<a onclick="getHappyIndexRespDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
				// 						'<span style="background-color: red;"></span>'+
				// 						'All</a>'+
				// 						'</li>';

				// 						var catArr = response.arrMain[0];

				// 						if (catArr) {
				// 							var c=0;
				// 							var catClr = ['','#000','#eb1c24'];
				// 							//Loop Array
				// 							$.each(catArr,function(key,value){
				// 								if(c>0){
				// 						 			var columColor = catClr[c];
				// 						 			happyIndexCategory += '<li><a onclick="getHappyIndexRespDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
				// 						 		}
				// 						 		c++;
				// 							});
				// 						}
				// 						happyIndexCategory += '</ul></div>';
										
				// 						$('#happyIndexResCate').append(happyIndexCategory);

				// 						$('#happyIndexResWeekBackBtn').css('visibility','hidden');
				// 						$('#happyIndexResDaysBackBtn').css('visibility','visible');

				// 						var data = google.visualization.arrayToDataTable(response.arrMain);
				// 					    var options = {
				// 							width: '100%',
				// 							height: '100%',
				// 							colors: ['#000','#eb1c24'],
				// 							hAxis: { title: '',  titleTextStyle: { color: '#333' } },
				// 							vAxis: {
				// 								minValue: 0,
				// 								viewWindow: {
				// 									// min: 0,
				// 									// max: 100
				// 								}
				// 							},
				// 							// curveType: 'function',
				// 							chartArea: {
				// 								top: 30,
				// 								left: 50,
				// 								// height: '50%',
				// 								width: '100%'
				// 							},
				// 							legend: 'none',
				// 							pointSize: 5
				// 						};
				// 						var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
				// 						chart.draw(data, options);
				// 					}
				// 				});	
				// 			}
				// 		}
				// 	}
				// });	
			}
		}
	}
</script>

<script type="text/javascript">
	function getHappyIndexRespWeekGraph(orgId,officeId,departmentId,monthNum) {
		$.ajax({
			type: "POST",
			//dataType:"json",
			url: "{{URL::to('admin/getHappyIndexRespWeekGraph')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,monthNum:monthNum,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				$('#happyIndexResCate').html('');

				var happyIndexCategory = '';

				happyIndexCategory = '<div class="motCat">'+
				'<ul>'+
				'<li>'+
				'<a onclick="getHappyIndexRespWeekCat(\'\',\'\',\''+monthNum+'\');">'+
				'<span style="background-color: red;"></span>'+
				'All</a>'+
				'</li>';

				var catArr = response.arrMain[0];

				if (catArr) {
					var c=0;
					var catClr = ['','#000','#eb1c24'];
					//Loop Array
					$.each(catArr,function(key,value){
						if(c>0){
				 			var columColor = catClr[c];
				 			happyIndexCategory += '<li><a onclick="getHappyIndexRespWeekCat('+key+',\'' + columColor + '\',\''+monthNum+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
				 		}
				 		c++;
					});
				}
				happyIndexCategory += '</ul></div>';
				
				$('#happyIndexResCate').append(happyIndexCategory);

				$('#happyIndexResDaysBackBtn').css('visibility','hidden');
				$('#happyIndexResWeekBackBtn').css('visibility','visible');

				if (response.maxCount <= 0) {
					var maxCount = 100;
				}

				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					colors: ['#000','#eb1c24'],
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							// min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
				};
				var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
				chart.draw(data, options);

				google.visualization.events.addListener(chart, 'select', selectHandler1);

		       	function selectHandler1() {
		       		var yearMonth = response.yearMonth;
					var selectedItem = chart.getSelection();
					if (selectedItem[0]) {
						var weekNum 	 = selectedItem[0].row;
						var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
					}else{
						var weekNum	= 0;
						var value = 0;
					}
				    if (value>0) {
						var form_values   = $("#dashboard-report-form").serializeArray();
						var orgId  			= form_values[1]['value'];
						var officeId      	= form_values[2]['value'];
						var departmentId  	= form_values[3]['value'];

						//Happy index responders days graph
						getHappyIndexRespDaysGraph(orgId,officeId,departmentId,weekNum,yearMonth);

						//Happy index days graph
						getHappyIndexDaysGraph(orgId,officeId,departmentId,weekNum,yearMonth);

						// $.ajax({
						// 	type: "POST",
						// 	//dataType:"json",
						// 	url: "{{URL::to('admin/getHappyIndexRespDaysGraph')}}",
						// 	data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
						// 	success: function(response)
						// 	{
						// 		$('#happyIndexResCate').html('');

						// 		var happyIndexCategory = '';

						// 		happyIndexCategory = '<div class="motCat">'+
						// 		'<ul>'+
						// 		'<li>'+
						// 		'<a onclick="getHappyIndexRespDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
						// 		'<span style="background-color: red;"></span>'+
						// 		'All</a>'+
						// 		'</li>';

						// 		var catArr = response.arrMain[0];

						// 		if (catArr) {
						// 			var c=0;
						// 			var catClr = ['','#000','#eb1c24'];
						// 			//Loop Array
						// 			$.each(catArr,function(key,value){
						// 				if(c>0){
						// 		 			var columColor = catClr[c];
						// 		 			happyIndexCategory += '<li><a onclick="getHappyIndexRespDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
						// 		 		}
						// 		 		c++;
						// 			});
						// 		}
						// 		happyIndexCategory += '</ul></div>';
								
						// 		$('#happyIndexResCate').append(happyIndexCategory);

						// 		$('#happyIndexResWeekBackBtn').css('visibility','hidden');
						// 		$('#happyIndexResDaysBackBtn').css('visibility','visible');

						// 		var data = google.visualization.arrayToDataTable(response.arrMain);
						// 	    var options = {
						// 			width: '100%',
						// 			height: '100%',
						// 			colors: ['#000','#eb1c24'],
						// 			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
						// 			vAxis: {
						// 				minValue: 0,
						// 				viewWindow: {
						// 					// min: 0,
						// 					// max: 100
						// 				}
						// 			},
						// 			// curveType: 'function',
						// 			chartArea: {
						// 				top: 30,
						// 				left: 50,
						// 				// height: '50%',
						// 				width: '100%'
						// 			},
						// 			legend: 'none',
						// 			pointSize: 5
						// 		};
						// 		var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
						// 		chart.draw(data, options);
						// 	}
						// });	
					}
				}
			}
		});
	}
</script>

<script type="text/javascript">
	function getHappyIndexRespDaysGraph(orgId,officeId,departmentId,weekNum,yearMonth) {
		$.ajax({
			type: "POST",
			//dataType:"json",
			url: "{{URL::to('admin/getHappyIndexRespDaysGraph')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,weekNum:weekNum,yearMonth:yearMonth,index:1,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				$('#happyIndexResCate').html('');

				var happyIndexCategory = '';

				happyIndexCategory = '<div class="motCat">'+
				'<ul>'+
				'<li>'+
				'<a onclick="getHappyIndexRespDaysCat(\'\',\'\',\''+weekNum+'\',\''+yearMonth+'\');">'+
				'<span style="background-color: red;"></span>'+
				'All</a>'+
				'</li>';

				var catArr = response.arrMain[0];

				if (catArr) {
					var c=0;
					var catClr = ['','#000','#eb1c24'];
					//Loop Array
					$.each(catArr,function(key,value){
						if(c>0){
				 			var columColor = catClr[c];
				 			happyIndexCategory += '<li><a onclick="getHappyIndexRespDaysCat('+key+',\'' + columColor + '\',\''+weekNum+'\',\''+yearMonth+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
				 		}
				 		c++;
					});
				}
				happyIndexCategory += '</ul></div>';
				
				$('#happyIndexResCate').append(happyIndexCategory);

				$('#happyIndexResWeekBackBtn').css('visibility','hidden');
				$('#happyIndexResDaysBackBtn').css('visibility','visible');

				if (response.maxCount <= 0) {
					var maxCount = 100;
				}

				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					colors: ['#000','#eb1c24'],
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							// min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
				};
				var chart = new google.visualization.LineChart(document.getElementById('happyIndexResponders'));
				chart.draw(data, options);
			}
		});
	}
</script>

<script type="text/javascript">

function drawDiagnostic(containerId) {

	// $('#diagnosticCate').html();
	var diagCategory = '';

	diagCategory = '<div class="motCat">'+
	'<ul>'+
	'<li>'+
	'<a onclick="getDiagnosticCat(\'\',\'\');">'+
	'<span style="background-color: red;"></span>'+
	'All</a>'+
	'</li>';

	var catArr = <?php echo json_encode($resultArray['diagnosticArray']['arrMain'][0]);?>;

	if (catArr) {
		var c=0;
		var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
		//Loop Array
		$.each(catArr,function(key,value){
			if(c>0){
	 			var columColor = catClr[c];
	 			diagCategory += '<li><a onclick="getDiagnosticCat('+key+',\'' + columColor + '\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
	 		}
	 		c++;
		});
	}
	diagCategory += '</ul></div>';
	
	$('#diagnosticCate').append(diagCategory);

	var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['diagnosticArray']['arrMain']);?>);
	var options = {
		width: '100%',
		height: '100%',
		// colors: ['#ffb4b7','#ff6469','#eb1c24','#939598','#bfbfbf','#000'],
		colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
		hAxis: {
			titleTextStyle: {
				color: '#333'
			}
		},
		vAxis: {
			minValue: 0,
			viewWindow: {
				@if($resultArray['diagnosticArray']['maxCount'] <= 0)
					max: 100
				@endif
				// min: 0,
				// max: 100
			}
		},
		// curveType: 'function',
		chartArea: {
			top: 30,
			left: 50,
			// height: '50%',
			width: '100%'
		},
		legend: 'none',
		pointSize: 5
		// legend: { position: 'bottom',
		// 	textStyle: {bold:true},
		// 	pagingTextStyle: {
		// 		color: '#616161'
		// 	},
		// 	scrollArrows:{
		// 		activeColor: '#eb1c24',
		// 		inactiveColor:'#e0e0e0'
		// 	}
		// }
	};
	var chart = new google.visualization.LineChart(document.getElementById(containerId));
	chart.draw(data, options);

	google.visualization.events.addListener(chart, 'select', selectHandler);

       function selectHandler() {

      	// var selection = chart.getSelection();

		var selectedItem = chart.getSelection();

		// console.log(selectedItem);
		// return false;
		if (selectedItem[0]) {
			var category = selectedItem[0].column;
	       	//var label = 0;
		    if (selectedItem[0]) {
		      // var label = data.getColumnLabel(selectedItem.column);
		      if (selectedItem[0].row) {
		      	var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
		      }else{
		      	var value = 0;
		      }
		    }
		}else{
			var value = 0;
		}

	    // console.log(selectedItem);
	    // console.log(category);
	    // console.log(selectedItem);
	    // // console.log(label);
	    // console.log(value);
	    // return false;
	    if (value>0) {
			var form_values   = $("#dashboard-report-form").serializeArray();
			var orgId  			= form_values[1]['value'];
			var officeId      	= form_values[2]['value'];
			var departmentId  	= form_values[3]['value'];

			$('.loader-img').show();
			var orgId = "{{base64_encode($orgId)}}";
			
			$.ajax({
				type: "POST",
				//dataType:"json",
				url: "{{URL::to('admin/getDashboardSubDiagGraph')}}",
				data: {orgId:orgId,category:category,officeId:officeId,departmentId:departmentId,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					$('.loader-img').hide();

					$('#diagnosticCate').html('');
					
					var diagCategory = '';

					diagCategory = '<div class="motCat">'+
					'<ul>'+
					'<li>'+
					'<a onclick="getDiagnosticSubCat(\'\',\'\',\''+category+'\');">'+
					'<span style="background-color: red;"></span>'+
					'All</a>'+
					'</li>';

					var catArr = response.qusId;

					if (catArr) {
						var c=0;
						var catClr = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];

						//Loop Array
						$.each(catArr,function(key,value){
						// console.log(key);
							//if(c>0){
					 			var columColor = catClr[c];
						// console.log(columColor);
					 			diagCategory += '<li><a onclick="getDiagnosticSubCat('+key+',\'' + columColor + '\',\''+category+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
					 		// }
					 		c++;
						});
					}
					diagCategory += '</ul></div>';
					
					$('#diagnosticCate').append(diagCategory);

					$('#diagBackBtn').css('visibility','visible');
					// return false;
					google.charts.load('current', {packages: ['corechart']});
					google.charts.setOnLoadCallback(drawSubDiagnostic);

					function drawSubDiagnostic() {

						// $('#diagnostic').remove();

						var data = google.visualization.arrayToDataTable(response.arrMain);
						var options = {
							width: '100%',
							height: '100%',
							colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
							// colors: ['#ffb4b7','#ff6469','#eb1c24','#939598','#bfbfbf','#000'],
							hAxis: {
								titleTextStyle: {
									color: '#333'
								}
							},
							vAxis: {
								minValue: 0,
								viewWindow: {
									// min: 0,
									// max: 100
								}
							},
							// curveType: 'function',
							chartArea: {
								top: 30,
								left: 50,
								// height: '50%',
								width: '100%'
							},
							legend: 'none',
							pointSize: 5
							// legend: { position: 'bottom',
							// 	textStyle: {bold:true},
							// 	pagingTextStyle: {
							// 		color: '#616161'
							// 	},
							// 	scrollArrows:{
							// 		activeColor: '#eb1c24',
							// 		inactiveColor:'#e0e0e0'
							// 	}
							// }
						};
						var chart = new google.visualization.LineChart(document.getElementById('diagnostic'));
						chart.draw(data, options);	
					}
				}
			});	
		}	
	}
}
</script>

<script type="text/javascript">
	$('#diagBackBtn').on('click',function(){

		$('#diagBackBtn').css('visibility','hidden');

		$('#diagnosticCate').html('');
		
		var diagCategory = '';

		diagCategory = '<div class="motCat">'+
		'<ul>'+
		'<li>'+
		'<a onclick="getDiagnosticCat(\'\',\'\');">'+
		'<span style="background-color: red;"></span>'+
		'All</a>'+
		'</li>';

		var catArr = <?php echo json_encode($resultArray['diagnosticArray']['arrMain'][0]);?>;

		if (catArr) {
			var c=0;
			var catClr = ['','#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
			//Loop Array
			$.each(catArr,function(key,value){
				if(c>0){
		 			var columColor = catClr[c];
		 			diagCategory += '<li><a onclick="getDiagnosticCat('+key+',\'' + columColor + '\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		 		}
		 		c++;
			});
		}
		diagCategory += '</ul></div>';
		
		$('#diagnosticCate').append(diagCategory);

		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['diagnosticArray']['arrMain']);?>);
		var options = {
			width: '100%',
			height: '100%',
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			// colors: ['#ffb4b7','#ff6469','#eb1c24','#939598','#bfbfbf','#000'],
			hAxis: {
				titleTextStyle: {
					color: '#333'
				}
			},
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if($resultArray['diagnosticArray']['maxCount'] <= 0)
						max: 100
					@endif
					// min: 0,
					// max: 100
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				// height: '50%',
				width: '100%'
			},
			legend: 'none',
			pointSize: 5
			// legend: { position: 'bottom',
			// 	textStyle: {bold:true},
			// 	pagingTextStyle: {
			// 		color: '#616161'
			// 	},
			// 	scrollArrows:{
			// 		activeColor: '#eb1c24',
			// 		inactiveColor:'#e0e0e0'
			// 	}
			// }
		};
		var chart = new google.visualization.LineChart(document.getElementById('diagnostic'));
		chart.draw(data, options);

		google.visualization.events.addListener(chart, 'select', selectHandler);

	       function selectHandler() {

	      	// var selection = chart.getSelection();

			var selectedItem = chart.getSelection();
			if (selectedItem[0]) {
				var category = selectedItem[0].column;
		       	//var label = 0;
			    if (selectedItem[0]) {
					// var label = data.getColumnLabel(selectedItem.column);
					if (selectedItem[0].row) {
						var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
					}else{
						var value = 0;
					}
			    }
			}else{
				var value = 0;
			}

		    // console.log(selectedItem);
		    // console.log(category);
		    // console.log(selectedItem);
		    // // console.log(label);
		    // console.log(value);
		    // return false;
		    if (value>0) {
				var form_values   = $("#dashboard-report-form").serializeArray();
				var orgId  			= form_values[1]['value'];
				var officeId      	= form_values[2]['value'];
				var departmentId  	= form_values[3]['value'];

				$('.loader-img').show();
				var orgId = "{{base64_encode($orgId)}}";
				
				$.ajax({
					type: "POST",
					//dataType:"json",
					url: "{{URL::to('admin/getDashboardSubDiagGraph')}}",
					data: {orgId:orgId,category:category,officeId:officeId,departmentId:departmentId,"_token":'<?php echo csrf_token()?>'},
					success: function(response)
					{
						$('.loader-img').hide();
						//console.log(response);
						//console.log(response.measures);

						$('#diagnosticCate').html('');
					
						var diagCategory = '';

						diagCategory = '<div class="motCat">'+
						'<ul>'+
						'<li>'+
						'<a onclick="getDiagnosticSubCat(\'\',\'\',\''+category+'\');">'+
						'<span style="background-color: red;"></span>'+
						'All</a>'+
						'</li>';

						var catArr = response.qusId;

						console.log(catArr);
						console.log(category);

						// return false;

						if (catArr) {
							var c=0;
							var catClr = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];

							//Loop Array
							$.each(catArr,function(key,value){
							// console.log(key);
								//if(c>0){
						 			var columColor = catClr[c];
							// console.log(columColor);
						 			diagCategory += '<li><a onclick="getDiagnosticSubCat('+key+',\'' + columColor + '\',\''+category+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
						 		// }
						 		c++;
							});
						}
						diagCategory += '</ul></div>';
						
						$('#diagnosticCate').append(diagCategory);

						$('#diagBackBtn').css('visibility','visible');
						// return false;
						google.charts.load('current', {packages: ['corechart']});
						google.charts.setOnLoadCallback(drawSubDiagnostic);

						function drawSubDiagnostic() {

							// $('#diagnostic').remove();
							var data = google.visualization.arrayToDataTable(response.arrMain);
							var options = {
								width: '100%',
								height: '100%',
								colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
								// colors: ['#ffb4b7','#ff6469','#eb1c24','#939598','#bfbfbf','#000'],
								hAxis: {
									titleTextStyle: {
										color: '#333'
									}
								},
								vAxis: {
									minValue: 0,
									viewWindow: {
										// min: 0,
										// max: 100
									}
								},
								// curveType: 'function',
								chartArea: {
									top: 30,
									left: 50,
									// height: '50%',
									width: '100%'
								},
								legend: 'none',
								pointSize: 5
								// legend: { position: 'bottom',
								// 	textStyle: {bold:true},
								// 	pagingTextStyle: {
								// 		color: '#616161'
								// 	},
								// 	scrollArrows:{
								// 		activeColor: '#eb1c24',
								// 		inactiveColor:'#e0e0e0'
								// 	}
								// }
							};
							var chart = new google.visualization.LineChart(document.getElementById('diagnostic'));
							chart.draw(data, options);	
						}
					}
				});	
			}	
		}
	});
</script>

<script type="text/javascript">
	function getDiagnosticCat(cateId=false,columnColor=false) {

		var form_values   	= $("#dashboard-report-form").serializeArray();

		var orgId      		= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		// console.log(orgId);
		// console.log(officeId);
		// console.log(departmentId);
		// console.log(cateId);
		// console.log(columnColor);
		// return false;

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getDiagnosticCat')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,categoryId:cateId,columnColor:columnColor,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				console.log(response.columnColor);
				// return false
				if (response.columnColor==null) {
					var lineColors = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
				}else {
					var lineColors = [columnColor];
				}
				if (response.maxCount <= 0) {
					var maxCount = 100;
				}
				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					// colors: ['#ff454b'],
					colors: lineColors,
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							// min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
				};
				var chart = new google.visualization.LineChart(document.getElementById('diagnostic'));
				chart.draw(data, options);

				google.visualization.events.addListener(chart, 'select', selectHandler);

			       	function selectHandler() {

				      	// var selection = chart.getSelection();

						var selectedItem = chart.getSelection();

						// console.log(selectedItem);
						// return false;
						if (selectedItem[0]) {
							var category = selectedItem[0].column;
					       	//var label = 0;
						    if (selectedItem[0]) {
						      // var label = data.getColumnLabel(selectedItem.column);
						      if (selectedItem[0].row) {
						      	var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
						      }else{
						      	var value = 0;
						      }
						    }
						}else{
							var value = 0;
						}

					    // console.log(selectedItem);
					    // console.log(category);
					    // console.log(selectedItem);
					    // // console.log(label);
					    // console.log(value);
					    // return false;
					    if (value>0) {
							var form_values   = $("#dashboard-report-form").serializeArray();
							var orgId  			= form_values[1]['value'];
							var officeId      	= form_values[2]['value'];
							var departmentId  	= form_values[3]['value'];

							$('.loader-img').show();
							var orgId = "{{base64_encode($orgId)}}";
							
							$.ajax({
								type: "POST",
								//dataType:"json",
								url: "{{URL::to('admin/getDashboardSubDiagGraph')}}",
								data: {orgId:orgId,category:cateId,officeId:officeId,departmentId:departmentId,"_token":'<?php echo csrf_token()?>'},
								success: function(response)
								{
									$('.loader-img').hide();
									//console.log(response);
									//console.log(response.arrMain);

									$('#diagnosticCate').html('');
					
									var diagCategory = '';

									diagCategory = '<div class="motCat">'+
									'<ul>'+
									'<li>'+
									'<a onclick="getDiagnosticSubCat(\'\',\'\',\''+cateId+'\');">'+
									'<span style="background-color: red;"></span>'+
									'All</a>'+
									'</li>';

									var catArr = response.qusId;

									if (catArr) {
										var c=0;
										var catClr = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];

										//Loop Array
										$.each(catArr,function(key,value){
										// console.log(key);
											//if(c>0){
									 			var columColor = catClr[c];
										// console.log(columColor);
									 			diagCategory += '<li><a onclick="getDiagnosticSubCat('+key+',\'' + columColor + '\',\''+cateId+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
									 		// }
									 		c++;
										});
									}
									diagCategory += '</ul></div>';
									
									$('#diagnosticCate').append(diagCategory);

									$('#diagBackBtn').css('visibility','visible');
									// return false;
									google.charts.load('current', {packages: ['corechart']});
									google.charts.setOnLoadCallback(drawSubDiagnostic);

									function drawSubDiagnostic() {

										console.log(response.arrMain);
										// $('#diagnostic').remove();

										var data = google.visualization.arrayToDataTable(response.arrMain);
										var options = {
											width: '100%',
											height: '100%',
											colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
											// colors: ['#ffb4b7','#ff6469','#eb1c24','#939598','#bfbfbf','#000'],
											hAxis: {
												titleTextStyle: {
													color: '#333'
												}
											},
											vAxis: {
												minValue: 0,
												viewWindow: {
													// min: 0,
													// max: 100
												}
											},
											// curveType: 'function',
											chartArea: {
												top: 30,
												left: 50,
												// height: '50%',
												width: '100%'
											},
											legend: 'none',
											pointSize: 5
											// legend: { position: 'bottom',
											// 	textStyle: {bold:true},
											// 	pagingTextStyle: {
											// 		color: '#616161'
											// 	},
											// 	scrollArrows:{
											// 		activeColor: '#eb1c24',
											// 		inactiveColor:'#e0e0e0'
											// 	}
											// }
										};
										var chart = new google.visualization.LineChart(document.getElementById('diagnostic'));
										chart.draw(data, options);	
									}
								}
							});	
						}	
				}
			}
		});		
	}
</script>

<script type="text/javascript">
	function getDiagnosticSubCat(qusId=false,columnColor=false,cateId) {

		var form_values   	= $("#dashboard-report-form").serializeArray();

		var orgId      		= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		// console.log(orgId);
		// console.log(officeId);
		// console.log(departmentId);
		// console.log(cateId);
		// console.log(columnColor);
		// return false;

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getDiagnosticSubCat')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,qusId:qusId,categoryId:cateId,columnColor:columnColor,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				console.log(response.columnColor);
				// return false
				if (response.columnColor==null) {
					var lineColors = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
				}else {
					var lineColors = [columnColor];
				}
				if (response.maxCount <= 0) {
					var maxCount = 100;
				}
				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					// colors: ['#ff454b'],
					colors: lineColors,
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							// min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
				};
				var chart = new google.visualization.LineChart(document.getElementById('diagnostic'));
				chart.draw(data, options);
			}
		});		
	
	}
</script>

<script type="text/javascript">
	function drawDirecting(containerId) {

		var beliefCategory = '';

		beliefCategory = '<div class="motCat">'+
		'<ul>'+
		'<li>'+
		'<a onclick="getDotBeliefCat(\'\',\'\');">'+
		'<span style="background-color: red;"></span>'+
		'All</a>'+
		'</li>';

		var catArr = <?php echo json_encode($resultArray['dotArray']['beliefId']);?>;

		// console.log(catArr);
		// return false;

		if (catArr) {
			var c=0;
			var catClr = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
			//Loop Array
			$.each(catArr,function(key,value){
				// if(c>0){
		 			var columColor = catClr[c];
		 			beliefCategory += '<li><a onclick="getDotBeliefCat('+key+',\'' + columColor + '\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		 		// }
		 		c++;
			});
		}
		beliefCategory += '</ul></div>';
		
		$('#beliefCate').append(beliefCategory);

		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['dotArray']['arrMain']);?>);
		var options = {
			width: '100%',
			height: '100%',
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			// colors: ['#ff454b'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					// min: 0,
					@if ($resultArray['dotArray']['maxCount'] <= 0) 
						max: 100
					@endif
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				// height: '50%',
				width: '100%'
			},
			legend: 'none',
			pointSize: 5
			// legend: { position: 'bottom',
			// 	textStyle: {bold:true},
			// 	pagingTextStyle: {
			// 		color: '#616161'
			// 	},
			// 	scrollArrows:{
			// 		activeColor: '#eb1c24',
			// 		inactiveColor:'#e0e0e0'
			// 	}
			// }
		};
		var chart = new google.visualization.LineChart(document.getElementById(containerId));
		chart.draw(data, options);

		google.visualization.events.addListener(chart, 'select', selectHandler);

       	function selectHandler() {

      //  		var selectedItem = chart.getSelection()[0];
	     //   	var value = 0;
		    // if (selectedItem) {
		    //   var value = data.getColumnLabel(selectedItem.column);
		    // }

		    var selectedItem = chart.getSelection();
			if (selectedItem[0]) {
		       	//var label = 0;
			    if (selectedItem[0]) {
			    	if (selectedItem[0].row) {
						var label = data.getColumnLabel(selectedItem[0].column);
						var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
					}else{
						// var label = 0;
						var value = 0;
					}
			    }
			}else{
				var value = 0;
			}
		    if (value>0) {
				var form_values   = $("#dashboard-report-form").serializeArray();
				var orgId  			= form_values[1]['value'];
				var officeId      	= form_values[2]['value'];
				var departmentId  	= form_values[3]['value'];
				var belief = label;

				$('.loader-img').show();
				var orgId = "{{base64_encode($orgId)}}";
				
				$.ajax({
					type: "POST",
					//dataType:"json",
					url: "{{URL::to('admin/getDashboardDotValues')}}",
					data: {orgId:orgId,belief:belief,officeId:officeId,departmentId:departmentId,"_token":'<?php echo csrf_token()?>'},
					success: function(response)
					{
						$('.loader-img').hide();
						$('#directingBackBtn').css('visibility','visible');
						$('#beliefCate').html('');

						var beliefCategory = '';

						beliefCategory = '<div class="motCat">'+
						'<ul>'+
						'<li>'+
						'<a onclick="getDotValueCat(\'\',\'\',\''+belief+'\');">'+
						'<span style="background-color: red;"></span>'+
						'All</a>'+
						'</li>';

						var catArr = response.valueName;

						console.log(catArr);
						// return false;

						if (catArr) {
							var c=0;
							var catClr = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
							//Loop Array
							$.each(catArr,function(key,value){
								// if(c>0){
						 			var columColor = catClr[c];
						 			beliefCategory += '<li><a onclick="getDotValueCat('+key+',\'' + columColor + '\',\''+belief+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
						 		// }
						 		c++;
							});
						}
						beliefCategory += '</ul></div>';
						
						$('#beliefCate').append(beliefCategory);

						// if (response.maxCount>0) {
						// 	var maxValue = '';
						// }else{
						// 	var maxValue = 100;
						// }
						if (response.maxCount <= 0) {
							var maxCount = 100;
						}
						// return false;
						google.charts.load('current', {packages: ['corechart']});
						google.charts.setOnLoadCallback(drawDOTValueArray);

						function drawDOTValueArray() {

							var data = google.visualization.arrayToDataTable(response.arrMain);
							var options = {
								width: '100%',
								height: '100%',
								// colors: ['#ff454b'],
								colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
								hAxis: {
									titleTextStyle: {
										color: '#333'
									}
								},
								vAxis: {
									minValue: 0,
									viewWindow: {
										// min: 0,
										// max: maxCount
									}
								},
								// curveType: 'function',
								chartArea: {
									top: 30,
									left: 50,
									// height: '50%',
									width: '100%'
								},
								legend: 'none',
								pointSize: 5
								// legend: { position: 'bottom',
								// 	textStyle: {bold:true},
								// 	pagingTextStyle: {
								// 		color: '#616161'
								// 	},
								// 	scrollArrows:{
								// 		activeColor: '#eb1c24',
								// 		inactiveColor:'#e0e0e0'
								// 	}
								// }
							};
							var chart = new google.visualization.LineChart(document.getElementById('directing'));
							chart.draw(data, options);	
						}
					}
				});
			}
		}
	}
</script>

<script type="text/javascript">
	$('#directingBackBtn').on('click',function(){

		$('#directingBackBtn').css('visibility','hidden');

		$('#beliefCate').html('');

		var beliefCategory = '';

		beliefCategory = '<div class="motCat">'+
		'<ul>'+
		'<li>'+
		'<a onclick="getDotBeliefCat(\'\',\'\');">'+
		'<span style="background-color: red;"></span>'+
		'All</a>'+
		'</li>';

		var catArr = <?php echo json_encode($resultArray['dotArray']['beliefId']);?>;

		// console.log(catArr);
		// return false;

		if (catArr) {
			var c=0;
			var catClr = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
			//Loop Array
			$.each(catArr,function(key,value){
				// if(c>0){
		 			var columColor = catClr[c];
		 			beliefCategory += '<li><a onclick="getDotBeliefCat('+key+',\'' + columColor + '\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
		 		// }
		 		c++;
			});
		}
		beliefCategory += '</ul></div>';
		
		$('#beliefCate').append(beliefCategory);


		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['dotArray']['arrMain']);?>);
		var options = {
			width: '100%',
			height: '100%',
			// colors: ['#ff454b'],
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					// min: 0,
					@if ($resultArray['dotArray']['maxCount'] <= 0) 
						max: 100
					@endif
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				// height: '50%',
				width: '100%'
			},
			legend: 'none',
			pointSize: 5
			// legend: { position: 'bottom',
			// 	textStyle: {bold:true},
			// 	pagingTextStyle: {
			// 		color: '#616161'
			// 	},
			// 	scrollArrows:{
			// 		activeColor: '#eb1c24',
			// 		inactiveColor:'#e0e0e0'
			// 	}
			// }
		};
		var chart = new google.visualization.LineChart(document.getElementById('directing'));
		chart.draw(data, options);

		google.visualization.events.addListener(chart, 'select', selectHandler);

       	function selectHandler() {
      //  		var selectedItem = chart.getSelection()[0];
	     //   	var value = 0;
		    // if (selectedItem) {
		    //   var value = data.getColumnLabel(selectedItem.column);
		    // }	

		    var selectedItem = chart.getSelection();
			if (selectedItem[0]) {
		       	//var label = 0;
			    if (selectedItem[0]) {
			    	if (selectedItem[0].row) {
						var label = data.getColumnLabel(selectedItem[0].column);
						var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
					}else{
						// var label = 0;
						var value = 0;
					}
			    }
			}else{
				var value = 0;
			}
		    if (value>0) {
				var form_values   = $("#dashboard-report-form").serializeArray();
				var orgId  			= form_values[1]['value'];
				var officeId      	= form_values[2]['value'];
				var departmentId  	= form_values[3]['value'];
				var belief = label;

				$('.loader-img').show();
				var orgId = "{{base64_encode($orgId)}}";
				
				$.ajax({
					type: "POST",
					//dataType:"json",
					url: "{{URL::to('admin/getDashboardDotValues')}}",
					data: {orgId:orgId,belief:belief,officeId:officeId,departmentId:departmentId,"_token":'<?php echo csrf_token()?>'},
					success: function(response)
					{
						$('.loader-img').hide();
						console.log(response);
						$('#directingBackBtn').css('visibility','visible');

						$('#beliefCate').html('');

						var beliefCategory = '';

						beliefCategory = '<div class="motCat">'+
						'<ul>'+
						'<li>'+
						'<a onclick="getDotValueCat(\'\',\'\',\''+belief+'\');">'+
						'<span style="background-color: red;"></span>'+
						'All</a>'+
						'</li>';

						var catArr = response.valueName;

						console.log(catArr);
						// return false;

						if (catArr) {
							var c=0;
							var catClr = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
							//Loop Array
							$.each(catArr,function(key,value){
								// if(c>0){
						 			var columColor = catClr[c];
						 			beliefCategory += '<li><a onclick="getDotValueCat('+key+',\'' + columColor + '\',\''+belief+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
						 		// }
						 		c++;
							});
						}
						beliefCategory += '</ul></div>';
						
						$('#beliefCate').append(beliefCategory);

						// if (response.maxCount>0) {
						// 	var maxValue = '';
						// }else{
						// 	var maxValue = 100;
						// }
						// return false;
						google.charts.load('current', {packages: ['corechart']});
						google.charts.setOnLoadCallback(drawDOTValueArray);

						function drawDOTValueArray() {
							var data = google.visualization.arrayToDataTable(response.arrMain);
							var options = {
								width: '100%',
								height: '100%',
								// colors: ['#ff454b'],
								colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
								hAxis: {
									titleTextStyle: {
										color: '#333'
									}
								},
								vAxis: {
									minValue: 0,
									viewWindow: {
										// min: 0,
										// max: maxValue
									}
								},
								// curveType: 'function',
								chartArea: {
									top: 30,
									left: 50,
									// height: '50%',
									width: '100%'
								},
								legend: 'none',
								pointSize: 5
								// legend: { position: 'bottom',
								// 	textStyle: {bold:true},
								// 	pagingTextStyle: {
								// 		color: '#616161'
								// 	},
								// 	scrollArrows:{
								// 		activeColor: '#eb1c24',
								// 		inactiveColor:'#e0e0e0'
								// 	}
								// }
							};
							var chart = new google.visualization.LineChart(document.getElementById('directing'));
							chart.draw(data, options);	
						}
					}
				});
			}
		}
	});
</script>

<script type="text/javascript">
	function getDotBeliefCat(cateId=false,columnColor=false) {
		

		var form_values   	= $("#dashboard-report-form").serializeArray();

		var orgId      		= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		console.log(orgId);
		console.log(officeId);
		console.log(departmentId);
		console.log(cateId);
		console.log(columnColor);
		// return false;

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getDotBeliefCat')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,categoryId:cateId,columnColor:columnColor,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				console.log(response.columnColor);
				// return false
				if (response.columnColor==null) {
					var lineColors = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
				}else {
					var lineColors = [columnColor];
				}

				// if (response.maxCount>0) {
				// 	var maxValue = '';
				// }else{
				// 	var maxValue = 100;
				// }
				if (response.maxCount <= 0) {
					var maxCount = 100;
				}
				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					// colors: ['#ff454b'],
					colors: lineColors,
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							// min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
				};
				var chart = new google.visualization.LineChart(document.getElementById('directing'));
				chart.draw(data, options);

				google.visualization.events.addListener(chart, 'select', selectHandler);

			       	function selectHandler() {

		      //  		var selectedItem = chart.getSelection()[0];
			     //   	var value = 0;
				    // if (selectedItem) {
				    //   var value = data.getColumnLabel(selectedItem.column);
				    // }

				    var selectedItem = chart.getSelection();
					if (selectedItem[0]) {
				       	//var label = 0;
					    if (selectedItem[0]) {
					    	if (selectedItem[0].row) {
								var label = data.getColumnLabel(selectedItem[0].column);
								var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
							}else{
								// var label = 0;
								var value = 0;
							}
					    }
					}else{
						var value = 0;
					}
				    if (value>0) {
						var form_values   = $("#dashboard-report-form").serializeArray();
						var orgId  			= form_values[1]['value'];
						var officeId      	= form_values[2]['value'];
						var departmentId  	= form_values[3]['value'];
						var belief = label;

						$('.loader-img').show();
						var orgId = "{{base64_encode($orgId)}}";
						
						$.ajax({
							type: "POST",
							//dataType:"json",
							url: "{{URL::to('admin/getDashboardDotValues')}}",
							data: {orgId:orgId,belief:belief,officeId:officeId,departmentId:departmentId,"_token":'<?php echo csrf_token()?>'},
							success: function(response)
							{
								$('.loader-img').hide();
								$('#directingBackBtn').css('visibility','visible');

								$('#beliefCate').html('');

								var beliefCategory = '';

								beliefCategory = '<div class="motCat">'+
								'<ul>'+
								'<li>'+
								'<a onclick="getDotValueCat(\'\',\'\',\''+belief+'\');">'+
								'<span style="background-color: red;"></span>'+
								'All</a>'+
								'</li>';

								var catArr = response.valueName;

								console.log(catArr);
								// return false;

								if (catArr) {
									var c=0;
									var catClr = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
									//Loop Array
									$.each(catArr,function(key,value){
										// if(c>0){
								 			var columColor = catClr[c];
								 			beliefCategory += '<li><a onclick="getDotValueCat('+key+',\'' + columColor + '\',\''+belief+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
								 		// }
								 		c++;
									});
								}
								beliefCategory += '</ul></div>';
								
								$('#beliefCate').append(beliefCategory);

								// if (response.maxCount>0) {
								// 	var maxValue = '';
								// }else{
								// 	var maxValue = 100;
								// }
								// return false;
								google.charts.load('current', {packages: ['corechart']});
								google.charts.setOnLoadCallback(drawDOTValueArray);

								function drawDOTValueArray() {

									var data = google.visualization.arrayToDataTable(response.arrMain);
									var options = {
										width: '100%',
										height: '100%',
										// colors: ['#ff454b'],
										colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
										hAxis: {
											titleTextStyle: {
												color: '#333'
											}
										},
										vAxis: {
											minValue: 0,
											viewWindow: {
												// min: 0,
												// max: maxValue
											}
										},
										// curveType: 'function',
										chartArea: {
											top: 30,
											left: 50,
											// height: '50%',
											width: '100%'
										},
										legend: 'none',
										pointSize: 5
										// legend: { position: 'bottom',
										// 	textStyle: {bold:true},
										// 	pagingTextStyle: {
										// 		color: '#616161'
										// 	},
										// 	scrollArrows:{
										// 		activeColor: '#eb1c24',
										// 		inactiveColor:'#e0e0e0'
										// 	}
										// }
									};
									var chart = new google.visualization.LineChart(document.getElementById('directing'));
									chart.draw(data, options);	
								}
							}
						});
					}
				}
			}
		});		
	}
</script>

<script type="text/javascript">
	function getDotValueCat(valueId=false,columnColor=false,beliefId) {
		
		var form_values   	= $("#dashboard-report-form").serializeArray();

		var orgId      		= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		// console.log(orgId);
		// console.log(officeId);
		// console.log(departmentId);
		// console.log(cateId);
		// console.log(columnColor);
		// return false;

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getDotValueCat')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,valueId:valueId,beliefId:beliefId,columnColor:columnColor,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				console.log(response.columnColor);
				// return false
				if (response.columnColor==null) {
					var lineColors = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
				}else {
					var lineColors = [columnColor];
				}

				// if (response.maxCount>0) {
				// 	var maxValue = '';
				// }else{
				// 	var maxValue = 100;
				// }
				if (response.maxCount <= 0) {
					var maxCount = 100;
				}
				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					// colors: ['#ff454b'],
					colors: lineColors,
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
				};
				var chart = new google.visualization.LineChart(document.getElementById('directing'));
				chart.draw(data, options);
			}
		});		
	
	}
</script>

<script type="text/javascript">
	function drawThumbsup(containerId) {

		var thumbsupCategory = '';

	    thumbsupCategory = '<div class="motCat">'+
	    '<ul>'+
	    '<li>'+
	    '<a onclick="getThumbsupBeliefCat(\'\',\'\');">'+
	    '<span style="background-color: red;"></span>'+
	    'All</a>'+
	    '</li>';

	    var catArr = <?php echo json_encode($resultArray['thumbsupArray']['beliefsName']);?>;

	    console.log(catArr);

	    if (catArr) {
	      var c=0;
	      var catClr = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
	      //Loop Array
	      $.each(catArr,function(key,value){
	        // if(c>0){
	          var columColor = catClr[c];
	          thumbsupCategory += '<li><a onclick="getThumbsupBeliefCat('+key+',\'' + columColor + '\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
	        // }
	        c++;
	      });
	    }
	    thumbsupCategory += '</ul></div>';
	    $('#thumbsupCate').append(thumbsupCategory);

		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['thumbsupArray']['arrMain']);?>);
		var options = {
			width: '100%',
			height: '100%',
			// colors: ['#ff454b'],
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					// min: 0,
					@if ($resultArray['thumbsupArray']['maxCount'] <= 0) 
						max: 100
					@endif
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				// height: '50%',
				width: '100%'
			},
			legend: 'none',
			pointSize: 5
			// legend: { position: 'bottom',
			// 	textStyle: {bold:true},
			// 	pagingTextStyle: {
			// 		color: '#616161'
			// 	},
			// 	scrollArrows:{
			// 		activeColor: '#eb1c24',
			// 		inactiveColor:'#e0e0e0'
			// 	}
			// }
		};
		var chart = new google.visualization.LineChart(document.getElementById(containerId));
		chart.draw(data, options);

		google.visualization.events.addListener(chart, 'select', selectHandler);

       	function selectHandler() {

	     //   	var selectedItem = chart.getSelection()[0];
	     //   	var value = 0;
		    // if (selectedItem) {
		    //   var value = data.getColumnLabel(selectedItem.column);
		    // }
			var selectedItem = chart.getSelection();
			if (selectedItem[0]) {
		       	//var label = 0;
			    if (selectedItem[0]) {
			    	if (selectedItem[0].row) {
						var label = data.getColumnLabel(selectedItem[0].column);
						var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
					}else{
						// var label = 0;
						var value = 0;
					}
			    }
			}else{
				var value = 0;
			}
		    // console.log(value);
		    // return false;
			// var message = '';
			// for (var i = 0; i < selection.length; i++) {
			// 	var item = selection[i];
			// 	if (item.row != null && item.column != null) {
			// 		var str = data.getFormattedValue(item.row, item.column);
			// 		var belief = data.getValue(chart.getSelection()[0].row, 0);
			if (value>0) {
				var form_values   = $("#dashboard-report-form").serializeArray();
				var orgId  			= form_values[1]['value'];
				var officeId      	= form_values[2]['value'];
				var departmentId  	= form_values[3]['value'];
				var belief = label;

				$('.loader-img').show();
				var orgId = "{{base64_encode($orgId)}}";
				
				$.ajax({
					type: "POST",
					//dataType:"json",
					url: "{{URL::to('admin/getDashboardThumbsupValues')}}",
					data: {orgId:orgId,belief:belief,officeId:officeId,departmentId:departmentId,"_token":'<?php echo csrf_token()?>'},
					success: function(response)
					{
						$('.loader-img').hide();
						console.log(response);
						$('#thumbsupBackBtn').css('visibility','visible');
						// return false;

						// var maxVal = response.max;

						// console.log(maxVal);

						$('#thumbsupCate').html('');

			            var thumbsupCategory = '';

			            thumbsupCategory = '<div class="motCat">'+
			            '<ul>'+
			            '<li>'+
			            '<a onclick="getThumbsupValuesCat(\'\',\'\',\''+belief+'\');">'+
			            '<span style="background-color: red;"></span>'+
			            'All</a>'+
			            '</li>';

			            var catArr = response.thumbsupValues;

			            console.log(catArr);
			            // return false;

			            if (catArr) {
			              var c=0;
			              var catClr = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
			              //Loop Array
			              $.each(catArr,function(key,value){
			                // if(c>0){
			                  var columColor = catClr[c];
			                  thumbsupCategory += '<li><a onclick="getThumbsupValuesCat('+key+',\'' + columColor + '\',\''+belief+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
			                // }
			                c++;
			              });
			            }
			            thumbsupCategory += '</ul></div>';
			            
			            $('#thumbsupCate').append(thumbsupCategory);

						// if (response.max>0) {
						// 	var maxValue = '';
						// }else{
						// 	var maxValue = 100;
						// }

						google.charts.load('current', {packages: ['corechart']});
						google.charts.setOnLoadCallback(drawThumbsupArray);

						function drawThumbsupArray() {
							var data = google.visualization.arrayToDataTable(response.arrMain);
							var options = {
								width: '100%',
								height: '100%',
								// colors: ['#ff454b'],
								colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
								hAxis: {
									titleTextStyle: {
										color: '#333'
									}
								},
								vAxis: {
									minValue: 0,
									viewWindow: {
										// min: 0
										// max:maxValue
									}
								},
								// curveType: 'function',
								chartArea: {
									top: 30,
									left: 50,
									// height: '50%',
									width: '100%'
								},
								legend: 'none',
								pointSize: 5
								// legend: { position: 'bottom',
								// 	textStyle: {bold:true},
								// 	pagingTextStyle: {
								// 		color: '#616161'
								// 	},
								// 	scrollArrows:{
								// 		activeColor: '#eb1c24',
								// 		inactiveColor:'#e0e0e0'
								// 	}
								// }
							};
							var chart = new google.visualization.LineChart(document.getElementById('thumbsup'));
							chart.draw(data, options);	
						}
					}
				});
			}
			//	}
			//}
		}
	}
</script>

<script type="text/javascript">
	$('#thumbsupBackBtn').on('click',function(){
		$('#thumbsupBackBtn').css('visibility','hidden');

		$('#thumbsupCate').html('');
	    var thumbsupCategory = '';

	    thumbsupCategory = '<div class="motCat">'+
	    '<ul>'+
	    '<li>'+
	    '<a onclick="getThumbsupBeliefCat(\'\',\'\');">'+
	    '<span style="background-color: red;"></span>'+
	    'All</a>'+
	    '</li>';

	    var catArr = <?php echo json_encode($resultArray['thumbsupArray']['beliefsName']);?>;

	    console.log(catArr);

	    if (catArr) {
	      var c=0;
	      var catClr = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
	      //Loop Array
	      $.each(catArr,function(key,value){
	        // if(c>0){
	          var columColor = catClr[c];
	          thumbsupCategory += '<li><a onclick="getThumbsupBeliefCat('+key+',\'' + columColor + '\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
	        // }
	        c++;
	      });
	    }
	    thumbsupCategory += '</ul></div>';
	    $('#thumbsupCate').append(thumbsupCategory);

		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['thumbsupArray']['arrMain']);?>);
		var options = {
			width: '100%',
			height: '100%',
			// colors: ['#ff454b'],
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					// min: 0,
					@if ($resultArray['thumbsupArray']['maxCount'] <= 0) 
						max: 100
					@endif
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				// height: '50%',
				width: '100%'
			},
			legend: 'none',
			pointSize: 5
			// legend: { position: 'bottom',
			// 	textStyle: {bold:true},
			// 	pagingTextStyle: {
			// 		color: '#616161'
			// 	},
			// 	scrollArrows:{
			// 		activeColor: '#eb1c24',
			// 		inactiveColor:'#e0e0e0'
			// 	}
			// }
		};
		var chart = new google.visualization.LineChart(document.getElementById('thumbsup'));
		chart.draw(data, options);

		google.visualization.events.addListener(chart, 'select', selectHandler);

       	function selectHandler() {

      //  		var selectedItem = chart.getSelection()[0];
	     //   	var value = 0;
		    // if (selectedItem) {
		    //   var value = data.getColumnLabel(selectedItem.column);
		    // }

		    var selectedItem = chart.getSelection();
			if (selectedItem[0]) {
		       	//var label = 0;
			    if (selectedItem[0]) {
			    	if (selectedItem[0].row) {
						var label = data.getColumnLabel(selectedItem[0].column);
						var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
					}else{
						// var label = 0;
						var value = 0;
					}
			    }
			}else{
				var value = 0;
			}

	  //      	var selection = chart.getSelection();
			// var message = '';
			// for (var i = 0; i < selection.length; i++) {
			// 	var item = selection[i];
			// 	if (item.row != null && item.column != null) {
			// 		var str = data.getFormattedValue(item.row, item.column);
			// 		var belief = data.getValue(chart.getSelection()[0].row, 0);
			if (value>0) {
				var form_values   = $("#dashboard-report-form").serializeArray();
				var orgId  			= form_values[1]['value'];
				var officeId      	= form_values[2]['value'];
				var departmentId  	= form_values[3]['value'];
				var belief = label;

				$('.loader-img').show();
				var orgId = "{{base64_encode($orgId)}}";
				
				$.ajax({
					type: "POST",
					//dataType:"json",
					url: "{{URL::to('admin/getDashboardThumbsupValues')}}",
					data: {orgId:orgId,belief:belief,officeId:officeId,departmentId:departmentId,"_token":'<?php echo csrf_token()?>'},
					success: function(response)
					{
						$('.loader-img').hide();
						console.log(response);
						$('#thumbsupBackBtn').css('visibility','visible');
						// return false;

						// var maxVal = response.max;
						// console.log(maxVal);

						$('#thumbsupCate').html('');

			            var thumbsupCategory = '';

			            thumbsupCategory = '<div class="motCat">'+
			            '<ul>'+
			            '<li>'+
			            '<a onclick="getThumbsupValuesCat(\'\',\'\',\''+belief+'\');">'+
			            '<span style="background-color: red;"></span>'+
			            'All</a>'+
			            '</li>';

			            var catArr = response.thumbsupValues;

			            console.log(catArr);
			            // return false;

			            if (catArr) {
			              var c=0;
			              var catClr = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
			              //Loop Array
			              $.each(catArr,function(key,value){
			                // if(c>0){
			                  var columColor = catClr[c];
			                  thumbsupCategory += '<li><a onclick="getThumbsupValuesCat('+key+',\'' + columColor + '\',\''+belief+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
			                // }
			                c++;
			              });
			            }
			            thumbsupCategory += '</ul></div>';
			            
			            $('#thumbsupCate').append(thumbsupCategory);

						// if (response.max>0) {
						// 	var maxValue = '';
						// }else{
						// 	var maxValue = 100;
						// }

						google.charts.load('current', {packages: ['corechart']});
						google.charts.setOnLoadCallback(drawThumbsupArray);

						function drawThumbsupArray() {
							var data = google.visualization.arrayToDataTable(response.arrMain);
							var options = {
								width: '100%',
								height: '100%',
								colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
								// colors: ['#ff454b'],
								hAxis: {
									titleTextStyle: {
										color: '#333'
									}
								},
								vAxis: {
									minValue: 0,
									viewWindow: {
										// min: 0
										// max:maxValue
									}
								},
								// curveType: 'function',
								chartArea: {
									top: 30,
									left: 50,
									// height: '50%',
									width: '100%'
								},
								legend: 'none',
								pointSize: 5
								// legend: { position: 'bottom',
								// 	textStyle: {bold:true},
								// 	pagingTextStyle: {
								// 		color: '#616161'
								// 	},
								// 	scrollArrows:{
								// 		activeColor: '#eb1c24',
								// 		inactiveColor:'#e0e0e0'
								// 	}
								// }
							};
							var chart = new google.visualization.LineChart(document.getElementById('thumbsup'));
							chart.draw(data, options);	
						}
					}
				});
			}
				//}
			//}
		}
	});
</script>

<script type="text/javascript">
  function getThumbsupBeliefCat(cateId=false,columnColor=false) {

    var form_values     = $("#dashboard-report-form").serializeArray();

    var orgId         = form_values[1]['value'];
    var officeId        = form_values[2]['value'];
    var departmentId    = form_values[3]['value'];

    console.log(orgId);
    console.log(officeId);
    console.log(departmentId);
    console.log(cateId);
    console.log(columnColor);
    // return false;

    $.ajax({
      type: "POST",
      url: "{{URL::to('admin/getThumbsupBeliefCat')}}",
      data: {orgId:orgId,officeId:officeId,departmentId:departmentId,categoryId:cateId,columnColor:columnColor,"_token":'<?php echo csrf_token()?>'},
      success: function(response)
      {
        console.log(response['columnColor']);
        // return false;
        if (response['columnColor']==null) {
          var lineColors = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
        }else {
          var lineColors = [columnColor];
        }

        // if (response['maxCount']>0) {
        //   var maxValue = '';
        // }else{
        //   var maxValue = 100;
        // }
        if (response.maxCount <= 0) {
			var maxCount = 100;
		}
        var data = google.visualization.arrayToDataTable(response['arrMain']);
          var options = {
          width: '100%',
          height: '100%',
          // colors: ['#ff454b'],
          colors: lineColors,
          hAxis: { title: '',  titleTextStyle: { color: '#333' } },
          vAxis: {
            minValue: 0,
            viewWindow: {
              // min: 0,
              max: maxCount
            }
          },
          // curveType: 'function',
          chartArea: {
            top: 30,
            left: 50,
            // height: '50%',
            width: '100%'
          },
          legend: 'none',
          pointSize: 5
        };
        var chart = new google.visualization.LineChart(document.getElementById('thumbsup'));
        chart.draw(data, options);

        google.visualization.events.addListener(chart, 'select', selectHandler);

          function selectHandler() {

           //     var selectedItem = chart.getSelection()[0];
           //     var value = 0;
            // if (selectedItem) {
            //   var value = data.getColumnLabel(selectedItem.column);
            // }
          var selectedItem = chart.getSelection();
          if (selectedItem[0]) {
                //var label = 0;
              if (selectedItem[0]) {
                if (selectedItem[0].row) {
                var label = data.getColumnLabel(selectedItem[0].column);
                var value = data.getValue(selectedItem[0].row, selectedItem[0].column);
              }else{
                // var label = 0;
                var value = 0;
              }
              }
          }else{
            var value = 0;
          }
            // console.log(value);
            // return false;
          // var message = '';
          // for (var i = 0; i < selection.length; i++) {
          //  var item = selection[i];
          //  if (item.row != null && item.column != null) {
          //    var str = data.getFormattedValue(item.row, item.column);
          //    var belief = data.getValue(chart.getSelection()[0].row, 0);
          if (value>0) {
            var form_values   = $("#dashboard-report-form").serializeArray();
            var orgId       = form_values[1]['value'];
            var officeId        = form_values[2]['value'];
            var departmentId    = form_values[3]['value'];
            var belief = label;

            $('.loader-img').show();
            var orgId = "{{base64_encode($orgId)}}";
            
            $.ajax({
              type: "POST",
              //dataType:"json",
              url: "{{URL::to('admin/getDashboardThumbsupValues')}}",
              data: {orgId:orgId,belief:belief,officeId:officeId,departmentId:departmentId,"_token":'<?php echo csrf_token()?>'},
              success: function(response)
              {
                $('.loader-img').hide();
                console.log(response);
                $('#thumbsupBackBtn').css('visibility','visible');
                // return false;

                // var maxVal = response.max;

                // console.log(maxVal);

                $('#thumbsupCate').html('');

	            var thumbsupCategory = '';

	            thumbsupCategory = '<div class="motCat">'+
	            '<ul>'+
	            '<li>'+
	            '<a onclick="getThumbsupValuesCat(\'\',\'\',\''+belief+'\');">'+
	            '<span style="background-color: red;"></span>'+
	            'All</a>'+
	            '</li>';

	            var catArr = response.thumbsupValues;

	            console.log(catArr);
	            // return false;

	            if (catArr) {
	              var c=0;
	              var catClr = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
	              //Loop Array
	              $.each(catArr,function(key,value){
	                // if(c>0){
	                  var columColor = catClr[c];
	                  thumbsupCategory += '<li><a onclick="getThumbsupValuesCat('+key+',\'' + columColor + '\',\''+belief+'\');"><span style="background-color: '+columColor+'"></span>'+value+'</a></li>';
	                // }
	                c++;
	              });
	            }
	            thumbsupCategory += '</ul></div>';
	            
	            $('#thumbsupCate').append(thumbsupCategory);

                // if (response.max>0) {
                //   var maxValue = '';
                // }else{
                //   var maxValue = 100;
                // }

                google.charts.load('current', {packages: ['corechart']});
                google.charts.setOnLoadCallback(drawThumbsupArray);

                function drawThumbsupArray() {
                  var data = google.visualization.arrayToDataTable(response.arrMain);
                  var options = {
                    width: '100%',
                    height: '100%',
                    // colors: ['#ff454b'],
                    colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
                    hAxis: {
                      titleTextStyle: {
                        color: '#333'
                      }
                    },
                    vAxis: {
                      minValue: 0,
                      viewWindow: {
                        // min: 0
                        // max:maxValue
                      }
                    },
                    // curveType: 'function',
                    chartArea: {
                      top: 30,
                      left: 50,
                      // height: '50%',
                      width: '100%'
                    },
                    legend: 'none',
                    pointSize: 5
                    // legend: { position: 'bottom',
                    //  textStyle: {bold:true},
                    //  pagingTextStyle: {
                    //    color: '#616161'
                    //  },
                    //  scrollArrows:{
                    //    activeColor: '#eb1c24',
                    //    inactiveColor:'#e0e0e0'
                    //  }
                    // }
                  };
                  var chart = new google.visualization.LineChart(document.getElementById('thumbsup'));
                  chart.draw(data, options);  
                }
              }
            });
          }
          //  }
          //}
        }
      }
    });   
  }
</script>

<script type="text/javascript">
	function getThumbsupValuesCat(valueId=false,columnColor=false,beliefId) {
		
    
    var form_values     = $("#dashboard-report-form").serializeArray();

    var orgId         = form_values[1]['value'];
    var officeId        = form_values[2]['value'];
    var departmentId    = form_values[3]['value'];

    // console.log(orgId);
    // console.log(officeId);
    // console.log(departmentId);
    // console.log(cateId);
    // console.log(columnColor);
    // return false;

    $.ajax({
      type: "POST",
      url: "{{URL::to('admin/getThumbsupValuesCat')}}",
      data: {orgId:orgId,officeId:officeId,departmentId:departmentId,valueId:valueId,beliefId:beliefId,columnColor:columnColor,"_token":'<?php echo csrf_token()?>'},
      success: function(response)
      {
        console.log(response);
        // return false
        if (response.columnColor==null) {
          var lineColors = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
        }else {
          var lineColors = [columnColor];
        }

        // if (response.max>0) {
        //   var maxValue = '';
        // }else{
        //   var maxValue = 100;
        // }
        if (response.maxCount <= 0) {
			var maxCount = 100;
		}
        var data = google.visualization.arrayToDataTable(response.arrMain);
          var options = {
          width: '100%',
          height: '100%',
          // colors: ['#ff454b'],
          colors: lineColors,
          hAxis: { title: '',  titleTextStyle: { color: '#333' } },
          vAxis: {
            minValue: 0,
            viewWindow: {
              // min: 0
              max: maxCount
            }
          },
          // curveType: 'function',
          chartArea: {
            top: 30,
            left: 50,
            // height: '50%',
            width: '100%'
          },
          legend: 'none',
          pointSize: 5
        };
        var chart = new google.visualization.LineChart(document.getElementById('thumbsup'));
        chart.draw(data, options);
      }
    });   
  
  
	}
</script>

<script type="text/javascript">
	function drawTribeometer(containerId) {
		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['tribeometerArray']['arrMain']);?>);
		var options = {
			width: '100%',
			height: '100%',
			colors: ['#000000','#eb1c24','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			// colors: ['#eb1c24','#000'],
			hAxis: {
				titleTextStyle: {
					color: '#333'
				}
			},
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if($resultArray['tribeometerArray']['maxCount'] <= 0)
						max: 100
					@endif
					// min: 0,
					// max: 110
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				// height: '50%',
				width: '100%'
			},
			legend: 'none',
			pointSize: 5
			// legend: { position: 'bottom',
			// 	textStyle: {bold:true},
			// 	pagingTextStyle: {
			// 		color: '#616161'
			// 	},
			// 	scrollArrows:{
			// 		activeColor: '#eb1c24',
			// 		inactiveColor:'#e0e0e0'
			// 	}
			// }
		};
		var chart = new google.visualization.LineChart(document.getElementById(containerId));
		chart.draw(data, options);
	}
</script>

<script type="text/javascript">
	function getTribeometerCat(cateId=false,columnColor=false) {

		var form_values   	= $("#dashboard-report-form").serializeArray();

		var orgId      		= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getTribeometerCat')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,categoryId:cateId,columnColor:columnColor,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				console.log(response.columnColor);
				// return false
				if (response.columnColor==null) {
					var lineColors = ['#000000','#eb1c24','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
				}else {
					var lineColors = [columnColor];
				}
				if (response.maxCount <= 0) {
					var maxCount = 100;
				}
				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					// colors: ['#ff454b'],
					colors: lineColors,
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							// min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
					// legend: { position: 'bottom',
					// 	textStyle: {bold:true},
					// 	pagingTextStyle: {
					// 		color: '#616161'
					// 	},
					// 	scrollArrows:{
					// 		activeColor: '#eb1c24',
					// 		inactiveColor:'#e0e0e0'
					// 	}
					// }
				};
				var chart = new google.visualization.LineChart(document.getElementById('tribeometer'));
				chart.draw(data, options);
			}
		});		
	}
</script>

<script type="text/javascript">
	function drawTeamRole(containerId) {
		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['teamRoleArray']['arrMain']);?>);
      	var options = {
			width: '100%',
			height: '100%',
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			// colors: ['#ff454b'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if($resultArray['teamRoleArray']['maxCount'] <= 0)
						max: 100
					@endif
					// min: 0,
					// max: 100
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				// height: '50%',
				width: '100%'
			},
			legend: 'none',
			pointSize: 5
			// legend: { position: 'bottom',
			// 	textStyle: {bold:true},
			// 	pagingTextStyle: {
			// 		color: '#616161'
			// 	},
			// 	scrollArrows:{
			// 		activeColor: '#eb1c24',
			// 		inactiveColor:'#e0e0e0'
			// 	}
			// }
		};
		var chart = new google.visualization.LineChart(document.getElementById(containerId));
		chart.draw(data, options);
	}
</script>

<script type="text/javascript">
	function getTeamRoleCat(cateId=false,columnColor=false) {

		var form_values   	= $("#dashboard-report-form").serializeArray();

		var orgId      		= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getTeamRoleCat')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,categoryId:cateId,columnColor:columnColor,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				console.log(response.columnColor);
				// return false
				if (response.columnColor==null) {
					var lineColors = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
				}else {
					var lineColors = [columnColor];
				}
				if (response.maxCount <= 0) {
					var maxCount = 100;
				}
				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					// colors: ['#ff454b'],
					colors: lineColors,
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							// min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
					// legend: { position: 'bottom',
					// 	textStyle: {bold:true},
					// 	pagingTextStyle: {
					// 		color: '#616161'
					// 	},
					// 	scrollArrows:{
					// 		activeColor: '#eb1c24',
					// 		inactiveColor:'#e0e0e0'
					// 	}
					// }
				};
				var chart = new google.visualization.LineChart(document.getElementById('teamRole'));
				chart.draw(data, options);
			}
		});		
	}
</script>

<script type="text/javascript">
	function drawMotivation(containerId) {
		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['motivationArray']['arrMain']);?>);
	    var options = {
			width: '100%',
			height: '100%',
			// colors: ['#ff454b'],
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if($resultArray['motivationArray']['maxCount'] <= 0)
						max: 100
					@endif
					// min: 0,
					// max: 100
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				// height: '50%',
				width: '100%'
			},
			legend: 'none',
			pointSize: 5
			// legend: { position: 'bottom',
			// 	textStyle: {bold:true},
			// 	pagingTextStyle: {
			// 		color: '#616161'
			// 	},
			// 	scrollArrows:{
			// 		activeColor: '#eb1c24',
			// 		inactiveColor:'#e0e0e0'
			// 	}
			// }
		};
		var chart = new google.visualization.LineChart(document.getElementById(containerId));
		chart.draw(data, options);
	}
</script>

<script type="text/javascript">
	function getMotivationCat(cateId=false,columnColor=false) {

		var form_values   	= $("#dashboard-report-form").serializeArray();

		var orgId      		= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getMotivationCat')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,categoryId:cateId,columnColor:columnColor,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				console.log(response.columnColor);
				// return false
				if (response.columnColor==null) {
					var lineColors = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
				}else {
					var lineColors = [columnColor];
				}
				if (response.maxCount <= 0) {
					var maxCount = 100;
				}
				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					// colors: ['#ff454b'],
					colors: lineColors,
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							// min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
					// legend: { position: 'bottom',
					// 	textStyle: {bold:true},
					// 	pagingTextStyle: {
					// 		color: '#616161'
					// 	},
					// 	scrollArrows:{
					// 		activeColor: '#eb1c24',
					// 		inactiveColor:'#e0e0e0'
					// 	}
					// }
				};
				var chart = new google.visualization.LineChart(document.getElementById('motivation'));
				chart.draw(data, options);
			}
		});		
	}
</script>

<script type="text/javascript">
	function drawPersonalityType(containerId) {
		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['personalityTypeArray']);?>);
	    var options = {
			width: '100%',
			height: '100%',
			// colors: ['#ff454b'],
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			hAxis: { title: '',  titleTextStyle: { color: '#333' } },
			vAxis: {
				minValue: 0,
				viewWindow: {
					min: 0,
					max: 100
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				// height: '50%',
				width: '100%'
			},
			legend: 'none',
			pointSize: 5
			// legend: { position: 'bottom',
			// 	textStyle: {bold:true},
			// }
		};
		var chart = new google.visualization.LineChart(document.getElementById(containerId));
		chart.draw(data, options);
	}
</script>

<script type="text/javascript">
	function getPerTypeCat(cateId=false,columnColor=false) {

		var form_values   	= $("#dashboard-report-form").serializeArray();

		var orgId      		= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getPerTypeCat')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,categoryId:cateId,columnColor:columnColor,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				console.log(response.columnColor);
				// return false
				if (response.columnColor==null) {
					var lineColors = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
				}else {
					var lineColors = [columnColor];
				}
				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					// colors: ['#ff454b'],
					colors: lineColors,
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							min: 0,
							max: 100
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
				};
				var chart = new google.visualization.LineChart(document.getElementById('personalityType'));
				chart.draw(data, options);
			}
		});		
	
	}
</script>

<!-- <script type="text/javascript">
	@if (!empty($resultArray['personalityTypeArray']['pTConditionArray']))
		var canvas = document.getElementById("personalityType");
		var ctx = canvas.getContext("2d");
		var myChart = new Chart(ctx, {
			type: 'radar',
			data: {
				labels: [@foreach($resultArray['personalityTypeArray']['personalityTypeFinalArray'] as $value)"{{$value['keys']}}",@endforeach],
				datasets: [{
					label: 'Percentage',
					data: [@foreach($resultArray['personalityTypeArray']['personalityTypeFinalArray'] as $value1){{$value1['values']}},@endforeach],
					backgroundColor: '#eb1c24',
					borderColor: '#eb1c24',
					//borderWidth: 5
				}]
			},
			options: {			
				legend: { display: false},	
			}
		});
	@else 
		$('#ptError').css('display','block');
	@endif
</script> -->

<script type="text/javascript">
	function drawCultureStructure(containerId) {
		var data = google.visualization.arrayToDataTable(<?php echo json_encode($resultArray['cultureStructureArray']['arrMain']);?>);
		var options = {
			width: '100%',
			height: '100%',
			// colors: ['#eb1c24','#ffb4b7','#000','#939598'],
			colors: ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'],
			hAxis: {
				titleTextStyle: {
					color: '#333'
				}
			},
			vAxis: {
				minValue: 0,
				viewWindow: {
					@if($resultArray['cultureStructureArray']['maxCount'] <= 0)
						max: 100
					@endif
					// min: 0,
					// max: 100
				}
			},
			// curveType: 'function',
			chartArea: {
				top: 30,
				left: 50,
				// height: '50%',
				width: '100%'
			},
			legend: 'none',
			pointSize: 5
			// legend: { position: 'bottom',
			// 	textStyle: {bold:true},
			// 	pagingTextStyle: {
			// 		color: '#616161'
			// 	},
			// 	scrollArrows:{
			// 		activeColor: '#eb1c24',
			// 		inactiveColor:'#e0e0e0'
			// 	}
			// }
		};
		var chart = new google.visualization.LineChart(document.getElementById(containerId));
		chart.draw(data, options);
	}
</script>

<script type="text/javascript">
	function getCultureStructureCat(cateId=false,columnColor=false) {

		var form_values   	= $("#dashboard-report-form").serializeArray();

		var orgId      		= form_values[1]['value'];
		var officeId      	= form_values[2]['value'];
		var departmentId  	= form_values[3]['value'];

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/getCultureStructureCat')}}",
			data: {orgId:orgId,officeId:officeId,departmentId:departmentId,categoryId:cateId,columnColor:columnColor,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				console.log(response.columnColor);
				// return false
				if (response.columnColor==null) {
					var lineColors = ['#000000','#ffcc9e','#f61d29','#3b3c37','#400c04','#ffd5d6','#f32063','#f6f5de','#f77028','#4c5327'];
				}else {
					var lineColors = [columnColor];
				}
				if (response.maxCount <= 0) {
					var maxCount = 100;
				}
				var data = google.visualization.arrayToDataTable(response.arrMain);
			    var options = {
					width: '100%',
					height: '100%',
					// colors: ['#ff454b'],
					colors: lineColors,
					hAxis: { title: '',  titleTextStyle: { color: '#333' } },
					vAxis: {
						minValue: 0,
						viewWindow: {
							// min: 0,
							max: maxCount
						}
					},
					// curveType: 'function',
					chartArea: {
						top: 30,
						left: 50,
						// height: '50%',
						width: '100%'
					},
					legend: 'none',
					pointSize: 5
					// legend: { position: 'bottom',
					// 	textStyle: {bold:true},
					// 	pagingTextStyle: {
					// 		color: '#616161'
					// 	},
					// 	scrollArrows:{
					// 		activeColor: '#eb1c24',
					// 		inactiveColor:'#e0e0e0'
					// 	}
					// }
				};
				var chart = new google.visualization.LineChart(document.getElementById('cultureStructure'));
				chart.draw(data, options);
			}
		});		
	}
</script>

<!-- <script type="text/javascript">
	@if (!empty($resultArray['cultureStructureArray']['CsConditionArray']))
		var canvas = document.getElementById("cultureStructure");
		var ctx = canvas.getContext("2d");
		var myChart = new Chart(ctx, {
			type: 'radar',
			data: {
				labels: [@foreach($resultArray['cultureStructureArray']['sotFinalArray'] as $value)"{{$value['title']}}",@endforeach],
				datasets: [{
					label: 'Percentage',
					data: [@foreach($resultArray['cultureStructureArray']['sotFinalArray'] as $value1){{$value1['percentage']}},@endforeach],
					backgroundColor: '#eb1c24',
					borderColor: '#eb1c24',
					//borderWidth: 5
				}]
			},
			options: {			
				legend: { display: false},	
			}
		});
	@else
		$('#csError').css('display','block');
	@endif
</script> -->


@include('layouts.adminFooter')


