<!-- header -->
@section('title', 'Edit Organisation')
@include('layouts.adminHeader')

<style>
.adit-btn button.savebtn {
    padding: 6px 40px;
}
.adit-btn .deleteOrg-btn{
	background-color: #fff;
    color: #eb1c24;
    border: 1px solid #eb1c24;
    padding: 5px 15px;
}
.adit-btn .deleteOrg-btn:hover,	.adit-btn .deleteOrg-btn:focus {
	background-color: #eb1c24;
	color: #fff; 
}
.sa-button-container > .cancel {
   background-color: #fff;
   color: #000;

}

.select-img-box .btn {

background-color: #eb1c24;
border-color: #eb1c24;

}
.select-img-box #upload {

    padding: 0;
    border: none;
    overflow: hidden;

}
.select-img-box .btn:hover {
	background-color: #000;
	border-color: #000;
}
.select-img-box .btn-success.focus, 
.select-img-box .btn-success:focus,
.select-img-box .btn-success:not(:disabled):not(.disabled).active, 
.select-img-box .btn-success:not(:disabled):not(.disabled):active, .show > .btn-success.dropdown-toggle {
    color: #fff;
    background-color: #000;
    border-color: #000 !important;
    box-shadow: none;
}


.modal-uplaod-content .modal-dialog {
    max-width: 900px;
}

.modal-uplaod-content div#upload-demo {
    padding-left: 0;
}
.modal-uplaod-content .add-logo-section {
    margin-left: 0;
}
.modal-uplaod-content strong {
    margin-bottom: 7px;
    display: block;
}
.croppie-container .cr-viewport {
	border: dashed 1px #eb1c24 !important;

}
</style>


<script src="<?php echo url('/public/croper/croppie.js'); ?>"></script>
<!--<link rel="stylesheet" href="<?php //echo url('/public/croper/bootstrap-3.min.css'); ?>">-->
<link rel="stylesheet" href="<?php echo url('/public/croper/croppie.css'); ?>">

<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="loader-img" style="display: none;width: 70px; top: -15px;"><img src="{{asset('public/images/loader.gif')}}"></div>
				<form id="org_form" method="post" enctype="multipart/form-data">
					{{csrf_field()}}

					<input id="org_logo" name="org_logo" type='hidden' value="{{$record->ImageURL}}" />

					<input type="hidden" name="orgId" value="{{$record->id}}">
					<div class="row">
						<div class="col-md-12">
							<h2> Organisation </h2>
							<div class="del-chng-cont">

								<!-- <div class="rgist-compy-logo"> 
									@if(!empty($record->ImageURL))
									<img id="blah" src="{{asset('public/uploads/org_images').'/'.$record->ImageURL}}" alt="Preview Image" class="mCS_img_loaded">
									<img id="blah" src="img" alt="Preview Image" class="mCS_img_loaded">						
									@else 
									<img id="blah" src="{{ asset('public/images/no-image.png')}}" alt="Preview Image" class="mCS_img_loaded">
									@endif
								</div> -->
								
								<div class="edit-profile-section" onclick="addOrgLogoPopUp();">
									@if(!empty($record->ImageURL))
									<div class="edit-profile-img">
										<img id="blah" src="{{asset('public/uploads/org_images').'/'.$record->ImageURL}}" alt="logo preview" class="mCS_img_loaded">
										<img id="blah" src="img" alt="Preview Image" class="mCS_img_loaded hide-img">	
									</div> 
									@else 
									<div class="edit-profile-img">
										<img id="blah" src="{{ asset('public/images/no-image.png')}}" alt="Preview Image" class="mCS_img_loaded">
										<img id="blah" src="img" alt="Preview Image" class="mCS_img_loaded hide-img">
									</div> 
									@endif
									<div class="edit-profile-btn">	
										<div id="form1" runat="server">
											<!--<input type="file" id="imgInp" name="org_images">-->
											<i class="fa fa-pencil" aria-hidden="true"></i>
										</div>
									</div>
								</div>				


							 	<!-- Modal -->
								<div class="modal fade modal-uplaod-content" id="addOrgLogo" role="dialog">
								<div class="modal-dialog">

								  <!-- Modal content-->
								  <div class="modal-content">
								    <div class="modal-header">
								    	<h2>Add Logo</h2>
								      <button type="button" class="close" data-dismiss="modal">&times;</button>
								    </div>
								    <div class="modal-body">
								      <p>
										<div class="add-logo-section" >
										<div class="container">
										<div class="panel panel-default">
										  <div class="panel-body">
										  	<div class="row">
										  		<div class="col-md-6 text-center">
													<div id="upload-demo" ></div>
										  		</div>
										  		<div class="col-md-6 select-img-box" style="padding-top:30px;">
													<strong>Select Image:</strong>
													
													<input type="file" id="upload">
													<br/>
													<a href="javascript:void(0);" class="btn btn-success upload-result">Upload Image</a>
										  		</div>

										  		<!--
										  		<div class="col-md-4 " style="">
													<div id="upload-demo-i" style="background:#e1e1e1;width:300px;padding:30px;height:300px;margin-top:30px">
													</div>
										  		</div>
												-->

										  	</div>
										  </div>
										  </div>
										</div>
									</div>
								</p>
						    </div>
						  </div>  
							</div>
						</div>


								<div class="chng-dlt-btn"></div>							

								</div>	

						</div>
					</div>

					<div class="row">
						<div class="col-md-3">
							<label>Name of Organisation </label>
						</div>
						<div class="col-md-9">
							<input class="checkOrgCount" id="org_name" name="org_name" type="text" value="{{ ucfirst($record->organisation)}}" placeholder="Organisation" maxlength="150">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label> Industry </label>
						</div>
						<div class="col-md-9">
							<input class="charCount" id="org_industry" name="org_industry" type="text" value="{{ucfirst($record->industry)}}" placeholder="Industry" maxlength="50">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label>Turn Over </label>
						</div>
						<div class="col-md-9">
							<input class="charCount numberControl" id="org_turnover" name="org_turnover" type="text" value="{{$record->turnover}}" placeholder="Turnover" maxlength="50">
						</div>
					</div>

					<!-- <div class="row">
						<div class="col-md-3">
							<label>Include weekends in statistics</label>
						</div>
						<div class="col-md-9 includeWeek">
							 <input type="checkbox" name="org_include_weekend" id="org_include_weekend" value="1" <?php if($record->include_weekend==1){ echo "checked"; } ?>  />
						</div>
					</div> -->

					<div class="adit-btn">
						<button type="button" class="deleteOrg-btn" onclick="deleteOraganisation('{{$record->id}}');"> Delete Organisation </button>	
						<button class="savebtn" type="button"> Save </button>
					</div>
					
				</form>

				<!-- call delect function -->
				<form id="delete-form" action="{{ URL::to('admin/organisation-delete') }}" style="display: none;" method="post">
					{{csrf_field()}}
					<input type="hidden" name="orgId" value="{{$record->id}}">
				</form>

			</div>
			<div class="ragistration-section">
				<h2> Contact </h2>
				<form id="org_form1">
					<div class="row">
						<div class="col-md-3">
							<label>Address 1 </label>
						</div>
						<div class="col-md-9">
							<input class="checkOrgCount" type="text" value="{{ucfirst($record->address1)}}" placeholder="Address1" name="org_address1" id="org_address1" maxlength="150">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label> Address 2 </label>
						</div>
						<div class="col-md-9">
							<input class="checkOrgCount" type="text" value="{{ucfirst($record->address2)}}" placeholder="Address2" name="org_address2" id="org_address2" maxlength="150">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label> Phone </label>
						</div>
						<div class="col-md-9">
							<input class="charCount numberControl" type="text" value="{{$record->phone}}" placeholder="Phone" id="org_phone" name="org_phone" maxlength="20">
							<!-- 	<input type="text" placeholder="+44 (0) 1325 734 845"> -->
						</div>
					</div>
					<!-- <div class="row">
						<div class="col-md-3">
							<label>E-mail</label>
						</div>
						<div class="col-md-9">
							<input class="charCount" type="text" value="{{!empty($user_record->email)?$user_record->email:''}}" placeholder="E-mail" id="org_email" name="org_email" maxlength="50">
						</div>
					</div> -->
					<div class="adit-btn">
						<button class="savebtn1" type="button" > Save </button>
					</div>
					
				</form>
			</div>

			<div class="ragistration-section">
				<h2> Lead Contact Person </h2>
				<form id="org_form4">
					<div class="row">
						<div class="col-md-3">
							<label>Lead Name </label>
						</div>
						<div class="col-md-9">
							
							<input class="charCount" type="text" name="lead_name" id="lead_name" maxlength="50" value="{{!empty($record->lead_name)?$record->lead_name:''}}">
							
							
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label>Lead Email </label>
						</div>
						<div class="col-md-9">
							
							<input  id="lead_email" type="text"   name="lead_email" maxlength="100" value="{{!empty($record->lead_email)?$record->lead_email:''}}">
						</div>
					</div>

					<div class="row">
						<div class="col-md-3">
							<label>Lead Phone </label>
						</div>
						<div class="col-md-9">
							
							<input class="charCount numberControl" id="lead_phone" type="text"  name="lead_phone" maxlength="15" value="{{!empty($record->lead_phone)?$record->lead_phone:''}}">
						</div>
					</div>
					<div class="adit-btn">
						<button type="button" class="savebtn4"> Save </button>
					</div>
				</form>
			</div> 
			<!-- <div class="ragistration-section">
				<h2> Admin User </h2>
				<form id="org_form2">
					<div class="row">
						<div class="col-md-3">
							<label>Name </label>
						</div>
						<div class="col-md-9">
							<div class="password-cont">
								<input class="charCount" type="text" value="{{!empty($user_record->name)?$user_record->name:''}}" name="client_name" id="client_name" maxlength="50">
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label>Password </label>
						</div>
						<div class="col-md-9">
							<div class="password-cont">
								<?php
								if(!empty($user_record->password2))
								{
									?>
									<input class="charCount" id="password-field" type="password" value="{{base64_decode($user_record->password2)}}" placeholder="........" name="client_password" id="client_password" maxlength="50">
									<?php
								}
								?>


								<span toggle="#password-field" class="field-icon toggle-password eay-icon"><img src="{{asset('public/images/eay-icon.png')}}"></span>


							</div>
						</div>
					</div>
					<div class="adit-btn">
						<button type="button" class="savebtn2"> Save </button>
					</div>
				</form>
			</div> -->

			@foreach($offices as $office_values)
			<div class="ragistration-section">
				<h2> Office </h2>
				<form id="org_form_ofc_{{$office_values->id}}">
					<div class="row">
						<div class="col-md-3">
							<label>Office Name </label>
						</div>
						<div class="col-md-9">
							<input type="text" value="{{ucfirst($office_values->office)}}" placeholder="Office" name="office_name" class="validate charCount" maxlength="50">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label> Number of Employees </label>
						</div>
						<div class="col-md-9">
							<input type="text" value="{{ucfirst($office_values->numberOfEmployees)}}" placeholder="Number of Employees" name="no_employee" class="validate charCount numberControl" maxlength="150">
						</div>
					</div>


					<div class="row">
						<div class="col-md-3">
							<label> Address</label>
						</div>
						<div class="col-md-9">
							<input type="text" value="{{$office_values->address}}" placeholder="Address" name="address" class="validate checkOrgCount" maxlength="150">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label> City </label>
						</div>
						<div class="col-md-9">
							<input type="text" value="{{$office_values->city}}" placeholder="City" name="city" class="validate charCount" maxlength="50">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label> Country </label>
						</div>
						<div class="col-md-9">
							<!-- <input type="text" value="{{$office_values->country}}" placeholder="Country" name="country" class="validate charCount" maxlength="50"> -->
							<select name="country" id="country" class="validate charCount" style="background:url(http://chetaru.gottadesigner.com/tribe365/public/images/down-arrow.png)no-repeat 95% center;">
								<option value="">Country</option>
								@foreach($country as $coun)
								<option value="{{$coun->id}}" <?php echo ($office_values->country == $coun->id)?"selected":"" ?>>{{ucfirst($coun->nicename)}}</option>
								@endforeach					
							</select>
						</div>
					</div>


					<div class="row">
						<div class="col-md-3">
							<label> Phone </label>
						</div>
						<div class="col-md-9">
							<input type="text" value="{{$office_values->phone}}" placeholder="Phone" name="phone" class="validate numberControl charCount" maxlength="20">
						</div>
					</div>

					<div class="adit-btn">
						<button type="button" onclick="updateForm('{{$office_values->id}}');"> Save </button>
					</div>
				</form>
			</div>
			@endforeach
			
		</div>
	</div>
	<div class="error-message success" style="display: none;">
		<span id="resp"></span>
	</div>
</main>

<script>
	$(".toggle-password").click(function() {
		$(this).toggleClass("field-icon");
		var input = $($(this).attr("toggle"));
		if (input.attr("type") == "password") {
			input.attr("type", "text");
		} else {
			input.attr("type", "password");
		}
	});
</script>

<script type="text/javascript">
	
	$(".numberControl").on("keypress keyup blur",function (event) {    
		$(this).val($(this).val().replace(/[^\d].+/, ""));
		if ((event.which < 48 || event.which > 57)) {
			event.preventDefault();
		}
	});

	$(".savebtn").click(function(e){
		$('.error-message').show();

		var org_name     = $('#org_name').val();
		var org_industry = $('#org_industry').val();
		var org_turnover = $('#org_turnover').val();
		var org_include_weekend = $('#org_include_weekend').val();		

		
		var empty = false;
		$('.charCount').each(function() {
			if ($(this).val().length > 50){
				empty = true;
			}
		});

		var checkOrgCount = false;
		$('.checkOrgCount').each(function() {
			if ($(this).val().length > 150){
				checkOrgCount = true;
			}
		});

		if (checkOrgCount){

			$('#resp').html('');
			$('#resp').html('All input fields should be less than 150 characters.');
		}else if (empty){

			$('#resp').html('');
			$('#resp').html('All input fields should be less than 50 characters.');
		} else if(org_name ==""){

			$('#resp').html('');
			$('#resp').html('Please enter Organisation.');
		} else if(org_industry ==""){

			$('#resp').html('');
			$('#resp').html('Please enter Industry.');
		} else if(org_turnover ==""){

			$('#resp').html('');
			$('#resp').html('Please enter Turnover.');
		} else {
			$('.loader-img').show();
			console.log('submit')
			e.preventDefault();
			$.ajax({
				url: "{{route('organisation-update')}}",
				type: "POST",
				data:  new FormData($("#org_form")[0]),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					location.reload()
					$('.loader-img').hide();
					/*$('#resp').html('');
					$('#resp').html('Organisation updated successfully.');*/
				}    
			});
		}

	});


	$(".savebtn1").click(function(){
		$('.error-message').show();

		var org_address1  = $('#org_address1').val();
		var org_address2  = $('#org_address2').val();
		var org_phone     = $('#org_phone').val();
		//var org_email     = $('#org_email').val();

		var empty = false;
		$('.charCount').each(function() {
			if ($(this).val().length > 50){
				empty = true;
			}
		});

		var checkOrgCount = false;
		$('.checkOrgCount').each(function() {
			if ($(this).val().length > 150){
				checkOrgCount = true;
			}
		});

		if (checkOrgCount)
		{
			$('#resp').html('');
			$('#resp').html('All input fields should be less than 150 characters.');
		}
		else if (empty)
		{
			$('#resp').html('');
			$('#resp').html('All input fields should be less than 50 characters.');
		} 
		else if(org_address1 =="")
		{
			$('#resp').html('');
			$('#resp').html('Please enter Address Line 1.');
		} 
		// else if(org_address2 ==""){

		// 	$('#resp').html('');
		// 	$('#resp').html('Please enter address.');
		// }
		else if(org_phone =="")
		{
			$('#resp').html('');
			$('#resp').html('Please enter Phone Number.');
		} 
		// else if(org_email =="")
		// {
		// 	$('#resp').html('');
		// 	$('#resp').html('Please enter Email Address.');
		// }
		else 
		{
			var form_values = $("#org_form1").serializeArray();

			form_values.push({name: "_token", value: "<?php echo csrf_token()?>"},
				{name:"orgId", value:<?php echo $record->id ?>});

			$.ajax({
				type: "POST",
				url: "{{route('organisation-update1')}}",
				data: form_values,
				success: function(response) {
					// if(response=="SUCCESS")
					// {
						$('#resp').html('');
						$('#resp').html('Contact Detail updated successfully.');
					// }
				}
			});
		}

	});


	$(".savebtn4").click(function(){
		$('.error-message').show();

		var lead_name     = $('#lead_name').val();
		var lead_email = $('#lead_email').val();
		var lead_phone = $('#lead_phone').val();

		var empty = false;
		$('.charCount').each(function() {
			if ($(this).val().length > 50){
				empty = true;
			}
		});
		
		var check_email = isEmail(lead_email);

		if (empty){

			$('#resp').html('');
			$('#resp').html('All input fields should be less than 50 characters.');
		} else	if(lead_name ==""){

			$('#resp').html('');
			$('#resp').html('Please enter Lead Name.');
		} else if(lead_email =="" || check_email==false){

			$('#resp').html('');
			$('#resp').html('Please enter Lead Email.');
		}else if(lead_phone ==""){

			$('#resp').html('');
			$('#resp').html('Please enter Lead Phone.');
		} else {

			var form_values = $("#org_form4").serializeArray();

			form_values.push({name: "_token", value: "<?php echo csrf_token()?>"},
				{name:"orgId", value:<?php echo $record->id ?>});

			$.ajax({
				type: "POST",
				url: "{{route('organisation-update4')}}",
				data: form_values,
				success: function(response) {
					// if(response=="SUCCESS")
					// {
						$('#resp').html('');
						$('#resp').html('Lead Details updated successfully.');
					// }
				}
			});
		}

	});


	// $(".savebtn2").click(function(){
	// 	$('.error-message').show();

	// 	var client_name     = $('#client_name').val();
	// 	var client_password = $('#client_password').val();

	// 	var empty = false;
	// 	$('.charCount').each(function() {
	// 		if ($(this).val().length > 50){
	// 			empty = true;
	// 		}
	// 	});
	
	// 	var checkOrgCount = false;
	// 	$('.checkOrgCount').each(function() {
	// 		if ($(this).val().length > 150){
	// 			checkOrgCount = true;
	// 		}
	// 	});

	// 	if (checkOrgCount){

	// 		$('#resp').html('');
	// 		$('#resp').html('All input fields should be less than 150 characters.');
	// 	}else if (empty){

	// 		$('#resp').html('');
	// 		$('#resp').html('All input fields should be less than 50 characters.');
	// 	} else	if(client_name ==""){

	// 		$('#resp').html('');
	// 		$('#resp').html('Please enter User Name.');
	// 	} else if(client_password ==""){

	// 		$('#resp').html('');
	// 		$('#resp').html('Please enter User Password.');
	// 	} else {

	// 		var form_values = $("#org_form2").serializeArray();

	// 		form_values.push({name: "_token", value: "<?php echo csrf_token()?>"},
	// 			{name:"orgId", value:<?php echo $record->id ?>});

	// 		$.ajax({
	// 			type: "POST",
	// 			url: "{{route('organisation-update2')}}",
	// 			data: form_values,
	// 			success: function(response) {
	// 				// if(response=="SUCCESS")
	// 				// {
	// 					$('#resp').html('');
	// 					$('#resp').html('User Detail updated successfully.');
	// 				// }
	// 			}
	// 		});
	// 	}

	// });

	function updateForm(id)
	{
		$('.error-message').show();

		var empty = false;
		$('.validate').each(function() {
			if ($(this).val().length == 0) {
				empty = true;
			}
		});

		if (empty) {

			$('#resp').html('');
			$('#resp').html('All the fields are mandatory.');


		} else {
			var form_values = $("#org_form_ofc_"+id).serializeArray();

			form_values.push({name: "_token", value: "<?php echo csrf_token()?>"},
				{name:"ofcId", value:id});

			$.ajax({
				type: "POST",
				url: "{{route('organisation-update3')}}",
				data: form_values,
				success: function(response) {
				// if(response=="SUCCESS")
				// {
					$('#resp').html('');
					$('#resp').html('Office is updated successfully.');
				// }
			}
		});
		}

		
	}
</script>
<script type="text/javascript">

	function isEmail(email)
	{
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	}

</script>
<script type="text/javascript">
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			
			reader.onload = function (e) {
				$('#blah').attr('src', e.target.result);
			}
			
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	$("#imgInp").change(function(){
		readURL(this);
	});
</script>

<script>
	function deleteOraganisation(orgId) {
		swal({
			title: "Are you sure",
			text: "You want to delete this Organisation?",
			type: null,
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes",
			closeOnConfirm: false,
			cancelButtonText: "No"
		},
		function(){
			//swal("Deleted!", "Your imaginary file has been deleted.", "success");
			$.ajax({
				url: "{{URL::to('admin/delete-oragnisation')}}",
				type: "POST",
				dataType: "JSON",
				data: {orgId:orgId,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					//swal("Deleted!", "Your imaginary file has been deleted.", "success");
					if (response.status == 200) {
						location.href = "{{URL::to('admin/admin-organisation')}}";
					}
				}
			});	
		});
	}
</script>

<script type="text/javascript">
$uploadCrop = $('#upload-demo').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        //type: 'circle',
        type: 'square'
    },
    boundary: {
        width: 300,
        height: 300
    }
});

$('#upload').on('change', function () { 
	var reader = new FileReader();
    reader.onload = function (e) {
    	$uploadCrop.croppie('bind', {
    		url: e.target.result
    	}).then(function(){
    		console.log('jQuery bind complete');
    	});
    	
    }
    reader.readAsDataURL(this.files[0]);
});


$('.upload-result').on('click', function (ev) {
	$uploadCrop.croppie('result', {
		type: 'canvas',
		size: 'viewport'
	}).then(function (resp) {


		$.ajax({
			url: "{{URL::to('admin/cropOrganizationImg')}}",
			type: "POST",
			data: {"image":resp,"_token":'<?php echo csrf_token()?>'},
			success: function (data) {
				$("#org_logo").val(data);

				html = '<img src="' + resp + '" />';
				//$("#upload-demo-i").html(html);
				$(".edit-profile-img").html(html);	
				$(".edit-profile-img").show();		
				$('#addOrgLogo').modal('toggle');		
			}
		});
	});
});
</script>
<script type="text/javascript">
function addOrgLogoPopUp(){
	$('#addOrgLogo').modal('toggle');
}
</script>

@include('layouts.adminFooter')