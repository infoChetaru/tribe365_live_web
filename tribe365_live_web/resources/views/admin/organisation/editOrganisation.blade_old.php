<!-- header -->
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">

				<form id="org_form" method="post" enctype="multipart/form-data">

					<div class="row">
						<div class="col-md-12">
							<h2> Organisation </h2>
							<div class="del-chng-cont">
								<div class="rgist-compy-logo"> <img src="{{asset('public/uploads/org_images').'/'.$record->ImageURL}}"></div>
								<div class="chng-dlt-btn">
									<button class="change"> Change </button>
									<button class="dlt"> delete </button>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-3">
							<label>Name of Organisation </label>
						</div>
						<div class="col-md-9">
							<input id="org_name" name="org_name" type="text" value="{{ $record->organisation}}" placeholder="Organisation">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label> Industry </label>
						</div>
						<div class="col-md-9">
							<input id="org_industry" name="org_industry" type="text" value="{{$record->industry}}" placeholder="Industry">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label>Turn Over </label>
						</div>
						<div class="col-md-9">
							<input id="org_turnover" class="numberControl" name="org_turnover" type="text" value="{{$record->turnover}}" placeholder="Turnover">
						</div>
					</div>
					<div class="adit-btn">
						<button class="savebtn" type="button"> Edit </button>
						<div class="error-message">
							<span id="resp">hi</span>
						</div>
					</div>
					
				</form>

			</div>
			<div class="ragistration-section">
				<h2> Contact </h2>
				<form>
					<div class="row">
						<div class="col-md-3">
							<label>Address 1 </label>
						</div>
						<div class="col-md-9">
							<input type="text" value="{{ucfirst($record->address1)}}" placeholder="Address1">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label> Address 2 </label>
						</div>
						<div class="col-md-9">
							<input type="text" value="{{ucfirst($record->address2)}}" placeholder="Address2">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label> Phone </label>
						</div>
						<div class="col-md-9">
							<input type="text" value="{{$record->phone}}" placeholder="Phone">
							<input type="text" placeholder="+44 (0) 1325 734 845">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label>E-mail</label>
						</div>
						<div class="col-md-9">
							<input type="text" value="{{$user_record->email}}" placeholder="E-mail">
						</div>
					</div>
					<div class="adit-btn">
						<button type="text"> Edit </button>
					</div>
				</form>
			</div>
			<div class="ragistration-section">
				<h2> User </h2>
				<form>
					<div class="row">
						<div class="col-md-3">
							<label>Password </label>
						</div>
						<div class="col-md-9">
							<div class="password-cont">
								<input id="password-field" type="password" value="{{base64_decode($user_record->password2)}}" placeholder="........">
								<span toggle="#password-field" class="field-icon toggle-password eay-icon"><img src="{{asset('public/images/eay-icon.png')}}"></span>
							</div>
						</div>
					</div>
					<div class="adit-btn">
						<button type="text"> Edit </button>
					</div>
				</form>
			</div>

			@foreach($offices as $office_values)
			<div class="ragistration-section">
				<h2> Office </h2>
				<form>
					<div class="row">
						<div class="col-md-3">
							<label>Office Name </label>
						</div>
						<div class="col-md-9">
							<input type="text" value="{{ucfirst($office_values->office)}}" placeholder="Office">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label> Number of Employees </label>
						</div>
						<div class="col-md-9">
							<input type="text" value="{{ucfirst($office_values->numberOfEmployees)}}" placeholder="Number of Employees">
						</div>
					</div>

					<!-- dept detail -->
					@php
					$departments = DB::table('departments')
					->where('officeId',$office_values->id)
					->where('status','Active')
					->get()
					@endphp

					@foreach($departments as $department_values)
					<div class="row">
						<div class="col-md-3">
							<label>Department Name </label>
						</div>
						<div class="col-md-9">
							<input type="text" value="{{ucfirst($department_values->department)}}" placeholder="Department Name">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label>Number of Employees</label>
						</div>
						<div class="col-md-9">
							<input type="text" value="{{$department_values->numberOfEmployees}}" placeholder="Number of Employees">
						</div>
					</div>
					@endforeach
					<div class="adit-btn">
						<button type="text"> Edit </button>
					</div>
				</form>
			</div>
			@endforeach

		</div>
	</div>
</main>

<script>
	$(".toggle-password").click(function() {
		$(this).toggleClass("field-icon");
		var input = $($(this).attr("toggle"));
		if (input.attr("type") == "password") {
			input.attr("type", "text");
		} else {
			input.attr("type", "password");
		}
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){

		$(".savebtn").click(function(){

			var org_name     = $('#org_name').val();
			var org_industry = $('#org_industry').val();
			var org_turnover = $('#org_turnover').val();

			if(org_name ==""){

				$('#resp').html('');
				$('#resp').html('Please enter organisation.');
			} else if(org_industry ==""){

				$('#resp').html('');
				$('#resp').html('Please enter industry.');
			} else if(org_turnover ==""){

				$('#resp').html('');
				$('#resp').html('Please enter turnover.');
			} else {

				var form_values = $("#org_form").serializeArray();
				console.log(form_values);

				$.ajax({
					type: "POST",
					url: "{{route('organisation-update')}}",
					data: {form_values,"_token":'<?php echo csrf_token()?>'},
					success: function(response) {
						console.log()
					}
				});
			}

		});
	});

</script>

@include('layouts.adminFooter')