<!-- header -->
@section('title', 'List Organisation')
@include('layouts.adminHeader')
<main class="main-content">
    <div class="add-fild-section organization-fild">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="loader-img" style="display: none;width: 70px; top: -57px;"><img src="{{asset('public/images/loader.gif')}}"></div>
                    <div class="search-cot">
                     <form method="post" action="">
                        <select name="searchByDate" id="searchByDate">
                            <option value="">Select Year</option>
                            <?php if($yearArray){ foreach($yearArray as $yearArrayVal){ ?>
                            <option value="<?php echo $yearArrayVal; ?>"><?php echo $yearArrayVal; ?></option>
                            <?php  } } ?>
                        </select>
                        <input type="hidden" name="orgid" id="orgid" value="<?php echo base64_encode($orgid); ?>">
                       <button type="button" class="dashboard-report-form frmSearch" style="width: auto;">Search</button>
                    </form>                         
                    <div align="center">                            
                        @if(session('message'))
                        <div class="alert alert-success" role="alert">                            
                            {{session('message')}}
                        </div>
                        @endif
                    </div>                        
                </div>
            </div>
        </div>
        <div class="row">
            <div class="organization-section  manage-reports ">
                @if(is_array($organisation) && count($organisation))
                @foreach($organisation as $key => $value)
                <div class="col-md-3">
                <div class="company-section">
                  <div class="company-detale">   
                   
                    <a target="_BLANK" href="{{ asset('public/uploads/pdf_report').'/'.$value->file_name }}">
                         <!--
                       <img src="{{ asset('public/images/pdf-img.jpeg')}}">
                        -->        
                    
                           
                    <h2><?php echo date("F Y", strtotime($value->created_at)); ?></h2>
                    </a>    
                  </div>
                  <div class="company-ovel-text">
                    <span>View Report </span>
                  </div>
                </div>
              </div>     
            @endforeach             
            @else 
            <div style="color: #8c8c8c;padding-left: 20px;">No record found.</div>
            @endif
        </div>
    </div>
</div>

<div class="organ-page-nav" >
    <!-- <ul class="pagination"> -->

        {!! $paginations->links('layouts.pagination') !!}

        <!--     </ul>         -->                           
    </div>
</div>
</main>

<script type="text/javascript">

$(".frmSearch").click(function(){
    $('.organization-section').html('');
    $('.organ-page-nav').html('');
    $('.loader-img').show();

    var searchByDate = $('#searchByDate').val();
    var orgid        = $('#orgid').val();    
    $.ajax({
        type: "POST",
        url: "{{URL('admin/organisation_pdf_search',base64_encode($orgid))}}",             
        data: {orgid:orgid,searchByDate:searchByDate,"_token":'<?php echo csrf_token()?>'},
        success: function(response) {
            if(response.length > 0){
                $('.organization-section').html('');
                $('.organization-section').append(response);
                $('.loader-img').hide();

            }else{

                $('.organization-section').html('');                                 
                $('.organization-section').append('<div style="color: #8c8c8c;padding-left: 20px;">No record found.</div>');
                $('.loader-img').hide();
            }
        }
    }); 

});
</script>
<style type="text/css">
    #searchByDate{
        background-color: white;
    }
</style>
@include('layouts.adminFooter')