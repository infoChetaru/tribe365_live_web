<!-- header -->
@section('title', 'Add Organisation')
@include('layouts.adminHeader')
<style type="text/css">
.select-img-box .btn {

    background-color: #eb1c24;
    border-color: #eb1c24;

}
.select-img-box #upload {

    padding: 0;
    border: none;
    overflow: hidden;

}
.select-img-box .btn:hover {
	background-color: #000;
	border-color: #000;
}
.select-img-box .btn-success.focus, 
.select-img-box .btn-success:focus,
.select-img-box .btn-success:not(:disabled):not(.disabled).active, 
.select-img-box .btn-success:not(:disabled):not(.disabled):active, .show > .btn-success.dropdown-toggle {
    color: #fff;
    background-color: #000;
    border-color: #000 !important;
    box-shadow: none;
}

.modal-uplaod-content .modal-dialog {
    max-width: 900px;
}

.modal-uplaod-content div#upload-demo {
    padding-left: 0;
}
.modal-uplaod-content .add-logo-section {
    margin-left: 0;
}
.modal-uplaod-content strong {
    margin-bottom: 7px;
    display: block;
}
.croppie-container .cr-viewport {
	border: dashed 1px #eb1c24 !important;

}
</style>


<script src="<?php echo url('/public/croper/croppie.js'); ?>"></script>
<!--<link rel="stylesheet" href="<?php //echo url('/public/croper/bootstrap-3.min.css'); ?>">-->
<link rel="stylesheet" href="<?php echo url('/public/croper/croppie.css'); ?>">

<main class="main-content">
	<form id="org_form" action="{!!route('admin-organisation.store')!!}" method="post" enctype="multipart/form-data">

		{{csrf_field()}}
		<input type="hidden" name="office_count" class="office_count" id="count" value="0"/>
		<input type="hidden" name="additional_user_count" class="additional_user_count" id="additional_user_count" value="0" />

		<input id="org_logo" name="org_logo" type='hidden' value="" />

		<div class="add-fild-section">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="fild-stats-cont">
							<h2> Add Organisations </h2>
							<div class="add-content">

								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<input id="org_name" name="org_name" type="text" class="form-control checkOrgCount"  placeholder="Name of Organisation"  maxlength="150">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input id="org_industry" name="org_industry" type="text" class="form-control charCount" placeholder="Industry"  maxlength="50">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input id="org_turnover" name="org_turnover" type="text" class="form-control numberControl charCount" placeholder="Turn Over"  maxlength="50">
										</div>
									</div>

									<!-- <div class="col-md-6">
										<div class="form-group includeWeekAdd">											
											 <input type="checkbox" name="org_include_weekend" id="org_include_weekend" value="1" checked="" />
											 <span>Include weekends in statistics</span>
										</div>
									</div> -->

								</div>

							</div>
							<div class="add-logo-section" onclick="addOrgLogoPopUp();">
								<div class="add-logo-box">
									<img src="{{ asset('public/images/add-icon.png')}}">
									<h4> Add Logo </h4>
									<!--<input id="org_logo" name="org_logo" type='file' value="" />-->
								</div>
								<div class="add-img-box" style="display: none;"> <img src="#" alt="logo preview" /> </div>
							</div>

						 	<!-- Modal -->
							<div class="modal fade modal-uplaod-content" id="addOrgLogo" role="dialog">
							<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<h2>Add Logo</h2>
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>
								<div class="modal-body">
									<p>
										<div class="add-logo-section" >
											<div class="container">
												<div class="panel panel-default">
													<div class="panel-body">
														<div class="row">
															<div class="col-md-6 text-center">
																<div id="upload-demo" ></div>
															</div>
															<div class="col-md-6 select-img-box" style="padding-top:30px;">
																<strong>Select Image:</strong>

																<input type="file" id="upload">
																<br/>
																<a href="javascript:void(0);" class="btn btn-success upload-result">Upload Image</a>
															</div>

															<!--
															<div class="col-md-4 " style="">
															<div id="upload-demo-i" style="background:#e1e1e1;width:300px;padding:30px;height:300px;margin-top:30px">
															</div>
															</div>
															-->
														</div>
													</div>
												</div>
											</div>
										</div>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="fild-stats-cont contact-cont">
							<h2> Contact </h2>
							<div class="add-content">

								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<input id="org_add1" name="org_add1" type="text" class="form-control checkOrgCount"  placeholder="Address1"  maxlength="150">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<input id="org_add2" name="org_add2" type="text" class="form-control checkOrgCount" placeholder="Address2"  maxlength="150">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<input id="org_phone" name="org_phone" type="phone" class="form-control numberControl charCount" placeholder="Phone"  maxlength="20">
										</div>
									</div>
									<!-- <div class="col-md-6">
										<div class="form-group">
											<input id="org_email" name="email" type="email" class="form-control charCount checkEmail" placeholder="E-Mail"  maxlength="50">
										</div>
									</div> -->
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="fild-stats-cont Create-cont">
							<h2>Lead Contact Person </h2>
							<div class="add-content">

								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<input id="lead_name" name="lead_name" type="text" class="form-control charCount"  placeholder="Lead Name"  maxlength="50">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<input id="lead_email" name="lead_email" type="email" class="form-control charCount"  placeholder="Lead Email" maxlength="100">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<input id="lead_phone" name="lead_phone" type="text" class="form-control charCount numberControl" placeholder="Lead Phone " maxlength="50">
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div> 
			<!-- 	<div class="row">
					<div class="col-md-12">
						<div class="fild-stats-cont Create-cont">
							<h2> Create Admin </h2>
							<div class="add-content">

								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<input id="user_name" name="user_name" type="text" class="form-control charCount"  placeholder="Username"  maxlength="50">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<input id="user_password" name="user_password" type="password" class="form-control charCount"  placeholder="Password" maxlength="50">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<input id="user_confirm_password" name="user_confirm_password" type="password" class="form-control charCount" placeholder="Confirm Password " maxlength="50">
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div> -->

				<!-- add office here -->
				<div class="row add-office-row"> </div>

				<!-- add user here -->
				<div class="row add-user-row"></div>


				<div class="add-plus-icon">
					<div class="add-plus-upper">
						<a href="#"><img src="{{ asset('public/images/plus-icon.png')}}"></a>
						<div class="add-section-btn">
							<a href="javascript:void(0);" class="add-office-btn addoffice">
								<span class="office-text"> Add Offices </span>
								<span class="office-icon"> <img src="{{asset('public/images/add-office-icon.png')}}"> </span>
							</a>
							<br>
							<!-- <a href="javascript:void(0);" class="add-user-btn addUserSection">
								<span class="user-text"> Add User </span>
								<span class="user-icon"> <img src="{{asset('public/images/add-user-icon.png')}}"> </span>
							</a> -->
						</div>
					</div>
				</div>

				<div class="Create-btn-cont">								
					<button type="button" onclick="validation();" class="btn save">Add ORGANISATION</button>
				</div>

			</div>
		</div>
	</form>
	<div class="error-message " style="display: none;">
		<span id="resp"></span>
	</div>	
</main>
<!-- footer -->
<script type="text/javascript">

	$(".add-office-row").on("keypress keyup blur",'.numberControl',function (event) {    
		$(this).val($(this).val().replace(/[^\d].+/, ""));
		if ((event.which < 48 || event.which > 57)) {
			event.preventDefault();
		}
	});
</script>
<script type="text/javascript">
// duplicate email in the database
$(".add-office-row").on("focusout",'.checkEmail',function () { 
	var me = this;
	$.ajax({
		type: "POST",
		url: "{!!URL::to('admin/search-email')!!}",				
		data: {email:$(this).val(),"_token":'<?php echo csrf_token()?>'},
		success: function(response) {
			if(response)
			{

				$('.error-message').show();
				$('#resp').html('Email already Exists.');
				$(me).val('');

			}
			else
			{
				$('.error-message').hide();
				$('#resp').html('');


			}
		}
	});

});

// duplicate email on the same page

function findDuplicate()
{
	var textArr=$(".checkEmail").get();

	var len=textArr.length;
	var inner=0,outer=0,index=0,dupLen=0;
	var dupArr=new Array();
	for(outer=0;outer<len;outer++)
	{
		for(inner=outer+1;inner<len;inner++)
		{
			if(textArr[outer].value==textArr[inner].value)
			{
				if(jQuery.inArray( textArr[outer], dupArr )==-1)
				{
					dupArr.push(textArr[outer]);
				}
				if(jQuery.inArray( textArr[inner], dupArr )==-1)
				{
					dupArr.push(textArr[inner]);
				}
			}
		}
	}

	return dupArr.length;
	for(i=0;i< dupArr.length;i++)
	{
		$("#"+dupArr[i].id).addClass("highlight");
	}
}

function validation() {


	var dupEmail = findDuplicate();



	$('.error-message').show();
	var org_name      = $('#org_name').val().trim();
	var org_industry  = $('#org_industry').val().trim();
	var org_turnover  = $('#org_turnover').val().trim();
	var org_add1      = $('#org_add1').val().trim();					
	var org_add2      = $('#org_add2').val().trim();
	//var org_email     = $('#org_email').val().trim();
	var org_phone     = $('#org_phone').val().trim();
	//var user_name     = $('#user_name').val().trim();
	//var user_password = $('#user_password').val().trim();
	//var user_confirm_password= $('#user_confirm_password').val().trim();

	//var org_logo_count = document.getElementById("org_logo").files.length;
	var org_logo_count        = $('#org_logo').val().trim();
	// var org_include_weekend   = $('#org_include_weekend').val().trim();

	//is_email = isEmail(org_email);

	var empty = false;
	$('.charCount').each(function() {
		if ($(this).val().length > 50){
			empty = true;
		}
	});


	var checkOrgCount = false;
	$('.checkOrgCount').each(function() {
		if ($(this).val().length > 150){
			checkOrgCount = true;
		}
	});

	if (checkOrgCount){

		$('#resp').html('');
		$('#resp').html('All input fields should be less than 150 characters.');
	}else if (empty){

		$('#resp').html('');
		$('#resp').html('All input fields should be less than 50 characters.');
	}
	else if(org_name =="")
	{
		$('#resp').html('');
		$('#resp').html('Please enter Organisation Name.');
	}
	else if(org_industry =="")
	{
		$('#resp').html('');
		$('#resp').html('Please enter Industry Type.');
	} 
	else if(org_turnover =="")
	{
		$('#resp').html('');
		$('#resp').html('Please enter Turnover.');
	}
	else if(org_logo_count== 0)
	{
		$('#resp').html('');
		$('#resp').html('Please upload Organisation Logo');
	} 
	else if(org_add1 =="")
	{
		$('#resp').html('');
		$('#resp').html('Please enter Address Line 1.');
	}
	else if(org_add1==org_add2) 
	{
		$('#resp').html('');
		$('#resp').html('Address Line 1 and Line 2 are same! You can leave blank in Line 2');
	} 
	else if(org_phone =="")
	{
		$('#resp').html('');
		$('#resp').html('Please enter Phone Number.');
	} 

	else 
	{
		$('.save').prop("disabled", true);
		var flag = 0;
		$(".office_validation").each(function(index) {

			if($(this).val().trim()==''){
				$('.save').prop("disabled", false);
				$('#resp').html('Please enter valid Office Detail');

				flag++;
			} 

		});

		if(flag == 0) {
			
			var user_flag = 0;
			$(".add_user_validation").each(function(index) {

				if($(this).val().trim()=='') {
					$('.save').prop("disabled", false);
					$('#resp').html('Please enter valid User Detail');

					user_flag++;
				} 

			});

			if (user_flag == 0) {

				
				$.ajax({
					type: "POST",
					url: "{!!URL::to('admin/get-org-name')!!}",				
					data: {org_name:org_name,"_token":'<?php echo csrf_token()?>'},
					success: function(response) {
						console.log('email '+response);

						if(response)
						{
							$('.save').prop("disabled", false);
							$('#resp').html('');
							$('#resp').html('This Organisation Already Exists.');

						}
						else
						{
							if(dupEmail>0)
							{
								$('.save').prop("disabled", false);
								$('#resp').html('');
								$('#resp').html('Duplicate E-mail.');
								alert("Duplicate E-mail.");

								return false;
							}
							else
							{
								console.log('success');
								$('.save').prop("disabled", true);
								$('#org_form').submit();
							}
						}
					}
				});	
			}
		}
	} 
}				
</script>

<script type="text/javascript">

	function isEmail(email){
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	}

</script>

<!-- in add organisation add part-->
<script type="text/javascript">

	var max_fields      = 10;
	var wrapper         = $(".add-office-row"); 				
	var next            = 1;
	var countryOpt = [];
	var country =  <?php echo json_encode($countryList); ?>;

	$.each(country,function(key,value){
		countryOpt.push('<option value="'+value['id']+'">'+ value['nicename'] +'</option>');
	});
	
				// add office on click				
				$('.addoffice').on('click',function(e){					
					e.preventDefault();

					var total_fields = wrapper[0].childNodes.length;

					// $('.add-office-row').append('<div id="field'+next+'" class="col-md-12"><button type="button" style="float:right;" class="remove" data-id="'+next+'">remove</button><div class="fild-stats-cont"><h2> Add Office </h2><div class="add-content"><div class="row"><div class="col-md-6"><div class="form-group"><input name="office_name'+next+'" type="text" class="form-control office_validation charCount" placeholder="Office Name" maxlength="50"></div></div><div class="col-md-6"><div class="form-group"><input name="office_noOf_employee'+next+'" type="text" class="numberControl form-control office_validation charCount" placeholder="Number of Employees"></div></div><div class="col-md-6"><div class="form-group"><input name="office_address'+next+'" type="text" class="form-control office_validation checkOrgCount"  placeholder="Address" maxlength="150"></div></div><div class="col-md-6"> <div class="form-group"><input name="office_city'+next+'" type="text" class="form-control office_validation charCount" placeholder="City" maxlength="50"></div></div><div class="col-md-6"> <div class="form-group"><input name="office_country'+next+'" type="text" class="form-control office_validation charCount" placeholder="Country" maxlength="50"></div></div><div class="col-md-6"> <div class="form-group"><input name="office_phone'+next+'" type="text" class="numberControl form-control office_validation charCount" placeholder="Phone" maxlength="20"></div></div></div><div class="add-content"><div class="row"><div class="col-md-3"><div class="form-group"><input name="user_name'+next+'[]" type="text" class="form-control add_user_validation charCount" placeholder="Name" maxlength="50"></div></div><div class="col-md-3"><div class="form-group"><input name="user_email'+next+'[]" type="email" class="form-control add_user_validation charCount checkEmail" placeholder="Email"></div></div><div class="col-md-3"><div class="form-group"><input type="password" class="form-control add_user_validation charCount" placeholder="Password " name="user_password'+next+'[]"></div></div><div class="col-md-3"><div class="form-group"><select class="form-control add_user_validation addDeptOpt" style="background:url({{asset("public/images/down-arrow.png")}})no-repeat 95% center;"  name="department'+next+'[]" ></select></div></div><div class="createfield addUserHere'+next+'"></div><div class="Add-depart-btn"><button id="add_dept'+next+'" class="addUser'+next+'" type="button"> Add User </button></div></div></div></div></div>');


					$('.add-office-row').append('<div id="field'+next+'" class="col-md-12"><button type="button" style="float:right;" class="remove" data-id="'+next+'">remove</button><div class="fild-stats-cont"><h2> Add Office </h2><div class="add-content"><div class="row"><div class="col-md-6"><div class="form-group"><input name="office_name'+next+'" type="text" class="form-control office_validation charCount" placeholder="Office Name" maxlength="50"></div></div><div class="col-md-6"><div class="form-g	roup"><input name="office_noOf_employee'+next+'" type="text" class="numberControl form-control office_validation charCount" placeholder="Number of Employees"></div></div><div class="col-md-6"><div class="form-group"><input name="office_address'+next+'" type="text" class="form-control office_validation checkOrgCount"  placeholder="Address" maxlength="150"></div></div><div class="col-md-6"> <div class="form-group"><input name="office_city'+next+'" type="text" class="form-control office_validation charCount" placeholder="City" maxlength="50"></div></div><div class="col-md-6"> <div class="form-group">'+
						'<select name="office_country'+next+'" id="office_country'+next+'" class="form-control office_validation charCount" style="background:url(http://chetaru.gottadesigner.com/tribe365/public/images/down-arrow.png)no-repeat 95% center;">'+
							'<option value="">Country</option>'+
							countryOpt+
						'</select>'+
						// '</div></div><div class="col-md-6"> <div class="form-group"><input name="office_phone'+next+'" type="text" class="numberControl form-control office_validation charCount" placeholder="Phone" maxlength="20"></div></div></div><div class="add-content"><div class="row"><div class="col-md-3"><div class="form-group"><input name="user_name'+next+'[]" type="text" class="form-control add_user_validation charCount" placeholder="Name" maxlength="50"></div></div><div class="col-md-3"><div class="form-group"><input name="user_email'+next+'[]" type="email" class="form-control add_user_validation charCount checkEmail" placeholder="Email"></div></div><div class="col-md-3"><div class="form-group"><input type="password" class="form-control add_user_validation charCount" placeholder="Password " name="user_password'+next+'[]"></div></div><div class="col-md-3"><div class="form-group"><select class="form-control add_user_validation addDeptOpt" style="background:url({{asset("public/images/down-arrow.png")}})no-repeat 95% center;"  name="department'+next+'[]" ></select></div></div><div class="createfield addUserHere'+next+'"></div><div class="Add-depart-btn"><button id="add_dept'+next+'" class="addUser'+next+'" type="button"> Add User </button></div></div></div></div></div>');
						'</div></div><div class="col-md-6"> <div class="form-group"><input name="office_phone'+next+'" type="text" class="numberControl form-control office_validation charCount" placeholder="Phone" maxlength="20"></div></div></div><div class="add-content"><div class="row"><div class="col-md-3"><div class="form-group"><input name="user_name'+next+'[]" type="text" class="form-control add_user_validation charCount" placeholder="Name" maxlength="50"></div></div><div class="col-md-3"><div class="form-group"><input name="user_last_name'+next+'[]" type="text" class="form-control add_user_validation charCount" placeholder="Last Name" maxlength="50"></div></div><div class="col-md-3"><div class="form-group"><input name="user_email'+next+'[]" type="email" class="form-control add_user_validation charCount checkEmail" placeholder="Email"></div></div><div class="col-md-3"><div class="form-group"><select class="form-control add_user_validation addDeptOpt" style="background:url({{asset("public/images/down-arrow.png")}})no-repeat 95% center;"  name="department'+next+'[]" ></select></div></div><div class="createfield addUserHere'+next+'"></div><div class="Add-depart-btn"><button id="add_dept'+next+'" class="addUser'+next+'" type="button"> Add User </button></div></div></div></div></div>');


				// add department in office (add organisation)				
				$('.add-office-row').on('click','.addUser'+next,function(e){

					e.preventDefault();

					var fieldNum = this.id.charAt(this.id.length-1);
					var addDeptHere  = ".addUserHere" + fieldNum;

					// $(addDeptHere).append('<div class="add-content"><button type="button" class="delete-user">-</button><div class="row"><div class="col-md-3"><div class="form-group"><input name="user_name'+fieldNum+'[]" type="text" class="form-control add_user_validation charCount" placeholder="Name "></div></div><div class="col-md-3"><div class="form-group"><input name="user_email'+fieldNum+'[]" type="email" class="form-control add_user_validation charCount checkEmail" placeholder="Email "></div></div><div class="col-md-3"><div class="form-group"><input type="password" class="form-control add_user_validation charCount" placeholder=" Password "name="user_password'+fieldNum+'[]" ></div></div><div class="col-md-3"><div class="form-group"><select  class="form-control add_user_validation addDeptOpt" style="background:url({{asset("public/images/down-arrow.png")}})no-repeat 95% center;" name="department'+fieldNum+'[]">'+getCutomDepartments()+'</select></div></div></div></div>');
					$(addDeptHere).append('<div class="add-content"><button type="button" class="delete-user">-</button><div class="row"><div class="col-md-3"><div class="form-group"><input name="user_name'+fieldNum+'[]" type="text" class="form-control add_user_validation charCount" placeholder="Name "></div></div><div class="col-md-3"><div class="form-group"><input name="user_last_name'+fieldNum+'[]" type="text" class="form-control add_user_validation charCount" placeholder="Last Name "></div></div><div class="col-md-3"><div class="form-group"><input name="user_email'+fieldNum+'[]" type="email" class="form-control add_user_validation charCount checkEmail" placeholder="Email "></div></div><div class="col-md-3"><div class="form-group"><select  class="form-control add_user_validation addDeptOpt" style="background:url({{asset("public/images/down-arrow.png")}})no-repeat 95% center;" name="department'+fieldNum+'[]">'+getCutomDepartments()+'</select></div></div></div></div>');

					//$('.addDeptOpt option').remove();
					//$('.addDeptOpt').append(getCutomDepartments());

				});

				//remove department in office (add organisation)
				$('.add-office-row').on('click','.removeDept',function(){

					$(this).parent('div').remove();
				});

				$(".office_count").val(next);
				next++;

				//$('.addDeptOpt option').remove();
				$('.addDeptOpt').append(getCutomDepartments());
				
			});	

$('.row.add-office-row').on('click','.remove',function(e){
	e.preventDefault();

	var div_id = "field"+$(this).attr("data-id");

	$('#'+div_id).remove();
});

$('.add-office-row').on('click','.delete-user',function(e){
	e.preventDefault();



	$(this).parent('.add-content').remove();				

});

</script>

<script type="text/javascript">

	var max_fields      = 10;
	var wrapper         = $(".add-user-row"); 				
	var user_count      = 1;

		// add user detail on click in organisation
		$('.addUserSection').on('click',function(e){
			e.preventDefault();

			var total_fields = wrapper[0].childNodes.length;

			$('.add-user-row').append('<div class="col-md-12"><div class="fild-stats-cont additional-user"><h2> Add User </h2><div class="add-content"><div class="row"><div class="col-md-4"><div class="form-group"><input name="additional_user_name'+user_count+'[]" type="text" class="form-control add_user_validation charCount" placeholder="Name " maxlength="50"></div></div><div class="col-md-4"><div class="form-group"><input name="additional_user_password'+user_count+'[]" type="password" class="form-control add_user_validation charCount" placeholder="Password "></div></div><div class="col-md-4"><div class="form-group"><input type="password" class="form-control add_user_validation charCount" placeholder="Confirm Password "></div></div><div class="addUserHere'+user_count+'"></div><div class="Add-depart-btn"><button id="add_dept'+user_count+'" class="addUser'+user_count+'" type="button"> Add User </button></div></div></div></div></div>');


		// add user in add user section (add organisation)	
		$('.add-user-row').on('click','.addUser'+user_count,function(e){
			e.preventDefault();

			var userFieldNum = this.id.charAt(this.id.length-1);
			var addUserHere  = ".addUserHere" + userFieldNum;

			console.log(userFieldNum);

			$(addUserHere).append('<div class="row"><div class="col-md-4"><div class="form-group"><input name="additional_user_name'+userFieldNum+'[]" type="text" class="form-control add_user_validation charCount"  placeholder="Name " maxlength="50"></div></div><div class="col-md-4"><div class="form-group"><input name="additional_user_password'+userFieldNum+'[]" type="password" class="form-control add_user_validation charCount"  placeholder="Password "></div></div><div class="col-md-4"><div class="form-group"><input type="password" class="form-control add_user_validation charCount" placeholder="Confirm Password "></div></div><button class="removeUser" type="button">X</button></div>');
		});

        //remove user from Add user section(add organisation)
        $('.add-user-row').on('click','.removeUser',function(){

        	$(this).parent('div').remove();
        });

        $(".additional_user_count").val(user_count);
        user_count++

    });

</script>

<script type="text/javascript">

	$(".numberControl").on("keypress keyup blur",function (event) {    
		$(this).val($(this).val().replace(/[^\d].+/, ""));
		if ((event.which < 48 || event.which > 57)) {
			event.preventDefault();
		}
	});
</script>

<script type="text/javascript">

	function getCutomDepartments()
	{
		var custom_dept_arr = new Array();

		@php 

		$controller = App::make('\App\Http\Controllers\Admin\CommonController');

		$custom_dept_array = $controller->callAction('getAllDepartments',array());

		foreach($custom_dept_array as $key => $val): @endphp

		custom_dept_arr.push('@php echo $val @endphp');

		@php endforeach; @endphp

		return custom_dept_arr;
	}

</script>

<script type="text/javascript">
$uploadCrop = $('#upload-demo').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        //type: 'circle',
        type: 'square'
    },
    boundary: {
        width: 300,
        height: 300
    }
});


$('#upload').on('change', function () { 
	var reader = new FileReader();
    reader.onload = function (e) {
    	$uploadCrop.croppie('bind', {
    		url: e.target.result
    	}).then(function(){
    		console.log('jQuery bind complete');
    	});
    	
    }
    reader.readAsDataURL(this.files[0]);
});


$('.upload-result').on('click', function (ev) {
	$uploadCrop.croppie('result', {
		type: 'canvas',
		size: 'viewport'
	}).then(function (resp) {


		$.ajax({
			url: "{{URL::to('admin/cropOrganizationImg')}}",
			type: "POST",
			data: {"image":resp,"_token":'<?php echo csrf_token()?>'},
			success: function (data) {
				$("#org_logo").val(data);

				html = '<img src="' + resp + '" />';
				//$("#upload-demo-i").html(html);
				$(".add-img-box").html(html);	
				$(".add-img-box").show();		
				$('#addOrgLogo').modal('toggle');		
			}
		});
	});
});
</script>

<script type="text/javascript">
function addOrgLogoPopUp(){
	$('#addOrgLogo').modal('toggle');
}
</script>
@include('layouts.adminFooter')

