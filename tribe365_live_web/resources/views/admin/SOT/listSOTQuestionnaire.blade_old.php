@section('title', 'Manage SOT functional lens')
@include('layouts.adminHeader')
<main class="main-content cot_inner_pages">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">						
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                                    
								{{session('message')}}
							</div>
							@endif
						</div>

						<div class="prof-acount-section report-belief">
							<div class="compy-logo">
								<h5> Supercharging </h5>
							</div>		

							<div class="search-cot sot-list-detail">              
								<span style="background-color: #cfe2f3;">A=Power</span> <span style="background-color: #fce5cd;">B=Role</span> <span style="background-color: #d9ead3;">C=Tribe</span> <span style="background-color: #f4cccc;">D=Person</span>   
							</div>				
						</div>

						<div class="value-list menual_fun_lens_wrap functional-lens">							
							<ul class="nav nav-tabs">		
								<li><a data-toggle="tab" class="active" href="#menu1">Culture Information Quistonanire</a></li>
								<li><a data-toggle="tab" href="#menu2">Console - Supercharging - organisation Culture Structure</a></li>		
							</ul>

							<div class="tab-content">

								<div id="menu1" class="tab-pane fade in active show">
									<table>
										<tr>
											<th>Section</th>
											<th>Question</th>
											<th>Type</th>	
											<th>Action</th>								
										</tr>
										<?php $counter=2;
										$count =1;
										?>	
										@foreach($sotQuesList as $value)
										<form id="form" action="{{URL::to('admin/update-sot-culture-question')}}" method="post">
											{{csrf_field()}}
											<input type="hidden" name="sotQuestionId" value="{{base64_encode($value->id)}}">	
											<tr class="table-class <?php if($counter % 2 == 0) echo 'grey-css'; ?>">
												<td>{{$value->section}}</td>
												<td>
													<textarea name="question">{{$value->question}}</textarea>
												</td>
												<td>{{$value->type}}</td>
												<td>
													<div class="editable">
														<button class="editbtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
														<button style="display: none;" class="submitbtn" type="submit">submit</button>
													</div>
												</td>
											</tr>
										</form>
										
										@if($count % 4 == 0)
										<tr class="table-class <?php if($counter % 2 == 0) echo 'grey-css'; ?>">
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										@endif

										<?php 
										$counter++;
										$count++;
										?>

										@endforeach
									</table>
								</div>

								<div id="menu2" class="tab-pane fade">
									<table>
										<tr>
											<th>Type</th>
											<th>Image</th>
											<th>Summary</th>				
										</tr>
										@foreach($sotCultureStructureList as $cValue)																															
										<tr class="table-class sot-change-img">
											<td>{{$cValue->title}}</td>
											<td>
												<img width="100px" height="100px" src="{{asset('public/uploads/sot\/').$cValue->imgUrl}}">
												<div class="editable">
													<button class="editbtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
							
												</div>
												<form id="imgForm" action="{{URL::to('admin/update-sot-culture-img')}}" method="post" enctype="multipart/form-data">
													{{csrf_field()}}
													<input type="file" name="file">
													<input type="hidden" name="cultureSrtId" value="{{base64_encode($cValue->id)}}">
													<button type="submit" class="btn">update</button>
												</form>
											</td>
											<td style="cursor: pointer;" data-toggle="modal" data-toggle="tooltip" title="click to edit">
												<a href="{{route('sot.edit',['id'=>base64_encode($cValue->id)])}}">
													@foreach($cValue->cultureSummary as $sValue)
													<li style="color:#000"><span style="color:#000;">{{$sValue->summary}}</span></li>
													@endforeach
												</a>
											</td>											
										</tr>
									</form>
									@endforeach
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>			
</div>
</main>
<script type="text/javascript">
	$(document).ready(function() {

		$('.submitbtn').hide();
		
		$(".table-class").find("input,select,textarea").prop("disabled", true);		

	});
</script>
<script type="text/javascript">
	$('.editbtn').on('click',function(){

		$(this).closest('.editable').find('.submitbtn').show();
		$(this).closest('.editable').find('.editbtn').hide();	
		$(this).closest('.table-class').find('input,select,textarea').prop('disabled',false);	
	})
</script>
<script>
	$(document).ready(function(){
    $('.editbtn').click(function() {
      $('#imgForm').toggle("slide");
    });
});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$("textarea").each(function(){
			var scroll_height = $(this).get(0).scrollHeight;
			$(this).css('height', scroll_height + 'px');
		});		
	});
</script>
@include('layouts.adminFooter')