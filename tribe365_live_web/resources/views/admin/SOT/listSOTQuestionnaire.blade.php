@section('title', 'Manage SOT functional lens')
@include('layouts.adminHeader')
<main class="main-content cot_inner_pages">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">						
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                
								{{session('message')}}
							</div>
							@endif
						</div>

						<div class="prof-acount-section report-belief">
							<div class="compy-logo">
								<h5>Supercharging- Culture Structure Questions </h5>
							</div>		

							<div class="search-cot sot-list-detail">   
								@foreach($sotCultureStructureList as $cValue)

								@if($cValue->id == 1)
								<span style="background-color: #f4cccc;">{{ucfirst($cValue->type)}}={{$cValue->title}}</span>
								@elseif($cValue->id == 2)
								<span style="background-color: #cfe2f3;">{{ucfirst($cValue->type)}}={{$cValue->title}}</span>
								@elseif($cValue->id == 3)
								<span style="background-color: #fce5cd;">{{ucfirst($cValue->type)}}={{$cValue->title}}</span>
								@elseif($cValue->id == 4)
								<span style="background-color: #d9ead3;">{{ucfirst($cValue->type)}}={{$cValue->title}}</span>
								@endif

								@endforeach           
								
							</div>				
						</div>

						<div class="value-list menual_fun_lens_wrap functional-lens">							
							<ul class="nav nav-tabs">		
								<li><a data-toggle="tab" class="active" href="#menu1">Culture Information Questionnaire</a></li>
								<li><a data-toggle="tab" href="#menu2">Console - Supercharging- Organisation Culture Structure</a></li>		
							</ul>

							<div class="tab-content">

								<div id="menu1" class="tab-pane fade in active show">
									<table>
										<tr>
											<th>Section</th>
											<th>Question</th>
											<th>Type</th>	
											<th>Action</th>								
										</tr>
										<?php $counter=2;
										$count =1;
										?>	
										@foreach($sotQuesList as $value)
										<form id="form" action="{{URL::to('admin/update-sot-culture-question')}}" method="post">
											{{csrf_field()}}
											<input type="hidden" name="sotQuestionId" value="{{base64_encode($value->id)}}">	
											<tr class="table-class <?php if($counter % 2 == 0) echo 'grey-css'; ?>">
												<td>{{$value->section}}</td>
												<td>
													<textarea name="question">{{$value->question}}</textarea>
												</td>
												<td>{{$value->type}}</td>
												<td>
													<div class="editable">
														<button class="editbtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
														<button style="display: none;" class="submitbtn" type="submit">submit</button>
													</div>
												</td>
											</tr>
										</form>
										
										@if($count % 4 == 0)
										<tr class="table-class <?php if($counter % 2 == 0) echo 'grey-css'; ?>">
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										@endif

										<?php 
										$counter++;
										$count++;
										?>

										@endforeach
									</table>
								</div>

								<div id="menu2" class="tab-pane fade">
									<table>
										<tr>
											<th>Type</th>
											<th>Image</th>
											<th>Summary</th>				
										</tr>
										@foreach($sotCultureStructureList as $cValue)																										
										<tr class="table-class sot-change-img">
											
											<form id="form" action="{{URL::to('admin/update-sot-culture-structure-title')}}" method="post">
												{{csrf_field()}}
												<input type="hidden" name="sotCulStrId" value="{{base64_encode($cValue->id)}}">
												<td class="sot-tital">
													<textarea name="sotCulStrTitle">{{$cValue->title}}</textarea> 
													<div class="editable">
														<button class="editbtn1" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
														<button style="display: none;" class="submitbtn1" type="submit">submit</button>
													</div>
												</td>
											</form>

											<td class="sot-img-eadit">
												<div class="sot-img-change">
													<img width="100px" height="100px" src="{{asset('public/uploads/sot\/').$cValue->imgUrl}}"></div>
													<div class="editable">
														<button id="editbutton{{$cValue->id}}" class="editbtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
													</div>

													<form id="imgForm{{$cValue->id}}" action="{{URL::to('admin/update-sot-culture-img')}}" method="post" enctype="multipart/form-data">
														{{csrf_field()}}
														<input type="file" name="file">
														<input type="hidden" name="cultureSrtId" value="{{base64_encode($cValue->id)}}">
														<button type="submit" class="btn updtbtn">update</button>
													</form>
												</td>
												<td style="cursor: pointer;">								
													@foreach($cValue->cultureSummary as $sValue)
													<li style="color:#000"><span style="color:#000;">{{$sValue->summary}}</span></li>									
													@endforeach
													<a href="{{route('sot.edit',['id'=>base64_encode($cValue->id)])}}"><button class="sot-text-eadit"><i class="fa fa-pencil" aria-hidden="true"></i></button></a>
												</td>	
											</tr>
										</form>

										@endforeach
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>			
	</div>
</main>
<script type="text/javascript">
	$(document).ready(function() {

		$('.submitbtn').hide();
		
		$(".table-class").find("input,select,textarea").prop("disabled", true);		
		$('.table-class').find('.updtbtn').prop('disabled',true);
		$('#imgForm1').hide();

		@foreach($sotCultureStructureList as $cValue)
		$('#imgForm{{$cValue->id}}').hide();
		@endforeach
	});
</script>
<script type="text/javascript">
	$('.editbtn').on('click',function(){

		$(this).closest('.editable').find('.submitbtn').show();
		$(this).closest('.editable').find('.editbtn').hide();	
		$(this).closest('.table-class').find('input,select,textarea').prop('disabled',false);	
		$(this).closest('.table-class').find('.updtbtn').prop('disabled',false);
	})
</script>
<script type="text/javascript">
	$('.editbtn1').on('click',function(){
		$(this).closest('.editable').find('.submitbtn1').show();
		$(this).closest('.editable').find('.editbtn1').hide();	
		$(this).closest('.table-class').find('input,select,textarea').prop('disabled',false);	
		$(this).closest('.table-class').find('.updtbtn').prop('disabled',false);
	})
</script>
<script>
	@foreach($sotCultureStructureList as $cValue)

	$(document).ready(function(){
		$('#editbutton{{$cValue->id}}').click(function() {
			$('#imgForm{{$cValue->id}}').toggle("slide");
		});
	});

	@endforeach
</script>
<!-- <script type="text/javascript">
	$(document).ready(function() {
		$("textarea").each(function(){
			var scroll_height = $(this).get(0).scrollHeight;
			$(this).css('height', scroll_height + 'px');
		});		
	});
</script> -->
@include('layouts.adminFooter')