<!-- header -->
@section('title', 'Edit DOT')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section dot">
		<div class="container">
			<div class="dot-section">
				<h2> Directing </h2>
				<div class="form-section">				
					<div align="center">                            
						@if(session('message'))
						<div class="alert alert-success" role="alert">                                    
							{{session('message')}}
						</div>
						@endif
					</div>					
					<form id="dot_form" action="" method="post">

						{{csrf_field()}}

						<div class="row evidence">
							<div class="col-md-12">
								<div class="form-group">
									<label>  Start Date </label>
									<input class="checkOrgCount" id="dot_date" type="text" value="{{!empty($dots->dot_date)?date_format(date_create($dots->dot_date),'d-m-Y'):''}}" name="dot_date" placeholder="Enter date of D.O.T" readonly="">
								</div>
								<div class="form-group">
									<label> Vision </label>
									<input class="checkOrgCount" id="vision" value="{{!empty($dots->vision)?$dots-> vision:''}}" type="text" name="vision" placeholder="Enter Vision" maxlength="250">

									<a href="{{URL::to('admin/get-evidence-list\/').base64_encode($dots->id).'/'.base64_encode('vision').'/'.base64_encode($dots->id)}}"><button type="button" class="evidence-btn">Evidence</button></a>
									
								</div>
								<div class="form-group">
									<label> Mission </label>
									<input class="checkOrgCount" id="mission" value="{{!empty($dots->mission)?$dots->mission:''}}" type="text" name="mission" placeholder="Enter Mission"  maxlength="250">	

									<a href="{{URL::to('admin/get-evidence-list\/').base64_encode($dots->id).'/'.base64_encode('mission').'/'.base64_encode($dots->id)}}"><button type="button" class="evidence-btn">Evidence</button></a>

								</div>
								<div class="form-group">
									<label> Focus </label>
									<input class="checkOrgCount" id="focus" value="{{!empty($dots->focus)?$dots->focus:''}}" type="text" name="focus" placeholder="Enter Focus"  maxlength="250">

									<a href="{{URL::to('admin/get-evidence-list\/').base64_encode($dots->id).'/'.base64_encode('focus').'/'.base64_encode($dots->id)}}"><button type="button" class="evidence-btn">Evidence</button></a>
								</div>
							</div>
						</div>

						<div class="adit-btn">
							<button class="savebtn" type="button"> Save </button>				
						</div>

					</form>
				</div>
			</div>

			<div class="dot-section Belfs-valus-section">
				<div class="form-section">

					<div class="Belfs-valus">
						<!-- add belief here -->
						@php $counter=1 @endphp

						
						<?php
						foreach($beliefs as $belief)
						{

							?>
							<div class="Belfs-valus-repeat">

								<form id="belief_form{{$counter}}" action="" method="post">
									<div class="edit-belief-btn">
										<button class="savebtnblf{{$counter}}" type="button"> Save Belief</button>	
										<button type="button" onclick="removeSaveBelief(<?php echo $belief['id']; ?>,'<?php echo $belief['name'];?>')">Remove Belief</button>	
									</div>
									<input id="counter{{$counter}}" type="hidden" name="counter" value="{{$counter}}">

									<div class="row">
										<div class="col-md-3">
											<div class="form-group">

												<label> Beliefs </label>

												<input class="checkOrgCount" type="text" name="belief_name{{$counter}}" value="{{!empty($belief['name'])?$belief['name']:''}}" placeholder="Enter Belief" class="belief_validation"  maxlength="100">	

												<input id="belief_id{{$counter}}" type="hidden" name="belief_id{{$counter}}" value="{{!empty($belief['id'])?$belief['id']:''}}">

												<a href="{{URL::to('admin/get-evidence-list\/').base64_encode($dots->id).'/'.base64_encode('belief').'/'.base64_encode($belief['id'])}}"><button type="button" class="evidence-btn">Evidence</button></a>
											</div>
										</div>
										<div class="col-md-9">
											<button type="button" onclick="getCutomValueModal(<?php echo $belief['id']; ?>,<?php echo $dots->id; ?>)" class="evidence-btn">Evidence</button>
											<label> Value </label>
											<div class="form-group">
												<div class="row addValueHere{{$counter}} toggleOpen">
													<?php

													$res =array();
													$result1 = array();

													foreach ($belief['belief_value'] as $bValue) {

														$result1 = array();
														$res[$bValue->id]=$bValue->name;
														array_push($result1, $res);


													}


													?>
													<div class="col-md-12">
														<button type="button" class="down">down</button>
														<ul class="value-checkbox">



															<?php
															foreach($dotValues as $value)
															{
																$result2 = array();
																if(!empty($result1[0]))
																{
																	$result2 = $result1[0];
																}
																else{

																	$result2 = array();
																}

																if($vId = array_search($value->id,$result2))
																{

																	?>

																	<li class="active"><input type="checkbox" id="{{$counter}}{{$vId}}" name="value_name{{$counter}}[]" value="{{$vId}},{{$value->id}}" checked="checked" onchange="deleteValue({!!$vId!!},{!!$counter!!}{!!$vId!!},{!!$value->id!!});"><label>{{$value->name}}</label></li>

																	<?php
																}
																else
																{
																	?>
																	<li><input type="checkbox" name="value_name{{$counter}}[]" value="{{$value->id}}" ><label>{{$value->name}}</label></li>

																	<?php
																}

																?>




																<?php
															}

															?>


														</ul>





													</div>


													<!-- 											<button id="" class="add_value{{$counter}} belief" type="button"> Add Value </button> -->
												</div>					
											</div>
										</div>
									</div>								

								</form>
							</div>
							@php $counter++ @endphp

							<?php

						}
						?>
						
						<form id="add_belief_form" action="" method="post">
							<input type="hidden" class="belief_count" name="belief_count" value="0">
							<div class="">
								<div class="repeat-row">
									<div class="add-belief-here"></div>	
								</div>
								<button type="button" class="belief addBeliefNew">Add More Belief</button>
							</div>

							<div class="Create-btn-cont">
								<button type="button" onclick="addNewBelief();" class="btn"> Apply Changes </button>
							</div>
						</form>	
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="error-message success" style="display: none;">
		<span id="resp"></span>
	</div>
</main>

<!-- Modal create new user-->
<button id="modalbtn" type="hidden" style="display: none;" data-toggle="modal" data-target="#myModal1"></button>

<div class="setCustomValueModelHere"></div>


<script type="text/javascript">
	function getCutomValueModal(beliefId,dotId){
		
		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/get-custom-value-evidence-modal')}}",
			data: {beliefId:beliefId,dotId:dotId,"_token":'<?php echo csrf_token()?>'},

			success: function(response){
				$('.setCustomValueModelHere div').remove();
				$('.setCustomValueModelHere').append(response);
				$('#modalbtn').click();

				//console.log(response)
			}
		});
	}
</script>


<script type="text/javascript">


	$(document).ready(function(){               
		$(".savebtnblf1").click(function() {  
			$(".mCSB_scrollTools").animate({ scrollTop: 0 }, "slow");  
		});
	});

	$(function() {
		$("#dot_date").datepicker({
			showAnim: "fold",
			dateFormat: "dd-mm-yy"
		});
	});
</script>
<!-- add belief -->
@php $counter=1 @endphp
@foreach($beliefs as $belief)
<script type="text/javascript">
	$('.savebtnblf{{$counter}}').on('click',function(){

		// var test = [];
  //       $("input[name='value_name{{$counter}}[]']:checked").each(function() {
  //           test.push($(this).val());
  //       });
  //       console.log(test);
  //       if (test.length==0) {
		// 	alert("Please select at least one value.");
		// 	return false;
  //       }	
		
		var form_values = $("#belief_form{{$counter}}").serializeArray();

		form_values.push({name: "_token", value: "<?php echo csrf_token()?>"});

		$.ajax({
			type: "POST",
			url: "{{route('dot-belief-update')}}",
			data: form_values,
			success: function(response) {	
				//console.log(response);				
				if(response=="SUCCESS")
				{	
					// console.log("hi");
					$('.error-message').show();
					$(".demo-y").animate({ scrollTop: 0 }, "slow");  
					$('#resp').html('');
					$('#resp').html('Records updated successfully.');
					location.reload();	

				}
			}
		});

	});

	$('.Belfs-valus').on('click','.add_value{{$counter}}',function(e){
		e.preventDefault();

		$('.addValueHere{{$counter}}').append('<div class="col-md-4"><select class="belief_validation addDotValuesOpt" name ="value_name_new{{$counter}}[]" style="background:url({{asset("public/images/down-arrow.png")}})no-repeat 95% center;">'+getCutomDotValues()+'</select><button class="removeValue" type="button">X</button></div>');
	});
</script>

@php $counter++ @endphp
@endforeach()

<script type="text/javascript">

	$(".savebtn").click(function(){
		$('.error-message').show();

		var ddot    = $('#dot_date').val();
		var vision  = $('#vision').val();
		var mission = $('#mission').val();
		var focus   = $('#focus').val();

		
		var empty = false;
		$('.charCount').each(function() {
			if ($(this).val().length > 50){
				empty = true;
			}
		});


		var checkOrgCount = false;
		$('.checkOrgCount').each(function() {
			if ($(this).val().length > 150){
				checkOrgCount = true;
			}
		});

		if (checkOrgCount){

			$('#resp').html('');
			$('#resp').html('All input fields should be less than 150 characters.');
		}else if (empty){

			$('#resp').html('');
			$('#resp').html('All input fields should be less than 50 characters.');
		}
		else if(ddot ==""){

			$('#resp').html('');
			$('#resp').html('Please select date.');
		}else if(vision ==""){

			$('#resp').html('');
			$('#resp').html('Please enter Vision.');
		}else if(mission ==""){

			$('#resp').html('');
			$('#resp').html('Please enter Mission.');
		}else if(focus ==""){

			$('#resp').html('');
			$('#resp').html('Please enter Focus.');
		}else{

			var form_values = $("#dot_form").serializeArray();

			form_values.push({name: "_token", value: "<?php echo csrf_token()?>"},
				{name:"dotId", value:<?php echo $dots->id ?>});

			$.ajax({
				type: "POST",
				url: "{{route('dot-update')}}",
				data: form_values,
				success: function(response) {					
					if(response=="SUCCESS")
					{					
						location.reload();	
						$('#resp').html('');
						$('#resp').html('DOT updated Successfully.');
					}
				}
			});
		}
	});
</script>

<script type="text/javascript">
	function addNewBelief(){
		//$('.error-message').show();

		var form_values = $("#add_belief_form").serializeArray();

		console.log(form_values)

		form_values.push({name: "_token", value: "<?php echo csrf_token()?>"},
			{name:"dotId", value:<?php echo $dots->id ?>});

		$.ajax({
			type: "POST",
			url: "{{route('dot-add-belief')}}",
			data: form_values,
			success: function(response) {	

				//console.log('response '+response);
				location.reload();
				
				if(response=="SUCCESS")
				{						
					$('#resp').html('');
					$('#resp').html('Records added Successfully');
					location.reload();

				}else{

					$('#resp').html('');
					$('#resp').html(response);
				}
			}
		});
	}
</script>

<script type="text/javascript">

	var next = 1;

// add belief on click				
$('.Belfs-valus').on('click','.addBeliefNew',function(e){			
	e.preventDefault();

	$('.add-belief-here').append('<div class="Belfs-valus-repeat"><div class="row "><div class="col-md-3"><div class="form-group"><label> Beliefs </label><input class="belief_validation" type="text"  maxlength="100" name="belief_name_new'+next+'" placeholder="Enter Belief"></div></div><div class="col-md-9"><label> Value </label><div class="form-group"><div class="row addValueHereNew'+next+' toggleOpen"></div></div></div><button class="removeBelief" type="button">X</button></div></div>');
	// console.log(next);
	getCutomDotValues(next);
// // add values in belief				
// $('.Belfs-valus').on('click','.add_value_new'+next,function(e){
// 	e.preventDefault();

// 	var fieldNum = this.id.charAt(this.id.length-1);
// 	var addValueHere  = ".addValueHereNew" + fieldNum;

// 	$(addValueHere).append('<div class="col-md-4"><select class="belief_validation" name ="value_name_new'+fieldNum+'[]" style="background:url({{asset("public/images/down-arrow.png")}})no-repeat 95% center;">'+getCutomDotValues()+'</select><button class="removeValue" type="button">X</button></div>');

// });


//remove remove beliefs (add dots)
$('.Belfs-valus').on('click','.removeBelief',function(){

	$(this).parent('div').parent('div').remove();
});

//remove remove beliefs (add dots)
$('.Belfs-valus').on('click','.removeValue',function(){

	$(this).parent('div').remove();
});

next++;

$(".belief_count").val(next);

});	

</script>

<script type="text/javascript">
	//remove remove beliefs
	$(document).ready(function(){

		$('.Belfs-valus').on('click','.removeBelief',function(){

			$(this).parent('div').parent('div').remove();
		});

		$('.Belfs-valus').on('click','.removeValue',function(){

			$(this).parent('div').remove();
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){

		$('.addDotValuesOpt option').remove();
		$('.addDotValuesOpt').append(getCutomDotValues());
	});

	function getCutomDotValues(next){
		
		var dot_values_arr = new Array();

		form_values = {name: "_token", value: "<?php echo csrf_token()?>",variable:next};

		$.ajax({
			type: "GET",
			url: "{{url('admin/get-custom-dot-list')}}",
			data: form_values,
			dataType: 'json',
			success: function(response) {	

				toString = response.toString();

				resCheckbox = toString.replace(/,/g , '');

				var addValueHere  = ".addValueHereNew" + next;

				$(addValueHere).append('<div class="col-md-12">	<button type="button" class="down">down</button><ul class="value-checkbox">'+resCheckbox+'</ul></div>');

			}
		});
	}


	function deleteValue(id,vid,vvid)
	{
		var $this = $(this);
		

		if(!$('#'+vid).is(':checked'))
		{
			if(confirm('Are you sure you want to delete?') == true){
				$.ajax({
					type: "POST",
					url: "{{URL::to('admin/dot-value-delete')}}",
					data: { "_token":'<?php echo csrf_token()?>',id:id},

					success: function(response) {

						$('#'+vid).removeAttr( "onchange" );	
						$('#'+vid).removeAttr( "value" );
						$( '#'+vid ).attr( "value",vvid );	
					}
				});

			}
			else{
				console.log(vid);
				//$('#'+vid).attr('checked', true);
				$('#'+vid).closest('li').addClass('active');
				document.getElementById(vid).checked = true;

			}
		}
	}


	function removeSaveBelief(id,name)
	{


		if(confirm('Are you sure you want to delete '+name+' ?') == true){
			$.ajax({
				type: "POST",
				url: "{{URL::to('admin/dot-belief-delete')}}",
				data: { "_token":'<?php echo csrf_token()?>',id:id},

				success: function(response) {

					location.reload();	
				}
			});
		}
	}
</script>

@include('layouts.adminFooter')