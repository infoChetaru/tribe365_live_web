<!-- header -->
@section('title', 'List DOT')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section organization-fild" >
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="search-section">
						<form>
							<input type="search" placeholder="Search...">
						</form>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="organization-section">

					@foreach($dot as $key => $value)

					<div class="col-md-6">
						<div class="company-section">
							<div class="company-logo">
								<!-- <a href="{{-- URL::to('admin/admin-organisation/'. $value->id . '/edit') }}"><img src="{{ asset('public/uploads/org_images').'/'.$value->ImageURL --}}"></a> -->
							</div>
							<div class="company-detale">
								<h2> {{$value->vision}} </h2>
								<div class="company-category">
									
								</div>
							</div>
						</div>
					</div>

					@endforeach 

				</div>
			</div>


			<div class="add-plus-icon">
				<div class="add-plus-upper">
					<a href="{!!route('admin-dot.create')!!}"><img src="{{ asset('public/images/plus-icon.png')}}"></a>


				</div>
			</div>
		</div>
	</div>
</div>
</main>

@include('layouts.adminFooter')