<!-- header -->
@section('title', 'List DOT Values')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">			
				<div class="row">
					<div class="col-md-12">
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                                    
								{{session('message')}}
							</div>
							@endif
						</div>
						<h2> Values  </h2>
						<div class="adit-btn">
							<a href="{!!route('custome-value')!!}"><button class="savebtn" type="button" > Add </button></a>
						</div>
						<div class="value-list">
							<table>
								<tr>
									<th> Name </th>

								</tr>
								@foreach($dotValuesList as $value)
								<tr>								
									<td>{{ucfirst($value->name)}}<div class="editable">
										<a href="{{URL::to('admin/edit-custom-value/'.base64_encode($value->id))}}">
											<button type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
										</a>
										<a href="{{URL::to('admin/delete-custom-value/'.base64_encode($value->id))}}" onclick="return confirm('Are you sure you want to delete ?');"> 
											<button type="button"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
										</a>
									</div></td>

								</tr>
								@endforeach()


							</table>
						</div>

					</div>
				</div>
			</div>

		</div>
		<div class="organ-page-nav">
			{!! $dotValuesList->links('layouts.pagination') !!}
		</div>
	</div>
</main>
@include('layouts.adminFooter')