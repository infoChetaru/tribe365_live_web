@section('title', 'Submission Feedback List')
@include('layouts.adminHeader')

<!-- header end -->
<div class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="chat-link-section chat-section feedback-list risk-register full-table">
				<table>
					<tr>
						<td>
							<table>
								<tr>
									<th> ID </th>
									<!-- <th> Initial Details </th> -->
									<th> Summary </th>
									<th> Initial Risk Score </th>
									<th> Actions Taken </th>
									<th> Status </th>
								</tr>
								@foreach($feedbackList as $feedback)
									<form action="{{URL::to('admin/update-improvements')}}" method="post">
									{{csrf_field()}}
										<tr class="table-class">
											<td>{{$feedback->id}}</td>
											<!-- <td>
												<div class="report-email toltip-section">
													<span class="hide-text feedback-msg">{{$feedback->message}}</span>
												</div>
											</td> -->
											<td>
												<div class="report-email toltip-section">
													<span class="hide-text feedback-msg">						{{$feedback->feedbackSummary}}
													</span>
												</div>
											</td>
											<td>{{$feedback->riskScore}}</td>
											@if(!empty($feedback->actionTaken))
												<td>
													<div class="report-email toltip-section">
														<span class="hide-text feedback-msg">					<ul class="font-weight-600">
																@foreach($feedback->actionTaken as $actionVal)
																	<li>{{$actionVal->description}}</li>
																@endforeach
															</ul>
														</span>
													</div>
												</td>
											@else
											<td>
												<div class="report-email toltip-section">
													<span class="hide-text feedback-msg">					
														<ul class="font-weight-600">
															-
														</ul>
													</span>
												</div>
											</td>
											@endif
											<td>{{$feedback->feedbackStatus}}</td>
										</tr>
									</form>
								@endforeach
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="organ-page-nav">		
</div>

<!-- footer -->
@include('layouts.adminFooter')
