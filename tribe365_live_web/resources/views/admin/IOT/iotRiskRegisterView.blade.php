@section('title', 'Risk Register')
@include('layouts.adminHeader')
<style type="text/css">
	.chat-link-section.risk-register table tr td table tr td {
	    padding: 17px 15px 15px 20px!important;
	}
</style>

<!-- header end -->
<main class="main-content cot_inner_pages">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">						
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                
								{{session('message')}}
							</div>
							@endif
						</div>
						<div class="search-dia-question">
							<h2> Risk Register </h2>	

							<form id="risk-register-form" action="#" method="POST" class="diagnostic">
								<select id="risk-register-org" name="orgId"> 
									<option value="">All Organisations</option>
									@foreach($organisations as $value)
										<option {{($orgId==$value->id)?'Selected':'' }} value="{{$value->id}}">{{$value->organisation}}</option>
									@endforeach()
								</select>
								<button type="button" class="risk-register-form" style="width: auto;">Search</button>
							</form>
						</div>

						<!-- <div class="prof-acount-section report-belief">
							<div class="compy-logo">
								<h5>Risk Register</h5>
							</div>		
						</div>

						<div class="search-cot">				
							<form id="risk-register-form" action="#" method="POST">
								{{csrf_field()}}					
								<select id="risk-register-org" name="orgId"> 
									<option value="">All Organisations</option>
									@foreach($organisations as $value)
										<option {{($orgId==$value->id)?'Selected':'' }} value="{{$value->id}}">{{$value->organisation}}</option>
									@endforeach()
								</select>
								<button type="button" class="risk-register-form" style="width: auto;">Search</button>
							</form> 
						</div> -->
						<div class="loader-img dashboard-loader" style="display:none; width: 70px;margin-top: -6em;">
							<img src="{{asset('public/images/loader.gif')}}">
						</div>
						<div class="value-list menual_fun_lens_wrap functional-lens risk-register-tabs">							
							<ul class="nav nav-tabs">
								@if(!empty($themesCategory))
									@php
										$i=1;
									@endphp
									@foreach($themesCategory as $themescate)
										@if($themescate->id == 1)
											<li><a data-toggle="tab" class="active" href="#menu{{$i}}">{{ucfirst($themescate->title)}}</a></li>
										@else
											<li><a data-toggle="tab" href="#menu{{$i}}">{{ucfirst($themescate->title)}}</a></li>		
										@endif
										@php
											$i++;
										@endphp
									@endforeach
								@endif
							</ul>

							<div class="tab-content">
							@if(!empty($feedbackList))
								@php
									$j=1;
								@endphp
								@foreach($feedbackList as $feedbackVals)
									@if($feedbackVals['themeId'] == 1)
										<div id="menu{{$j}}" class="tab-pane fade in active show">
											<div class="chat-link-section chat-section feedback-list risk-register">
											<table>
												<tr>
													<td>
														<table>
															<tr>
																<th> ID </th>
																<th style="min-width: 130px;"> Date </th>
																<th> Organisation </th>
																<th style="width: 300px;"> Details </th>
																<!-- <th> SWOT </th> -->
																<!-- <th> Theme </th> -->
																<th> Initial Likelihood </th>
																<th> Initial Consequence </th>
																<th> Initial Risk Score </th>
																<!-- <th> Actions Taken </th> -->
																<!-- <th> Mitigated Score </th> -->
																<!-- <th> Status </th> -->
																<!-- <th> Update </th> -->
																<!-- <th> Action </th> -->
															</tr>
															@foreach($feedbackVals['feedbackArr'] as $feedback)
																<form action="{{URL::to('admin/update-risk-register')}}" method="post">
																{{csrf_field()}}
																	<tr class="table-class">
																		<input type="hidden" name="feedbackId" value="{{$feedback->id}}">
																		<td>{{$feedback->id}}</td>
																		<td>{{date('d-m-Y',strtotime($feedback->created_at))}}</td>
																		<td>{{$feedback->organisation}}</td>
																		<td>
																			<div class="report-email toltip-section">
																				<span class="hide-text feedback-msg">{{$feedback->message}}</span>
																			</div>
																		</td>
																		<td>-</td>
																		<td>-</td>
																		<td>{{$feedback->riskScore}}</td>
																	</tr>
																</form>
															@endforeach
														</table>
													</td>
												</tr>
											</table>
											<!-- <div class="organ-page-nav">
												{!! $feedbackListTbl1->links('layouts.pagination') !!}
											</div> -->
										</div>
									</div>
									@else
									<div id="menu{{$j}}" class="tab-pane fade in">
										<div class="chat-link-section chat-section feedback-list risk-register">
											<table>
												<tr>
													<td>
														<table>
															<tr>
																<th> ID </th>
																<th style="min-width: 130px;"> Date </th>
																<th> Organisation </th>
																<th style="width: 300px;"> Details </th>
																<!-- <th> SWOT </th> -->
																<!-- <th> Theme </th> -->
																<th> Initial Likelihood </th>
																<th> Initial Consequence </th>
																<th> Initial Risk Score </th>
																<!-- <th> Actions Taken </th> -->
																<!-- <th> Mitigated Score </th> -->
																<!-- <th> Status </th> -->
																<!-- <th> Update </th> -->
																<!-- <th> Action </th> -->
															</tr>
															@foreach($feedbackVals['feedbackArr'] as $feedback)
																<form action="{{URL::to('admin/update-risk-register')}}" method="post">
																{{csrf_field()}}
																	<tr class="table-class">
																		<input type="hidden" name="feedbackId" value="{{$feedback->id}}">
																		<td>{{$feedback->id}}</td>
																		<td>{{date('d-m-Y',strtotime($feedback->created_at))}}</td>
																		<td>{{$feedback->organisation}}</td>
																		<td>
																			<div class="report-email toltip-section">
																				<span class="hide-text feedback-msg">{{$feedback->message}}</span>
																			</div>
																		</td>
																		<td>-</td>
																		<td>-</td>
																		<td>{{$feedback->riskScore}}</td>
																	</tr>
																</form>
															@endforeach
														</table>
													</td>
												</tr>
											</table>
											<!-- @if($j==2)
												<div class="organ-page-nav">
													{!! $feedbackListTbl2->links('layouts.pagination') !!}
												</div>
											@elseif($j==3)
												<div class="organ-page-nav">
													{!! $feedbackListTbl3->links('layouts.pagination') !!}
												</div>
											@elseif($j==4)
												<div class="organ-page-nav">
													{!! $feedbackListTbl4->links('layouts.pagination') !!}
												</div>
											@endif -->
										</div>
									</div>
									@endif
									@php
										$j++;
									@endphp
								@endforeach
							@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>			
	</div>
</main>





<!-- modal button  -->
<button style="display: none;" type="button" class="modal-btn" data-toggle="modal" data-target="#myModal"></button>
<!-- modal data appended here -->
<div id="modal-data"></div>

<script type="text/javascript">
	$('.risk-register-form').on('click',function(){
		$('.loader-img').show();			

		var form_values   = $("#risk-register-form").serializeArray();
		var orgId      		= form_values[0]['value'];

		// console.log(orgId);return false;
		
		$.ajax({
			type: "GET",
			url: "{{URL::to('admin/risk-register')}}",
			data: {orgId:orgId,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				$('.loader-img').hide();
				$('body').html('');	
				$('body').append(response);
			}
		});		
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.submitbtn').hide();
		$(".table-class").find("textarea").prop("disabled", true);		
		$(".table-class").find("select").prop("disabled", true);		
	});
</script>
<script type="text/javascript">
	$('.editbtn').on('click',function(){
		$(this).closest('.editable').find('.submitbtn').show();
		$(this).closest('.editable').find('.viewbtn').hide();
		$(this).closest('.editable').find('.editbtn').hide();	
		$(this).closest('.table-class').find('textarea').prop('disabled',false);		
		$(this).closest('.table-class').find('select').prop('disabled',false);		
	})
</script>
<script type="text/javascript">

	$('select[name="SWOTCate"]').on('change', function(){    
		var SWOTId = $(this).val();
		var orgId = $(this).attr('data-orgId');
		var id = $(this).attr('data-feedbackid');

		$.ajax({
			url: "{{URL::to('admin/getThemesFromSWOT')}}",
			type: "POST",
			dataType: "JSON",
			data: {orgId:orgId,SWOTId:SWOTId,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{	
				var themeList = response.themeListTbl;
				var optionArr = [];
				if ( themeList.length != 0) {
					$.each(themeList,function(key,value){
						var option = '<option value="'+value['id']+'">'+value['title']+'</option>';
						optionArr.push(option);
					});
					$('select.select'+id).html(optionArr);
				}else{
					$('select.select'+id).html('<option value="">No Risks</option>');
				}
			}
		});
	    
	});

	// $( "#SWOTCate" ).change(function() {
	//   	var SWOTId = $('#SWOTCate').val();
	// 	console.log(SWOTId);return false;
	// });





	// function changeFeedThemes(orgId) {
	// 	var SWOTId = $('.SWOTCate').val();

	// 	// console.log($('.SWOTCate').val());return false;

	// 	// $.ajax({
	// 	// 	url: "{{URL::to('admin/getThemesFromSWOT')}}",
	// 	// 	type: "POST",
	// 	// 	dataType: "JSON",
	// 	// 	data: {orgId:orgId,SWOTId:SWOTId,"_token":'<?php echo csrf_token()?>'},
	// 	// 	success: function(response)
	// 	// 	{
	// 	// 		//swal("Deleted!", "Your imaginary file has been deleted.", "success");
	// 	// 		if (response.status == 200) {
	// 	// 			//window.location.href = '';
	// 	// 			location.reload();
	// 	// 		}
	// 	// 	}
	// 	// });

	// 	console.log(SWOTId);return false;




	// 	// getThemesFromSWOT
	// 	// SWOTCate
	// }
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.create-btn').click(function(){     
			$('#modal-data').html('');    
			var feedbackId = $(this).data('id');			
			$('#feedbackId').val(feedbackId);

			$.ajax({
				type: "POST",
				url: "{{route('get-theme-modal')}}",
				data: {"feedbackId":feedbackId, "_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{	
				// console.log(response)
				$('#modal-data').append(response);
				$('.modal-btn').click();
			}
		});

		});
	});
</script>

<script type="text/javascript">
	function validation()
	{
		var themeId    = $('#theme').val();
		var feedbackId = $('#feedback_id').val().trim();

		console.log('themeId ' + themeId)
		console.log('feedbackId ' +feedbackId)

		if(!themeId) 
		{
			console.log('error-message')
			$('.error-message').show();
			$('#resp').html('');
			$('#resp').html('Please select theme.');	
		}		
		else
		{
			$.ajax({
				type: "POST",
				url: "{{route('update-feedback-theme')}}",
				data: {themeId:themeId, feedbackId:feedbackId, "_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{				
					console.log(response)
					location.reload(true);
				}
			});
		}
	}
</script>

<script type="text/javascript">
	function deleteFeedbacks(id,orgId) {

		if(confirm('Are you sure you want to delete?') == true){
			$.ajax({
				url: "{{URL::to('admin/delete-feedbacks')}}",
				type: "POST",
				dataType: "JSON",
				data: {orgId:orgId,feedbackId:id,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					//swal("Deleted!", "Your imaginary file has been deleted.", "success");
					if (response.status == 200) {
						//window.location.href = '';
						location.reload();
					}
				}
			});
		}	
	}
</script>

<!-- footer -->
@include('layouts.adminFooter')

