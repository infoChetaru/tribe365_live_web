<style type="text/css">
	.delete-feedback :hover{
		background-color: #eb1c24 !important;
	}
	.tim button {
    padding-right: 0;
	}
	.tim button:hover {
		background-color: transparent;
	}
	.dt-tim-section {padding: 9px 15px !important;}
	.report-email.toltip-section .toltip-box{
		max-width: 340px !important;
		width: auto !important;
	}
</style>

@section('title', 'IOT')
@include('layouts.adminHeader')
<!-- header end -->
<div class="main-content">
	<div class="add-fild-section">
		<div class="chat-link-section">
			<div class="container">
				<div class="row">

					@foreach($feedbackList as $feedback)
					<div class="col-md-4">
						<div class="chat-link"> 
							<div class="dt-tim-section">
								<div class="dt">
									<strong> {{$feedback->id}} </strong>
									<span> </span>
								</div>
								<div class="tim">
									<strong> </strong>
									<span>
										<button class="btn chat-btn delete-feedback" onclick="deleteFeedbacks('{{base64_encode($feedback->id)}}','{{base64_encode($feedback->orgId)}}')">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
										 </span>
								</div>
							</div>
							<ul>
								<li>
									<strong> Date Of Submission</strong>
									<span> {{date('d-m-Y',strtotime($feedback->created_at))}} </span>
								</li>
								<li>
									<strong> Organisation </strong>
									<span> {{$feedback->organisation}} </span>
								</li>
								<li>
									<strong> Office</strong>
									<span>{{$feedback->office}}</span>
								</li>
								<li>
									<strong> Department</strong>
									<span>{{$feedback->department}}</span>
								</li>
								<li>
									<strong> Initial Text</strong>
									<div class="toltip-section">
										<span class="hide-text">{{ucfirst($feedback->message)}}</span>
										<div class="toltip-box">{{ucfirst($feedback->message)}}</div>
									</div>
								</li>
								<li>
									<strong> Image </strong>
									<!-- <span> -->
										@if($feedback->image)
										<img width="40px" height="40px" src="{{asset('public/uploads/iot_files/'.$feedback->image)}}">
										@else
										<img width="40px" height="40px" src="{{asset('public/images/no-image.jpg')}}">
										@endif
										<!-- </span> -->
									</li>

									<li>
										<strong> Reporter Email </strong>
										<!-- <span>{{$feedback->email}}</span> -->
										<div class="report-email toltip-section">
										<span class="hide-text">{{$feedback->email}}</span>
										<div class="toltip-box">{{$feedback->email}}</div>
									</div>
									</li>
									<li>
										<strong> Theme/Trends/Risk</strong>		
										@if(!empty($feedback->themes))	
										<div class="toltip-section">
											<span class="hide-text">{{implode(', ', $feedback->themes)}}</span>
											<div class="toltip-box">{{implode(', ', $feedback->themes)}}</div>
										</div>
										@else
										<span>-</span>
										@endif			
										
									</li>								
								</ul>
								<div class="cht-lnk-btn">
									<a href="{{url::to('admin/get-chat/'.($feedback->orgId).'/'.base64_encode($feedback->id))}}"><button class="btn chat-btn"> Chat </button></a>

									<button class="create-btn btn chat-btn" data-toggle="modal" data-id="{{$feedback->id}}"> Link </button>

								</div>
							</div>
						</div>

						@endforeach

					</div>
				</div>
			</div>
		</div>
		<div class="error-message" style="display: none;">
			<span id="resp"></span>
		</div> 
	</div>
	<div class="organ-page-nav">		
		{!! $feedbackListTbl->withPath('')->links('layouts.pagination') !!}
	</div>
	<!-- modal button  -->
	<button style="display: none;" type="button" class="modal-btn" data-toggle="modal" data-target="#myModal"></button>
	<!-- modal data appended here -->
	<div id="modal-data"></div>
	<!-- footer -->
	@include('layouts.adminFooter')

	<script type="text/javascript">
		$(document).ready(function(){
			$('.create-btn').click(function(){     
				$('#modal-data').html('');    
				var feedbackId = $(this).data('id');			
				$('#feedbackId').val(feedbackId);

				$.ajax({
					type: "POST",
					url: "{{route('get-theme-modal')}}",
					data: {"feedbackId":feedbackId, "_token":'<?php echo csrf_token()?>'},
					success: function(response)
					{	
					// console.log(response)
					$('#modal-data').append(response);
					$('.modal-btn').click();
				}
			});

			});
		});
	</script>

	<script type="text/javascript">
		function validation()
		{
			var themeId    = $('#theme').val();
			var feedbackId = $('#feedback_id').val().trim();

			console.log('themeId ' + themeId)
			console.log('feedbackId ' +feedbackId)

			if(!themeId) 
			{
				console.log('error-message')
				$('.error-message').show();
				$('#resp').html('');
				$('#resp').html('Please select theme.');	
			}		
			else
			{
				$.ajax({
					type: "POST",
					url: "{{route('update-feedback-theme')}}",
					data: {themeId:themeId, feedbackId:feedbackId, "_token":'<?php echo csrf_token()?>'},
					success: function(response)
					{				
						console.log(response)
						location.reload(true);
					}
				});
			}
		}
	</script>

	<script type="text/javascript">
	function deleteFeedbacks(id,orgId) {

		if(confirm('Are you sure you want to delete?') == true){
			$.ajax({
				url: "{{URL::to('admin/delete-feedbacks')}}",
				type: "POST",
				dataType: "JSON",
				data: {orgId:orgId,feedbackId:id,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					//swal("Deleted!", "Your imaginary file has been deleted.", "success");
					if (response.status == 200) {
						//window.location.href = '';
						location.reload();
					}
				}
			});
		}	
	}
</script>
