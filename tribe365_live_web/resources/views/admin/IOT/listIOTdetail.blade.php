@section('title', 'List IOT Detail')
<?php header('Access-Control-Allow-Origin: *'); ?>
@include('layouts.adminHeader')
<div class="add-fild-section">
	<div class="container">
		<div class="ragistration-section">
			<div class="row">
				<div class="col-md-12">
					<div align="center">							
						@if(session('message'))
						<div class="alert alert-success" role="alert">                                    
							{{session('message')}}
						</div>
						@endif
					</div>
					<h2> Improving </h2>					
					<div class="value-list">
						<table class="jsonData">
							<tr>								
								<th> ID</th>
								<th> App Name</th>
								<th> Email</th>		
								<th> Message Detail</th>
								<th> Images</th>
								<th> Location</th>
								<th> Latitude</th>		
								<th> Longitude</th>
								<th> Report Date</th>
								<th> Report Time</th>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(window).load(function(){
		
		$.ajax({
			type: "POST",		
			url: "http://tellsid.softintelligence.co.uk/index.php/apitellsid/getIOTdetail",
			data: {app_name:'Tribe365',org_id:<?php echo (!empty($orgId))?$orgId:''; ?>},
			success: function(response) {

				var obj = JSON.parse(response);
				len = obj.response.length;

				var tableDataArray = [];

				for (var i = 0; i < len; ++i) {

					var image = obj.response[i]['images'];
					var latitude = parseFloat(obj.response[i]['latitude']).toFixed(6);
					var longitude = parseFloat(obj.response[i]['longitude']).toFixed(6);

					if(image)
					{
						var image = '<a href="'+image+'"target="_blank"><img src="'+image+'" border="0" alt="image" title="image" height="50px" width="80px"></a>';
					}else{
						var image= '<img src="http://tellsid.softintelligence.co.uk/assets/admin/images/no_result.png" border="0" title="image" height="50px" width="80px">';
					}

					var html = '<tr><td>'+obj.response[i]['changeit_id']+'</td><td>'+obj.response[i]['app_name']+'</td><td>'+obj.response[i]['email_id']+'</td><td>'+obj.response[i]['msg_detail']+'</td><td>'+image+'</td><td>'+obj.response[i]['location_detail']+'</td><td>'+latitude+'</td><td>'+longitude+'</td><td>'+obj.response[i]['report_date'] +'</td><td>'+obj.response[i]['report_time']+'</td></tr>';

					tableDataArray.push(html);
					
				}
				$('.jsonData').append(tableDataArray);

				// console.log(html)
			}
		});
	});
</script>
@include('layouts.adminFooter')