<div id="myModal" class="modal fade multiple-select" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<form id="modal-form" action="#" method="post">
				<div class="modal-body ">
					<input type="hidden" name="feedbackId" id="feedback_id" value="{{$feedbackId}}">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="">						
						<select id="theme" name="theme[]" multiple="multiple">					
							@foreach($themeList as $themes)
							<option {{(in_array($themes->id, $selectedfeedbackArr))?'selected':''}} value="{{$themes->id}}">{{ucfirst($themes->title)}}</option>
							@endforeach()
						</select>							
					</div>
				</div>
				
				<div class="modal-footer">
					<button id="add-new-theme-show" type="button" class="btn btn-default"> Add new theme </button>	
					<button type="button" onclick="validation()" class="btn btn-default"> Save </button>
				</div>					
			</form>

			<form id="new-theme-form">				
				<div id="new-theme-input" style="display: none;">
					<input type="text" id="theme-title" name="theme_title"> 	
					<button id="add-new-theme" type="button"> Add </button>
				</div>
			</form>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(function () {
		$('#theme').multiselect({
			includeSelectAllOption: true,
			enableFiltering:true,
			enableCaseInsensitiveFiltering: true,
			nonSelectedText: 'Select theme',
		});			
	});
</script>


<!-- <script type="text/javascript">
	$(function () {
		$('#theme').multiselect({
			includeSelectAllOption: true,
			enableFiltering:true,
			enableCaseInsensitiveFiltering: true,
		});			
	});
</script> -->
<!-- add new theme by ajax when modal is open -->
<script type="text/javascript">
	$('#add-new-theme-show').on('click',function(){
		$('#new-theme-input').show();
	});
</script>
<script type="text/javascript">
	$('#add-new-theme').on('click',function(){
		
		var feedbackId = "{{$feedbackId}}";
		var themeTitle = $('#theme-title').val();

		$.ajax({
			type: "POST",
			url: "{{route('add-new-theme-by-ajax')}}",
			data: {feedbackId:feedbackId,themeTitle:themeTitle, "_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				var obj = JSON.parse(response);
				var htmlContainer = obj.htmlContainer;
				var htmlOption    = obj.htmlOption
				
				$('#new-theme-input').hide();
				$('#theme').html('');
				$('.multiselect-container').html('');
				$('#theme').append(htmlOption);
				$('.multiselect-container').append(htmlContainer);			
			}
		});
	});
</script>