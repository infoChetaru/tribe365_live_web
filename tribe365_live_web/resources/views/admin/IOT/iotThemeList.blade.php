<!-- header -->
@section('title', 'IOT')
@include('layouts.adminHeader')
<div class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="chat-link-section chat-section feedback-list risk-register">
				<table>
					<tr>
						<td>
							<table>
								<tr>
									<th> Title </th>
									<th> Date Opened </th>
									<th> Description </th>
									<th> Type </th>
									<th> Initial Likelihood </th>
									<th> Initial Consequence </th>
									<th> Initial Risk Rating </th>
									<th> Current Likelihood </th>
									<th> Current Consequence </th>
									<th> Current Risk Rating </th>
									<th> Submissions </th>
									<th> Organisation </th>
									<th> Linked Actions </th>
									<th> Status </th>
									<th style="min-width: 200px;"> Notes </th>
									<th style="min-width: 160px;"> Action </th>
								</tr>
								@foreach($themeListTbl as $value)
									<form action="{{URL::to('admin/update-theme')}}" method="post">
									{{csrf_field()}}
										<input type="hidden" name="themeId" value="{{base64_encode($value->id)}}">	
										<tr class="table-class">
											<td><input type="text" name="title" value="{{ucfirst($value->title)}}" required="" style="background-color: #fff;"></td>
											<td>
												<input class="charCount date" type="text" name="date_opened" placeholder="Date Opened" autocomplete="off" value="{{date('d-m-Y',strtotime($value->dateOpened))}}" required="" style="background-color: #fff;">
											</td>
											<td>
												<textarea name="description" required="">{{ucfirst($value->description)}}</textarea>
											</td>
											<td>
												<select name="themeCategory">
													@foreach($themeCategory as $themesCate)
														<option {{($themesCate->id==$value->themeCateId)?'Selected':'' }} 
														value="{{$themesCate->id}}">{{$themesCate->title}}</option>
													@endforeach
												</select>
											</td>
											<td>
												<select name="initial_likelihood" class="initial">
													<option value="" disabled="" selected=""> Select initial likelihood </option>
													<option {{($value->initialLikelihood=='0')?'selected':''}} value="0"> 0 </option>
													<option {{($value->initialLikelihood=='1')?'selected':''}} value="1"> 1 </option>
													<option {{($value->initialLikelihood=='2')?'selected':''}} value="2"> 2 </option>
													<option {{($value->initialLikelihood=='3')?'selected':''}} value="3"> 3 </option>
													<option {{($value->initialLikelihood=='4')?'selected':''}} value="4"> 4 </option>
													<option {{($value->initialLikelihood=='5')?'selected':''}} value="5"> 5 </option>
												</select>
											</td>
											<td>
												<select name="initial_consequence" class="initial">
													<option value="" disabled="" selected=""> Select initial consequence </option>
													<option {{($value->initialConsequence=='0')?'selected':''}} value="0"> 0 </option>
													<option {{($value->initialConsequence=='1')?'selected':''}} value="1"> 1 </option>
													<option {{($value->initialConsequence=='2')?'selected':''}} value="2"> 2 </option>
													<option {{($value->initialConsequence=='3')?'selected':''}} value="3"> 3 </option>
													<option {{($value->initialConsequence=='4')?'selected':''}} value="4"> 4 </option>
													<option {{($value->initialConsequence=='5')?'selected':''}} value="5"> 5 </option>
												</select>
											</td>
											<td name="initialRiskRating" class="text-center pt-30">{{$value->initialRiskRating}}</td>
											<td>
												<select name="current_likelihood" class="initial">
													<option value="" disabled="" selected=""> Select current likelihood </option>
													<option {{($value->currentLikelihood=='0')?'selected':''}} value="0"> 0 </option>
													<option {{($value->currentLikelihood=='1')?'selected':''}} value="1"> 1 </option>
													<option {{($value->currentLikelihood=='2')?'selected':''}} value="2"> 2 </option>
													<option {{($value->currentLikelihood=='3')?'selected':''}} value="3"> 3 </option>
													<option {{($value->currentLikelihood=='4')?'selected':''}} value="4"> 4 </option>
													<option {{($value->currentLikelihood=='5')?'selected':''}} value="5"> 5 </option>
												</select>
											</td>
											<td>
												<select name="current_consequence" class="initial">
													<option value="" disabled="" selected=""> Select current likelihood </option>
													<option {{($value->currentConsequence=='0')?'selected':''}} value="0"> 0 </option>
													<option {{($value->currentConsequence=='1')?'selected':''}} value="1"> 1 </option>
													<option {{($value->currentConsequence=='2')?'selected':''}} value="2"> 2 </option>
													<option {{($value->currentConsequence=='3')?'selected':''}} value="3"> 3 </option>
													<option {{($value->currentConsequence=='4')?'selected':''}} value="4"> 4 </option>
													<option {{($value->currentConsequence=='5')?'selected':''}} value="5"> 5 </option>
												</select>
											</td>
											<td name="currentRiskRating" class="text-center pt-30">{{$value->currentRiskRating}}</td>
											<td>
												<div data-href="{{ URL::to('admin/feedback-list-from-theme-list/').'/'.base64_encode($value->orgId).'/'.base64_encode($value->id)}}">
													<select name="submissions[]" multiple="multiple" required="" class="clickable">
														<?php $j = 0;?>
														@foreach($value->allSubmission as $submissionVal)
															@php
																$actionIds = $value->submission;
															@endphp
															@if(!empty($actionIds))
																@if(in_array($submissionVal->id,$actionIds))
																	<?php $j++; ?>
																@endif
																<option {{( in_array($submissionVal->id,$actionIds))?'Selected':'' }} value="{{$submissionVal->id}}">{{$submissionVal->message}}</option>
															@else
																<option value="{{$submissionVal->id}}">{{$submissionVal->message}}</option>
															@endif
														@endforeach
													</select>
													@if( !empty($j) && $j>3 )
														<span class="action-view-more">View more</span>
													@endif
												</div>
											</td>
											<!-- <td class="text-center pt-30">
												@if(!empty($value->submission))	
													<a href="{{URL::to('admin/submission-feedback/'.base64_encode($value->id).'/'.base64_encode($value->orgId))}}">
														<span style="color: #000;">{{count($value->submission)}}</span>
													</a>
												@else
													<span style="color: #000;">0</span>
												@endif
											</td> -->
											<td>
												<select name="organisation" class="theme-list-org-dropdown organisation">
													<option value="" disabled="" selected=""> All organisation </option>
													@foreach($organisations as $oValue)
													<option {{($oValue->id==$value->orgId)?'selected':''}} value="{{$oValue->id}}">{{ucfirst($oValue->organisation)}}</option>
													@endforeach
												</select>
											</td> 
											<!-- <td>
												@if(!empty($value->actions))	
													<a href="{{ URL::to('admin/action-list/').'/'.base64_encode($value->orgId)}}">	
														<span style="color: #000;">{{($value->actions)}}</span>
													</a>
												@else
													<span style="color: #000;">{{($value->actions)}}</span>
												@endif
											</td> -->
											<td>
												<div data-href="{{ URL::to('admin/action-list/').'/'.base64_encode($value->orgId).'/1/'.base64_encode($value->id)}}">
													<select name="actionTaken[]" multiple="multiple" required="" class="clickable">
														<?php $i = 0;?>
														@foreach($value->actions as $action)
															@php
																$actionIds = explode(',', $value->linkedAction);
															@endphp
															@if(!empty($actionIds))
																@if(in_array($action->id,$actionIds))
																	<?php $i++; ?>
																@endif
																<option {{( in_array($action->id,$actionIds))?'Selected':'' }} value="{{$action->id}}">{{$action->description}}</option>
															@else
																<option value="{{$action->id}}">{{$action->description}}</option>
															@endif
														@endforeach
													</select>
													@if( !empty($i) && $i>3 )
														<span class="action-view-more">View more</span>
													@endif
												</div>
												<div class="action-plus-risk" style="display: none;">
													<button type="button" class="plus-icon-risk"><i class="fa fa-plus" aria-hidden="true"></i></button>
													<textarea class="action-textarea"></textarea>
													<select name="resUsers" class="resUsers">
														<option value="" disabled="" selected="">Responsible Person</option>
														<!-- @foreach($value->responsiblePersonList as $resPersonList)
															<option value="{{$resPersonList->id}}">{{ucfirst($resPersonList->name)." ".ucfirst($resPersonList->lastName)}}</option>
														@endforeach -->
													</select>
													<button type="button" class="save-btn">Save</button>
													<input type="hidden" name="orgId" value="{{$value->orgId}}">
												</div>
											</td>
											<td>
												<select name="status">
													<option value="" disabled="" selected=""> Select status </option>
													<option {{($value->themeStatus=='Open')?'selected':''}} value="open"> Open </option>
													<option {{($value->themeStatus=='Closed')?'selected':''}} value="Closed"> Closed </option>
												</select>
											</td>
											<td>
												@php
													$notesVal = $value->themeNote;
												@endphp
												@if(!empty($notesVal))
													<div class="theme-notes-message">
														<div class="report-email toltip-section notes">
															<!-- <h6>{{$notesVal->message}}</h6> -->
															<h6 class="hide-text feedback-msg">{{$notesVal->message}}</h6>
															<span class="theme-notes-time">{{"[".ucfirst($notesVal->name)." ".ucfirst($notesVal->lastName)." ".date('d/m/Y H:i',strtotime($notesVal->created_at))."]"}}</span>
															@if(count($value->allThemeNote) > 1)
																<div class="notes-view-more">
																	<a href="#" data-toggle="modal" data-target="#viewMoreModal_{{$value->id}}">View More</a>
																</div>
															@endif
														</div>
													</div>
													<!--Notes Modal -->
													<div class="modal fade" id="viewMoreModal_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
														<div class="modal-dialog modal-dialog-centered" role="document">
															<div class="modal-content notes-modal">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal">&times;</button>
																</div>
																<div class="modal-body">
																	@foreach($value->allThemeNote as $allNotes)
																	  	<div class="theme-notes-message p-2">
																			<h6>{{$allNotes->message}}</h6>
																			<span class="theme-notes-time">{{"[".ucfirst($allNotes->name)." ".ucfirst($allNotes->lastName)." ".date('d/m/Y H:i',strtotime($allNotes->created_at))."]"}}</span>
																		</div>
																		<hr>
																	@endforeach
																</div>
															</div>
														</div>
													</div>
												@endif												
												<textarea name="comment" class="mt-2">{{ucfirst($value->comment)}}</textarea>
												<!-- @if (!empty($value->comment) && !empty($value->updated_at))
													<span class="theme-notes-time">{{"[".ucfirst($value->adminName)." ".$value->updated_at."]"}}</span>
            									@endif -->
											</td>
											<td>
												<div class="editable">
													<button class="editbtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
													<button class="viewbtn" type="button" onclick="deleteThemes('{{base64_encode($value->id)}}','{{base64_encode($value->orgId)}}')"> 
														<i class="fa fa-trash" aria-hidden="true"></i> 
													</button>
													<button style="display: none;" class="submitbtn" type="submit">submit</button>
													<!-- <button style="display: none;" class="submitbtn" type="button" onclick="validation($(this))">submit</button> -->
													<!-- <button type="button" onclick="validation()" class="btn save"> Update </button> -->
												</div>
											</td>
										</tr>
									</form>
								@endforeach
							</table>
						</td>
					</tr>
				</table>
			</div>
			<!-- <div class="chat-link-section">
				<div class="container">
					<div class="row">

						@foreach($themeListTbl as $value)
						<div class="col-md-4">
							<div class="chat-link"> 
								<div class="dt-tim-section">
									<div class="dt">
										<strong> {{ucfirst($value->title)}}</strong>
										<span>  </span>
									</div>									
								</div>
								<ul>
									<li>
										<strong> Date Opened</strong>
										<span> {{date('d-m-Y',strtotime($value->dateOpened))}}</span>										
									</li>
									<li>
										<strong> Description </strong>
										<div class="toltip-section">
											<span class="hide-text">{{ucfirst($value->description)}}</span>
											<div class="toltip-box">{{ucfirst($value->description)}}</div>
										</div>
									</li>
									<li>
										<strong> Type </strong>
										<span>{{ucfirst($value->categoryTitle)}}</span>
									</li>
									<li>
										<strong> Organisation</strong>
										<span>{{ucfirst($value->organisation)}}</span>
									</li>
									<li>
										<strong> Submissions</strong>
										@if(!empty($value->submission))	
											<a href="{{URL::to('admin/submission-feedback/'.base64_encode($value->id).'/'.base64_encode($value->orgId))}}">
												<span>{{count($value->submission)}}</span>
											</a>
										@else
											<span>0</span>
										@endif
									</li>
									<li>
										<strong> Initial likelihood</strong>
										<span>{{($value->initialLikelihood)}}</span>
									</li>
									<li>
										<strong> Initial consequence</strong>
										<span>{{($value->initialConsequence)}}</span>
									</li>
									<li>
										<strong> Current likelihood</strong>
										<span>{{($value->currentLikelihood)}}</span>
									</li>
									<li>
										<strong> Current consequence</strong>
										<span>{{($value->currentConsequence)}}</span>
									</li>
									<li>
										<strong> Linked action</strong>
										<div class="toltip-section">
											@if(!empty($value->actions))	
												<a href="{{ URL::to('admin/action-list/').'/'.base64_encode($value->orgId)}}">	
													<span class="hide-text">{{ucfirst($value->actions)}}</span>
												</a>
											@else
												<span class="hide-text">{{ucfirst($value->actions)}}</span>
											@endif
											<div class="toltip-box">{{ucfirst($value->actions)}}</div>
										</div>
									</li>
									<li>
										<strong> Status</strong>
										<span>{{ucfirst($value->themeStatus)}}</span>
									</li>
								</ul>
								<div class="cht-lnk-btn">
									<a href="{{url::to('admin/edit-theme'.'/'.($value->orgId).'/'.base64_encode($value->id))}}" data-toggle="tooltip" title="Edit theme"><button class="btn chat-btn">Edit</button></a>
									<button class="btn chat-btn" onclick="deleteThemes('{{base64_encode($value->id)}}','{{base64_encode($value->orgId)}}')"> Delete </button>
								</div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div> -->
			<div class="add-plus-icon">
				<div class="add-plus-upper">
					<a href="{{route('add-theme')}}"><img src="{{asset('public/images/plus-icon.png')}}">
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="error-message" style="display: none;">
		<span id="resp"></span>
	</div>
</div>
<div class="organ-page-nav">
	{!! $themeListTblpagi->links('layouts.pagination') !!}                        
</div>

 

<script type="text/javascript">
	$(document).ready(function(){
		$('.submitbtn').hide();
		$(".table-class").find("textarea").prop("disabled", true);		
		$(".table-class").find("select").prop("disabled", true);		
		$(".table-class").find("input").prop("disabled", true);	
		$(".table-class").find(".action-plus-risk").hide();
		$(".table-class").find("[name='comment']").hide();
		$(document.body).on("click", ".table-class td.disabled div", function() {
			location.href = $(this).attr('data-href');
		});
		$( ".table-class select.clickable" ).each(function( index ) {
			if( $(this).val() ){
				$(this).parent().parent().addClass('disabled');
			}
		});
	});
</script>
<script type="text/javascript">
	$('select[name="actionTaken[]"]').select2({
		closeOnSelect : false,
		placeholder : "Select Actions",
		allowHtml: true,
		allowClear: true,
		// tags: true
	});
	$('select[name="submissions[]"]').select2({
		closeOnSelect : false,
		placeholder : "Select Submission",
		allowHtml: true,
		allowClear: true,
		// tags: true
	});
</script>
<script type="text/javascript">
	$('.editbtn').on('click',function(){
		$(this).closest('.editable').find('.submitbtn').show();
		$(this).closest('.editable').find('.viewbtn').hide();
		$(this).closest('.editable').find('.editbtn').hide();	
		$(this).closest('.table-class').find('textarea').prop('disabled',false);		
		$(this).closest('.table-class').find('select').prop('disabled',false);		
		$(this).closest('.table-class').find('input').prop('disabled',false);		
		$(this).closest('.table-class').find(".action-plus-risk").show();
		$(this).closest(".table-class").find(".action-plus-risk").find(".action-textarea").hide();
		$(this).closest(".table-class").find(".action-plus-risk").find(".save-btn").hide();
		$(this).closest(".table-class").find(".action-plus-risk").find(".resUsers").hide();
		$(this).closest(".table-class").find("[name='comment']").show();
		$(this).closest(".table-class").find(".action-view-more").hide();
		$(this).closest(".table-class").find("td").removeClass('disabled');
	});
</script>
<script type="text/javascript">
	$('.plus-icon-risk').on('click',function(){
		$(this).closest(".table-class").find(".action-plus-risk").find(".action-textarea").show();
		$(this).closest(".table-class").find(".action-plus-risk").find(".save-btn").show();
		$(this).closest(".table-class").find(".action-plus-risk").find(".resUsers").show();

		var orgId = $(this).closest(".table-class").find("[name='orgId']").val();

		$.ajax({
			url: "{{URL::to('admin/responsiblePersonList')}}",
			type: "POST",
			dataType: "JSON",
			data: {orgId:orgId,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{	
				if (response.status == 200) {
					var resUsers = response.resUsers;

					$('.resUsers option').remove();
					
					$('.resUsers').append('<option value="" disabled="" selected="">Responsible Person</option>');

					$.each(resUsers,function(key,value){
						$('.resUsers').append('<option value="'+value['id']+'">'+value['name']+' '+value['lastName']+'</option>');
					});
				}
			}
		});		
	});
</script>
<script type="text/javascript">
	$('.organisation').on('change', function() {

		var orgId = $(this).closest(".table-class").find(".organisation").val();
		$(this).closest(".table-class").find("[name='orgId']").val(orgId);
		var actionTaken = $(this).closest(".table-class").find("[name='actionTaken[]']");
		var submissions = $(this).closest(".table-class").find("[name='submissions[]']");

		$(this).closest(".table-class").find(".action-plus-risk").find(".action-textarea").hide();
		$(this).closest(".table-class").find(".action-plus-risk").find(".save-btn").hide();
		$(this).closest(".table-class").find(".action-plus-risk").find(".resUsers").hide();

		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: "{{route('get-actions-by-ajax-edit')}}",
			data: {orgId:orgId, "_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{
				if (response.status == 200) {
					$(actionTaken).html(response.htmlOption).trigger('change');
					$(submissions).html(response.feedbackOption).trigger('change');
				}
			}
		});
	});
</script>
<script type="text/javascript">
	$('.initial').on('change', function() {

		var initial_likelihood = $(this).closest(".table-class").find("[name='initial_likelihood']").val();
		var initial_consequence = $(this).closest(".table-class").find("[name='initial_consequence']").val();
		var current_likelihood = $(this).closest(".table-class").find("[name='current_likelihood']").val();
		var current_consequence = $(this).closest(".table-class").find("[name='current_consequence']").val();
		var initialRiskRtg = $(this).closest(".table-class").find("[name='initialRiskRating']");
		var currentRiskRtg = $(this).closest(".table-class").find("[name='currentRiskRating']");

		var initialRiskRating = (initial_likelihood*initial_consequence);
		var currentRiskRating = (current_likelihood*current_consequence);

		var intialRiskRatingVal = '-';
		if (initialRiskRating>=1 && initialRiskRating<=4) {
           	intialRiskRatingVal = "Low";
        }else if (initialRiskRating>=5 && initialRiskRating<=12) {
            intialRiskRatingVal = "Medium";
        }else if (initialRiskRating>=13 && initialRiskRating<=25) {
            intialRiskRatingVal = "High";
        }

        var currentRiskRatingVal = '-';
		if (currentRiskRating>=1 && currentRiskRating<=4) {
           	currentRiskRatingVal = "Low";
        }else if (currentRiskRating>=5 && currentRiskRating<=12) {
            currentRiskRatingVal = "Medium";
        }else if (currentRiskRating>=13 && currentRiskRating<=25) {
            currentRiskRatingVal = "High";
        }

		$(initialRiskRtg).html(intialRiskRatingVal);		
		$(currentRiskRtg).html(currentRiskRatingVal);		
	});
</script>

<script type="text/javascript">
	$('.save-btn').on('click',function(){
		var actionText = $(this).closest(".table-class").find(".action-textarea").val();
		var orgId = $(this).closest(".table-class").find("[name='orgId']").val();
		var userId = $(this).closest(".table-class").find("[name='resUsers']").val();
		var actionTaken = $(this).closest(".table-class").find("[name='actionTaken[]']");
		var actionTextBlank = $(this).closest(".table-class").find(".action-textarea");
		var userIdBlank = $(this).closest(".table-class").find("[name='resUsers']");

		if (actionText.trim() == '') {
			$('.error-message').show();
			$('#resp').html('Please enter some text.');
		}else if (userId == null) {
			$('.error-message').show();
			$('#resp').html('Please select responsible person.');
		}else{
			$('#resp').html('');
			$.ajax({
				url: "{{URL::to('admin/add-action-from-add-theme')}}",
				type: "POST",
				dataType: "JSON",
				data: {orgId:orgId,actionText:actionText,userId:userId,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{	
					if (response.status == 200) {
						$(actionTaken).append('<option selected="selected" value="'+response.actionId+'">'+response.actionText+'</option>').trigger('change');
						$(actionTextBlank).val('');
						$(userIdBlank).val('');
					}
				}
			});		
		}
	});
</script>
<script type="text/javascript">
	function validation($this)
	{
		$('.error-message').show();

		var title = $this.closest(".table-class").find("[name='title']").val();
		var date_opened = $this.closest(".table-class").find("[name='date_opened']").val();
		var description = $this.closest(".table-class").find("[name='description']").val();
		var themeCategory = $this.closest(".table-class").find("[name='themeCategory']").val();
		var initial_likelihood = $this.closest(".table-class").find("[name='initial_likelihood']").val();
		var initial_consequence = $this.closest(".table-class").find("[name='initial_consequence']").val();
		var current_likelihood = $this.closest(".table-class").find("[name='current_likelihood']").val();
		var current_consequence = $this.closest(".table-class").find("[name='current_consequence']").val();
		var organisation = $this.closest(".table-class").find("[name='organisation']").val();
		var actionTaken = $this.closest(".table-class").find("[name='actionTaken[]']").val();
		var status = $this.closest(".table-class").find("[name='status']").val();

		console.log(title);
		console.log(date_opened);
		console.log(description);
		console.log(themeCategory);
		console.log(initial_likelihood);
		console.log(initial_consequence);
		console.log(current_likelihood);
		console.log(current_consequence);
		console.log(organisation);
		console.log(actionTaken);
		console.log(status);
		// return false;

		if($.trim(title) == '') {
			$('#resp').html('');
			$('#resp').html('Please enter title.');
		}
		if($.trim(date_opened) == '') {
			$('#resp').html('');
			$('#resp').html('Please select date.');
		}
		else if($.trim(description) == '') {
			$('#resp').html('');
			$('#resp').html('Please enter description.');
		}
		else if($.trim(themeCategory) == '') {
			$('#resp').html('');
			$('#resp').html('Please select type.');
		}
		else if($.trim(initial_likelihood) == '') {
			$('#resp').html('');
			$('#resp').html('Please enter initial likelihood.');
		}
		else if($.trim(initial_consequence) == '') {
			$('#resp').html('');
			$('#resp').html('Please enter initial consequence.');
		}
		else if($.trim(current_likelihood) == '') {
			$('#resp').html('');
			$('#resp').html('Please enter current likelihood.');
		}
		else if($.trim(current_consequence) == '') {
			$('#resp').html('');
			$('#resp').html('Please enter current consequence.');
		}
		else if($.trim(organisation) == '') {
			$('#resp').html('');
			$('#resp').html('Please select organisation.');
		}
		else if($.trim(actionTaken) == '') {
			$('#resp').html('');
			$('#resp').html('Please select action.');
		}
		else if($.trim(status) == '') {
			$('#resp').html('');
			$('#resp').html('Please select status.');
		}
		else {
			$('.submitbtn').prop("disabled", true);
			$('[name="edit-theme-form"]').submit();
		}
	}
</script>

<script type="text/javascript">
	$(document).on('focus', '.date',function(){
		$(this).datepicker({
			todayHighlight:true,
			dateFormat: 'dd-mm-yy',
			minDate:'0',
			autoclose:true
		});
	});
</script>

<script type="text/javascript">
	function deleteThemes(id,orgId) {

		if(confirm('Are you sure you want to delete?') == true){
			$.ajax({
				url: "{{URL::to('admin/delete-themes')}}",
				type: "POST",
				dataType: "JSON",
				data: {orgId:orgId,themeId:id,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					//swal("Deleted!", "Your imaginary file has been deleted.", "success");
					if (response.status == 200) {
						//window.location.href = '';
						location.reload();
					}
				}
			});
		}	
	}
</script>

<!-- footer -->
@include('layouts.adminFooter')