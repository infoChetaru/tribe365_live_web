@section('title', 'IOT')
@include('layouts.adminHeader')

<!-- header end -->
<div class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="chat-link-section chat-section feedback-list risk-register">
				<table>
					<tr>
						<td>
							<table>
								<tr>
									<th> ID </th>
									<th style="min-width: 140px;"> Date </th>
									<th> Organisation </th>
									<th> Department </th>
									<th style="min-width: 200px;"> Office </th>
									<th style="min-width: 200px;"> User Name </th>
									<th> Initial Details </th>
									<th> SWOT </th>
									<th> Risks </th>
									<th> Summary </th>
									<th> Initial Risk Score </th>
									<th> Actions Taken </th>
									<th> Status </th>
									<th style="min-width: 200px;"> Action </th>
								</tr>
								@foreach($feedbackList as $feedback)
									<form action="{{URL::to('admin/update-improvements')}}" method="post">
									{{csrf_field()}}
										<tr class="table-class">
											<input type="hidden" name="feedbackId" value="{{$feedback->id}}">
											<td>{{$feedback->id}}</td>
											<td>{{date('d-m-Y',strtotime($feedback->created_at))}}</td>
											<td>{{$feedback->organisation}}</td>
											<td>{{$feedback->department}}</td>
											<td>{{$feedback->office}}</td>
											<td>{{$feedback->userName}}</td>
											<td>
												<div class="report-email toltip-section">
													<span class="hide-text feedback-msg">{{$feedback->message}}</span>
													<!-- <div class="toltip-box">{{$feedback->message}}</div> -->
												</div>
											</td>
											<td>
												<div>
													<!-- <select onchange="changeFeedThemes('{{$feedback->orgId}}');" class="SWOTCate"> -->
													<select name="SWOTCate[]" data-orgId="{{$feedback->orgId}}" data-feedbackId="{{$feedback->id}}" multiple="multiple">
														<!-- <option value="">Select SWOT </option> -->
														@foreach($themeCateList as $themesCate)
															@php
																$swotIds = explode(',',$feedback->SWOT);
															@endphp
															<option {{( in_array($themesCate->id,$swotIds))?'Selected':'' }} 
															value="{{$themesCate->id}}">{{$themesCate->title}}</option>
														@endforeach
													</select>
												</div>
											</td>
											<td>
												<div data-href="{{ URL::to('admin/theme-list-from-feedback-list/').'/'.base64_encode($feedback->orgId).'/'.base64_encode($feedback->id)}}" class="feedbackThemes">
												<!-- <div class="feedbackThemes"> -->
													<select name="themesId[]" class="select{{$feedback->id}} clickable" multiple="multiple">
														@php
															$themeListTblQuery = DB::table('iot_themes')
													            ->select('iot_themes.id','iot_themes.title')
													            ->where('iot_themes.status','Active');
													        if(!empty($feedback->orgId)){
													            $themeListTblQuery->where('iot_themes.orgId',$feedback->orgId);
													        }
													        $themeList = $themeListTblQuery->get();

													        $themeIds = explode(',',$feedback->themeId);
														@endphp
														<?php $j = 0;?>
														@if(count($themeList))
															@foreach($themeList as $themeVal)
																@if(in_array($themeVal->id,$feedback->themes))
																	<?php $j++; ?>
																@endif
																<option {{( in_array($themeVal->id,$feedback->themes))?'Selected':'' }} value="{{$themeVal->id}}">{{$themeVal->title}}</option>
															@endforeach
														@else
															<option value=""></option>
														@endif
													</select>
													@if( !empty($j) && $j>3 )
														<span class="action-view-more">View more</span>
													@endif
												</div>
												<div class="action-plus-risk" style="display: none;">
													<button type="button" class="plus-icon-theme"><i class="fa fa-plus" aria-hidden="true"></i></button>
													<textarea class="risk-textarea"></textarea>
													<select name="allSwot" class="allSwot">
														@foreach($themeCateList as $themesCate)
															<option value="{{$themesCate->id}}">{{$themesCate->title}}</option>
														@endforeach
													</select>
													<button type="button" class="risks-save-btn">Save</button>
												</div>
											</td>
											<td>
												<textarea cols="30" name="initialText">{{$feedback->feedbackSummary}}</textarea>
											</td>
											<td>
												<div>
													<select name="initialRiskScore">
														@foreach($initialRiskScore as $initialRiskVal)
															<option {{($initialRiskVal->id==$feedback->initialRiskScore)?'Selected':'' }} value="{{$initialRiskVal->id}}">{{$initialRiskVal->title}}</option>
														@endforeach
													</select>
												</div>
											</td>
											<td>
												<div data-href="{{ URL::to('admin/action-list/').'/'.base64_encode($feedback->orgId).'/2/'.base64_encode($feedback->id)}}">
													<select name="actionTaken[]" multiple="multiple" required="" class="clickable">
														<?php $i = 0;?>
														@foreach($feedback->actions as $actionVal)
															@php
																$actionIds = explode(',',$feedback->actionTaken);
															@endphp
															@if(!empty($actionIds))
																@if(in_array($actionVal->id,$actionIds))
																	<?php $i++; ?>
																@endif
																<option {{( in_array($actionVal->id,$actionIds))?'Selected':'' }} value="{{$actionVal->id}}">{{$actionVal->description}}</option>
															@else
																<option value="{{$actionVal->id}}">{{$actionVal->description}}</option>
															@endif
														@endforeach
													</select>
													@if( !empty($i) && $i>3 )
														<span class="action-view-more">View more</span>
													@endif
												</div>
												<div class="action-plus-risk">
													<button type="button" class="plus-icon-risk"><i class="fa fa-plus" aria-hidden="true"></i></button>
													<textarea class="action-textarea"></textarea>
													<select name="resUsers" class="resUsers">
														<option value="" disabled="" selected="">Responsible Person</option>
													</select>
													<button type="button" class="save-btn">Save</button>
													<input type="hidden" name="userId" value="{{$feedback->userId}}">
												</div>
												<input type="hidden" name="orgId" value="{{$feedback->orgId}}">
											</td>
											<td>
												<select name="status">
													@if($feedback->feedbackStatus == 1)
														<option selected="" value="1">Open</option>
														<option value="2">Close</option>
													@else if($feedback->feedbackStatus == 2)
														<option value="1">Open</option>
														<option selected="" value="2">Close</option>
													@endif
												</select>
											</td>
											<td>
												<div class="editable">
													<a href="{{url::to('admin/get-chat/'.($feedback->orgId).'/'.base64_encode($feedback->id))}}" style="float: none;margin-left: 0;">
														<button class="viewbtn" type="button">
															<i class="fa fa-eye" aria-hidden="true"></i>
														</button>
													</a>
													<button class="editbtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
													<button class="deletebtn" type="button" onclick="deleteSubmissions('{{base64_encode($feedback->id)}}','{{base64_encode($feedback->orgId)}}')"> 
														<i class="fa fa-trash" aria-hidden="true"></i> 
													</button>
													<button style="display: none;" class="submitbtn" type="submit">submit</button>
												</div>
											</td>
										</tr>
									</form>
								@endforeach
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div class="error-message" style="display: none;">
		<span id="resp"></span>
	</div> 
</div>
<div class="organ-page-nav">		
	{!! $feedbackListTbl->withPath('')->links('layouts.pagination') !!}
</div>
<!-- modal button  -->
<button style="display: none;" type="button" class="modal-btn" data-toggle="modal" data-target="#myModal"></button>
<!-- modal data appended here -->
<div id="modal-data"></div>
<!-- footer -->

<script src="{{asset('public/extranal_js_css/select2.min.js')}}"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script> -->
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />s -->
<link href="{{asset('public/extranal_js_css/select2.min.css')}}" rel="stylesheet" />
<script type="text/javascript">
	
	$('select[name="SWOTCate[]"]').select2({
		closeOnSelect : false,
		placeholder : "Select SWOT",
		allowHtml: true,
		allowClear: true,
		// tags: true // создает новые опции на лету
	});
	$('select[name="themesId[]"]').select2({
		closeOnSelect : false,
		placeholder : "Select Risks",
		allowHtml: true,
		allowClear: true,
		// tags: true // создает новые опции на лету
	});
	$('select[name="actionTaken[]"]').select2({
		closeOnSelect : false,
		placeholder : "Select Actions",
		allowHtml: true,
		allowClear: true,
		// tags: true // создает новые опции на лету
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.submitbtn').hide();
		$(".table-class").find("textarea").prop("disabled", true);		
		$(".table-class").find("select").prop("disabled", true);	
		$(".table-class").find(".action-plus-risk").hide();
		$(document.body).on("click", ".table-class td.disabled div", function() {
			location.href = $(this).attr('data-href');
		});
		$( ".table-class select.clickable" ).each(function( index ) {
			if( $(this).val() ){
				$(this).parent().parent().addClass('disabled');
			}
		});
	});
</script>
<script type="text/javascript">
	$('.editbtn').on('click',function(){
		$(this).closest('.editable').find('.submitbtn').show();
		$(this).closest('.editable').find('.viewbtn').hide();
		$(this).closest('.editable').find('.editbtn').hide();	
		$(this).closest('.editable').find('.deletebtn').hide();	
		$(this).closest('.table-class').find('textarea').prop('disabled',false);		
		$(this).closest('.table-class').find('select').prop('disabled',false);	
		$(this).closest('.table-class').find(".action-plus-risk").show();
		$(this).closest(".table-class").find(".action-plus-risk").find(".action-textarea").hide();
		$(this).closest(".table-class").find(".action-plus-risk").find(".save-btn").hide();	
		$(this).closest(".table-class").find(".action-plus-risk").find(".resUsers").hide();
		$(this).closest(".table-class").find(".action-view-more").hide();
		$(this).closest(".table-class").find(".action-plus-risk").find(".risk-textarea").hide();
		$(this).closest(".table-class").find(".action-plus-risk").find(".allSwot").hide();	
		$(this).closest(".table-class").find(".action-plus-risk").find(".risks-save-btn").hide();	
		$(this).closest(".table-class").find("td").removeClass('disabled');
	});
</script>
<script type="text/javascript">
	$('.plus-icon-risk').on('click',function(){
		$(this).closest(".table-class").find(".action-plus-risk").find(".action-textarea").show();
		$(this).closest(".table-class").find(".action-plus-risk").find(".save-btn").show();
		$(this).closest(".table-class").find(".action-plus-risk").find(".resUsers").show();

		var orgId = $(this).closest(".table-class").find("[name='orgId']").val();

		$.ajax({
			url: "{{URL::to('admin/responsiblePersonList')}}",
			type: "POST",
			dataType: "JSON",
			data: {orgId:orgId,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{	
				if (response.status == 200) {
					var resUsers = response.resUsers;

					$('.resUsers option').remove();
					
					$('.resUsers').append('<option value="" disabled="" selected="">Responsible Person</option>');

					$.each(resUsers,function(key,value){
						$('.resUsers').append('<option value="'+value['id']+'">'+value['name']+' '+value['lastName']+'</option>');
					});
				}
			}
		});
	});
</script>
<script type="text/javascript">
	$('.plus-icon-theme').on('click',function(){
		$(this).closest(".table-class").find(".action-plus-risk").find(".risk-textarea").show();
		$(this).closest(".table-class").find(".action-plus-risk").find(".allSwot").show();	
		$(this).closest(".table-class").find(".action-plus-risk").find(".risks-save-btn").show();
	});
</script>
<script type="text/javascript">
	$('.save-btn').on('click',function(){
		var actionText = $(this).closest(".table-class").find(".action-textarea").val();
		var orgId = $(this).closest(".table-class").find("[name='orgId']").val();
		var userId = $(this).closest(".table-class").find("[name='resUsers']").val();
		var feedbackId = $(this).closest(".table-class").find("[name='feedbackId']").val();
		var actionTaken = $(this).closest(".table-class").find("[name='actionTaken[]']");
		var actionTextBlank = $(this).closest(".table-class").find(".action-textarea");
		var userIdBlank = $(this).closest(".table-class").find("[name='resUsers']");

		if (actionText.trim() == '') {
			$('.error-message').show();
			$('#resp').html('Please enter some text.');
		}else if (userId == null) {
			$('.error-message').show();
			$('#resp').html('Please select responsible person.');
		}else{
			$('#resp').html('');
			$.ajax({
				url: "{{URL::to('admin/add-action-from-feedback')}}",
				type: "POST",
				dataType: "JSON",
				data: {orgId:orgId,actionText:actionText,userId:userId,feedbackId:feedbackId,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{	
					if (response.status == 200) {
						$(actionTaken).append('<option selected="selected" value="'+response.actionId+'">'+response.actionText+'</option>').trigger('change');
						$(actionTextBlank).val('');
						$(userIdBlank).val('');
						// location.reload();
					}
				}
			});		
		}

	});
</script>
<script type="text/javascript">
	$('.risks-save-btn').on('click',function(){
		var riskText = $(this).closest(".table-class").find(".risk-textarea").val();
		var orgId = $(this).closest(".table-class").find("[name='orgId']").val();
		var swotId = $(this).closest(".table-class").find("[name='allSwot']").val();
		var feedbackId = $(this).closest(".table-class").find("[name='feedbackId']").val();
		var actionTaken = $(this).closest(".table-class").find("[name='actionTaken[]']").val();
		var themeBox = $(this).closest(".table-class").find("[name='themesId[]']");
		var riskTextBlank = $(this).closest(".table-class").find(".risk-textarea");
		var swotIdBlank = $(this).closest(".table-class").find("[name='allSwot']");

		// console.log(riskText);
		// console.log(orgId);
		// console.log(swotId);
		// console.log(feedbackId);
		// console.log(actionTaken);
		// return false;

		if (riskText.trim() == '') {
			$('.error-message').show();
			$('#resp').html('Please enter some text.');
		}else{
			$('#resp').html('');
			$.ajax({
				url: "{{URL::to('admin/add-risks-from-feedback')}}",
				type: "POST",
				dataType: "JSON",
				data: {orgId:orgId,riskText:riskText,swotId:swotId,feedbackId:feedbackId,actionTaken:actionTaken,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{	
					if (response.status == 200) {
						$(themeBox).append('<option selected="selected" value="'+response.themeId+'">'+response.riskText+'</option>').trigger('change');
						$(riskTextBlank).val('');
						$(swotIdBlank).val('');
						// location.reload();
					}
				}
			});		
		}

	});
</script>
<script type="text/javascript">

	$('select[name="SWOTCate"]').on('change', function(){    
		var SWOTId = $(this).val();
		var orgId = $(this).attr('data-orgId');
		var id = $(this).attr('data-feedbackid');

		$.ajax({
			url: "{{URL::to('admin/getThemesFromSWOT')}}",
			type: "POST",
			dataType: "JSON",
			data: {orgId:orgId,SWOTId:SWOTId,"_token":'<?php echo csrf_token()?>'},
			success: function(response)
			{	
				var themeList = response.themeListTbl;
				var optionArr = [];
				if ( themeList.length != 0) {
					$.each(themeList,function(key,value){
						var option = '<option value="'+value['id']+'">'+value['title']+'</option>';
						optionArr.push(option);
					});
					$('select.select'+id).html(optionArr);
				}else{
					$('select.select'+id).html('<option value="">No Risks</option>');
				}
			}
		});
	    
	});

	// $( "#SWOTCate" ).change(function() {
	//   	var SWOTId = $('#SWOTCate').val();
	// 	console.log(SWOTId);return false;
	// });





	// function changeFeedThemes(orgId) {
	// 	var SWOTId = $('.SWOTCate').val();

	// 	// console.log($('.SWOTCate').val());return false;

	// 	// $.ajax({
	// 	// 	url: "{{URL::to('admin/getThemesFromSWOT')}}",
	// 	// 	type: "POST",
	// 	// 	dataType: "JSON",
	// 	// 	data: {orgId:orgId,SWOTId:SWOTId,"_token":'<?php echo csrf_token()?>'},
	// 	// 	success: function(response)
	// 	// 	{
	// 	// 		//swal("Deleted!", "Your imaginary file has been deleted.", "success");
	// 	// 		if (response.status == 200) {
	// 	// 			//window.location.href = '';
	// 	// 			location.reload();
	// 	// 		}
	// 	// 	}
	// 	// });

	// 	console.log(SWOTId);return false;




	// 	// getThemesFromSWOT
	// 	// SWOTCate
	// }
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.create-btn').click(function(){     
			$('#modal-data').html('');    
			var feedbackId = $(this).data('id');			
			$('#feedbackId').val(feedbackId);

			$.ajax({
				type: "POST",
				url: "{{route('get-theme-modal')}}",
				data: {"feedbackId":feedbackId, "_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{	
				// console.log(response)
				$('#modal-data').append(response);
				$('.modal-btn').click();
			}
		});

		});
	});
</script>

<script type="text/javascript">
	function validation()
	{
		var themeId    = $('#theme').val();
		var feedbackId = $('#feedback_id').val().trim();

		console.log('themeId ' + themeId)
		console.log('feedbackId ' +feedbackId)

		if(!themeId) 
		{
			console.log('error-message')
			$('.error-message').show();
			$('#resp').html('');
			$('#resp').html('Please select theme.');	
		}		
		else
		{
			$.ajax({
				type: "POST",
				url: "{{route('update-feedback-theme')}}",
				data: {themeId:themeId, feedbackId:feedbackId, "_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{				
					console.log(response)
					location.reload(true);
				}
			});
		}
	}
</script>

<script type="text/javascript">
	function deleteSubmissions(id,orgId) {
		if(confirm('Are you sure you want to delete?') == true){
			$.ajax({
				url: "{{URL::to('admin/delete-feedbacks')}}",
				type: "POST",
				dataType: "JSON",
				data: {orgId:orgId,feedbackId:id,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					if (response.status == 200) {
						location.reload();
					}
				}
			});
		}	
	}
</script>

@include('layouts.adminFooter')
