<!-- header -->
@section('title', 'Add User')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">

			<div class="dot-section">
				<div class="message-cls" align="center">                            
					@if(session('message'))
					<div class="alert alert-success" role="alert">
						{{session('message')}}
					</div>
					@endif
					@if(session('error'))
					<div class="alert alert-danger" role="alert">
						{{session('error')}}
					</div>
					@endif
				</div> 
				<h2> Add Risk </h2>
				
				<div class="form-section">
					<form id="add-theme-form" action="{{route('store-theme')}}" method="post">
						{{csrf_field()}}						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">									
									<input class="charCount date" id="date-opened"  type="text" name="date_opened" placeholder="Date Opened" autocomplete="off">
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input class="charCount" id="title" type="text" name="title" placeholder="Title">
								</div>									
							</div>		
							
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">									
									<input class="charCount" id="description"  type="text" name="description" placeholder="Description">
								</div>									
							</div>				
							<div class="col-md-6">
								<div class="form-group">									
									<select id="organisation" name="organisation">
										<option value="" disabled="" selected=""> All organisation </option>
										@foreach($organisations as $oValue)
										<option value="{{$oValue->id}}">{{ucfirst($oValue->organisation)}}</option>
										@endforeach
									</select>
								</div>									
							</div>
							<input type="hidden" name="orgId" id="orgId" value="">
						</div>

						<div class="row add-action" style="display: none;">
							<div class="col-md-6">
								<div class="form-group tt">									
									<div class="select-mnth">						
										<select  id="theme" class="linked-action" name="linked_action[]" multiple="multiple">

										</select>							
									</div>
									<div class="action-plus-risk">
										<button type="button" class="plus-icon-risk"><i class="fa fa-plus" aria-hidden="true"></i></button>		
									</div>
								</div>								
							</div>
							<div class="col-md-6 action-text" style="display: none;">
								<div class="row action-inner">
									<div class="col-md-6" >
										<div class="form-group">
											<input class="charCount" id="addAction" type="text" name="addAction" placeholder="Add New Action">
										</div>
									</div>
									<div class="col-md-6" >
										<div class="form-group">
										<select style="height: 44px" id="resUsers" name="resUsers" class="form-control" required>
											<option value="" disabled="" selected>Responsible Person</option>
										</select>
									</div>
									</div>
									<div class="action-btn">
										<button type="button" class="save-btn">Save</button>
									</div>
								</div>
								
								
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">									
									<select id="status" name="status">
										<option value="" disabled="" selected=""> Select status </option>
										<option value="Open"> Open </option>
										<option value="Closed"> Closed </option>
									</select>
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group">									
									<select id="type" name="type">
										@foreach($types as $tValue)
										<option value="{{$tValue->id}}">{{ucfirst($tValue->title)}}</option>
										@endforeach
									</select>
								</div>									
							</div>																
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">	
									<select id="initial-likelihood" name="initial_likelihood">
										<option value="" disabled="" selected=""> Select initial likelihood </option>
										<option value="0"> 0 </option>
										<option value="1"> 1 </option>
										<option value="2"> 2 </option>
										<option value="3"> 3 </option>
										<option value="4"> 4 </option>
										<option value="5"> 5 </option>
									</select>
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<select id="initial-consequence" name="initial_consequence">
										<option value="" disabled="" selected=""> Select initial consequence </option>
										<option value="0"> 0 </option>
										<option value="1"> 1 </option>
										<option value="2"> 2 </option>
										<option value="3"> 3 </option>
										<option value="4"> 4 </option>
										<option value="5"> 5 </option>
									</select>
								</div>									
							</div>														
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">	
									<select id="current-likelihood" name="current_likelihood">
										<option value="" disabled="" selected=""> Select current likelihood </option>
										<option value="0"> 0 </option>
										<option value="1"> 1 </option>
										<option value="2"> 2 </option>
										<option value="3"> 3 </option>
										<option value="4"> 4 </option>
										<option value="5"> 5 </option>
									</select>
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group">									
									<select id="current-consequence" name="current_consequence">
										<option value="" disabled="" selected=""> Select current likelihood </option>
										<option value="0"> 0 </option>
										<option value="1"> 1 </option>
										<option value="2"> 2 </option>
										<option value="3"> 3 </option>
										<option value="4"> 4 </option>
										<option value="5"> 5 </option>
									</select>
								</div>									
							</div>														
						</div>

						

						<div class="row">
							<div class="col-md-12">
								<div class="Create-btn-cont">
									<button type="button" onclick="validation()" class="btn save">Add</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="error-message" style="display: none;">
					<span id="resp"></span>
				</div> 
			</div>
		</div>
	</main>

	@include('layouts.adminFooter')

	<script type="text/javascript">
		$('.plus-icon-risk').on('click',function(){
			$(".action-text").show();
			var orgId = $('#orgId').val();
			$.ajax({
				url: "{{URL::to('admin/responsiblePersonList')}}",
				type: "POST",
				dataType: "JSON",
				data: {orgId:orgId,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{	
					if (response.status == 200) {
						var resUsers = response.resUsers;
						$.each(resUsers,function(key,value){
							$('#resUsers').append('<option value="'+value['id']+'">'+value['name']+'</option>');
						});
					}
				}
			});		
		});
	</script>

	<script type="text/javascript">
	$('.save-btn').on('click',function(){
		var actionText = $("#addAction").val();
		var orgId = $('#orgId').val();
		var userId = $('#resUsers').val();
		        
		if (actionText.trim() == '') {
			$('.error-message').show();
			$('#resp').html('Please enter some text.');
		}else if (userId == null) {
			$('.error-message').show();
			$('#resp').html('Please select responsible person.');
		}else{
			$('#resp').html('');
			$.ajax({
				url: "{{URL::to('admin/add-action-from-add-theme')}}",
				type: "POST",
				dataType: "JSON",
				data: {orgId:orgId,actionText:actionText,userId:userId,"_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					if (response.status == 200) {
						$('.multiselect-container').append('<li class="active"><a tabindex="0"><label class="checkbox" title="'+response.actionText+'"><input type="checkbox" value="'+response.actionId+'">'+response.actionText+'</label></a></li>');	

						if($('.multiselect-selected-text').html() == 'Select action'){
							$('.multiselect-selected-text').html(response.actionText);
						}else{
							var multiText = $('.multiselect-selected-text').html();

							var allSelectedText = multiText.match(/All selected/g);

							

							if (allSelectedText != null) {
								var count = multiText.substring(
								    multiText.lastIndexOf("(") + 1, 
								    multiText.lastIndexOf(")")
								);
								var totalCount = parseInt(count)+1;
								$('.multiselect-selected-text').html('All selected ('+totalCount+')');
							}else{
								var selectedText = multiText.match(/selected/g);
								if (selectedText != null) {
									var count = multiText.charAt(0);
									var totalCount = parseInt(count)+1;
									$('.multiselect-selected-text').html(totalCount +' selected');
								}else{
									$('.multiselect-selected-text').html('2 selected');
								}
							}

							// $('.multiselect-selected-text').append(','+response.actionText);
						}

						$('#theme').append('<option selected value="'+response.actionId+'">'+response.actionText+'</option>');
						$('#addAction').val('');
						$('#resUsers').val('');
					}
				}
			});		
		}
	});
</script>

	<script type="text/javascript">		
		$(function () {
			$('#theme').multiselect({				
				includeSelectAllOption: true,
				enableFiltering:true,
				nonSelectedText: 'Select action',
				numberDisplayed: 1
			});			
		});
	</script>

	<script type="text/javascript">
		$('#organisation').on('change', function() {
			$('.add-action').show();
			// $('.tt').show();

			$('#theme').html(" ");
			
			$('#theme').multiselect('refresh');			
			$('.multiselect-selected-text').html('Select action');

			var orgId = $('#organisation').val();

			$('#orgId').val(orgId);

			$.ajax({
				type: "POST",
				url: "{{route('get-actions-by-ajax')}}",
				data: {orgId:orgId, "_token":'<?php echo csrf_token()?>'},
				success: function(response)
				{
					var obj = JSON.parse(response);
					var htmlContainer = obj.htmlContainer;
					var htmlOption    = obj.htmlOption

					$(".action-text").hide();
					$('#new-theme-input').hide();
					$('#theme').html('');
					$('.multiselect-container').html('');
					$('#theme').append(htmlOption);
					$('.multiselect-container').append(htmlContainer);			
				}
			});
		});
	</script>

	<script type="text/javascript">
		function validation()
		{

			$('.error-message').show();

			var date_opened    	= $('#date-opened').val();
			var title           = $('#title').val();	
			var description		= $('#description').val();
			var type 		    = $('#type').val();
			var organisation 	= $('#organisation').val();
			var status 		    = $('#status').val();
			var initial_likelihood  = $('#initial-likelihood').val();
			var initial_consequence = $('#initial-consequence').val();
			var current_likelihood  = $('#current-likelihood').val();
			var current_consequence = $('#current-consequence').val();
			var linked_action 		= $('.linked-action').val();
			
			if($.trim(date_opened) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please select date.');
			}
			else if($.trim(organisation) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please select organisation.');
			}
			else if($.trim(title) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please enter title.');
			}
			else if($.trim(linked_action) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please enter linked action.');
			}
			else if($.trim(description) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please enter description.');
			}
			else if($.trim(type) == '' )
			{
				$('#resp').html('');
				$('#resp').html('Please select type.');
			}			
			else if($.trim(status) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please select status.');
			}
			else if($.trim(initial_likelihood) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please enter initial likelihood.');
			}
			else if($.trim(initial_consequence) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please enter initial consequence.');
			}
			else if($.trim(current_likelihood) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please enter current likelihood.');
			}
			else if($.trim(current_consequence) == '')
			{
				$('#resp').html('');
				$('#resp').html('Please enter current consequence.');
			}			
			else
			{
				$('.save').prop("disabled", true);
				$('#add-theme-form').submit();
			}
		}
	</script>

	<script type="text/javascript">
		$(document).on('focus', '.date',function(){
			$(this).datepicker({
				todayHighlight:true,
				dateFormat: 'dd-mm-yy',
				minDate:'0',
				autoclose:true
			});
		});
	</script>