<!-- header -->
@section('title', 'Add User')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="dot-section">
				<h2> Add Staff </h2>
				<div class="form-section">
					<form id="add-staff-form" action="{!!route('admin-user.store')!!}" method="post">
						{{csrf_field()}}

						<input type="hidden" name="orgId" value="{{$orgId}}">
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									
									<input class="charCount" id="name"  type="text" name="name" placeholder="Name" maxlength="50">
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group">

									<input class="charCount" id="email" type="email" name="email" placeholder="Email" autocomplete="off" maxlength="50">
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group">
									
									<input class="charCount" id="password" type="password" name="password" placeholder="Password" maxlength="50">
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group">
									
									<input class="charCount" id="confirm-password" type="password" name="password_confirmation" placeholder="Confirm password" maxlength="50">
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group">

									<select name="officeId" id="office" style="background:url(http://chetaru.gottadesigner.com/tribe365/public/images/down-arrow.png)no-repeat 95% center;">
										<option value="">select office</option>
										@foreach($offices as $officeValue)
										<option value="{{$officeValue->id}}">{{ucfirst($officeValue->office)}}</option>
										@endforeach					
									</select>
								</div>		
							</div>
							<div class="col-md-6">
								<div class="form-group">

									<select name="departmentId" id="department" style="background:url(http://chetaru.gottadesigner.com/tribe365/public/images/down-arrow.png)no-repeat 95% center;">
										<option value="">select department</option>
										@foreach($departments as $value)
										<option value="{{$value->id}}">{{strtoupper($value->department)}}</option>
										@endforeach				
									</select>
								</div>								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="Create-btn-cont">
									<button type="button" onclick="validation()" class="btn save">Add</button>
								</div>

							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="error-message" style="display: none;">
				<span id="resp"></span>
			</div>
		</div>
	</div>
</main>

<script type="text/javascript">

	function validation() {

		$('.error-message').show();

		var name    		= $('#name').val();
		var email           = $('#email').val();
		var password        = $('#password').val();
		var confirmPassword = $('#confirm-password').val();
		var office 			= $('#office').val();
		var department 		= $('#department').val();

       //check email type
       is_email = isEmail(email);



       var empty = false;
       $('.charCount').each(function() {
       	if ($(this).val().length > 50){
       		empty = true;
       	}
       });

       if (empty){

       	$('#resp').html('');
       	$('#resp').html('All input fields should be less than 50 characters.');
       }else if($.trim(name) == ''){

       	$('#resp').html('');
       	$('#resp').html('Please enter Name.');
       }else if($.trim(email) == '' || !is_email){

       	$('#resp').html('');
       	$('#resp').html('Please enter Valid Email.');
       }else if($.trim(password) == ''){

       	$('#resp').html('');
       	$('#resp').html('Please enter Password.');
       }else if (password !=confirmPassword){

       	$('#resp').html('');
       	$('#resp').html('Password and Confirm-password are not matching. Please try again.');
       }else if ($.trim(office) == '' ){

       	$('#resp').html('');
       	$('#resp').html('Please select Office.');
       }else if ($.trim(department) == ''){

       	$('#resp').html('');
       	$('#resp').html('Please select Department.');
       }else{

       	$('.save').prop("disabled", true);

       	$.ajax({
       		type: "POST",
       		url: "{!!URL::to('admin/search-email')!!}",				
       		data: {email:email,"_token":'<?php echo csrf_token()?>'},
       		success: function(response) {
       			console.log('email '+response);

       			if(response)
       			{
       				$('.save').prop("disabled", false);
       				$('#resp').html('');
       				$('#resp').html('Email Already Exists.');
       			}
       			else
       			{
       				$('.save').prop("disabled", true);
       				$('#add-staff-form').submit();
       			}
       		}
       	});       	
       }
   }

   function isEmail(email)
   {
   	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
   	return regex.test(email);
   }

</script>

@include('layouts.adminFooter')