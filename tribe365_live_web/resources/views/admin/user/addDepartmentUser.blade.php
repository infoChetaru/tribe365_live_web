<!-- header -->
@section('title', 'Add User')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">

			<div class="dot-section">
				<div class="message-cls" align="center">                            
					@if(session('message'))
					<div class="alert alert-success" role="alert">
						{{session('message')}}
					</div>
					@endif
					@if(session('error'))
					<div class="alert alert-danger" role="alert">
						{{session('error')}}
					</div>
					@endif
				</div> 
				<h2> Add Staff </h2>
				<div class="adit-btn">
					<button type="button" class="btn" data-toggle="modal" data-target="#myModal">Add staff via csv file</button>
				</div>
				<div class="form-section">
					<form id="add-staff-form" action="{!!route('admin-user.store')!!}" method="post">
						{{csrf_field()}}

						<input type="hidden" name="orgId" id="orgId" value="{{$orgId}}">
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									
									<input class="charCount" id="name"  type="text" name="name" placeholder="First Name" maxlength="50">
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group">
									
									<input class="charCount" id="lastName"  type="text" name="lastName" placeholder="Last Name" maxlength="50">
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group">

									<input class="charCount" id="email" type="email" name="email" placeholder="Email" autocomplete="off" maxlength="50">
								</div>									
							</div>

							<!-- <div class="col-md-6">
								<div class="form-group">
									
									<input class="charCount" id="password" type="password" name="password" placeholder="Password" maxlength="50">
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group">
									
									<input class="charCount" id="confirm-password" type="password" name="password_confirmation" placeholder="Confirm password" maxlength="50">
								</div>									
							</div> -->
							
							<div class="col-md-6">
								<div class="form-group">

									<select name="officeId" id="office" style="background:url(http://chetaru.gottadesigner.com/tribe365/public/images/down-arrow.png)no-repeat 95% center;">
										<option value="">select office</option>
										@foreach($offices as $officeValue)
										<option value="{{$officeValue->id}}">{{ucfirst($officeValue->office)}}</option>
										@endforeach					
									</select>
								</div>		
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<select name="departmentId" id="department" style="background:url(http://chetaru.gottadesigner.com/tribe365/public/images/down-arrow.png)no-repeat 95% center;">
										<option value="">Select department</option>
										@foreach($departments as $value)
										<option value="{{$value->id}}">{{ucfirst($value->department)}}</option>
										@endforeach				
									</select>
								</div>								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="Create-btn-cont">
									<button type="button" onclick="validation()" class="btn save">Add</button>
								</div>
							</div>
						</div>
					</form>
				</div>

				<!-- <div>
					<form action="{{route('add-user-csv')}}" method="POST" enctype="multipart/form-data" accept=".csv">
						{{csrf_field()}}
						<h4>Import CSV file</h4>
						<label>Select File</label>
						<input type="file" name="csv_file">					
						<button type="button" class="btn"> submit </button>
					</form>					
				</div>
				<div>
					<h5> Export CSV</h5>
					<button> <a href="{{URL::to('admin/user-list-export'.'/'.base64_encode($orgId))}}">Export</a></button>		
				</div>
			</div>-->
			<div class="error-message" style="display: none;">
				<span id="resp"></span>
			</div> 
		</div>
	</div>
</main>
<!-- Modal example -->
<div class="modal fade add-catalog-section" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
				<h4>Add staff via CSV file</h4>
				<div class="add-catalog-content">
					<form id="csv-form" action="#" enctype="multipart/form-data" accept=".csv" class="md-form">	
						{{csrf_field()}}	
						
						<ul>
							<li>
								<p>1. Export the list of existing staff</p><a class="export-btn btn" href="{{URL::to('admin/user-list-export'.'/'.base64_encode($orgId))}}">Export</a>
							</li>
							<li>
								<p class="p-color" style="color: #7F7F7F;">2. Update the staff list by adding new entries updating the existing one.</p>
							</li>
							<li> <p class="p-color" style="color: #7F7F7F;">3. Upload the updated staff list </p>
								<div class="file-field" >
									<input type="hidden" name="orgId" value="{{$orgId}}">
									<input type="file" id="real-input" name="csv_file" disabled="true">
									<!-- <input class="browse-btn" value="Upload" type="text"> -->
									<a href="#" class="btn browse-btn btn-color" style="background:#7F7F7F">Upload</a>
								</div>
								<span class="file-info"> </span>
								
							</li>
						</ul>
						<button style="background:#7F7F7F" id="csv-btn" type="button" class="btn mdl-submt-btn-color" disabled=""> submit </button>	
					</form>

					<div class="error-message-csv" style="display: none;">
						<span id="resp-csv"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$("#csv-btn").click(function(e){
		$('#myModal').modal({backdrop: 'static', keyboard: false})

		$('.error-message-csv').show();

		var org_logo_count = document.getElementById("real-input").files.length;

		if(org_logo_count== 0)
		{			
			$('#resp-csv').html('');
			$('#resp-csv').html('Please upload staff csv file.');
		}
		else
		{
			$('#resp-csv').html('');

			e.preventDefault();
			$.ajax({
				url: "{{route('add-user-csv')}}",
				type: 'POST',
				data: new FormData($("#csv-form")[0]),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data)
				{					
					if(data.status=='error')
					{						
						$('#resp-csv').html('');
						$('#resp-csv').html('Please upload CSV file.');
					}
					else
					{
						$('.message-cls').html('<div class="alert alert-success" role="alert">Record added successfully.</div>');
						$('#myModal').modal('hide');
						$('.error-message').hide();						
					}
				}    
			});
		} 
	});	
</script>

<script type="text/javascript">
	//file upload button enable on click
	$(document).ready(function(){
		$('input[type="file"]').change(function(){
			$('.mdl-submt-btn-color').prop("disabled", false);
			$(".mdl-submt-btn-color").css("background", "");
		});
	});
</script>
<!-- Modal end -->
<!-- on export button click enable upload -->
<script type="text/javascript">
	$(".export-btn").click(function(e){			
		$('#real-input').prop("disabled", false);
		$(".p-color").css("color", "");
		$(".btn-color").css("background", "");
	});	
</script>
<!-- on modal close reset form -->
<script type="text/javascript">
	$('#myModal').on('hidden.bs.modal', function () {
		console.log('reset')
		$('#real-input').prop("disabled", true);
		$(this).find('form').trigger('reset');
	});
</script>


<script type="text/javascript">
	function validation()
	{

		$('.error-message').show();

		var name    		= $('#name').val();
		var lastName   		= $('#lastName').val();
		var email           = $('#email').val();
		/*var password        = $('#password').val();
		var confirmPassword = $('#confirm-password').val();*/
		var office 			= $('#office').val();
		var department 		= $('#department').val();

       //check email type
       is_email = isEmail(email);

       var empty = false;
       $('.charCount').each(function() {
       	if ($(this).val().length > 50){
       		empty = true;
       	}
       });

       if (empty){

       	$('#resp').html('');
       	$('#resp').html('All input fields should be less than 50 characters.');
       }else if($.trim(name) == ''){

       	$('#resp').html('');
       	$('#resp').html('Please enter first name.');
       }else if($.trim(lastName) == ''){

       	$('#resp').html('');
       	$('#resp').html('Please enter last name.');
       }else if($.trim(email) == '' || !is_email){

       	$('#resp').html('');
       	$('#resp').html('Please enter Valid Email.');
       }

      /* else if($.trim(password) == ''){

       	$('#resp').html('');
       	$('#resp').html('Please enter Password.');
       }else if (password !=confirmPassword){

       	$('#resp').html('');
       	$('#resp').html('Password and Confirm-password are not matching. Please try again.');
       }*/

       else if ($.trim(office) == '' ){

       	$('#resp').html('');
       	$('#resp').html('Please select Office.');
       }else if ($.trim(department) == ''){

       	$('#resp').html('');
       	$('#resp').html('Please select Department.');
       }else{

       	$('.save').prop("disabled", true);

       	$.ajax({
       		type: "POST",
       		url: "{!!URL::to('admin/search-email')!!}",				
       		data: {email:email,"_token":'<?php echo csrf_token()?>'},
       		success: function(response) {
       			console.log('email '+response);

       			if(response)
       			{
       				$('.save').prop("disabled", false);
       				$('#resp').html('');
       				$('#resp').html('Email Already Exists.');
       			}
       			else
       			{
       				$('.save').prop("disabled", true);
       				$('#add-staff-form').submit();
       			}
       		}
       	});       	
       }
   }

   function isEmail(email)
   {
   	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
   	return regex.test(email);
   }

</script>
<!-- 
<script type="text/javascript">
	$('#office').on('change', function() {
		$('#department option').remove();
		var orgId 	 = $('#orgId').val();
		var officeId = $('#office').val();
		
		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getDepartmentByOfficecIdforGraph')!!}",
			data: {officeId:officeId,orgId:orgId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{
				$('#department').append('<option value="" selected>Select Department</option>');
				$('#department').append(response);
			}
		});
	});
</script> -->

<script type="text/javascript">
	const uploadButton = document.querySelector('.browse-btn');
	const fileInfo = document.querySelector('.file-info');
	const realInput = document.getElementById('real-input');

	uploadButton.addEventListener('click', (e) => {
		realInput.click();
	});

	realInput.addEventListener('change', () => {
		const name = realInput.value.split(/\\|\//).pop();
		const truncated = name.length > 20 
		? name.substr(name.length - 20) 
		: name;

		fileInfo.innerHTML = truncated;
	});
</script>
@include('layouts.adminFooter')