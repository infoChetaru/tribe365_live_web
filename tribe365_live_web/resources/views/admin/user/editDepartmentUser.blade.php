<!-- header -->
@section('title', 'Edit User')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="dot-section">
				<h2> Edit Staff </h2>
				<div class="form-section">

					{{ Form::model($users, ['method' => 'PUT', 'route' => array('admin-user.update', base64_encode($users->id)),'id'=>'add-staff-form']) }}

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<input class="charCount" id="name"  type="text" name="name" placeholder="Name" maxlength="50" value="{{$users->name}}">
							</div>									
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<input class="charCount" id="lastName"  type="text" name="lastName" placeholder="Last Name" maxlength="50" value="{{$users->lastName}}">
							</div>									
						</div>
						<input type="hidden" name="orgId" value="{{$users->orgId}}">

						<!-- <div class="col-md-6">
							<div class="form-group">
								<input class="charCount" id="password" type="password" name="password" placeholder="Password" maxlength="50" value="{{base64_decode($users->password2)}}">
							</div>									
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<input class="charCount" id="confirm-password" type="password" name="password_confirmation" placeholder="Confirm password" maxlength="50" value="{{base64_decode($users->password2)}}">
							</div>									
						</div> -->

						<!-- <div class="col-md-6">
							<div class="form-group">
								<select name="orgId" id="organisation" style="background:url({{asset('public/images/down-arrow.png')}})no-repeat 95% center;">
									<option value="" disabled="">Select Organisation</option>
									@foreach($organisations as $org)
									<option {{($org->id==$users->orgId)?'selected':''}} value="{{$org->id}}">{{ucfirst($org->organisation)}}</option>
									@endforeach			
								</select>
							</div>		
						</div> -->
						<div class="col-md-6">
							<div class="form-group">

								<select name="officeId" id="office" style="background:url({{asset('public/images/down-arrow.png')}})no-repeat 95% center;">
									<option value="" disabled="">Select Office</option>
									@foreach($allOffices as $officeVal)	
										<option {{($officeVal->id==$offices->id)?'selected':''}} value="{{$officeVal->id}}">{{$officeVal->office}}</option>
									@endforeach
								</select>
							</div>		
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<select name="departmentId" id="department" style="background:url({{asset('public/images/down-arrow.png')}})no-repeat 95% center;">
									<option value="" disabled="" selected="">Select Department</option>								
									@foreach($customeDepartments as $cusDep)						
										<option {{($cusDep->id==$departments->departmentId)?'selected':''}}  value="{{$cusDep->id}}">{{$cusDep->department}}</option>
									@endforeach
								</select>
							</div>								
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="Create-btn-cont">
								<button type="button" onclick="validation()" class="btn save">Save</button>
							</div>
						</div>
					</div>
					{{Form::close()}}
				</div>
			</div>
			<div class="error-message" style="display: none;">
				<span id="resp"></span>
			</div>
		</div>
	</div>
</main>

<script type="text/javascript">

	function validation()
	{
		$('.error-message').show();

		var name    		= $('#name').val();		
		var lastName    		= $('#lastName').val();		
		// var password        = $('#password').val();
		// var confirmPassword = $('#confirm-password').val();
		var organisation    = $('#organisation').val();
		var office 			= $('#office').val();
		var department 		= $('#department').val();

		// console.log(password)

		var empty = false;
		$('.charCount').each(function() {
			if ($(this).val().length > 50){
				empty = true;
			}
		});

		if(empty){

			$('#resp').html('');
			$('#resp').html('All input fields should be less than 50 characters.');
		}else if($.trim(name)==''){

			$('#resp').html('');
			$('#resp').html('Please enter first name.');
		}else if($.trim(lastName)==''){

			$('#resp').html('');
			$('#resp').html('Please enter last name.');
		}


		/*else if($.trim(password) == ''){

			$('#resp').html('');
			$('#resp').html('Please enter Password.');
		}else if (password !=confirmPassword){

			$('#resp').html('');
			$('#resp').html('Password and Confirm-password are not matching. Please try again.');
		}*/


		/*else if ($.trim(organisation) == ''){

			$('#resp').html('');
			$('#resp').html('Please select Office.');
		}*/else if ($.trim(office) == ''){

			$('#resp').html('');
			$('#resp').html('Please select Office.');
		}else if ($.trim(department) == ''){

			$('#resp').html('');
			$('#resp').html('Please select Department.');
		}else{
			$('.save').prop("disabled", true);

			$('#add-staff-form').submit();
		}
	}
</script>

<!-- <script type="text/javascript">
	$('#organisation').on('change', function() {
		$('#office option').remove();
		var orgId = $('#organisation').val();
		
		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getOfficesByOrgId')!!}",				
			data: {orgId:orgId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{
				$('#office').append(response);
			}
		});
	});
</script>
 -->
@include('layouts.adminFooter')