@section('title', 'Manage COT Personality Type')
@include('layouts.adminHeader')
<main class="main-content cot_inner_pages">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">						
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                                
								{{session('message')}}
							</div>
							@endif
						</div>
						<div class="value-list menual_fun_lens_wrap functional-lens">
							<div class="menual_fun_lens_tabs">
								<!-- <h2 class="tab_tbl_ttl">COT-Functional Lens Values</h2> -->
								<h2 class="tab_tbl_ttl">Personality Type</h2>
							</div>
							<ul class="nav nav-tabs">		
								<li><a data-toggle="tab" class="active" href="#menu1">Initial Value Table</a></li>
								<li><a data-toggle="tab" href="#menu2">Insight For Strength Value Table</a></li>
								<li><a data-toggle="tab" href="#menu3">Tribe Tips Table</a></li>
							</ul>


							<div class="tab-content">
								
								<div id="menu1" class="tab-pane fade in active show">
									<table>
										<tr>
											<th>Value</th>
											<th>Title</th>
											<th>Description</th>	
											<th>Action</th>								
										</tr>
										<?php $counter=2; $editCount=1; ?>
										@foreach($initialValueList as $value)

										<form id="form" action="{{URL::to('admin/update-cot-fun-initial-value')}}" method="post">
											{{csrf_field()}}
											<input type="hidden" name="initialValueId" value="{{base64_encode($value->id)}}">
											<tr class="table-class <?php if($counter % 2 == 0) echo 'grey-css'; ?>">

												@if($editCount <= 8)

												<td><input type="text" name="iValue" value="{{ucfirst($value->value)}}"></td>
												<td><input type="text" name="iTitle" value="{{ucfirst($value->title)}}"></td>

												@else
												<td>{{ucfirst($value->value)}}</td>
												<td>{{ucfirst($value->title)}}</td>
												@endif

												<td><textarea name="description">{{ucfirst($value->description)}}</textarea></td>

												<td>
													<div class="editable">
														<button class="editbtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
														<button class="submitbtn" style="display: none;" type="submit">submit</button>
													</div>
												</td>
											</tr>		
										</form>		
										<?php $counter++; $editCount++; ?>		
										@endforeach
									</table>
								</div>

								<div id="menu2" class="tab-pane fade">
									<table>
										<tr>
											<th>Value</th>
											<th>Title</th>
											<th>Start</th>
											<th>End</th>
											<th>Positive</th>	
											<th>Allowable Weakness</th>		
											<th>Action</th>						
										</tr>
										<?php $counter=2; ?>	
										@foreach($insightForStrengthValueList as $valueList)
										<form id="form" action="{{URL::to('admin/update-cot-fun-strength-value')}}" method="post">
											{{csrf_field()}}
											<input type="hidden" name="insightForStrengthValueId" value="{{base64_encode($valueList->id)}}">
											<tr class="table-class <?php if($counter % 2 == 0) echo 'grey-css'; ?>">
												<td>{{ucfirst($valueList->section)}}={{$valueList->start}}-{{$valueList->end}}</td>
												<td>{{ucfirst($valueList->title)}} {{$valueList->title_key}}</td>	
												<td><input type="text" name="start" value="{{$valueList->start}}"></td>
												<td><input type="text" name="end" value="{{$valueList->end}}"></td>
												<td>
													<input type="text" name="positives" value="{{ucfirst($valueList->positives)}}">
												</td>
												<td>
													<input type="text" name="allowableWeaknesses" value="{{ucfirst($valueList->allowable_weaknesses)}}">
												</td>
												<td>
													<div class="editable">
														<button class="editbtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
														<button class="submitbtn" type="submit">submit</button>
													</div>
												</td>
											</tr>		
										</form>	
										<?php $counter++; ?>				
										@endforeach
									</table>
								</div>

								<div id="menu3" class="tab-pane fade">
									<table>
										<tr>
											<th>Value</th>
											<th>Title</th>
											<th>Summary</th>	
											<th>Seek</th>		
											<th>Persuade</th>	
											<th>Opposite</th>	
											<th>Action</th>														
										</tr>
										<?php $counter=2; ?>	
										@foreach($tribeTipsList as $tipsValue)	

										<form id="form" action="{{URL::to('admin/update-cot-tribe-tips-summary')}}" method="post">
											{{csrf_field()}}									
											<input type="hidden" name="tribeTipsId" value="{{base64_encode($tipsValue->id)}}">
											<tr class="table-class <?php if($counter % 2 == 0) echo 'grey-css'; ?>">
												<td>{{$tipsValue->value}}</td>
												<td>{{ucfirst($tipsValue->title)}}</td>

												<td>
													<textarea name="summary">{{ucfirst($tipsValue->summary)}}</textarea>
												</td>

												<td style="cursor: pointer;" data-toggle="modal" data-toggle="tooltip" title="click to edit">

													<a href="{{URL::to('admin/edit-cot-fun-tribe-tips-value/'.base64_encode($tipsValue->id).'/'.base64_encode('seek'))}}">
														@foreach($tipsValue->seek_value_list as $seekV)
														<li style="color:#f00"><span style="color:#000;">{{$seekV->value}}</span></li>
														@endforeach
													</a>

												</td>
												
												<td style="cursor: pointer;" data-toggle="modal" data-toggle="tooltip" title="click to edit">
													<a href="{{URL::to('admin/edit-cot-fun-tribe-tips-value/'.base64_encode($tipsValue->id).'/'.base64_encode('persuade'))}}">
														@foreach($tipsValue->persuade_value_list as $persuadeV)
														<li style="color:#f00"><span style="color:#000;">{{$persuadeV->value}}</span></li>
														@endforeach	
													</a>									
												</td>
												<td>{{ucfirst($tipsValue->opposite)}}</td>
												<td>
													<div class="editable">
														<button class="editbtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
														<button class="submitbtn" style="display: none;" type="submit">submit</button>
													</div>
												</td>
											</tr>
										</form>
										<?php $counter++; ?>	
										@endforeach
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<!-- <div class="organ-page-nav">
			{{--$initialValueList->links('layouts.pagination')--}}
		</div> -->
	</div>
</main>

<script type="text/javascript">
	$(document).ready(function() {

		$('.submitbtn').hide();
		$(".table-class").find("input,select,textarea").prop("disabled", true);		

	});
</script>
<script type="text/javascript">
	$('.editbtn').on('click',function(){

		$(this).closest('.editable').find('.submitbtn').show();
		$(this).closest('.editable').find('.editbtn').hide();	
		$(this).closest('.table-class').find('input,select,textarea').prop('disabled',false);	
	})
</script>
<!-- <script type="text/javascript">
	$(document).ready(function() {
		$("textarea").each(function(){
			var scroll_height = $(this).get(0).scrollHeight;
			$(this).css('height', scroll_height + 'px');
		});		
	});
</script> -->
@include('layouts.adminFooter')