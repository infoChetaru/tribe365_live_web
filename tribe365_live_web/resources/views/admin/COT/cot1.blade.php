<!-- header -->
@section('title', 'graphic table ')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="organistiondetail-section">
				<div class="row">
					<div class="col-md-12">
						<div class="prof-acount-section">
							<div class="compy-logo">
								<img src="http://production.chetaru.co.uk/tribe365/public/images/infosys-logo.png" class="mCS_img_loaded">
							</div>
							<div class="edit-box">
								<div class="profile-btn">
									<a href="#"> <img src="http://production.chetaru.co.uk/tribe365/public/images/profile-icon.png"> Profile </a>
								</div>
								<div class="account-btn">
									<a href="http://production.chetaru.co.uk/tribe365/admin/admin-organisation/MTA0/edit"> <img src="http://production.chetaru.co.uk/tribe365/public/images/account-icon.png" class="mCS_img_loaded"> Account </a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="team-role-map">
					<h1> Cot</h1>
					<div class="row">
						<div class="col-md-6">
							<div class="team-map-box">
								<img src="http://production.chetaru.co.uk/tribe365/public/images/temp-map-icon.png">
								<h4> Team Role Map </h4>
							</div>
						</div>
						<div class="col-md-6">
							<div class="team-map-box">
								<img src="http://production.chetaru.co.uk/tribe365/public/images/funtion-icon.png">
								<h4> Functional Lens  </h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
@include('layouts.adminFooter')