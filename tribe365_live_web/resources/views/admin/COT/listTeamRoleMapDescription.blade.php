@section('title', 'Team Role Map description')
@include('layouts.adminHeader')

<style>
	.cotCategory{
		-webkit-appearance: radio;
	}
</style>

<main class="main-content cot_inner_pages">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">						
						<div id='success-msg' align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                                    
								{{session('message')}}
							</div>
							@endif
						</div>
						<div class="prof-acount-section report-belief">
							<div class="compy-logo">
								<h5> Team Role Map Description </h5>
							</div>
						</div>

						<div class="value-list menual_fun_lens_wrap">
							<table>
								<tr>
									<th>Title</th>
									<th>Value</th>
									<th>Description</th>	
									<!-- <th>Category</th>						 -->
									<th>Action</th>					
								</tr>

								<?php $counter=2; ?>
								@foreach($teamRoleMap as $value)
								
								<form action="{{URL::to('admin/update-cot-team-role-description')}}" method="POST">
									{{csrf_field()}}
									<input type="hidden" name="maperId" value="{{base64_encode($value->id)}}">
									<tr class="table-class <?php if($counter % 2 == 0) echo 'grey-css'; ?>">

										<td><textarea name="maperName"> {{ucfirst($value->maper)}}</textarea></td>

										<td><textarea name="sortDescription"> {{ucfirst($value->short_description)}}</textarea></td>
										<td>
											<textarea name="longDescription" class="description">{{ucfirst($value->long_description)}} </textarea>
										</td>
										<!-- <td>
											@foreach($teamRoleMapCategory as $cate)
												<input type="radio" class="cotCategory" name="cotCategory" value="{{$cate->id}}" {{($value->categoryId == $cate->id) ? 'checked' : ''}} >
												{{$cate->category_name}}<br>
											@endforeach
										</td> -->
										<td>
											<div class="editable">
												<button class="editbtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
												<button style="display: none;" class="submitbtn" type="submit">submit</button>
											</div>
										</td>										
									</tr>
								</form>
								<?php $counter++; ?>
								@endforeach						
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
	<div class="error-message" style="display: none;">
		<span id="resp"></span>
	</div>	
</main>

<script type="text/javascript">
	$(document).ready(function() {

		$('.submitbtn').hide();
		$(".table-class").find("textarea").prop("disabled", true);	
		// $("input[type=radio]").attr('disabled', true);		
	});
</script>
<script type="text/javascript">
	$('.editbtn').on('click',function(){

		$(this).closest('.editable').find('.submitbtn').show();
		$(this).closest('.editable').find('.editbtn').hide();	
		$(this).closest('.table-class').find('textarea').prop('disabled',false);	
		// $(this).closest('.table-class').find('input.cotCategory').prop('disabled',false);	
	})
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("textarea").each(function(){
			var scroll_height = $(this).get(0).scrollHeight;
			$(this).css('height', scroll_height + 'px');
		});		
		$(".table-class").each(function(){
	  var h = $(this).find('textarea.description').outerHeight(true);
	  $(this).find('textarea').height(h);
	});
	});
</script>

@include('layouts.adminFooter')