<!-- header -->
@section('title', 'team role mape')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="organistiondetail-section">
				<div class="row">
					<div class="col-md-12">
						<div class="prof-acount-section">
							<div class="compy-logo">
								<img src="http://production.chetaru.co.uk/tribe365/public/images/infosys-logo.png" class="mCS_img_loaded">
							</div>
							<div class="edit-box">
								<div class="profile-btn">
									<a href="#"> <img src="http://production.chetaru.co.uk/tribe365/public/images/profile-icon.png"> Profile </a>
								</div>
								<div class="account-btn">
									<a href="http://production.chetaru.co.uk/tribe365/admin/admin-organisation/MTA0/edit"> <img src="http://production.chetaru.co.uk/tribe365/public/images/account-icon.png" class="mCS_img_loaded"> Account </a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cot-tab">
					<h2> Team Role Map </h2>
					<ul class="nav nav-tabs" role="tablist">
						<li class="active">
							<a href="#individual" class="active"  role="tab" data-toggle="tab">
								<img class="wite-icon mCS_img_loaded" src="http://production.chetaru.co.uk/tribe365/public/images/individual-icon.png">
								<span> Individual</span>
								
							</a>
						</li> 
						<li class="">
							<a href="#offices" data-toggle="tab" aria-expanded="false" class="">
								<img class="wite-icon mCS_img_loaded" src="http://production.chetaru.co.uk/tribe365/public/images/tem-icon.png">
								<span> Team </span>
							</a>
						</li>        
					</ul>
				</div>
				<div class="cot-tab-detail-section tab-content">
					<div class="tab-pane" id="individual">
						<div class="individual-content team-content">
							<div class="individual-cate">
								<div class="cate-left">
									<h4> Organisations</h4>
									<span>  Organisations </span>
									<span> Coordinator </span>
								</div>
								<div class="cate-repeat">
								<div class="cate-right">
									<h5> Oliver</h5>
									<span class="green">  1 </span>
									<span class="orange"> 4 </span>
								</div>
								<div class="cate-right">
									<h5> Dean </h5>
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right">
									<h5> Ryan </h5>
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right ">
									<h5> Mathew </h5>
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right ">
									<h5> Steven</h5>
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right ">
									<h5> Amy </h5>
									<span class="white">   </span>
									<span class="orange"> R </span>
								</div>
								<div class="cate-right ">
									<h5> Kayleigh </h5>
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right ">
									<h5> Kyle </h5>
									<span class="green">  2 </span>
									<span class="orange"> R </span>
								</div>
								<div class="cate-right ">
									<h5> Beth</h5>
									<span class="green">  2 </span>
									<span class="orange"> R </span>
								</div>
								</div>
							</div>
							<div class="individual-cate">
								<div class="cate-left ">
									<h4> Controllers</h4>
									<span>  Implementer </span>
									<span> Completer </span>
								</div>
								<div class="cate-repeat">
								<div class="cate-right">
									<span class="green">  1 </span>
									<span class="orange"> 4 </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="orange"> R </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right">
									<span class="green">  2 </span>
									<span class="orange"> R </span>
								</div>
								<div class="cate-right">
									<span class="green">  2 </span>
									<span class="orange"> R </span>
								</div>
								</div>
							</div>
							<div class="individual-cate">
								<div class="cate-left">
									<h4> Adviseres</h4>
									<span>  Monitor Evaluator </span>
									<span> Team worker </span>
								</div>
								<div class="cate-repeat">
								<div class="cate-right">
									<span class="green">  1 </span>
									<span class="orange"> 4 </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="orange"> R </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right">
									<span class="green">  2 </span>
									<span class="orange"> R </span>
								</div>
								<div class="cate-right">
									<span class="green">  2 </span>
									<span class="orange"> R </span>
								</div>
								</div>
							</div>
							<div class="individual-cate">
								<div class="cate-left">
									<h4> Explorers</h4>
									<span>  Plant </span>
									<span> Resource Investigator </span>
								</div>
								<div class="cate-repeat">
								<div class="cate-right">
									<span class="green">  1 </span>
									<span class="orange"> 4 </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="orange"> R </span>
								</div>
								<div class="cate-right">
									<span class="white">   </span>
									<span class="white">  </span>
								</div>
								<div class="cate-right">
									<span class="green">  2 </span>
									<span class="orange"> R </span>
								</div>
								<div class="cate-right">
									<span class="green">  2 </span>
									<span class="orange"> R </span>
								</div>
								</div>
							</div>
							<div class="individual-cate">
								<div class="cate-left">
								<h4> Key </h4>
								</div>
								<div class="key-box">
									<a href="#" class="btn green"> 1st Preference </a>
									<a href="#" class="btn yallow"> 2nd Preference </a>
									<a href="#" class="btn blue"> 3rd Preference </a>
									<a href="#" class="btn orange"> R = Reserve Role </a>
								</div>
							</div>
							<div class="view-content-section">
								<div class="plan-view">
									<table>
										<tr class="green">
											<th>Plan</th>
											<th><span class="count"> 01 </span>Goals</th>
											<th><span class="count"> 02 </span> Ideas</th>
											<th><span class="count"> 03 </span> Plan</th>
										</tr>
										<tr>
											<td> Direction</td>
											<td> Shaper</td>
											<td> Plant</td>
											<td> Monitor Evaluator</td>
										</tr>
										<tr>
											<td> Connection</td>
											<td> Coordinator</td>
											<td> Resource Investigator</td>
											<td> Implementer </td>
										</tr>
									</table>
								</div>
								<div class="do-view">
									<table>
										<tr class="orange">
											<th>DO</th>
											<th> <span class="count"> 04 </span> Contacts</th>
											<th> <span class="count"> 05 </span> Organise</th>
										</tr>
										<tr>
											<td> Direction</td>
											<td> </td>
											<td> Implementer </td>

										</tr>
										<tr>
											<td> Connection</td>
											<td> Resource Investigator Team-Worker</td>
											<td> Coordinator </td>
										</tr>
									</table>
								</div>
								<div class="finish-view">
									<table>
										<tr class="red">
											<th>Finish</th>
											<th><span class="count"> 06 </span> Follow Through</th>
										</tr>
										<tr>
											<td> Direction</td>
											<td> Completer </td>

										</tr>
										<tr>
											<td> Connection</td>
											<td> Implementer </td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
@include('layouts.adminFooter')