@section('title', 'List COT Personality Type')
@include('layouts.adminHeader')
<main class="main-content cot_inner_pages">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">						
						<div id='success-msg' align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                                    
								{{session('message')}}
							</div>
							@endif
						</div>

						<div class="loader-img" style="display: none;width: 70px; padding-top: 39px"><img src="{{asset('public/images/loader.gif')}}"></div>
						
						<div class="prof-acount-section report-belief">
							<div class="compy-logo">
								<img src="{{asset('public/uploads/org_images\/').$organisations->ImageURL}}" class="mCS_img_loaded">
								<h5> {{ucfirst($organisations->organisation)}} </h5>
							</div>
							
							<div class="search-cot">
								<div><a href="{{URL::to('admin/manual-functional-lens-detail/'.base64_encode($organisations->id))}}"><button>Manage Personality Type </button></a></div>
								<select id="office">
									<option value="" >All Office</option>
									@foreach($offices as $office)
									<option {{(session('officeId')==$office->id)?'selected':''}} value="{{$office->id}}">{{$office->office}}</option>
									@endforeach
								</select>
								<select id="department" name="departmentId">
									@if(session('customDept'))
									<option value="">All Department</option>
									@foreach(session('customDept') as $cdept)
									<option {{(session('departmentId')==$cdept->id)?'selected':''}} value="{{$cdept->id}}">{{$cdept->department}}</option>
									@endforeach
									@else
									<option value="" selected>All Department</option>
									@endif									
								</select>							
							</div>

							<div class="Fun_lens_date"><h5>Personality Type</h5><div class="Fun_lens_date_edit"><input id="fun_start_date" type="text" name="" value="<?php if($organisations->COT_fun_lens_start_date){ echo date('d-M-Y', strtotime($organisations->COT_fun_lens_start_date)); }else{  echo date('d-M-Y'); } ?>" readonly=""></div></div>
						</div>

						<div class="value-list scrol-class lens_tbl">
							<table>
								<tr>
									<th>All Staff</th>
									<th>All Office</th>
									<th>All department</th>
									<th>E/I</th>
									<th>Score</th>
									<th>S/N</th>
									<th>Score</th>
									<th>T/F</th>
									<th>Score</th>
									<th>J/P</th>
									<th>Score</th>
									<th>Action</th>
								</tr>

								<?php $counter=2; ?>
								@foreach($users as $value)

								<?php $formtId = 'form'.$value->id; ?>

								{{ Form::model($value, ['method' => 'PUT', 'route' => array('cot.update', base64_encode($value->id)),'id'=>$formtId]) }}

								<tr class="table-class <?php if($counter % 2 == 0) echo 'grey-css'; ?>">
									<td>{{ucfirst($value->name)}}</td>
									<td>{{ucfirst($value->office)}}</td>
									<td>{{ucfirst($value->department)}}</td>
									<td>
										<select class="checkOrgCount{{$value->id}}" name="ei_value">
											<option value="" selected="">-</option>
											<option value="E" <?php echo ($value->EI=='E')?'selected':''; ?>>E</option>
											<option value="I" <?php echo ($value->EI=='I')?'selected':''; ?>>I</option>
										</select>
									</td>
									<td>
										<!-- <input class="numberControl" type="" name="ei_score" value="{{ucfirst($value->EI_score)}}"> -->

										<select class="checkOrgCount{{$value->id}}" name="ei_score">
											<option value="" disabled="" selected="">-</option>
											@for($i=1; $i<=21; $i++)
											<option value="{{$i}}" <?php echo ($value->EI_score==$i)?'selected':''; ?>>{{$i}}</option>	
											@endfor									
										</select>
									</td>
									<td>
										<select class="checkOrgCount{{$value->id}}" name="sn_value">
											<option value="" selected="">-</option>
											<option value="S" <?php echo ($value->SN=='S')?'selected':''; ?>>S</option>
											<option value="N" <?php echo ($value->SN=='N')?'selected':''; ?>>N</option>
										</select>
									</td>									
									<td>
										<!-- <input class="numberControl" type="" name="sn_score" value="{{ucfirst($value->SN_score)}}"> -->

										<select class="checkOrgCount{{$value->id}}" name="sn_score">
											<option value="" disabled="" selected="">-</option>
											@for($i=1; $i<=26; $i++)
											<option value="{{$i}}" <?php echo ($value->SN_score==$i)?'selected':''; ?>>{{$i}}</option>	
											@endfor									
										</select>
									</td>

									<td>
										<select class="checkOrgCount{{$value->id}}" name="tn_value">
											<option value="" selected="">-</option>
											<option value="T" <?php echo ($value->TF=='T')?'selected':''; ?>>T</option>
											<option value="F" <?php echo ($value->TF=='F')?'selected':''; ?>>F</option>
										</select>
									</td>
									<td>
										<!-- <input class="numberControl" type="" name="tn_score" value="{{ucfirst($value->TF_score)}}"> -->
										<select class="checkOrgCount{{$value->id}}" name="tn_score">
											<option value="" disabled="" selected="">-</option>
											@for($i=1; $i<=24; $i++)
											<option value="{{$i}}" <?php echo ($value->TF_score==$i)?'selected':''; ?>>{{$i}}</option>	
											@endfor									
										</select>
									</td>

									<td>
										<select class="checkOrgCount{{$value->id}}" name="jp_value">
											<option value="" selected="">-</option>
											<option value="J" <?php echo ($value->JP=='J')?'selected':''; ?>>J</option>
											<option value="P" <?php echo ($value->JP=='P')?'selected':''; ?>>P</option>
										</select>
									</td>
									<td>
										<!-- <input class="numberControl" type="" name="jp_score" value="{{ucfirst($value->JP_score)}}"> -->
										<select class="checkOrgCount{{$value->id}}" name="jp_score">
											<option value="" disabled="" selected="">-</option>
											@for($i=1; $i<=22; $i++)
											<option value="{{$i}}" <?php echo ($value->JP_score==$i)?'selected':''; ?>>{{$i}}</option>	
											@endfor									
										</select>

									</td>

									<input type="hidden" name="userId" value="{{$value->id}}">
									<input type="hidden" name="cotFunId" value="{{$value->funLensId}}">

									<td>
										<div class="editable">
											<button class="editbtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
											<button class="submitbtn dis{{$value->id}}" onclick="validation('{{$value->id}}')" type="button" style="display: none;">submit</button>
										</div>
									</td>
								</td>
							</tr>
							{{ Form::close() }}
							<?php $counter++; ?>
							@endforeach

						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="organ-page-nav">
		{{$users->links('layouts.pagination')}}
	</div>
</div>
<div class="error-message" style="display: none;">
	<span id="resp"></span>
</div>
</main>
<script type="text/javascript">
	$(document).ready(function() {

		$('.submitbtn').hide();
		$(".table-class").find("input,select").prop("disabled", true);		

	});
</script>
<script type="text/javascript">
	$('.editbtn').on('click',function(){

		$(this).closest('.editable').find('.submitbtn').show();
		$(this).closest('.editable').find('.editbtn').hide();	
		$(this).closest('.table-class').find('input,select').prop('disabled',false);	
	})
</script>
<script type="text/javascript">

	$(".numberControl").on("keypress keyup blur",function (event) {    
		$(this).val($(this).val().replace(/[^\d].+/, ""));
		if ((event.which < 48 || event.which > 57)) {
			event.preventDefault();
		}
	});
</script>

<script type="text/javascript">
	$('#office').on('change', function() {
		$('.loader-img').show();
		$('#department option').remove();

		var officeId = $('#office').val();

		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/getDepartmentByOfficecId')!!}",				
			data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{	
				// console.log(response)
				$('#department').append('<option value="">Select Department</option>');
				$('#department').append(response);
			}
		});

		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/cot-functional-lens/'.base64_encode($organisations->id))!!}",				
			data: {officeId:officeId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{	
				// console.log(response)			
				$('body').html(response);
			}
		});


	});
</script>
<script type="text/javascript">
	
	$('#department').on('change', function() {
		$('.loader-img').show();
		// $('#department option').remove();
		var departmentId = $('#department').val();
		var officeId     = $('#office').val();
		// console.log(officeId)
		// console.log(departmentId)
		$.ajax({
			type: "POST",
			url: "{!!URL::to('admin/cot-functional-lens/'.base64_encode($organisations->id))!!}",				
			data: {departmentId:departmentId,officeId:officeId,"_token":'<?php echo csrf_token()?>'},
			success: function(response) 
			{	
				// console.log(response)			
				$('body').html(response);
			}
		});
	});
</script>

<script type="text/javascript">
	$(function(){

		$("#fun_start_date").datepicker({
			showAnim: "fold",
			dateFormat: "dd-M-yy",
			showOn: "button",
			buttonImage: "{{asset('public/images/edit_date.png')}}",
			buttonImageOnly: true,
			buttonText: "Select date",
			onSelect: function(dateText) {

				var orgId = '<?php echo($organisations->id); ?>';
				$.ajax({
					type: "POST",
					url: "{!!URL::to('admin/updateCOTfuncLensStartingDate')!!}",				
					data: {cotDate:dateText,orgId:orgId,"_token":'<?php echo csrf_token()?>'},
					success: function(response) 
					{							
						$('div').remove('.alert');						
						$('#success-msg').append('<div class="alert alert-success" role="alert">Date updated successfully.</div>');
					}

				});				
			}
		});

	});
</script>
<script type="text/javascript">

	function validation(id)
	{	
		console.log('.dis'+id)
		$('.dis'+id).attr('disabled','disabled');
		$('.error-message').show();

		var count = 0;
		$('.checkOrgCount'+id).each(function(index) {

			if(!$(this).val())
			{
				$('.dis'+id).removeAttr('disabled');
				$('#resp').html('');
				$('#resp').html('All fields are required.');

				count++;
			} 

		});

		if(count==0){
			$('#resp').html('');
			$('#form'+id).submit();
			console.log('submit')
		}

	}

</script>

@include('layouts.adminFooter')