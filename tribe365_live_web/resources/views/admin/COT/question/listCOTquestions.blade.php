@section('title', 'List COT Questions')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                                    
								{{session('message')}}
							</div>
							@endif
						</div>
						<h2> Connecting-Team Role Map Questions </h2>
						<div class="adit-btn">
							<a href="{{route('cot-question.create')}}"><button class="savebtn" type="button" > Add </button></a>
						</div>
						<div class="value-list">
							<table>
								<tr><th> Question </th></tr>
								
								@foreach($cotQuestions as $value)

								<tr><td>
									{{ucfirst($value->question)}}										
									<div class="editable">
										<a href="{!!route('cot-question.edit',['id'=>base64_encode($value->id)])!!}">
											<button type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
										</a>

										{{ Form::open(['method'  => 'delete', 'route' => [ 'cot-question.destroy', base64_encode($value->id) ] ]) }}

										{{ Form::button('', ['type'=>'submit','onclick'=>'return confirm("Are you sure you want to delete ?")']) }}

										{{ Form::close() }}
									</div>
								</td></tr>

								@endforeach							
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="organ-page-nav">
			{{-- $departments->links('layouts.pagination') --}}
		</div>
	</div>
</main>
@include('layouts.adminFooter')