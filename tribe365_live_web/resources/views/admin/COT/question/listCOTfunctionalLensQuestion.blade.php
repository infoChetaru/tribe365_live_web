@section('title', 'List COT Personality Type Questions')
@include('layouts.adminHeader')
<main class="main-content">
	<div class="add-fild-section">
		<div class="container">
			<div class="ragistration-section">
				<div class="row">
					<div class="col-md-12">
						<div align="center">                            
							@if(session('message'))
							<div class="alert alert-success" role="alert">                
								{{session('message')}}
							</div>
							@endif
						</div>
						<h2> Connecting-Personality Type Questions </h2>						
						<div class="value-list">
							<table>
								<tr><th> Question </th></tr>								
								@foreach($cotFuncQuestions as $value)
								<tr>
									<td>{{ucfirst($value->question)}}
										<div class="editable">
											<a href="{!!URL::to('admin/cot-fun-que-option/'.base64_encode($value->id))!!}">
												<button type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
											</a>
										</div>
									</td>
								</tr>
								@endforeach							
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="organ-page-nav">
			{{$cotFuncQuestions->links('layouts.pagination')}}
		</div>
	</div>
</main>
@include('layouts.adminFooter')