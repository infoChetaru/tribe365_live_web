<!DOCTYPE html>
<html lang="en">
<head>
  <title>Tribe365 Set New Password</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta name="description" content="">
  <meta name="author" content="">
  <style type="text/css">
    @media screen and (max-width:480px) {
      table tr td {padding-left: 20px !important; padding-right: 20px !important;}
      .reset-img {width: 100% !important;}
      * {box-sizing: border-box; -webkit-box-sizing: border-box; -ms-box-sizing: border-box;}
      table {width: 100%;}
    }
  </style>
</head>

<body style="background: #f6f6f6">

 <div class="container" style="width: 100%;max-width: 100%;margin: 0 auto;display: block;text-align: center;overflow: hidden; font-family: Arial;">

   <div class="emil_table" style="width: 100%;display: block;text-align: left;font-family: Arial;">

    <center> 
      <table align="center" cellspacing="0" cellpadding="0" width="100%">
        <tr>
          <td>
            <table cellspacing="0" cellpadding="0" width="100%">
              <tr>
                <td style="padding:20px 40px;background-color: #fff;">                   
                  <img src="{{asset('public/images/logo.png')}}">
                </td>
              </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%" style="background-color:#f8f8f8;">
              <tr>
                <td style="font-size: 25px;font-weight: bold; font-family: Arial; padding: 30px 40px 20px;
                text-align: left;"><h1 style="margin: 0; font-size: 35px;"> Hello <span style="color: #eb1c24">{{ucfirst($name)." ".ucfirst($lastName)}},</h1>
                </td>
              </tr>
              <tr>
                <td style="font-size: 16px; padding: 0 40px"> To verify your email address, click the following link: </td>
              </tr>
              <tr>
                <td  style="padding: 20px 40px;" class="reset-img"> <a href="{{URL::to('setUserPassword/' . $passwordLink)}}"> <img width="290px" src="{{asset('public/images/set_pwd.png')}}"></a></td>
              </tr>
              <tr>
                <td style="font-size: 14px; padding: 3px 40px 11px 38px"> If you're having trouble clicking *Verify your email address* button,copy and paste URL below into your browser. <br> <a href="#"></a>{{URL::to('setUserPassword/' . $passwordLink)}}</td>
              </tr>
              <tr>
                <td style="font-size: 14px; padding: 3px 40px 11px 38px"> This link expires 5 days after the original verification request. <br></td>
              </tr>
              <tr>
                <td style="font-size: 16px; padding: 0 40px 30px">Sincerely, <br>Tribe 365 Team</td>
              </tr>
            </table>
            <table style="background-color:#ec0928; width: 100%">
              <tr>
                <td  style="color: #fff; font-size: 12px; width: 100%; background-color:#ec0928; padding:5px 40px;"> Tribe 365 </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </center>
  </div>
</div>

</body>
</html>