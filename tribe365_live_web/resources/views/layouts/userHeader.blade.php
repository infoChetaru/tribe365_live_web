<!DOCTYPE html>
<html>
	<head>						
		<title>Tribe 365 - @yield('title') </title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="{{asset('public/images/Favicon-icon.png')}}" type="image/x-icon"/>
		<link rel="stylesheet" type="text/css" href="{{asset('public/css/bootstrap.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('public/css/style.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('public/css/responsive.css')}}">
		<link rel="stylesheet" href="{{asset('public/css/jquery.mCustomScrollbar.css')}}">	
		<link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.teal-blue.min.css">
		<link rel="stylesheet" href="{{asset('public/css/scrollert.min.css')}}">
		<link rel="stylesheet" href="{{asset('public/css/scrollert.css')}}">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 

		<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet"> -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" rel="stylesheet">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">

		<!-- Sweet alert CDN -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css" rel="stylesheet">

		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js" type="text/javascript"></script>	

		<!-- gauge chart -->
		<script src="https://cdn.zingchart.com/zingchart.min.js"></script>
		<script src="https://code.getmdl.io/1.2.1/material.min.js"></script>

		<!-- Cropper -->
		<script src="<?php echo url('/public/croper/croppie.js'); ?>"></script>
		<link rel="stylesheet" href="<?php echo url('/public/croper/croppie.css'); ?>">

		<!-- Loader js -->
		<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/loader.js'); ?>"></script>
		<!-- chart js -->
		<script type="text/javascript" src="<?php echo url('/public/extranal_js_css/Chart.js'); ?>"></script>

		<!-- for multi selection in IOT select -->
		<script src="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js" type="text/javascript"></script>						
	</head>
	<body>
		<div class="wrapper">
			<div id="examples">
				<div class="content demo-y">
					<div class="app-body user-dashboard-body">
						<header style="height: 120px;">
							<div class="logo-box" style="box-shadow:none;">
								<a href="{{ url('/') }}"><img src="{{ asset('public/images/logo.png')}}"></a>

								<img class="active-logo" src="{{ asset('public/images/Favicon-icon.png') }}">

							</div>
							@php
								$orgId = Auth::user()->orgId;
								$organisations = DB::table('organisations')->where('id',$orgId)->where('status','Active')->first();

								$orgImage = $organisations->ImageURL;
								$orgName  = $organisations->organisation;
							@endphp
							<div class="org-logo user-dashboard-org">
								<a href="{{ url('/') }}"><img src="{{ asset('public/uploads/org_images/'.$orgImage) }}"></a>
								<h6>{{$orgName}}</h6>
							</div>
							
							<!-- <?php
							if(!empty($org_detail))
							{
							$org_detail = explode('-', $org_detail);

							$orgId    = $org_detail[0];
							$org_logo = $org_detail[1];

							?>	
							<div class="org-logo">
							<?php if(Request::segment(2)=='feedback-list') { ?>

							<a href="javascript:goBackBtn();"><img src="{{ asset('public/uploads/org_images/'.$org_logo) }}"></a>

							<?php }else if(Request::segment(2)=='editSotQuestions'){ ?>

							<?php }else{ ?>		
							<a href="{{ url('/admin/organisation-detail/'.$orgId) }}"><img src="{{ asset('public/uploads/org_images/'.$org_logo) }}"></a>
							<?php } ?>
							</div>

							<?php
							}
							?>			 -->				
							<div class="profile-box user-dashboard">
								<div class="profile-img">
									<img src="{{ asset('public/uploads/user_profile\/').Auth::user()->imageUrl}}">
								</div>
								<div class="profile-user-list dropdown"> 
									<span class="dropdown-toggle" data-toggle="dropdown" style="background: url('{{ asset('public/images/user-drop-icon.png') }}')no-repeat;">{{ucfirst(Auth::user()->name)." ".ucfirst(Auth::user()->lastName)}} </span>
									<ul class="dropdown-menu">
										<li>
											<a href="{{ route('user.profile') }}"><i class="fa fa-user" aria-hidden="true"></i> Profile </a>
										</li>
										<li>
											<a href="{{URL::to('user/profile-password-edit')}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Change Password </a>
										</li>
										<li>
											<a href="{{ route('user.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a>
										</li>
										<form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
										{{ csrf_field() }}
										</form>
									</ul>
								</div>
							</div>
						</header>

	<script type="text/javascript">

		function gobackImp(){
			$('#impPasswordDiv').show();
			$('#forgotPassDiv').hide();
			$('#resetPassDiv').hide();
		}

		function resetPassSubmit(){
			$('#passErrorSorryDiv').hide();
			$('#resetPassf1').hide();
			$('#sorryMsgError').hide();
			$('#resetPassf2').hide();
			$('#resetPassf3').hide();
			$('#resetPassf4').hide();

			var currentPassImp 	= $('#currentPassImp').val();
			var newPassImp 		= $('#newPassImp').val();
			var confirmPassImp 	= $('#confirmPassImp').val();
			if(currentPassImp==''){
				$('#resetPassf1').show();
				return false;
			}else if(newPassImp==''){
				$('#resetPassf2').show();
				return false;
			}else if(confirmPassImp==''){
				$('#resetPassf3').show();
				return false;
			}else if(newPassImp!=confirmPassImp){
				$('#resetPassf3').hide();
				$('#resetPassf4').show();
				return false;
			}

			$('#spanSub1').hide();		
			$('#spanImg1').show();


			$.ajax({
				url: "{{URL::to('admin/resetPasswordImprovementSubmit')}}",
				type: "POST",
				dataType: "JSON",
				data: {currentPassImp:currentPassImp,newPassImp:newPassImp,"_token":'<?php echo csrf_token()?>'},
				success: function(response){
					//console.log(response);
					//swal("Deleted!", "Your imaginary file has been deleted.", "success");
					
					if (response.status == 200 && response.passwordChk == 1) {		

						$('#showSucceesMsg1').show();		
						$('#mainContecntModal1').hide();

						//$('#ImprovementsModal').modal('toggle');
						//swal("Password!", "Password has been changed..", "success");
					}else{
						$('#spanSub1').show();		
						$('#spanImg1').hide();
						//swal("Password!", "Sorry! your password not match.", "error");
						$('#resetPassf1').hide();
						$('#sorryMsgError').show();
						$('#resetPassf2').hide();	
						$('#resetPassf3').hide();	
						$('#resetPassf4').hide();	
						$('#passErrorDiv').hide();	
						$('#passErrorSorryDiv').hide();	
					}			
				}
			});	

		}

		function improvementChkPass(){	
			var improvementPassword = $('#improvementPassword').val();
			if(improvementPassword==''){
				//swal("Password!", "Please enter your password.", "error");	
				$('#passErrorDiv').show();
				return false;
			}
			$.ajax({
				url: "{{URL::to('admin/improvementChkPass')}}",
				type: "POST",
				dataType: "JSON",
				data: {improvementPassword:improvementPassword,"_token":'<?php echo csrf_token()?>'},
				success: function(response){
					//console.log(response);
					//swal("Deleted!", "Your imaginary file has been deleted.", "success");
					
					var url = $('#improvementUrl').val();
					if (response.status == 200 && response.passwordChk == 1) {
						location.href = url;
					}else{
						//swal("Password!", "Sorry! your password not match.", "error");	
						$('#passErrorSorryDiv').show();
						$('#passErrorDiv').hide();

						$('#resetPassf1').hide();
						$('#sorryMsgError').hide();
						$('#resetPassf2').hide();	
						$('#resetPassf3').hide();	
						$('#resetPassf4').hide();	
						$('#passErrorDiv').hide();	

					}
					
				}
			});	
		}

		function forgotPassClick(){
			forgotPassSubmit();
		}

		function resetPassClick(){
			$('#impPasswordDiv').hide();
			$('#resetPassDiv').show();
			$('#forgotPassDiv').hide();
			$('#sorryMsgError').hide();

			$('#showSucceesMsg').hide();		
			$('#mainContecntModal').show();

			$('#showSucceesMsg1').hide();		
			$('#mainContecntModal1').show();

			
		}

		function forgotPassSubmit(){
			//var improvementEmail = $('#improvementEmail').val();
			//if(improvementEmail==''){	
				//$('#passErrorDiv1').show();		
				//return false;
			//}
			//Check email format
			//var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		    //if (reg.test(improvementEmail) == false) {
		        //$('#passErrorDiv1').hide();		
		        //$('#passErrorDiv2').show();		
		        //return false;
		    //}

			$('#showSucceesMsg').show();		
			$('#mainContecntModal').hide();

			$.ajax({
				url: "{{URL::to('admin/improvementForgotPassword')}}",
				type: "POST",
				dataType: "JSON",
				data: {"_token":'<?php echo csrf_token()?>'},
				success: function(response){
					//console.log(response);
					//swal("Deleted!", "Your imaginary file has been deleted.", "success");
					
					if (response.status == 200 && response.passwordChk == 1) {		
						$('#ImprovementsModal').modal('toggle');
						$('#spanImg111').hide();		
						$('#spanDiv111').show();

						//swal("Email!", "Email sent.", "success");
					}else{
						$('#spanSub').show();		
						$('#spanImg').hide();
						//swal("Password!", "Sorry! your email not match.", "error");	
					}			
				}
			});	
		}

	</script>

	<script>
	function goBackBtn() {
	  window.history.back();
	}
	</script>