<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
  @font-face {
    font-family: 'Poppins';
    src: url('https://production.chetaru.co.uk/tribe365/public/fonts/Poppins-Medium.woff2') format('woff2'),
      url('https://production.chetaru.co.uk/tribe365/public/fonts/Poppins-Medium.woff') format('woff');
    font-weight: 500;
    font-style: normal;
}

@font-face {
    font-family: 'Nabila';
    src: url('https://production.chetaru.co.uk/tribe365/public/fonts/Nabila.woff2') format('woff2'),
      url('https://production.chetaru.co.uk/tribe365/public/fonts/Nabila.woff') format('woff');
    font-weight: normal;
    font-style: normal;
}
@font-face {
    font-family: 'Montserrat';
    src: url('https://production.chetaru.co.uk/tribe365/public/fonts/Montserrat-Light.woff2') format('woff2'),
      url('https://production.chetaru.co.uk/tribe365/public/fonts/Montserrat-Light.woff') format('woff');
    font-weight: 300;
    font-style: normal;
}


body{
  padding: 50px 0;
  margin: 0;

}
@media screen (max-width: 480px) {
  table {display: block; width: 100% !important;}
  table tr {display: block;width: 100% !important;}
  table tr td {display: block;width: 100% !important;}
}
  
  </style>
</head>
<body style="background: #f2f2f2 !important;">
  <table cellpadding="0" cellspacing="0" border="0" style="max-width: 1440px; width: 100%; margin: 0 auto;     background: #f2f2f2; height: 550px;" >
    <tr>
      <td width="65%" style="border-left: 8px solid #ec0928; padding-left: 30px; padding-right: 30px; padding-bottom: 50px; text-align: center;" valign="middle">
        <table width="100%"  cellpadding="0" cellspacing="0" border="0" style="text-align: center; width: 100%;">
          <tr>
            <td  style="padding-top: 50px;">
              <img src="{{asset('public/images/cap.png')}}" alt="">
              <h1 style="font-size: 50px; color: #222222;margin: 0;line-height:normal; font-family:'nabila';">Congratulations!<br> <strong style=" color: #ec0928; font-family:'poppins'; font-weight: 500; font-size: 40px;">{{$userName}}</strong></h1> </td>
          </tr>
          <tr>
            <td align="center" style="text-align: center; width: 100%; margin: 0 auto; padding-top: 10px;  padding-bottom: 15px; margin-bottom: 0 !important;" valign="middle"> 
              <img src="{{asset('public/images/kudos-line.png')}}">
             <!--  <table style="height: 10px; padding-bottom: 0; max-width: 80px; width: 100%;" cellpadding="0" cellspacing="0" align="center">
                <tr><td style="border-top: 2px solid #ec0928;width: 20%; margin: 0 auto; height: 4px; padding-bottom: 0 !important; margin-bottom: 0 !important;"></td></tr>
              </table> -->
            </td>
          </tr>
          <tr>
            <td style="font-size: 21px;line-height: 28px; color: #797979;font-family: 'Montserrat';">You have earned the highest Kudos <br>this month. Well Done!</td>
          </tr>
        </table>
      </td>
        <td width="35%" style="background: #e4e4e4;" valign="middle">
          <table  width="100%"  cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td style="vertical-align: middle;text-align: center;"> 
                <img src="{{$userImage}}" alt="">
              </td>
            </tr>
          </table>
        </td>
    </tr>
  </table>

</body>
</html>










<!-- <!DOCTYPE html>
<html lang="en">
<head>
  <title>Kudos Champion</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta name="description" content="">
  <meta name="author" content="">
  <style type="text/css">
   * {box-sizing: border-box; -webkit-box-sizing: border-box; -ms-box-sizing: border-box;}
    @media screen and (max-width:480px) {
      table tr td {padding-left: 20px !important; padding-right: 20px !important;}
      .reset-img {width: 100% !important;}
      * {box-sizing: border-box; -webkit-box-sizing: border-box; -ms-box-sizing: border-box;}
     
    }
     table {width: 100%;}
      img {max-width: 100%; }

.img img {
    width: 200px;
    height: 200px;
    border-radius: 100%;
}

span img {text-align: right;width: 50px;}

span {
    width: 100%;
    display: block;
    text-align: right;
}

h3 {
    margin: 0;
    font-size: 16px;
}
  </style>
</head>

<body style="background: #f6f6f6">

 <div class="container" style="width: 100%;max-width: 100%;margin: 0 auto;display: block;text-align: center;overflow: hidden; font-family: Arial;">

   <div class="emil_table" style="width: 100%;display: block;text-align: left;font-family: Arial;">

    <center> 
      <table style="width: 100%; max-width: 600px; background-color: #fff;">
        <tr>
          <td style="width: 55%;padding: 33px;">
            <span><img src="{{asset('public/images/kudos-tilt-crown.png')}}"></span>
            <h3>Congratulations {{$userName}}</h3>
            <p> You have earned you heightest kudos </p>
          </td>
          <td style="width: 45%;padding: 25px 0;text-align: center;">
          <div class="img" style="width: 200px; height: 200px; border-radius: 100%; background-color: #eb1c24; padding-left: 20px; overflow: hidden; box-sizing: border-box; float: right;"> <img src="{{asset('public/uploads/user_profile/'.$userImage)}}">
          </div>
            
          </td>
        </tr>
      </table>
    </center>
  </div>
</div>

</body>
</html> -->