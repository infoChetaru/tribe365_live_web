				</div>
				<footer>
					<p> 2018 © Tribe 365 </p>
				</footer>
			</div>
			
			<script type="text/javascript" src="{{ asset('public/js/jquery.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('public/js/popper.js') }}"></script>
			<script type="text/javascript" src="{{ asset('public/js/bootstrap.min.js') }}"></script>
			<script src="{{ asset('public/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
			<!-- datepicker css -->f
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script> 
			
			<script>
				(function($){
					$(window).on("load",function(){
						$(".demo-y").mCustomScrollbar();
					});
				})(jQuery);

				function readURL(input) {
					if (input.files && input.files[0]) {
						var reader = new FileReader();

						reader.onload = function (e) {
							$('#blah').attr('src', e.target.result);
						}
						reader.readAsDataURL(input.files[0]);
					}
				}

				$("#org_logo").change(function(){
					readURL(this);
				});	

				//side bar
				$(".tob-nve-btn").click(function() {
					$("body").toggleClass('sild-menu');
				});			
			</script>	
			<!-- in add organisation add part-->
			<script type="text/javascript">

				var max_fields      = 10;
				var wrapper         = $(".add-office-row"); 				
				var next            = 1;
				
				// add office on click				
				$('.addoffice').on('click',function(e){					
					e.preventDefault();

					var total_fields = wrapper[0].childNodes.length;

					$('.add-office-row').append('<div id="field'+next+'" class="col-md-12"><div class="fild-stats-cont"><h2> Add Office </h2><div class="add-content"><div class="row"><div class="col-md-6"><div class="form-group"><input name="office_name'+next+'" type="text" class="form-control office_validation" placeholder="Office Name"></div></div><div class="col-md-6"><div class="form-group"><input name="office_noOf_employee'+next+'" type="text" class="numberControl form-control office_validation"  placeholder="Number of Employees"></div></div></div><div class="department-list-cont"><div class="row item"><div class="col-md-6"><div class="form-group"><input name="office_dept_name'+next+'[]" type="text" class="form-control office_validation" placeholder="Department Name '+next+'"></div></div><div class="col-md-6"><div class="form-group"><input name="dept_noOf_employee'+next+'[]" type="text" class="numberControl form-control office_validation" placeholder="Number of Employees '+next+'"></div></div></div><div class="addDeptHere'+next+'"></div><div class="Add-depart-btn"><button id="add_dept'+next+'" class="add_dept'+next+'" type="button"> Add Department </button></div></div></div></div></div>');

				// add department in office (add organisation)				
				$('.add-office-row').on('click','.add_dept'+next,function(e){
					e.preventDefault();

					var fieldNum = this.id.charAt(this.id.length-1);
					var addDeptHere  = ".addDeptHere" + fieldNum;

					$(addDeptHere).append('<div class="row item"><div class="col-md-6"><div class="form-group"><input name="office_dept_name'+fieldNum+'[]" type="text" class="form-control office_validation" placeholder="Department Name '+fieldNum+'"></div></div><div class="col-md-6"><div class="form-group"><input name="dept_noOf_employee'+fieldNum+'[]" type="text" class="numberControl form-control office_validation" placeholder="Number of Employees '+fieldNum+'"></div></div><button class="removeDept" type="button">X</button></div>');
				});

				//remove department in office (add organisation)
				$('.add-office-row').on('click','.removeDept',function(){

					$(this).parent('div').remove();
				});

				$(".office_count").val(next);
				next++;
				
			});	

		</script>

		<script type="text/javascript">

			var max_fields      = 10;
			var wrapper         = $(".add-user-row"); 				
			var user_count      = 1;

		// add user detail on click in organisation
		$('.addUserSection').on('click',function(e){
			e.preventDefault();

			var total_fields = wrapper[0].childNodes.length;

			$('.add-user-row').append('<div class="col-md-12"><div class="fild-stats-cont additional-user"><h2> Add User </h2><div class="add-content"><div class="row"><div class="col-md-4"><div class="form-group"><input name="additional_user_name'+user_count+'[]" type="text" class="form-control add_user_validation" placeholder="Name '+user_count+'"></div></div><div class="col-md-4"><div class="form-group"><input name="additional_user_password'+user_count+'[]" type="password" class="form-control add_user_validation" placeholder="Password '+user_count+'"></div></div><div class="col-md-4"><div class="form-group"><input type="password" class="form-control add_user_validation" placeholder="Confirm Password "></div></div><div class="addUserHere'+user_count+'"></div><div class="Add-depart-btn"><button id="add_dept'+user_count+'" class="addUser'+user_count+'" type="button"> Add User </button></div></div></div></div></div>');


		// add user in add user section (add organisation)	
		$('.add-user-row').on('click','.addUser'+user_count,function(e){
			e.preventDefault();

			var userFieldNum = this.id.charAt(this.id.length-1);
			var addUserHere  = ".addUserHere" + userFieldNum;

			console.log(userFieldNum);

			$(addUserHere).append('<div class="row"><div class="col-md-4"><div class="form-group"><input name="additional_user_name'+userFieldNum+'[]" type="text" class="form-control add_user_validation"  placeholder="Name '+userFieldNum+'"></div></div><div class="col-md-4"><div class="form-group"><input name="additional_user_password'+userFieldNum+'[]" type="text" class="form-control add_user_validation"  placeholder="Password '+userFieldNum+'"></div></div><div class="col-md-4"><div class="form-group"><input type="text" class="form-control add_user_validation" placeholder="Confirm Password "></div></div><button class="removeUser" type="button">X</button></div>');
		});

        //remove user from Add user section(add organisation)
        $('.add-user-row').on('click','.removeUser',function(){

        	$(this).parent('div').remove();
        });

        $(".additional_user_count").val(user_count);
        user_count++

    });

</script>

<script type="text/javascript">

	$(".numberControl").on("keypress keyup blur",function (event) {    
		$(this).val($(this).val().replace(/[^\d].+/, ""));
		if ((event.which < 48 || event.which > 57)) {
			event.preventDefault();
		}
	});
</script>


</body>
</html>

