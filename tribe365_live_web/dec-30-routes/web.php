<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('thumsup', function () {
	return view('admin.report.report_graph.dotThumbsuplist');

});

Route::get('iot1', function () {
	return view('admin.IOT.iot1');

});

Route::get('/iot2', function () {
	return view('admin.IOT.iot2');
});

Route::get('/iot3', function () {
	return view('admin.IOT.iot3');
});

Route::get('/iot4', function () {
	return view('admin.IOT.iot4');
});


Route::get('/', function () {
	return view('layouts.comingsoon');
});


/*SET NEW USER PASSWORD**/ 
Route::get('setUserPassword/{token}', 'API\ApiOrganisationController@setUserPassword')->name('setUserPassword');

//update new password of user
Route::post('updateNewUserpassword/{confirmationCode}','API\ApiOrganisationController@updateNewUserpassword');



/*----------FORGOT PASSWORD---------------*/

Route::get('resetPassword/{confirmationCode}', [
	'as' => 'confirmation_path',
	'uses' => 'API\ApiLoginController@resetPassword'
]);
Route::post('updatepassword/{confirmationCode}','API\ApiLoginController@updatePassword');


/*web*/
/*get forgot page*/
Route::get('forgot-password','Admin\AdminLoginController@getForgotPassword');
/*update forgot password*/
Route::post('forgotPassword','Admin\AdminLoginController@forgotPassword');

/*----------FORGOT PASSWORD----------------*/



//Ram added
Route::get('admin/getIndexesOfAllOrganization/{date?}','Admin\AdminReportController@getIndexesOfAllOrganization');
Route::get('admin/getIndexesOfAllOrganizationByPastDate','Admin\AdminReportController@getIndexesOfAllOrganizationByPastDate');


//improvement password
Route::post('admin/improvementChkPass','Admin\AdminLoginController@improvementChkPass');
Route::post('admin/improvementForgotPassword','Admin\AdminLoginController@improvementForgotPassword');

Route::get('resetPasswordImprovement/{confirmationCode}', [
	'as' => 'confirmation_path',
	'uses' => 'API\ApiLoginController@resetPasswordImprovement'
]);
Route::post('updatepasswordImprovement/{confirmationCode}','API\ApiLoginController@updatePasswordImprovement');
Route::post('admin/resetPasswordImprovementSubmit','Admin\AdminLoginController@resetPasswordImprovementSubmit');



/*Admin Authentication*/
Route::get('/admin','Admin\AdminLoginController@index');
Route::post('/admin/login',['as'=>'admin.auth','uses'=>'Admin\AdminLoginController@login']);
Route::post('/admin/logout',['as'=>'admin.logout','uses'=>'Admin\AdminLoginController@logout']);

/*ADMIN FUNCTIONALITY*/
Route::group(['prefix'=>'admin','middleware'=>['admin']],function ()
{	
	/*ADMIN PROFILE FUNCTIONALITY*/

	/*admin profile*/
	Route::get('profile',['as'=>'admin.profile','uses'=>'Admin\AdminLoginController@getProfile']);
	/*admin profile update*/
	Route::post('profile-update',['as'=>'admin.profile.update','uses'=>'Admin\AdminLoginController@updateProfile']);
	/*admin profile password*/
	Route::get('profile-password-edit','Admin\AdminLoginController@getEditProfilePassword');
	/*admin profile update password*/
	Route::post('profile-password-update','Admin\AdminLoginController@updateProfilePassword');


	/*Dashboard FUNCTIONALITY*/
	Route::get('/admin-dashboard','Admin\AdminDashboardController@index');


	/*Organisation FUNCTIONALITY*/

	Route::resource('admin-organisation','Admin\AdminOrganisationController');
	/*UPDATE organisation*/
	Route::post('organisation-update','Admin\AdminOrganisationController@organisation_update')->name('organisation-update');

	Route::post('organisation-update1','Admin\AdminOrganisationController@organisation_update1')->name('organisation-update1');

	Route::post('organisation-update2','Admin\AdminOrganisationController@organisation_update2')->name('organisation-update2');

	Route::post('organisation-update3','Admin\AdminOrganisationController@organisation_update3')->name('organisation-update3');	
	Route::post('organisation-update4','Admin\AdminOrganisationController@organisation_update4')->name('organisation-update4');	
	/*Inactive organisation*/
	Route::post('organisation-delete','Admin\AdminOrganisationController@deleteOrganisation');
	/*delete organisation*/
	Route::post('delete-oragnisation','Admin\AdminOrganisationController@deleteWholeOrganisation');
	
	/*DOT FUNCTIONALITY*/	

	Route::resource('admin-dot','Admin\AdminDotController');
	/*show add page of custom adding page*/
	Route::get('custome-value','Admin\AdminDotController@createCustomValues')->name('custome-value');
	/*store custom value*/
	Route::post('store-custom-value','Admin\AdminDotController@storeCustomValues')->name('store-custom-value');
	/*get edit page of DOT*/
	Route::resource('dot-edit','Admin\AdminDotController');
	/*add DOT*/
	Route::get('dot-create/{id}','Admin\AdminDotController@addDot');
	/*UPDATE DOT*/
	Route::post('dot-update','Admin\AdminDotController@updateDotDetail')->name('dot-update');
	/*delete dot*/
	Route::post('dot-value-delete','Admin\AdminDotController@deleteDot');
	/* delete belief */
	Route::post('dot-belief-delete','Admin\AdminDotController@deleteBelief');
	/*UPDATE belief*/
	Route::post('dot-belief-update','Admin\AdminDotController@updateDotBeliefDetail')->name('dot-belief-update');
	/*ADD belief in edit section*/
	Route::post('dot-add-belief','Admin\AdminDotController@addBelief')->name('dot-add-belief');
	/*show add page of custom adding page*/
	Route::get('custome-value','Admin\AdminDotController@createCustomValues')->name('custome-value');
	/*store custom value*/
	Route::post('store-custom-value','Admin\AdminDotController@storeCustomValues')->name('store-custom-value');
	/*get custom values list*/
	Route::get('dot-custom-values','Admin\AdminDotController@beliefCustomeValues')->name('dot-custom-values');
	/*edit custom DOT value*/
	Route::get('edit-custom-value/{id}','Admin\AdminDotController@editCustomValue')->name('edit-custom-value');
	/*update dot custom values*/
	Route::post('update-custom-value','Admin\AdminDotController@updateCustomValues')->name('update-custom-value');
	/*delet dot custom values*/
	Route::get('delete-custom-value/{id}','Admin\AdminDotController@deleteCustomValue')->name('delete-custom-value');
	/*get evidence list*/ 
	Route::get('get-evidence-list/{dotId}/{type}/{id}','Admin\AdminDotController@getEvidenceList')->name('get-evidence-list');
	/*get custom dot values modal */
	Route::post('get-custom-value-evidence-modal','Admin\AdminDotController@getDotCustomValueList')->name('get-custom-value-evidence-modal');




	/*USER FUNCTIONALITY*/

	Route::resource('admin-user','Admin\AdminUserController');	

	Route::get('add-staff/{id}','Admin\AdminUserController@addStaff');
	/*get user /staff list*/
	Route::get('view-staff/{id}','Admin\AdminUserController@viewStaffList');
	/*add user by csv file*/
	Route::post('add-user-csv','Admin\AdminUserController@addUserCSV')->name('add-user-csv');
	/*export user list csv*/
	Route::get('user-list-export/{id}','Admin\AdminUserController@exportUserList');


	/*ORGANISATION FUNCTIONALITY*/

	/*get organisation detail DOT COT org. SOT etc*/
	Route::get('organisation-detail/{id}','Admin\AdminOrganisationController@showOrganisationDetail');	
	/*get department create page */
	Route::get('add-office/{id}','Admin\AdminOrganisationController@addOffice');

	Route::get('list-office/{id}','Admin\AdminOrganisationController@listOffice');
	
	Route::post('create-office','Admin\AdminOrganisationController@createOffice');

	/*DEPARTMENT FUNCTIONALITY*/

	/*get all departments list*/
	Route::resource('departments','Admin\AdminDepartmentController');
	
	
	/*ACTION FUNCTIONALITY*/

	/*Dot action lits*/
	Route::get('action-list/{id}','Admin\AdminActionController@getActionList');
	/*add DOT action */
	Route::get('add-action/{id}','Admin\AdminActionController@addAction');
	/*store active detail*/
	Route::resource('action','Admin\AdminActionController');
	/*get actions comments*/
	Route::get('action-comments/{id}','Admin\AdminActionController@getCommentsList');
	/*add new comments*/
	Route::post('action-add-comment','Admin\AdminActionController@addComment');
	/*updated actione status*/
	Route::post('action-status-update','Admin\AdminActionController@updateActionStatus');
	/*get responsible user list*/
	Route::POST('get-responsible-users','Admin\AdminActionController@getResUser');
	/*delete action*/
	Route::get('delete-action/{id}','Admin\AdminActionController@deleteAction');
	/*get action theme modal response */
	Route::post('get-action-theme-modal','Admin\AdminActionController@getActionThemeModal')->name('get-action-theme-modal');
	/*add new action theme by ajax when modal is open*/
	Route::post('add-new-action-theme-by-ajax','Admin\AdminActionController@addNewActionThemeByAjax')->name('add-new-action-theme-by-ajax');

	

	/*COMMON FUNCTIONALITY*/

	/*DEPARTMENT functionality*/
	Route::post('admin-add-department','Admin\CommonController@addNewDepartment')->name('admin-add-department');
	/*Common controller*/
	Route::get('get-department','Admin\CommonController@getAllDepartments');	
	/*get dot custom values list*/
	Route::get('get-custom-dot-list','Admin\CommonController@getCustomDotValues');
	/*check email is exists in table*/
	Route::post('search-email','Admin\CommonController@getEmail');	
	/*get offices list*/
	Route::post('get-offices','Admin\CommonController@getOfficeList');
	/*get departments list*/
	Route::post('get-department','Admin\CommonController@getDepartmentList');
	/*get user list*/
	Route::post('get-users','Admin\CommonController@getUsers');

	Route::get('inArray','Admin\CommonController@inArray');
	/*get org name*/
	Route::post('get-org-name','Admin\CommonController@getOrgName')->name('get-org-name');
	/*get search data of the values */
	Route::get('search-dot-values','Admin\CommonController@getDotValueSearchData')->name('search-dot-values');
	// /*send notification mail */
	Route::get('sendNotificationMail','Admin\CommonController@sendNotificationMail')->name('sendNotificationMail');
	/*get offices by organisation*/
	Route::post('getOfficesByOrgId','Admin\CommonController@getOfficesByOrgId');	
	/*get departments by office*/
	Route::post('getDepartmentByOfficecId','Admin\CommonController@getDepartmentByOfficecId');
	/*search organisation functionality in list organisation*/
	Route::post('searchOrganisations','Admin\CommonController@searchOrganisations');
	/*get office search result list*/
	Route::post('searchOffice','Admin\CommonController@searchOffice');
	/*get office search result list*/
	Route::post('searchStaff','Admin\CommonController@searchStaff');
	/*get departments search result list*/
	Route::post('searchDepartment','Admin\CommonController@searchDepartment'); 
	/*get search reports organisation result list*/
	Route::post('searchReportOrganisations','Admin\CommonController@searchReportOrganisations');
	/*get department by office id this is used in report graph section*/
	Route::post('getDepartmentByOfficecIdforGraph','Admin\CommonController@getDepartmentByOfficecIdforGraph');




	/*COT QUESTIONS FUNCTIONALITY*/

	Route::resource('cot-question','Admin\AdminCOTquestionController');
	/*get functional lens question list*/
	Route::get('cot-fun-questions','Admin\AdminCOTquestionController@getCOTfunctionalLensQuestionList');
	/*get cot functional lens questions option list*/
	Route::get('cot-fun-que-option/{id}','Admin\AdminCOTquestionController@getCOTfunctionalLensQuestionOptionList');
	/*update functional lens question and option*/
	Route::post('update-cot-fun-que-opt','Admin\AdminCOTquestionController@updateFunctionalLensQueOpt');


	
	/*COT FUNCTIONALITY*/

	Route::resource('cot','Admin\AdminCOTController');

	/*get cot detail page*/
	Route::get('cot-detail/{id}','Admin\AdminCOTController@getCotDetail');

	/*get team role map view*/
	Route::match(['get', 'post'],'cot-team-role-map/{id}','Admin\AdminCOTController@getTeamRoleMap');
	/*update organisaton has_cot status*/
	Route::get('cot-team-role-map-update-has-cot/{id}','Admin\AdminCOTController@updateCOTstatus');
	/*get COt maper rank DATA FROM API*/
	Route::get('getCOTmaperkey','API\ApiCOTController@getCOTmaperkey');
	/*get list of functional lens list*/
	Route::match(['get', 'post'],'cot-functional-lens/{id}','Admin\AdminCOTController@getFunctinalLensList');
	/*get functional lens manage page */
	Route::get('manual-functional-lens-detail','Admin\AdminCOTController@manageFunctionalLensList');
	/*update cot functional initial values*/
	Route::post('update-cot-fun-initial-value','Admin\AdminCOTController@updateCOTinitiatialValue');
	/*update cot functional insight values*/
	Route::post('update-cot-fun-strength-value','Admin\AdminCOTController@updateCOTfunctionalLensInsightForStrenghtValue');
	//edit page of COT functional lens tribe tips seek and persuade values
	Route::get('edit-cot-fun-tribe-tips-value/{id}/{type}','Admin\AdminCOTController@editCOTfunTribeTipsSeekPersuadeValues');
	/*update cot functional lens tribe tips seek and persuade values*/
	Route::post('update-cot-fun-tribe-tips-value','Admin\AdminCOTController@updateCOTtribeSeekPersuadevalues');
	/*delete cot functional lense tribe tips value */
	Route::get('delete-cot-fun-tribe-tips-value/{id}','Admin\AdminCOTController@deleteCOTfunLensTribeTipsvalue');
	/*update cot functional lens starting date*/
	Route::post('updateCOTfuncLensStartingDate','Admin\AdminCOTController@updateCOTfuncLensStartingDate');
	/*update cot functional lens tribe tips summay*/
	Route::post('update-cot-tribe-tips-summary','Admin\AdminCOTController@updateTribeTipsSummary');
	/*get COT team role map description list*/
	Route::get('cot-team-role-map-description','Admin\AdminCOTController@getTeamRoleMapDescriptionList');
	/*update cot team role map description values*/
	Route::post('update-cot-team-role-description','Admin\AdminCOTController@updateCOTteamroleMapDescription');
	
	
	/*REPORT FUNCTIONALITY*/

	Route::resource('reports','Admin\AdminReportController');
	/*get */
	Route::match(['get', 'post'],'dot-reports/{id}','Admin\AdminReportController@getDOTreports');
	/*get rating list of belief and values */
	Route::match(['get', 'post'],'report-belief-value-rating-list/{id}','Admin\AdminReportController@getBeliefValueRatingList')->name('report-belief-value-rating-list');
	/*get belief indivisual ratings*/
	Route::match(['get', 'post'],'report-belief-individual-ratings/{id}','Admin\AdminReportController@getIndividualBeliefValueRatingList')->name('report-belief-individual-ratings');
	/*get diagnostic reports organisations list*/
	Route::get('diagnostic-report-org','Admin\AdminReportController@getDiagOrgList');
	/*get diagnostic resport graph*/
	Route::get('diagnostic-report-graph/{id}','Admin\AdminReportController@getDiagGraph'); 
	/*get diagnostic reports organisations list*/
	Route::get('tribeometer-report-org','Admin\AdminReportController@getTribeoOrgList');
	/*get tribeometer resport graph*/
	Route::get('tribeometer-report-graph/{id}','Admin\AdminReportController@getTribeoGraph');
	Route::get('manage-report','Admin\AdminReportController@manageReport');
	/*get cot functional lens */
	Route::post('getCOTFunctionalLensGraph','Admin\AdminReportController@getCOTFunctionalLensGraph');
	/*get sot motivation data for graph*/
	Route::post('getSOTmotivationGraph','Admin\AdminReportController@getSOTmotivationGraph'); 
	/*get COT team role mape for report graph*/
	Route::post('getCOTteamRoleMapGraph','Admin\AdminReportController@getCOTteamRoleMapGraph'); 
	/*get tribeometer report graph*/
	Route::post('getReportTribeometerGraph','Admin\AdminReportController@getReportTribeometerGraph'); 
	/*get doagnostic report graph*/
	Route::post('getReportDiagnosticGraph','Admin\AdminReportController@getReportDiagnosticGraph'); 
	/*get motivational report graph*/
	Route::post('getReportMotivationalGraph','Admin\AdminReportController@getReportMotivationalGraph'); 
	/*get culture report graph*/
	Route::post('getReportCultureGraph','Admin\AdminReportController@getReportCultureGraph');
	/*get culture report graph*/
	Route::post('getReportFunctionalGraph','Admin\AdminReportController@getReportFunctionalGraph'); 

	/*get diagnostic sub report of graphs*/
	Route::post('getDiagnsticSubGraph','Admin\AdminReportController@getDiagnsticSubGraph')->name('getDiagnsticSubGraph'); 

	/*get DOT bubble list for report*/
	Route::post('getDotThumbsUpBubbleList','Admin\AdminReportController@getDotThumbsUpBubbleList')->name('getDotThumbsUpBubbleList');

	// get happy index report yearly
	Route::post('getReportHappyIndexGraphYear','Admin\AdminReportController@getReportHappyIndexGraphYear'); 
	// get happy index report Monthly
	Route::post('getReportHappyIndexGraphMonth','Admin\AdminReportController@getReportHappyIndexGraphMonth'); 
	// get happy index report Weekly
	Route::post('getReportHappyIndexGraphWeek','Admin\AdminReportController@getReportHappyIndexGraphWeek'); 
	// get happy index report daily
	Route::post('getReportHappyIndexGraphDay','Admin\AdminReportController@getReportHappyIndexGraphDay'); 


	//Report index
	Route::post('dotTabData','Admin\AdminReportController@dotTabData');

	
	/*IOT FUNCTIONALITY*/

	
	Route::resource('iot','Admin\AdminIOTController');
	/*get iot dashboard*/
	Route::match(['get','post'],'iot-dashboard/{orgId}','Admin\AdminIOTController@getIotDeshboard');
	/*get feedback list*/
	Route::get('feedback-list/{orgId}/{status}/{officeId}','Admin\AdminIOTController@getFeedbackList');
	/*get create page of theme*/
	Route::get('add-theme','Admin\AdminIOTController@createTheme')->name('add-theme');
	/*get custom theme list*/
	Route::get('theme-list/{orgId}/{id}','Admin\AdminIOTController@getThemeList')->name('theme-list');
	/*store custom theme*/
	Route::post('store-theme','Admin\AdminIOTController@storeCustomTheme')->name('store-theme');
	/*get edit page of cutome theme*/
	Route::get('edit-theme/{orgId}/{id}','Admin\AdminIOTController@editCustomTheme')->name('edit-theme');
	/*update theme*/
	Route::post('update-theme','Admin\AdminIOTController@updateCustomTheme')->name('update-theme');
	/*get chat screen*/
	Route::get('get-chat/{orgId}/{feedbackId}','Admin\AdminIOTController@getChatMessages')->name('get-chat');
	/*send msg*/
	Route::post('send-chat-msg','Admin\AdminIOTController@sendChatMessages')->name('send-chat-msg');
	/*get msgs by ajax call*/
	Route::post('get-chat-messages-by-ajax','Admin\AdminIOTController@getChatMessagesByAjax')->name('get-chat-messages-by-ajax');
	/*update theme to feedback*/
	Route::post('update-feedback-theme','Admin\AdminIOTController@updateFeedbackTheme')->name('update-feedback-theme');
	/*get theme modal response */
	Route::post('get-theme-modal','Admin\AdminIOTController@getThemeModal')->name('get-theme-modal');
	/*get theme modal response */
	Route::get('complete-feedback/{id}','Admin\AdminIOTController@completeFeedback')->name('complete-feedback');
	/*add new theme by ajax when modal is open*/
	Route::post('add-new-theme-by-ajax','Admin\AdminIOTController@addNewThemeByAjax')->name('add-new-theme-by-ajax');
	/*get IOT actions*/
	Route::post('getActionsByAjax','Admin\AdminIOTController@getActionsByAjax')->name('get-actions-by-ajax');
	// Delete themes
	Route::post('delete-themes','Admin\AdminIOTController@deleteThemes');
	//Delete Feedbacks
	Route::post('delete-feedbacks','Admin\AdminIOTController@deleteFeedbacks');




	/*SOT FUNCTIONALITY*/
	Route::resource('sot','Admin\AdminSOTController');
	/*get sot culture questions list */
	Route::get('sot-culture-questionnaire-list','Admin\AdminSOTController@getSOTquestionnaireList');
	/*sot detail page*/
	Route::get('sot-detail/{id}','Admin\AdminSOTController@sotHomeDetail');
	/*update sot-culture information question*/
	Route::post('update-sot-culture-question','Admin\AdminSOTController@updateCultureInfoQuestion');
	/*update sot-culture-structure-summary-value*/
	Route::post('update-sot-culture-structure-summary','Admin\AdminSOTController@updateCultureSummaryValue');
	/*sott culture structure image update*/
	Route::post('update-sot-culture-img','Admin\AdminSOTController@updateCultureStructureImag');

	/*list SOT Motivation users list*/
	Route::match(['get', 'post'],'sot-motivation-users/{id}','Admin\AdminSOTController@getSOTmotivationUserList');
	//update sot motivation detail
	Route::post('sot-update-motivation-users','Admin\AdminSOTController@updateSOTmotivationDetail');
	/*update SOT motivation starting date*/
	Route::post('updateSOTmotivationStartingDate','Admin\AdminSOTController@updateSOTmotivationStartingDate');
	/*sot Motivation  value list*/
	Route::get('sot-motivation-values','Admin\AdminSOTController@getSOTmotivationValuesList');
	/*update sot-motivation-values*/
	Route::post('update-sot-motivation-value','Admin\AdminSOTController@updateSOTmotivationValue');	

	/*get sot motivation questions list*/
	Route::get('sot-motivation-questions','Admin\AdminSOTController@getSOTmotivationQuestionnaireList');
	/*get sot motivation question option*/
	Route::get('sot-mot-que-option/{id}','Admin\AdminSOTController@getSOTmotivationQuesOptionList');
	/*update sot motivation questions option*/
	Route::post('update-sot-mot-que-opt','Admin\AdminSOTController@updateSOTmotivationQueOpt');
	/*update Culture Structure Title*/
	Route::post('update-sot-culture-structure-title','Admin\AdminSOTController@updateCultureStructureTitle');

	/*DIAGNOSTIC FUNCTIONALITY*/
	Route::resource('diagnostic','Admin\AdminDiagnosticController');
	/*get diagnostic option list*/
	Route::get('diagnostic-option','Admin\AdminDiagnosticController@getDiagnosticOptionList');
	/*update diagnostic options*/
	Route::post('update-dia-opt','Admin\AdminDiagnosticController@upadateDiagnosticOption');
	/*get diagnostinc categories list*/
	Route::get('diagnostinc-cat-values','Admin\AdminDiagnosticController@getDiagnosticValuesList');
	/*update diagnostic category values */
	Route::post('updateDiagnosticCategoryValues','Admin\AdminDiagnosticController@updateDiagnosticCategoryValues');
	

	/*TRIBEOMETER FUNCTIONALITY*/
	Route::resource('tribeometer','Admin\AdminTribeometerController');
	/*get tribeometer option list*/
	Route::get('tribeometer-option','Admin\AdminTribeometerController@getTribeometerOptionList');
	/*update tribeometer options*/
	Route::post('update-tri-opt','Admin\AdminTribeometerController@upadateTribeometerOption');
	/*get tribeometer categories list*/
	Route::get('tribeometer-cat-values','Admin\AdminTribeometerController@getTribeometerValuesList');
	/*update tribeometer category values */
	Route::post('updateTribeometerCategoryValues','Admin\AdminTribeometerController@updateTribeometerCategoryValues');

	Route::get('pdf/{id}','Admin\AdminReportController@showPdfReport');

	Route::post('exportPdf','Admin\AdminReportController@exportPdf');

	/*PERFORMANCE FUNCTIONALITY*/	
	Route::resource('performance','Admin\AdminPerformanceController');
	/*get performace*/
	Route::match(['get','post'],'performace-detail','Admin\AdminPerformanceController@getPerformance')->name('performace-detail');


	
});