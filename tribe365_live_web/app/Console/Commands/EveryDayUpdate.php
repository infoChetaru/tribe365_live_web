<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
use View;

class EveryDayUpdate extends Command
{
        /**
     * The name and signature of the console command.
     *
     * @var string
     */
        protected $signature = 'notification:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification every day at 4:00 PM';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {   

        //Need only on 1 to 5 days
        if(date('D')=="Mon" || date('D')=="Tue" || date('D')=="Wed" || date('D')=="Thu" || date('D')=="Fri"){

            $title   = "Feedback";
            $message = "How's things at work today?";

            $users = DB::table('users')
            ->select('users.id','users.name','users.fcmToken','offices.office','offices.country','country.timezone')
            ->leftjoin('offices','offices.id','users.officeId')
            ->leftjoin('country','country.id','offices.country')
            ->where('users.roleId',3)
            ->where('users.fcmToken','!=','')
            ->where('users.status','Active')->get();

            foreach($users as $value){
                $notiArray = array('fcmToken'=> $value->fcmToken, 'title'=> $title ,'message'=> $message, 'totbadge'=>0, 'feedbackId'=>'');
                if($value->timezone){
                    date_default_timezone_set($value->timezone);
                    if(date('H:i')=='16:00') {
                        if (!empty($value->id)) {
                            $loggedInUser = DB::table('oauth_access_tokens')->where('user_id',$value->id)->where('revoked',0)->first();
                            if (!empty($loggedInUser)){
                                app('App\Http\Controllers\Admin\CommonController')->sendFcmNotify($notiArray);                   
                            }
                        }
                    }
                }
                $this->info(date('H:i'));
            }    

            /*
            Mail::send('welcome', [], function($message) {
                $message->to('ram@chetaru.com')->subject(date('D').'=== 111=> Testing mails at: '.date('Y-m-d H:i:s')); 
            });
            */

        }

        //cron for culture index and engagement index
        if(date('H:i')=='23:52'){
        //if(date('H:i')=='19:27'){
            
            /*
            Mail::send('welcome', [], function($message) {
                $message->to('ram@chetaru.com')->subject('Testing mails at: '.date('Y-m-d H:i:s')); 
            });
            */
            app('App\Http\Controllers\Admin\AdminReportController')->getIndexesOfAllOrganization();
        }     
        //cron for diagnostic and sub diagnostic graph
        if (date('Y-m-d H:i') == date('Y-m-d',strtotime('last day of this month'))." 23:45") {
            app('App\Http\Controllers\Admin\AdminReportController')->getDiagnosticIndexReport();   
        }
        //cron for tribeopmeter
        if (date('Y-m-d H:i') == date('Y-m-d',strtotime('last day of this month'))." 23:46") {
            app('App\Http\Controllers\Admin\AdminReportController')->getTribeometerIndexReport();    
        }
        //cron for culture structure
        if (date('Y-m-d H:i') == date('Y-m-d',strtotime('last day of this month'))." 23:47") {
            app('App\Http\Controllers\Admin\AdminReportController')->getCultureStructureIndexReport();  
        }
        //cron for team role
        if (date('Y-m-d H:i') == date('Y-m-d',strtotime('last day of this month'))." 23:48") {  
            app('App\Http\Controllers\Admin\AdminReportController')->getTeamRoleIndexReport();  
        }
        //cron for motivation
        if (date('Y-m-d H:i') == date('Y-m-d',strtotime('last day of this month'))." 23:49") {
            app('App\Http\Controllers\Admin\AdminReportController')->getMotivationIndexReport();  
        }
        //cron for DOT beliefs and graph
        if (date('Y-m-d H:i') == date('Y-m-d',strtotime('last day of this month'))." 23:50") { 
            app('App\Http\Controllers\Admin\AdminReportController')->getDirectingIndexReport();  
        }
        //cron for personality type
        if (date('Y-m-d H:i') == date('Y-m-d',strtotime('last day of this month'))." 23:51") { 
            app('App\Http\Controllers\Admin\AdminReportController')->getPersonalityTypeIndexReport();  
        }
        //cron for kudos at dashboard
        if (date('Y-m-d H:i') == date('Y-m-d',strtotime('last day of this month'))." 23:43") { 
            app('App\Http\Controllers\Admin\AdminReportController')->getKudosCountReport();  
        }
        //cron for kudos at kudos leaderboard
        if (date('Y-m-d H:i') == date('Y-m-d',strtotime('last day of this month'))." 23:41") { 
            app('App\Http\Controllers\Admin\AdminReportController')->getKudosChampLeaderboard();  
        }
        //time zone for asia
        date_default_timezone_set('Asia/Kolkata');
        //cron for happy index Percentage
        if (date('H:i') ==  '05:00') {
            app('App\Http\Controllers\Admin\AdminReportController')->getHappyIndexReport();  
        }
        //cron for happy index person count
        if (date('H:i') ==  '05:02') {
            app('App\Http\Controllers\Admin\AdminReportController')->getHappyIndexDailyCountReport();  
        }
        //cron for users count daily
        if (date('H:i') ==  '05:05') {
            app('App\Http\Controllers\Admin\AdminReportController')->getUsersCountReport();  
        }
        //Kudos Notification
        // date_default_timezone_set('Asia/Kolkata');
        // if (date('Y-m-d H:i') == date('Y-m-d',strtotime('first day of this month'))." 00:00") {
        if (date('Y-m-d H:i') == date('Y-m-d 16:02')) {
            app('App\Http\Controllers\Admin\AdminDashboardController')->kudosNotification();  
        }
    }

}
