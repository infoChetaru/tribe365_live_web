<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Client;
use DB;
use Mail;


class ApiOrganisationController extends Controller
{


    //get organisation list
  public function addOrganisation(){

    $postData = Input::all();

    $resultArray = array();
    
    $status   = DB::table('users')->where('email',$postData['email'])->where('status','Active')->first();
    
    if(!empty($status))
    {          
     return response()->json(['code'=>400,'status'=>false,'service_name'=>'add-organisation','message'=>'Email already exists','data'=>$resultArray]);
   }
   else
   { 
        //check img
    if($postData['organisation_logo']){
      $image = $postData['organisation_logo'];
      $image = str_replace('data:image/png;base64,', '', $image);
      $image = str_replace(' ', '+', $image);
      $imageName = 'org_'.time().'.png'; 
      \File::put(public_path('uploads/org_images/'). $imageName, base64_decode($image));
    } 
    else
    {
      $imageName='';
    }

    $orgInsertArray = array(
      'organisation'       => $postData['organisation'],
      'industry'           => $postData['industry'],           
      'ImageURL'           => $imageName,
      'address1'           => $postData['address'],      
      'phone'              => $postData['phone'],          
      'numberOfEmployees'  => 0,
      'turnover'           => $postData['turnover'],
      'address2'           => $postData['address2'],
      'numberOfOffices'    => count($postData['offices']),
      'numberOfDepartments'=> 0,
      'lead_name'          => $postData['lead_name'],
      'lead_email'         => $postData['lead_email'],
      'lead_phone'         => $postData['lead_phone'],
      'status'             => 'Active',            
      'created_at'         => date('Y-m-d H:i:s'),
      
    );

    $org = DB::table('organisations')
    ->where('organisation',$postData['organisation'])
    ->first();

    if($org)
    {
      return response()->json(['code'=>400,'status'=>false,'service_name'=>'add-organisation','message'=>'Organisation already exists','data'=>$orgInsertArray]);
    }
    
    $orgId = DB::table('organisations')->insertGetId($orgInsertArray);

    if ($orgId) {

      foreach ($postData['offices'] as $value) {

        $officeInsertArray =array(
          'office'  => $value['office'], 
          'address' => $value['address'],
          'city'    => $value['city'],
          'country' => $value['country'],
          'phone'   => $value['phone'],
          'status'  => 'Active',
          'orgId'   => $orgId,   
          'numberOfEmployees'=> $value['noOfEmployee'],
          'numberOfDepartments'=>0,                            
          'created_at'=>date('Y-m-d H:i:s'),
        );

        $userStatus = DB::table('offices')->insertGetId($officeInsertArray);
      }

      /*      $clientInsertArray =array(
                'name'      => ucfirst($postData['name']),
                'email'     => $postData['email'],
                'password'  => bcrypt($postData['password']),
                'password2' => base64_encode($postData['password']),
                'status'    => 'Active',
                'orgId'     => $orgId,
                'roleId'    => 2,
                'created_at'=> date('Y-m-d H:i:s'),
            );

            $userStatus = DB::table('users')->insertGetId($clientInsertArray);*/
          }

          if($orgId){

            $organisation = DB::table('organisations') 
            ->where('id',$orgId)  
            ->where('status','Active')    
            ->latest()
            ->first();

            if($organisation) {

              $result['organisation_id']    = $orgId;
              $result['organisation']       = $organisation->organisation;
              $result['industry']           = $organisation->industry;
              $result['numberOfEmployees']  = $organisation->numberOfEmployees;
              $result['numberOfOffices']    = $organisation->numberOfOffices;
              $result['numberOfDepartments']= $organisation->numberOfDepartments;
              $result['address1']           = $organisation->address1;
              $result['address2']           = $organisation->address2;
              $result['turnover']           = $organisation->turnover;                
              $result['phone']              = $organisation->phone;      
              $result['status']             = $organisation->status; 

              $result['organisation_logo']  = url('/public/uploads/org_images').'/'. $organisation->ImageURL;

              $user = DB::table('users')->where('orgId', $orgId)->where('roleId',2)->first();

              $result['name'] =''; 

              if(!empty($user))
              {
               $result['name'] =$user->name; 
             }

             $dotStaus = DB::table('dots')
             ->where('orgId',$organisation->id)
             ->first();

             if($dotStaus)
             {
              $result['isDot']  = true; 
            }
            else
            {
              $result['isDot']  =false;
            }

            $offices = DB::table('offices')
            ->where('status','Active')
            ->where('orgId',$organisation->id)
            ->get();

            $officesArray = array();

            foreach ($offices as $key => $office_value) {

             $office_res['office_id'] = $office_value->id;
             $office_res['office']    = $office_value->office;
             $office_res['address']   = $office_value->address;
             $office_res['city']      = $office_value->city;
             $office_res['country']   = $office_value->country;
             $office_res['phone']     = $office_value->phone;

             array_push($officesArray, $office_res);
           }

           $result['offices'] = $officesArray;

           array_push($resultArray, $result);
         } 
         else
         {

          return response()->json(['code'=>400,'status'=>false,'service_name'=>'add-organisation','message'=>'Organisations','data'=>$resultArray]);
        }
      }

      return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-organisation','message'=>'Organisation added successfully','data'=>$resultArray]);
    }

  }


  public function getOrganisationList_test()
  {

    $page           = Input::get('page');

    $recordLimit    = 10;
    $pageCount      = $page - 1; 
    $offset         = $recordLimit * $pageCount;
    $totalPageCount = 0;

    $wrclaue = array();

    $wrclaue['status'] = 'Active';

    $pageCount = app('App\Http\Controllers\API\ApiCommonController')->getPageCountQuery('organisations', $wrclaue);   
    $totalPageCount = app('App\Http\Controllers\API\ApiCommonController')->getPageCount($pageCount,$recordLimit);

    if(!empty($page))
    {
     $organisation  = DB::table('organisations')->orderBy('id','DESC')->where('status','Active')->offset($offset)->limit($recordLimit)->get();
   }else{
    $organisation  = DB::table('organisations')->orderBy('id','DESC')->where('status','Active')->get();
  }

  $controller = new ApiCommonController();

  $resultArray = array();

  $counter=0;
  foreach ($organisation as $key => $value)
  {
    $result['index']              = $counter;
    $result['organisation_id']    = $value->id;
    $result['organisation']       = $value->organisation;
    $result['industry']           = $value->industry;
    $result['numberOfEmployees']  = $value->numberOfEmployees;
    $result['numberOfOffices']    = $department_array = $controller->callAction('getOfficeCount',array($value->id));
    $result['numberOfDepartments']= $department_array = $controller->callAction('getDepartmentCount',array($value->id));
    $result['address1']           = $value->address1 . ', '.$value->address2;
    $result['address2']           = $value->address2;
    $result['phone']              = $value->phone;        
    $result['turnover']           = $value->turnover;
    $result['status']             = $value->status;  

    $result['lead_name']          = $value->lead_name;        
    $result['lead_email']         = $value->lead_email;
    $result['lead_phone']         = $value->lead_phone;

    if($value->ImageURL)
    {
      $result['organisation_logo']  = url('/public/uploads/org_images').'/'. $value->ImageURL;
    } 
    else
    {
      $result['organisation_logo']  = url('/public/images/no-image.png');
    }


    $result['reportPdfUrl'] = '';
    if($value->PdfReportUrl)
    {
     $result['reportPdfUrl'] = url('/public/pdf_report').'/'.$value->PdfReportUrl; 
   }



   $user = DB::table('users')->select('name','email')->where('roleId',2)->where('orgId',$value->id)->first();
   if(!empty($user))
   {
    $result['name']              = $user->name;  
    $result['email']             = $user->email;  
  }


  $dotStaus = DB::table('dots')
  ->where('orgId',$value->id)
  ->first();

  if($dotStaus)
  {
    $result['isDot']  = true; 
  } 
  else
  {
    $result['isDot']  = false;
  }


  $offices = DB::table('offices')
  ->where('status','Active')
  ->where('orgId',$value->id)
  ->get();

  $officesArray = array();

  foreach ($offices as $key => $office_value) {

   $office_res['office_id'] = $office_value->id;
   $office_res['office']    = $office_value->office;
   $office_res['numberOfEmployees']    = $office_value->numberOfEmployees;
   $office_res['address']   = $office_value->address;
   $office_res['city']      = $office_value->city;
   $office_res['country']   = $office_value->country;
   $office_res['phone']     = $office_value->phone;

   array_push($officesArray, $office_res);
 }

 $result['offices'] = $officesArray;

 array_push($resultArray, $result);


 $counter++;
}

return response()->json(['code'=>200,'status'=>true,'service_name'=>'organisation-list','message'=>'Organisations','data'=>$resultArray,'totalPageCount'=>$totalPageCount,'currentPage'=>$page]);

}


    //get organisation list
public function getOrganisationList()
{

 $organisation = DB::table('organisations')->orderBy('id','DESC')->where('status','Active')->get();

 $controller = new ApiCommonController();

 $resultArray = array();

 $counter=0;
 foreach ($organisation as $key => $value) {
  $result['index']              = $counter;
  $result['organisation_id']    = $value->id;
  $result['organisation']       = $value->organisation;
  $result['industry']           = $value->industry;
  $result['numberOfEmployees']  = $value->numberOfEmployees;
  $result['numberOfOffices']    = $department_array = $controller->callAction('getOfficeCount',array($value->id));
  $result['numberOfDepartments']= $department_array = $controller->callAction('getDepartmentCount',array($value->id));
  $result['address1']           = $value->address1 . ', '.$value->address2;
  $result['address2']           = $value->address2;
  $result['phone']              = $value->phone;        
  $result['turnover']           = $value->turnover;
  $result['status']             = $value->status;  

  $result['lead_name']          = $value->lead_name;        
  $result['lead_email']         = $value->lead_email;
  $result['lead_phone']         = $value->lead_phone; 

  if($value->ImageURL)
  {
    $result['organisation_logo']  = url('/public/uploads/org_images').'/'. $value->ImageURL;
  } 
  else
  {
    $result['organisation_logo']  = url('/public/images/no-image.png');
  }

  $result['reportPdfUrl'] = '';
  if($value->PdfReportUrl)
  {
   $result['reportPdfUrl'] = url('/public/uploads/pdf_report').'/'.$value->PdfReportUrl; 
 }

 $user = DB::table('users')->select('name','email')->where('roleId',2)->where('orgId',$value->id)->first();
 if(!empty($user))
 {
  $result['name']              = $user->name;  
  $result['email']             = $user->email;  
}

$dotStaus = DB::table('dots')->where('orgId',$value->id)->first();

if($dotStaus)
{
  $result['isDot']  = true; 
} 
else
{
  $result['isDot']  =false;
}


$offices = DB::table('offices')
->where('status','Active')
->where('orgId',$value->id)
->get();

$officesArray = array();

foreach ($offices as $key => $office_value) {

 $office_res['office_id'] = $office_value->id;
 $office_res['office']    = $office_value->office;
 $office_res['numberOfEmployees']    = $office_value->numberOfEmployees;
 $office_res['address']   = $office_value->address;
 $office_res['city']      = $office_value->city;
 $office_res['country']   = $office_value->country;
 $office_res['phone']     = $office_value->phone;

 array_push($officesArray, $office_res);
}

$result['offices'] = $officesArray;

array_push($resultArray, $result);


$counter++;
}

return response()->json(['code'=>200,'status'=>true,'service_name'=>'organisation-list','message'=>'Organisations','data'=>$resultArray]);

}

//get custom department
public function getDepartmentList() {

  // $departments = DB::table('all_department')
  // ->where('status','Active')
  // ->get();

  $users = Auth::user();
  $orgId = $users->orgId;

  $departments = DB::table('all_department')
  ->select('all_department.id','all_department.department')
  ->distinct()
  ->leftJoin('departments', 'departments.departmentId','all_department.id')
  ->leftJoin('users', 'users.departmentId','departments.id')
  ->where('departments.orgId',$orgId)
  ->where('all_department.status','Active')
  ->where('departments.status','Active')
  ->where('users.status','Active')
  ->get();

  $resultArray = array();

  foreach ($departments as $key => $value) {

    $result['id'] = $value->id;
    $result['department'] = strtoupper($value->department);

    array_push($resultArray, $result);
  }

  if($resultArray){

    return response()->json(['code'=>200,'status'=>true,'service_name'=>'department-list','message'=>'Department list','data'=>$resultArray]);

  } else {
    return response()->json(['code'=>400,'status'=>false,'service_name'=>'department-list','message'=>'Departments not found','data'=>$resultArray]);

  }

}


/*add office users*/
public function addOfficeUser()
{

  $postData = Input::all();

  $resultArray = array();
  
  $status = DB::table('users')->where('email',$postData['email'])->where('status','Active')->first();

  if($status)
  {          
    return response()->json(['code'=>400,'status'=>false,'service_name'=>'add-office-users','message'=>"This email already exists",'data'=>[]]);
  }


  $usreInserArray = array(
    'name'=> ucfirst($postData['name']),
    'email'=> $postData['email'],
    // 'password'=> bcrypt($postData['password']),
    // 'password2'=> base64_encode($postData['password']),
    'orgId'=> $postData['orgId'],
    'officeId'=> $postData['officeId'],
    'departmentId'=> 0,
    'status'=>'Active',
    'roleId'=>3,
    'created_at'=>date('Y-m-d H:i:s'),
  );

  $user = DB::table('users')->insertGetId($usreInserArray);

  $departmentInsertArray = array(
    'orgId'=> $postData['orgId'],
    'officeId'=> $postData['officeId'],
    'departmentId'=> $postData['departmentId'],
    'status'=>'Active',
    'created_at'=>date('Y-m-d H:i:s'),
  );

  $isDepartment = DB::table('departments')->where('officeId',$postData['officeId'])->where('departmentId',$postData['departmentId'])->first(); 

  if(empty($isDepartment)){

    $departments = DB::table('departments')->insertGetId($departmentInsertArray);

    DB::table('users')->where('id', $user)->update(['departmentId'=>$departments]);

  }else{

   $departments =  $isDepartment->id; 

   DB::table('users')->where('id', $user)->update(['departmentId'=>$departments]);
 }

 /*send email to new user*/
 $data  = array('name'=>$postData['name'] ,'passwordLink'=>base64_encode($postData['email']));

 $email = $postData['email'];
 Mail::send('layouts/userPasswordSetEmail', $data, function($message) use($email) 
 {
  $message->from('notification@tribe365.co', 'Tribe365');
  $message->to($email);
  $message->subject('Verify your email address');
});


 return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-office-users','message'=>'User added successfully','data'=>$resultArray]);

}


/*To Check Password Link*/
public function setUserPassword()
{

  $token = request()->segment(2);
  $email = base64_decode(request()->segment(2));

  $result = DB::table('users')->where('email',$email)->where('status','Active')->first();

  if($result)
  {        

   $time       = $result->created_at;
   $time       = date('Y-m-d H:i',strtotime('+5 days',strtotime($time)));   
   $checkTime  = date('Y-m-d H:i');

   if($checkTime < $time)
   {

    $data = array('email'=>$email,'token'=>'updateNewUserpassword/'.$token);

    return view('layouts/updateNewUserpassword',compact('data'));
    
  }
  else
  {
    echo '<p style="text-align: center;background: #eb1c24;color: #fff; font-size: 25px;font-weight: bold; padding: 15px 5px;
    text-align: center;">Invalid Link</p>';
  }

}
else
{
  echo '<p style="text-align: center;background: #eb1c24;color: #fff; font-size: 25px;font-weight: bold; padding: 15px 5px;
  text-align: center;">Invalid Link</p>';
}    

}


/*To update password*/
public function updateNewUserpassword()
{

  $email            = Input::get('email')?Input::get('email'):'';
  $password         = Input::get('password')?Input::get('password'):'';

  $result = DB::table('users')->where('email',$email)->first();

  $rules = array('email'=>'required','password'=>'required','confirm_password'=>'required|same:password'); 

  if($result)
  {                                             
   $validator   = Validator::make(Input::all(),$rules);

   if($validator->fails())
   {
     return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
   }
   else
   { 

     $updateArray = array('password'=> str_replace("&nbsp;", '',bcrypt($password)),'password2'=> str_replace("&nbsp;", '',base64_encode($password)));

     DB::table('users')->where('email',$email)->update($updateArray);
     
     return redirect()->back()->with(['message'=>'Email address verified.']);
     
   }

 }else{

   echo '<p style="text-align: center;background: #eb1c24;color: #fff; font-size: 25px;font-weight: bold; padding: 15px 5px;
   text-align: center;">Invalid Link</p>';
 } 

}


public function updateOrganisation()
{
  $postData = Input::all();

  $insertArray['organisation']       = $postData['organisation'];
  $insertArray['industry']           = $postData['industry'];
  $insertArray['address1']           = $postData['address'];
  $insertArray['address2']           = $postData['address2'];
  $insertArray['phone']              = $postData['phone'];    
  $insertArray['lead_name']          = $postData['lead_name'];
  $insertArray['lead_email']         = $postData['lead_email'];
  $insertArray['lead_phone']         = $postData['lead_phone'];
  $insertArray['turnover']           = $postData['turnover'];
  $insertArray['updated_at']         = date('Y-m-d H:i:s');

  DB::table('organisations')->where('id', $postData['id'])->update($insertArray);

  foreach ($postData['offices'] as $value)
  {

    $officeInsertArray =array(
      'office'             => $value['office'], 
      'address'            => $value['address'],
      'city'               => $value['city'],
      'country'            => $value['country'],
      'phone'              => $value['phone'],
      'numberOfEmployees'  => $value['noOfEmployee'],
      'updated_at'         => date('Y-m-d H:i:s')
    );

    DB::table('offices')->where('id', $value['office_id'])->update($officeInsertArray);
  }          

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'update-organisation','message'=>'Organisation updated successfully','data'=>'']);           
}

public function addOffice()
{
  $postData = Input::all();


  $officeInsertArray =array(
    'office'  => $postData['office'], 
    'address' => $postData['address'],
    'city'    => $postData['city'],
    'country' => $postData['country'],
    'phone'   => $postData['phone'],
    'status'  => 'Active',
    'orgId'   => $postData['orgId'],   
    'numberOfEmployees'=>$postData['noOfEmployee'],
    'numberOfDepartments'=>0,                            
    'created_at'=>date('Y-m-d H:i:s'),
  );

  $status = DB::table('offices')->insertGetId($officeInsertArray);   

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-office','message'=>'office added successfully','data'=>$officeInsertArray]);         

}

public function updateLogo()
{
  $postData = Input::all();
  if($postData['organisation_logo']){
    $image = $postData['organisation_logo'];
    $image = str_replace('data:image/png;base64,', '', $image);
    $image = str_replace(' ', '+', $image);
    $imageName = 'org_'.time().'.png'; 
    \File::put(public_path('uploads/org_images/'). $imageName, base64_decode($image));

    $insertArray['ImageURL'] = $imageName;
    DB::table('organisations')
    ->where('id', $postData['id'])
    ->update($insertArray);
    $data['logo']=  url('/public/uploads/org_images').'/'. $imageName;
    return response()->json(['code'=>200,'status'=>true,'service_name'=>'update-logo','message'=>'logo updated successfully','data'=>$data]);         

  } 
}
/*get usrs by its type organisation/office/department */
public function getUserByType()
{

  $resultArray = array();

  $type    = Input::get('type');
  $typeId  = Input::get('typeId');

  $whereArr = array();
  if($type=='organisation'){

    $whereArr = array('orgId' =>$typeId);

  }else if($type=='office'){

    $whereArr = array('officeId'=> $typeId);
  }else if($type=='department'){

    $whereArr = array('departmentId'=> $typeId);
  }

  $users = DB::table('users')
  ->select('id','name','lastName','email','status','roleId','orgId','officeId','departmentId')
  ->where($whereArr)
  ->where('status','Active')
  ->where('roleId',3)
  ->get();


  return response()->json(['code'=>200,'status'=>true,'service_name'=>'get-user-by-type','message'=>'Users','data'=>$users]); 

}

/*add office users*/
public function updateStaffDetail()
{

  $resultArray = array();

  $postData = Input::all();

  $userId   = $postData['userId'];
  $name     = $postData['name'];
  // $password = $postData['password'];
  $orgId    = $postData['orgId'];
  $officeId = $postData['officeId'];
  $departmentId = $postData['departmentId'];

  $updateArray = array();
  $updateArray['name']         = ucfirst($name);       
  // $updateArray['password']     = bcrypt($password);
  // $updateArray['password2']    = base64_encode($password);
  $updateArray['officeId']     = $officeId;
  $updateArray['orgId']        = $orgId;             
  $updateArray['updated_at']   = date('Y-m-d H:i:s');

    // DB::table('users')->where('id', $userId)->update($updateArray);

    // $userStatus = DB::table('users')->where('id',$userId)->where('status','Active')->first();


  $department = DB::table('departments')->where('officeId',$officeId)->where('departmentId',$departmentId)->first();

  if (!empty($department))
  {
   $departmentId = $department->id;

 }else{

  $departmentId = DB::table('departments')->insertGetId(['officeId'=>$officeId,'departmentId'=>$departmentId,'orgId'=>$orgId,'created_at'=>date('Y-m-d H:i:s')]);
}

$updateArray['departmentId'] = $departmentId;

DB::table('users')->where('id', $userId)->update($updateArray);

/*if($userStatus)
{
    DB::table('departments')->where('id', $userStatus->departmentId)->update(['departmentId'=>$departmentId,'updated_at'=> date('Y-m-d H:i:s')]);
  }*/

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'update-staff-detail','message'=>'Staff detail updated successfully.','data'=>$resultArray]);

}
/*remove staff from organisation*/

public function deleteStaff()
{
  $resultArray = array();

  $userId   = Input::get('userId');

  DB::table('users')->where('id', $userId)->update(['status'=>'Inactive']);

  DB::table('oauth_access_tokens')->where('user_id', $userId)->update(['revoked'=>1]);

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'delete-staff','message'=>'Staff deleted successfully.','data'=>$resultArray]);

}

/*get country list*/
public function getCountryList()
{

  $resultArray = array();

  $countryTbl = DB::table('country')->where('status','Active')->orderBy('nicename','ASC')->get();

  foreach($countryTbl as $value)
  {

    $result['id']          = $value->id;
    $result['countryName'] = $value->nicename;

    array_push($resultArray, $result);
  }

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'country-list','message'=>'','data'=>$resultArray]);
}

//delete organisation
public function deleteOrganisation()
{
      $orgId   = Input::get('orgId');

      $data['actions'] = DB::table('actions')->where('orgId',$orgId)->delete();

      $data['actions_comment'] = DB::table('actions_comment')
      ->leftJoin('users','users.id','actions_comment.userId')
      ->where('users.orgId',$orgId)->delete();

      $data['assets'] = DB::table('assets')->where('orgId',$orgId)->delete();

      $data['clients'] = DB::table('clients')->where('orgId',$orgId)->delete();

      $data['cot_answers'] = DB::table('cot_answers')->where('orgId',$orgId)->delete();

      $data['cot_functional_lens_answers'] = DB::table('cot_functional_lens_answers')->where('orgId',$orgId)->delete();

      $data['cot_functional_lens_initial_value_records'] = DB::table('cot_functional_lens_initial_value_records')->where('orgId',$orgId)->delete();

      $data['cot_functional_lens_records'] = DB::table('cot_functional_lens_records')
      ->leftJoin('users','users.id','cot_functional_lens_records.userId')
      ->where('users.orgId',$orgId)->delete();

      $data['departments'] = DB::table('departments')->where('orgId',$orgId)->delete();

      $data['diagnostic_answers'] = DB::table('diagnostic_answers')->where('orgId',$orgId)->delete();

      $data['dots_beliefs'] = DB::table('dots_beliefs')
      ->leftJoin('dots','dots.id','dots_beliefs.dotId')
      ->where('dots.orgId',$orgId)->get();

      //foreach ($data['dots_beliefs'] as $key => $value) {

        $data['dots_values'] = DB::table('dots_values')
        ->leftJoin('dots_beliefs','dots_values.beliefId','dots_beliefs.id')
        ->leftJoin('dots','dots.id','dots_beliefs.dotId')
        ->where('dots.orgId',$orgId)->delete();
      //}

      $data['dots_beliefs_delete'] = DB::table('dots_beliefs')
      ->leftJoin('dots','dots.id','dots_beliefs.dotId')
      ->where('dots.orgId',$orgId)->delete();

      $data['dots'] = DB::table('dots')->where('orgId',$orgId)->delete();

      $data['dot_bubble_rating_records'] = DB::table('dot_bubble_rating_records')
      ->leftJoin('dots','dots.id','dot_bubble_rating_records.dot_id')
      ->where('dots.orgId',$orgId)->delete();

      $data['dot_evidence'] = DB::table('dot_evidence')
      ->leftJoin('dots','dots.id','dot_evidence.dotId')
      ->where('dots.orgId',$orgId)->delete();

      $data['dot_values_ratings'] = DB::table('dot_values_ratings')
      ->leftJoin('dots','dots.id','dot_values_ratings.dotId')
      ->where('dots.orgId',$orgId)->delete();

      $data['happy_indexes'] = DB::table('happy_indexes')
      ->leftJoin('users','users.id','happy_indexes.userId')
      ->where('users.orgId',$orgId)->delete();

      $data['iot_allocated_themes'] = DB::table('iot_allocated_themes')
      ->leftJoin('iot_feedbacks','iot_feedbacks.id','iot_allocated_themes.feedbackId')
      ->leftJoin('iot_themes','iot_themes.id','iot_allocated_themes.themeId')
      ->where('iot_feedbacks.orgId',$orgId)
      ->where('iot_themes.orgId',$orgId)->delete();

      $data['iot_messages'] = DB::table('iot_messages')
      ->leftJoin('iot_feedbacks','iot_feedbacks.id','iot_messages.feedbackId')
      ->where('iot_feedbacks.orgId',$orgId)->delete();

       $data['iot_feedbacks'] = DB::table('iot_feedbacks')->where('orgId',$orgId)->delete();

       $data['iot_themes'] = DB::table('iot_themes')->where('orgId',$orgId)->delete();

       $data['oauth_access_tokens'] = DB::table('oauth_access_tokens')
      ->leftJoin('users','users.id','oauth_access_tokens.user_id')
      ->where('users.orgId',$orgId)->delete();

       $data['offices'] = DB::table('offices')->where('orgId',$orgId)->delete();

       $data['sot_answers'] = DB::table('sot_answers')
      ->leftJoin('users','users.id','sot_answers.userId')
      ->where('users.orgId',$orgId)->delete();

       $data['sot_motivation_answers'] = DB::table('sot_motivation_answers')->where('orgId',$orgId)->delete();

       $data['sot_motivation_user_records'] = DB::table('sot_motivation_user_records')
      ->leftJoin('users','users.id','sot_motivation_user_records.userId')
      ->where('users.orgId',$orgId)->delete();

       $data['tribeometer_answers'] = DB::table('tribeometer_answers')->where('orgId',$orgId)->delete();

       $data['users'] = DB::table('users')->where('orgId',$orgId)->delete();

      $data['organisations'] = DB::table('organisations')->where('id',$orgId)->delete();

      //return json_encode(array('status'=>200,'message'=>'Organisation Successfully Deleted'));
      return response()->json(['code'=>200,'status'=>true,'service_name'=>'delete-organisation','message'=>'Organisation successfully deleted']);

    
}

}
