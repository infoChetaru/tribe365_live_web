<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Client;
use DB;
use Carbon\Carbon;
use DateTime;
use DatePeriod;
use DateInterval;


class ApiDotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
    }

    public function getDotDetail()
    {

    	$resultArray = array();

    	$user = Auth::user();

    	$orgId = Input::get('orgId');
    	
    	$dot_data = DB::table('dots')
    	->where('orgId',$orgId)     
    	->latest()
    	->first();

    	$result = array();

    	if(!empty($dot_data)){  

         //dot data
    		$resultArray['id']                      = $dot_data->id;
    		$resultArray['vision']                  = $dot_data->vision;
    		$resultArray['mission']                 = $dot_data->mission;
    		$resultArray['focus']                   = $dot_data->focus;
    		$resultArray['orgId']                   = $dot_data->orgId;
    		$resultArray['dot_date']                = $dot_data->dot_date;
    		$resultArray['introductory_information']= $dot_data->introductory_information;

    		if($user->roleId==3)
    		{

    			$dot_evidence = DB::table('dot_evidence')
    			->where('dotId',$dot_data->id)    
    			->where('userId',$user->id)
    			->where('status','Active') 
    			->get();

    		}else{

    			$dot_evidence = DB::table('dot_evidence')
    			->where('dotId',$dot_data->id)    
    			->where('status','Active') 
    			->get();

    		}

    		$evidenceArr = array();

    		foreach ($dot_evidence as $evidence) {

    			$evd['id']          = (string)$evidence->id;
    			$evd['description'] = $evidence->description;
    			$evd['userId']      = $evidence->userId;
    			$evd['created_at']  = $evidence->created_at;
    			$evd['section']     = $evidence->section;
    			$evd['sectionId']   = $evidence->sectionId;
    			$evd['fileURL']     = '';

    			if($evidence->fileURL)
    			{
    				$evd['fileURL']     = url('/public/uploads/evidence_images').'/'. $evidence->fileURL;
    			}

    			array_push($evidenceArr, $evd);

    		}

    		$resultArray['evidence'] = $evidenceArr;

         //get beliefs data 
    		$beliefs_data = DB::table('dots_beliefs')
    		->where('dotId',$dot_data->id)     
    		->get();


    		$belief_arr = array();

    		foreach($beliefs_data as $value) {

    			$belief['id'] = $value->id;
    			$belief['name'] = $value->name;
    			$belief['dotId'] = $value->dotId;


    			$dots_value = DB::table('dots_values')
    			->where('beliefId',$value->id)    
    			->where('status','Active') 
    			->get();

    			$dotsValueArr = array();

    			$count = 0;
    			foreach ($dots_value as $Vvalue) {

    				$dotValueList = DB::table('dot_value_list')
    				->where('id',$Vvalue->name)     
    				->first();

    				$dValuesName = '';
    				if($dotValueList){

    					$dValuesName = $dotValueList->name;
    				}

    				$dotV["index"]     = $count;
    				$dotV["id"]        = $Vvalue->id;
    				$dotV['name']      = $dValuesName;
    				$dotV["valueId"]   = $Vvalue->name;
    				$dotV["beliefId"]  = $Vvalue->beliefId;
    				$dotV["status"]    = $Vvalue->status;
    				$dotV["created_at"]= $Vvalue->created_at;
    				$dotV["updated_at"]=$Vvalue->updated_at;
    				$dotV['dotId']     = $value->dotId;



    				$valuesRatings = db::table('dot_values_ratings')
    				->where('valueId', $Vvalue->id)
    				->where('userId',$user->id)
    				->first();

    				$dotV["ratings"]='';

    				if($valuesRatings){
    					$dotV["ratings"]= $valuesRatings->ratings;
    				}

    				array_push($dotsValueArr, $dotV);

    				$count++;
    			}

    			$belief['belief_value'] = $dotsValueArr;

    			array_push($belief_arr, $belief);
    		}

    		$resultArray['belief'] = $belief_arr;

    		return response()->json(['code'=>200,'status'=>true,'service_name'=>'dot-detail','message'=>'DOT Detail','data'=>$resultArray]);

    	} 
    	else
    	{
    		return response()->json(['code'=>400,'status'=>false,'service_name'=>'dot-detail','message'=>'DOT Detail not found','data'=>$resultArray]);
    	}

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addDot()
    {

    	$vision   = Input::get('vision');
    	$mission  = Input::get('mission');
    	$focus    = Input::get('focus');
    	$orgId    = Input::get('orgId');
    	$dotDate  = Input::get('dot_date');
    	$introductory_information = Input::get('introductory_information');

    	$resultArray = array();

    	$status = DB::table('dots')
    	->where('orgId',$orgId)    
    	->first();

    	if($status){

    		return response()->json(['code'=>400,'status'=>false,'service_name'=>'add-dot-detail','message'=>'DOT already exists for this organisation','data'=>$resultArray]);

    	} else {

    		$dotInsertArray = array(
    			'vision'=> $vision,
    			'mission'=> $mission,
    			'focus'=> $focus,
    			'orgId'=> $orgId,
    			'introductory_information'=> $introductory_information,
    			'dot_date'=> date('Y-m-d H:i:s', strtotime($dotDate)),
    			'created_at'=> date('Y-m-d H:i:s')
         );

    		$dotId = DB::table('dots')->insertGetId($dotInsertArray);

    		if(!empty($dotId)){ 

    			$dotsBeliefs = Input::get('belief');

           //add dot beliefs
    			foreach($dotsBeliefs as $belief) {

    				$dotBeliefInsertArray = array(
    					'name'=> $belief['name'],
    					'dotId' => $dotId,
    					'created_at'=> date('Y-m-d H:i:s')
                   );

    				$dotBeliefId = DB::table('dots_beliefs')->insertGetId($dotBeliefInsertArray);

               //add beliefs values
    				foreach ($belief['belief_value'] as $belief_value)
    				{

    					$dotBeliefValueInsertArray = array(
    						'name'=> $belief_value['id'],
    						'beliefId'=> $dotBeliefId,
    						'created_at'=> date('Y-m-d H:i:s')
                      );

    					$dotBeliefValueId = DB::table('dots_values')->insertGetId($dotBeliefValueInsertArray);            

            }//dot belief value foreach

            }//dot belief foreach


            $dot_data = DB::table('dots')
            ->where('orgId',$orgId)
            ->latest()     
            ->first();

            if(!empty($dot_data)){  

         //dot data
            	$resultArray['id']      = $dot_data->id;
            	$resultArray['vision']  = $dot_data->vision;
            	$resultArray['mission'] = $dot_data->mission;
            	$resultArray['focus']   = $dot_data->focus;
            	$resultArray['orgId']   = $dot_data->orgId;
            	$resultArray['dot_date']= $dot_data->dot_date;
            	$resultArray['introductory_information']= $dot_data->introductory_information;

         //get beliefs data 
            	$beliefs_data = DB::table('dots_beliefs')
            	->where('dotId',$dot_data->id)     
            	->get();


            	$belief_arr = array();

            	foreach($beliefs_data as $value) {

            		$belief['id'] = $value->id;
            		$belief['name'] = $value->name;
            		$belief['dotId'] = $value->dotId;


            		$dots_value = DB::table('dots_values')
            		->where('beliefId',$value->id)  
            		->where('status','Active')   
            		->get();

            		$dotsValueArr = array();

            		$count = 0;
            		foreach ($dots_value as $Vvalue) {

            			$dotValueList = DB::table('dot_value_list')
            			->where('id',$Vvalue->name)     
            			->first();

            			$dValuesName = '';
            			if($dotValueList){

            				$dValuesName = $dotValueList->name;
            			}

            			$dotV["index"]     = $count;
            			$dotV["id"]        = $Vvalue->id;
            			$dotV["name"]      = $dValuesName;
            			$dotV["beliefId"]  = $Vvalue->beliefId;
            			$dotV["status"]    = $Vvalue->status;
            			$dotV["created_at"]= $Vvalue->created_at;
            			$dotV["updated_at"]= $Vvalue->updated_at;
            			$dotV['dotId']     = $value->dotId;

            			array_push($dotsValueArr, $dotV);
            			$count++;
            		}


            		$belief['belief_value'] = $dotsValueArr;

            		array_push($belief_arr, $belief);
            	}

            	$resultArray['belief'] = $belief_arr;
            }

            return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-dot-detail','message'=>'Dot added successfully','data'=>$resultArray]);

        } else {

        	return response()->json(['code'=>400,'status'=>false,'service_name'=>'add-dot-detail','message'=>'Dot not added','data'=>$resultArray]);
        }
    }
}

/*add evidence*/
public function addDotEvidence(Request $request)
{

	$postData = Input::all();

	$user = Auth::user();

	$resultArray = array();

	if(!empty($postData['file']))
	{               

		$image = $request->file('file');

		$fileName   = 'evidence_'.time().'.'.$image->getClientOriginalExtension();

		$destinationPath = public_path('/uploads/evidence_images/');

		$status = $image->move($destinationPath, $fileName);

	}else{

		$fileName='';
	}
	/*one field is required*/
	if(empty($postData['description']) && empty($fileName)){

		return response()->json(['code'=>400,'status'=>false,'service_name'=>'add-evidence','message'=>'One field required','data'=>$resultArray]);

	}

	$insertArray = array(
		'description'=> $postData['description'],
		'fileURL'    => $fileName,
		'userId'     => $user->id,
		'dotId'      => $postData['dotId'],
		'section'    => $postData['section'],
		'sectionId'  => $postData['sectionId'],
		'created_at' => date('Y-m-d H:i:s')
  );

	$status = DB::table('dot_evidence')->insertGetId($insertArray);      

	if($fileName){

		$resultArray['file_name'] = url('/public/uploads/evidence_images').'/'. $fileName;
	} 
	else
	{
		$resultArray['file_name'] ="";
	}     
	$resultArray['id'] =$status;
	$resultArray['description'] =$insertArray['description'];
	$resultArray['created_at'] =$insertArray['created_at'];

       //send notification mail Pooja Nenava added a new evidence for mission/vision/focus of Organisation name

	$user = Auth::user();

	$organisation = DB::table('organisations')->select('organisation')->where('id',$user->orgId)->first();

	$orgName ='';
	if($organisation){
		$orgName = $organisation->organisation;
	}

	$msg =  '<strong>'.ucfirst($user->name).'</strong>'.' added a new evidence for ' .$postData['section']. ' of Organisation: <strong>'.$orgName.'</strong>';

	
	$notificationArray = array('msg'=>$msg);

	$controller = app('App\Http\Controllers\Admin\CommonController')->sendNotificationMail($notificationArray);

	return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-evidence','message'=>'Evidence added successfully','data'=>$resultArray]);
}


public function updateDotEvidence(Request $request)
{

	$postData = Input::all();
      // print_r($postData);

      // die();

	$user = Auth::user();

	$resultArray = array();

	if(!empty($postData['file']))
	{               

		$image = $request->file('file');

		$fileName   = 'evidence_'.time().'.'.$image->getClientOriginalExtension();

		$destinationPath = public_path('/uploads/evidence_images/');

		$status = $image->move($destinationPath, $fileName);

	} 
	else
	{
		$fileName='';
	}
	/*one field is required*/
	if(empty($postData['description']) && empty($fileName)){

		return response()->json(['code'=>400,'status'=>false,'service_name'=>'add-evidence','message'=>'One field required','data'=>$resultArray]);

	}

	$insertArray = array(
		'description'=> $postData['description'],
		'fileURL'    => $fileName,
		'updated_at' => date('Y-m-d H:i:s')
  );
	
	

	DB::table('dot_evidence')
	->where('id',$postData['id'])
	->update($insertArray);     

	if($fileName){

		$resultArray['file_name'] = url('/public/uploads/evidence_images').'/'. $fileName;
	} 
	else
	{
		$resultArray['file_name'] ="";
	}    
	$resultArray['description'] =  $insertArray['description'];
	$resultArray['updated_at']  =  $insertArray['updated_at'];
	$resultArray['id']          =  $postData['id'];

	

	return response()->json(['code'=>200,'status'=>true,'service_name'=>'update-evidence','message'=>'Evidence updated successfully','data'=>$resultArray]);
}


/*get belief values list*/
public function getBeliefValuesList()
{

	$resultArray  = array();

	$customValues = DB::table('dot_value_list')
	->where('status','Active')
	->orderBy('name','ASC') 
	->get();


	foreach ($customValues as $value) {

		$result['id']   = $value->id;
		$result['name'] = $value->name;

		array_push($resultArray, $result);
	}

	if($resultArray){

		return response()->json(['code'=>200,'status'=>true,'service_name'=>'get-belief-values','message'=>'Belief values list','data'=>$resultArray]);

	} else {

		return response()->json(['code'=>400,'status'=>false,'service_name'=>'get-belief-values','message'=>'Belief values not found','data'=>$resultArray]);
	}
}


public function updateDot()
{

	$vision   = Input::get('vision');
	$mission  = Input::get('mission');
	$focus    = Input::get('focus');
	$orgId    = Input::get('orgId');
	$dotDate  = Input::get('dot_date');
	$dotId  = Input::get('dot_id');
	$introductory_information = Input::get('introductory_information');

	$resultArray = array();

	$dotInsertArray = array(
		'vision'=> $vision,
		'mission'=> $mission,
		'focus'=> $focus,
		'orgId'=> $orgId,
		'introductory_information'=> $introductory_information,
		'dot_date'=> date('Y-m-d H:i:s', strtotime($dotDate)),
		'updated_at'=> date('Y-m-d H:i:s')
  );


	DB::table('dots')
	->where('id', $dotId)
	->update($dotInsertArray);

	
	$dotsBeliefs = Input::get('belief');

           //add dot beliefs
	foreach($dotsBeliefs as $belief) {

		$dotBeliefInsertArray = array(
			'name'=> $belief['name'],
			'updated_at'=> date('Y-m-d H:i:s')
     );


		DB::table('dots_beliefs')
		->where('id', $belief['belief_id'])
		->update($dotBeliefInsertArray);

               //add beliefs values
		foreach ($belief['belief_value'] as $belief_value)
		{

			$dotBeliefValueInsertArray = array(
				'name'=> $belief_value['id'],

				'updated_at'=> date('Y-m-d H:i:s')
            );


			DB::table('dots_values')
			->where('id', $belief_value['value_id'])
			->update($dotBeliefValueInsertArray);            

            }//dot belief value foreach

            }//dot belief foreach


            $dot_data = DB::table('dots')
            ->where('orgId',$orgId)
            ->latest()     
            ->first();

            if(!empty($dot_data)){  

         //dot data
            	$resultArray['id']      = $dot_data->id;
            	$resultArray['vision']  = $dot_data->vision;
            	$resultArray['mission'] = $dot_data->mission;
            	$resultArray['focus']   = $dot_data->focus;
            	$resultArray['orgId']   = $dot_data->orgId;
            	$resultArray['dot_date']= $dot_data->dot_date;
            	$resultArray['introductory_information']= $dot_data->introductory_information;

         //get beliefs data 
            	$beliefs_data = DB::table('dots_beliefs')
            	->where('dotId',$dot_data->id)     
            	->get();


            	$belief_arr = array();

            	foreach($beliefs_data as $value) {

            		$belief['id'] = $value->id;
            		$belief['name'] = $value->name;
            		$belief['dotId'] = $value->dotId;


            		$dots_value = DB::table('dots_values')
            		->where('beliefId',$value->id)    
            		->where('status','Active') 
            		->get();

            		$dotsValueArr = array();

            		$count = 0;
            		foreach ($dots_value as $Vvalue) {

            			$dotValueList = DB::table('dot_value_list')
            			->where('id',$Vvalue->name)     
            			->first();

            			$dValuesName = '';
            			if($dotValueList){

            				$dValuesName = $dotValueList->name;
            			}

            			$dotV["index"]     = $count;
            			$dotV["id"]        = $Vvalue->id;
            			$dotV["name"]      = $dValuesName;
            			$dotV["beliefId"]  = $Vvalue->beliefId;
            			$dotV["status"]    = $Vvalue->status;
            			$dotV["created_at"]= $Vvalue->created_at;
            			$dotV["updated_at"]= $Vvalue->updated_at;
            			$dotV['dotId']     = $value->dotId;

            			array_push($dotsValueArr, $dotV);
            			$count++;
            		}


            		$belief['belief_value'] = $dotsValueArr;

            		array_push($belief_arr, $belief);
            	}

            	$resultArray['belief'] = $belief_arr;
            }

            return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-dot-detail','message'=>'Dot added successfully','data'=>$resultArray]);



        }


        public function addBelief()
        {

        	$dotBeliefInsertArray = array(
        		'name'=> Input::get('name'),
        		'dotId'=> Input::get('dotId'),
        		'created_at'=> date('Y-m-d H:i:s')
          );

        	$beliefId = DB::table('dots_beliefs')->insertGetId($dotBeliefInsertArray);

        	$beliefValue = Input::get('belief_value');

        	foreach ($beliefValue as  $value)
        	{

        		$dotValueInsertArray = array(
        			'name'       => $value['id'],
        			'beliefId'   => $beliefId,
        			'status'     =>'Active',
        			'created_at' => date('Y-m-d H:i:s')
             );

        		$Value = DB::table('dots_values')->insertGetId($dotValueInsertArray); 

        	}

        	$dotBeliefInsertArray['id'] = $beliefId;

        	if($beliefId)
        	{
        		return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-dot-belief','message'=>'Belief added successfully','data'=>$dotBeliefInsertArray]);
        	}  

        }




        public function addValue()
        {
        	$value = Input::get('values');

        	$resultArray = array();

        	foreach ($value as $value)
        	{

        		$dotValueInsertArray = array(
        			'name'=> $value['valueId'],
        			'beliefId'=> Input::get('beliefId'),
        			'created_at'=> date('Y-m-d H:i:s')
             );

        		$Value = DB::table('dots_values')->insertGetId($dotValueInsertArray);

        		$valueId = $value['valueId'];
        		$beliefId= Input::get('beliefId');

        		$dotValueList = DB::table('dot_value_list')
        		->where('id',$valueId)     
        		->first();

        		$dValuesName = '';
        		if($dotValueList){

        			$dValuesName = $dotValueList->name;
        		}

        		$dotV["id"]        = $Value;
        		$dotV["valueId"]   = $valueId;
        		$dotV["name"]      = $dValuesName;
        		$dotV["beliefId"]  = $beliefId;
        		$dotV["beliefName"]  =Input::get('beliefName');

        		array_push($resultArray, $dotV);

        	}

        	if($Value)
        	{
        		return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-belief-values','message'=>'Values added successfully.','data'=>$resultArray]);
        	}  

        }

        /*delete dot values*/
        public function deleteDotValue()
        {

        	$resultArray  = array();

        	$dotValueId   = Input::get('id');

        	$updatetArray = array('status'=>'Inactive');

        	DB::table('dots_values')
        	->where('id',$dotValueId)
        	->update($updatetArray);  

        	return response()->json(['code'=>200,'status'=>true,'service_name'=>'delete-dot-value','message'=>'DOT value deleted successfully','data'=>$resultArray]);
        }

        /*get evidence list*/
        public function getEvidenceList()
        {

        	$resultArray = array();

        	$dotId      = Input::get('dotId');
        	$sectionId  = Input::get('sectionId');
        	$section    = Input::get('section');

        	$wrCls = array();

        	if($sectionId){
        		$wrCls['sectionId']= $sectionId;
        	}

        	$wrCls['section']= $section;
        	$wrCls['dotId']= $dotId;

        	$dotEvidence = DB::table('dot_evidence')
        	->where('status','Active')
        	->where($wrCls)      
        	->orderBy('id','DESC')
        	->get();

        	foreach ($dotEvidence as $evidence)
        	{
        		$evd['id']          = $evidence->id;
        		$evd['description'] = $evidence->description;
        		$evd['userId']      = $evidence->userId;
        		$evd['created_at']  = $evidence->created_at;
        		$evd['section']     = $evidence->section;
        		$evd['fileURL']     = '';

        		if($evidence->fileURL)
        		{
        			$evd['fileURL']     = url('/public/uploads/evidence_images').'/'. $evidence->fileURL;
        		}

        		array_push($resultArray, $evd);

        	}

        	if($resultArray)
        	{

        		return response()->json(['code'=>200,'status'=>true,'service_name'=>'get-evidence-list','message'=>'Evidence list.','data'=>$resultArray]);
        	}

        	return response()->json(['code'=>400,'status'=>false,'service_name'=>'get-evidence-list','message'=>'Evidence not found.','data'=>$resultArray]);

        }

        public function updateBelief()
        {

        	$dotInsertArray = array(
        		'name'=> Input::get('name')
        		
          );
        	
        	DB::table('dots_beliefs')
        	->where('id', Input::get('id'))
        	->update($dotInsertArray);

        	return response()->json(['code'=>200,'status'=>true,'service_name'=>'update-belief','message'=>'Belief updated successfully','data'=>$dotInsertArray]); 

        }

        public function deleteBelief()
        {
        	$beliefId = Input::get('id');
        	

        	DB::table('dots_beliefs')
        	->where('id',$beliefId) 
        	->delete();  

        	DB::table('dots_values')
        	->where('beliefId',$beliefId) 
        	->delete();  

        	return response()->json(['code'=>200,'status'=>true,'service_name'=>'delete-belief','message'=>'Belief deleted successfully','data'=>""]); 


        }

      //add bubble rating to the user
        public function addbubbleRatings()
        {
        	$resultArray = array();
        	$user = Auth::user();

        	$toUserIdArray      = Input::get('toUserId'); 
        	$fromUserId         = $user->id;
        	$dotId              = Input::get('dotId');
        	$dotBeliefId        = Input::get('dotBeliefId');
        	$dotValueId         = Input::get('dotValueId');
        	$dotValueNameId     = Input::get('dotValueNameId');
        	$bubbleFlag         = Input::get('bubbleFlag');

        	foreach($toUserIdArray as $key => $ids)
        	{

        		$insertArray = array(
        			'to_bubble_user_id'  => $ids,
        			'from_bubble_user_id'=> $fromUserId,
        			'dot_id'             => $dotId,
        			'dot_belief_id'      => $dotBeliefId,
        			'dot_value_id'       => $dotValueId,
        			'dot_value_name_id'  => $dotValueNameId,
        			'bubble_flag'        => $bubbleFlag,
        			'status'             => 'Active',
        			'created_at'         => date('Y-m-d H:i:s')
             );

        		DB::table('dot_bubble_rating_records')->insertGetId($insertArray);


        		$user = DB::table('users')->select('fcmToken')->where('id', $ids)->where('status','Active')->first();

        		$value = DB::table('dot_value_list')->select('name')->where('id', $dotValueNameId)->first();

        		$dotValueName = '';
        		if($value)
        		{
        			$dotValueName = $value->name;
        		}

        		$fcmToken = '';
        		if($user)
        		{
        			$fcmToken = $user->fcmToken;
        		}

        		$title    = 'Thumbs Up: '.$dotValueName;
        		$message  = 'You have received a thumbs up in '.$dotValueName;
          //get badge count 
        		$totbadge = $this->getIotNotificationBadgeCount(array('userId'=>$ids));

        		$test = app('App\Http\Controllers\API\ApiCommonController')->sendFcmNotify($fcmToken, $title, $message, $totbadge);

        		$notificationArray = array(
        			'to_bubble_user_id'  => $ids,
        			'from_bubble_user_id'=> $fromUserId,
        			"title" => $title,
        			"description" => $message,            
        			'dot_value_id' => $dotValueId,
        			'dot_value_name_id' => $dotValueNameId,
        			'notificationType' => 'thumbsup',
        			"created_at" => date('Y-m-d H:i:s')
             );

        		DB::table('iot_notifications')->insertGetId($notificationArray);

        	}

        	return response()->json(['code'=>200,'status'=>true,'service_name'=>'DOT-add-bubble-ratings','message'=>'Bubble ratings provided successfully.','data'=>$resultArray]);
        	
        }

        /*get bubble rating list*/
        public function getBubbleRatingList(Request $request)
        {

        	$resultArray = array();
        	$beliefArr   = array();

        	$orgId      = $request->orgId;
        	$year       = $request->year;
        	$month      = $request->month;
        	$userId     = $request->userId;

        	$dots = DB::table('dots')->select('id')->where('orgId',$orgId)->first();

        	if(empty($dots->id))
        	{

        		if($request->reportStatus)
        		{
        			return $beliefArr;
        		}

        		return response()->json(['code'=>400,'status'=>false,'service_name'=>'DOT-bubble-ratings-list','message'=>'DOT not available.','data'=>$resultArray]);
        	}

        	$dotId = $dots->id;

        	$beliefsData = DB::table('dots_beliefs')->where('dotId',$dotId)->get();

        	
        	foreach($beliefsData as $bValue)
        	{

        		$belief['id']   = $bValue->id;
        		$belief['name'] = ucfirst($bValue->name);
        		
        		$dotsValue = DB::table('dots_values')->where('beliefId',$bValue->id)->where('status','Active')->get();

        		$dotsValueArr = array();

        		$count = 0;
        		foreach ($dotsValue as $Vvalue)
        		{

        			$dotValueList = DB::table('dot_value_list')->where('id',$Vvalue->name)->first();

        			$upVotTblQuery = DB::table('dot_bubble_rating_records')            
                    ->where('dot_belief_id',$bValue->id)
                    ->where('dot_value_name_id',$Vvalue->name)
                    ->where('bubble_flag',"1");
                    // ->whereYear('created_at', '=', $year)
                    // ->whereMonth('created_at', '=', $month);
                    if($userId)
                    {
                        $upVotTblQuery->where('to_bubble_user_id',$userId);
                    }
                    if($month && $year){
                        $upVotTblQuery->whereYear('created_at', '=', $year)
                                    ->whereMonth('created_at', '=', $month);
                    }
                    $upVotTbl = $upVotTblQuery->get();
        			
        			$dValuesName = '';
        			if($dotValueList)
        			{
        				$dValuesName = $dotValueList->name;
        			}

        			$dotV["index"]     = $count;
        			$dotV["id"]        = $Vvalue->id;
        			$dotV['name']      = $dValuesName;          
        			$dotV['upVotes']   = count($upVotTbl);

        			$valuesRatingsQuery = db::table('dot_values_ratings')
        			->where('valueId', $Vvalue->id);            
        			if($userId) 
        			{
        				$valuesRatingsQuery->where('userId',$userId);
        			}            

        			$valuesRatings = $valuesRatingsQuery->first();

        			$dotV["ratings"]='';

        			if($valuesRatings)
        			{
        				$dotV["ratings"]= $valuesRatings->ratings;
        			}

        			array_push($dotsValueArr, $dotV);

        			$count++;
        		}

        		$belief['beliefValueArr'] = $dotsValueArr;

        		array_push($beliefArr, $belief);
        	}

        	if($request->reportStatus)
        	{
        		return $beliefArr;
        	}


        	return response()->json(['code'=>200,'status'=>true,'service_name'=>'DOT-bubble-ratings-list','message'=>'Bubble ratings.','data'=>$beliefArr]);


        }

        /*get dot from user ratings*/
        public function getBubbleFromUserRating()
        {

        	$resultArray = array();

        	$user = Auth::user();

        	$fromUserId     = $user->id;
        	$toUserId       = Input::get('toUserId');
        	$dotBeliefId    = Input::get('dotBeliefId');
        	$dotValueNameId = Input::get('dotValueNameId');

        	$upVotTbl = DB::table('dot_bubble_rating_records')
        	->where('to_bubble_user_id',$toUserId)
        	->where('from_bubble_user_id',$fromUserId)->where('dot_belief_id',$dotBeliefId)
        	->where('dot_value_name_id',$dotValueNameId)->where('bubble_flag',"0")
        	->get();

        	$downVotTbl = DB::table('dot_bubble_rating_records')
        	->where('to_bubble_user_id',$toUserId)
        	->where('from_bubble_user_id',$fromUserId)->where('dot_belief_id',$dotBeliefId)
        	->where('dot_value_name_id',$dotValueNameId)->where('bubble_flag',"1")
        	->get();

        	$resultArray['upVotes']   = count($upVotTbl);
        	$resultArray['downVotes'] = count($downVotTbl);

        	return response()->json(['code'=>200,'status'=>true,'service_name'=>'DOT-bubble-from-user-ratings','message'=>'DOT bubble from user ratings.','data'=>$resultArray]);

        }

        /*get dot report*/
        public function getDOTreportGraph()
        {

        	$orgId = Input::get('orgId');

        	$dots = DB::table('dots')->where('orgId',$orgId)->first();

        	$dotValuesArray = array();
        	
        	if(!empty($dots))
        	{

        		$dotBeliefs = DB::table('dots_beliefs')
        		->where('status','Active')
        		->where('dotId',$dots->id)        
        		->orderBy('id','ASC')->get();
        		
        		foreach ($dotBeliefs as $key => $bValue)
        		{

        			$dotValues = DB::table('dots_values')
        			->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
        			->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
        			->where('dots_values.status','Active')
        			->where('dots_values.beliefId',$bValue->id)
        			->orderBy('dot_value_list.id','ASC')
        			->get();

        			$bRatings = DB::table('dot_values_ratings')
        			->leftjoin('users','users.id','=','dot_values_ratings.userId')
        			->where('users.status','Active') 
        			->where('beliefId', $bValue->id)->avg('ratings');

        			$valuesArray = array();
        			foreach ($dotValues as $key => $vValue) 
        			{
        				$vRatings = DB::table('dot_values_ratings')
        				->leftjoin('users','users.id','=','dot_values_ratings.userId')
        				->where('users.status','Active')
        				->where('valueId', $vValue->id)->avg('ratings');

        				$vResult['valueId']      = $vValue->id;
        				$vResult['valueName']    = ucfirst($vValue->name);

        				$vResult['valueRatings'] = 0;
        				if($vRatings)
        				{
                $vResult['valueRatings'] = round(($vRatings-1), 2); //number_format("1000000",2)
            }
            array_push($valuesArray, $vResult);
        }

        $result['beliefId']      = $bValue->id;
        $result['beliefName']    = ucfirst($bValue->name);           

        $result['beliefRatings'] = 0;
        if($bRatings)
        {
        	$result['beliefRatings'] = number_format(($bRatings-1), 2);
        }

        $result['beliefValues']  = $valuesArray;

        array_push($dotValuesArray, $result);
    }
}

return response()->json(['code'=>200,'status'=>true,'service_name'=>'DOT-report','message'=>'','data'=>$dotValuesArray]);
}



/*get dot valuse list*/
public function getDOTValuesList()
{

	$resultArray = array();

	$user = Auth::user();

	$orgId = Input::get('orgId');

	$dot_data = DB::table('dots')->where('orgId',$orgId)->latest()->first();

	$result = array();

	if(!empty($dot_data))
	{  

         //get beliefs data 
		$beliefs_data = DB::table('dots_beliefs')->where('dotId',$dot_data->id)->get();

		$belief_arr = array();

		foreach($beliefs_data as $value)
		{

			$belief['id'] = $value->id;
			$belief['name'] = $value->name;
			$belief['dotId'] = $value->dotId;

			$dots_value = DB::table('dots_values')->where('beliefId',$value->id)->where('status','Active')->get();

			$dotsValueArr = array();

			$count = 0;
			foreach($dots_value as $Vvalue)
			{

				$dotValueList = DB::table('dot_value_list')->where('id',$Vvalue->name)->first();

				$dValuesName = '';
				if($dotValueList)
				{
					$dValuesName = $dotValueList->name;
				}

				$dotV["index"]     = $count;
				$dotV["id"]        = $Vvalue->id;
				$dotV['name']      = $dValuesName;
				$dotV["valueId"]   = $Vvalue->name;
				$dotV["beliefId"]  = $Vvalue->beliefId;
				$dotV["status"]    = $Vvalue->status;
				$dotV["created_at"]= $Vvalue->created_at;
				$dotV["updated_at"]= (string)$Vvalue->updated_at;
				$dotV['dotId']     = $value->dotId;

				$valuesRatings = db::table('dot_values_ratings')->where('valueId', $Vvalue->id)->where('userId',$user->id)->first();

				$dotV["ratings"]='';

				if($valuesRatings){
					$dotV["ratings"]= $valuesRatings->ratings;
				}

				array_push($dotsValueArr, $dotV);

				$count++;
			}

			$belief['belief_value'] = $dotsValueArr;

			array_push($belief_arr, $belief);
		}

		$resultArray['belief'] = $belief_arr;

		return response()->json(['code'=>200,'status'=>true,'service_name'=>'dot-value-list','message'=>'DOT value list','data'=>$resultArray]);

	} 
	else
	{
		return response()->json(['code'=>400,'status'=>false,'service_name'=>'dot-value-list','message'=>'DOT values not found','data'=>$resultArray]);
	}

}


/*DOT bubble report by multiple department and values*/
public function addDOTBubbleRatingsToMultiDepartment()
{


	$resultArray = array();
	$user  = Auth::user();
	$orgId = $user->orgId;

	$toUserIdArray = array();

	$toUserIdArray      = Input::get('toUserId'); 
	$fromUserId         = $user->id;
	$dotId              = Input::get('dotId');
	$bubbleFlag         = Input::get('bubbleFlag');

	$departmentIdArray  = Input::get('departmentIdArray');

	$options = Input::get('options');
	
	foreach ($departmentIdArray as $deptId)
	{

		$users = DB::table('users')->select('id')                 
		->where('departmentId', $deptId)
		->where('orgId', $orgId)->where('status', 'Active')->get();

		foreach($users as $uValue)
		{
			if($user->id != $uValue->id)
			{
				array_push($toUserIdArray, $uValue->id);
			}            
		}
	}


	foreach($toUserIdArray as $key => $ids)
	{

		foreach ($options as $oValue)
		{

			$insertArray = array(
				'to_bubble_user_id'  => $ids,
				'from_bubble_user_id'=> $fromUserId,
				'dot_id'             => $dotId,
				'dot_belief_id'      => $oValue['dotBeliefId'],
				'dot_value_id'       => $oValue['dotValueId'],
				'dot_value_name_id'  => $oValue['dotValueNameId'],
				'bubble_flag'        => $bubbleFlag,
				'status'             => 'Active',
				'created_at'         => date('Y-m-d H:i:s')
            );

			DB::table('dot_bubble_rating_records')->insertGetId($insertArray);


			$user = DB::table('users')->select('fcmToken')->where('id', $ids)->where('status','Active')->first();

			$value = DB::table('dot_value_list')->select('name')->where('id', $oValue['dotValueNameId'])->first();

			$dotValueName = '';
			if($value)
			{
				$dotValueName = $value->name;
			}

			$fcmToken = '';
			if($user)
			{
				$fcmToken = $user->fcmToken;
			}

			$title    = 'Thumbs Up: '.$dotValueName;
			$message  = 'You have received a thumbs up in '.$dotValueName;
            // $totbadge = 1;

			$totbadge = $this->getIotNotificationBadgeCount(array('userId'=>$ids));
			
			$test = app('App\Http\Controllers\API\ApiCommonController')->sendFcmNotify($fcmToken, $title, $message, $totbadge);


			$notificationArray = array(
				'to_bubble_user_id'  => $ids,
				'from_bubble_user_id'=> $fromUserId,
				"title" => $title,
				"description" => $message,            
				'dot_value_id' => $oValue['dotValueId'],
				'dot_value_name_id' => $oValue['dotValueNameId'],
				'notificationType' => 'thumbsup',
				"created_at" => date('Y-m-d H:i:s')
            );

			DB::table('iot_notifications')->insertGetId($notificationArray);

		}

	}

	return response()->json(['code'=>200,'status'=>true,'service_name'=>'DOT-add-bubble-ratings','message'=>'Bubble ratings provided successfully.','data'=>$resultArray]);

}

/*get DOT bubble rating notification list*/
public function getBubbleRatingNotificationList()
{

	$resultArray = array();

	$userId         = Input::get('userId');
	$page           = Input::get('page');

	$recordLimit    = 10;
	$pageCount      = $page - 1; 
	$offset         = $recordLimit * $pageCount;
	$totalPageCount = 0;

	$wrclaue = array();
	
	$wrclaue['status']             = 'Active';
	$wrclaue['to_bubble_user_id']  = $userId;
	$wrclaue['isMsgRead']          = "1";
	
	$pageCount = app('App\Http\Controllers\API\ApiCommonController')->getPageCountQuery('iot_notifications', $wrclaue);   

	$totalPageCount = app('App\Http\Controllers\API\ApiCommonController')->getPageCount($pageCount,$recordLimit);

	if($page)
	{
		$bubbleNotifTbl = DB::table('iot_notifications')
		->where($wrclaue)
		->offset($offset)
		->limit($recordLimit)
		->orderBy('id','DESC')
		->get();
	}
	else
	{
		$bubbleNotifTbl = DB::table('iot_notifications')
		->where($wrclaue)        
		->orderBy('id','DESC')
		->get();
	}
	

	foreach($bubbleNotifTbl as $value)
	{
		$result["id"]               = $value->id;
		$result["title"]            = (string)$value->title;
		$result["description"]      = (string)$value->description;  
		$result["feedbackId"]       = (string)$value->feedbackId; 
		$result["notificationType"] = (string)$value->notificationType;      
		$result["created_at"]       = (string)$value->created_at;
		$result['lastMessage']      = '';
		$result['file']             = false;

		if($value->isMsgRead==0)
		{
			$result['isRead'] = false;
		}
		else
		{
			$result['isRead'] = true;
		}

		if($value->feedbackId)
		{
			$feedback = DB::table('iot_feedbacks')->select('message')->where('id',$value->feedbackId)->first();

			if($feedback)
			{
				$result["title"]  = $feedback->message;
			}
		}

		array_push($resultArray, $result);
	}

        // DB::table('iot_notifications')->where('to_bubble_user_id', $userId)->update(['isMsgRead'=>'1']);


	return response()->json(['code'=>200, 'status'=>true,'service_name'=>'DOT-bubble-ratings-notification','message'=>'','totalPageCount'=>$totalPageCount,'currentPage'=>$page,'data'=>$resultArray]);

}

/*get DOT bubble rating un read notification list*/
public function getBubbleRatingUnReadNotificationList()
{

	$resultArray = array();

	$userId         = Input::get('userId');

	$bubbleNotifTbl = DB::table('iot_notifications')
	->where('status', 'Active')
	->where('to_bubble_user_id', $userId)
	->where('isMsgRead',"0")
	->orderBy('id','DESC')
	->get();

	foreach($bubbleNotifTbl as $value)
	{
		$result["id"]               = $value->id;
		$result["title"]            = (string)$value->title;
		$result["description"]      = (string)$value->description;  
		$result["feedbackId"]       = (string)$value->feedbackId; 
		$result["notificationType"] = (string)$value->notificationType;      
		$result["created_at"]       = (string)$value->created_at;
		$result['lastMessage']      = '';
		$result['file']             = false;

		if($value->isMsgRead==0)
		{
			$result['isRead'] = false;
		}
		else
		{
			$result['isRead'] = true;
		}

		if($value->feedbackId)
		{
			$feedback = DB::table('iot_feedbacks')->select('message')->where('id',$value->feedbackId)->first();

			if($feedback)
			{
				$result["title"]  = $feedback->message;
			}
		}

		array_push($resultArray, $result);
	}

	$ansStatus   = array(); 
	$toDoList    = array();

	$month  = date('m');
	$year   = date('Y');
	$day    = date('d');

	$statusKey = app('App\Http\Controllers\API\ApiChecklistController')->getVar('dot_values_ratings');

	$ansStatus[$statusKey]= app('App\Http\Controllers\API\ApiChecklistController')->isDotValuecompleted($userId);

	$dataTableArr = array('dot_values_ratings','cot_functional_lens_answers','cot_answers','sot_answers','sot_motivation_answers','tribeometer_answers','diagnostic_answers');

	foreach($dataTableArr as $value)
	{

		$tableStatus = DB::table($value)->where('userId',$userId)->first(); 

		if($value !='dot_values_ratings')
		{
			$statusKey = app('App\Http\Controllers\API\ApiChecklistController')->getVar($value);

			if(!empty($tableStatus))
			{     
				$ansStatus[$statusKey]= true;
			}
			else
			{
				$ansStatus[$statusKey]= false;
			}
		}

          //Get month wise status
		$queryDot = DB::table($value)->where('userId',$userId); 
		$queryDot->whereYear('updated_at','=', $year);
		$queryDot->whereMonth('updated_at','=', $month);
		$dotvaluesMonthsWise = $queryDot->first();

		if (empty($dotvaluesMonthsWise))
		{
			$queryDot = DB::table($value)->where('userId',$userId); 
			$queryDot->whereYear('created_at','=', $year);
			$queryDot->whereMonth('created_at','=', $month);
			$dotvaluesMonthsWise = $queryDot->first();
		}   

		if(!empty($dotvaluesMonthsWise))
		{       
			$toDoList[$statusKey] = true;
		}
		else
		{
			$toDoList[$statusKey] = false;
		} 
	}    



       //tribe values
	$querybubble = DB::table('dot_bubble_rating_records')->where('from_bubble_user_id',$userId); 
	$querybubble->whereDate('updated_at','=', date('Y-m-d'));   
	$bubbleRatings = $querybubble->first();

	if (empty($bubbleRatings))
	{
		$querybubble = DB::table('dot_bubble_rating_records')->where('from_bubble_user_id',$userId);
		$querybubble->whereDate('created_at','=', date('Y-m-d'));
		$bubbleRatings = $querybubble->first();
	} 

	if(!empty($bubbleRatings))
	{
		$toDoList['bubbleRatings'] = true;
	}else{
		$toDoList['bubbleRatings'] = false;
	} 

	return response()->json(['code'=>200, 'status'=>true, 'service_name'=>'DOT-bubble-ratings-unread-notification', 'message'=>'', 'data'=>$resultArray, 'ansStatus'=>$ansStatus, 'toDoList'=>$toDoList]);

}

/*get un read notification count*/
public function getBubbleUnReadNotifications()
{

	$resultArray = array();

	$userId = Input::get('userId');
        //orWhere clause not woring here
	$bubbleNotifTblthumbsup = DB::table('iot_notifications')
	->where('isMsgRead','0')
	->where('status','Active')       
	->where('notificationType','thumbsup')
	->where('to_bubble_user_id',$userId)
	->count();

	$bubbleNotifTblchat = DB::table('iot_notifications')
	->where('isMsgRead','0')
	->where('status','Active')
	->Where('notificationType','chat') 
	->where('to_bubble_user_id',$userId)
	->count();

	

    $ansStatus   = array(); 
    $toDoList    = array();

    $month  = date('m');
    $year   = date('Y');
    $day    = date('d');

    $ansStatusCount = 0;
    $toDoListcount = 0;

    $statusKey = app('App\Http\Controllers\API\ApiChecklistController')->getVar('dot_values_ratings');

    $ansStatus[$statusKey]= app('App\Http\Controllers\API\ApiChecklistController')->isDotValuecompleted($userId);
    if (empty($ansStatus[$statusKey])) {
        $ansStatusCount += 1;
    }

    $dataTableArr = array('dot_values_ratings','cot_functional_lens_answers','cot_answers','sot_answers','sot_motivation_answers','tribeometer_answers','diagnostic_answers');

    foreach($dataTableArr as $value)
    {

        $tableStatus = DB::table($value)->where('userId',$userId)->first(); 

        if($value !='dot_values_ratings')
        {
            $statusKey = app('App\Http\Controllers\API\ApiChecklistController')->getVar($value);

            if(!empty($tableStatus))
            {     
                $ansStatus[$statusKey]= true;
            }
            else
            {
                $ansStatus[$statusKey]= false;
                    $ansStatusCount += 1;
            }
        }

          //Get month wise status
        $queryDot = DB::table($value)->where('userId',$userId); 
        $queryDot->whereYear('updated_at','=', $year);
        $queryDot->whereMonth('updated_at','=', $month);
        $dotvaluesMonthsWise = $queryDot->first();

        if (empty($dotvaluesMonthsWise))
        {
            $queryDot = DB::table($value)->where('userId',$userId); 
            $queryDot->whereYear('created_at','=', $year);
            $queryDot->whereMonth('created_at','=', $month);
            $dotvaluesMonthsWise = $queryDot->first();
        }   

        if(!empty($dotvaluesMonthsWise))
        {       
            $toDoList[$statusKey] = true;
        }
        else
        {
            $toDoList[$statusKey] = false;
            if ($ansStatusCount<1) {
                $toDoListcount += 1;
            }
        }
    }   
    //tribe values
    $querybubble = DB::table('dot_bubble_rating_records')->where('from_bubble_user_id',$userId); 
    $querybubble->whereDate('updated_at','=', date('Y-m-d'));   
    $bubbleRatings = $querybubble->first();

    if (empty($bubbleRatings))
    {
        $querybubble = DB::table('dot_bubble_rating_records')->where('from_bubble_user_id',$userId);
        $querybubble->whereDate('created_at','=', date('Y-m-d'));
        $bubbleRatings = $querybubble->first();
    } 

    if(!empty($bubbleRatings))
    {
        $toDoList['bubbleRatings'] = true;
    }else{
        $toDoList['bubbleRatings'] = false;
        if ($ansStatusCount<1) {
            $toDoListcount += 1;
        }
    }   
    
    if ($ansStatusCount>0) {
        $toDoListcount = 0;
    }
    $resultArray['notificationCount'] = ($bubbleNotifTblthumbsup + $bubbleNotifTblchat + $ansStatusCount + $toDoListcount);

	return response()->json(['code'=>200, 'status'=>true,'service_name'=>'DOT-bubble-ratings-notification','message'=>'','data'=>$resultArray,'ansStatus'=>$ansStatus,'toDoList'=>$toDoList]);

}


/*get badge notiication count*/
public function getIotNotificationBadgeCount($arr)
{

	$userId = $arr['userId'];

	$resultArray = array();
	$ansStatus   = array(); 
	$toDoList    = array();

	$month  = date('m');
	$year   = date('Y');
	$day    = date('d');

	$statusKey = app('App\Http\Controllers\API\ApiChecklistController')->getVar('dot_values_ratings');

	$ansStatus[$statusKey]= app('App\Http\Controllers\API\ApiChecklistController')->isDotValuecompleted($userId);

	$dataTableArr = array('dot_values_ratings','cot_functional_lens_answers','cot_answers','sot_answers','sot_motivation_answers','tribeometer_answers','diagnostic_answers');

	foreach($dataTableArr as $value)
	{

		$tableStatus = DB::table($value)->where('userId',$userId)->first(); 

		if($value !='dot_values_ratings')
		{
			$statusKey = app('App\Http\Controllers\API\ApiChecklistController')->getVar($value);

			if(!empty($tableStatus))
			{     
				$ansStatus[$statusKey]= true;
			}
			else
			{
				$ansStatus[$statusKey]= false;
			}
		}

          //Get month wise status
		$queryDot = DB::table($value)->where('userId',$userId); 
		$queryDot->whereYear('updated_at','=', $year);
		$queryDot->whereMonth('updated_at','=', $month);
		$dotvaluesMonthsWise = $queryDot->first();

		if (empty($dotvaluesMonthsWise))
		{
			$queryDot = DB::table($value)->where('userId',$userId); 
			$queryDot->whereYear('created_at','=', $year);
			$queryDot->whereMonth('created_at','=', $month);
			$dotvaluesMonthsWise = $queryDot->first();
		}   

		if(!empty($dotvaluesMonthsWise))
		{       
			$toDoList[$statusKey] = true;
		}
		else
		{
			$toDoList[$statusKey] = false;
		} 
	}    



       //tribe values
	$querybubble = DB::table('dot_bubble_rating_records')->where('from_bubble_user_id',$userId); 
	$querybubble->whereDate('updated_at','=', date('Y-m-d'));   
	$bubbleRatings = $querybubble->first();

	if (empty($bubbleRatings))
	{
		$querybubble = DB::table('dot_bubble_rating_records')->where('from_bubble_user_id',$userId);
		$querybubble->whereDate('created_at','=', date('Y-m-d'));
		$bubbleRatings = $querybubble->first();
	} 

	if(!empty($bubbleRatings))
	{
		$toDoList['bubbleRatings'] = true;
	}else{
		$toDoList['bubbleRatings'] = false;
	}

        //check false count of users answers status
	$counter = 0;
	foreach ($ansStatus as $key=> $value)
	{
		if($value===false)
		{
			$counter++;
		}          
	} 

       //if all the answers are filled then check in to do lists pending status
	if($counter==0)
	{
		foreach ($toDoList as $key => $value)
		{
			if($value===false)
			{
				$counter++;
			}
		}
	}

        //unread count of notification
	$bubbleNotifTbl = DB::table('iot_notifications')->where('isMsgRead','0')->where('status','Active')->where('notificationType','thumbsup')->orWhere('notificationType','chat')->where('to_bubble_user_id',$userId)->count();

	$totalNotification = (count($bubbleNotifTbl)+$counter+1);

	return $totalNotification;
}
/*change status of the unread notification*/
public function changeNotificationStatus()
{
	$resultArray    = array();
	$notificationId = Input::get('notificationId');
	$updatetArray['isMsgRead'] = "1";
	DB::table('iot_notifications')->where('id',$notificationId)->update($updatetArray);
	return response()->json(['code'=>200, 'status'=>true,'service_name'=>'DOT-bubble-ratings-notification-status-change','message'=>'','data'=>$resultArray]);
}



/*change status of the unread notification*/
public function readAllNotification()
{
    $resultArray   = array();
    $userId        = Input::get('userId');
    $updatetArray['isMsgRead'] = "1";
    DB::table('iot_notifications')->where('to_bubble_user_id',$userId)->update($updatetArray);
    return response()->json(['code'=>200, 'status'=>true,'service_name'=>'DOT-bubble-ratings-notification-status-change','message'=>'','data'=>$resultArray]);
}

/*add happy index detail*/
public function addHappyIndex()
{
	$resultArray = array();

	$userId    = Input::get('userId');
	$moodValue = Input::get('moodStatus');

	$happyIndexInsertArray = array(
		'userId'=> $userId,
		'moodValue'=> $moodValue,
		'created_at'=> date('Y-m-d H:i:s'),
  );

	$happyIndexId = DB::table('happy_indexes')->insertGetId($happyIndexInsertArray);

	return response()->json(['code'=>200, 'status'=>true,'service_name'=>'dashboard-add-happy-index','message'=>'','data'=>$resultArray]);
}


/*get dashboard detail*/
public function getDashboardDetail(){

    $resultArray = array();
    $orgId      = Input::get('orgId');
    $dot        = DB::table('dots')->where('orgId',$orgId)->first();


    //Get info of org
    $org = DB::table('organisations')
            ->select('include_weekend')
            ->where('id',$orgId)->first();
    //Get weekend flag
    $excludeWeekend = $org->include_weekend;   //1 for include weekend else not


    $vision = '';
    if($dot){
        $vision = $dot->vision;
    }

    $resultArray['vision'] = $vision;

    //get tribeometer detail
    $queCatTbl = DB::table('tribeometer_questions_category')->get();

    $users = DB::table('tribeometer_answers')
    ->leftjoin('users','users.id','tribeometer_answers.userId')
    ->select('tribeometer_answers.userId')
    ->where('users.status','Active')
    ->groupBy('tribeometer_answers.userId')
    ->where('users.orgId',$orgId)->get();

    $userCount = count($users);

    $optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
    $optCount = count($optionsTbl)-1;

    $tribeoResultArray = array();
    foreach ($queCatTbl as $value){
        $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();

        $quecount = count($questionTbl);
        $perQuePercen = 0;
        foreach ($questionTbl as  $queValue){
            $diaAnsTbl = DB::table('tribeometer_answers')     
            ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
            ->leftJoin('users','users.id','tribeometer_answers.userId')
            ->where('users.status','Active')
            ->where('tribeometer_answers.orgId',$orgId)
            ->where('tribeometer_questions.id',$queValue->id)
            ->where('tribeometer_questions.category_id',$value->id)
            ->sum('answer');

            //avg of all questions
            if ($userCount !=0){
                $perQuePercen += ($diaAnsTbl/$userCount); 
            }            
        }

        $score = ($perQuePercen/($quecount*$optCount));
        $totalPercentage = ($perQuePercen/($quecount*$optCount))*100;

        $value1['title']      =  $value->title;       
        $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');      

        array_push($tribeoResultArray, $value1);
    }

    $resultArray['tribeometerDetail'] = $tribeoResultArray;


    $user = Auth::user();
    $userId = $user->id;
    $isUser = DB::table('happy_indexes')->where('userId',$userId)->where('status','Active')->whereDate('created_at', date('Y-m-d'))->first();
    $weekDay = date('w', strtotime(date('Y-m-d')));

    $resultArray['userGivenfeedback'] = false;
    //  if($isUser) 
    //  {
    //     $resultArray['userGivenfeedback'] = true;
    // }
    if($isUser || $weekDay == 0 || $weekDay == 6) {
        $resultArray['userGivenfeedback'] = true;
    }


    /*get previous day happy index detail*/
    $userTypeArr = DB::table('happy_index_mood_values')->where('id',3)->where('status','Active')->get();
    $totalIndexes = 0;
    $totalUser    = 0;
    $happyIndexcount = 0;

    if ((date('D') == 'Mon') || (date('D') == 'Sun')) {
      $date = date('Y-m-d',strtotime('last Friday'));
    }else{
      $date = date('Y-m-d',strtotime(Carbon::yesterday()));
    }

    foreach($userTypeArr as $moodVal){
        $happyIndexDataQuery = DB::table('happy_index_dashboard_graph')
          ->where('status','Active')
          ->where('categoryId',$moodVal->id)
          ->whereDate('date',$date);
        if(!empty($orgId)) {
            $happyIndexDataQuery->where('orgId',$orgId);
        }
        if(empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->where('departmentId',$departmentId);
        } elseif(empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->where('departmentId',$departmentId);
        }
        if ($excludeWeekend != 1) {
          //Get weekends
          $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($date,1);
          $happyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
        } 
        $happyIndexData = $happyIndexDataQuery->first();  
        if (!empty($happyIndexData)) {
          if($excludeWeekend != 1){ //Exclude Weekend Days
            $happyIndexcount = $happyIndexData->without_weekend;
          }else{
              $happyIndexcount = $happyIndexData->with_weekend;
          }
        }else{
            $happyIndexcount = 0;
        }
        // $userCount = DB::table('happy_indexes')
        // ->leftjoin('users','users.id','happy_indexes.userId')
        // ->where('happy_indexes.status','Active')
        // ->where('users.status','Active')
        // ->where('happy_indexes.moodValue',$type)
        // ->where('users.orgId',$orgId)        
        // ->whereDate('happy_indexes.created_at', date('Y-m-d', strtotime('-1 days')))
        // ->count();
        // $totalUser +=$userCount;
        // if($type==3){
        //     $totalIndexes += $userCount;
        // }       
    }


    // $happyIndexPer = 0;
    // if($totalIndexes !=0 && $totalUser !=0){ 
    //   $happyIndexPer = (($totalIndexes/($totalUser))*100);
    // }

    $resultArray['happyIndex'] = round($happyIndexcount,2);

    //get yesterday's previous day happy index
    $totalIndexesOfyesterdaysPreviousday = 0;
    $totalUserOfyesterdaysPreviousday    = 0;
    $totalHappyUserYesterday = 0;
    $totalIndexesOfyesterday = 0;
    foreach($userTypeArr as $moodVal){
        //Yesterday user percent
        $yesterdayHappyIndexDataQuery = DB::table('happy_index_dashboard_graph')
          ->where('status','Active')
          ->where('categoryId',$moodVal->id);
        if ((date('D') == 'Mon') || (date('D') == 'Sun')) {
            $yesterdayHappyIndexDataQuery->whereDate('date',date('Y-m-d', strtotime('last Friday')));
        }else{
            $yesterdayHappyIndexDataQuery->whereDate('date',date('Y-m-d', strtotime('-1 days')));
        }  
        if(!empty($orgId)) {
            $yesterdayHappyIndexDataQuery->where('orgId',$orgId);
        }
        if(empty($officeId) && empty($departmentId)) {
            $yesterdayHappyIndexDataQuery->whereNull('officeId');
            $yesterdayHappyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && empty($departmentId)) {
            $yesterdayHappyIndexDataQuery->where('officeId',$officeId);
            $yesterdayHappyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && !empty($departmentId)) {
            $yesterdayHappyIndexDataQuery->where('officeId',$officeId);
            $yesterdayHappyIndexDataQuery->where('departmentId',$departmentId);
        } elseif(empty($officeId) && !empty($departmentId)) {
            $yesterdayHappyIndexDataQuery->whereNull('officeId');
            $yesterdayHappyIndexDataQuery->where('departmentId',$departmentId);
        }
        // if ($excludeWeekend != 1) {
        //     //Get weekends
        //     $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth(date('Y-m-d', strtotime('-1 days')),1);
        //     $yesterdayHappyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
        // } 
        $yesterdayHappyIndexData = $yesterdayHappyIndexDataQuery->first();  
        if (!empty($yesterdayHappyIndexData)) {
            if($excludeWeekend != 1){ //Exclude Weekend Days
                $yesterdayHappyIndexcount = $yesterdayHappyIndexData->without_weekend;
            }else{
                $yesterdayHappyIndexcount = $yesterdayHappyIndexData->with_weekend;
            }
        }else{
            $yesterdayHappyIndexcount = 0;
        }

        //Previous Yesterday user percent
        $previousyesHappyIndexDataQuery = DB::table('happy_index_dashboard_graph')
            ->where('status','Active')
            ->where('categoryId',$moodVal->id);
        if ((date('D') == 'Mon') || (date('D') == 'Tue')) {
            $previousyesHappyIndexDataQuery->whereDate('date',date('Y-m-d', strtotime('last Friday')));
        }else{
            $previousyesHappyIndexDataQuery->whereDate('date',date('Y-m-d', strtotime('-2 days')));
        }
        if(!empty($orgId)) {
            $previousyesHappyIndexDataQuery->where('orgId',$orgId);
        }
        if(empty($officeId) && empty($departmentId)) {
            $previousyesHappyIndexDataQuery->whereNull('officeId');
            $previousyesHappyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && empty($departmentId)) {
            $previousyesHappyIndexDataQuery->where('officeId',$officeId);
            $previousyesHappyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && !empty($departmentId)) {
            $previousyesHappyIndexDataQuery->where('officeId',$officeId);
            $previousyesHappyIndexDataQuery->where('departmentId',$departmentId);
        } elseif(empty($officeId) && !empty($departmentId)) {
            $previousyesHappyIndexDataQuery->whereNull('officeId');
            $previousyesHappyIndexDataQuery->where('departmentId',$departmentId);
        }
        // if ($excludeWeekend != 1) {
        //     //Get weekends
        //     $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth(date('Y-m-d', strtotime('-2 days')),1);
        //     $previousyesHappyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
        // } 
        $previousyesHappyIndexData = $previousyesHappyIndexDataQuery->first();  
        if (!empty($previousyesHappyIndexData)) {
            if($excludeWeekend != 1){ //Exclude Weekend Days
                $previousyesHappyIndexcount = $previousyesHappyIndexData->without_weekend;
            }else{
                $previousyesHappyIndexcount = $previousyesHappyIndexData->with_weekend;
            }
        }else{
            $previousyesHappyIndexcount = 0;
        }

        //Happy index DD
        $happyIndexDD = 0;
        if($previousyesHappyIndexcount !=0 && $yesterdayHappyIndexcount !=0) {
            $happyIndexDD = ((-1*($previousyesHappyIndexcount - $yesterdayHappyIndexcount))/($previousyesHappyIndexcount));
        }
        $happyIndex['DD'] =round($happyIndexDD,2);

        //Start date of happy index
        $happyIndexDashboardStartQuery = DB::table('happy_index_dashboard_graph')
            ->where('status','Active');
        if(!empty($orgId)) {
            $happyIndexDashboardStartQuery->where('orgId',$orgId);
        }
        if(empty($officeId) && empty($departmentId)) {
            $happyIndexDashboardStartQuery->whereNull('officeId');
            $happyIndexDashboardStartQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && empty($departmentId)) {
            $happyIndexDashboardStartQuery->where('officeId',$officeId);
            $happyIndexDashboardStartQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && !empty($departmentId)) {
            $happyIndexDashboardStartQuery->where('officeId',$officeId);
            $happyIndexDashboardStartQuery->where('departmentId',$departmentId);
        } elseif(empty($officeId) && !empty($departmentId)) {
            $happyIndexDashboardStartQuery->whereNull('officeId');
            $happyIndexDashboardStartQuery->where('departmentId',$departmentId);
        }
        $happyIndexDashboardStart = $happyIndexDashboardStartQuery->orderBy('id','ASC')->first();

        if (!empty($happyIndexDashboardStart)) {
            $startDate = $happyIndexDashboardStart->date;
        }else{
            $startDate = date('Y-m-d',strtotime('-1 days'));
        }
        $currentDate = date('Y-m-d',strtotime('-1 days'));

        //Get weekends
        $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($startDate, $currentDate);

        //All previous days percent
        $allPreviousDayHappyIndexDataQuery = DB::table('happy_index_dashboard_graph')
            ->where('status','Active')
            ->where('categoryId',$moodVal->id)
            ->whereDate('date','<',date('Y-m-d'))
            ->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
        if(!empty($orgId)) {
            $allPreviousDayHappyIndexDataQuery->where('orgId',$orgId);
        }
        if(empty($officeId) && empty($departmentId)) {
            $allPreviousDayHappyIndexDataQuery->whereNull('officeId');
            $allPreviousDayHappyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && empty($departmentId)) {
            $allPreviousDayHappyIndexDataQuery->where('officeId',$officeId);
            $allPreviousDayHappyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && !empty($departmentId)) {
            $allPreviousDayHappyIndexDataQuery->where('officeId',$officeId);
            $allPreviousDayHappyIndexDataQuery->where('departmentId',$departmentId);
        } elseif(empty($officeId) && !empty($departmentId)) {
            $allPreviousDayHappyIndexDataQuery->whereNull('officeId');
            $allPreviousDayHappyIndexDataQuery->where('departmentId',$departmentId);
        }
        // if ($excludeWeekend != 1) {
        //     // $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
        //     // $currentDate    = date('Y-m-d'); 
        //     //Get weekends
        //     $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($startDate, $currentDate);
        //     $allPreviousDayHappyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
        // }
        if($excludeWeekend != 1){ //Exclude Weekend Days
            $allPreviousDayHappyIndexData = $allPreviousDayHappyIndexDataQuery->sum('without_weekend');  
        }else{
            $allPreviousDayHappyIndexData = $allPreviousDayHappyIndexDataQuery->sum('with_weekend');  
        }

        //No. of weekdays
        $start = new DateTime($startDate);
        $end = new DateTime($currentDate);
        // otherwise the  end date is excluded (bug?)
        $end->modify('+1 day');
        $interval = $end->diff($start);
        // total days
        $days = $interval->days;
        // create an iterateable period of date (P1D equates to 1 day)
        $period = new DatePeriod($start, new DateInterval('P1D'), $end);
        // best stored as array, so you can add more than one
        $holidays = [];
        foreach($period as $dt) {
            $curr = $dt->format('D');
            // substract if Saturday or Sunday
            if ($curr == 'Sat' || $curr == 'Sun') {
                $days--;
            }
            // (optional) for the updated question
            elseif (in_array($dt->format('Y-m-d'), $holidays)) {
                $days--;
            }
        }
        $weekdaysCount  = $days;

        //Calculate MA for happy index
        $movingAvgIndex = 0;
        if($weekdaysCount !=0 && $allPreviousDayHappyIndexData !=0){
            $movingAvgIndex = ($allPreviousDayHappyIndexData/$weekdaysCount);  
        }

        $maValue = 0;
        if($movingAvgIndex !=0){
            //-1(MA – yesterday's H.I. value)/ MA
            $maValue = ((-1*($movingAvgIndex - $yesterdayHappyIndexcount))/($movingAvgIndex));  
            // $maValue = ((-1*($movingAvgIndex - $happyIndexPerOfyesterday))/($movingAvgIndex))*100;  
            //$maValue = -1*($movingAvgIndex - $happyIndexPer)/$movingAvgIndex;  
        }
        $happyIndex['MA'] = round($maValue,2);
    

        // $userCount = DB::table('happy_indexes')
        // ->leftjoin('users','users.id','happy_indexes.userId')
        // ->where('happy_indexes.status','Active')
        // ->where('users.status','Active')
        // ->where('happy_indexes.moodValue',$type)
        // ->where('users.orgId',$orgId)        
        // ->whereDate('happy_indexes.created_at', date('Y-m-d', strtotime('-2 days')))
        // ->count();

        // $totalUserOfyesterdaysPreviousday +=$userCount;

        // $yesterdayUserCount = DB::table('happy_indexes')
        // ->leftjoin('users','users.id','happy_indexes.userId')
        // ->where('happy_indexes.status','Active')
        // ->where('users.status','Active')
        // ->where('happy_indexes.moodValue',$type)
        // ->where('users.orgId',$orgId)        
        // ->whereDate('happy_indexes.created_at', date('Y-m-d', strtotime('-1 days')))
        // ->count();

        // $totalHappyUserYesterday +=$yesterdayUserCount;

        // if($type==3){
        //     $totalIndexesOfyesterdaysPreviousday += $userCount;            
        //     $totalIndexesOfyesterday += $yesterdayUserCount;
        // }       
    }
    $resultArray['happyIndexDD'] =$happyIndex['DD'];
    $resultArray['happyIndexMA'] = $happyIndex['MA'];

    // $happyIndexPerOfyesterdaysPreviousday = 0;
    // if($totalIndexesOfyesterdaysPreviousday !=0 && $totalUserOfyesterdaysPreviousday !=0){
    //     $happyIndexPerOfyesterdaysPreviousday = ($totalIndexesOfyesterdaysPreviousday/($totalUserOfyesterdaysPreviousday));
    // }

    // $happyIndexPerOfyesterday = 0;
    // if($totalIndexesOfyesterday !=0 && $totalHappyUserYesterday !=0){
    //     $happyIndexPerOfyesterday = ($totalIndexesOfyesterday/($totalHappyUserYesterday));       
    // }

    // //((-1*(day before yesterday H.I. value – yesterday's H.I. value)) /( day before yesterday H.I. value))*100

    // $happyIndexDD = 0;
    // if($happyIndexPerOfyesterdaysPreviousday !=0){
    //     $happyIndexDD = ((-1*($happyIndexPerOfyesterdaysPreviousday - $happyIndexPerOfyesterday))/($happyIndexPerOfyesterdaysPreviousday))*100;
    // }

    // $resultArray['happyIndexDD'] =round($happyIndexDD,2);

    //   //M.A.(First calculate Moving Average:) = (Total number of happy faces from all previous days/number of days)
    //   //=-1(M.A – todays yesterday H.I. value) / M.A

    // $happyFacesCount = DB::table('happy_indexes')
    // ->leftjoin('users','users.id','happy_indexes.userId')
    // ->where('happy_indexes.status','Active')
    // ->where('users.status','Active')
    // ->where('happy_indexes.moodValue','3')
    // ->where('users.orgId',$orgId)        
    // ->whereDate('happy_indexes.created_at', '<', date('Y-m-d'))
    // //->whereDate('happy_indexes.created_at','<=', date('Y-m-d', strtotime('-1 days')))
    // ->count();

    // //No of days from first staff created of organisation till yesterday
    // $firstStaffOfOrg = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    // if (!empty($firstStaffOfOrg)) {
    //     $firstStaffOfOrgDate = strtotime($firstStaffOfOrg->created_at);
    //     $yesterdayDate = strtotime(Carbon::today());
        
    //     if ($firstStaffOfOrgDate < $yesterdayDate) {
    //         $firstStaffOfOrgDate = date_create(date('Y-m-d',strtotime($firstStaffOfOrg->created_at)));
    //         $yesterdayDate = date_create(Carbon::yesterday());
            
    //         $diff = date_diff($firstStaffOfOrgDate,$yesterdayDate);
    //         $noOfDays = $diff->format("%a")+1;
    //     }else{
    //         $noOfDays = 0;
    //     }
    // }else{
    //     $noOfDays = 0;
    // }

    // //$happyIndexFirstData = DB::table('happy_indexes')->select('created_at')->orderBy('created_at','asc')->first();

    // $movingAvgIndex = 0;

    // if($noOfDays !=0 && $happyFacesCount !=0){
    //     $movingAvgIndex = ($happyFacesCount/$noOfDays);  
    //     //$movingAvgIndex = ($happyFacesCount/$noOfDays)*100;  
    // }

    // $maValue = 0;
    // //if($movingAvgIndex !=0 && $happyIndexPer !=0)
    // if($movingAvgIndex !=0){
    //     //-1(MA – yesterday's H.I. value)/ MA
    //     $maValue = ((-1*($movingAvgIndex - $happyIndexPerOfyesterday))/($movingAvgIndex))*100;  
    //     //$maValue = -1*($movingAvgIndex - $happyIndexPer)/$movingAvgIndex;  
    // }

    // $resultArray['happyIndexMA'] = round($maValue,2);




    //check DOT rating is given by user on every dot value if it is yes then true
    $userDetails = Auth::user();
    $departmentId = $userDetails->departmentId;

    $orgUsers = DB::table('users')->where('status','Active')->where('orgId',$orgId)->get();

    $departments = DB::table('departments')->select('departmentId')->where('id',$departmentId)->first();

    $departId = $departments->departmentId;

        if($excludeWeekend ==1){  //1 for include weekend else not
              $resultArray['indexOrg'] = DB::table('indexReportRecords')
                ->where('orgId',$orgId)
                ->whereNull('officeId')
                ->whereNull('departmentId')
                ->whereNull('isAllDepId')
                ->whereDate('date',date('Y-m-d', strtotime('-1 days')))
                ->sum('total_count');
        }else{
              $resultArray['indexOrg'] = DB::table('indexReportRecords')
                ->where('orgId',$orgId)
                ->whereNull('officeId')
                ->whereNull('departmentId')
                ->whereNull('isAllDepId')
                ->whereDate('date',date('Y-m-d', strtotime('-1 days')))
               ->sum('total_count_exclude_weekend');
        }
        
        //print_r($resultArray['indexOrg']);die();
            // $resultArray['indexDept'] = DB::table('indexReportRecords')
            // ->where('orgId',$orgId)
            // ->where('departmentId',$departmentId)
            // ->where('date',date('Y-m-d', strtotime('-1 days')))
            // ->sum('total_count');

         if($excludeWeekend ==1){  //1 for include weekend else not
            $resultArray['indexDept'] = DB::table('indexReportRecords')
            // ->leftjoin('departments','departments.id','indexReportRecords.departmentId')
            // ->leftjoin('all_department','all_department.id','departments.departmentId')
            // ->where('departments.status','active')
            // ->where('all_department.status','active')
            ->where('orgId',$orgId)
            ->where('departmentId',$departId)
            ->whereNull('officeId')
            ->where('isAllDepId',1)
            // ->where('departmentId',$departmentId)
            ->whereDate('date',date('Y-m-d', strtotime('-1 days')))
            ->sum('total_count');
        }else{
            $resultArray['indexDept'] = DB::table('indexReportRecords')
            // ->leftjoin('departments','departments.id','indexReportRecords.departmentId')
            // ->leftjoin('all_department','all_department.id','departments.departmentId')
            // ->where('departments.status','active')
            // ->where('all_department.status','active')
            ->where('orgId',$orgId)
            ->where('departmentId',$departId)
            ->whereNull('officeId')
            ->where('isAllDepId',1)
            // ->where('departmentId',$departmentId)
            ->whereDate('date',date('Y-m-d', strtotime('-1 days')))
            ->sum('total_count_exclude_weekend');
        }
    //print_r($resultArray['indexDept']);die();
    
    if($excludeWeekend ==1){  //1 for include weekend else not
        $dayBeforeYesterdayIndexOrg = DB::table('indexReportRecords')
            ->where('orgId',$orgId)
            ->whereNull('officeId')
            ->whereNull('departmentId')
            ->whereNull('isAllDepId')
            ->whereDate('date',date('Y-m-d', strtotime('-2 days')))
            ->sum('total_count');
    }else{
         $dayBeforeYesterdayIndexOrg = DB::table('indexReportRecords')
            ->where('orgId',$orgId)
            ->whereNull('officeId')
            ->whereNull('departmentId')
            ->whereNull('isAllDepId')
            ->whereDate('date',date('Y-m-d', strtotime('-2 days')))
            ->sum('total_count_exclude_weekend');
    }

    // $dayBeforeYesterdayIndexDept = DB::table('indexReportRecords')
    //     ->where('orgId',$orgId)
    //     ->where('departmentId',$departmentId)
    //     ->where('date',date('Y-m-d', strtotime('-2 days')))
    //     ->sum('total_count');


    if($excludeWeekend ==1){  //1 for include weekend else not
        $dayBeforeYesterdayIndexDept = DB::table('indexReportRecords')
            // ->leftjoin('departments','departments.id','indexReportRecords.departmentId')
            // ->leftjoin('all_department','all_department.id','departments.departmentId')
            // ->where('departments.status','active')
            // ->where('all_department.status','active')
            ->where('orgId',$orgId)
            ->where('departmentId',$departId)
            ->whereNull('officeId')
            ->where('isAllDepId',1)
            // ->where('departmentId',$departmentId)
            ->whereDate('date',date('Y-m-d', strtotime('-2 days')))
            ->sum('total_count');
    }else{
        $dayBeforeYesterdayIndexDept = DB::table('indexReportRecords')
            // ->leftjoin('departments','departments.id','indexReportRecords.departmentId')
            // ->leftjoin('all_department','all_department.id','departments.departmentId')
            // ->where('departments.status','active')
            // ->where('all_department.status','active')
            ->where('orgId',$orgId)
            ->where('departmentId',$departId)
            ->whereNull('officeId')
            ->where('isAllDepId',1)
            // ->where('departmentId',$departmentId)
            ->whereDate('date',date('Y-m-d', strtotime('-2 days')))
            ->sum('total_count_exclude_weekend');
    }

    $resultArray['indexOrgDD'] = 0;
    if (!empty($dayBeforeYesterdayIndexOrg)) {
        $resultArray['indexOrgDD'] = round((-1*($dayBeforeYesterdayIndexOrg - $resultArray['indexOrg'])/$dayBeforeYesterdayIndexOrg)*100,2);
    }

    $resultArray['indexDepDD'] = 0;
    if (!empty($dayBeforeYesterdayIndexDept)) {
        $resultArray['indexDepDD'] = round((-1*($dayBeforeYesterdayIndexDept - $resultArray['indexDept'])/$dayBeforeYesterdayIndexDept)*100,2);
    }

    if($excludeWeekend ==1){  //1 for include weekend else not
        $allPreviousDaysRecordOfOrg = DB::table('indexReportRecords')
            ->where('orgId',$orgId)
            ->whereNull('officeId')
            ->whereNull('departmentId')
            ->whereNull('isAllDepId')
            ->whereDate('date','<',date('Y-m-d'))
            ->sum('total_count');
    }else{
         $allPreviousDaysRecordOfOrg = DB::table('indexReportRecords')
            ->where('orgId',$orgId)
            ->whereNull('officeId')
            ->whereNull('departmentId')
            ->whereNull('isAllDepId')
            ->whereDate('date','<',date('Y-m-d'))
            ->sum('total_count_exclude_weekend');
    }

    // $allPreviousDaysRecordOfDept = DB::table('indexReportRecords')
    //     ->where('orgId',$orgId)
    //     ->where('departmentId',$departmentId)
    //     ->where('date','<',date('Y-m-d'))
    //     ->sum('total_count');


    if($excludeWeekend ==1){  //1 for include weekend else not
        $allPreviousDaysRecordOfDept = DB::table('indexReportRecords')
            // ->leftjoin('departments','departments.id','indexReportRecords.departmentId')
            // ->leftjoin('all_department','all_department.id','departments.departmentId')
            // ->where('departments.status','active')
            // ->where('all_department.status','active')
            ->where('orgId',$orgId)
            ->where('departmentId',$departId)
            ->whereNull('officeId')
            ->where('isAllDepId',1)
            ->whereDate('date','<',date('Y-m-d'))
            ->sum('total_count');
    }else{
        $allPreviousDaysRecordOfDept = DB::table('indexReportRecords')
            // ->leftjoin('departments','departments.id','indexReportRecords.departmentId')
            // ->leftjoin('all_department','all_department.id','departments.departmentId')
            // ->where('departments.status','active')
            // ->where('all_department.status','active')
            ->where('orgId',$orgId)
            ->where('departmentId',$departId)
            ->whereNull('officeId')
            ->where('isAllDepId',1)
            ->whereDate('date','<',date('Y-m-d'))
            ->sum('total_count_exclude_weekend');
    }
        
    // ->where('departmentId',$departmentId)

    //No of days from first staff created of organisation till yesterday
    $firstStaffOfOrg = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    if (!empty($firstStaffOfOrg)) {
        $firstStaffOfOrgDate = strtotime($firstStaffOfOrg->created_at);
        $yesterdayDate = strtotime(Carbon::today());
        
        if ($firstStaffOfOrgDate < $yesterdayDate) {
            $firstStaffOfOrgDate = date_create(date('Y-m-d',strtotime($firstStaffOfOrg->created_at)));
            $yesterdayDate = date_create(Carbon::yesterday());
            
            $diff = date_diff($firstStaffOfOrgDate,$yesterdayDate);
            $noOfDays = $diff->format("%a")+1;
        }else{
            $noOfDays = 0;
        }
    }else{
        $noOfDays = 0;
    }

    //No of days from first staff created of organisation of department till yesterday
    $firstStaffOfDept = DB::table('users')
        ->select('users.created_at')
        ->leftjoin('departments','departments.id','users.departmentId')
        ->leftjoin('all_department','all_department.id','departments.departmentId')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('departments.departmentId',$departId)
        //->where('departments.departmentId',$departments->departmentId)
        ->where('users.orgId',$orgId)
        ->orderBy('users.created_at','ASC')
        ->first();

    if (!empty($firstStaffOfDept)) {
        $firstStaffOfDeptDate = strtotime($firstStaffOfDept->created_at);
        $yesterdayDate = strtotime(Carbon::today());
        
        if ($firstStaffOfDeptDate < $yesterdayDate) {
            $firstStaffOfDeptDate = date_create(date('Y-m-d',strtotime($firstStaffOfDept->created_at)));
            $yesterdayDate = date_create(Carbon::yesterday());
            
            $diff = date_diff($firstStaffOfDeptDate,$yesterdayDate);
            $noOfDaysDept = $diff->format("%a")+1;
        }else{
            $noOfDaysDept = 0;
        }
    }else{
        $noOfDaysDept = 0;
    }

    //First calculate Moving Average of organisation
    $movingAverageOrgIndex = 0;
    if (!empty($allPreviousDaysRecordOfOrg) && !empty($noOfDays)) {
        $movingAverageOrgIndex = ($allPreviousDaysRecordOfOrg/$noOfDays);
    }
    //Now Calculate MA
    $resultArray['indexOrgMA'] = 0;
    if (!empty($movingAverageOrgIndex)) {
        $resultArray['indexOrgMA'] = round((-1*($movingAverageOrgIndex - $resultArray['indexOrg'])/$movingAverageOrgIndex)*100,2);
    }

    //First calculate Moving Average of department
    $movingAverageDeptIndex = 0;
    if (!empty($allPreviousDaysRecordOfDept) && !empty($noOfDaysDept)) {
        $movingAverageDeptIndex = ($allPreviousDaysRecordOfDept/$noOfDaysDept);
    }
    //Now Calculate MA
    $resultArray['indexDeptMA'] = 0;
    if (!empty($movingAverageDeptIndex)) {
        $resultArray['indexDeptMA'] = round((-1*($movingAverageDeptIndex - $resultArray['indexDept'])/$movingAverageDeptIndex)*100,2);
    }

    return response()->json(['code'=>200, 'status'=>true,'service_name'=>'dashboard-detail','message'=>'','data'=>$resultArray]);

}

// public function getDashboardDetail()
// {

// 	$resultArray = array();

// 	$orgId = Input::get('orgId');

// 	$dot = DB::table('dots')->where('orgId',$orgId)->first();

// 	$vision = '';
// 	if($dot)
// 	{
// 		$vision = $dot->vision;
// 	}

// 	$resultArray['vision'] = $vision;

//     //get tribeometer detail
// 	$queCatTbl = DB::table('tribeometer_questions_category')->get();

// 	$users = DB::table('tribeometer_answers')
// 	->leftjoin('users','users.id','tribeometer_answers.userId')
// 	->select('tribeometer_answers.userId')
// 	->where('users.status','Active')
// 	->groupBy('tribeometer_answers.userId')
// 	->where('users.orgId',$orgId)->get();

// 	$userCount = count($users);

// 	$optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
// 	$optCount = count($optionsTbl)-1;

// 	$tribeoResultArray = array();
// 	foreach ($queCatTbl as $value)
// 	{
// 		$questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();

// 		$quecount = count($questionTbl);

// 		$perQuePercen = 0;
// 		foreach ($questionTbl as  $queValue)
// 		{
// 			$diaAnsTbl = DB::table('tribeometer_answers')     
// 			->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
// 			->leftJoin('users','users.id','tribeometer_answers.userId')
// 			->where('users.status','Active')
// 			->where('tribeometer_answers.orgId',$orgId)
// 			->where('tribeometer_questions.id',$queValue->id)
// 			->where('tribeometer_questions.category_id',$value->id)
// 			->sum('answer');

//         //avg of all questions
// 			if ($userCount !=0)
// 			{
// 				$perQuePercen += ($diaAnsTbl/$userCount); 
// 			}
			
// 		}

// 		$score = ($perQuePercen/($quecount*$optCount));
// 		$totalPercentage = ($perQuePercen/($quecount*$optCount))*100;

// 		$value1['title']      =  $value->title;       
// 		$value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');      

// 		array_push($tribeoResultArray, $value1);
// 	}

// 	$resultArray['tribeometerDetail'] = $tribeoResultArray;


//  $user = Auth::user();

//  $userId = $user->id;

//  $isUser = DB::table('happy_indexes')->where('userId',$userId)->where('status','Active')->whereDate('created_at', date('Y-m-d'))->first();

//  $resultArray['userGivenfeedback'] = false;
//  if($isUser) 
//  {
//     $resultArray['userGivenfeedback'] = true;
// }


// /*get previous day happy index detail*/
// $userTypeArr = array('3','2','1');

// $totalIndexes = 0;
// $totalUser    = 0;
// foreach($userTypeArr as $type)
// {

//   $userCount = DB::table('happy_indexes')
//   ->leftjoin('users','users.id','happy_indexes.userId')
//   ->where('happy_indexes.status','Active')
//   ->where('happy_indexes.moodValue',$type)
//   ->where('users.orgId',$orgId)        
//   ->whereDate('happy_indexes.created_at', date('Y-m-d', strtotime('-1 days')))
//   ->count();

//   $totalUser +=$userCount;

//   if($type==3) 
//   {
//      $totalIndexes += $userCount;
//  }       
// }


// $happyIndexPer = 0;
// if($totalIndexes !=0 && $totalUser !=0)
// { 
//   $happyIndexPer = (($totalIndexes/($totalUser))*100);
// }

// $resultArray['happyIndex'] = round($happyIndexPer,2);

// //get yesterday's previous day happy index
// $totalIndexesOfyesterdaysPreviousday = 0;
// $totalUserOfyesterdaysPreviousday    = 0;
// foreach($userTypeArr as $type)
// {

//     $userCount = DB::table('happy_indexes')
//     ->leftjoin('users','users.id','happy_indexes.userId')
//     ->where('happy_indexes.status','Active')
//     ->where('happy_indexes.moodValue',$type)
//     ->where('users.orgId',$orgId)        
//     ->whereDate('happy_indexes.created_at', date('Y-m-d', strtotime('-2 days')))
//     ->count();

//     $totalUserOfyesterdaysPreviousday +=$userCount;

//     if($type==3) 
//     {
//         $totalIndexesOfyesterdaysPreviousday += $userCount;
//     }       
// }

// $happyIndexPerOfyesterdaysPreviousday = 0;
// if($totalIndexesOfyesterdaysPreviousday !=0 && $totalUserOfyesterdaysPreviousday !=0)
// {
//     $happyIndexPerOfyesterdaysPreviousday = ($totalIndexesOfyesterdaysPreviousday/($totalUserOfyesterdaysPreviousday))*100;       
// }

//     // =-1(previous day yesterday H.I. value – todays yesterday H.I. value) / previous day yesterday H.I. value 

// $happyIndexDD = 0;
// if($happyIndexPerOfyesterdaysPreviousday !=0) 
// {
//     $happyIndexDD = -1*($happyIndexPerOfyesterdaysPreviousday - $happyIndexPer)/$happyIndexPerOfyesterdaysPreviousday;
// }

// $resultArray['happyIndexDD'] =round($happyIndexDD,2);

//   //M.A.(First calculate Moving Average:) = (Total number of happy faces from all previous days/number of days)
//   //=-1(M.A – todays yesterday H.I. value) / M.A

// $happyFacesCount = DB::table('happy_indexes')
// ->leftjoin('users','users.id','happy_indexes.userId')
// ->where('happy_indexes.status','Active')
// ->where('happy_indexes.moodValue','3')
// ->where('users.orgId',$orgId)        
// ->whereDate('happy_indexes.created_at','<=', date('Y-m-d', strtotime('-1 days')))->count();


// $noOfDaysTbl = DB::table('happy_indexes')
// ->select(DB::raw('DATE(happy_indexes.created_at) as date'), DB::raw('count(*) as views'))
// ->groupBy('date')
// ->leftjoin('users','users.id','happy_indexes.userId')
// ->where('happy_indexes.status','Active')
// ->whereDate('happy_indexes.created_at','<=', date('Y-m-d', strtotime('-1 days')))
// ->where('users.orgId',$orgId)->get();

// $movingAvgIndex = 0;
// $noOfDays = count($noOfDaysTbl);

// if($noOfDays !=0 && $happyFacesCount !=0)
// {
//     $movingAvgIndex = ($happyFacesCount/$noOfDays);  
// }

// $maValue = 0;
// if($movingAvgIndex !=0 && $happyIndexPer !=0)
// {
//     $maValue = -1*($movingAvgIndex - $happyIndexPer)/$movingAvgIndex;  
// }

// $resultArray['happyIndexMA'] = round($maValue,2);

// return response()->json(['code'=>200, 'status'=>true,'service_name'=>'dashboard-detail','message'=>'','data'=>$resultArray]);

// }

//Index report for DOT completed Values
protected function indexReportForDotCompletedValues($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $userId   = $perArr["userId"];
    $departmentId   = $perArr["departmentId"];

    // print_r($orgId."<br>");
    // print_r($departmentId."<br>");
    $query = DB::table('dots_values')  
    ->select('dots_values.id AS id')     
    ->leftjoin('dots_beliefs','dots_beliefs.id','dots_values.beliefId')
    ->leftjoin('dots','dots.id','dots_beliefs.dotId')
    ->where('dots_beliefs.status','Active')
    ->where('dots.status','Active')
    ->where('dots_values.status','Active');
    if($orgId) 
    {
        $query->where('dots.orgId', $orgId);
    }
    $dotValues = $query->get();

    foreach($dotValues as $value)
    {
        $dvValueQuery = DB::table('dot_values_ratings')
        ->leftjoin('users','users.id','dot_values_ratings.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('dot_values_ratings.userId',$userId)
        ->where('dot_values_ratings.valueId',$value->id)
        ->where('dot_values_ratings.status','Active');
        if (!empty($departmentId)) {
            $dvValueQuery->where('departments.departmentId',$departmentId);
        }
        $dvValue = $dvValueQuery->first();
        if(!$dvValue)
        {
            return false;
        }
    }
    if (count($dotValues) == 0) {
      return false;
    }else{
      return true;
    }
    //return true;
}

//Index report for Team Role Completed Values
protected function indexReportForTeamroleCompleted($perArr = array())
{
    $orgId = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $query = DB::table('cot_answers') 
    ->select('cot_answers.userId')
    ->distinct()
    ->rightjoin('users','users.id','cot_answers.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->where('cot_answers.status','Active')
    ->where('cot_answers.orgId',$orgId);
    if (!empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }
    $result = $query->get();

    return $result;
}

//Index report for Personality Type Completed Values
protected function indexReportForpersonalityTypeCompleted($perArr = array())
{

    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $query = DB::table('cot_functional_lens_answers')
    ->select('cot_functional_lens_answers.userId')
    ->distinct()
    ->rightjoin('users','users.id','cot_functional_lens_answers.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->where('cot_functional_lens_answers.status','Active')
    ->where('cot_functional_lens_answers.orgId',$orgId);
    
    if (!empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }
    $result = $query->get();

    return $result;
}

//Index report for Culture structure Completed
protected function indexReportForCultureStructureCompleted($perArr = array())
{

    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $query = DB::table('sot_answers')   
    ->select('sot_answers.userId')
    ->distinct()      
    ->leftJoin('users','users.id','=','sot_answers.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('sot_answers.status','Active')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->where('sot_answers.updated_at','LIKE',date("Y-m", strtotime("-1 month"))."%")
    // ->where('sot_answers.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%")
    //->whereMonth('sot_answers.created_at', Carbon::now()->subMonth()->month)
    ->where('users.orgId',$orgId);

    if (!empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }  
    $result = $query->get();

    return $result;
}

//Index report for Culture structure Completed for all previous days
protected function indexReportForCultureStructureCompletedAllPreviousDays($perArr = array())
{

    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $query = DB::table('sot_answers')   
    ->select('sot_answers.userId')
    ->distinct()      
    ->leftJoin('users','users.id','sot_answers.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('sot_answers.status','Active')
    ->where('users.status','Active')
    // ->whereDate('sot_answers.created_at', '<', date('Y-m-d'))
    ->whereDate('sot_answers.updated_at', '<', date('Y-m-d'))
    ->where('users.orgId',$orgId);

    if (!empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }  
    $result = $query->get();

    return $result;
}

//Index report for motivation Completed
protected function indexReportForMotivationCompleted($perArr = array())
{
    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $query = DB::table('sot_motivation_answers')
    ->select('sot_motivation_answers.userId')
    ->distinct()      
    ->leftJoin('users','users.id','=','sot_motivation_answers.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('sot_motivation_answers.status','Active')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->where('users.orgId',$orgId);   

    if (!empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }  
    // if($orgId)
    // {
    //     $query->where('users.orgId',$orgId);
    // }
    $result = $query->get();

    return $result;
}

//Index report for Thumbsup Completed
protected function indexReportForThumbsupCompleted($perArr = array())
{
    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $query = DB::table('dot_bubble_rating_records')
    ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
    ->leftjoin('users','users.id','dot_bubble_rating_records.to_bubble_user_id')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('dots.status','Active')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->where('dot_bubble_rating_records.status','Active')
    //->whereDate('dot_bubble_rating_records.created_at',Carbon::today());                 
    ->whereDate('dot_bubble_rating_records.created_at',Carbon::yesterday());                 
    if(!empty($orgId))
    {
        $query->where('dots.orgId',$orgId);
    }
    if (!empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }  
    $bubbleCount = $query->count('bubble_flag');

    return $bubbleCount;
}

//Index report for Thumbsup Completed day before yesterday
protected function indexReportForThumbsupCompletedDayBeforeYesterday($perArr = array())
{
    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $dayBeforeYesterdayQuery = DB::table('dot_bubble_rating_records')
    ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
    ->leftjoin('users','users.id','dot_bubble_rating_records.to_bubble_user_id')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->whereDate('dot_bubble_rating_records.updated_at',date('Y-m-d', strtotime('-2 days')));                 
    //->whereDate('dot_bubble_rating_records.created_at',Carbon::yesterday());                 
    //->whereDate('dot_bubble_rating_records.created_at',date('Y-m-d', strtotime('-2 days')));                 
    if(!empty($orgId))
    {
        $dayBeforeYesterdayQuery->where('dots.orgId',$orgId);
    }
    if (!empty($departmentId)) {
        $dayBeforeYesterdayQuery->where('departments.departmentId',$departmentId);
    }  
    $dayBeforeYesterdayBubbleCount = $dayBeforeYesterdayQuery->count('bubble_flag');

    return $dayBeforeYesterdayBubbleCount;
}

//Index report for Thumbsup Completed all previous days
protected function indexReportForThumbsupCompletedAllPreviousDays($perArr = array())
{
    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $allPreviousDayQuery = DB::table('dot_bubble_rating_records')
    ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
    ->leftjoin('users','users.id','dot_bubble_rating_records.to_bubble_user_id')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->whereDate('dot_bubble_rating_records.updated_at', '<', date('Y-m-d'));
    // ->whereDate('dot_bubble_rating_records.created_at', '<', date('Y-m-d'));
    //->whereDate('dot_bubble_rating_records.created_at',date('Y-m-d', strtotime('-2 days')));                 
    if(!empty($orgId))
    {
        $allPreviousDayQuery->where('dots.orgId',$orgId);
    }
    if (!empty($departmentId)) {
        $allPreviousDayQuery->where('departments.departmentId',$departmentId);
    }  
    $allPreviousDayBubbleCount = $allPreviousDayQuery->count('bubble_flag');

    return $allPreviousDayBubbleCount;
}

//Index report for happy index Completed
protected function indexReportForHappyIndexCompleted($perArr = array())
{
    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $userTypeArr = array('3','2','1');//3 for happy and 2 for avrage and 1 for sad

    $totalIndexes = 0;
    $totalUser    = 0;
    foreach($userTypeArr as $type)
    {
        $happyUserCountQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('happy_indexes.status','Active')
        ->where('happy_indexes.moodValue',$type)
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('users.orgId',$orgId)        
        ->whereDate('happy_indexes.created_at', Carbon::yesterday());

        if (!empty($departmentId)) {
            $happyUserCountQuery->where('departments.departmentId',$departmentId);
        }  
        $happyUserCount = $happyUserCountQuery->count();

        $totalUser +=$happyUserCount;

        if($type==3) 
        {
            $totalIndexes += $happyUserCount;
        }       
    }

    $happyIndexcount = 0;
    if($totalIndexes !=0 && $totalUser !=0)
    { 
      $happyIndexcount = (($totalIndexes)/($totalUser))*100;
    }

    return $happyIndexcount;
}

//Index report for happy index Completed day before yesterday
protected function indexReportForHappyIndexCompletedDayBeforeYesterday($perArr = array())
{
    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $userTypeArr = array('3','2','1');

    $totalIndexes = 0;
    $totalUser    = 0;
    foreach($userTypeArr as $type)
    {
        $happyUserCountQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('happy_indexes.status','Active')
        ->where('happy_indexes.moodValue',$type)
        ->where('users.orgId',$orgId)        
        ->whereDate('happy_indexes.created_at', date('Y-m-d', strtotime('-2 days')));
        //->whereDate('happy_indexes.created_at', Carbon::yesterday());

        if (!empty($departmentId)) {
            $happyUserCountQuery->where('departments.departmentId',$departmentId);
        }  

        $happyUserCount = $happyUserCountQuery->count();

        $totalUser +=$happyUserCount;

        if($type==3) 
        {
            $totalIndexes += $happyUserCount;
        }       
    }

    $happyIndexcount = 0;
    if($totalUser !=0)
    { 
      $happyIndexcount = round((($totalIndexes/($totalUser))*100),2);
    }

    return $happyIndexcount;
}

//Index report for happy index Completed For all previous days
protected function indexReportForHappyIndexCompletedAllPreviousDays($perArr = array())
{
    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $userTypeArr = array('3','2','1');

    $totalIndexes = 0;
    $totalUser    = 0;
    foreach($userTypeArr as $type)
    {
        $happyUserCountQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('happy_indexes.status','Active')
        ->where('happy_indexes.moodValue',$type)
        ->where('users.orgId',$orgId)        
        ->whereDate('happy_indexes.created_at', '<', date('Y-m-d'));
       
        if (!empty($departmentId)) {
            $happyUserCountQuery->where('departments.departmentId',$departmentId);
        }  

        $happyUserCount = $happyUserCountQuery->count();

        $totalUser +=$happyUserCount;

        if($type==3) 
        {
            $totalIndexes += $happyUserCount;
        }       
    }

    $happyIndexcount = 0;
    if($totalUser !=0)
    { 
      $happyIndexcount = round((($totalIndexes/($totalUser))*100),2);
    }

    return $happyIndexcount;
}

//Index report for diagnostic answer
protected function indexReportForDiagAnsCompletedValues($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $userId   = $perArr["userId"];
    $departmentId   = $perArr["departmentId"];

    $isDiagnosticAnsDoneQuery  = DB::table('diagnostic_answers')
        ->leftjoin('users','users.id','diagnostic_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('diagnostic_answers.userId',$userId)
        ->where('diagnostic_answers.orgId',$orgId)
        ->where('diagnostic_answers.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('diagnostic_answers.updated_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        // ->where('diagnostic_answers.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        //->whereMonth('diagnostic_answers.created_at',Carbon::now()->subMonth()->month);

    if (!empty($departmentId)) {
        $isDiagnosticAnsDoneQuery->where('departments.departmentId',$departmentId);
    }  
    $isDiagnosticAnsDone = $isDiagnosticAnsDoneQuery->first();

    return $isDiagnosticAnsDone;
}

//Index report for diagnostic answer for all previous days
protected function indexReportForDiagAnsCompletedValuesForAllPreviousDays($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $userId   = $perArr["userId"];
    $departmentId   = $perArr["departmentId"];

    $isDiagnosticAnsDoneForAllPreviousDaysQuery  = DB::table('diagnostic_answers')
        ->leftjoin('users','users.id','diagnostic_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('diagnostic_answers.userId',$userId)
        ->where('diagnostic_answers.orgId',$orgId)
        ->where('diagnostic_answers.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->whereDate('diagnostic_answers.updated_at', '<', date('Y-m-d'));
        // ->whereDate('diagnostic_answers.created_at', '<', date('Y-m-d'));
       
    if (!empty($departmentId)) {
        $isDiagnosticAnsDoneForAllPreviousDaysQuery->where('departments.departmentId',$departmentId);
    }  
    $isDiagnosticAnsDoneForAllPreviousDays = $isDiagnosticAnsDoneForAllPreviousDaysQuery->first();

    return $isDiagnosticAnsDoneForAllPreviousDays;
}

//Index report for Tribe Ans Completed values
protected function indexReportForTribeAnsCompletedValues($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $userId   = $perArr["userId"];
    $departmentId   = $perArr["departmentId"];

    $isTribeometerAnsDoneQuery = DB::table('tribeometer_answers')
        ->leftjoin('users','users.id','tribeometer_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('tribeometer_answers.userId',$userId)
        ->where('tribeometer_answers.orgId',$orgId)
        ->where('tribeometer_answers.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('tribeometer_answers.updated_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        // ->where('tribeometer_answers.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        //->whereMonth('tribeometer_answers.created_at',Carbon::now()->subMonth()->month);

    if (!empty($departmentId)) {
        $isTribeometerAnsDoneQuery->where('departments.departmentId',$departmentId);
    } 
    $isTribeometerAnsDone = $isTribeometerAnsDoneQuery->first();
    return $isTribeometerAnsDone;
}

//Index report for Tribe Ans Completed values
protected function indexReportForTribeAnsCompletedValuesForAllPreviousDays($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $userId   = $perArr["userId"];
    $departmentId   = $perArr["departmentId"];

    $isTribeometerAnsDoneForAllPreviousDaysQuery = DB::table('tribeometer_answers')
        ->leftjoin('users','users.id','tribeometer_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('tribeometer_answers.userId',$userId)
        ->where('tribeometer_answers.orgId',$orgId)
        ->where('tribeometer_answers.status','Active')
        ->where('users.status','Active')
        ->whereDate('tribeometer_answers.updated_at', '<', date('Y-m-d'));
        // ->whereDate('tribeometer_answers.created_at', '<', date('Y-m-d'));

    if (!empty($departmentId)) {
        $isTribeometerAnsDoneForAllPreviousDaysQuery->where('departments.departmentId',$departmentId);
    } 
    $isTribeometerAnsDoneForAllPreviousDays = $isTribeometerAnsDoneForAllPreviousDaysQuery->first();
    return $isTribeometerAnsDoneForAllPreviousDays;
}

//Index report for value performance
protected function indexReportForValuePerformanceCompleted($perArr = array())
{
    $departmentId   = $perArr["departmentId"]; 
    $dotId   = $perArr["dotId"]; 

    $allDotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dotId)
        ->orderBy('id','ASC')
        ->get();
        $totalBeliefRatings = 0;
        $dotValuesCount = 0;
        foreach ($allDotBeliefs as $key => $bValue)
        {
            $dotValues = DB::table('dots_values')
            ->where('dots_values.status','Active')
            ->where('dots_values.beliefId',$bValue->id)           
            ->count();
            $dotValuesCount += $dotValues;

            $beliefRatingQuery = DB::table('dot_values_ratings')
            ->leftjoin('users','users.id','dot_values_ratings.userId')
            ->leftJoin('departments','departments.id','users.departmentId')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('dot_values_ratings.beliefId',$bValue->id)
            ->where('users.status','Active')
            ->where('departments.status','Active')
            ->where('all_department.status','Active')
            ->where('dot_values_ratings.status','Active');

            if (!empty($departmentId)) {
                $beliefRatingQuery->where('departments.departmentId',$departmentId);
            } 
            $beliefRating = $beliefRatingQuery->avg('ratings');
            if($beliefRating)
            {
                $totalBeliefRatings += $beliefRating-1;
            }
        }
        $avgRatings = $totalBeliefRatings;
        $valuesPerformance = ((($avgRatings/$dotValuesCount)/5)*100);

        return $valuesPerformance;
}

//Index report for Feedback completed
protected function indexReportForFeedbackCompleted($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $departmentId   = $perArr["departmentId"];

    $iotFeedbackQuery = DB::table('iot_feedbacks')
    ->select('iot_feedbacks.id')
    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->where('iot_feedbacks.status','!=','Inactive')
    ->where('iot_feedbacks.orgId',$orgId);
    //->where('iot_feedbacks.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
    //->whereMonth('iot_feedbacks.created_at',Carbon::now()->subMonth()->month);

    if (!empty($departmentId)) {
        $iotFeedbackQuery->where('departments.departmentId',$departmentId);
    } 
    $iotFeedback = $iotFeedbackQuery->count();

    return $iotFeedback;
}

//Index report for Feedback completed for all previous days
protected function indexReportForFeedbackCompletedForAllPreviousDays($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $departmentId   = $perArr["departmentId"];

    $iotFeedbackQuery = DB::table('iot_feedbacks')
    ->select('iot_feedbacks.id')
    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('users.status','Active')
    ->where('iot_feedbacks.status','!=','Inactive')
    ->where('iot_feedbacks.orgId',$orgId)
    ->whereDate('iot_feedbacks.created_at', '<', date('Y-m-d'));
    
    if (!empty($departmentId)) {
        $iotFeedbackQuery->where('departments.departmentId',$departmentId);
    } 
    $iotFeedback = $iotFeedbackQuery->count();

    return $iotFeedback;
}

//Index report for User count for tribeometer
protected function indexReportForUserCountForTribe($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $departmentId   = $perArr["departmentId"];

    $usersForTribeQuery = DB::table('tribeometer_answers')
    ->leftjoin('users','users.id','tribeometer_answers.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->select('tribeometer_answers.userId')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->groupBy('tribeometer_answers.userId')
    ->where('users.orgId',$orgId);

    if (!empty($departmentId)) {
        $usersForTribeQuery->where('departments.departmentId',$departmentId);
    } 
    $usersForTribe = $usersForTribeQuery->get();
    $userCountForTribe = count($usersForTribe);

    return $userCountForTribe;
}

protected function indexReportForTribeDnC($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $departmentId   = $perArr["departmentId"];
    $userCount   = $perArr["userCount"];

    $queCatTbl    = DB::table('tribeometer_questions_category')->get();
    $tribeDirectionnConnection = 0;
    if (!empty($userCountForTribe)){
        $optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
        $optCount = count($optionsTbl)-1; 

        foreach ($queCatTbl as $value){
            $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();
            $quecount = count($questionTbl);

            $perQuePercen = 0;
            foreach ($questionTbl as  $queValue){
                $diaQuery = DB::table('tribeometer_answers')           
                ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
                ->leftjoin('users','users.id','=','tribeometer_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.status','Active') 
                ->where('departments.status','Active') 
                ->where('all_department.status','Active') 
                ->where('tribeometer_answers.orgId',$orgId)
                ->where('tribeometer_questions.id',$queValue->id)
                ->where('tribeometer_questions.category_id',$value->id);

                if (!empty($departmentId)) {
                    $diaQuery->where('departments.departmentId',$departmentId);
                } 
                $diaAnsTbl = $diaQuery->sum('answer');

                //avg of all questions
                $perQuePercen += ($diaAnsTbl/$userCountForTribe); 
            }
            $score = ($perQuePercen/($quecount*$optCount));
            $totalPercentage = (($perQuePercen/($quecount*$optCount))*100)*4;
            $tribeDirectionnConnection += $totalPercentage;
        }
    }

    // $queCatTbl    = DB::table('tribeometer_questions_category')->get();

    // $optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
    //     $optCount = count($optionsTbl)-1; 
    //     $tribeDirectionnConnection = 0;
    //     foreach ($queCatTbl as $value)
    //     {
    //         $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();
    //         $quecount = count($questionTbl);

    //         $perQuePercen = 0;
    //         foreach ($questionTbl as  $queValue)
    //         {    
    //             $diaQuery = DB::table('tribeometer_answers')           
    //             ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
    //             ->leftjoin('users','users.id','=','tribeometer_answers.userId')
    //             ->leftJoin('departments','departments.id','users.departmentId')
    //             ->leftJoin('all_department','all_department.id','departments.departmentId')
    //             ->where('users.status','Active') 
    //             ->where('tribeometer_answers.orgId',$orgId)
    //             ->where('tribeometer_questions.id',$queValue->id)
    //             ->where('tribeometer_questions.category_id',$value->id);

    //             if (!empty($departmentId)) {
    //                 $diaQuery->where('departments.departmentId',$departmentId);
    //             } 
    //             $diaAnsTbl = $diaQuery->sum('answer');

    //                 //avg of all questions
    //             $perQuePercen += ($diaAnsTbl/$userCount); 
    //         }

    //         $score = ($perQuePercen/($quecount*$optCount));
    //         $totalPercentage = round(((($perQuePercen/($quecount*$optCount))*100)*4),2);

    //         $tribeDirectionnConnection += $totalPercentage;

    //     }

    return $tribeDirectionnConnection;
}

//Index report for User count for diagnostics
protected function indexReportForUserCountForDiagnostics($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $departmentId   = $perArr["departmentId"];

    $diagQuery = DB::table('diagnostic_answers')
    ->leftjoin('users','users.id','diagnostic_answers.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->select('diagnostic_answers.userId')
    ->where('users.status','Active')
    ->where('departments.status','Active') 
    ->where('all_department.status','Active') 
    ->groupBy('diagnostic_answers.userId')
    ->where('users.orgId',$orgId);

    if (!empty($departmentId)) {
        $diagQuery->where('departments.departmentId',$departmentId);
    } 
    $diagUsers = $diagQuery->get();
    $diagUserCount = count($diagUsers);

    return $diagUserCount;
}

protected function indexReportForDiagnosticCore($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $departmentId   = $perArr["departmentId"];
    $userCount   = $perArr["userCount"];
    //$diagnosticQueCatTbl = $perArr["diagnosticQueCatTbl"];

    $diagnosticQueCatTbl    = DB::table('diagnostic_questions_category')->where('id','!=',5)->get();

    $totalDiagnosticCore = 0;

    if (!empty($diagUserCount)){
        $diaOptionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
        $diaOptCount    = count($diaOptionsTbl); 

        $diagnosticCorePer = 0;
        foreach ($diagnosticQueCatTbl as $value){
            $diaQuestionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();
            $diaQuecount = count($diaQuestionTbl);

            $diaPerQuePercen = 0;
            foreach ($diaQuestionTbl as  $queValue){
                $diaQuery = DB::table('diagnostic_answers')            
                ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
                ->leftjoin('users','users.id','=','diagnostic_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.status','Active')   
                ->where('departments.status','Active') 
                ->where('all_department.status','Active') 
                ->where('diagnostic_answers.orgId',$orgId)
                ->where('diagnostic_questions.id',$queValue->id)
                ->where('diagnostic_questions.category_id',$value->id);

                if (!empty($departmentId)) {
                    $diaQuery->where('departments.departmentId',$departmentId);
                } 
                $diaAnsTbl = $diaQuery->sum('answer'); 
                $diaPerQuePercen += ($diaAnsTbl/$diagUserCount);               
            }
            $diaTotalPercentage = ($diaPerQuePercen/($diaQuecount*$diaOptCount))*100;
            $diagnosticCorePer += $diaTotalPercentage;
        }
        $totalDiagnosticCore = ($diagnosticCorePer/5);
    }
   
    // $diaOptionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
    // $diaOptCount = count($diaOptionsTbl); 

    // $diagnosticCorePer = 0;

    // foreach ($diagnosticQueCatTbl as $value)
    // {
    //     $diaQuestionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

    //     $diaQuecount = count($diaQuestionTbl);

    //     $diaPerQuePercen = 0;
    //     foreach ($diaQuestionTbl as  $queValue)
    //     {
    //         $diaQuery = DB::table('diagnostic_answers')            
    //         ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
    //         ->leftjoin('users','users.id','=','diagnostic_answers.userId')
    //         ->leftJoin('departments','departments.id','users.departmentId')
    //         ->leftJoin('all_department','all_department.id','departments.departmentId')
    //         ->where('users.status','Active')   
    //         ->where('diagnostic_answers.orgId',$orgId)
    //         ->where('diagnostic_questions.id',$queValue->id)
    //         ->where('diagnostic_questions.category_id',$value->id);

    //         if (!empty($departmentId)) {
    //             $diaQuery->where('departments.departmentId',$departmentId);
    //         } 

    //         $diaAnsTbl = $diaQuery->sum('answer'); 

    //         $diaPerQuePercen += ($diaAnsTbl/$userCount);               
    //     }

    //     $diaTotalPercentage = ($diaPerQuePercen/($diaQuecount*$diaOptCount))*100;

    //     $diagnosticCorePer += $diaTotalPercentage;
    // }

    // $totalDiagnosticCore = round(($diagnosticCorePer/5),2);

    return $totalDiagnosticCore;

}

protected function indexReportForDiagnosticStress($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $departmentId   = $perArr["departmentId"];
    $userCount   = $perArr["userCount"];
    //$diagnosticQueCatTbl = $perArr["diagnosticQueCatTbl"];

    $diagnosticQueCatTbl    = DB::table('diagnostic_questions_category')->where('id',5)->get();

    $totalDiagnosticStress = 0;
    if (!empty($diagUserCount)){
        $diagOptionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
        $diagOptCount = count($diagOptionsTbl); 

        $diagnosticStress = 0;

        foreach ($diagnosticCatTbl as $value){
            $diagQuestionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();
            $diagQuecount = count($diagQuestionTbl);
            $diagPerQuePercen = 0;
            foreach ($diagQuestionTbl as  $queValue){
                $diaQuery = DB::table('diagnostic_answers')            
                ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
                ->leftjoin('users','users.id','=','diagnostic_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.status','Active')
                ->where('departments.status','Active') 
                ->where('all_department.status','Active')    
                ->where('diagnostic_answers.orgId',$orgId)
                ->where('diagnostic_questions.id',$queValue->id)
                ->where('diagnostic_questions.category_id',$value->id);

                if (!empty($departmentId)) {
                    $diaQuery->where('departments.departmentId',$departmentId);
                } 
                $diagAnsTbl = $diaQuery->sum('answer'); 

                $diagPerQuePercen += ($diagAnsTbl/$diagUserCount);               
            }

            // $diagScore = ($diagPerQuePercen/($diagQuecount*$diagOptCount));
            $diagTotalPercentage = ($diagPerQuePercen/($diagQuecount*$diagOptCount))*100;

            //$value1['title']      =  $value->title;
            //$value1['score']      =  number_format((float)$diagScore, 2, '.', '');
            // $value1['percentage'] =  number_format((float)$diagTotalPercentage, 2, '.', '');
            $diagnosticStress = (($diagTotalPercentage-100)*(-1))*3;    
            //array_push($diagnosticStressArray, $value1);
        }
        $totalDiagnosticStress = ($diagnosticStress);
    }
   
    // $diagOptionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
    //     $diagOptCount = count($diagOptionsTbl); 

    //     $diagnosticStress = 0;

    //     foreach ($diagnosticQueCatTbl as $value)
    //     {
    //         $diagQuestionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

    //         $diagQuecount = count($diagQuestionTbl);

    //         $diagPerQuePercen = 0;
    //         foreach ($diagQuestionTbl as  $queValue)
    //         {
    //             $diaQuery = DB::table('diagnostic_answers')            
    //             ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
    //             ->leftjoin('users','users.id','=','diagnostic_answers.userId')
    //             ->leftJoin('departments','departments.id','users.departmentId')
    //             ->leftJoin('all_department','all_department.id','departments.departmentId')
    //             ->where('users.status','Active')   
    //             ->where('diagnostic_answers.orgId',$orgId)
    //             ->where('diagnostic_questions.id',$queValue->id)
    //             ->where('diagnostic_questions.category_id',$value->id);

    //             if (!empty($officeId) && empty($departmentId)) {
    //                 $diaQuery->where('users.officeId',$officeId);
    //             }
    //             elseif (!empty($officeId) && !empty($departmentId)) {
    //                 $diaQuery->where('users.officeId',$officeId);
    //                 $diaQuery->where('users.departmentId',$departmentId);
    //             }
    //             elseif (empty($officeId) && !empty($departmentId)) {
    //                 $diaQuery->where('departments.departmentId',$departmentId);
    //             } 

    //             $diagAnsTbl = $diaQuery->sum('answer'); 

    //             $diagPerQuePercen += ($diagAnsTbl/$userCount);               
    //         }

    //         $diagTotalPercentage = ($diagPerQuePercen/($diagQuecount*$diagOptCount))*100;

    //         $diagnosticStress = (($diagTotalPercentage-100)*(-1))*3;    
    //     }
    //     $totalDiagnosticStress = round(($diagnosticStress),2);

    return $totalDiagnosticStress;

}


}
