<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use DB;
use Mail;

class ApiSOTController extends Controller
{
  /*get questions list*/
  public function getSotQuestionList()
  {

   $result = array();
   $questions = DB::table('sot_questionnaire_records')->select('id','section','question','type')->where('status','Active')->get();
   // group questions according to their section
   $questions = array_chunk($questions->toArray(),4);

  //  foreach ($questions as $element) 
  //  {
  //   $result[$element->section][] = $element;
  // }

   return response()->json(['code'=>200,'status'=>true,'service_name'=>'questions-list','message'=>'Questions list','data'=>$questions]);
 }

 /*add sot answer*/
 public function addSOTanswers()
 {

  $resultArray = array();

  $user = Auth::user();

  $optionArr = Input::get('answer');
  $userId    = $user->id;

  $insertId ='';

  foreach ($optionArr as  $value)
  {
    $insertArray = array(
      'userId'                 => $userId,             
      'question_id'            => $value['id'], 
      'score'                  => 1,               
      'status'                 => 'Active',
      'created_at'             => date('Y-m-d H:i:s'),
    );

    $insertId = DB::table('sot_answers')->insertGetId($insertArray);
  }                    


  if($insertId)
  {
    return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-SOT-answer','message'=>'SOT answer added successfully.','data'=>$resultArray]);
  }

  return response()->json(['code'=>400,'status'=>false,'service_name'=>'add-SOT-answer','message'=>'SOT answer not added','data'=>$resultArray]);
}


/*get SOT detail */
public function getSOTdetail(Request $request)
{

 $resultArray = array();
 $user        = Auth::user();

 if($request->userId)
 {
  $userId = $request->userId;
}
else
{
  $userId = $user->id;
}

if($request->orgId) 
{
  $orgId = $request->orgId;
}
else 
{
  $orgId = $user->orgId;
}


$sotCultureStrTbl = DB::table('sot_culture_structure_records')->select('id','type','title','imgUrl')->where('status','Active')->get();

$sotCountArray = array();
$countArr      = array();
$resultArray['IsQuestionnaireAnswerFilled'] = false;

foreach ($sotCultureStrTbl as $value) 
{

  $SOTCount = DB::table('sot_answers')
  ->select(DB::raw('sum(score) AS count'))
  ->leftJoin('sot_questionnaire_records','sot_questionnaire_records.id','=','sot_answers.question_id')
  ->join('users','users.id','=','sot_answers.userId')
  ->where('users.status','Active')
  ->where('sot_questionnaire_records.type',$value->type)    
  ->where('users.orgId',$orgId)        
  ->first();
  
  $value->SOTCount = ($SOTCount->count)?(string)$SOTCount->count:"0";


  $value->imgUrl   = url('public/uploads/sot/').'/'.$value->imgUrl;

  if ($SOTCount->count) {
    $resultArray['IsQuestionnaireAnswerFilled']   = true;
  }


  $IsUserFilledAnswer = DB::table('sot_answers')
  ->select(DB::raw('sum(score) AS count'))->where('userId',$userId)->first();

  if ($IsUserFilledAnswer->count) {
    $resultArray['IsUserFilledAnswer']   = true;
  }
  else
  {
    $resultArray['IsUserFilledAnswer']   = false;
  }

  array_push($countArr, $SOTCount->count);
  array_push($sotCountArray, $value);

}

// find maximum value array
$sotCountArray1 = array();
foreach ($sotCultureStrTbl as $value) 
{

  $SOTCount = DB::table('sot_answers')
  ->select(DB::raw('sum(score) AS count'))
  ->leftJoin('sot_questionnaire_records','sot_questionnaire_records.id','=','sot_answers.question_id')
  ->leftJoin('users','users.id','=','sot_answers.userId')
  ->where('users.status','Active')
  ->where('sot_questionnaire_records.type',$value->type)    
  ->where('users.orgId',$orgId)        
  ->first();
  
  $value->SOTCount = ($SOTCount->count)?(string)$SOTCount->count:"0";

  if (!empty($SOTCount->count) && max($countArr)==$SOTCount->count)
  {
    array_push($sotCountArray1, $value);

  }        

}


//detail of culture structure 
$sotStrDetailArr = array();

foreach($sotCountArray1 as $sValue)
{
  $sotStrDetail = DB::table('sot_culture_structure_records')->where('id',$sValue->id)->where('status','Active')->first();

  $summary = DB::table('sot_culture_structure_summary_records')->select('summary')->where('type',$sValue->id)->where('status','Active')->get();

  $sValue->summary = $summary;
  
  array_push($sotStrDetailArr, $sValue);
  break; 
}

$resultArray['sotDetailArray']         = $sotCountArray;
$resultArray['sotSummaryDetailArray']  = $sotStrDetailArr;

if($request->reportStatus)
{
  return $resultArray;
}

return response()->json(['code'=>200,'status'=>true,'service_name'=>'SOT-detail','message'=>'SOT detail.','data'=>$resultArray]);

}

/*get filed question answers of user*/
public function getSOTquestionAnswers()
{

  $resultArray = array();

  $user = Auth::user();
  $result = array();

  $userId = $user->id;

  $questions = DB::table('sot_questionnaire_records')->select('id','section','question','type')->where('status','Active')->get();

  $queArray = array();

  foreach($questions as $value)
  {
    $sotqueAns = DB::table('sot_answers')->where('question_id',$value->id)->where('userId',$userId)->first();

    $value->isChecked = false;
    if($sotqueAns)
    {
     $value->isChecked = true;
   }

   array_push($queArray, $value);

 }

 $sotqueAns = array_chunk($queArray,4);

 if($sotqueAns) 
 {
  return response()->json(['code'=>200,'status'=>true,'service_name'=>'SOT-question-answers','message'=>'SOT question answers.','data'=>$sotqueAns]);
}

return response()->json(['code'=>200,'status'=>false,'service_name'=>'SOT-question-answers','message'=>'SOT question answers.','data'=>$resultArray]);

}

/*update sot question answers*/
public function updateSOTquestionAnswer()
{

  $resultArray = array();

  $user = Auth::user();
  $userId = $user->id;

  $answerArray = Input::get('answer');
  
  $sotSumitedAnswers = DB::table('sot_answers')->where('status','Active')->where('userId',$userId)->get();

  for($i=0; $i < count($sotSumitedAnswers); $i++)
  { 

    $updatedId = $sotSumitedAnswers[$i]->id;

    if(!empty($answerArray[$i]['id']))
    {
      $updatedArray['question_id'] = $answerArray[$i]['id'];
    }

    $updatedArray['updated_at']  = date('Y-m-d H:i:s');
    

    DB::table('sot_answers')->where('id',$updatedId)->update($updatedArray);
  }

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'SOT-update-answers','message'=>'SOT answers updated successfully.','data'=>$resultArray]);
  

  // $resultArray = array();

  // $user = Auth::user();
  // $userId = $user->id;

  // $answerArray = Input::get('answer');

  // DB::table('sot_answers')->where('userId',$userId)->delete();

  // foreach ($answerArray as $value)
  // {

  //   $insertArray = array(
  //     'userId'                 => $userId,             
  //     'question_id'            => $value['id'], 
  //     'score'                  => 1,               
  //     'status'                 => 'Active',
  //     'created_at'             => date('Y-m-d H:i:s'),
  //   );

  //   $insertId = DB::table('sot_answers')->insertGetId($insertArray);
  
  // }

  // return response()->json(['code'=>200,'status'=>true,'service_name'=>'SOT-update-answers','message'=>'SOT answers updated successfully.','data'=>$resultArray]);
}

/*get sot motivation completed answers*/
public function getSOTmotivationCompletedAnswer()
{

  $resultArray = array();

  $user   = Auth::user();
  $userId = $user->id;

  $ans = DB::table('sot_motivation_answers')->where('userId',$userId)->first();

  if(empty($ans)) 
  {
    return response()->json(['code'=>400,'status'=>false,'service_name'=>'SOT-motivation-question-answers','message'=>'SOT answers not filled.','data'=>[]]);
  }

  $questionTbl = DB::table('sot_motivation_questions')->select('id as questionId','question as questionName')->where('status','Active')->get();

  foreach ($questionTbl as $value)
  {

   $optionsTbl = DB::table('sot_motivation_question_options')->select('id AS optionId','option_name AS option')->where('status','Active')->where('question_id',$value->questionId)->get();

   $value->option = array();
   foreach ($optionsTbl as $optValue)
   {

    $comAns = DB::table('sot_motivation_answers')->where('questionId',$value->questionId)->where('userId',$userId)->where('optionId',$optValue->optionId)->first();

    $optValue->points = 0;
    if($comAns)
    {
      $optValue->points    = $comAns->answer;
      $optValue->answerId  = $comAns->id;
      // $value->answerId     = $comAns->id;
    }      
    array_push($value->option, $optValue);
  }     

  array_push($resultArray, $value);
}

return response()->json(['code'=>200,'status'=>true,'service_name'=>'SOT-motivation-question-answers','message'=>'','data'=>$resultArray]);
}

/*update sot motivation answers*/
public function updateSOTmotivationAnswers()
{
  $resultArray = array();

  $optionArr = Input::get('answer');
  $user      = Auth::user();
  $userId    = $user->id; 

  for ($i=0; $i < count($optionArr); $i++)
  {
    foreach ($optionArr[$i]['option'] as $value)
    {

      $updateArray = array('answer'=> $value['rating'],'updated_at' => date('Y-m-d H:i:s'));

      DB::table('sot_motivation_answers')->where('id',$value['answerId'])->where('userId',$userId)->update($updateArray);
      
    }             
  } 

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'Update-cot-func-lens-answer','message'=>'cot motivation answer updated successfully.','data'=>$resultArray]);
}



/*request for questionary links*/
public function isSOTmotivationAnswersDone()
{
  $resultArray = array();

  $user   = Auth::user();
  $email  = $user->email;
  $name   = $user->name;
  $orgId  = $user->orgId;
  $userId = $user->id; 

  $org = DB::table('organisations')->select('organisation','SOT_motivation_start_date')->where('id',$orgId)->first();

  $resultArray['SOTmotivationStartDate'] ='';
  if($org)
  {    
    $resultArray['SOTmotivationStartDate'] = date('d-M-Y', strtotime($org->SOT_motivation_start_date));
  }

  //check mail is sent or not

  $resultArray['isSOTmotivationMailSent'] = "0";
  if($user->is_SOT_request_mail_sent)
  {
    $resultArray['isSOTmotivationMailSent'] = $user->is_SOT_request_mail_sent;    
  }

  $isUserAnswersFilled = DB::table('sot_motivation_answers')->where('userId',$userId)->first();
  //check answers done or not
  $resultArray['isUserAnswersFilled'] = "0";
  if($isUserAnswersFilled)
  {
    $resultArray['isUserAnswersFilled'] = "1";    
  }

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'SOT-motivation-answer-done','message'=>'SOT motivation answer done.','data'=>$resultArray]);
}


/*send mail for request of SOT Motivation score request*/
public function requestSOTmotivationScoreMail(){

 $resultArray = array();

 $user   = Auth::user();
 $email  = $user->email;
 $name   = $user->name;
 $orgId  = $user->orgId;
 $userId = $user->id; 

 $org = DB::table('organisations')->select('organisation')->where('id',$orgId)->first();

 $orgName ='';
 if ($org) {
   $orgName = $org->organisation;
 }

 $data = ['msg'=>'Please provide SOT-Motivation score to '.ucfirst($name).':' .$email.' working for organisation '.ucfirst($orgName)];

 $email ='team@tribe365.co';   

 $te = Mail::send('layouts/notificationMail', $data, function($message) use($email) 
 {
  $message->from('notification@tribe365.co', 'Tribe365');
  $message->to($email);
  $message->subject('Request SOT Motivations Score.');
});

 DB::table('users')->where('id',$userId)->update(['is_SOT_request_mail_sent'=>"1"]);

 return response()->json(['code'=>200,'status'=>true,'service_name'=>'SOT-request-motivation-score-mail','message'=>'SOT Motivation score request mail sent successfully.','data'=>$resultArray]);
}

/*get sot motivation user list */
public function getSOTmotivationUserList(Request $request)
{
  $resultArray = array();

  // $orgId  = Input::get('orgId');
  // $userId = Input::get('userId');

  $orgId  = $request->orgId;
  $userId = $request->userId;

  $query = DB::table('sot_motivation_answers')
  ->select('sot_motivation_answers.userId')
  ->leftjoin('users','users.id','sot_motivation_answers.userId')
  ->select('sot_motivation_answers.userId')
  ->where('users.status','Active')
  ->groupBy('sot_motivation_answers.userId')
  ->where('users.orgId',$orgId);

  if (!empty($userId))
  {
    $query->where('sot_motivation_answers.userId',$userId);
  }  
  
  $usersList = $query->get();

  $totalUser = count($usersList);

  if(empty($totalUser))
  {
    return response()->json(['code'=>400,'status'=>false,'service_name'=>'SOT-motivation-users','message'=>'SOT answers are not done by user.','data'=>$resultArray]);
  }

  $categoryTbl = DB::table('sot_motivation_value_records')->where('status','Active')->get();

  foreach ($categoryTbl as $value)
  {  

    $query1 = DB::table('sot_motivation_answers AS sotans')
    ->leftJoin('sot_motivation_question_options AS qoption', 'qoption.id', '=', 'sotans.optionId')
    ->leftjoin('users','users.id','sotans.userId')   
    ->where('users.status','Active')
    ->where('sotans.orgId',$orgId)
    ->where('sotans.status','Active')
    ->where('qoption.category_id',$value->id);

    if(!empty($userId))
    {      
      $query1->where('sotans.userId',$userId);
    }    

    $ansTbl = $query1->sum('sotans.answer'); 

    $result['title'] = $value->title;
    $result['score'] = (string)$ansTbl;

    if(empty($userId))
    {
      $result['score'] = number_format((float)($ansTbl/$totalUser), 2); 
    }

    array_push($resultArray, $result);

  }

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'SOT-motivation-users','message'=>'SOT motivation users.','data'=>$resultArray]);
}

//get COT Motivation questions
public function getSOTmotivationQuestions()
{

 $resultArray = array();

 $questionTbl = DB::table('sot_motivation_questions')->select('id AS questionId','question AS questionName')->where('status','Active')->get();

 foreach ($questionTbl as $queValue) 
 {

  $cotOptions = DB::table('sot_motivation_question_options')->select('id AS OptionId','option_name AS option','category_id AS categoryId')
  ->where('question_id',$queValue->questionId)
  ->where('status','Active')->get();

  $optArray = array();

  foreach ($cotOptions as $ansValue)
  {
    array_push($optArray, $ansValue);
  }

  $queValue->option = $optArray;

  array_push($resultArray, $queValue);
}

return response()->json(['code'=>200,'status'=>true,'service_name'=>'sot-motivation-questions','message'=>'Sot question list.','data'=>$resultArray]);

}


/*add SOT motivation answer*/
public function addSOTmotivationAnswer()
{

 $resultArray = array();

 $optionArr = Input::get('answer');
 $user      = Auth::user();
 $userId    = $user->id;
 $orgId     = $user->orgId;

 $insertId  ='';
 for ($i=0; $i < count($optionArr); $i++)
 { 

  foreach ($optionArr[$i]['option'] as  $value)
  {
    $insertArray = array(
      'userId'                 => $userId, 
      'orgId'                  => $orgId,
      'questionId'             => $optionArr[$i]['questionId'],                
      'optionId'               => $value['optionId'],
      'answer'                 => $value['rating'],     
      'status'                 => 'Active',
      'created_at'             => date('Y-m-d H:i:s'),
    );
    
    $insertId = DB::table('sot_motivation_answers')->insertGetId($insertArray);
  }                    

}

if($insertId)
{
  return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-sot-motivation-answer','message'=>'SOT motivation answer added successfully.','data'=>$resultArray]);
}

return response()->json(['code'=>400,'status'=>false,'service_name'=>'add-sot-motivation-answer','message'=>'SOT motivation answer not added','data'=>$resultArray]);

}

//get sot motivation report
public function getSOTmotivationReport(Request $request)
{

  $resultArray = array();

  // $orgId  = Input::get('orgId');
  // $userId = Input::get('userId');

  $orgId  = $request->orgId;
  $userId = $request->userId;

  $categoryTbl = DB::table('sot_motivation_value_records')->where('status','Active')->get();

  foreach ($categoryTbl as $value)
  {  

    $query = DB::table('sot_motivation_answers AS sotans')
    ->leftJoin('sot_motivation_question_options AS qoption', 'qoption.id', '=', 'sotans.optionId');
    
    if(!empty($userId))
    {
      $query->where('sotans.userId',$userId);
    }
    
    $query->where('sotans.orgId',$orgId)
    ->where('sotans.status','Active')
    ->where('qoption.category_id',$value->id);


    $ansTbl = $query->sum('sotans.answer'); 

    $result['title'] = $value->title;
    $result['score'] = (string)$ansTbl;


    array_push($resultArray, $result);

  }

  if($request->reportStatus)
  {
    return $resultArray;
  }

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'sot-motivation-report','message'=>'','data'=>$resultArray]);
}


}