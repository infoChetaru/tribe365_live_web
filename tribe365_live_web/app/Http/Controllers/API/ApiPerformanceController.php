<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Client;
use DB;

class ApiPerformanceController extends Controller
{

	public function getPerformance()
	{

		$resultArray = array();

		$orgId = Input::get('orgId');
		$reportType = Input::get('reportType');
		$date = Input::get('date');

		$month ='';
		$year  ='';
		$currentDate = '';
		if($reportType == 'daily' || $reportType == '') 
		{
			$currentDate = date('Y-m-d');          
		}       
		else if($reportType == 'monthly')
		{
			$month = date('m', strtotime($date));
			$year  = date('Y', strtotime($date));
		}


		$organisations = DB::table('organisations')->where('status','Active')->orderBy('organisation','ASC')->get();

		$usersQuery = DB::table('users')->where('status','Active');

		if(!empty($orgId))
		{
			$usersQuery->where('orgId',$orgId);
		}

		$users = $usersQuery->get();

		$userCount = count($users);



		$query = DB::table('dot_bubble_rating_records')
		->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id');                  

		if(!empty($orgId))
		{
			$query->where('dots.orgId',$orgId);
		}

		if(!empty($currentDate))
		{
			$query->whereDate('dot_bubble_rating_records.created_at',$currentDate);
		}

		if(!empty($month) && !empty($year))
		{ 
			$query->whereMonth('dot_bubble_rating_records.created_at', $month);
			$query->whereYear('dot_bubble_rating_records.created_at', $year);
		}

		$bubbleCount = $query->count('bubble_flag');

		$resultArray['thumbCompleted'] = "0";
		if(!empty($bubbleCount) && !empty($userCount))
		{
			$resultArray['thumbCompleted'] = number_format(($bubbleCount/$userCount), 2);
		}



		/*DOT value complete*/
		$dotValueRatingCompletedUserArr = array();
		$dotValueRatingUpdatedUserArr   = array();

		foreach($users as $duValue)
		{

    //check DOT rating is given by user on every dot value if it is yes then true
			$dotPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);

			$status = $this->getDotPerformance($dotPerArr);

			if($status)
			{
				array_push($dotValueRatingCompletedUserArr, $duValue->id);      
			}

    //for updated
			$dotPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);

			$uStatus = $this->getDotPerformance($dotPerArr);

			if($uStatus)
			{
				array_push($dotValueRatingUpdatedUserArr, $duValue->id);        
			}
		}



        //for complete
		$dotValueRatingCompletedUserArrCount = count($dotValueRatingCompletedUserArr);

		$resultArray['dotCompleted'] = "0";
		if(!empty($dotValueRatingCompletedUserArrCount) && !empty($userCount))
		{
			$resultArray['dotCompleted'] = number_format(($dotValueRatingCompletedUserArrCount/$userCount)*100,2);
		}

        //for update
		$dotValueRatingUpdatedUserArrCount = count($dotValueRatingUpdatedUserArr);

		$resultArray['dotUpdated'] = "0";
		if(!empty($dotValueRatingUpdatedUserArrCount) && !empty($userCount))
		{
			$resultArray['dotUpdated'] = number_format(($dotValueRatingUpdatedUserArrCount/$userCount)*100,2);
		}



        //for team role map comple
		$teamPerArr = array('orgId' => $orgId,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
		$teamRoleStatus = $this->getTeamRoleMap($teamPerArr);

		$temRoleMapCount = count($teamRoleStatus);

		$resultArray['tealRoleMapCompleted'] = "0";
		if (!empty($temRoleMapCount) && !empty($userCount))
		{
			$resultArray['tealRoleMapCompleted'] = number_format(($temRoleMapCount/$userCount)*100,2);
		}

        //for team role map update
		$teamPerUpdateArr = array('orgId' => $orgId,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
		$teamRoleUpdateStatus = $this->getTeamRoleMap($teamPerUpdateArr);

		$temRoleMapUpdateCount = count($teamRoleUpdateStatus);

		$resultArray['tealRoleMapUpdated'] = "0";
		if (!empty($temRoleMapUpdateCount) && !empty($userCount))
		{
			$resultArray['tealRoleMapUpdated'] = number_format(($temRoleMapUpdateCount/$userCount)*100,2);
		}



        //for personality type
		$personalityPerArr = array('orgId' => $orgId,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
		$personalityTypeStatus = $this->getPersonalityType($personalityPerArr);

		$personalityTypeCount = count($personalityTypeStatus);

		$resultArray['personalityTypeCompleted'] = "0";
		if (!empty($personalityTypeCount) && !empty($userCount))
		{
			$resultArray['personalityTypeCompleted'] = number_format(($personalityTypeCount/$userCount)*100,2);
		}

        //for personality type updated
		$personalityUpdatePerArr = array('orgId' => $orgId,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
		$personalityTypeUpdateStatus = $this->getPersonalityType($personalityUpdatePerArr);

		$personalityTypeUpdateCount = count($personalityTypeUpdateStatus);

		$resultArray['personalityTypeUpdated'] = "0";
		if (!empty($personalityTypeUpdateCount) && !empty($userCount))
		{
			$resultArray['personalityTypeUpdated'] = number_format(($personalityTypeUpdateCount/$userCount)*100,2);
		}


        //for culture structure comleted        
		$cultureStructurePerArr = array('orgId' => $orgId,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
		$cultureStructureStatus = $this->getCultureStructure($cultureStructurePerArr);


		$cultureStructureCount = count($cultureStructureStatus);

		$resultArray['cultureStructureCompleted'] = "0";
		if (!empty($cultureStructureCount) && !empty($userCount))
		{
			$resultArray['cultureStructureCompleted'] = number_format(($cultureStructureCount/$userCount)*100,2);
		}


        //for personality type updated
		$cultureStructureUpdatePerArr = array('orgId' => $orgId,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
		$cultureStructureUpdateStatus = $this->getCultureStructure($cultureStructureUpdatePerArr);

		$cultureStructureUpdateCount = count($cultureStructureUpdateStatus);

		$resultArray['cultureStructureUpdated'] = "0";
		if (!empty($cultureStructureUpdateCount) && !empty($userCount))
		{
			$resultArray['cultureStructureUpdated'] = number_format(($cultureStructureUpdateCount/$userCount)*100,2);
		}



        //for motivation comleted       
		$motivationPerArr = array('orgId' => $orgId,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
		$motivationStatus = $this->getMotivation($motivationPerArr);

		$motivationCount = count($motivationStatus);

		$resultArray['motivationCompleted'] = "0";
		if (!empty($motivationCount) && !empty($userCount))
		{
			$resultArray['motivationCompleted'] = number_format(($motivationCount/$userCount)*100,2);
		}

        //for motivation type updated
		$motivationCountPerArr = array('orgId' => $orgId,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
		$motivationCountUpdateStatus = $this->getMotivation($motivationCountPerArr);

		$motivationCountUpdateCount = count($motivationCountUpdateStatus);

		$resultArray['motivationCountUpdated'] = "0";
		if (!empty($motivationCountUpdateCount) && !empty($userCount))
		{
			$resultArray['motivationCountUpdated'] = number_format(($motivationCountUpdateCount/$userCount)*100,2);
		}


		$data_array =  array("app_name"=>'Tribe365', "org_id"=> $orgId);

		$make_call = $this->getImprovements('POST', 'http://tellsid.softintelligence.co.uk/index.php/apitellsid/getIOTdetail', json_encode($data_array));

		$response = json_decode($make_call, true);

		$imprUserArray = array();
		foreach($response['response'] as $value)
		{           
			if($value['app_name']=='Tribe365')
			{
				$email = $value['email_id'];
				$user = DB::table('users')->where('id',$email)->where('status','Active')->first();

				$userOrgId ='';
				$userId ='';
				if($user)
				{
					$userOrgId = $user->orgId;
					$userId    = $user->id;
				}

				if($orgId)
				{               
					if($userOrgId==$orgId)
					{
						array_push($imprUserArray, $userId);
					}
				}
				else
				{
					array_push($imprUserArray, $userId);
				}
			}
		}

		$improvementsCount = count(array_unique($imprUserArray));

		$resultArray['improvements'] = "0";
		if(!empty($improvementsCount) && !empty($userCount))
		{
			$resultArray['improvements'] = number_format(($improvementsCount/$userCount),2);
		}

		return response()->json(['code'=>200,'status'=>true,'service_name'=>'get-performance','message'=>'','data'=>$resultArray]);

		// return view('admin/performance/performance',compact('resultArray','organisations','orgId','reportType','date'));
	}

    //check user given rating to the each value of dot
	protected function getDotPerformance($perArr = array())
	{

		$orgId    = $perArr["orgId"];
		$userId   = $perArr["userId"];
		$updateAt = $perArr["updateAt"];
		$currentDate = $perArr["currentDate"];
		$month        = $perArr["month"];
		$year         = $perArr["year"];

		if(!$orgId)
		{
			$user = DB::table('users')->where('status','Active')->where('id',$userId)->first();

			$orgId ='';
			if($user)
			{
				$orgId = $user->orgId;
			}           
		}

		$query = DB::table('dots_values')  
		->select('dots_values.id AS id')     
		->leftjoin('dots_beliefs','dots_beliefs.id','dots_values.beliefId')
		->leftjoin('dots','dots.id','dots_beliefs.dotId')
		->where('dots_beliefs.status','Active')
		->where('dots.status','Active')
		->where('dots_values.status','Active');

		if($orgId) 
		{
			$query->where('dots.orgId', $orgId);
		}

		$dotValues = $query->get();

		foreach($dotValues as $value)
		{
			$dvValue = DB::table('dot_values_ratings')
			->rightjoin('users','users.id','dot_values_ratings.userId')
			->where('users.status','Active')
			->where('dot_values_ratings.userId',$userId)
			->where('dot_values_ratings.valueId',$value->id)
			->where('dot_values_ratings.status','Active');

			if($updateAt)
			{
				$dvValue->whereNotNull('dot_values_ratings.updated_at');
				if(!empty($currentDate))
				{
					$dvValue->whereDate('dot_values_ratings.updated_at',$currentDate);

					$isDotValueRating = $dvValue->first();

					if($isDotValueRating)
					{
						return true;
					}
				}
				if(!empty($month) && !empty($year))
				{ 
					$dvValue->whereMonth('dot_values_ratings.updated_at', $month);
					$dvValue->whereYear('dot_values_ratings.updated_at', $year);

					$isDotValueRating = $dvValue->first();

					if($isDotValueRating)
					{
						return true;
					}
				}
			}
			else
			{

				if(!empty($month) && !empty($year))
				{ 
					$dvValue->whereMonth('dot_values_ratings.created_at', $month);
					$dvValue->whereYear('dot_values_ratings.created_at', $year);
				}

			}


			$isDotValueRating = $dvValue->first();

			if(!$isDotValueRating)
			{
				return false;
			}
		}

		return true;
	}

    //get team role performance
	protected function getTeamRoleMap($perArr = array())
	{
		$orgId    = $perArr['orgId'];
		$updateAt = $perArr["updateAt"];
		$currentDate = $perArr["currentDate"];
		$month        = $perArr["month"];
		$year         = $perArr["year"];


		$query = DB::table('cot_answers') 
		->select('cot_answers.userId')
		->distinct()
		->rightjoin('users','users.id','cot_answers.userId')
		->where('users.status','Active')
		->where('cot_answers.status','Active');

		if($orgId)
		{
			$query->where('cot_answers.orgId',$orgId);
		}

		if($updateAt)
		{
			$query->whereNotNull('cot_answers.updated_at');
			if(!empty($currentDate))
			{
				$query->whereDate('cot_answers.updated_at',$currentDate);
			}
			if(!empty($month) && !empty($year))
			{ 
				$query->whereMonth('cot_answers.updated_at', $month);
				$query->whereYear('cot_answers.updated_at', $year);
			}
		}
		else
		{

			if(!empty($month) && !empty($year))
			{ 
				$query->whereMonth('cot_answers.created_at', $month);
				$query->whereYear('cot_answers.created_at', $year);
			}

		}

		$result = $query->get();

		return $result;
	}

    //for personality type 
	protected function getPersonalityType($perArr = array())
	{

		$orgId    = $perArr['orgId'];
		$updateAt = $perArr["updateAt"];
		$currentDate = $perArr["currentDate"];
		$month        = $perArr["month"];
		$year         = $perArr["year"];

		$query = DB::table('cot_functional_lens_answers')
		->select('cot_functional_lens_answers.userId')
		->distinct()
		->rightjoin('users','users.id','cot_functional_lens_answers.userId')
		->where('users.status','Active')
		->where('cot_functional_lens_answers.status','Active');


		if($orgId)
		{
			$query->where('cot_functional_lens_answers.orgId',$orgId);
		}
		if($updateAt)
		{
			$query->whereNotNull('cot_functional_lens_answers.updated_at');
			if(!empty($currentDate))
			{
				$query->whereDate('cot_functional_lens_answers.updated_at',$currentDate);
			}
			if(!empty($month) && !empty($year))
			{ 
				$query->whereMonth('cot_functional_lens_answers.updated_at', $month);
				$query->whereYear('cot_functional_lens_answers.updated_at', $year);
			}
		}
		else
		{
			if(!empty($month) && !empty($year))
			{ 
				$query->whereMonth('cot_functional_lens_answers.created_at', $month);
				$query->whereYear('cot_functional_lens_answers.created_at', $year);
			}

		}

		$result = $query->get();

		return $result;
	}

    //for SOT culture structure 
	protected function getCultureStructure($perArr = array())
	{

		$orgId    = $perArr['orgId'];
		$updateAt = $perArr["updateAt"];
		$currentDate = $perArr["currentDate"];
		$month        = $perArr["month"];
		$year         = $perArr["year"];

		$query = DB::table('sot_answers')   
		->select('sot_answers.userId')
		->distinct()      
		->leftJoin('users','users.id','=','sot_answers.userId')
		->where('sot_answers.status','Active')
		->where('users.status','Active');     

		if($orgId)
		{
			$query->where('users.orgId',$orgId);
		}

		if($updateAt)
		{
			$query->whereNotNull('sot_answers.updated_at');
			if(!empty($currentDate))
			{
				$query->whereDate('sot_answers.updated_at',$currentDate);
			}
			if(!empty($month) && !empty($year))
			{ 
				$query->whereMonth('sot_answers.updated_at', $month);
				$query->whereYear('sot_answers.updated_at', $year);
			}
		}
		else
		{     
			if(!empty($month) && !empty($year))
			{ 
				$query->whereMonth('sot_answers.created_at', $month);
				$query->whereYear('sot_answers.created_at', $year);
			}
		}

		$result = $query->get();

		return $result;
	}

    //for SOT culture structure 
	protected function getMotivation($perArr = array())
	{

		$orgId    = $perArr['orgId'];
		$updateAt = $perArr["updateAt"];
		$currentDate = $perArr["currentDate"];
		$month        = $perArr["month"];
		$year         = $perArr["year"];

		$query = DB::table('sot_motivation_answers')
		->select('sot_motivation_answers.userId')
		->distinct()      
		->leftJoin('users','users.id','=','sot_motivation_answers.userId')
		->where('sot_motivation_answers.status','Active')
		->where('users.status','Active');     


		if($orgId)
		{
			$query->where('sot_motivation_answers.orgId',$orgId);
		}
		if($updateAt)
		{
			$query->whereNotNull('sot_motivation_answers.updated_at');
			if(!empty($currentDate))
			{
				$query->whereDate('sot_motivation_answers.updated_at',$currentDate);
			}
			if(!empty($month) && !empty($year))
			{ 
				$query->whereMonth('sot_motivation_answers.updated_at', $month);
				$query->whereYear('sot_motivation_answers.updated_at', $year);
			}
		}
		else
		{

			if(!empty($month) && !empty($year))
			{ 
				$query->whereMonth('sot_motivation_answers.created_at', $month);
				$query->whereYear('sot_motivation_answers.created_at', $year);
			}
		}

		$result = $query->get();

		return $result;
	}

    //for improvements 

	function getImprovements($method, $url, $data){
		$curl = curl_init();

		switch ($method)
		{
			case "POST":
			curl_setopt($curl, CURLOPT_POST, 1);
			if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			break;
			case "PUT":
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
			if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);                              
			break;
			default:
			if ($data)
				$url = sprintf("%s?%s", $url, http_build_query($data));
		}

        // OPTIONS:
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'APIKEY: 111111111111111111111',
			'Content-Type: application/json',
		));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
		$result = curl_exec($curl);
		if(!$result){die("Connection Failure");}
		curl_close($curl);
		return $result;
	}


}
