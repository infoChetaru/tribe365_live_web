<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use DB;
use Mail;
use URL;

class ApiIOTController extends Controller{

	/*post feedback*/
	public function postFeedback(){
		$resultArray = array();

		$message = Input::get('message');
		$userId  = Input::get('userId');
		$orgId   = Input::get('orgId');
		$image   = Input::get('image');

		$insertArray['message'] = $message;
		$insertArray['userId']  = $userId;
		$insertArray['orgId']   = $orgId;
		
		if($image){			
			$image = str_replace('data:image/png;base64,', '', $image);
			$image = str_replace(' ', '+', $image);
			$imageName = 'iotFeedback_'.time().'.png'; 
			\File::put(public_path('uploads/iot_files/'). $imageName, base64_decode($image));
			$insertArray['image'] = $imageName;			
		} 

		$EmailMsg = $message;
		if($image){		
			$EmailMsg.= '<br /><img src='.URL::to('public/uploads/iot_files/'. $imageName).'>';
		}


		//Get user info
		$getUserInfo = DB::table('users')
		->where('id',$userId)
		->first();


		//Send mail
		// $email ='pooja@chetaru.com';    //For dev
		$email ='team@tribe365.co';   //For live

		$emailMessage = "You have received a new feedback from <b>".$getUserInfo->email."</b>.<br /><br />";
		$emailMessage.=  "<b>Feedback: </b><br />";
		$emailMessage.=  $EmailMsg;

		$data = ['msg'=>$emailMessage];
		$te= Mail::send('layouts/PostFeedbacknotificationMail', $data, function($message) use($email){
		    $message->from('notification@tribe365.co', 'Tribe365');
		    $message->to($email);
		    $message->subject('Tribe365: Feedback Received');
		});
		//---------------------------------------------------------------------------------

		$insertGetId = DB::table('iot_feedbacks')->insertGetId($insertArray);
		return response()->json(['code'=>200,'status'=>true,'service_name'=>'iot-post-feedback','message'=>'Record added successfully.','data'=>$resultArray]);
	}

	/*get feedback detail*/
	public function getFeedbackDetail(){
		$resultArray = array();

		$userId = Input::get('userId');

		$feedbackTbl = DB::table('iot_feedbacks')
		->where('userId',$userId)	
		->where('status','!=','Inactive')
		->orderBy('id','ASC')
		->get();

		$feedbackArr = array();
		foreach($feedbackTbl as $value)	{
			$result['id']        = $value->id;
			$result['message']   = $value->message;
			$result['image']     = '';
			$result['createdAt'] = $value->created_at;

			if($value->image){				
				$result["image"] = url('public/uploads/iot_files').'/'. $value->image;
			}

   	        //get messages
			$messages = DB::table('iot_messages')
			->select('iot_messages.id','iot_messages.sendTo','iot_messages.sendFrom','users.roleId','users.name','iot_messages.message','users.imageUrl','iot_messages.created_at','iot_messages.file')->leftJoin('users','users.id','iot_messages.sendFrom')->where('iot_messages.feedbackId', $value->id)->where('iot_messages.status','Active')->orderBy('iot_messages.id','ASC')->get();

			$msgArr = array();
			foreach($messages as $mValue){
				$msgResult["id"]           = $mValue->id;
				$msgResult["sendTo"]       = $mValue->sendTo;
				$msgResult["sendFrom"]     = $mValue->sendFrom;
				$msgResult["name"]         = $mValue->name;
				$msgResult["message"]      = $mValue->message;			
				$msgResult["created_at"]   = $mValue->created_at;
				$msgResult["userImageUrl"] = '';
				$msgResult["msgImageUrl"]  = '';
				$msgResult["userType"]     = '';

				if($mValue->roleId == 1) {
					$msgResult["userType"] = 'Admin';
				}else{
					$msgResult["userType"] = 'User';
				}

				if($mValue->imageUrl){
					$msgResult["userImageUrl"] = url('public/uploads/user_profile').'/'. $mValue->imageUrl;
				}

				if($mValue->file){
					$msgResult["msgImageUrl"] = url('public/uploads/iot_files').'/'. $mValue->file;
				}

				array_push($msgArr, $msgResult);
			}

			$result['messages'] = $msgArr;			
			array_push($feedbackArr, $result); 
		}
		return response()->json(['code'=>200,'status'=>true,'service_name'=>'iot-get-feedback-detail','message'=>'','data'=>$feedbackArr]);
	}

	/*send chat msg*/
	public function iotSendMsg(){
		$resultArray = array();

		$sendFrom   = Input::get('sendFrom');
		$sendTo     = Input::get('sendTo');
		$message    = Input::get('message');//msg or base64 image code
		$feedbackId = Input::get('feedbackId');
		$postType   = Input::get('postType'); //img, msg

		$insertArray['sendFrom']   = $sendFrom;
		$insertArray['sendTo']     = $sendTo;		
		$insertArray['feedbackId'] = $feedbackId;
		$insertArray['status']     = 'Active';
		$insertArray['created_at'] = date('Y-m-d H:i:s');

		if($postType == 'msg'){
			$insertArray['message'] = $message;			
		}else if($postType == 'img'){
			$image = str_replace('data:image/png;base64,', '', $message);
			$image = str_replace(' ', '+', $image);
			$imageName = 'iot_'.time().'.png'; 
			\File::put(public_path('uploads/iot_files/'). $imageName, base64_decode($image));

			$insertArray['file'] = $imageName;
		}


		if($postType == 'msg'){
			$EmailMsg = $message;
		}else if($postType == 'img'){
			$EmailMsg = '<br /><img src='.URL::to('public/uploads/iot_files/'. $imageName).'>';
		}	
		
		//Get user info
		$getUserInfo = DB::table('users')
		->where('id',$sendFrom)
		->first();

		//Get user info
		$getfeedbackdInfo = DB::table('iot_feedbacks')
		->where('id',$feedbackId)
		->first();


		//Send mail
		$email ='team@tribe365.co';    
		$emailMessage = "You have received a new chat message from <b>".$getUserInfo->email."</b>.<br /><br />";

		$emailMessage.= "<b>Feedback: </b><br />";
		$emailMessage.= $getfeedbackdInfo->message."<br /><br />";

		$emailMessage.= "<b>Message: </b><br />";
		$emailMessage.= $EmailMsg;

		$data = ['msg'=>$emailMessage];

		$te= Mail::send('layouts/IOTSentMsgMail', $data, function($message) use($email){
		    $message->from('notification@tribe365.co', 'Tribe365');
		    $message->to($email);
		    $message->subject('Tribe365: Chat Message Received');
		});
		//---------------------------------------------------------------------------------




		$insertGetId = DB::table('iot_messages')->insertGetId($insertArray);
		$messages = DB::table('iot_messages')
		->select('iot_messages.id','iot_messages.sendTo','iot_messages.sendFrom','users.roleId','users.name','iot_messages.message','users.imageUrl','iot_messages.created_at','iot_messages.file')->leftJoin('users','users.id','iot_messages.sendFrom')->where('iot_messages.feedbackId',$feedbackId)->where('iot_messages.status','Active')->orderBy('iot_messages.id','ASC')->get();

		$msgArr = array();
		foreach($messages as $mValue){
			$msgResult["id"] = $mValue->id;
			$msgResult["sendTo"] = $mValue->sendTo;
			$msgResult["sendFrom"] = $mValue->sendFrom;
			$msgResult["name"] = $mValue->name;
			$msgResult["message"] = $mValue->message;			
			$msgResult["created_at"] = $mValue->created_at;
			$msgResult["userImageUrl"] = '';
			$msgResult["msgImageUrl"]  = '';
			$msgResult["userType"]     = '';
			
			if($mValue->roleId == 1){
				$msgResult["userType"] = 'Admin';
			}else{
				$msgResult["userType"] = 'User';
			}

			if($mValue->imageUrl){
				$msgResult["userImageUrl"] = url('public/uploads/user_profile').'/'. $mValue->imageUrl;
			}

			if($mValue->file){
				$msgResult["msgImageUrl"] = url('public/uploads/iot_files').'/'. $mValue->file;
			}			
			array_push($msgArr, $msgResult);
		}

		$resultArray['messages'] = $msgArr;
		return response()->json(['code'=>200,'status'=>true,'service_name'=>'iot-send-msg','message'=>'','data'=>$resultArray]);
	}

	/*get chat inbox list*/
	public function getInboxChatList(){
		$userId = Input::get('userId');

		$feedbackInbox = DB::table('iot_feedbacks')
		->select('id','message' )
		->where('userId',$userId)	
		->where('status','!=','Inactive')
		->orderBy('id','ASC')

		->whereIn('id', function($query)
		{
			$query->select('feedbackId')->from('iot_messages');
		})->get();


		$inboxArr = array();
		foreach ($feedbackInbox as $value){

			$result['id']           = $value->id;
			$result['feedback_msg'] = $value->message;
			$result['message']      = '';
			$result['date']         = '';
			$result['image']        = false;

			$lastMessages = DB::table('iot_messages')
			->select('message','file','created_at')
			->where('feedbackId',$value->id)
			->where('status','Active')
			->orderBy('id','DESC')->first();

			if($lastMessages){
				$result['message'] = $lastMessages->message;
				$result['date']    = $lastMessages->created_at;

				if($lastMessages->file){
					$result['image'] = true;
				}
			}
			array_push($inboxArr, $result);
		}


		$resultArray['inbox'] = $inboxArr;		
		return response()->json(['code'=>200,'status'=>true,'service_name'=>'iot-inbox-list','message'=>'','data'=>$resultArray]);
	}

	/*get chatting massages*/	
	public function getChatMessages(Request $request){
		$feedbackId = Input::get('feedbackId');

		$users = Auth::user();

       //get messages
		$messages = DB::table('iot_messages')
		->select('iot_messages.id','iot_messages.sendTo','iot_messages.sendFrom','users.roleId','users.name','iot_messages.message','users.imageUrl','iot_messages.created_at','iot_messages.file')->leftJoin('users','users.id','iot_messages.sendFrom')->where('iot_messages.feedbackId',$feedbackId)->where('iot_messages.status','Active')->orderBy('iot_messages.id','ASC')->get();

		$msgArr = array();
		foreach($messages as $mValue){
			$msgResult["id"]           = $mValue->id;
			$msgResult["sendTo"]       = $mValue->sendTo;
			$msgResult["sendFrom"]     = $mValue->sendFrom;
			$msgResult["name"]         = $mValue->name;
			$msgResult["message"]      = $mValue->message;			
			$msgResult["created_at"]   = $mValue->created_at;
			$msgResult["userImageUrl"] = '';
			$msgResult["msgImageUrl"]  = '';
			$msgResult["userType"]     = '';
			
			if($mValue->roleId == 1) {
				$msgResult["userType"] = 'Admin';
			}else{
				$msgResult["userType"] = 'User';
			}

			if($mValue->imageUrl){
				$msgResult["userImageUrl"] = url('public/uploads/user_profile').'/'. $mValue->imageUrl;
			}

			if($mValue->file){
				$msgResult["msgImageUrl"] = url('public/uploads/iot_files').'/'. $mValue->file;
			}

			array_push($msgArr, $msgResult);
		}

		$resultArray['messages'] = $msgArr;
		return response()->json(['code'=>200,'status'=>true,'service_name'=>'iot-get-msg','message'=>'','data'=>$resultArray]);
	}

	/*get theme list*/
	public function getThemeList(){
		$resultArray = array();
		$orgId = Input::get('orgId');
		$themListTbl = DB::table('iot_themes')->select('id','title')->where('orgId',$orgId)->where('status','Active')->get();

		$themeArr = array();
		foreach($themListTbl as $value) {
			$result['id']    = $value->id;
			$result['title'] = $value->title;

			array_push($themeArr, $result);
		}
		$resultArray['themeList'] = $themeArr;
		return response()->json(['code'=>200,'status'=>true,'service_name'=>'iot-get-theme-list','message'=>'','data'=>$resultArray]);
	}
}
