<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use DB;


class ApiTribeometerController extends Controller
{
	public function getTribeometerQuestionList()
	{
		$resultArray = array();

		$questionTbl = DB::table('tribeometer_questions')->select('id AS questionId','question')->where('status','Active')->get();

		foreach ($questionTbl as $value)
		{
			$options = DB::table('tribeometer_question_options')->select('id AS optionId','option_name AS optionName')->where('status','Active')->get();

			$value->options = $options;

			array_push($resultArray, $value);
		}

		return response()->json(['code'=>200,'status'=>true,'service_name'=>'Tribeometer-questions-list','message'=>'Tribeometer questionslist','data'=>$resultArray]);

	}
	/*add diagnostic answers*/
	public function addTribeometerAnswers()
	{

		$resultArray = array();

		$insertId  = '';

		$optionArr = Input::get('answer');
		$user      = Auth::user();
		$userId    = $user->id; 
		$orgId     = $user->orgId;

		foreach ($optionArr as  $value)
		{
			$optionRatingTbl = DB::table('tribeometer_question_options')->select('option_rating')->where('id',$value['optionId'])->first();

			$optionRating = 0;
			if (!empty($optionRatingTbl))
			{
				$optionRating = $optionRatingTbl->option_rating;
			}

			
			$insertArray = array(
				'userId'                 => $userId, 
				'orgId'                  => $orgId,
				'questionId'             => $value['questionId'],                
				'optionId'               => $value['optionId'],
				'answer'                 => $optionRating,            
				'status'                 => 'Active',
				'created_at'             => date('Y-m-d H:i:s'),
			);

			$insertId = DB::table('tribeometer_answers')->insertGetId($insertArray);
		} 

		if($insertId)
		{
			return response()->json(['code'=>200,'status'=>true,'service_name'=>'Add-tribeometer-answer','message'=>'Tribeometer answer added successfully.','data'=>$resultArray]);
		}

		return response()->json(['code'=>400,'status'=>false,'service_name'=>'Add-tribeometer-answer','message'=>'Tribeometer answer added successfully.','data'=>$resultArray]);
	}

	/*get Tribeometer completed answers*/
	public function getTribeometerCompletedAnswers()
	{
		$resultArray = array();

		$user = Auth::user();
		$userId = $user->id;

		$ans = DB::table('tribeometer_answers')->where('userId',$userId)->first();

		if (empty($ans)) 
		{
			return response()->json(['code'=>400,'status'=>false,'service_name'=>'Tribeometer-question-answers','message'=>'Tribeometer answers not filled.','data'=>[]]);
		}
		

		
		$questionTbl = DB::table('tribeometer_questions')->select('id as questionId','question')->where('status','Active')->get();

		$queArray = array();

		foreach ($questionTbl as $value)
		{

			$optionsTbl = DB::table('tribeometer_question_options')->select('id AS optionId','option_name AS optionName')->where('status','Active')->get();

			$value->options = array();
			foreach ($optionsTbl as $optValue)
			{
				$diaAns = DB::table('tribeometer_answers')->where('questionId',$value->questionId)->where('userId',$userId)->where('optionId',$optValue->optionId)->first();

				$optValue->isChecked = false;
				if($diaAns)
				{
					$optValue->isChecked = true;
					$optValue->answerId = $diaAns->id;
					$value->answerId = $diaAns->id;
				}


				array_push($value->options, $optValue);
			}			

			array_push($queArray, $value);
		}		

		return response()->json(['code'=>200,'status'=>true,'service_name'=>'Tribeometer-question-answers','message'=>'Tribeometer question answers.','data'=>$queArray]);
	}


	public function updateTribeometerAnswers()
	{
		$resultArray = array();
		$optionArr = Input::get('answer');
		$user      = Auth::user();
		$userId    = $user->id; 

		foreach ($optionArr as  $value)
		{
			$optionRatingTbl = DB::table('tribeometer_question_options')->select('option_rating')->where('id',$value['optionId'])->first();

			$optionRating = 0;
			if (!empty($optionRatingTbl))
			{
				$optionRating = $optionRatingTbl->option_rating;
			}

			$updateArray = array(              
				'optionId'               => $value['optionId'],
				'answer'                 => $optionRating,
				'updated_at'             => date('Y-m-d H:i:s'),
			);

			
			DB::table('tribeometer_answers')->where('id',$value['answerId'])->where('userId',$userId)->update($updateArray);
		} 
		return response()->json(['code'=>200,'status'=>true,'service_name'=>'Update-tribeometer-answer','message'=>'Tribeometer answer updated successfully.','data'=>$resultArray]);
	}


	/*get diagnostic reports*/
	public function getTribeometerReport()
	{

		$resultArray = array();

		$orgId = Input::get('orgId');

		$queCatTbl = DB::table('tribeometer_questions_category')->get();

		//get user list
		// $users = DB::table('users')->where('orgId',$orgId)->where('status','Active')->get();

		$users = DB::table('tribeometer_answers')
		->leftjoin('users','users.id','tribeometer_answers.userId')
		->select('tribeometer_answers.userId')
		->where('users.status','Active')
		->groupBy('tribeometer_answers.userId')
		->where('users.orgId',$orgId)
		->get();
		
		$userCount = count($users);

		if(empty($userCount))
		{
			return response()->json(['code'=>400,'status'=>false,'service_name'=>'tribeometer-report','message'=>'Tribeometer answers not done yet.','data'=>$resultArray]);
		}		


		$optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
		$optCount = count($optionsTbl)-1; 
		
		foreach ($queCatTbl as $value)
		{
			$questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();

			$quecount = count($questionTbl);

			$perQuePercen = 0;
			foreach ($questionTbl as  $queValue)
			{
				$diaAnsTbl = DB::table('tribeometer_answers')			
				->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
				->leftJoin('users','users.id','tribeometer_answers.userId')
				->where('users.status','Active')
				->where('tribeometer_answers.orgId',$orgId)
				->where('tribeometer_questions.id',$queValue->id)
				->where('tribeometer_questions.category_id',$value->id)
				->sum('answer');

                //avg of all questions
				$perQuePercen += ($diaAnsTbl/$userCount);				
			}

			$score = ($perQuePercen/($quecount*$optCount));
			$totalPercentage = ($perQuePercen/($quecount*$optCount))*100;

			$value1['title']      =  $value->title;
			$value1['score']      =  number_format((float)$score, 2, '.', '');
			$value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');			

			array_push($resultArray, $value1);
		}

		return response()->json(['code'=>200,'status'=>true,'service_name'=>'tribeometer-report','message'=>'','data'=>$resultArray]);

	}

}
