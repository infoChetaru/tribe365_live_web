<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use DB;


class ApiActionController extends Controller
{
	/*get action tier list*/
	public function actionTierList()
	{

		$resultArray = array();

		$user = Auth::user();

		if($user->roleId==3)
		{
			$resultArray = array( array(
				"id" => 6,
				"name" => strtoupper("Individual")
			));
		}
		else
		{

			$tierList = DB::table('actionResponsibleStatus')
			->select('id','name')
			->where('status','Active')
			->orderBy('id','ASC')
			->get();

			foreach ($tierList as  $value) {
				
				$result['id']   = $value->id;
				$result['name'] = strtoupper($value->name);

				array_push($resultArray, $result);
			}

		}
		return response()->json(['code'=>200,'status'=>true,'service_name'=>'action-tier-list','message'=>'Action tier list','data'=>$resultArray]);
	}

	/*add action*/
	public function addAction()
	{		
		
		$resultArray = array();

		$startedDate = strtotime(Input::get('startedDate'));
		$dueDate     = strtotime(Input::get('dueDate'));

		if($dueDate < $startedDate)
		{
			return response()->json(['code'=>400,'status'=>false,'service_name'=>'Add-action','message'=>'Start date more than End date','data'=>$resultArray]);
		}
		
		$insertArray['userId']         = Input::get('userId');  
		$insertArray['description']    = Input::get('description');
		$insertArray['startedDate']    = date('Y-m-d H:i:s', strtotime(Input::get('startedDate')));		
		$insertArray['dueDate']        = date('Y-m-d H:i:s', strtotime(Input::get('dueDate')));
		$insertArray['responsibleId']  = Input::get('tierId');	
		$insertArray['orgStatus']      = Input::get('orgStatus');
		$insertArray['orgId']          = Input::get('orgId');
		$insertArray['status']         = 'Active';
		$insertArray['created_at']     = date('Y-m-d H:i:s');
		$insertArray['forUserId']      = Input::get('IndividualUserId');
		$insertArray['officeId']       = Input::get('officeId');   
		$insertArray['departmentId']   = Input::get('departmentId');
		$insertArray['responsibleUserId'] = Input::get('responsibleUserId');
		$insertArray['themeId']           = json_encode(Input::get('themeId'));

		$status = DB::table('actions')->insertGetId($insertArray);

		 //send notification mail
		$user = Auth::user();

		$organisation = DB::table('organisations')->select('organisation')->where('id',$user->orgId)->first();

		$orgName ='';
		if($organisation)
		{
			$orgName = $organisation->organisation;
		}


		$msg = '<strong>'.ucfirst($user->name).'</strong>'.' added a new  <strong>action</strong> for Organisation: <strong>'.$orgName.'</strong>';

		$notificationArray = array('msg'=>$msg);

		app('App\Http\Controllers\Admin\CommonController')->sendNotificationMail($notificationArray);
		
		return response()->json(['code'=>200,'status'=>true,'service_name'=>'Add-action','message'=>'Added successfully','data'=>$resultArray]);
		
	}


	/*get actions list*/
	public function getActionList()
	{

		$resultArray = array();

		$orgId = Input::get('orgId');

		$user = Auth::user();
		
		if($user->roleId == 3)
		{			
			$actionsList = DB::table('actions')
			->select('actions.themeId','actions.id','actions.userId','actions.description','actions.startedDate','actions.dueDate','actions.responsibleId','actions.orgStatus','actions.orgId','users.name','offices.office','offices.id AS officeId','all_department.department','ruser.name as rname','actionResponsibleStatus.name AS tier','actions.responsibleId','departments.id AS departmentId','actions.responsibleUserId')

			->leftJoin('users','users.id', '=', 'actions.userId')	
			->leftJoin('users as ruser','ruser.id', '=', 'actions.responsibleUserId')	
			->leftJoin('actionResponsibleStatus','actionResponsibleStatus.id', '=', 'actions.responsibleId')   

			->leftJoin('offices','offices.id', '=', 'actions.officeId')			
			->leftJoin('departments','departments.id', '=', 'actions.departmentId')	
			->leftJoin('all_department','all_department.id', '=', 'departments.departmentId')

			->where('actions.responsibleUserId', $user->id)
			->orWhere('actions.userId', $user->id)		
			->where('actions.orgId', $orgId)	
			->where('actions.status', 'Active')
			->orderBy('actions.id','DESC')
			->get();

		}
		else if($user->roleId == 2)
		{

			$actionsList = DB::table('actions')
			->select('actions.themeId','actions.id','actions.userId','actions.description','actions.startedDate','actions.dueDate','actions.responsibleId','actions.orgStatus','actions.orgId','users.name','ruser.name as rname','actionResponsibleStatus.name AS tier','actions.responsibleId','actions.responsibleUserId')
			->leftJoin('users','users.id', '=', 'actions.userId')
			->leftJoin('users as ruser','ruser.id', '=', 'actions.responsibleUserId')	
			->leftJoin('actionResponsibleStatus','actionResponsibleStatus.id', '=', 'actions.responsibleId')   	
			->where('actions.responsibleUserId', $user->id)
			->orWhere('actions.userId', $user->id)	
			->where('actions.orgId', $orgId)
			->where('actions.status', 'Active')			
			->orderBy('actions.id','DESC')
			->get();	

		} 
		else 
		{

			$actionsList = DB::table('actions')
			->select('actions.themeId','actions.id','actions.userId','actions.description','actions.startedDate','actions.dueDate','actions.responsibleId','actions.orgStatus','actions.orgId','users.name','offices.office','offices.id AS officeId','all_department.department','ruser.name as rname','actionResponsibleStatus.name AS tier','actions.responsibleId','departments.id AS departmentId','actions.responsibleUserId')

			->leftJoin('users','users.id', '=', 'actions.userId')
			->leftJoin('users as ruser','ruser.id', '=', 'actions.responsibleUserId')		
			->leftJoin('offices','offices.id', '=', 'actions.officeId')			
			->leftJoin('departments','departments.id', '=', 'actions.departmentId')	
			->leftJoin('all_department','all_department.id', '=', 'departments.departmentId')	
			->leftJoin('actionResponsibleStatus','actionResponsibleStatus.id', '=', 'actions.responsibleId')   
			->where('actions.orgId', $orgId)
			->where('actions.status', 'Active')			
			->orderBy('actions.id','DESC')
			->get();

		}
		
		foreach ($actionsList as $key => $value) 
		{
			$result['id']           	= (string)$value->id;
			$result["userId"]       	= (string)$value->userId;
			$result["tier"]       	    = (string)$value->tier;
			$result["tierId"]           = (string)$value->responsibleId;
			$result["responsibleName"]  = (string)$value->rname;
			$result["responsibleUserId"]= (string)$value->responsibleUserId;
			$result["description"]  	= (string)$value->description;
			$result["startedDate"]  	= date('d-m-Y', strtotime($value->startedDate));
			$result["dueDate"]      	= date('d-m-Y', strtotime($value->dueDate));
			// $result["responsibleId"]= $value->responsibleId;
			$result["orgStatus"]    	= (string)strtoupper($value->orgStatus);
			$result["orgId"]        	= (string)$value->orgId;

			$result['offDeptName']='';
			$result['offDeptId'] = '';
			if(!empty($value->office) && $value->responsibleId==4)
			{
				$result['offDeptName']= (string)$value->office;
				$result['offDeptId']  = (string)$value->officeId;
			}
			else if(!empty($value->department) && $value->responsibleId==5)
			{
				$result["offDeptName"] = (string)$value->department;
				$result['offDeptId']   = (string)$value->departmentId;
			}
			//assignee
			$result["name"]   = (string)$value->name;

			//get themes assign to actions
			$themeIdArr = json_decode($value->themeId);
			
			$themesArr  = array();
			if($themeIdArr)
			{				
				foreach($themeIdArr as $themeId) 
				{
					$theme = DB::table('iot_themes')->where('id',$themeId)->where('status','Active')->first();

					if($theme)
					{
						$tresult['id']    = $theme->id;
						$tresult['title'] = $theme->title; 

						array_push($themesArr, $tresult);
					}
				}				
			}	

			$result['themes'] = $themesArr;		

			array_push($resultArray, $result);
		}

		return response()->json(['code'=>200,'status'=>true,'service_name'=>'action-list','message'=>'Actions list','data'=>$resultArray]);

	}
	/*do comments*/
	public function addComment()
	{		
		$user = Auth::user();
		$insertArray['actionId']       = Input::get('actionId');  
		$insertArray['userId']         = $user->id;  
		$insertArray['comment']    	   = Input::get('comment');
		$insertArray['created_at']     = date('Y-m-d H:i:s');


		$status = DB::table('actions_comment')->insertGetId($insertArray);
		$name = DB::table('users')->select('name')->where('id',$user->id)->first();
		$insertArray['name'] = $name->name;


		$user = Auth::user();

		$organisation = DB::table('organisations')->select('organisation')->where('id',$user->orgId)->first();

		$orgName ='';
		if($organisation){
			$orgName = $organisation->organisation;
		}
		
		if($status)
		{
			//send notification mail
			$user = Auth::user();

			$msg = '<strong>'.ucfirst($user->name).'</strong> commented on action of Organisation: <strong>'.$orgName.'</strong>';
			
			$notificationArray = array('msg'=>$msg);
			app('App\Http\Controllers\Admin\CommonController')->sendNotificationMail($notificationArray);

			return response()->json(['code'=>200,'status'=>true,'service_name'=>'Add-action-comment','message'=>'Added successfully','data'=>$insertArray]);
		}
		
	}
	/*get list of actions*/
	public function ListComment()
	{		
		
		$actionId     = Input::get('actionId');  
		

		$actionsComment = DB::table('actions_comment as AC')
		->select('AC.id','AC.userId','AC.comment','AC.created_at','users.name')
		->leftJoin('users','users.id', '=', 'AC.userId')		
		->where('AC.actionId',$actionId)
		->orderBy('AC.created_at','DESC')
		->get();
		if($actionsComment)
		{
			return response()->json(['code'=>200,'status'=>true,'service_name'=>'List-action-comment','message'=>'Action List','data'=>$actionsComment]);
		}
		
	}
	/*update status of the action*/
	public function updateStatus()
	{		
		$user                    = Auth::user();
		$actionId                = Input::get('actionId');
		$insertArray['orgStatus']= Input::get('orgStatus');  
		
		
		DB::table('actions')->where('id', $actionId)->update($insertArray);

		$actions = DB::table('actions')->select('orgStatus')->where('id',$actionId)->first();    
		
		$user = Auth::user();

		$organisation = DB::table('organisations')->select('organisation')->where('id',$user->orgId)->first();

		$orgName ='';
		if($organisation){
			$orgName = $organisation->organisation;
		}

		//send notification mail
		$user = Auth::user();

		$msg = '<p><strong>'.ucfirst($user->name).'</strong>'.' changed the status of <strong>action</strong> for Organisation: <strong>'.$orgName.'</strong>';

		$notificationArray = array('msg'=>$msg);

		app('App\Http\Controllers\Admin\CommonController')->sendNotificationMail($notificationArray);


		return response()->json(['code'=>200,'status'=>true,'service_name'=>'Update-action-status','message'=>'Updated successfully','data'=>$actions]);
		
		
	}


	/*update action detail*/
	public function updateAction()
	{

		$resultArray = array();

		$startedDate = strtotime(Input::get('startedDate'));
		$dueDate     = strtotime(Input::get('dueDate'));

		if($dueDate < $startedDate)
		{

			return response()->json(['code'=>400,'status'=>false,'service_name'=>'Updated-action','message'=>'Start date more than End date','data'=>$resultArray]);
		}

		$actionId   	   = Input::get('actionId');

		$insertArray['userId']         = Input::get('userId');  
		$insertArray['description']    = Input::get('description');		
		$insertArray['responsibleId']  = Input::get('tierId');	
		$insertArray['orgStatus']      = Input::get('orgStatus');
		$insertArray['orgId']          = Input::get('orgId');
		$insertArray['status']         = 'Active';		
		$insertArray['forUserId']      = Input::get('IndividualUserId');
		$insertArray['officeId']       = Input::get('officeId');   
		$insertArray['departmentId']   = Input::get('departmentId');
		$insertArray['responsibleUserId'] = Input::get('responsibleUserId');
		$insertArray['startedDate']    = date('Y-m-d H:i:s', strtotime(Input::get('startedDate')));		
		$insertArray['dueDate']        = date('Y-m-d H:i:s', strtotime(Input::get('dueDate')));
		$insertArray['updated_at']     = date('Y-m-d H:i:s');
		$insertArray['themeId']           = json_encode(Input::get('themeId'));
		DB::table('actions')->where('id', $actionId)->update($insertArray);
		
		return response()->json(['code'=>200,'status'=>true,'service_name'=>'Updated-action','message'=>'Updated successfully.','data'=>$resultArray]);

	}

	/*delete action*/
	public function deleteAction()
	{

		$resultArray = array();

		$actionId     = Input::get('actionId');

		$insertArray['status']     = "Inactive";

		DB::table('actions')->where('id', $actionId)->update($insertArray);

		return response()->json(['code'=>200,'status'=>true,'service_name'=>'Delete-action','message'=>'Action deleted successfully.','data'=>$resultArray]);
	}

	/*get action count*/
	public function getActionStatusCount()
	{
		$resultArray = array();

		$userId = Input::get('userId');
		$orgId  = Input::get('orgId');	
		
		$user = Auth::user();
		
		$wrArray = array();
		if ($user->roleId !=1) 
		{
			$wrArray = array('userId'=>$userId);
		}

		$actionStarted = DB::table('actions')->where('status','Active')->Where('orgStatus','Started')->where('orgId',$orgId)->where($wrArray)->get();

		$actionNotStarted = DB::table('actions')->where('status','Active')->where('orgStatus','Not Started')->where('orgId',$orgId)->where($wrArray)->get();

		$actionCompleted = DB::table('actions')->where('status','Active')->Where('orgStatus','Completed')->where('orgId',$orgId)->where($wrArray)->get();

		$resultArray['statusStarted']    = count($actionStarted);
		$resultArray['statusNotStarted'] = count($actionNotStarted);
		$resultArray['statusCompleted']  = count($actionCompleted);

		return response()->json(['code'=>200,'status'=>true,'service_name'=>'action-count','message'=>'','data'=>$resultArray]);
	}
}
