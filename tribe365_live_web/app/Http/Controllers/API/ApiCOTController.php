<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use DB;
use Mail;

class ApiCOTController extends Controller
{
  public $successCode = 200;
  public $errorCode   = 400;

  public function getCOTQuestions()
  {
    $resultArray = array();

    $questions = DB::table('cot_questions')->select('id AS questionId','question AS questionName')->where('status','Active')->get();

    foreach ($questions as $queValue) 
    {
      $cotOptions = DB::table('cot_options')->select('id AS OptionId','option_name AS option','maper AS roleMapId')
      ->where('cot_question_id',$queValue->questionId)
      ->where('status','Active')->get();

      $optArray = array();

      foreach ($cotOptions as $ansValue)
      {
        array_push($optArray, $ansValue);
      }

      $queValue->option = $optArray;

      array_push($resultArray, $queValue);
    }

    return response()->json(['code'=>200,'status'=>true,'service_name'=>'get-cot-questions','message'=>'cot question list.','data'=>$resultArray]);
  }

  /*add cot answer*/
  public function addCOTAnswer()
  {
    $resultArray = array();

    $optionArr = Input::get('answer');
    $insertId ='';
    for ($i=0; $i < count($optionArr); $i++)
    { 

      foreach ($optionArr[$i]['option'] as  $value)
      {
        $insertArray = array(
          'userId'                 => Input::get('userId'), 
          'orgId'                  => Input::get('orgId'),
          'questionId'             => $optionArr[$i]['questionId'],                
          'optionId'               => $value['optionId'],
          'answer'                 => $value['point'],
          'cot_role_map_option_id' => $value['roleMapId'],
          'status'                 => 'Active',
          'created_at'             => date('Y-m-d H:i:s'),
        );

        $insertId = DB::table('cot_answers')->insertGetId($insertArray);
      }                
    }

    if($insertId)
    {
      return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-cot-answer','message'=>'COT answer added successfully.','data'=>$resultArray]);
    }

    return response()->json(['code'=>400,'status'=>false,'service_name'=>'add-cot-answer','message'=>'COT answer not added','data'=>$resultArray]);

  }

  /*get cot team role map completed answers*/
  public function getCOTteamRoleCompletedAnswers()
  {

    $resultArray = array();

    $user   = Auth::user();
    $userId = $user->id;

    $ans = DB::table('cot_answers')->where('userId',$userId)->first();

    if (empty($ans)) 
    {
      return response()->json(['code'=>400,'status'=>false,'service_name'=>'COT-team-role-map-question-answers','message'=>'cot team role map answers not filled.','data'=>[]]);
    }
    
    $questionTbl = DB::table('cot_questions')->select('id as questionId','question')->where('status','Active')->get();

    $queArray = array();

    foreach ($questionTbl as $value)
    {

      $optionsTbl = DB::table('cot_options')->select('id AS optionId','option_name AS optionName')->where('status','Active')->where('cot_question_id',$value->questionId)->get();

      $value->options = array();
      foreach ($optionsTbl as $optValue)
      {
        $diaAns = DB::table('cot_answers')->where('questionId',$value->questionId)->where('userId',$userId)->where('optionId',$optValue->optionId)->first();

        $optValue->point = "0";
        if($diaAns)
        {
          $optValue->point     = $diaAns->answer;
          $optValue->answerId  = $diaAns->id;
          $value->answerId     = $diaAns->id;
        }      

        array_push($value->options, $optValue);
      }     

      array_push($queArray, $value);
    }   

    return response()->json(['code'=>200,'status'=>true,'service_name'=>'COT-team-role-map-question-answers','message'=>'','data'=>$queArray]);
  }

  /*update team role map answer*/
  public function updateCOTteamRoleMapAnswers()
  {
    $resultArray = array();

    $optionArr   = Input::get('answer');
    $user        = Auth::user();
    $userId      = $user->id; 

    for ($i=0; $i < count($optionArr); $i++)
    {
      foreach ($optionArr[$i]['option'] as  $value)
      {
        $updateArray = array( 
          'answer'        => $value['point'],                
          'updated_at'    => date('Y-m-d H:i:s'),
        );

        DB::table('cot_answers')->where('id',$value['answerId'])->where('userId',$userId)->update($updateArray);
      }             
    } 
    return response()->json(['code'=>200,'status'=>true,'service_name'=>'Update-cot-team-role-answer','message'=>'COT team role map answer updated successfully.','data'=>$resultArray]);
  }

  /*get cot functional lens completed answers*/
  public function getCOTfuncLensCompletedAnswers()
  {

    $resultArray = array();

    $user   = Auth::user();
    $userId = $user->id;

    $ans = DB::table('cot_functional_lens_answers')->where('userId',$userId)->first();

    if (empty($ans)) 
    {
      return response()->json(['code'=>400,'status'=>false,'service_name'=>'COT-fun-lens-question-answers','message'=>'cot functional lens answers not filled.','data'=>[]]);
    }
    
    $questionTbl = DB::table('cot_functional_lens_questions')->select('id as questionId','question')->where('status','Active')->get();

    $queArray = array();

    foreach ($questionTbl as $value)
    {

      $optionsTbl = DB::table('cot_functional_lens_question_options')->select('id AS optionId','option_name AS optionName')->where('status','Active')->where('question_id',$value->questionId)->get();

      $value->options = array();
      foreach ($optionsTbl as $optValue)
      {
        $comAns = DB::table('cot_functional_lens_answers')->where('questionId',$value->questionId)->where('userId',$userId)->where('optionId',$optValue->optionId)->first();

        $optValue->isChecked = false;
        if($comAns)
        {
          $optValue->isChecked = true;
          $optValue->answerId  = $comAns->id;
          $value->answerId     = $comAns->id;
        }      
        array_push($value->options, $optValue);
      }     

      array_push($queArray, $value);
    }   

    return response()->json(['code'=>200,'status'=>true,'service_name'=>'COT-fun-lens-question-answers','message'=>'','data'=>$queArray]);
  }  

  /*update cot functional lens answers*/
  public function updateCOTfunLensAnswers()
  {
    $resultArray = array();

    $optionArr = Input::get('answer');
    $user      = Auth::user();
    $userId    = $user->id; 

    foreach ($optionArr as $value)
    {
      $updateArray = array('optionId'=> $value['optionId'],'updated_at'=> date('Y-m-d H:i:s'));
      
      DB::table('cot_functional_lens_answers')->where('id',$value['answerId'])->where('userId',$userId)->update($updateArray);
    } 

    return response()->json(['code'=>200,'status'=>true,'service_name'=>'Update-cot-func-lens-answer','message'=>'cot functional lens answer updated successfully.','data'=>$resultArray]);
  }


  public function getCOTindividualSummary(Request $request)
  {

    $resultArray = array();
    
    if($request->userId)
    {
      $userId = $request->userId;
    }
    else
    {
     $user = Auth::user();
     $userId = $user->id;
   }

   $mapers = DB::table('cot_role_map_options')->where('status','Active')->orderBy('id','ASC')->get();

   $users = DB::table('users')->select('id','name','orgId')->where('id',$userId)->where('status','Active')->orderBy('id','DESC')->get();

   $usersArray = array();

   foreach($users as $key => $value) 
   {

    foreach($mapers as $key => $maper)
    {
      $maperCount = DB::table('cot_answers')
      ->where('orgId',$value->orgId)
      ->where('userId',$value->id)
      ->where('cot_role_map_option_id',$maper->id)->where('status','Active')->sum('answer');

      $resultArray = array();

      $Value[$maper->maper_key] = $maperCount;    

      array_push($resultArray, $Value);
    }   

    arsort($resultArray[0]);

    $i = 0;
    $prev = "";
    $j = 0;

    $new  = array();
    $new1 = array();

    foreach($resultArray[0] as $key => $val)
    { 
      if($val != $prev)
      { 
        $i++; 
      }

      $new1['title'] = $key; 
      $new1['value'] = $i;        
      $prev = $val;

      if ($key==$mapers[0]->maper_key) {
        $new1['priority'] = 1;
      }elseif ($key==$mapers[1]->maper_key) {
        $new1['priority'] = 2;
      }elseif ($key==$mapers[2]->maper_key) {
        $new1['priority'] = 3;
      }elseif ($key==$mapers[3]->maper_key) {
        $new1['priority'] = 4;
      }elseif ($key==$mapers[4]->maper_key) {
        $new1['priority'] = 5;
      }elseif ($key==$mapers[5]->maper_key) {
        $new1['priority'] = 6;
      }elseif ($key==$mapers[6]->maper_key) {
        $new1['priority'] = 7;
      }elseif ($key==$mapers[7]->maper_key) {
        $new1['priority'] = 8;
      }

      array_push($new,$new1);
    } 

    $value->new = $new;

    $value->totalKeyCount = $resultArray[0];


    foreach($mapers as $mapValue)
    {
      $mapersArray = array();

      $res[$mapValue->maper_key] = $mapValue->maper;

      array_push($mapersArray, $res);
    }

    $value->mapersArray = $mapersArray[0];

    array_push($usersArray, $value);
  }
    // print_r($usersArray);

  $usersArray = $this->sortarr($usersArray);
   
    //convert array to string
    $finalValue = '';
    foreach ($usersArray as $key => $finalValue);

    $cotAnswered = true;
    if(empty($resultArray[0]['point']))
    {
      $cotAnswered = false;
    }

    if($request->reportStatus)
    {
      return $finalValue;
    }

    return response()->json(['code'=>200,'status'=>true,'cotAnswered'=>$cotAnswered,'service_name'=>'cot-indivisual-summary','message'=>'COT indivisual summary.','data'=>$finalValue]);
  }

  /*sorting array by priority*/
  function sortarr($arr1)
  {

  // print_r($arr1[0]->mapersArray);
  // die();

    $arrUsers = array();
    $newArray = array();
    $tes =array();

    for($i=0; $i<count($arr1); $i++)
    {    
      $tes['userId']    = $arr1[$i]->id;
      $tes['userName']  = $arr1[$i]->name;   

      $arrt = (array)$arr1[$i]->new;

      usort($arrt, function($x, $y)
      {

       if ($x['value']== $y['value'] ) 
       {
        if($x['priority']<$y['priority'])
        {
          return 0;
        }
        else
        {
          return 1;
        } 
      }
    }); 

      $tt = array();
      foreach($arrt as $key => $value)
      {         
        if ($value['value']!=0)
        {
          $tes[$value['title']] = $key+1;
        } else {
          $tes[$value['title']] = 0;
        }

      }   

      $tes['totalKeyCount'] = $arr1[$i]->totalKeyCount;

      $tes['mapersArray'] = array();
      if (!empty($arr1[$i]->mapersArray))
      {
        $tes['mapersArray']   = $arr1[$i]->mapersArray;
      }
      
      array_push($newArray,(object)$tes);
    }

  // print_r($newArray);
    return $newArray;

  }

  /*get COT answer list */
  public function getCOTteamSummary()
  {
    $resultArray = array();
    $finalArray  = array();

    $orgId        = Input::get('orgId');
    $searchKey    = Input::get('searchKey');
    $officeId     = Input::get('officeId');
    $departmentId = Input::get('departmentId');
    $maperKey     = Input::get('maperKey');
    $preferenceKey= Input::get('preferenceKey');
    $page         = Input::get('page');

    $recordLimit    = 10;
    $pageCount      = $page - 1; 
    $offset         = $recordLimit * $pageCount;
    $totalPageCount = 0;


    $wrclaue = array();

    if($officeId)
    {
      $wrclaue['officeId'] = $officeId;
    }

    if($departmentId)
    {
      $wrclaue['departmentId'] = $departmentId;
    }

    $wrclaue['status'] = 'Active';
    $wrclaue['orgId']  = $orgId;
    $wrclaue['roleId']  = 3;

    $pageCount = app('App\Http\Controllers\API\ApiCommonController')->getPageCountQuery('users', $wrclaue);   
    $totalPageCount = app('App\Http\Controllers\API\ApiCommonController')->getPageCount($pageCount,$recordLimit);

    if($page==1000)
    {
      $users  = DB::table('users')
      ->select('id','name','orgId')
      ->where($wrclaue)->where('status','Active')->where('name','LIKE',"%{$searchKey}%")        
      ->orderBy('id','DESC')            
      ->get();

    }else{

      $users  = DB::table('users')
      ->select('id','name','orgId')
      ->where($wrclaue)->where('status','Active')->where('name','LIKE',"%{$searchKey}%")        
      ->orderBy('id','DESC')
      ->offset($offset)
      ->limit($recordLimit)
      ->get();

    }


    $cotRoleMapOptions = DB::table('cot_role_map_options')->where('status','Active')->get();

    $usersArray = array();

    foreach ($users as $key => $value) 
    {

      foreach($cotRoleMapOptions as $key => $maper)
      {
        $maperCount = DB::table('cot_answers')
        ->where('orgId',$value->orgId)
        ->where('userId',$value->id)
        ->where('cot_role_map_option_id',$maper->id)->where('status','Active')->sum('answer');

        $resultArray = array();

        $Value[$maper->maper_key] = $maperCount;    

        array_push($resultArray, $Value);
      }   

      arsort($resultArray[0]);

      $i = 0;
      $prev = "";
      $j = 0;

      $new  = array();
      $new1 = array();

      foreach($resultArray[0] as $key => $val)
      { 
        if($val != $prev)
        { 
          $i++; 
        }

        $new1['title'] = $key; 
        $new1['value'] = $i;        
        $prev = $val;

        if ($key==$cotRoleMapOptions[0]->maper_key) {
          $new1['priority'] = 1;
        }elseif ($key==$cotRoleMapOptions[1]->maper_key) {
          $new1['priority'] = 2;
        }elseif ($key==$cotRoleMapOptions[2]->maper_key) {
          $new1['priority'] = 3;
        }elseif ($key==$cotRoleMapOptions[3]->maper_key) {
          $new1['priority'] = 4;
        }elseif ($key==$cotRoleMapOptions[4]->maper_key) {
          $new1['priority'] = 5;
        }elseif ($key==$cotRoleMapOptions[5]->maper_key) {
          $new1['priority'] = 6;
        }elseif ($key==$cotRoleMapOptions[6]->maper_key) {
          $new1['priority'] = 7;
        }elseif ($key==$cotRoleMapOptions[7]->maper_key) {
          $new1['priority'] = 8;
        }

        array_push($new,$new1);
      } 

      $value->new = $new;

      $value->totalKeyCount = $resultArray[0];

      foreach($cotRoleMapOptions as $mapValue)
      {
        $mapersArray = array();
        
        $res[$mapValue->maper_key] = $mapValue->maper;

        array_push($mapersArray, $res);
      }

      $value->mapersArray = $mapersArray[0];


      if($maperKey)
      {
        foreach ($new as $key => $valueKey)
        {
          if($key==$maperKey)
          {
           if ($preferenceKey=='secondary')
           {
            if($new[$key]==2)
            {
             array_push($usersArray, $value);
           }
         }elseif ($preferenceKey=='tertiary')
         {
          if($new[$key]==3)
          {
           array_push($usersArray, $value);
         }
       }
       else if($preferenceKey=='primary')
       {
        if($new[$key]==1)
        {
         array_push($usersArray, $value);
       }
     }else{
       if($new[$key]==1)
       {
         array_push($usersArray, $value);
       }
     }                 
   }
 }
}
else
{
  array_push($usersArray, $value);
}

}
/*print_r($usersArray);
die();*/
$usersArray = $this->sortarr($usersArray);

return response()->json(['code'=>200,'status'=>true,'service_name'=>'COT-team-summary','message'=>'COT team summary.','data'=>$usersArray,'totalPageCount'=>$totalPageCount,'currentPage'=>$page]);
}




/*check cot answers submited by user or not*/
public function isCOTanswerDone()
{
  $resultArray = array();

  $user  = Auth::user();
  $orgId = $user->orgId;
  $userId= $user->id;

  $isAnswered = DB::table('cot_answers')->where('orgId',$orgId)->where('userId',$userId)->get();

  $resultArray['isCOTanswered'] = count($isAnswered)?true:false;

  $organisaton = DB::table('organisations')->where('id',$orgId)->first();

  $resultArray['isQuestionDestributed'] = !empty(($organisaton->has_cot))?true:false;


  return response()->json(['code'=>200,'status'=>true,'service_name'=>'is-cot-answer-done','message'=>'COT answer done.','data'=>$resultArray]);
}


/*get overpresented,underpresented user of COT*/
public function getCOTmaperkey()
{
  $resultArray = array();
  $finalArray  = array();
  $rankArray   = array();

  $orgId  = Input::get('orgId');

  $query = DB::table('cot_answers')
  ->select('users.id','users.orgId','users.name')
  ->leftjoin('users','users.id','cot_answers.userId')
  ->where('users.status','Active')
  ->where('users.orgId',$orgId);

  $users = $query->get();

  $resultArray1 = array();

  foreach ($users as  $value)
  {
    $data['id']    = $value->id;
    $data['orgId'] = $value->orgId;
    $data['name']  = $value->name;

    array_push($resultArray1, $data);
  }

//uniquly sort
  $users23 = array_unique($resultArray1,SORT_REGULAR);

  $finalUser = array();

  foreach ($users23 as  $value55)
  {
    $object = (object)array();    
    
    $object->id     = $value55['id'];
    $object->orgId  = $value55['orgId'];
    $object->name   = $value55['name'];

    array_push($finalUser,$object);
  }

  $mapers = DB::table('cot_role_map_options')->where('status','Active')->orderBy('id','ASC')->get();

  $usersArray = array();
  foreach ($finalUser as $key => $value) 
  {

    foreach($mapers as $key => $maper)
    {
      $maperCount = DB::table('cot_answers')
      ->where('orgId',$value->orgId)
      ->where('userId',$value->id)
      ->where('cot_role_map_option_id',$maper->id)->where('status','Active')->sum('answer');

      $resultArray = array();

      $Value[$maper->maper_key] = $maperCount;    

      array_push($resultArray, $Value);
    }   

    arsort($resultArray[0]);

    $i = 0;
    $prev = "";
    $j = 0;

    $new  = array();
    $new1 = array();

    foreach($resultArray[0] as $key => $val)
    { 
      if($val != $prev)
      { 
        $i++; 
      }

      $new1['title'] = $key; 
      $new1['value'] = $i;        
      $prev = $val;

      if ($key==$mapers[0]->maper_key) {
        $new1['priority'] = 1;
      }elseif ($key==$mapers[1]->maper_key) {
        $new1['priority'] = 2;
      }elseif ($key==$mapers[2]->maper_key) {
        $new1['priority'] = 3;
      }elseif ($key==$mapers[3]->maper_key) {
        $new1['priority'] = 4;
      }elseif ($key==$mapers[4]->maper_key) {
        $new1['priority'] = 5;
      }elseif ($key==$mapers[5]->maper_key) {
        $new1['priority'] = 6;
      }elseif ($key==$mapers[6]->maper_key) {
        $new1['priority'] = 7;
      }elseif ($key==$mapers[7]->maper_key) {
        $new1['priority'] = 8;
      }

      array_push($new,$new1);
    } 

    $value->new = $new;

    $value->totalKeyCount = $resultArray[0];

    array_push($usersArray, $value);

  }

  $usersArray = $this->sortarr($usersArray);


  $shaper = 0; 
  $coordinator = 0; 
  $completerFinisher = 0; 
  $teamworker = 0; 
  $implementer = 0; 
  $monitorEvaluator = 0; 
  $plant = 0; 
  $resourceInvestigator = 0; 

  foreach ($usersArray as $key1 => $value1)
  {

   if($value1->shaper==1 || $value1->shaper==2 || $value1->shaper==3)
   {
    $shaper++;     
  }
  if($value1->coordinator==1 || $value1->coordinator==2 || $value1->coordinator==3)
  {
    $coordinator++;
  }
  if($value1->completerFinisher==1 || $value1->completerFinisher==2 || $value1->completerFinisher==3)
  {
    $completerFinisher++;
  }
  if($value1->teamworker==1 || $value1->teamworker==2 || $value1->teamworker==3)
  {
    $teamworker++;
  }
  if($value1->implementer==1 || $value1->implementer==2 || $value1->implementer==3)
  {
    $implementer++;
  }
  if($value1->monitorEvaluator==1 || $value1->monitorEvaluator==2 || $value1->monitorEvaluator==3)
  {
    $monitorEvaluator++;
  }
  if($value1->plant==1 || $value1->plant==2 || $value1->plant==3)
  {
    $plant++;
  }
  if($value1->resourceInvestigator==1 || $value1->resourceInvestigator==2 || $value1->resourceInvestigator==3)
  {
    $resourceInvestigator++;
  }

}

$data['shaper']              = $shaper;
$data['coordinator']         = $coordinator;
$data['completerFinisher']   = $completerFinisher;
$data['teamworker']          = $teamworker;
$data['implementer']         = $implementer;
$data['monitorEvaluator']    = $monitorEvaluator;
$data['plant']               = $plant;
$data['resourceInvestigator']= $resourceInvestigator;


$totalUsers = count($finalUser);

/*get presented values*/
foreach ($data as $key => $value)
{

 if (!empty($value) && (!empty($totalUsers)))
 {

  $data['shaper']              = number_format((($shaper/$totalUsers)*100),2);
  $data['coordinator']         = number_format((($coordinator/$totalUsers)*100),2);
  $data['completerFinisher']   = number_format((($completerFinisher/$totalUsers)*100),2);
  $data['teamworker']          = number_format((($teamworker/$totalUsers)*100),2);
  $data['implementer']         = number_format((($implementer/$totalUsers)*100),2);
  $data['monitorEvaluator']    = number_format((($monitorEvaluator/$totalUsers)*100),2);
  $data['plant']               = number_format((($plant/$totalUsers)*100),2);
  $data['resourceInvestigator']= number_format((($resourceInvestigator/$totalUsers)*100),2);
}
else
{
  $data['shaper']              = 0;
  $data['coordinator']         = 0;
  $data['completerFinisher']   = 0;
  $data['teamworker']          = 0;
  $data['implementer']         = 0;
  $data['monitorEvaluator']    = 0;
  $data['plant']               = 0;
  $data['resourceInvestigator']= 0;
}
}

foreach($mapers as $value)
{
  $mapersArray = array();
  $reus[$value->maper_key] = $value->maper;
  array_push($mapersArray, $reus);
} 

$data['mapersArray'] = $mapersArray[0];

return response()->json(['code'=>200,'status'=>true,'service_name'=>'COT-maper-rank','message'=>'COT maper rank','data'=>$data]);

}

/*check functional questionary assign to the user*/
public function isQuestionsDistributedToUser()
{
  $resultArray = array();

  $user   = Auth::user();
  $userId = $user->id;
  $orgId  = $user->orgId;


  $organisation = DB::table('organisations')->where('id',$orgId)->first();

  $resultArray['COTFunLensStartDate'] = date('d-M-Y');
  if(!empty($organisation->COT_fun_lens_start_date)) 
  {
    $resultArray['COTFunLensStartDate'] =  date('d-M-Y', strtotime($organisation->COT_fun_lens_start_date));
  }

  $resultArray['isCOTquestionsMailSent'] = $user->is_COT_questions_mail_sent;

  $status = DB::table('cot_functional_lens_records')->where('userId',$userId)->where('status','Active')->first();

    //check cot functional lens answers done by user
  $isCOTfunclensAnswersDoneTbl = DB::table('cot_functional_lens_answers')->where('userId',$userId)->where('status','Active')->first();

  if(!empty($isCOTfunclensAnswersDoneTbl))
  {
    $resultArray['isCOTfunclensAnswersDone'] = true;
  }else{
    $resultArray['isCOTfunclensAnswersDone'] = false;
  }

  if($status)
  {      
    $resultArray['isQuestionDistributed'] = true;     

    return response()->json(['code'=>200,'status'=>true,'service_name'=>'COT-questions-distributed-to-user','message'=>'COT questions distributed to user','data'=>$resultArray]);   
  }else{

    $resultArray['isQuestionDistributed'] = false;

    return response()->json(['code'=>200,'status'=>true,'service_name'=>'COT-questions-distributed-to-user','message'=>'COT questions not distributed to user','data'=>$resultArray]);
  }

}

/*request for questionary links*/
public function requestQuestionnaireList()
{
  $resultArray = array();

  $user  = Auth::user();
  $email = $user->email;
  $name  = $user->name;
  $orgId = $user->orgId;
  $userId=$user->id; 

  $organisation = DB::table('organisations')->select('organisation')->where('id',$orgId)->first();

  $orgName ='';
  if($organisation){
    $orgName = $organisation->organisation;
  }

  $data = ['msg'=>'Please provide a link for questionnaire to '.ucfirst($name).':' .$email.' working for organisation '.ucfirst($orgName)];

  $email ='team@tribe365.co';   

  $te= Mail::send('layouts/notificationMail', $data, function($message) use($email) 
  {
    $message->from('notification@tribe365.co', 'Tribe365');
    $message->to($email);
    $message->subject('Request a link to questionnaire for COT Functional Lens');
  });

  DB::table('users')->where('id',$userId)->update(['is_COT_questions_mail_sent'=>"1"]);

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'COT-functional-lens-questions-request-mail','message'=>' COT questions request mail sent successfully.','data'=>$resultArray]);
}


/*get value of the COT functional lens */
public function getCOTFunctionalLensDetail(Request $request)
{   

  $resultArray = array();

  // $userId   = Input::get('userId');

  $userId = $request->userId;

  //check cot functional lens answers done by user
  $isCOTfunclensAnswersDoneTbl = DB::table('cot_functional_lens_answers')->where('userId',$userId)->where('status','Active')->first();

  $resultArray['isCOTfunclensAnswersDone'] = false;
  if(!empty($isCOTfunclensAnswersDoneTbl))
  {
    $resultArray['isCOTfunclensAnswersDone'] = true;
  }

//get pre define ids
  $funcLensValues = app('App\Http\Controllers\Admin\CommonController')->getCotFunLensInitialValue();

  $initialValEIArray = array();
  $countValEIArray   = array();

  foreach ($funcLensValues as $value)
  {
    $value1 = $value[0];
    $value2 = $value[1];

    $countE = DB::table('cot_functional_lens_answers AS cfla')   
    ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
    ->where('cfla.userId',$userId)->where('cflqo.initial_value_id',$value1)->get();

    //option table changes
    $query = DB::table('cot_functional_lens_answers AS cfla')    
    ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId');
    $query->where('cfla.userId',$userId)->where('cflqo.initial_value_id',$value2);

    $countI = $query->get();

    if(count($countE) > count($countI))
    {
      $countValEI = count($countE)-count($countI);
      $initialValEI  =  $value1;
    }
    elseif (count($countE) < count($countI))
    {
      $countValEI = count($countI)-count($countE);
      $initialValEI  =  $value2;    
    }
    elseif(count($countE) == count($countI))
    {
      $countValEI    = count($countE)-count($countI);
      $initialValEI  =  $value1;
    }
    else
    {
      $countValEI    = '';
      $initialValEI  = '';
    }
    
    array_push($initialValEIArray, $initialValEI);
    array_push($countValEIArray, $countValEI);   
  }


  $valueTypeArray = array();
  $valueTypeArray['value'] = array();
  $valueTypeArray['score'] = array();

  if(!empty($initialValEIArray) && !empty($countValEIArray))
  {
    $valueTypeArray = array('value'=> $initialValEIArray,'score'=> $countValEIArray);
  }

  $funcLensKeyDetail = array();

  foreach ($valueTypeArray['value'] as $value)
  {

    $funcLensIniValueRec = DB::table('cot_functional_lens_initial_value_records')->where('id',$value)->first();

    $results = array();

    if($funcLensIniValueRec)
    {
      $results['value']       = $funcLensIniValueRec->value;
      $results['title']       = $funcLensIniValueRec->title;
      $results['description'] = $funcLensIniValueRec->description;

      array_push($funcLensKeyDetail, $results);
    }
  }


  $strengthValueArray = array();

  for($i=0; $i < count($valueTypeArray['value']); $i++)
  {

    if(!empty($valueTypeArray['value'][$i]) && !empty($valueTypeArray['score'][$i]))
    {

      $strengthValueRec = DB::table('cot_functional_lens_insight_for_strength_value_records')     
      ->where('section',$valueTypeArray['value'][$i])
      ->Where('start','<=',$valueTypeArray['score'][$i])
      ->Where('end','>=',$valueTypeArray['score'][$i])       
      ->first();     

      if($strengthValueRec)
      {
        $table = DB::table('cot_functional_lens_initial_value_records')->select('value','title')->where('id',$valueTypeArray['value'][$i])->first();  
        $valuesKey    = '';
        $initialTitle = '';
        if(!empty($table))
        {
          $valuesKey    = $table->value;
          $initialTitle = $table->title;
        } 

        $strengthResults['score']                = $valueTypeArray['score'][$i];
        $strengthResults['value']                = $valuesKey;
        $strengthResults['title']                = $strengthValueRec->title.' '.$initialTitle;
        $strengthResults['positives']            = (string)$strengthValueRec->positives;
        $strengthResults['allowableWeaknesses']  = (string)$strengthValueRec->allowable_weaknesses;

        array_push($strengthValueArray, $strengthResults);
      }

    }else{

      $valuesKey = '';
      $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$valueTypeArray['value'][$i])->first();  
      if(!empty($table))
      {
        $valuesKey = $table->value;
      } 
      //if score will be zero
      $strengthResults['score']                = $valueTypeArray['score'][$i];
      $strengthResults['value']                = $valuesKey;
      $strengthResults['title']                = '';
      $strengthResults['positives']            = '';
      $strengthResults['allowableWeaknesses']  = '';
      array_push($strengthValueArray, $strengthResults);
    }
  }

  $matchValueArr = array();
  for($i=0; $i < count($valueTypeArray['value']); $i++)
  {

    $valuesKey = '';
    $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$valueTypeArray['value'][$i])->first();  

    if(!empty($table))
    {
      $valuesKey = $table->value;
    }

    array_push($matchValueArr, $valuesKey); 
  }

  $matchValue = implode('', $matchValueArr);

  $initialValueJson = json_encode($valueTypeArray['value']);

  $valueCombinationList = DB::table('cot_functional_lens_initial_value_records')->where('value',$initialValueJson)->first();

  $valueCombinationArray = array();

  if($valueCombinationList)
  {
    $cTips['value']   = $matchValue;
    $cTips['title']   = $matchValue;
    $cTips['summary'] = $valueCombinationList->description;

    array_push($valueCombinationArray, $cTips);
  }

//remove last and first string
  $tribeMatchValue = json_encode(array_slice($valueTypeArray['value'], 1, -1));


  $tribeTipsList = DB::table('cot_functional_lens_tribe_tips_records')->where('value',$tribeMatchValue)->get();


  $tribeTipsArray = array();
  foreach ($tribeTipsList as $value)
  {
    $tTips['value']   = substr($matchValue, 1, -1);
    $tTips['title']   = substr($matchValue, 1, -1);
    $tTips['summary'] = $value->summary;

    $tribeTipsPersuadeList = DB::table('cot_functional_lens_tribe_tips_seek_persuade_value_records')
    ->select('value','value_type AS valueType')
    ->where('status','Active')
    ->where('value_type','persuade')
    ->where('lens_tribe_tips_id',$value->id)->get();

    $tribeTipsSeekList = DB::table('cot_functional_lens_tribe_tips_seek_persuade_value_records')
    ->select('value','value_type AS valueType')
    ->where('status','Active')
    ->where('value_type','seek')
    ->where('lens_tribe_tips_id',$value->id)->get();

    $tTips['PersuadeArray'] = $tribeTipsPersuadeList;
    $tTips['seekArray']     = $tribeTipsSeekList;

    array_push($tribeTipsArray, $tTips);
  }

  $resultArray['funcLensKeyDetail'] = $funcLensKeyDetail;
  $resultArray['initialValueList']  = $strengthValueArray;
  $resultArray['valueCombination']  = $valueCombinationArray;
  $resultArray['tribeTipsArray']    = $tribeTipsArray;

  if($request->reportStatus)
  {
    return $resultArray;
  }

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'COT-functional-lens-detail','message'=>'COT functional lens detail.','data'=>$resultArray]);
}






public function getCOTMapperSummary()
{   
 $summary = DB::table('cot_role_map_options')->get();

 $resultArray = array();

 foreach ($summary as $value)
 {
  if($value->maper=="completerFinisher")
  {
    $results['title']   = "COMPLETER FINISHER";
  }
  else if($value->maper=="monitorEvaluator")
  {
    $results['title']   = "MONITOR EVALUATOR";
  }
  else if($value->maper=="resourceInvestigator")
  {
    $results['title']   = "RESOURCE INVESTIGATOR";

  }
  else
  {
    $results['title']   = strtoupper($value->maper);
  }



  $results['short_description']   = $value->short_description;
  $results['long_description'] = $value->long_description;
  array_push($resultArray, $results);


}


return response()->json(['code'=>200,'status'=>true,'service_name'=>'COT-role-map-summary','message'=>'COT role map summary.','data'=>$resultArray]);

}

/*get COT functional lens questions*/
public function getCOTfunctionalLensQuestionsList()
{
  $resultArray = array();

  $questions = DB::table('cot_functional_lens_questions')->select('id AS questionId','question AS questionName')->where('status','Active')->get();

  foreach ($questions as $queValue) 
  {
    $options = DB::table('cot_functional_lens_question_options')->select('id AS OptionId','option_name AS optionName','value_name AS valueName','initial AS initialValue')->where('question_id',$queValue->questionId)->where('status','Active')->get();

    $optArray = array();

    foreach ($options as $ansValue)
    {
      array_push($optArray, $ansValue);
    }

    $queValue->options = $optArray;

    array_push($resultArray, $queValue);
  }

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'get-cot-functional-lens-questions','message'=>'cot functional lens question list.','data'=>$resultArray]);
}

/*add cot functional lens answers*/
public function addCOTfunctionalLensAnswer()
{
  $resultArray = array();

  $insertId  = '';

  $optionArr = Input::get('answer');
  $user   = Auth::user();
  $userId = $user->id; 
  $orgId  = $user->orgId;

  foreach ($optionArr as  $value)
  {
    $insertArray = array(
      'userId'                 => $userId, 
      'orgId'                  => $orgId,
      'questionId'             => $value['questionId'],                
      'optionId'               => $value['optionId'],
      'answer'                 => 1,            
      'status'                 => 'Active',
      'created_at'             => date('Y-m-d H:i:s'),
    );

    $insertId = DB::table('cot_functional_lens_answers')->insertGetId($insertArray);
  } 

  if($insertId)
  {
    return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-cot-functional-lens-answer','message'=>'COT functional lens answer added successfully.','data'=>$resultArray]);
  }

  return response()->json(['code'=>400,'status'=>false,'service_name'=>'add-cot-functional-lens-answer','message'=>'COT functional lens answer not added.','data'=>$resultArray]);

}
/*get Team Role Map Values List*/
public function getCOTteamRoleMapValues()
{
  $teamRoleMap = DB::table('cot_role_map_options')->select('id','maper')->where('status','Active')->get();

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'Team-role-map-values-list','message'=>'','data'=>$teamRoleMap]);
}

}
