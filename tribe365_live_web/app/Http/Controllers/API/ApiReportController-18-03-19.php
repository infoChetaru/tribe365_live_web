<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use DB;
use Mail;

class ApiReportController extends Controller
{

  /*get DOT report*/
  public function getDOTreportGraph()
  {

    $orgId     = Input::get('orgId');
    $beliefId  = Input::get('beliefId');

    $dots = DB::table('dots')->where('orgId',$orgId)->first();

    $dotValuesArray = array();

    if(!empty($dots))
    {

      $query = DB::table('dots_beliefs')
      ->where('status','Active')
      ->where('dotId',$dots->id);  

      if(!empty($beliefId))
      {
        $query->where('id',$beliefId);
      }

      $query->orderBy('id','ASC');
      $dotBeliefs = $query->get();

      foreach ($dotBeliefs as $key => $bValue)
      {

        $dotValues = DB::table('dots_values')
        ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
        ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
        ->where('dots_values.status','Active')
        ->where('dots_values.beliefId',$bValue->id)
        ->orderBy('dot_value_list.id','ASC')
        ->get();

        $bRatings = DB::table('dot_values_ratings')
        ->leftjoin('users','users.id','=','dot_values_ratings.userId')
        ->where('users.status','Active') 
        ->where('beliefId', $bValue->id)->avg('ratings');

        $valuesArray = array();
        foreach ($dotValues as $key => $vValue) 
        {
          $vRatings = DB::table('dot_values_ratings')
          ->leftjoin('users','users.id','=','dot_values_ratings.userId')
          ->where('users.status','Active')
          ->where('valueId', $vValue->id)->avg('ratings');

          $vResult['valueId']      = $vValue->id;
          $vResult['valueName']    = ucfirst($vValue->name);

          $vResult['valueRatings'] = 0;
          if($vRatings)
          {
            $vResult['valueRatings'] = number_format(($vRatings-1), 2);
          }
          array_push($valuesArray, $vResult);
        }

        $result['beliefId']      = $bValue->id;
        $result['beliefName']    = ucfirst($bValue->name);           

        $result['beliefRatings'] = 0;
        if($bRatings)
        {
          $result['beliefRatings'] = number_format(($bRatings-1), 2);
        }

        $result['beliefValues']  = $valuesArray;

        array_push($dotValuesArray, $result);
      }
    }

    return response()->json(['code'=>200,'status'=>true,'service_name'=>'DOT-report','message'=>'','data'=>$dotValuesArray]);
  }

  /*get COT functional lens report*/
  public function getReportFunctionalLensGraph()
  {

    $orgId        = Input::get('orgId');  
    $officeId       = Input::get('officeId');
    $departmentId = Input::get('departmentId');

/*        $cotFunResultArray = array();

        $query = DB::table('cot_functional_lens_answers')
        ->select('cot_functional_lens_answers.userId AS id')
        ->leftjoin('users','users.id','cot_functional_lens_answers.userId')
        ->where('users.status','Active')
        ->groupBy('cot_functional_lens_answers.userId')
        ->where('users.orgId',$orgId);
        $usersList = $query->get();*/

        $flag = 0 ;
        $departments = [];
        if (!empty($officeId)) 
        {          
           //for new department
          $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
        }
        else
        {
          if(!empty($departmentId))
          {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
          }
        }

        $cotFunResultArray = array();

        $query = DB::table('cot_functional_lens_answers')
        ->select('cot_functional_lens_answers.userId AS id')
        ->leftjoin('users','users.id','cot_functional_lens_answers.userId')    
        ->where('users.status','Active')
        ->groupBy('cot_functional_lens_answers.userId')
        ->where('users.orgId',$orgId);

        if(!empty($officeId))
        {
         $query->where('officeId',$officeId);
       }
       if(!empty($departmentId))
       {   
        $i = 0;
        if($departmentsNew->isEmpty())
        {
          $flag = 1;
        }
        foreach ($departmentsNew as $departments1) 
        {         
          if($i == 0)
          {
            $query->where('users.departmentId',$departments1->id);
          }
          else
          {
            $query->orWhere('users.departmentId',$departments1->id);
          }
          $i++;
        }
      }

      $usersList = $query->get();

// reset if department is not in organisation
      if($flag==1)
      {
        $usersList = array();
      }

      foreach ($usersList as $key => $userValue)
      {
        $variable = array('EI','SN','TF','JP');

        $initialValEIArray = array();

        foreach ($variable as $value)
        {
          $value1 = substr($value, -2,1);
          $value2 = substr($value, 1);

          $countE = DB::table('cot_functional_lens_answers AS cfla')
          ->select('cflqo.option_name AS optionName')       
          ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')

          ->leftJoin('users','users.id','=','cfla.userId')
          ->where('users.status','Active')

          ->where('cfla.userId',$userValue->id)->where('cflqo.initial',$value1)->get();

          $countI = DB::table('cot_functional_lens_answers AS cfla')
          ->select('cflqo.option_name AS optionName')
          ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')

          ->leftJoin('users','users.id','=','cfla.userId')
          ->where('users.status','Active')

          ->where('cfla.userId',$userValue->id)->where('cflqo.initial',$value2)->get();

          $initialValEI  = '';
          if(count($countE) > count($countI))
          {            
            $initialValEI  =  $value1;
          }
          elseif (count($countE) < count($countI))
          {           
            $initialValEI  =  $value2;
          }
          elseif(count($countE) == count($countI))
          {
           $initialValEI  =  $value1;
         }           
         array_push($initialValEIArray, $initialValEI);  
       }

       $valueTypeArray = array();
       $valueTypeArray['value'] = array();

       if(!empty($initialValEIArray))
       {
        $valueTypeArray = array('value'=>$initialValEIArray);
      }

      $matchValueArr =array();
      for($i=0; $i < count($valueTypeArray['value']); $i++)
      {
        array_push($matchValueArr, $valueTypeArray['value'][$i]); 
      }

    //remove last and first string
      $tribeMatchValue = substr(implode('', $matchValueArr), 1, -1);

      $tribeTipsList = DB::table('cot_functional_lens_tribe_tips_records')->where('value',$tribeMatchValue)->get();

      $tribeTipsArray = array();
      foreach ($tribeTipsList as $ttvalue)
      {
        $tTips['value']   = $ttvalue->value;       
        array_push($cotFunResultArray, $tTips);
      }
    }

    $ST =0;
    $SF =0;
    $NF =0;
    $NT =0;

    foreach ($cotFunResultArray as $finalArrayValue)
    { 
     if ($finalArrayValue['value']=='ST'){

       $ST++;
     }elseif ($finalArrayValue['value']=='SF'){

       $SF++;
     }elseif ($finalArrayValue['value']=='NF'){

       $NF++;
     }elseif ($finalArrayValue['value']=='NT'){

       $NT++;
     }

   }

   $totalUser = count($usersList);


   if (!empty($totalUser))
   {
    $stPercent = number_format((($ST/$totalUser)*100),2);
    $sfPercent = number_format((($SF/$totalUser)*100),2);
    $nfPercent = number_format((($NF/$totalUser)*100),2);
    $ntPercent = number_format((($NT/$totalUser)*100),2);
  }
  else
  {
    $stPercent = 0;
    $sfPercent = 0;
    $nfPercent = 0;
    $ntPercent = 0;
  }

  $funcLensPercentageArray = array('st'=>$stPercent,'sf'=>$sfPercent,'nf'=>$nfPercent,'nt'=>$ntPercent);

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'COT-functional-lens-report','message'=>'','data'=>$funcLensPercentageArray]);
}

/*get overpresented,underpresented user of COT*/
public function getCOTteamRoleMapReport()
{

  $orgId        = Input::get('orgId');
  $officeId     = Input::get('officeId');
  $departmentId = Input::get('departmentId');

  $resultArray = array();
  $finalArray  = array();
  $rankArray   = array();
  
  $flag = 0 ;

  $departments = [];
  if (!empty($officeId)) 
  {
   //for new department
    $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
  }
  else
  {
    if(!empty($departmentId))
    {
      $departmentsNew = DB::table('departments')
      ->select('departments.id','all_department.department')
      ->leftJoin('all_department','all_department.id','departments.departmentId')
      ->where('departments.orgId',$orgId)
      ->where('departments.departmentId',$departmentId) 
      ->where('departments.status','Active') 
      ->orderBy('all_department.department','ASC')->get();
    }

  }

  $query = DB::table('cot_answers')
  ->select('users.id','users.orgId','users.name')
  ->leftjoin('users','users.id','cot_answers.userId')
  ->where('users.status','Active')
  ->where('users.orgId',$orgId);

  if(!empty($officeId))
  {
    $query->where('users.officeId',$officeId);
  }
  if(!empty($departmentId))
  {   
    $i = 0;
    
    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {
      if($i == 0)
      {
        $query->where('users.departmentId',$departments1->id);
      }
      else
      {
        $query->orWhere('users.departmentId',$departments1->id);
      }
      $i++;
    }
  }

  $users = $query->get();

  $resultArray1 = array();

  foreach ($users as  $value)
  {
    $data['id'] = $value->id;
    $data['orgId'] = $value->orgId;
    $data['name'] = $value->name;

    array_push($resultArray1, $data);
  }

  $users23 = array_unique($resultArray1,SORT_REGULAR);

  $finalUser = array();

  foreach ($users23 as  $value55)
  {
    $object = (object)array();    
    
    $object->id    = $value55['id'];
    $object->orgId = $value55['orgId'];
    $object->name  = $value55['name'];

    array_push($finalUser,$object);
  }
// reset if department is not in organisation
  if($flag==1)
  {
    $finalUser = array();
  }

  $cotRoleMapOptions = DB::table('cot_role_map_options')->where('status','Active')->orderBy('id','ASC')->get();

  $usersArray = array();
  foreach ($finalUser as $key => $value) 
  {

    foreach($cotRoleMapOptions as $key => $maper)
    {
      $maperCount = DB::table('cot_answers')
      ->where('orgId',$value->orgId)
      ->where('userId',$value->id)
      ->where('cot_role_map_option_id',$maper->id)->where('status','Active')->sum('answer');

      $resultArray = array();

      $Value[$maper->maper] = $maperCount;    

      array_push($resultArray, $Value);
    }   

    arsort($resultArray[0]);

    $i = 0;
    $prev = "";
    $j = 0;

    $new  = array();
    $new1 = array();

    foreach($resultArray[0] as $key => $val)
    { 
      if($val != $prev)
      { 
        $i++; 
      }

      $new1['title'] = $key; 
      $new1['value'] = $i;        
      $prev = $val;

      if ($key=='shaper') {
        $new1['priority'] = 1;
      }elseif ($key=='coordinator') {
        $new1['priority'] = 2;
      }elseif ($key=='implementer') {
        $new1['priority'] = 3;
      }elseif ($key=='completerFinisher') {
        $new1['priority'] = 4;
      }elseif ($key=='monitorEvaluator') {
        $new1['priority'] = 5;
      }elseif ($key=='teamworker') {
        $new1['priority'] = 6;
      }elseif ($key=='plant') {
        $new1['priority'] = 7;
      }elseif ($key=='resourceInvestigator') {
        $new1['priority'] = 8;
      }
      array_push($new,$new1);
    } 

    $value->new = $new;

    $value->totalKeyCount = $resultArray[0];

    array_push($usersArray, $value);

  }

  $usersArray = $this->sortarr($usersArray);

  $shaper = 0; 
  $coordinator = 0; 
  $completerFinisher = 0; 
  $teamworker = 0; 
  $implementer = 0; 
  $monitorEvaluator = 0; 
  $plant = 0; 
  $resourceInvestigator = 0; 

  foreach ($usersArray as $key1 => $value1)
  {

    if($value1->shaper==1 || $value1->shaper==2 || $value1->shaper==3)
    {
      $shaper++;     
    }
    if($value1->coordinator==1 || $value1->coordinator==2 || $value1->coordinator==3)
    {
      $coordinator++;
    }
    if($value1->completerFinisher==1 || $value1->completerFinisher==2 || $value1->completerFinisher==3)
    {
      $completerFinisher++;
    }
    if($value1->teamworker==1 || $value1->teamworker==2 || $value1->teamworker==3)
    {
      $teamworker++;
    }
    if($value1->implementer==1 || $value1->implementer==2 || $value1->implementer==3)
    {
      $implementer++;
    }
    if($value1->monitorEvaluator==1 || $value1->monitorEvaluator==2 || $value1->monitorEvaluator==3)
    {
      $monitorEvaluator++;
    }
    if($value1->plant==1 || $value1->plant==2 || $value1->plant==3)
    {
      $plant++;
    }
    if($value1->resourceInvestigator==1 || $value1->resourceInvestigator==2 || $value1->resourceInvestigator==3)
    {
      $resourceInvestigator++;
    }
    
  }

  $data['shaper']              = $shaper;
  $data['coordinator']         = $coordinator;
  $data['completerFinisher']   = $completerFinisher;
  $data['teamworker']          = $teamworker;
  $data['implementer']         = $implementer;
  $data['monitorEvaluator']    = $monitorEvaluator;
  $data['plant']               = $plant;
  $data['resourceInvestigator']= $resourceInvestigator;


  $totalUsers = count($finalUser);

  foreach ($data as $key => $value)
  {

   if (!empty($value) && (!empty($totalUsers)))
   {

    $data['shaper']              = number_format((($shaper/$totalUsers)*100),2);
    $data['coordinator']         = number_format((($coordinator/$totalUsers)*100),2);
    $data['completerFinisher']   = number_format((($completerFinisher/$totalUsers)*100),2);
    $data['teamworker']          = number_format((($teamworker/$totalUsers)*100),2);
    $data['implementer']         = number_format((($implementer/$totalUsers)*100),2);
    $data['monitorEvaluator']    = number_format((($monitorEvaluator/$totalUsers)*100),2);
    $data['plant']               = number_format((($plant/$totalUsers)*100),2);
    $data['resourceInvestigator']= number_format((($resourceInvestigator/$totalUsers)*100),2);
  }
  else
  {
    $data['shaper']              = 0;
    $data['coordinator']         = 0;
    $data['completerFinisher']   = 0;
    $data['teamworker']          = 0;
    $data['implementer']         = 0;
    $data['monitorEvaluator']    = 0;
    $data['plant']               = 0;
    $data['resourceInvestigator']= 0;
  }
}

return response()->json(['code'=>200,'status'=>true,'service_name'=>'COT-maper-rank','message'=>'COT maper rank','data'=>$data]);

}

/*sorting array by priority*/
function sortarr($arr1)
{

//   print_r($arr1[0]->totalKeyCount);
// die();
  $arrUsers = array();
  $newArray = array();
  $tes =array();

  for($i=0; $i<count($arr1); $i++)
  {    
    $tes['userId']    = $arr1[$i]->id;
    $tes['userName']  = $arr1[$i]->name;   

    $arrt = (array)$arr1[$i]->new;
    
    usort($arrt, function($x, $y)
    {

      if ($x['value']== $y['value'] ) 
      {
        if($x['priority']<$y['priority'])
        {
          return 0;
        }
        else
        {
          return 1;
        } 
      }
    }); 

    $tt = array();
    foreach($arrt as $key => $value)
    {         
      if ($value['value']!=0)
      {
        $tes[$value['title']] = $key+1;
      } else{
        $tes[$value['title']] = 0;
      }

    }   

    $tes['totalKeyCount'] = $arr1[$i]->totalKeyCount;

    array_push($newArray,(object)$tes);
  }

  // print_r($newArray);
  return $newArray;

}


/*get SOT culture structure report */
public function getSOTcultureStructureReport()
{

 $resultArray = array();
 $user        = Auth::user();

 $orgId        = Input::get('orgId');
 $officeId     = Input::get('officeId');
 $departmentId = Input::get('departmentId');

 $flag = 0 ;
 $departments = [];
 if (!empty($officeId)) 
 {
  //for new department
  $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
}
else
{
  if(!empty($departmentId))
  {
    $departmentsNew = DB::table('departments')
    ->select('departments.id','all_department.department')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('departments.orgId',$orgId)
    ->where('departments.departmentId',$departmentId) 
    ->where('departments.status','Active') 
    ->orderBy('all_department.department','ASC')->get();
  }
}

$sotCultureStrTbl = DB::table('sot_culture_structure_records')->select('id','type','title','imgUrl')->where('status','Active')->get();

$sotCountArray = array();
$countArr      = array();
$resultArray['IsQuestionnaireAnswerFilled'] = false;

foreach ($sotCultureStrTbl as $value) 
{

  $query = DB::table('sot_answers')
  ->select(DB::raw('sum(score) AS count'))
  ->leftJoin('sot_questionnaire_records','sot_questionnaire_records.id','=','sot_answers.question_id')
  ->join('users','users.id','=','sot_answers.userId')
  ->where('users.status','Active')
  ->where('sot_questionnaire_records.type',$value->type)    
  ->where('users.orgId',$orgId);

  if(!empty($officeId))
  {
   $query->where('users.officeId',$officeId);
 }

 if(!empty($departmentId))
 {   
  $i = 0;

  if($departmentsNew->isEmpty())
  {
    $flag = 1;
  }
  foreach ($departmentsNew as $departments1) 
  {       
    if($i == 0)
    {
      $query->where('users.departmentId',$departments1->id);
    }
    else
    {
      $query->orWhere('users.departmentId',$departments1->id);
    }
    $i++;
  }
}       

$SOTCount = $query->first();

$value->SOTCount = ($SOTCount->count)?(string)$SOTCount->count:"0";

// reset if department is not in organisation
if($flag==1)
{    
  $value->SOTCount = "0";
}


$value->imgUrl   = url('public/uploads/sot/').'/'.$value->imgUrl;

if ($SOTCount->count) {
  $resultArray['IsQuestionnaireAnswerFilled']   = true;
}


$IsUserFilledAnswer = DB::table('sot_answers')
->select(DB::raw('sum(score) AS count'))->where('userId',$user->id)->first();

if ($IsUserFilledAnswer->count) {
  $resultArray['IsUserFilledAnswer']   = true;
}
else
{
  $resultArray['IsUserFilledAnswer']   = false;
}

array_push($countArr, $SOTCount->count);
array_push($sotCountArray, $value);

}

if($flag==1)
{
  $sotCountArray  = array();
}

// find maximum value array
$sotCountArray1 = array();
foreach ($sotCultureStrTbl as $value) 
{

  $query = DB::table('sot_answers')
  ->select(DB::raw('sum(score) AS count'))
  ->leftJoin('sot_questionnaire_records','sot_questionnaire_records.id','=','sot_answers.question_id')
  ->leftJoin('users','users.id','=','sot_answers.userId')
  ->where('users.status','Active')
  ->where('sot_questionnaire_records.type',$value->type)    
  ->where('users.orgId',$orgId);

  if(!empty($officeId))
  {
   $query->where('users.officeId',$officeId);
 }

 if(!empty($departmentId))
 {   


  if($departmentsNew->isEmpty())
  {
    $flag = 1;
  }
  $department3 = array();
  foreach ($departmentsNew as $departments1) 
  {       
    $departments2 = $departments1->id;
    array_push($department3, $departments2);

  }

  $query->whereIn('users.departmentId',$department3);
}            

$SOTCount = $query->first();

$value->SOTCount = ($SOTCount->count)?(string)$SOTCount->count:"0";

if (!empty($SOTCount->count) && max($countArr)==$SOTCount->count)
{
  array_push($sotCountArray1, $value);
}        

}

//detail of culture structure 
$sotStrDetailArr = array();

// reset if department is not in organisation
if($flag==1)
{
  $sotCountArray1 = array();
}

foreach($sotCountArray1 as $sValue)
{
  $sotStrDetail = DB::table('sot_culture_structure_records')->where('id',$sValue->id)->where('status','Active')->first();

  $summary = DB::table('sot_culture_structure_summary_records')->select('summary')->where('type',$sValue->id)->where('status','Active')->get();

  $sValue->summary = $summary;

  array_push($sotStrDetailArr, $sValue);
  break; 
}

$resultArray['sotDetailArray']         = $sotCountArray;
$resultArray['sotSummaryDetailArray']  = $sotStrDetailArr;


return response()->json(['code'=>200,'status'=>true,'service_name'=>'SOT-culture-structure-report','message'=>'','data'=>$resultArray]);

}

//get SOT motivation report
public function getSOTmotivationReport()
{

  $resultArray = array();

  $orgId        = Input::get('orgId');  
  $officeId     = Input::get('officeId');
  $departmentId = Input::get('departmentId');

  $flag = 0 ;
  $departments = [];
  if (!empty($officeId)) 
  {
  //for new department
    $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
  }
  else
  {
    if(!empty($departmentId))
    {
      $departmentsNew = DB::table('departments')
      ->select('departments.id','all_department.department')
      ->leftJoin('all_department','all_department.id','departments.departmentId')
      ->where('departments.orgId',$orgId)
      ->where('departments.departmentId',$departmentId) 
      ->where('departments.status','Active') 
      ->orderBy('all_department.department','ASC')->get();
    }
  }

  $query = DB::table('sot_motivation_answers')
  ->leftjoin('users','users.id','sot_motivation_answers.userId')
  ->select('sot_motivation_answers.userId')
  ->where('users.status','Active')
  ->groupBy('sot_motivation_answers.userId')
  ->where('users.orgId',$orgId);
  
  if(!empty($officeId))
  {
    $query->where('officeId',$officeId);
  }

  if(!empty($departmentId))
  {   
    $i = 0;

    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {       
      if($i == 0)
      {
        $query->where('users.departmentId',$departments1->id);
      }
      else
      {
        $query->orWhere('users.departmentId',$departments1->id);
      }
      $i++;
    }
  } 

  $usersList = $query->get();

  $totalUser = count($usersList);

  // reset if department is not in organisation
  if($flag==1)
  {
    $totalUser = 0;
  }

  if(empty($totalUser))
  {
    return response()->json(['code'=>400,'status'=>true,'service_name'=>'SOT-motivation-report','message'=>'SOT answers are not done by user.','data'=>$resultArray]);
  }

  $categoryTbl = DB::table('sot_motivation_value_records')->where('status','Active')->get();

  foreach ($categoryTbl as $value)
  {  

    $query = DB::table('sot_motivation_answers AS sotans')
    ->leftJoin('sot_motivation_question_options AS qoption', 'qoption.id', '=', 'sotans.optionId')
    ->leftjoin('users','users.id','=','sotans.userId')
    ->where('users.status','Active')                     
    ->where('sotans.orgId',$orgId)
    ->where('sotans.status','Active')
    ->where('qoption.category_id',$value->id);

    if(!empty($officeId))
    {
     $query->where('users.officeId',$officeId);
   }

   if(!empty($departmentId))
   {   


    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    $department3 = array();
    foreach ($departmentsNew as $departments1) 
    {       
      $departments2 = $departments1->id;
      array_push($department3, $departments2);

    }

    $query->whereIn('users.departmentId',$department3);
  } 

  $ansTbl = $query->sum('sotans.answer'); 

  $result['title'] = $value->title;    
  $result['score'] = number_format((float)($ansTbl/$totalUser), 2); 

  array_push($resultArray, $result);
}

return response()->json(['code'=>200,'status'=>true,'service_name'=>'SOT-motivation-report','message'=>'','data'=>$resultArray]);
}


/*get diagnostic reports*/
public function getDiagnosticReportForGraph()
{

  $resultArray = array();

  $orgId        = Input::get('orgId');
  $officeId     = Input::get('officeId');
  $departmentId = Input::get('departmentId');

  $flag = 0 ;
  $departments = [];
  if (!empty($officeId)) 
  {
  //for new department
    $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
  }
  else
  {
    if(!empty($departmentId))
    {
      $departmentsNew = DB::table('departments')
      ->select('departments.id','all_department.department')
      ->leftJoin('all_department','all_department.id','departments.departmentId')
      ->where('departments.orgId',$orgId)
      ->where('departments.departmentId',$departmentId) 
      ->where('departments.status','Active') 
      ->orderBy('all_department.department','ASC')->get();
    }
  }

  $diagnosticQueCatTbl = DB::table('diagnostic_questions_category')->get();

  $query = DB::table('diagnostic_answers')
  ->leftjoin('users','users.id','diagnostic_answers.userId')
  ->select('diagnostic_answers.userId')
  ->where('users.status','Active')
  ->groupBy('diagnostic_answers.userId')
  ->where('users.orgId',$orgId);

  if(!empty($officeId))
  {
    $query->where('users.officeId',$officeId);
  }

  if(!empty($departmentId))
  {   
    $i = 0;

    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {       
      if($i == 0)
      {
        $query->where('users.departmentId',$departments1->id);
      }
      else
      {
        $query->orWhere('users.departmentId',$departments1->id);
      }
      $i++;
    }
  }


  $users     = $query->get();
  $userCount = count($users);

 // reset if department is not in organisation
  if($flag==1)
  {
    $userCount = 0;
  }

  if (empty($userCount))
  {
    return response()->json(['code'=>400,'status'=>true,'service_name'=>'Report-diagnostic-report','message'=>'Diagnostic answers not done yet.','data'=>$resultArray]);
  }

  $optionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
  $diaOptCount = count($optionsTbl)-1; 

  foreach ($diagnosticQueCatTbl as $value)
  {
    $questionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

    $quecount = count($questionTbl);

    $perQuePercen = 0;
    foreach ($questionTbl as  $queValue)
    {
      $diaQuery = DB::table('diagnostic_answers')            
      ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
      ->leftjoin('users','users.id','=','diagnostic_answers.userId')
      ->where('users.status','Active')   
      ->where('diagnostic_answers.orgId',$orgId)
      ->where('diagnostic_questions.id',$queValue->id)
      ->where('diagnostic_questions.category_id',$value->id);

      if (!empty($officeId))
      {
        $diaQuery->where('users.officeId',$officeId);                    
      }

      if(!empty($departmentId))
      {   


        if($departmentsNew->isEmpty())
        {
          $flag = 1;
        }
        $department3 = array();
        foreach ($departmentsNew as $departments1) 
        {       
          $departments2 = $departments1->id;
          array_push($department3, $departments2);

        }

        $diaQuery->whereIn('users.departmentId',$department3);
      }

      $diaAnsTbl = $diaQuery->sum('answer'); 

      $perQuePercen += ($diaAnsTbl/$userCount);       
    }


    $score = ($perQuePercen/($quecount*$diaOptCount));
    $totalPercentage = ($perQuePercen/($quecount*$diaOptCount))*100;

    $value1['title']      =  $value->title;
    $value1['score']      =  number_format((float)$score, 2, '.', '');
    $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');      

    array_push($resultArray, $value1);
  }

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'Report-diagnostic-report','message'=>'','data'=>$resultArray]);

}


/*get diagnostic reports*/
public function getTribeometerReportForGraph()
{

  $resultArray = array();

  $orgId        = Input::get('orgId');
  $officeId     = Input::get('officeId');
  $departmentId = Input::get('departmentId');

  $flag = 0 ;
  $departments = [];
  if (!empty($officeId)) 
  {
     //for new department
    $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
  }
  else
  {
    if(!empty($departmentId))
    {
      $departmentsNew = DB::table('departments')
      ->select('departments.id','all_department.department')
      ->leftJoin('all_department','all_department.id','departments.departmentId')
      ->where('departments.orgId',$orgId)
      ->where('departments.departmentId',$departmentId) 
      ->where('departments.status','Active') 
      ->orderBy('all_department.department','ASC')->get();
    }
  }

  $query = DB::table('tribeometer_answers')
  ->leftjoin('users','users.id','tribeometer_answers.userId')
  ->select('tribeometer_answers.userId')
  ->where('users.status','Active')
  ->groupBy('tribeometer_answers.userId')
  ->where('users.orgId',$orgId);

  if(!empty($officeId))
  {
    $query->where('users.officeId',$officeId);
  }

  if(!empty($departmentId))
  {   
    $i = 0;

    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {       
      if($i == 0)
      {
        $query->where('users.departmentId',$departments1->id);
      }
      else
      {
        $query->orWhere('users.departmentId',$departments1->id);
      }
      $i++;
    }
  }

  $users     = $query->get();
  $userCount = count($users);

   // reset if department is not in organisation
  if($flag==1)
  {
    $userCount = 0;
  }

  if(empty($userCount))
  {
    return response()->json(['code'=>400,'status'=>true,'service_name'=>'Report-tribeometer-report','message'=>'Tribeometer answers not done yet.','data'=>$resultArray]);
  }   

  $optionsTbl = DB::table('tribeometer_question_options')->where('status','Active')->get();
  $optCount   = count($optionsTbl)-1; 

  $queCatTbl    = DB::table('tribeometer_questions_category')->get();
  foreach ($queCatTbl as $value)
  {
    $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();

    $quecount = count($questionTbl);

    $perQuePercen = 0;
    foreach ($questionTbl as  $queValue)
    {
      $triQuery = DB::table('tribeometer_answers')           
      ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
      ->leftjoin('users','users.id','=','tribeometer_answers.userId')
      ->where('users.status','Active') 
      ->where('tribeometer_answers.orgId',$orgId)
      ->where('tribeometer_questions.id',$queValue->id)
      ->where('tribeometer_questions.category_id',$value->id);

      if (!empty($officeId))
      {
        $triQuery->where('users.officeId',$officeId);                    
      }

      if(!empty($departmentId))
      {   


        if($departmentsNew->isEmpty())
        {
          $flag = 1;
        }
        $department3 = array();
        foreach ($departmentsNew as $departments1) 
        {       
          $departments2 = $departments1->id;
          array_push($department3, $departments2);

        }

        $triQuery->whereIn('users.departmentId',$department3);
      }

      $triAnsTbl = $triQuery->sum('answer');

      $perQuePercen += ($triAnsTbl/$userCount);       
    }

    $score           = ($perQuePercen/($quecount*$optCount));
    $totalPercentage = ($perQuePercen/($quecount*$optCount))*100;

    $value1['title']      =  $value->title;
    $value1['score']      =  number_format((float)$score, 2, '.', '');
    $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');      

    array_push($resultArray, $value1);
  }

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'Report-tribeometer-report','message'=>'','data'=>$resultArray]);

}

/*get report pdf url */
public function getReportPdfUrl()
{
  $resultArray = array();
  $orgId = Input::get('orgId');

  $organisation = DB::table('organisations')->select('PdfReportUrl')->where('id',$orgId)->first();

  $resultArray['reportPdfUrl'] = '';
  if(!empty($organisation->PdfReportUrl))
  {
   $resultArray['reportPdfUrl'] = url('/public/uploads/pdf_report').'/'.$organisation->PdfReportUrl; 
 }

 return response()->json(['code'=>200,'status'=>true,'service_name'=>'Report-pdf-url','message'=>'','data'=>$resultArray]);
}

}
