<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use DB;


class ApiDiagnosticController extends Controller
{
	public function getDiagnosticQuestionList()
	{
		$resultArray = array();

		$questionTbl = DB::table('diagnostic_questions')->select('id AS questionId','question')->where('status','Active')->get();

		foreach ($questionTbl as $value)
		{
			$options = DB::table('diagnostic_question_options')->select('id AS optionId','option_name AS optionName')->where('status','Active')->get();

			$value->options = $options;

			array_push($resultArray, $value);
		}

		return response()->json(['code'=>200,'status'=>true,'service_name'=>'Diagnostic-questions-list','message'=>'Diagnostic questionslist','data'=>$resultArray]);

	}
	/*add diagnostic answers*/
	public function addDiagnosticAnswers()
	{

		$resultArray = array();

		$insertId  = '';

		$optionArr = Input::get('answer');
		$user      = Auth::user();
		$userId    = $user->id; 
		$orgId     = $user->orgId;

		foreach ($optionArr as  $value)
		{
			$optionRatingTbl = DB::table('diagnostic_question_options')->select('option_rating')->where('id',$value['optionId'])->first();

			$optionRating = 0;
			if (!empty($optionRatingTbl))
			{
				$optionRating = $optionRatingTbl->option_rating;
			}
			
			$insertArray = array(
				'userId'                 => $userId, 
				'orgId'                  => $orgId,
				'questionId'             => $value['questionId'],                
				'optionId'               => $value['optionId'],
				'answer'                 => $optionRating,            
				'status'                 => 'Active',
				'created_at'             => date('Y-m-d H:i:s'),
			);

			$insertId = DB::table('diagnostic_answers')->insertGetId($insertArray);
		} 

		if($insertId)
		{
			return response()->json(['code'=>200,'status'=>true,'service_name'=>'Add-diagnostic-answer','message'=>'Diagnostic answer added successfully.','data'=>$resultArray]);
		}

		return response()->json(['code'=>400,'status'=>false,'service_name'=>'Add-diagnostic-answer','message'=>'Diagnostic answer added successfully.','data'=>$resultArray]);

	}
	/*check tribeometer and diagnostic answers done or not*/
	public function isDiagnosticTribeometerAnswerDone()
	{
		$resultArray =array();

		$user = Auth::user();
		$userId = $user->id;

		$isDiagnosticAnsDone  = DB::table('diagnostic_answers')->where('userId',$userId)->first();

		$isTribeometerAnsDone = DB::table('tribeometer_answers')->where('userId',$userId)->first();

		$resultArray['isDiagnosticAnsDone'] = false;
		if($isDiagnosticAnsDone)
		{
			$resultArray['isDiagnosticAnsDone'] =true;
		} 

		$resultArray['isTribeometerAnsDone'] = false;
		if($isTribeometerAnsDone)
		{
			$resultArray['isTribeometerAnsDone'] =true;
		} 


		return response()->json(['code'=>200,'status'=>true,'service_name'=>'Is-Diagnostic-Tribeometer-Answer-Done','message'=>'','data'=>$resultArray]);
	}

	/*get diagnostic completed answers*/
	public function getDiagnosticCompletedAnswers()
	{
		$resultArray = array();

		$user = Auth::user();
		$userId = $user->id;

		$ans = DB::table('diagnostic_answers')->where('userId',$userId)->first();

		if (empty($ans)) 
		{
			return response()->json(['code'=>400,'status'=>false,'service_name'=>'Diagnostic-question-answers','message'=>'Diagnostic answers not filled.','data'=>[]]);
		}
		
		$questionTbl = DB::table('diagnostic_questions')->select('id as questionId','question')->where('status','Active')->get();

		$queArray = array();

		foreach ($questionTbl as $value)
		{

			$optionsTbl = DB::table('diagnostic_question_options')->select('id AS optionId','option_name AS optionName')->where('status','Active')->get();
			$value->options = array();
			foreach ($optionsTbl as $optValue)
			{
				$diaAns = DB::table('diagnostic_answers')->where('questionId',$value->questionId)->where('userId',$userId)->where('optionId',$optValue->optionId)->first();

				$optValue->isChecked = false;
				if($diaAns)
				{
					$optValue->isChecked = true;
					$optValue->answerId = $diaAns->id;
					$value->answerId = $diaAns->id;
				}
				

				array_push($value->options, $optValue);
			}			

			array_push($queArray, $value);
		}		

		return response()->json(['code'=>200,'status'=>true,'service_name'=>'Diagnostic-question-answers','message'=>'Diagnostic question answers.','data'=>$queArray]);
	}


	public function updateDiagnosticAnswers()
	{
		$resultArray = array();
		$optionArr   = Input::get('answer');
		$user        = Auth::user();
		$userId      = $user->id; 

		foreach ($optionArr as  $value)
		{
			$optionRatingTbl = DB::table('diagnostic_question_options')->select('option_rating')->where('id',$value['optionId'])->first();

			$optionRating = 0;
			if (!empty($optionRatingTbl))
			{
				$optionRating = $optionRatingTbl->option_rating;
			}
			
			$updateArray = array(              
				'optionId'      => $value['optionId'],
				'answer'        => $optionRating,
				'updated_at'    => date('Y-m-d H:i:s'),
			);
			
			DB::table('diagnostic_answers')->where('id',$value['answerId'])->where('userId',$userId)->update($updateArray);
		} 
		return response()->json(['code'=>200,'status'=>true,'service_name'=>'Update-diagnostic-answer','message'=>'Diagnostic answer updated successfully.','data'=>$resultArray]);
	}


	/*get diagnostic reports*/
	public function getDiagnosticReport()
	{

		$resultArray = array();

		$orgId = Input::get('orgId');

		$diagnosticQueCatTbl = DB::table('diagnostic_questions_category')->get();

		//get user list		
		$users = DB::table('diagnostic_answers')
		->leftjoin('users','users.id','diagnostic_answers.userId')
		->select('diagnostic_answers.userId')
		->where('users.status','Active')
		->groupBy('diagnostic_answers.userId')
		->where('users.orgId',$orgId)
		->get();

		$userCount = count($users);
		
		if (empty($userCount))
		{
			return response()->json(['code'=>400,'status'=>false,'service_name'=>'diagnostic-report','message'=>'Diagnostic answers not done yet.','data'=>$resultArray]);
		}
		
		$optionsTbl = DB::table('diagnostic_question_options')->where('status','Active')->get();
		$diaOptCount = count($optionsTbl); 
		
		foreach($diagnosticQueCatTbl as $value)
		{
			$questionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

			$quecount = count($questionTbl);

			$perQuePercen = 0;
			foreach ($questionTbl as  $queValue)
			{
				$diaAnsTbl = DB::table('diagnostic_answers')			
				->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
				->leftJoin('users','users.id','diagnostic_answers.userId')
				->where('users.status','Active')
				->where('diagnostic_answers.orgId',$orgId)
				->where('diagnostic_questions.id',$queValue->id)
				->where('diagnostic_questions.category_id',$value->id)
				->sum('answer');

				$perQuePercen += ($diaAnsTbl/$userCount);						
			}


			$score = ($perQuePercen/($quecount*$diaOptCount));
			$totalPercentage = ($perQuePercen/($quecount*$diaOptCount))*100;

			$value1['title']      =  $value->title;
			$value1['categoryId'] =  $value->id;
			$value1['score']      =  number_format((float)$score, 2, '.', '');
			$value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');			

			array_push($resultArray, $value1);
		}
		

		return response()->json(['code'=>200,'status'=>true,'service_name'=>'diagnostic-report','message'=>'','data'=>$resultArray]);

	}
}
