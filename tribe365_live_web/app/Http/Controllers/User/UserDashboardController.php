<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use DateTime;
use Mail;
use Config;
use DatePeriod;
use DateInterval;

class UserDashboardController extends Controller
{
  	public function index(Request $request) {

  		$a = app('App\Http\Controllers\Admin\CommonController')->checkAdmin(Auth::user());
	    if (!empty($a)) {
	      	return redirect('/');
	    }

  		$resultArray = array();

  		if(empty(Auth::user())) {
            return redirect('/');
        }

		$orgId = Auth::user()->orgId;
		$officeId = $request->officeId;
	    $departmentId = $request->departmentId;

	    if(isset($request->ispdf)){
	      	$ispdf = $request->ispdf;  
	    }else{
	      	$ispdf = 2;
	    }

	    //Get weekend flag from organisation
	    $organisation = DB::table('organisations')->where('status','Active')->where('id',$orgId)->first();

	    $includeWeekend = $organisation->include_weekend;

	    $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get();

	    $all_department = DB::table('all_department')->where('status','Active')->get();

	    $departments = [];

	    if (!empty($officeId)) {
			$departments = DB::table('departments')
				->select('departments.id','all_department.department')
				->leftJoin('all_department','all_department.id','departments.departmentId')
				->leftJoin('users','users.departmentId','departments.id')
				->where('departments.officeId',$officeId) 
				->where('all_department.status','Active') 
				->where('departments.status','Active') 
				->where('users.status','Active') 
				->orderBy('all_department.department','ASC')
				->groupBy('all_department.department','departments.id')->get();
	    }elseif (empty($officeId)) {
			$all_department = DB::table('departments')
				->select('all_department.id','all_department.department')
				->leftJoin('all_department','all_department.id','departments.departmentId')
				->leftJoin('users','users.departmentId','departments.id')
				->where('departments.orgId',$orgId) 
				->where('departments.status','Active') 
				->where('all_department.status','Active') 
				->where('users.status','Active') 
				->orderBy('all_department.department','ASC')
				->groupBy('all_department.department','all_department.id')->get();
	    }

	    for ($i = 11; $i >= 0; $i--) {
	      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
	    }

	    $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();

	    //Culture Index graph
	    $cultureIndexPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend, 'months'=>$months,'index'=>1,'ispdf'=>$ispdf);
	    $resultArray['cultureIndexArray'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getCultureIndexValues($cultureIndexPer);

	    //Engagement Index graph
	    $engagementIndexPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend, 'months'=>$months,'index'=>2,'ispdf'=>$ispdf);
	    $resultArray['engagementIndexArray'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getCultureIndexValues($engagementIndexPer);

	    //Happy Index graph
	    $happyIndexPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend, 'months'=>$months,'ispdf'=>$ispdf);
	    $resultArray['happyIndexArray'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getHappyIndexValues($happyIndexPer);

	    //Diagnostic graph
	    $diagnosticPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend, 'months'=>$months,'ispdf'=>$ispdf);
	    $resultArray['diagnosticArray'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getDiagnosticValues($diagnosticPer);

	    // $diagnosticQueCatTbl = DB::table('diagnostic_questions_category')->get();

	    //DOT graph
	    $dotPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend,'months'=>$months,'ispdf'=>$ispdf);
	    $resultArray['dotArray'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getDOTBeliefs($dotPer);

	    //Thumbsup graph
	    $thumbsupPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend,'months'=>$months);
	    $resultArray['thumbsupArray'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getThumbsupBeliefs($thumbsupPer);

	    // echo "<pre>";print_r($resultArray['thumbsupArray']);die();

	    //Team Role graph
	    $teamRolePer = array('orgId'=>$orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend,'months'=>$months);
	    $resultArray['teamRoleArray'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getTeamRoleValues($teamRolePer);

	    //Tribeometer graph
	    $tribeometerPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend,'months'=>$months,'ispdf'=>$ispdf);
	    $resultArray['tribeometerArray'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getTribeometerValues($tribeometerPer);

	    // $queCatTbl      = DB::table('tribeometer_questions_category')->get();

	    //Motivation graph
	    $motivationPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend,'months'=>$months,'ispdf'=>$ispdf);
	    $resultArray['motivationArray'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getMotivationValues($motivationPer);

	    // $categoryTbl   = DB::table('sot_motivation_value_records')->where('status','Active')->get();

	    //Personality Type graph
	    $personalityTypePer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend,'months'=>$months,'ispdf'=>$ispdf);
	    $resultArray['personalityTypeArray'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getPersonalityTypeValues($personalityTypePer);

	    //Culture structure graph
	    $cultureStructurePer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend, 'months'=>$months,'ispdf'=>$ispdf);
	    $resultArray['cultureStructureArray'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getCultureStructureValues($cultureStructurePer);

	    //Happy index responders graph
	    $happyIndexResPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend, 'months'=>$months,'ispdf'=>$ispdf);
	    $resultArray['happyIndexResArray'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getHappyIndexRespValues($happyIndexResPer);

	    //Dot Values Completed
	    $dotValuesPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);
	    $dotComplete = app('App\Http\Controllers\Admin\AdminDashboardController')->dotValuesComplete($dotValuesPer);

	    $resultArray['dotComplete'] = $dotComplete['dotCompleted'];
	    $resultArray['dotRatingCount'] = $dotComplete['dotRatingCount'];

	    //Team Role Completed
	    $teamRolePerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);
	    $resultArray['teamRoleComplete'] = app('App\Http\Controllers\Admin\AdminDashboardController')->teamRoleComplete($teamRolePerArr);

	    //Teamrole MM and MA count
	    $resultArray['teamRoleCompletedMMMA'] = 0;
	    if (!empty($staff)) {
	      $resultArray['teamRoleCompletedMMMA'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getTeamroleMAMMCount($teamRolePerArr);
	    }
	    // else
	    // {
	    //   $resultArray['teamRoleCompletedMMMA']['MM'] = 0;
	    //   $resultArray['teamRoleCompletedMMMA']['MA'] = 0;
	    // }

	    //Personality Type Completed
	    $personalityTypePerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);
	    $resultArray['personalityTypeComplete'] = app('App\Http\Controllers\Admin\AdminDashboardController')->personalityTypeComplete($personalityTypePerArr);

	    //Personality type MM and MA count
	    $personalityTypeCompletedArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);

	    $resultArray['personalityTypeCompletedMAMM'] = 0;
	    if (!empty($staff)) {
	      $resultArray['personalityTypeCompletedMAMM'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getPersonalityTypeMAMMCount($personalityTypeCompletedArr);
	    }
	    else
	    {
	      $resultArray['personalityTypeCompletedMAMM']['MM'] == 0;
	      $resultArray['personalityTypeCompletedMAMM']['MA'] == 0;
	    }

	    //Motivation Completed
	    $motivationPerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);
	    $resultArray['motivationComplete'] = app('App\Http\Controllers\Admin\AdminDashboardController')->motivationComplete($motivationPerArr);

	    //Motivation MM and MA count
	    $motivationCompletedArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);

	    $resultArray['motivationCompletedMAMM'] = 0;
	    if (!empty($staff)) {
	      $resultArray['motivationCompletedMAMM'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getMotivationMAMMCount($motivationCompletedArr);
	    }
	    else
	    {
	      $resultArray['motivationCompletedMAMM']['MM'] == 0;
	      $resultArray['motivationCompletedMAMM']['MA'] == 0;
	    }

	    //Kudos Completed yesterday
	    $kudosPerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);
	    $resultArray['kudosComplete'] = app('App\Http\Controllers\Admin\AdminDashboardController')->kudosComplete($kudosPerArr);

	    //MA and MM for bubble rating
	    $bubbleRatingData = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);

	    $resultArray['bubbleRating'] = 0;
	    if (!empty($staff)) {
	      $resultArray['bubbleRating'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getBubbleRatingCount($bubbleRatingData);
	    }
	    else
	    {
	      $resultArray['bubbleRating']['DD'] == 0;
	      $resultArray['bubbleRating']['MA'] == 0;
	    }

	    //Happy index Completed yesterday
	    $happyIndexPerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);
	    $resultArray['happyIndexComplete'] = app('App\Http\Controllers\Admin\AdminDashboardController')->happyIndexComplete($happyIndexPerArr);

	    //MA and MM for Happy Index
	    $happyIndexArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);

	    $resultArray['happyIndexDDMA'] = 0;
	    if (!empty($staff)) {
	      $resultArray['happyIndexDDMA'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getHappyIndexMMMA($happyIndexArr);
	    }
	    else
	    {
	      $resultArray['happyIndexDDMA']['DD'] == 0;
	      $resultArray['happyIndexDDMA']['MA'] == 0;
	    }

	    //Improvements Last month
	    $improvementPerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);
	    $resultArray['improvements'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getImprovements($improvementPerArr);

	    //MA and MM for improvements
	    $improvementArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);

	    $resultArray['improvementsMAMM'] = 0;
	    if (!empty($staff)) {
	      $resultArray['improvementsMAMM'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getImprovementsMMMA($improvementArr);
	    }else{
	      $resultArray['improvementsMAMM']['MM'] == 0;
	      $resultArray['improvementsMAMM']['MA'] == 0;
	    }

	    //Culture Structure Updated Last month
	    $cultureStructurePerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);
	    $resultArray['cultureStructure'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getCultureStructure($cultureStructurePerArr);

	    //culture  structure MM and MA count
	    $cultureStructureCompletedArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);

	    $resultArray['cultureStructureCompletedMAMM'] = 0;
	    if (!empty($staff)) {
	      $resultArray['cultureStructureCompletedMAMM'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getCultureStructureMAMMCount($cultureStructureCompletedArr);
	    }else{
	      $resultArray['cultureStructureCompletedMAMM']['MM'] == 0;
	      $resultArray['cultureStructureCompletedMAMM']['MA'] == 0;
	    }

	    //Tribeometer Updated Last month
	    $tribeometerPerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);
	    $resultArray['tribeometer'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getTribeometer($tribeometerPerArr);

	    //Tribeometer MM and MA count
	    $tribeometerUpdateArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);

	    $resultArray['tribeometerUpdatedMAMM'] = 0;
	    if (!empty($staff)) {
	      $resultArray['tribeometerUpdatedMAMM'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getTribeometerMAMMCount($tribeometerUpdateArr);
	    }
	    else
	    {
	      $resultArray['tribeometerUpdatedMAMM']['MM'] == 0;
	      $resultArray['tribeometerUpdatedMAMM']['MA'] == 0;
	    }

	    //Diagnostic Updated Last month
	    $diagnosticPerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);
	    $resultArray['diagnostic'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getDiagnostics($diagnosticPerArr);

	    //Diagnostic MM and MA count
	    $diagnosticUpdateArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);

	    $resultArray['diagnosticUpdatedMAMM'] = 0;
	    if (!empty($staff)) {
	      $resultArray['diagnosticUpdatedMAMM'] = app('App\Http\Controllers\Admin\AdminDashboardController')->getDiagnosticMAMMCount($diagnosticUpdateArr);
	    }
	    else
	    {
	      $resultArray['diagnosticUpdatedMAMM']['MM'] == 0;
	      $resultArray['diagnosticUpdatedMAMM']['MA'] == 0;
	    }

	    if($ispdf==1){
	      	return view('user/userDashboardPdf',compact('ispdf','organisation','resultArray','orgId','offices','all_department','departments','departmentId','officeId','includeWeekend'));
	    }else{
	      	return view('user/userDashboard',compact('ispdf','organisation','resultArray','orgId','offices','all_department','departments','departmentId','officeId','includeWeekend'));
	    }
  	}
}
