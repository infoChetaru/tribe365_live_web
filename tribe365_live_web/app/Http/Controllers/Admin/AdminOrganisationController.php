<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Validator;
use Hash;
use Illuminate\Support\Facades\Input;
use Config;
use View;
use Mail;
use URL;
use File;

class AdminOrganisationController extends Controller
{

  public function __construct(Request $request)
  {
    $orgid = base64_decode($request->segment(3));

    $organisations = DB::table('organisations')
    ->select('ImageURL')
    ->where('id',$orgid)
    ->first();

    if(!empty($organisations->ImageURL))
    {
      $org_detail = base64_encode($orgid)."-".$organisations->ImageURL;

      View::share('org_detail', $org_detail);
    }else {

      $org_detail = "";

      View::share('org_detail', $org_detail);

    }

  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $a = app('App\Http\Controllers\Admin\CommonController')->checkUser(Auth::user());

      if (!empty($a)) {
        return redirect('/admin');
      }
      
      $controller = new CommonController();

      $organisations = DB::table('organisations')
      ->where('status','Active')
      ->orderBy('id','DESC')  
      ->paginate(6);

      $resultArray = array();
      foreach ($organisations as $value) {

        $result['id']           = $value->id;
        $result['organisation'] = $value->organisation;
        $result['industry']     = $value->industry;
        $result['ImageURL']     = $value->ImageURL;
        $result['include_weekend']     = $value->include_weekend;

        $result['numberOfOffices']= $department_array = $controller->callAction('getOfficeCount',array($value->id));
        $result['numberOfDepartments'] = $department_array = $controller->callAction('getDepartmentCount',array($value->id));

        array_push($resultArray, (object)$result);
      }

      $organisation = (object)$resultArray;
      
      return view('admin/organisation/organisation',compact('organisation'))->with('paginations',$organisations);
    }

    function organisation_pdf($orgid=FALSE){
        if (Input::has('orgid')){
          $orgid  = base64_decode(Input::get('orgid'));
        }else{
          $orgid  = base64_decode($orgid);
        }

        //echo $orgid;die;
        if (Input::has('searchByDate')){
            $searchByDate = Input::get('searchByDate');
            $organisations  = DB::table('organization_pdf_reports')
            ->where('organization_id',$orgid)
            ->where('created_at','LIKE',$searchByDate."%")
            ->orderBy('id','DESC')  
            ->paginate(6);
        }else{
          $organisations  = DB::table('organization_pdf_reports')
          ->where('organization_id',$orgid)
          ->orderBy('id','DESC')  
          ->paginate(6);
        }

        

        $resultArray  = array();
        $yearArray    = array();
        foreach ($organisations as $value) {
            $result['id']          = $value->organization_id;
            $result['file_name']   = $value->file_name;
            $result['created_at']  = $value->created_at;
            if(!in_array(date("Y", strtotime($value->created_at)),$yearArray)){
              array_push($yearArray, date("Y", strtotime($value->created_at)));
            }
            array_push($resultArray, (object)$result);
        }
        $organisation = $resultArray;        
        return view('admin/organisation/organisationPdf',compact('organisation','orgid','yearArray'))->with('paginations',$organisations);
    }


    function organisation_pdf_search($orgid=FALSE){
        if (Input::has('orgid')){
          $orgid  = base64_decode(Input::get('orgid'));
        }else{
          $orgid  = base64_decode($orgid);
        }

        //echo $orgid;die;
        if (Input::has('searchByDate')){
            $searchByDate = Input::get('searchByDate');
            $organisations  = DB::table('organization_pdf_reports')
            ->where('organization_id',$orgid)
            ->where('created_at','LIKE',$searchByDate."%")
            ->orderBy('id','DESC')  
            ->paginate(6);
        }else{
          $organisations  = DB::table('organization_pdf_reports')
          ->where('organization_id',$orgid)
          ->orderBy('id','DESC')  
          ->paginate(6);
        }

        $resultArray = array();
        $str='';
          foreach ($organisations as $value) {         
            $str.='<div class="col-md-3">';
            $str.='<div class="company-section">';
            $str.='<div class="company-detale">';               
            $str.='<a target="_BLANK" href='.URL::to("public/uploads/pdf_report/".$value->file_name).'>';
            //$str.='<img src='.URL::to("/public/images/pdf-img.jpeg").'>';           
            $str.='<h2>'.date("F Y", strtotime($value->created_at)).'</h2>';
            $str.='</a>';
            $str.='</div>';
            $str.='<div class="company-ovel-text">';
            $str.='<span>View Generate Report </span>';
            $str.='</div>';
            $str.='</div>';
            $str.='</div>';
        }
        echo $str;die;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $countryList = DB::table('country')->select('id','name','nicename')->where('status','Active')->get();

      return view('admin/organisation/addOrganisation',compact('countryList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $inputData =Input::get();
      // print_r($inputData);

      $rules = array('email'=>'unique:users');
      $Validator =  Validator::make(Input::all(),$rules);

      if($Validator->fails()) {

        return redirect()->back()->withInput(Input::all())->withErrors($Validator->errors());
      }

      /*
      $image = $request->file('org_logo'); 
      $imageName   = 'org_'.time().'.'.$image->getClientOriginalExtension();
      $destination = public_path('uploads/org_images/');
      $image->move($destination, $imageName);
      */

      $imageName = Input::get('org_logo');


      if(Input::get('org_include_weekend')==1){
        $org_include_weekend = Input::get('org_include_weekend');
      }else{
        $org_include_weekend = 2;
      }


      //add organisation data
      $insertArray = array(
        'organisation'=>Input::get('org_name'),
        'address1'=>Input::get('org_add1'),
        'address2'=>Input::get('org_add2')?Input::get('org_add2'):'',
        'address3'=>Input::get('address3')?Input::get('address3'):'',
        'postcode'=>452015,
        'industry'=>Input::get('org_industry'),
        'phone'=>Input::get('org_phone'),
        'turnover'=>Input::get('org_turnover'),
        'include_weekend'=>$org_include_weekend,
        'imageURL' => $imageName,
        'numberOfEmployees'=>0,
        'numberOfOffices'=>Input::get('office_count'),
        'numberOfDepartments'=>0,
        'lead_name'=>Input::get('lead_name'),
        'lead_email'=>Input::get('lead_email'),
        'lead_phone'=>Input::get('lead_phone'),
        'status'=>'Active',
        'superOrganisation'=>'N',
        'created_at'=>date('Y-m-d H:i:s'),
        'updated_at'=>date('Y-m-d H:i:s')
      );

      $organisationsId = DB::table('organisations')->insertGetId($insertArray);
      // $organisationsId = 1000;
      //get count of offices
      $office_count = Input::get('office_count');

      if(!empty($organisationsId)) {

        //add client in organisation
        // $name = ucfirst(Input::get('user_name'));
        // $surname = ucfirst(Input::get('surname'))?Input::get('surname'):'';
        // $password = Input::get('user_password');

        // $userInsertArray = array(
        //   'name'=>$name,          
        //   'email'=>Input::get('email'),
        //   'password'=>bcrypt($password),
        //   'password2'=>base64_encode($password),
        //   'orgId'=>$organisationsId,
        //   'status'=>Config::get('constants.STATUS_ACTIVE'),
        //   'roleId' => 3,
        //   // 'adminStatus'=>Config::get('constants.STATUS_SUPER_ADMIN'),
        //   'created_at'=>date('Y-m-d H:i:s'),
        //   'updated_at'=>date('Y-m-d H:i:s'));
        // $status = DB::table('users')->insertGetId($userInsertArray);

        //get total employee in organisation
        $total_emp = array();
        //get total departments 
        $total_dept =array();

        for ($i=1; $i <=$office_count ; $i++) {

          $office_name = Input::get('office_name'.$i);

          $office_noOf_employee = Input::get('office_noOf_employee'.$i);

          $office_dept_name_arr = Input::get('office_dept_name'.$i);

          $office_dept_no_of_emp = Input::get('dept_noOf_employee'.$i);
          $office_address = Input::get('office_address'.$i);
          $office_city = Input::get('office_city'.$i);
          $office_country = Input::get('office_country'.$i);
          $office_phone = Input::get('office_phone'.$i);

          $office_user_name_arr = Input::get('user_name'.$i);
          $office_user_last_name_arr = Input::get('user_last_name'.$i);
          $office_user_email_arr = Input::get('user_email'.$i);
          $office_user_password_arr = Input::get('user_password'.$i);
          $office_department_arr = Input::get('department'.$i);

          array_push($total_emp, $office_noOf_employee);

          array_push($total_dept, count($office_dept_name_arr));
          //add office detail
          $officeInsertArray = array(
            'office'=>ucfirst(Input::get('office_name'.$i)),
            'numberOfEmployees'=> $office_noOf_employee,
            'numberOfDepartments'=>count($office_dept_name_arr),
            'address'=>$office_address,
            'city'=>$office_city,
            'country'=>$office_country,
            'phone'=>$office_phone,
            'orgId'=>$organisationsId,
            'status'=>Config::get('constants.STATUS_ACTIVE'),
            'type'=>Config::get('constants.OFFICE_TYPE'),
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
          );
          if(isset($officeInsertArray))
          {
            $officeId = DB::table('offices')->insertGetId($officeInsertArray);
          }

          //get all office-department-here
          for ($j=0; $j<count($office_user_name_arr); $j++) { 




            $user_name =$office_user_name_arr[$j];           
            $user_last_name =$office_user_last_name_arr[$j];           
            $user_email = $office_user_email_arr[$j];
            $user_password = $office_user_password_arr[$j];
            $user_department = $office_department_arr[$j];

            $euser = DB::table('users')->where('email',$user_email)->where('status','Active')->first();

            if(empty($euser->email))
            {

            //add department
              $departmentsInsertArray = array(
                'officeId'    => $officeId,
                'departmentId'=> $user_department,
                'status'      => 'Active',
                'created_at'  => date('Y-m-d H:i:s'),
                'orgId'       => $organisationsId

              );

              $isDepartment = DB::table('departments')
              ->where('officeId',$officeId)
              ->where('departmentId',$user_department)
              ->first();    

              if(empty($isDepartment))    
              {
                $deptId = DB::table('departments')->insertGetId($departmentsInsertArray);
              }
              else
              {
                $deptId =  $isDepartment->id; 
              }

              $EmployeeInsertArray = array(
                'name'        => ucfirst($user_name),
                'lastName'    => ucfirst($user_last_name),
                'email'       => $user_email,
                // 'password'    => bcrypt($user_password),
                // 'password2'   => base64_encode($user_password),
                'status'      => Config::get('constants.STATUS_ACTIVE'),
                'officeId'    => $officeId,
                'orgId'       => $organisationsId,
                'departmentId'=> $deptId,
                'created_at'  => date('Y-m-d H:i:s'),
                'updated_at'  => date('Y-m-d H:i:s'),
                'roleId'      => 3
              );

              $euser = DB::table('users')->where('email',$user_email)->where('status','Active')->first();

              if(empty($euser->email))
              {
                $status = DB::table('users')->insertGetId($EmployeeInsertArray);
              }

              /*send email to new user*/
              $data  = array('name'=>$user_name,'lastName'=>$user_last_name ,'passwordLink'=>base64_encode($user_email));

              $email = $user_email;
              Mail::send('layouts/userPasswordSetEmail', $data, function($message) use($email) 
              {
                $message->from('notification@tribe365.co', 'Tribe365');
                $message->to($email);
                $message->subject('Verify your email address');
              });

            }

          }


        }

        //add aditional users
        $additional_user_count = Input::get('additional_user_count'); 

        for($i=1; $i<=$additional_user_count; $i++) { 

          $additional_user_name = Input::get('additional_user_name'.$i);
          $password = Input::get('additional_user_password'.$i);
          $surname = ucfirst(Input::get('surname'))?Input::get('surname'):'';
          
          // for ($j=0; $j <count($additional_user_name) ; $j++) { 

          //   $insertArray = array(
          //     'name'=>$additional_user_name[$j],
          //     'surname'=>$surname,
          //     'email'=>Input::get('email'),
          //     'password'=>bcrypt(Input::get('additional_user_password'.[$j])),
          //     'orgId'=>$orgId,
          //     'status'=>Config::get('constants.STATUS_ACTIVE'),
          //     'roleId' => 2,
          //     'adminStatus'=>Config::get('constants.STATUS_SUPER_ADMIN'),
          //     'created_at'=>date('Y-m-d H:i:s'),
          //     'updated_at'=>date('Y-m-d H:i:s'));
          //   $status = DB::table('clients')->insertGetId($insertArray);
          // }       
        }

      }

      $updateArray = array(     
        'numberOfDepartments'=>array_sum($total_dept),
        'numberOfEmployees'=>array_sum($total_emp),
      );

      DB::table('organisations')
      ->where('id',$organisationsId)
      ->update($updateArray);

      return redirect('/admin/admin-organisation')->with('message','Record Added Successfully.');

    }

    function cropOrganizationImg(){
      $destination = public_path('uploads/org_images/');
      $data = Input::get('image');
      list($type, $data) = explode(';', $data);
      list(, $data)      = explode(',', $data);
      $data = base64_decode($data);
      $imageName = 'org_'.rand().time().'.png';
      file_put_contents($destination.$imageName, $data);
      echo $imageName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function showOrganisationDetail($id) {

      $id = base64_decode($id);

      if(empty($id)){
        return redirect()->back();
      }

      $organisations = DB::table('organisations')
      ->where('id',$id)
      ->first();

      $dots = DB::table('dots')
      ->where('orgId',$id)
      ->first();
      
      return view('admin/organisation/detailOrganisation',compact('organisations','dots'));
    }

    public function edit($id)
    {
      $id = base64_decode($id);
      $record = DB::table('organisations')
      ->where('id',$id)
      ->where('status','Active')
      ->first();   

      // $user_record = DB::table('users')
      // ->where('orgId',$id)
      // ->where('roleId',2)
      // ->first(); 

      $offices = DB::table('offices')
      ->where('orgId',$id)
      ->where('status','Active')
      ->get();

      $countryList = DB::table('country')->select('id','name','nicename')->where('status','Active')->get();

      $data['record'] = $record;
     // $data['user_record'] = $user_record;
      $data['offices'] = $offices;

      $data['country'] = $countryList;

      // print_r($data['offices']);

      return view('admin/organisation/editOrganisation',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
     //  $updateArray =  array('organisation'=>Input::get('organisation'),
     //   'address1'=>Input::get('address1'),
     //   'address2'=>Input::get('address2')?Input::get('address2'):'',
     //   'address3'=>Input::get('address3')?Input::get('address3'):'',
     //   'postcode'=>Input::get('postcode'),
     //   'industry'=>Input::get('industry'),
     //   'phone'=>Input::get('phone'),
     //   'turnover'=>Input::get('turnover'),
     //   'updated_at'=>date('Y-m-d H:i:s')
     // );
     //  $status = DB::table('organisations')
     //  ->where('id',$id)
     //  ->update($updateArray);
     //  if($status) 
     //  {
     //   return redirect('admin/admin-organisation')->with('message','Record Updated Successfully');
     // }
    }

    public function organisation_update(Request $request)
    {
      $postData = Input::get();

      /*
      if($request->hasfile('org_images')){
        $image = $request->file('org_images'); 
        $imageName   = 'org_'.time().'.'.$image->getClientOriginalExtension();
        $destination = public_path('uploads/org_images/');
        $image->move($destination, $imageName);
        $tableData['ImageURL'] = $imageName;
      }
      */
      
      $tableData['ImageURL'] = Input::get('org_logo');



      if(Input::get('org_include_weekend')==1){
        $org_include_weekend = Input::get('org_include_weekend');
      }else{
        $org_include_weekend = 2;
      }
      $tableData['include_weekend'] = $org_include_weekend;

      $tableData['organisation'] = $postData['org_name'];
      $tableData['turnover'] = $postData['org_turnover'];
      $tableData['industry'] = $postData['org_industry'];


      $status = DB::table('organisations')->where('id',$postData['orgId'])->update($tableData);
      
      echo"SUCCESS";    
    }

    public function organisation_update1(){
      $postData = Input::get();

      $tableData['address1'] = $postData['org_address1'];
      $tableData['address2'] = $postData['org_address2'];
      $tableData['phone']    = $postData['org_phone'];
      //$tableData1['email'] = $postData['org_email'];
      $status = DB::table('organisations')
      ->where('id',$postData['orgId'])
      ->update($tableData);
      // $status1 = DB::table('users')
      // ->where('orgId',$postData['orgId'])
      // ->where('roleId',2)
      // ->update($tableData1);

    // if($status) 
    // {
      echo"SUCCESS";
   // }

    }

    public function organisation_update2(){
      $postData = Input::get();

      $tableData['name'] = $postData['client_name'];
      $tableData['password2'] = base64_encode($postData['client_password']);

      $status = DB::table('users')
      ->where('orgId',$postData['orgId'])
      ->where('roleId',2)
      ->update($tableData);

  // if($status) 
  // {
      echo"SUCCESS";
 // }

    }


    public function organisation_update3(){
      $postData = Input::get();

      $tableData['office'] = $postData['office_name'];
      $tableData['numberOfEmployees'] = $postData['no_employee'];
      $tableData['address'] = $postData['address'];
      $tableData['city'] = $postData['city'];
      $tableData['country'] = $postData['country'];
      $tableData['phone'] = $postData['phone'];

      $status = DB::table('offices')
      ->where('id',$postData['ofcId'])
      ->update($tableData);

  // if($status) 
  // {
      echo"SUCCESS";
 // }

    }


    public function organisation_update4(){
      $postData = Input::get();

      $tableData['lead_name'] = $postData['lead_name'];
      $tableData['lead_email'] = $postData['lead_email'];
      $tableData['lead_phone'] = $postData['lead_phone'];
      

      $status = DB::table('organisations')
      ->where('id',$postData['orgId'])
      ->update($tableData);

  // if($status) 
  // {
      echo"SUCCESS";
 // }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //   $updateArray = array('status'=>'Inactive');
    //   $status = DB::table('clients')
    //   ->where('orgId',$id)
    //   ->update($updateArray);
    //   $status = DB::table('departments')
    //   ->where('orgId',$id)
    //   ->update($updateArray);
    //   $status = DB::table('offices')
    //   ->where('orgId',$id)
    //   ->update($updateArray);
    //   $updateArray = array('status'=>'Inactive','numberOfOffices'=>0,'numberOfEmployees'=>0,'numberOfDepartments'=>0);          
    //   $status = DB::table('organisations')
    //   ->where('id',$id)
    //   ->update($updateArray);
    //   if($status)
    //   {
    //     return redirect('/admin/admin-organisation')->with('message','Record Deleted Successfully');
    //   }
    // }

    public function addNewDepartment()
    {
      $postData = Input::get();

      $department_name = Input::get('department');

      $department = DB::table('departments')
      ->where('department',$department_name)
      ->get();


      if (count($department)) {

        $data = array('response'=>'error','message'=>'Department already exists.');
        return $data;

      } else {

        $insertArray = array(
          'department'=> $department_name,    
          'numberOfEmployees'=> 1,
          'officeId'=> 62,
          'orgId'=> 36     
        );

        $status = DB::table('departments')->insertGetId($insertArray);

        // $data = array('response'=>'success','message'=>'Department added successfully');

        $data['response'] = 'success';
        $data['message']  = 'Department added successfully';


        $controller = new CommonController();

        $department_array = $controller->callAction('getAllDepartments',array());
        
        $data['department_array'] = $department_array;

        return $data;
      }


    }

    public function getOrganisationDetail(){

      return view('admin/organisation/organisationDetail');
    }


    //inactive organisation
    public function deleteOrganisation()
    {

      $orgId = Input::get('orgId');

      $updateArray = array('status'=>'Inactive');          

      $status = DB::table('organisations')
      ->where('id',$orgId)
      ->update($updateArray);

      if($status)
      {
        return redirect('admin/admin-organisation')->with('message','Record Deleted Successfully');
      }
    }

      //Delete organisation
    public function deleteWholeOrganisation(Request $request)
    {
      $orgId = $request->orgId;

      //Delete image of organisation from folder
      $image = DB::table('organisations')->where('id',$orgId)->first();
      $imgUrl = $image->ImageURL;
      
      $imagePath = public_path('uploads/org_images/'.$imgUrl);
      if(File::exists($imagePath)) {
         File::delete($imagePath);
      }

      $data['actions'] = DB::table('actions')->where('orgId',$orgId)->delete();

      $data['actions_comment'] = DB::table('actions_comment')
        ->leftJoin('users','users.id','actions_comment.userId')
        ->where('users.orgId',$orgId)->delete();

      $data['assets'] = DB::table('assets')->where('orgId',$orgId)->delete();

      $data['clients'] = DB::table('clients')->where('orgId',$orgId)->delete();

      $data['cot_answers'] = DB::table('cot_answers')->where('orgId',$orgId)->delete();

      $data['cot_functional_lens_answers'] = DB::table('cot_functional_lens_answers')->where('orgId',$orgId)->delete();

      $data['cot_functional_lens_initial_value_records'] = DB::table('cot_functional_lens_initial_value_records')->where('orgId',$orgId)->delete();

      $data['cot_functional_lens_records'] = DB::table('cot_functional_lens_records')
        ->leftJoin('users','users.id','cot_functional_lens_records.userId')
        ->where('users.orgId',$orgId)->delete();

      $data['cot_personality_type_dashboard_graph'] = DB::table('cot_personality_type_dashboard_graph')->where('orgId',$orgId)->delete();

      $data['cot_team_role_dashboard_graph'] = DB::table('cot_team_role_dashboard_graph')->where('orgId',$orgId)->delete();

      $data['departments'] = DB::table('departments')->where('orgId',$orgId)->delete();

      $data['diagnostic_answers'] = DB::table('diagnostic_answers')->where('orgId',$orgId)->delete();

      $data['diagnostic_report_graph'] = DB::table('diagnostic_report_graph')->where('orgId',$orgId)->delete();

      $data['diagnostic_report_subgraph'] = DB::table('diagnostic_report_subgraph')->where('orgId',$orgId)->delete();

      $data['dots_values'] = DB::table('dots_values')
        ->leftJoin('dots_beliefs','dots_values.beliefId','dots_beliefs.id')
        ->leftJoin('dots','dots.id','dots_beliefs.dotId')
        ->where('dots.orgId',$orgId)->delete();

      $data['dots_beliefs_delete'] = DB::table('dots_beliefs')
        ->leftJoin('dots','dots.id','dots_beliefs.dotId')
        ->where('dots.orgId',$orgId)->delete();

      $data['dots'] = DB::table('dots')->where('orgId',$orgId)->delete();

      $data['dot_belief_graph'] = DB::table('dot_belief_graph')->where('orgId',$orgId)->delete();

      $data['dot_bubble_rating_records'] = DB::table('dot_bubble_rating_records')
        ->leftJoin('dots','dots.id','dot_bubble_rating_records.dot_id')
        ->where('dots.orgId',$orgId)->delete();

      $data['dot_evidence'] = DB::table('dot_evidence')
        ->leftJoin('dots','dots.id','dot_evidence.dotId')
        ->where('dots.orgId',$orgId)->delete();

      $data['dot_values_graph'] = DB::table('dot_values_graph')->where('orgId',$orgId)->delete();

      $data['dot_values_ratings'] = DB::table('dot_values_ratings')
        ->leftJoin('dots','dots.id','dot_values_ratings.dotId')
        ->where('dots.orgId',$orgId)->delete();

      $data['happy_indexes'] = DB::table('happy_indexes')
        ->leftJoin('users','users.id','happy_indexes.userId')
        ->where('users.orgId',$orgId)->delete();

      $data['happy_index_dashboard_graph'] = DB::table('happy_index_dashboard_graph')->where('orgId',$orgId)->delete();

      $data['happy_index_dashboard_responders_graph'] = DB::table('happy_index_dashboard_responders_graph')->where('orgId',$orgId)->delete();

      $data['indexReportRecords'] = DB::table('indexReportRecords')->where('orgId',$orgId)->delete();

      $data['iot_allocated_themes'] = DB::table('iot_allocated_themes')
        ->leftJoin('iot_feedbacks','iot_feedbacks.id','iot_allocated_themes.feedbackId')
        ->leftJoin('iot_themes','iot_themes.id','iot_allocated_themes.themeId')
        ->where('iot_feedbacks.orgId',$orgId)
        ->where('iot_themes.orgId',$orgId)->delete();

      $data['iot_messages'] = DB::table('iot_messages')
        ->leftJoin('iot_feedbacks','iot_feedbacks.id','iot_messages.feedbackId')
        ->where('iot_feedbacks.orgId',$orgId)->delete();

      $data['iot_notifications'] = DB::table('iot_notifications')
        ->leftJoin('iot_feedbacks','iot_feedbacks.id','iot_notifications.feedbackId')
        ->where('iot_feedbacks.orgId',$orgId)->delete();

      $data['iot_feedbacks'] = DB::table('iot_feedbacks')->where('orgId',$orgId)->delete();

      $data['iot_themes'] = DB::table('iot_themes')->where('orgId',$orgId)->delete();

      $data['iot_theme_notes'] = DB::table('iot_theme_notes')
        ->leftJoin('iot_themes','iot_themes.id','iot_theme_notes.themeId')
        ->where('iot_themes.orgId',$orgId)->delete();
       
      $data['kudos_belief_dashboard_graph'] = DB::table('kudos_belief_dashboard_graph')->where('orgId',$orgId)->delete();

      $data['kudos_leaderboard_graph'] = DB::table('kudos_leaderboard_graph')->where('orgId',$orgId)->delete();

      $data['kudos_values_dasboard_graph'] = DB::table('kudos_values_dasboard_graph')->where('orgId',$orgId)->delete();

      $data['oauth_access_tokens'] = DB::table('oauth_access_tokens')
        ->leftJoin('users','users.id','oauth_access_tokens.user_id')
        ->where('users.orgId',$orgId)->delete();

      $data['oauth_auth_codes'] = DB::table('oauth_auth_codes')
        ->leftJoin('users','users.id','oauth_auth_codes.user_id')
        ->where('users.orgId',$orgId)->delete();

      $data['oauth_clients'] = DB::table('oauth_clients')
        ->leftJoin('users','users.id','oauth_clients.user_id')
        ->where('users.orgId',$orgId)->delete();

      $data['offices'] = DB::table('offices')->where('orgId',$orgId)->delete();

      $data['organisations'] = DB::table('organisations')->where('id',$orgId)->delete();

      $data['organization_pdf_reports'] = DB::table('organization_pdf_reports')->where('organization_id',$orgId)->delete();

      $data['personality_type_answers'] = DB::table('personality_type_answers')->where('orgId',$orgId)->delete();

      $data['personality_type_dashboard_graph'] = DB::table('personality_type_dashboard_graph')->where('orgId',$orgId)->delete();

      $data['personality_type_dashboard_subgraph'] = DB::table('personality_type_dashboard_subgraph')->where('orgId',$orgId)->delete();

      $data['sot_answers'] = DB::table('sot_answers')
        ->leftJoin('users','users.id','sot_answers.userId')
        ->where('users.orgId',$orgId)->delete();

      $data['sot_culture_structure_dashboard_graph'] = DB::table('sot_culture_structure_dashboard_graph')->where('orgId',$orgId)->delete();

      $data['sot_motivation_answers'] = DB::table('sot_motivation_answers')->where('orgId',$orgId)->delete();

      $data['sot_motivation_dashboard_graph'] = DB::table('sot_motivation_dashboard_graph')->where('orgId',$orgId)->delete();

      $data['sot_motivation_user_records'] = DB::table('sot_motivation_user_records')
        ->leftJoin('users','users.id','sot_motivation_user_records.userId')
        ->where('users.orgId',$orgId)->delete();

      $data['tribeometer_answers'] = DB::table('tribeometer_answers')->where('orgId',$orgId)->delete();

      $data['tribeometer_dashboard_graph'] = DB::table('tribeometer_dashboard_graph')->where('orgId',$orgId)->delete();

      $data['update_date_performance_dashboard'] = DB::table('update_date_performance_dashboard')->where('orgId',$orgId)->delete();

      $data['users'] = DB::table('users')->where('orgId',$orgId)->delete();

      $data['users_daily_count'] = DB::table('users_daily_count')->where('orgId',$orgId)->delete();

      return json_encode(array('status'=>200,'message'=>'Organisation Successfully Deleted'));
    }


    /*public function listOffice($orgId)
    {
     $orgId = base64_decode($orgId);
     $office = DB::table('offices')
     ->where('orgId',$orgId)
     ->get();

     return view('admin/office/office',compact('office'));


   }*/

   public function listOffice($orgId)
    {
     $orgId = base64_decode($orgId);
     $office = DB::table('offices')
     ->select('offices.office','offices.address','offices.city','offices.country','offices.phone','offices.numberOfEmployees','country.id','country.nicename')
     ->leftJoin('country','country.id','offices.country')
     ->where('offices.status','Active')
     ->where('orgId',$orgId)
     ->get();

     return view('admin/office/office',compact('office'));
   }

   /*public function addOffice($orgId)
   {
    $orgId = base64_decode($orgId);

    return view('admin/office/addOffice',compact('orgId'));


  }*/

  public function addOffice($orgId)
  {
    $orgId = base64_decode($orgId);

    $countryList = DB::table('country')->select('id','name','nicename')->where('status','Active')->get();

    return view('admin/office/addOffice',compact('orgId','countryList'));
  }

  public function createOffice()
  {

    $postData = Input::get();
    $insertArray = array(
      'office'=> $postData['name'],    
      'numberOfEmployees'=> $postData['nemp'],
      'address'=> $postData['office_address'],
      'city'=> $postData['office_city'],
      'country'=> $postData['office_country'],
      'phone'=> $postData['office_phone'],
      'orgId'=>$postData['orgId'],
      'numberOfDepartments'=>0,
      'status'=>'Active',
      'created_at'=>date('Y-m-d H:i:s'),


    );


    $status = DB::table('offices')->insertGetId($insertArray);


    return redirect('admin/list-office/'.base64_encode($postData['orgId']))->with('message','Office Added Successfully');

  }

  public function kudosChamp()
  {
    return view('layouts/kudosChampion');
  }

}
