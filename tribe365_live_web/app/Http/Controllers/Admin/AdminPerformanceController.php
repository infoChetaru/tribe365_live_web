<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Validator;
use Hash;
use Illuminate\Support\Facades\Input;
use Config;
use View;
use Carbon\Carbon;
use DateTime;

class AdminPerformanceController extends Controller
{

  public function __construct()
  {
    $a = app('App\Http\Controllers\Admin\CommonController')->checkUser(Auth::user());

    if (!empty($a)) {
      return redirect('/admin');
    }
  }

   //  public function getPerformance()
   //  {

   //     $resultArray = array();

   //     $orgId = Input::get('orgId');
   //     $reportType = Input::get('reportType');
   //     $date = Input::get('date');

   //     $month ='';
   //     $year  ='';
   //     $currentDate = '';
   //     if($reportType == 'daily') 
   //     {
   //      $currentDate = date('Y-m-d');          
   //  }       
   //  else if($reportType == 'monthly')
   //  {
   //     $month = date('m', strtotime($date));
   //     $year  = date('Y', strtotime($date));
   // }


   // $organisations = DB::table('organisations')->where('status','Active')->orderBy('organisation','ASC')->get();

   // $usersQuery = DB::table('users')->where('status','Active');

   // if(!empty($orgId))
   // {
   //      $usersQuery->where('orgId',$orgId);
   //  }

   //  $users = $usersQuery->get();

   //  $userCount = count($users);



   //  $query = DB::table('dot_bubble_rating_records')
   //  ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
   //  ->where('dots.status','Active')
   //  ->where('dot_bubble_rating_records.status','Active');                  

   //  if(!empty($orgId))
   //  {
   //      $query->where('dots.orgId',$orgId);
   //  }

   //  if(!empty($currentDate))
   //  {
   //     $query->whereDate('dot_bubble_rating_records.created_at',$currentDate);
   //  }

   //  if(!empty($month) && !empty($year))
   //  { 
   //      $query->whereMonth('dot_bubble_rating_records.created_at', $month);
   //      $query->whereYear('dot_bubble_rating_records.created_at', $year);
   //  }

   //  $bubbleCount = $query->count('bubble_flag');

   //  $resultArray['thumbCompleted'] = "0";
   //  if(!empty($bubbleCount) && !empty($userCount))
   //  {
   //      $resultArray['thumbCompleted'] = number_format(($bubbleCount/$userCount), 2);
   //  }

   //  //MA and MM for bubble rating
   //  $bubbleRatingData = array('orgId' => $orgId);

   //  $resultArray['bubbleRating'] = $this->getBubbleRatingCount($bubbleRatingData);

   //  //print_r($resultArray['bubbleRating']);die();

   //  //Improvements 

   //  $improvementQuery = DB::table('iot_feedbacks')
   //  ->where('status','!=','Inactive');
   //  if(!empty($orgId))
   //  {
   //      $improvementQuery->where('orgId',$orgId);
   //  }

   //  if(!empty($currentDate))
   //  {
   //     $improvementQuery->whereDate('iot_feedbacks.created_at',$currentDate);
   //  }

   //  if(!empty($month) && !empty($year))
   //  { 
   //      $improvementQuery->whereMonth('iot_feedbacks.created_at', $month);
   //      $improvementQuery->whereYear('iot_feedbacks.created_at', $year);
   //  }

   //  $improvements = $improvementQuery->count();
   //  //print_r($improvements);die();

   //  $resultArray['improvements'] = "0";
   //  if(!empty($improvements) && !empty($userCount))
   //  {
   //      $resultArray['improvements'] = number_format(($improvements/$userCount), 2);
   //  }

   //  //MA and MM for improvements
   //  $improvementArr = array('orgId' => $orgId);

   //  $resultArray['improvementsMAMM'] = $this->getImprovementsMMMA($improvementArr);


   //  /*DOT value complete*/
   //  $dotValueRatingCompletedUserArr = array();
   //  $dotValueRatingUpdatedUserArr   = array();

   //  foreach($users as $duValue)
   //  {

   //      //check DOT rating is given by user on every dot value if it is yes then true
   //      $dotPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);

   //      $status = $this->getDotPerformance($dotPerArr);

   //      if($status)
   //      {
   //          array_push($dotValueRatingCompletedUserArr, $duValue->id);      
   //      }

   //      //for updated
   //      $dotPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);

   //      $uStatus = $this->getDotPerformance($dotPerArr);

   //      if($uStatus)
   //      {
   //          array_push($dotValueRatingUpdatedUserArr, $duValue->id);        
   //      }
   //  }



   //          //for complete
   //  $dotValueRatingCompletedUserArrCount = count($dotValueRatingCompletedUserArr);

   //  $resultArray['dotCompleted'] = "0";
   //  if(!empty($dotValueRatingCompletedUserArrCount) && !empty($userCount))
   //  {
   //      $resultArray['dotCompleted'] = number_format(($dotValueRatingCompletedUserArrCount/$userCount)*100,2);
   //  }

   //          //for update
   //  $dotValueRatingUpdatedUserArrCount = count($dotValueRatingUpdatedUserArr);

   //  $resultArray['dotUpdated'] = "0";
   //  if(!empty($dotValueRatingUpdatedUserArrCount) && !empty($userCount))
   //  {
   //      $resultArray['dotUpdated'] = number_format(($dotValueRatingUpdatedUserArrCount/$userCount)*100,2);
   //  }

   //  //Dot MM and MA count

   //  $dotRatingData = array('orgId' => $orgId,'date' => 'created_at');

   //  $resultArray['dotRatingCompleted'] = $this->getDotRatingCount($dotRatingData);

   //  $dotRatingData = array('orgId' => $orgId,'date' => 'updated_at');

   //  $resultArray['dotRatingUpdated'] = $this->getDotRatingCount($dotRatingData);



   //          //for team role map comple
   //  $teamPerArr = array('orgId' => $orgId,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
   //  $teamRoleStatus = $this->getTeamRoleMap($teamPerArr);

   //  $temRoleMapCount = count($teamRoleStatus);

   //  $resultArray['tealRoleMapCompleted'] = "0";
   //  if (!empty($temRoleMapCount) && !empty($userCount))
   //  {
   //      $resultArray['tealRoleMapCompleted'] = number_format(($temRoleMapCount/$userCount)*100,2);
   //  }

   //          //for team role map update
   //  $teamPerUpdateArr = array('orgId' => $orgId,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
   //  $teamRoleUpdateStatus = $this->getTeamRoleMap($teamPerUpdateArr);

   //  $temRoleMapUpdateCount = count($teamRoleUpdateStatus);

   //  $resultArray['tealRoleMapUpdated'] = "0";
   //  if (!empty($temRoleMapUpdateCount) && !empty($userCount))
   //  {
   //      $resultArray['tealRoleMapUpdated'] = number_format(($temRoleMapUpdateCount/$userCount)*100,2);
   //  }

   //  //Teamrole MM and MA count

   //  $teamRoleArr = array('orgId'=>$orgId,'date'=>'created_at'); 

   //  $resultArray['teamRoleCompleted'] = $this->getTeamroleMAMMCount($teamRoleArr);

   //  $teamRoleArr = array('orgId'=>$orgId,'date'=>'updated_at'); 

   //  $resultArray['teamRoleUpdated'] = $this->getTeamroleMAMMCount($teamRoleArr);

   //  //print_r($resultArray);die();




   //          //for personality type
   //  $personalityPerArr = array('orgId' => $orgId,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
   //  $personalityTypeStatus = $this->getPersonalityType($personalityPerArr);

   //  $personalityTypeCount = count($personalityTypeStatus);

   //  $resultArray['personalityTypeCompleted'] = "0";
   //  if (!empty($personalityTypeCount) && !empty($userCount))
   //  {
   //      $resultArray['personalityTypeCompleted'] = number_format(($personalityTypeCount/$userCount)*100,2);
   //  }

   //          //for personality type updated
   //  $personalityUpdatePerArr = array('orgId' => $orgId,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
   //  $personalityTypeUpdateStatus = $this->getPersonalityType($personalityUpdatePerArr);

   //  $personalityTypeUpdateCount = count($personalityTypeUpdateStatus);

   //  $resultArray['personalityTypeUpdated'] = "0";
   //  if (!empty($personalityTypeUpdateCount) && !empty($userCount))
   //  {
   //      $resultArray['personalityTypeUpdated'] = number_format(($personalityTypeUpdateCount/$userCount)*100,2);
   //  }

   //  //Personality type MM and MA count

   //  $personalityTypeArr = array('orgId'=>$orgId,'date'=>'created_at'); 

   //  $resultArray['personalityTypeCompletedMAMM'] = $this->getPersonalityTypeMAMMCount($personalityTypeArr);

   //  $personalityTypeArr = array('orgId'=>$orgId,'date'=>'updated_at'); 

   //  $resultArray['personalityTypeUpdatedMAMM'] = $this->getPersonalityTypeMAMMCount($personalityTypeArr);



   //          //for culture structure comleted        
   //  $cultureStructurePerArr = array('orgId' => $orgId,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
   //  $cultureStructureStatus = $this->getCultureStructure($cultureStructurePerArr);


   //  $cultureStructureCount = count($cultureStructureStatus);

   //  $resultArray['cultureStructureCompleted'] = "0";
   //  if (!empty($cultureStructureCount) && !empty($userCount))
   //  {
   //      $resultArray['cultureStructureCompleted'] = number_format(($cultureStructureCount/$userCount)*100,2);
   //  }


   //          //for culture  structure updated
   //  $cultureStructureUpdatePerArr = array('orgId' => $orgId,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
   //  $cultureStructureUpdateStatus = $this->getCultureStructure($cultureStructureUpdatePerArr);

   //  $cultureStructureUpdateCount = count($cultureStructureUpdateStatus);

   //  $resultArray['cultureStructureUpdated'] = "0";
   //  if (!empty($cultureStructureUpdateCount) && !empty($userCount))
   //  {
   //      $resultArray['cultureStructureUpdated'] = number_format(($cultureStructureUpdateCount/$userCount)*100,2);
   //  }

   //  //culture  structure MM and MA count

   //  $cultureStructureArr = array('orgId'=>$orgId,'date'=>'created_at'); 

   //  $resultArray['cultureStructureCompletedMAMM'] = $this->getCultureStructureMAMMCount($cultureStructureArr);

   //  $cultureStructureArr = array('orgId'=>$orgId,'date'=>'updated_at'); 

   //  $resultArray['cultureStructureUpdatedMAMM'] = $this->getCultureStructureMAMMCount($cultureStructureArr);



   //          //for motivation comleted       
   //  $motivationPerArr = array('orgId' => $orgId,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
   //  $motivationStatus = $this->getMotivation($motivationPerArr);

   //  $motivationCount = count($motivationStatus);

   //  $resultArray['motivationCompleted'] = "0";
   //  if (!empty($motivationCount) && !empty($userCount))
   //  {
   //      $resultArray['motivationCompleted'] = number_format(($motivationCount/$userCount)*100,2);
   //  }

   //          //for motivation type updated
   //  $motivationCountPerArr = array('orgId' => $orgId,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
   //  $motivationCountUpdateStatus = $this->getMotivation($motivationCountPerArr);

   //  $motivationCountUpdateCount = count($motivationCountUpdateStatus);

   //  $resultArray['motivationCountUpdated'] = "0";
   //  if (!empty($motivationCountUpdateCount) && !empty($userCount))
   //  {
   //      $resultArray['motivationCountUpdated'] = number_format(($motivationCountUpdateCount/$userCount)*100,2);
   //  }

   //  //Motivation MM and MA count

   //  $motivationArr = array('orgId'=>$orgId,'date'=>'created_at'); 

   //  $resultArray['motivationCompletedMAMM'] = $this->getMotivationMAMMCount($motivationArr);

   //  $motivationArr = array('orgId'=>$orgId,'date'=>'updated_at'); 

   //  $resultArray['motivationUpdatedMAMM'] = $this->getMotivationMAMMCount($motivationArr);

   //  return view('admin/performance/performance',compact('resultArray','organisations','orgId','reportType','date'));
   //  }


  public function calculateEngageIndex($id,$officeId,$departmentId,$date,$excludeWeekend){

    //$date='2018-08-20';
    //$date='2019-11-05';

    $orgId = base64_decode($id);
    $departmentId = $departmentId;
    $officeId = $officeId;
            
//------------------------------------Calculation Start---------------------------------------

   //check DOT g is given by user on every dot value if it is yes then true
    $usersQuery = DB::table('users')
    ->select('users.id as id')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('users.roleId',3);
    if(!empty($orgId)){
        $usersQuery->where('users.orgId',$orgId);
    }
    if($date){
        $usersQuery->whereDate('users.created_at','<=',$date);
    }
    // if($excludeWeekend!=1){
    //     //Get weekends
    //     // $getWeekendDates = $this->getWeekendDaysFromMonth($date,1);
    //     $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($date,1);
    //     $usersQuery->whereNotIn(DB::raw("(DATE_FORMAT(users.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    // }
    if (!empty($officeId) && empty($departmentId)) {
        $usersQuery->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $usersQuery->where('users.officeId',$officeId);
        $usersQuery->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $usersQuery->where('departments.departmentId',$departmentId);
    }
    $users      = $usersQuery->get();
    $userCount  = count($users);

    $dotValueRatingCompletedUserArr = array();
    foreach($users as $duValue){
        $dotPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);

        //$dotPerArr = array('orgId' => $orgId, 'userId' => $duValue->id, 'updateAt' => '', 'currentDate' => '', 'month' => '', 'year' => '');
        // $status = $this->indexReportForDotCompletedValues($dotPerArr);
        $status = app('App\Http\Controllers\Admin\AdminReportController')->indexReportForDotCompletedValues($dotPerArr);
        if($status){
            array_push($dotValueRatingCompletedUserArr, $duValue->id);      
        }
    }
    $dotValueRatingCompletedUserArrCount = count($dotValueRatingCompletedUserArr);

    $dotCompleted = "0";
    if(!empty($dotValueRatingCompletedUserArrCount) && !empty($userCount)){
        //echo $dotValueRatingCompletedUserArrCount." = ".$userCount;die;
        $dotCompleted = ($dotValueRatingCompletedUserArrCount/$userCount)*100;
    }

    //for team role map complete
    $teamPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);
    //$teamPerArr = array('orgId' => $orgId,'updateAt'=>'','currentDate'=>'','month'=>'','year'=>'');
    // $teamRoleStatus = $this->indexReportForTeamroleCompleted($teamPerArr);
    $teamRoleStatus = app('App\Http\Controllers\Admin\AdminReportController')->indexReportForTeamroleCompleted($teamPerArr);
    $temRoleMapCount        = count($teamRoleStatus);
    
    $tealRoleMapCompleted   = "0";
    if (!empty($temRoleMapCount) && !empty($userCount)){
        $tealRoleMapCompleted = ($temRoleMapCount/$userCount)*100;
    }

    //for personality type completed
    $personalityPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);
    //$personalityPerArr = array('orgId' => $orgId,'updateAt'=>'','currentDate'=>'','month'=>'','year'=>'');
    // $personalityTypeStatus = $this->indexReportForpersonalityTypeCompleted($personalityPerArr);
    $personalityTypeStatus = app('App\Http\Controllers\Admin\AdminReportController')->indexReportForpersonalityTypeCompleted($personalityPerArr);
    $personalityTypeCount       = count($personalityTypeStatus);

    $personalityTypeCompleted   = "0";
    if (!empty($personalityTypeCount) && !empty($userCount)){
        $personalityTypeCompleted = ($personalityTypeCount/$userCount)*100;
    }

    //for culture structure comleted       
    $cultureStructurePerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date,'excludeWeekend' => $excludeWeekend);
    $cultureStructureStatus = app('App\Http\Controllers\Admin\AdminReportController')->indexReportForCultureStructureCompleted($cultureStructurePerArr);
    // $cultureStructureStatus = $this->indexReportForCultureStructureCompleted($cultureStructurePerArr);
    $cultureStructureCount      = count($cultureStructureStatus);

    $cultureStructureCompleted  = "0";
    if (!empty($cultureStructureCount) && !empty($userCount)){
        $cultureStructureCompleted = ($cultureStructureCount/$userCount)*100;
    }

    //for motivation comleted       
    $motivationPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);
    //$motivationPerArr = array('orgId' => $orgId,'updateAt'=>'','currentDate'=>'','month'=>'','year'=>'');
    // $motivationStatus = $this->indexReportForMotivationCompleted($motivationPerArr);
    $motivationStatus = app('App\Http\Controllers\Admin\AdminReportController')->indexReportForMotivationCompleted($motivationPerArr);
    $motivationCount = count($motivationStatus);

    $motivationCompleted = "0";
    if (!empty($motivationCount) && !empty($userCount)){
        $motivationCompleted = ($motivationCount/$userCount)*100;
    }

    //Kudos/Thumbsup Completed for yesterday
    $thumbsupPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);
    $thumbsupCount = app('App\Http\Controllers\Admin\AdminReportController')->indexReportForThumbsupCompleted($thumbsupPerArr);
    // $thumbsupCount = $this->indexReportForThumbsupCompleted($thumbsupPerArr);

    $thumbCompleted = "0";
    if(!empty($thumbsupCount) && !empty($userCount)){
        $thumbCompleted = ($thumbsupCount/$userCount);
    }

    /*get previous day Good/happy index detail*/
    $happyIndexPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);
    $happyIndexCompleted = app('App\Http\Controllers\Admin\AdminReportController')->indexReportForHappyIndexCompleted($happyIndexPerArr);
    // $happyIndexCompleted = $this->indexReportForHappyIndexCompleted($happyIndexPerArr);

    //Tribeometer and diagnostics completed last month by users
    $diagnosticAnsCompletedUserArr = array();
    $tribeometerAnsCompletedUserArr = array();
    foreach($users as $duValue){
      if($date){
        //Get Previos month of date
        $timestamp = strtotime ("-1 month",strtotime ($date));
        //Convert into 'Y-m' format
        $previosMonthFromDate  =  date("Y-m",$timestamp);

        $isDiagnosticAnsDoneQuery  = DB::table('diagnostic_answers')
            ->leftjoin('users','users.id','diagnostic_answers.userId')
            ->leftJoin('departments','departments.id','users.departmentId')
            // ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('diagnostic_answers.userId',$duValue->id)
            ->where('diagnostic_answers.status','Active')
            ->where('users.status','Active')
            ->where('departments.status','Active')
            // ->where('all_department.status','Active')
            ->where('diagnostic_answers.updated_at','LIKE',$previosMonthFromDate."%");
        if($excludeWeekend!=1){
            //Get weekends
            $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($previosMonthFromDate,2);
            // $getWeekendDates = $this->getWeekendDaysFromMonth($previosMonthFromDate,2);
            //echo '<pre />';print_r($getWeekendDates);die;
            $isDiagnosticAnsDoneQuery->whereNotIn(DB::raw("(DATE_FORMAT(diagnostic_answers.updated_at,'%Y-%m-%d'))"),$getWeekendDates);
        }
      }else{
        $lastMonth = date("Y-m", strtotime("-1 month"));
        $isDiagnosticAnsDoneQuery  = DB::table('diagnostic_answers')
            ->leftjoin('users','users.id','diagnostic_answers.userId')
            ->leftJoin('departments','departments.id','users.departmentId')
            // ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('diagnostic_answers.userId',$duValue->id)
            ->where('diagnostic_answers.status','Active')
            ->where('users.status','Active')
            ->where('departments.status','Active')
            // ->where('all_department.status','Active')
            ->where('diagnostic_answers.updated_at','LIKE',$lastMonth."%");
        if($excludeWeekend!=1){
          //Get weekends
          $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($lastMonth,2);
          // $getWeekendDates = $this->getWeekendDaysFromMonth($lastMonth,2);
          //echo '<pre />';print_r($getWeekendDates);die;
          $isDiagnosticAnsDoneQuery->whereNotIn(DB::raw("(DATE_FORMAT(diagnostic_answers.updated_at,'%Y-%m-%d'))"),$getWeekendDates);
        }
      } 
      if (!empty($orgId)) {
          $isDiagnosticAnsDoneQuery->where('diagnostic_answers.orgId',$orgId);
      }
      if (!empty($officeId) && empty($departmentId)) {
          $isDiagnosticAnsDoneQuery->where('users.officeId',$officeId);
      }
      elseif (!empty($officeId) && !empty($departmentId)) {
          $isDiagnosticAnsDoneQuery->where('users.officeId',$officeId);
          $isDiagnosticAnsDoneQuery->where('users.departmentId',$departmentId);
      }
      elseif (empty($officeId) && !empty($departmentId)) {
          $isDiagnosticAnsDoneQuery->where('departments.departmentId',$departmentId);
      }  
      $isDiagnosticAnsDone = $isDiagnosticAnsDoneQuery->first();


      if($date){
        //Get Previos month of date
        $timestamp = strtotime ("-1 month",strtotime ($date));
        //Convert into 'Y-m' format
        $previosMonthFromDate  =  date("Y-m",$timestamp);

        $isTribeometerAnsDoneQuery = DB::table('tribeometer_answers')
            ->leftjoin('users','users.id','tribeometer_answers.userId')
            ->leftJoin('departments','departments.id','users.departmentId')
            // ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('tribeometer_answers.userId',$duValue->id)
            ->where('tribeometer_answers.status','Active')
            ->where('users.status','Active')
            ->where('tribeometer_answers.updated_at','LIKE',$previosMonthFromDate."%");
        if($excludeWeekend!=1){
          //Get weekends
          $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($previosMonthFromDate,2);
          // $getWeekendDates = $this->getWeekendDaysFromMonth($previosMonthFromDate,2);
          //echo '<pre />';print_r($getWeekendDates);die;
          $isTribeometerAnsDoneQuery->whereNotIn(DB::raw("(DATE_FORMAT(tribeometer_answers.updated_at,'%Y-%m-%d'))"),$getWeekendDates);
        }
      }else{
          $lastMonth = date("Y-m", strtotime("-1 month"));

          $isTribeometerAnsDoneQuery = DB::table('tribeometer_answers')
              ->leftjoin('users','users.id','tribeometer_answers.userId')
              ->leftJoin('departments','departments.id','users.departmentId')
              // ->leftJoin('all_department','all_department.id','departments.departmentId')
              ->where('tribeometer_answers.userId',$duValue->id)
              ->where('tribeometer_answers.status','Active')
              ->where('users.status','Active')
              ->where('tribeometer_answers.updated_at','LIKE',$lastMonth."%");
          if($excludeWeekend!=1){
            //Get weekends
            $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($lastMonth,2);
            // $getWeekendDates = $this->getWeekendDaysFromMonth($lastMonth,2);
            //echo '<pre />';print_r($getWeekendDates);die;
            $isTribeometerAnsDoneQuery->whereNotIn(DB::raw("(DATE_FORMAT(tribeometer_answers.updated_at,'%Y-%m-%d'))"),$getWeekendDates);
          }
      }
      if (!empty($orgId)) {
          $isTribeometerAnsDoneQuery->where('tribeometer_answers.orgId',$orgId);
      }
      if (!empty($officeId) && empty($departmentId)) {
          $isTribeometerAnsDoneQuery->where('users.officeId',$officeId);
      }elseif (!empty($officeId) && !empty($departmentId)) {
          $isTribeometerAnsDoneQuery->where('users.officeId',$officeId);
          $isTribeometerAnsDoneQuery->where('users.departmentId',$departmentId);
      }elseif (empty($officeId) && !empty($departmentId)) {
          $isTribeometerAnsDoneQuery->where('departments.departmentId',$departmentId);
      } 
      $isTribeometerAnsDone = $isTribeometerAnsDoneQuery->first();

      if(!empty($isDiagnosticAnsDone)){
          array_push($diagnosticAnsCompletedUserArr, $duValue->id);      
      }
      if($isTribeometerAnsDone){
          array_push($tribeometerAnsCompletedUserArr, $duValue->id);      
      }
    }

    $diagnosticAnsCompletedUserArrCount  = count($diagnosticAnsCompletedUserArr);
    $tribeometerAnsCompletedUserArrCount = count($tribeometerAnsCompletedUserArr);

    $diagnosticAnsCompleted = "0";
    if(!empty($diagnosticAnsCompletedUserArrCount) && !empty($userCount)){
        $diagnosticAnsCompleted = ($diagnosticAnsCompletedUserArrCount/$userCount)*100;
    }
    $tribeometerAnsCompleted = "0";
    if(!empty($tribeometerAnsCompletedUserArrCount) && !empty($userCount)){
        $tribeometerAnsCompleted = ($tribeometerAnsCompletedUserArrCount/$userCount)*100;
    }

    $releventCompanyScores = $dotCompleted + $tealRoleMapCompleted + $personalityTypeCompleted + $cultureStructureCompleted + $motivationCompleted + $thumbCompleted + $happyIndexCompleted + $diagnosticAnsCompleted + $tribeometerAnsCompleted;

    $engageIndex = round($releventCompanyScores,2);

    // if (!empty($releventCompanyScores)) {
    //     $indexOrg = round((($releventCompanyScores/2300)*1000),2);
    // }else{
    //     $indexOrg = 0;
    // }



     // print_r("Final result => ".$engageIndex."<br>");
     // print_r("Dot=>".$dotCompleted."<br>");
     // print_r("team role=>".$tealRoleMapCompleted."<br>");
     // print_r("PT=>".$personalityTypeCompleted."<br>");
     // print_r("CS=>".$cultureStructureCompleted."<br>");
     // print_r("motivation=>".$motivationCompleted."<br>");
     // print_r("Thumpsup=>".$thumbCompleted."<br>");
     // print_r("happy Index=>".$happyIndexCompleted."<br>");
     // print_r("Diagnostic Ans=>".$diagnosticAnsCompleted."<br>");
     // print_r("Tribeometer Ans=>".$tribeometerAnsCompleted."<br>");
     // die;
   

   return $engageIndex;
}

    public function getPerformance()
    {

        $resultArray = array();

        $orgId = Input::get('orgId');
        $reportType = Input::get('reportType');
        //$date = Input::get('date');

        // print_r(Carbon::today());
        // print_r(Carbon::now()->endOfWeek()->subWeek());
         // print_r(Carbon::now()->subMonth(12));
         // print_r(date('Y-m',strtotime('-12 months')));
         // print_r(new Carbon('last day of last month'));
         //die();

        // $month ='';
        // $year  ='';
        // $currentDate = '';
        // if($reportType == 'daily') 
        // {
        //     $currentDate = date('Y-m-d');          
        // }       
        // else if($reportType == 'monthly')
        // {
        //     $month = date('m', strtotime($date));
        //     $year  = date('Y', strtotime($date));
        //print_r(Carbon::today());die();
        // }
        $last12MonthFirstDate = '';
        $last6MonthFirstDate = '';
        $lastMonth = '';
        $thisMonth = '';
        $startOfSubWeek = '';
        $endOfSubWeek = '';
        $startOfWeek = '';
        $endOfWeek = '';
        $yesterday = '';
        $today = '';


        $thisMonthLastDate = date('Y-m-d',strtotime('last day of last month'));

        if ($reportType == 'lastMonths12') {
            //$last12Months = Carbon::now()->subMonth(12);
            $last12MonthFirstDate = date('Y-m-1',strtotime('-12 months'));
        }
        elseif ($reportType == 'lastMonths6') {
            //$last6Months = Carbon::now()->subMonth(6);
            $last6MonthFirstDate = date('Y-m-1',strtotime('-6 months'));
        }
        elseif ($reportType == 'lastMonth') {
            $lastMonth = date("Y-m", strtotime("-1 month"))."%";
        }
        elseif ($reportType == 'thisMonth') {
            $thisMonth = date("Y-m")."%";
        }
        elseif ($reportType == 'lastWeek') {
            $startOfSubWeek = Carbon::now()->startOfWeek()->subWeek();
            $endOfSubWeek = Carbon::now()->endOfWeek()->subWeek();
        }
        elseif ($reportType == 'thisWeek') {
            $startOfWeek = Carbon::now()->startOfWeek();
            $endOfWeek = Carbon::now()->endOfWeek();
        }
        elseif ($reportType == 'yesterday') {
            $yesterday = Carbon::yesterday();
        }
        elseif ($reportType == 'today') {
            $today = Carbon::today();
        }

        $organisations = DB::table('organisations')->where('status','Active')->orderBy('organisation','ASC')->get();

        //By default 1st organisation selected
        $organisation = DB::table('organisations')->where('status','Active')->orderBy('organisation','ASC')->first();
        if (empty($orgId)) {
            $orgId = $organisation->id;   
        }

        $usersQuery = DB::table('users')->where('status','Active')->where('roleId',3);

        if(!empty($orgId))
        {
            $usersQuery->where('orgId',$orgId);
        }
        if (!empty($last12MonthFirstDate) && !empty($thisMonthLastDate)) {
            $usersQuery->whereDate('created_at','<=',$thisMonthLastDate);
            //$usersQuery->whereBetween('created_at',array($last12MonthFirstDate,$thisMonthLastDate));
        }
        elseif (!empty($last6MonthFirstDate) && !empty($thisMonthLastDate)) {
            $usersQuery->whereDate('created_at','<=',$thisMonthLastDate);
            // $usersQuery->whereBetween('created_at',array($last6MonthFirstDate,$thisMonthLastDate));
        }
        elseif (!empty($lastMonth)) {
            $usersQuery->whereDate('created_at','<=',$thisMonthLastDate);            
            //$usersQuery->where('created_at','LIKE',$lastMonth);
        }
        elseif (!empty($thisMonth)) {
            $usersQuery->whereDate('created_at','<=',date('Y-m-d'));
            //$usersQuery->whereDate('created_at','<=',date('Y-m-d',strtotime('last day of this month')));
            //$usersQuery->where('created_at','LIKE',$thisMonth);
        }
        elseif (!empty($startOfSubWeek) && !empty($endOfSubWeek)) {
            $usersQuery->whereDate('created_at','<=',$endOfSubWeek);
            // $usersQuery->whereBetween('created_at',array($startOfSubWeek,$endOfSubWeek));
        }
        elseif (!empty($startOfWeek) && !empty($endOfWeek)) {
            $usersQuery->whereDate('created_at','<=',date('Y-m-d'));
            //$usersQuery->whereDate('created_at','<=',$endOfWeek);
            // $usersQuery->whereBetween('created_at',array($startOfWeek,$endOfWeek));
        }
        elseif (!empty($yesterday)) {
            $usersQuery->whereDate('created_at','<=',$yesterday);
            // $usersQuery->whereDate('created_at',$yesterday);
        }
        elseif (!empty($today)) {
            $usersQuery->whereDate('created_at','<=',$today);
            // $usersQuery->whereDate('created_at',$today);
        }
        $users = $usersQuery->get();

        $userCount = count($users);

        $query = DB::table('dot_bubble_rating_records')
        ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
        ->where('dots.status','Active')
        ->where('dot_bubble_rating_records.status','Active');                  

        if(!empty($orgId))
        {
            $query->where('dots.orgId',$orgId);
        }
        if (!empty($last12MonthFirstDate) && !empty($thisMonthLastDate)) {
            $query->whereBetween('dot_bubble_rating_records.created_at',array($last12MonthFirstDate,$thisMonthLastDate));
        }
        elseif (!empty($last6MonthFirstDate) && !empty($thisMonthLastDate)) {
            $query->whereBetween('dot_bubble_rating_records.created_at',array($last6MonthFirstDate,$thisMonthLastDate));
        }
        elseif (!empty($lastMonth)) {
            $query->where('dot_bubble_rating_records.created_at','LIKE',$lastMonth);
        }
        elseif (!empty($thisMonth)) {
            $query->where('dot_bubble_rating_records.created_at','LIKE',$thisMonth);
        }
        elseif (!empty($startOfSubWeek) && !empty($endOfSubWeek)) {
            $query->whereBetween('dot_bubble_rating_records.created_at',array($startOfSubWeek,$endOfSubWeek));
        }
        elseif (!empty($startOfWeek) && !empty($endOfWeek)) {
            $query->whereBetween('dot_bubble_rating_records.created_at',array($startOfWeek,$endOfWeek));
        }
        elseif (!empty($yesterday)) {
            $query->whereDate('dot_bubble_rating_records.created_at',$yesterday);
        }
        elseif (!empty($today)) {
            $query->whereDate('dot_bubble_rating_records.created_at',$today);
        }

        // if(!empty($currentDate))
        // {
        //    $query->whereDate('dot_bubble_rating_records.created_at',$currentDate);
        // }

        // if(!empty($month) && !empty($year))
        // { 
        //     $query->whereMonth('dot_bubble_rating_records.created_at', $month);
        //     $query->whereYear('dot_bubble_rating_records.created_at', $year);
        // }

        $bubbleCount = $query->count('bubble_flag');
        //print_r($userCount);die();

        $resultArray['thumbCompleted'] = "0";
        if(!empty($bubbleCount) && !empty($userCount))
        {
            $resultArray['thumbCompleted'] = number_format(($bubbleCount/$userCount), 2);
        }

        //MA and MM for bubble rating
        $bubbleRatingData = array('orgId' => $orgId);

        $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
        $resultArray['bubbleRating'] = 0;
        if (!empty($staff)) {
          $resultArray['bubbleRating'] = $this->getBubbleRatingCount($bubbleRatingData);
        }
        else
        {
          $resultArray['bubbleRating']['MM'] == 0;
          $resultArray['bubbleRating']['MA'] == 0;
        }

        //Improvements 

        $improvementQuery = DB::table('iot_feedbacks')
        ->where('status','!=','Inactive');
        if(!empty($orgId))
        {
            $improvementQuery->where('orgId',$orgId);
        }
        if (!empty($last12MonthFirstDate) && !empty($thisMonthLastDate)) {
            $improvementQuery->whereBetween('iot_feedbacks.created_at',array($last12MonthFirstDate,$thisMonthLastDate));
        }
        elseif (!empty($last6MonthFirstDate) && !empty($thisMonthLastDate)) {
            $improvementQuery->whereBetween('iot_feedbacks.created_at',array($last6MonthFirstDate,$thisMonthLastDate));
        }
        elseif (!empty($lastMonth)) {
            $improvementQuery->where('iot_feedbacks.created_at','LIKE',$lastMonth);
        }
        elseif (!empty($thisMonth)) {
            $improvementQuery->where('iot_feedbacks.created_at','LIKE',$thisMonth);
        }
        elseif (!empty($startOfSubWeek) && !empty($endOfSubWeek)) {
            $improvementQuery->whereBetween('iot_feedbacks.created_at',array($startOfSubWeek,$endOfSubWeek));
        }
        elseif (!empty($startOfWeek) && !empty($endOfWeek)) {
            $improvementQuery->whereBetween('iot_feedbacks.created_at',array($startOfWeek,$endOfWeek));
        }
        elseif (!empty($yesterday)) {
            $improvementQuery->whereDate('iot_feedbacks.created_at',$yesterday);
        }
        elseif (!empty($today)) {
            $improvementQuery->whereDate('iot_feedbacks.created_at',$today);
        }
        // if(!empty($currentDate))
        // {
        //    $improvementQuery->whereDate('iot_feedbacks.created_at',$currentDate);
        // }
        // if(!empty($month) && !empty($year))
        // { 
        //     $improvementQuery->whereMonth('iot_feedbacks.created_at', $month);
        //     $improvementQuery->whereYear('iot_feedbacks.created_at', $year);
        // }

        $improvements = $improvementQuery->count();
        //print_r($improvements);die();

        $resultArray['improvements'] = "0";
        if(!empty($improvements) && !empty($userCount))
        {
            $resultArray['improvements'] = number_format(($improvements/$userCount), 2);
        }

        //MA and MM for improvements
        $improvementArr = array('orgId' => $orgId);

        //$resultArray['improvementsMAMM'] = $this->getImprovementsMMMA($improvementArr);

        $resultArray['improvementsMAMM'] = 0;
        if (!empty($staff)) {
          $resultArray['improvementsMAMM'] = $this->getImprovementsMMMA($improvementArr);
        }
        else
        {
          $resultArray['improvementsMAMM']['MM'] == 0;
          $resultArray['improvementsMAMM']['MA'] == 0;
        }


        /*DOT value complete*/
        $dotValueRatingCompletedUserArr = array();
        $dotValueRatingUpdatedUserArr   = array();
        $lastMonthUserCount = array();
        $thisMonthUserCount = array();
        $allPreviousMonthUserCount = array();
        foreach($users as $duValue)
        {
            //check DOT rating is given by user on every dot value if it is yes then true
            //$dotPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
            $dotPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'updateAt'=>false,'last12MonthFirstDate'=>$last12MonthFirstDate,'thisMonthLastDate'=>$thisMonthLastDate,'last6MonthFirstDate'=>$last6MonthFirstDate,'lastMonth'=>$lastMonth,'thisMonth'=>$thisMonth,'startOfSubWeek'=>$startOfSubWeek,'endOfSubWeek'=>$endOfSubWeek,'startOfWeek'=>$startOfWeek,'endOfWeek'=>$endOfWeek,'yesterday'=>$yesterday,'today'=>$today);

            $status = $this->getDotPerformance($dotPerArr);

            if($status)
            {
                array_push($dotValueRatingCompletedUserArr, $duValue->id);      
            }
            //for updated
            //$dotPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
            $dotPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'updateAt'=>false,'last12MonthFirstDate'=>'','thisMonthLastDate'=>'','last6MonthFirstDate'=>'','lastMonth'=>'','thisMonth'=>'','startOfSubWeek'=>'','endOfSubWeek'=>'','startOfWeek'=>'','endOfWeek'=>'','yesterday'=>'','today'=>'');

            $uStatus = $this->getDotPerformance($dotPerArr);

            if($uStatus)
            {
                array_push($dotValueRatingUpdatedUserArr, $duValue->id);        
            }

            //Dot MM and MA count complete
            $dotRatingLastMonthArr = array('orgId' => $orgId,'userId'=>$duValue->id,'date'=>'lastMonth');
            $dotRatingThisMonthArr = array('orgId' => $orgId,'userId'=>$duValue->id,'date'=>'thisMonth');
            $dotRatingAllPreviousMonthArr = array('orgId' => $orgId,'userId'=>$duValue->id,'date'=>"allPreviousMonth");


            $lastMonthData = $this->getDotRatingCount($dotRatingLastMonthArr);
            $thisMonthData = $this->getDotRatingCount($dotRatingThisMonthArr);
            $allPreviousMonthData = $this->getDotRatingCount($dotRatingAllPreviousMonthArr);
            // print_r("last".$lastMonthData);
            // print_r("this".$thisMonthData);
            // print_r("allPrevious".$allPreviousMonthData);
            // die();
            // echo "<pre> this month data=>";print_r($thisMonthData);
            // echo "<pre> User id=>";print_r($duValue->id);
            if ($lastMonthData) 
            {
                array_push($lastMonthUserCount, $duValue->id);
            }
            if ($thisMonthData) 
            {
                array_push($thisMonthUserCount, $duValue->id);
            }
            if ($allPreviousMonthData) 
            {
                array_push($allPreviousMonthUserCount, $duValue->id);
            }
            //echo "<pre>UserId => ";print_r($i);
            //$resultArray['dotRatingCompleted'] = $this->getDotRatingCount($dotRatingData);
        }
        //for complete
        $dotValueRatingCompletedUserArrCount = count($dotValueRatingCompletedUserArr);

        $resultArray['dotCompleted'] = "0";
        if(!empty($dotValueRatingCompletedUserArrCount) && !empty($userCount))
        {
            $resultArray['dotCompleted'] = number_format(($dotValueRatingCompletedUserArrCount/$userCount)*100,2);
        }

          //For MA and MM
        //print_r($allPreviousMonthUserCount);die();
        $lastMonthUserArrCount = count($lastMonthUserCount);
        $thisMonthUserArrCount = count($thisMonthUserCount);
        $allPreviousMonthUserArrCount = count($allPreviousMonthUserCount);

        // print_r($lastMonthUserArrCount." ");
        // print_r($thisMonthUserArrCount." ");
        // print_r($allPreviousMonthUserArrCount);
        // die();

        $lastMonthStaffCount = $this->lastMonthStaffCount($orgId);

        //Last Month value
        $lastMonthUserArr = 0;
        if (!empty($lastMonthUserArrCount) && !empty($lastMonthStaffCount)) 
        {
          $lastMonthUserArr = (($lastMonthUserArrCount)/($lastMonthStaffCount))*100;
        }

        $thisMonthStaffCount = $this->thisMonthStaffCount($orgId);

        //This Month value
        $thisMonthUserArr = 0;
        if (!empty($thisMonthUserArrCount) && !empty($thisMonthStaffCount)) 
        {
          $thisMonthUserArr = (($thisMonthUserArrCount)/($thisMonthStaffCount))*100;
        }

        //all previous Month value
        $allPreviousMonthCount = 0;
        if (!empty($allPreviousMonthUserArrCount) && !empty($lastMonthStaffCount)) 
        {
          $allPreviousMonthCount = (($allPreviousMonthUserArrCount)/($lastMonthStaffCount))*100;
        }
        // print_r($lastMonthUserArrCount." ");
        // print_r($thisMonthUserArrCount." ");
        // print_r($allPreviousMonthUserArrCount." ");
        // die();


       // function get_months($date1, $date2) {
       //    $time1 = strtotime($date1);
       //    $time2 = strtotime($date2);

       //    if(date('m', $time1)==date('m', $time2) && date('Y', $time1)==date('Y', $time2)){
       //      return 1;
       //    }else{
       //    $my = date('mY', $time2);
       //    $months = array(date('F', $time1));
       //    while($time1 < $time2) {
       //      $time1 = strtotime(date('Y-m-d', $time1).' +1 month');
       //      if(date('mY', $time1) != $my && ($time1 < $time2))
       //      $months[] = date('F', $time1);
       //    }

       //    $months[] = date('F', $time2);
       //    return $months;
       //    }
       //  }

       //  //$date1 alway small -> Org created
       //  echo count(get_months('2019-11-06', '2019-10-30'));

        //Count all months till last month
        //$dotValueRatingFirstData = DB::table('organisations')->select('created_at')->where('id',$orgId)->orderBy('created_at','ASC')->first();
        //print_r($orgId);die();
        // $dotValueRatingFirstData = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
        // //print_r($dotValueRatingFirstData);die;

        // $firstDateOfOrg = date('Y-m-d',strtotime($dotValueRatingFirstData->created_at));
        // $lastMonthDate = date('Y-m-d',strtotime('-1 month'));

        $noOfPreviousMonths = 0;
        if (!empty($staff)) {
          $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));
        }
        else
        {
          $noOfPreviousMonths = 0;
        }

        //$noOfPreviousMonths = count($this->countNumberOfMonths($orgId));

       // print_r($lastMonthDate);die();
        //print_r($lastMonthDate);die();
        //print_r(Carbon::now()->subMonth());die();
      //   $date1 = new DateTime($dotValueRatingFirstData->created_at);
      //   $date2 = $date1->diff(new DateTime(Carbon::now()->subMonth()));
      // //  print_r($date2);die();
      //   $dotRatingAllMonthcount = ($date2->y*12) + $date2->m + 2;
      //   //print_r($dotRatingAllMonthcount);die();


        //$date1 alway small -> Org created
        // $time1 = strtotime($firstDateOfOrg);
        // $time2 = strtotime($lastMonthDate);

        //print_r(date('mY', $time2));die();
        // if ($time1 < $time2) 
        // {
        //   if(date('m', $time1)==date('m', $time2) && date('Y', $time1)==date('Y', $time2)){
        //     $noOfPreviousMonths = 1;
        //     //return 1;
        //   }else{
        //     $my = date('mY', $time2);
        //     $noOfPreviousMonths = array(date('F', $time1));
        //     while($time1 < $time2) {
        //       $time1 = strtotime(date('Y-m-d', $time1).' +1 month');
        //       if(date('mY', $time1) != $my && ($time1 < $time2))
        //       $noOfPreviousMonths[] = date('F', $time1);
        //     }

        //     $noOfPreviousMonths[] = date('F', $time2);
        //   }
        // }
        // else
        // {
        //   $noOfPreviousMonths = 0;
        // }


          // if(date('m', $time1)==date('m', $time2) && date('Y', $time1)==date('Y', $time2)){
          //   $noOfPreviousMonths = 1;
          //   //return 1;
          // }
          // else{
          //   if ($time1 < $time2) 
          //   {
          //     $my = date('mY', $time2);
          //     $noOfPreviousMonths = array(date('F', $time1));
          //     while($time1 < $time2) {
          //       $time1 = strtotime(date('Y-m-d', $time1).' +1 month');
          //       if(date('mY', $time1) != $my && ($time1 < $time2))
          //       $noOfPreviousMonths[] = date('F', $time1);
          //     }
          //     $noOfPreviousMonths[] = date('F', $time2);
          //   }
          //   else
          //   {
          //     $noOfPreviousMonths = 0;
          //   } 
          // }
        

        //print_r($noOfPreviousMonths);die();

        //Now calculate MM and MA

        $dotRatingCount = array();
        $dotRatingCount['MM'] = 0;
        $dotRatingCount['MA'] = 0;
        $dotCountMA = 0;
        
        if (!empty($lastMonthUserArr)) {

            $dotRatingCount['MM'] = number_format(((-1*($lastMonthUserArr - $thisMonthUserArr))/$lastMonthUserArr),2);
        }
        
        // if (!empty($allPreviousMonthUserArrCount) && !empty($dotRatingAllMonthcount)) {

        //     $dotCountMA = ($allPreviousMonthUserArrCount)/($dotRatingAllMonthcount);
        //    // print_r($dotCountMA);die();
        //     if (!empty($dotCountMA)) {
                
        //         $dotRatingCount['MA'] = number_format(((-1*($dotCountMA - $thisMonthUserArrCount))/$dotCountMA)*100,2);
        //     }
        // }
        if (!empty($allPreviousMonthCount) && !empty($noOfPreviousMonths)) {

            $dotCountMA = ($allPreviousMonthCount)/($noOfPreviousMonths);
           // print_r($dotCountMA);die();
            if (!empty($dotCountMA)) {
                
                $dotRatingCount['MA'] = number_format(((-1*($dotCountMA - $thisMonthUserArr))/$dotCountMA),2);
            }
        }
        //print_r($dotRatingCount);die();
        $resultArray['dotRatingCompleted'] = $dotRatingCount;

        //for update
        $dotValueRatingUpdatedUserArrCount = count($dotValueRatingUpdatedUserArr);

        $resultArray['dotUpdated'] = "0";
        if(!empty($dotValueRatingUpdatedUserArrCount) && !empty($userCount))
        {
            $resultArray['dotUpdated'] = number_format(($dotValueRatingUpdatedUserArrCount/$userCount)*100,2);
        }

        //Dot MM and MA count
        // $dotRatingData = array('orgId' => $orgId,'date' => 'created_at');

        // $resultArray['dotRatingCompleted'] = $this->getDotRatingCount($dotRatingData);

        // $dotRatingData = array('orgId' => $orgId,'date' => 'updated_at');

        // $resultArray['dotRatingUpdated'] = $this->getDotRatingCount($dotRatingData);



        //for team role map complete
        //$teamPerArr = array('orgId' => $orgId,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
        $teamPerArr = array('orgId' => $orgId,'updateAt'=>false,'last12MonthFirstDate'=>$last12MonthFirstDate,'thisMonthLastDate'=>$thisMonthLastDate,'last6MonthFirstDate'=>$last6MonthFirstDate,'lastMonth'=>$lastMonth,'thisMonth'=>$thisMonth,'startOfSubWeek'=>$startOfSubWeek,'endOfSubWeek'=>$endOfSubWeek,'startOfWeek'=>$startOfWeek,'endOfWeek'=>$endOfWeek,'yesterday'=>$yesterday,'today'=>$today);

        $teamRoleStatus = $this->getTeamRoleMap($teamPerArr);

        $temRoleMapCount = count($teamRoleStatus);
        //print_r($temRoleMapCount);die();

        $resultArray['tealRoleMapCompleted'] = "0";
        if (!empty($temRoleMapCount) && !empty($userCount))
        {
            $resultArray['tealRoleMapCompleted'] = number_format(($temRoleMapCount/$userCount)*100,2);
        }

        //for team role map update
        //$teamPerUpdateArr = array('orgId' => $orgId,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
        $teamPerUpdateArr = array('orgId' => $orgId,'updateAt'=>false,'last12MonthFirstDate'=>$last12MonthFirstDate,'thisMonthLastDate'=>$thisMonthLastDate,'last6MonthFirstDate'=>$last6MonthFirstDate,'lastMonth'=>$lastMonth,'thisMonth'=>$thisMonth,'startOfSubWeek'=>$startOfSubWeek,'endOfSubWeek'=>$endOfSubWeek,'startOfWeek'=>$startOfWeek,'endOfWeek'=>$endOfWeek,'yesterday'=>$yesterday,'today'=>$today);

        $teamRoleUpdateStatus = $this->getTeamRoleMap($teamPerUpdateArr);

        $temRoleMapUpdateCount = count($teamRoleUpdateStatus);

        $resultArray['tealRoleMapUpdated'] = "0";
        if (!empty($temRoleMapUpdateCount) && !empty($userCount))
        {
            $resultArray['tealRoleMapUpdated'] = number_format(($temRoleMapUpdateCount/$userCount)*100,2);
        }

        //Teamrole MM and MA count
        // $teamRoleArr = array('orgId'=>$orgId,'date'=>'created_at'); 

        // $resultArray['teamRoleCompleted'] = $this->getTeamroleMAMMCount($teamRoleArr);

        // $teamRoleArr = array('orgId'=>$orgId,'date'=>'updated_at'); 

        // $resultArray['teamRoleUpdated'] = $this->getTeamroleMAMMCount($teamRoleArr);
        
        $teamRoleCompletedArr = array('orgId'=>$orgId,'date'=>'created_at'); 
        $teamRoleUpdatedArr = array('orgId'=>$orgId,'date'=>'updated_at'); 

        $resultArray['teamRoleCompleted'] = 0;
        $resultArray['teamRoleUpdated'] = 0;
        if (!empty($staff)) {
          $resultArray['teamRoleCompleted'] = $this->getTeamroleMAMMCount($teamRoleCompletedArr);

          $resultArray['teamRoleUpdated'] = $this->getTeamroleMAMMCount($teamRoleUpdatedArr);
        }
        else
        {
          $resultArray['teamRoleCompleted']['MM'] == 0;
          $resultArray['teamRoleCompleted']['MA'] == 0;

          $resultArray['teamRoleUpdated']['MM'] == 0;
          $resultArray['teamRoleUpdated']['MA'] == 0;
        }

        //for personality type
        //$personalityPerArr = array('orgId'=>$orgId,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
        $personalityPerArr = array('orgId' => $orgId,'updateAt'=>false,'last12MonthFirstDate'=>$last12MonthFirstDate,'thisMonthLastDate'=>$thisMonthLastDate,'last6MonthFirstDate'=>$last6MonthFirstDate,'lastMonth'=>$lastMonth,'thisMonth'=>$thisMonth,'startOfSubWeek'=>$startOfSubWeek,'endOfSubWeek'=>$endOfSubWeek,'startOfWeek'=>$startOfWeek,'endOfWeek'=>$endOfWeek,'yesterday'=>$yesterday,'today'=>$today);

        $personalityTypeStatus = $this->getPersonalityType($personalityPerArr);

        $personalityTypeCount = count($personalityTypeStatus);

        $resultArray['personalityTypeCompleted'] = "0";
        if (!empty($personalityTypeCount) && !empty($userCount))
        {
            $resultArray['personalityTypeCompleted'] = number_format(($personalityTypeCount/$userCount)*100,2);
        }

        //for personality type updated
        //$personalityUpdatePerArr=array('orgId'=>$orgId,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
        $personalityUpdatePerArr = array('orgId' => $orgId,'updateAt'=>false,'last12MonthFirstDate'=>$last12MonthFirstDate,'thisMonthLastDate'=>$thisMonthLastDate,'last6MonthFirstDate'=>$last6MonthFirstDate,'lastMonth'=>$lastMonth,'thisMonth'=>$thisMonth,'startOfSubWeek'=>$startOfSubWeek,'endOfSubWeek'=>$endOfSubWeek,'startOfWeek'=>$startOfWeek,'endOfWeek'=>$endOfWeek,'yesterday'=>$yesterday,'today'=>$today);

        $personalityTypeUpdateStatus = $this->getPersonalityType($personalityUpdatePerArr);

        $personalityTypeUpdateCount = count($personalityTypeUpdateStatus);

        $resultArray['personalityTypeUpdated'] = "0";
        if (!empty($personalityTypeUpdateCount) && !empty($userCount))
        {
            $resultArray['personalityTypeUpdated'] = number_format(($personalityTypeUpdateCount/$userCount)*100,2);
        }

        //Personality type MM and MA count
        $personalityTypeCompletedArr = array('orgId'=>$orgId,'date'=>'created_at'); 
        $personalityTypeUpdatedArr = array('orgId'=>$orgId,'date'=>'updated_at'); 

        // $resultArray['personalityTypeCompletedMAMM'] = $this->getPersonalityTypeMAMMCount($personalityTypeArr);

        // $resultArray['personalityTypeUpdatedMAMM'] = $this->getPersonalityTypeMAMMCount($personalityTypeArr);
        $resultArray['personalityTypeCompletedMAMM'] = 0;
        $resultArray['personalityTypeUpdatedMAMM'] = 0;
        if (!empty($staff)) {
          $resultArray['personalityTypeCompletedMAMM'] = $this->getPersonalityTypeMAMMCount($personalityTypeCompletedArr);

          $resultArray['personalityTypeUpdatedMAMM'] = $this->getPersonalityTypeMAMMCount($personalityTypeUpdatedArr);
        }
        else
        {
          $resultArray['personalityTypeCompletedMAMM']['MM'] == 0;
          $resultArray['personalityTypeCompletedMAMM']['MA'] == 0;

          $resultArray['personalityTypeUpdatedMAMM']['MM'] == 0;
          $resultArray['personalityTypeUpdatedMAMM']['MA'] == 0;
        }



        //for culture structure comleted        
        //$cultureStructurePerArr = array('orgId' => $orgId,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
        $cultureStructurePerArr = array('orgId' => $orgId,'updateAt'=>false,'last12MonthFirstDate'=>$last12MonthFirstDate,'thisMonthLastDate'=>$thisMonthLastDate,'last6MonthFirstDate'=>$last6MonthFirstDate,'lastMonth'=>$lastMonth,'thisMonth'=>$thisMonth,'startOfSubWeek'=>$startOfSubWeek,'endOfSubWeek'=>$endOfSubWeek,'startOfWeek'=>$startOfWeek,'endOfWeek'=>$endOfWeek,'yesterday'=>$yesterday,'today'=>$today);

        $cultureStructureStatus = $this->getCultureStructure($cultureStructurePerArr);


        $cultureStructureCount = count($cultureStructureStatus);

        $resultArray['cultureStructureCompleted'] = "0";
        if (!empty($cultureStructureCount) && !empty($userCount))
        {
            $resultArray['cultureStructureCompleted'] = number_format(($cultureStructureCount/$userCount)*100,2);
        }


        //for culture  structure updated
        //$cultureStructureUpdatePerArr = array('orgId' => $orgId,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
        $cultureStructureUpdatePerArr = array('orgId' => $orgId,'updateAt'=>true,'last12MonthFirstDate'=>$last12MonthFirstDate,'thisMonthLastDate'=>$thisMonthLastDate,'last6MonthFirstDate'=>$last6MonthFirstDate,'lastMonth'=>$lastMonth,'thisMonth'=>$thisMonth,'startOfSubWeek'=>$startOfSubWeek,'endOfSubWeek'=>$endOfSubWeek,'startOfWeek'=>$startOfWeek,'endOfWeek'=>$endOfWeek,'yesterday'=>$yesterday,'today'=>$today);

        $cultureStructureUpdateStatus = $this->getCultureStructure($cultureStructureUpdatePerArr);

        $cultureStructureUpdateCount = count($cultureStructureUpdateStatus);

        $resultArray['cultureStructureUpdated'] = "0";
        if (!empty($cultureStructureUpdateCount) && !empty($userCount))
        {
            $resultArray['cultureStructureUpdated'] = number_format(($cultureStructureUpdateCount/$userCount)*100,2);
        }

        //culture  structure MM and MA count
        $cultureStructureCompletedArr = array('orgId'=>$orgId,'date'=>'created_at'); 
        $cultureStructureUpdatedArr = array('orgId'=>$orgId,'date'=>'updated_at'); 

        // $resultArray['cultureStructureCompletedMAMM'] = $this->getCultureStructureMAMMCount($cultureStructureArr);

        // $resultArray['cultureStructureUpdatedMAMM'] = $this->getCultureStructureMAMMCount($cultureStructureArr);

        $resultArray['cultureStructureCompletedMAMM'] = 0;
        $resultArray['cultureStructureUpdatedMAMM'] = 0;
        if (!empty($staff)) {
          $resultArray['cultureStructureCompletedMAMM'] = $this->getCultureStructureMAMMCount($cultureStructureCompletedArr);

          $resultArray['cultureStructureUpdatedMAMM'] = $this->getCultureStructureMAMMCount($cultureStructureUpdatedArr);
        }
        else
        {
          $resultArray['cultureStructureCompletedMAMM']['MM'] == 0;
          $resultArray['cultureStructureCompletedMAMM']['MA'] == 0;

          $resultArray['cultureStructureUpdatedMAMM']['MM'] == 0;
          $resultArray['cultureStructureUpdatedMAMM']['MA'] == 0;
        }
        
        //for motivation comleted       
        //$motivationPerArr = array('orgId' => $orgId,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
        $motivationPerArr = array('orgId' => $orgId,'updateAt'=>false,'last12MonthFirstDate'=>$last12MonthFirstDate,'thisMonthLastDate'=>$thisMonthLastDate,'last6MonthFirstDate'=>$last6MonthFirstDate,'lastMonth'=>$lastMonth,'thisMonth'=>$thisMonth,'startOfSubWeek'=>$startOfSubWeek,'endOfSubWeek'=>$endOfSubWeek,'startOfWeek'=>$startOfWeek,'endOfWeek'=>$endOfWeek,'yesterday'=>$yesterday,'today'=>$today);

        $motivationStatus = $this->getMotivation($motivationPerArr);

        $motivationCount = count($motivationStatus);

        $resultArray['motivationCompleted'] = "0";
        if (!empty($motivationCount) && !empty($userCount))
        {
            $resultArray['motivationCompleted'] = number_format(($motivationCount/$userCount)*100,2);
        }

        //for motivation type updated
        //$motivationCountPerArr = array('orgId' => $orgId,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
        $motivationCountPerArr = array('orgId' => $orgId,'updateAt'=>false,'last12MonthFirstDate'=>$last12MonthFirstDate,'thisMonthLastDate'=>$thisMonthLastDate,'last6MonthFirstDate'=>$last6MonthFirstDate,'lastMonth'=>$lastMonth,'thisMonth'=>$thisMonth,'startOfSubWeek'=>$startOfSubWeek,'endOfSubWeek'=>$endOfSubWeek,'startOfWeek'=>$startOfWeek,'endOfWeek'=>$endOfWeek,'yesterday'=>$yesterday,'today'=>$today);

        $motivationCountUpdateStatus = $this->getMotivation($motivationCountPerArr);

        $motivationCountUpdateCount = count($motivationCountUpdateStatus);

        $resultArray['motivationCountUpdated'] = "0";
        if (!empty($motivationCountUpdateCount) && !empty($userCount))
        {
            $resultArray['motivationCountUpdated'] = number_format(($motivationCountUpdateCount/$userCount)*100,2);
        }

        //Motivation MM and MA count
        $motivationCompletedArr = array('orgId'=>$orgId,'date'=>'created_at'); 
        $motivationUpdatedArr = array('orgId'=>$orgId,'date'=>'updated_at'); 

        // $resultArray['motivationCompletedMAMM'] = $this->getMotivationMAMMCount($motivationArr);

        // $resultArray['motivationUpdatedMAMM'] = $this->getMotivationMAMMCount($motivationArr);

        $resultArray['motivationCompletedMAMM'] = 0;
        $resultArray['motivationUpdatedMAMM'] = 0;
        if (!empty($staff)) {
          $resultArray['motivationCompletedMAMM'] = $this->getMotivationMAMMCount($motivationCompletedArr);

          $resultArray['motivationUpdatedMAMM'] = $this->getMotivationMAMMCount($motivationUpdatedArr);
        }
        else
        {
          $resultArray['motivationCompletedMAMM']['MM'] == 0;
          $resultArray['motivationCompletedMAMM']['MA'] == 0;

          $resultArray['motivationUpdatedMAMM']['MM'] == 0;
          $resultArray['motivationUpdatedMAMM']['MA'] == 0;
        }



        //for Diagnostic updated
        $diagnosticUpdatedPerArr = array('orgId' => $orgId,'updateAt'=>true,'last12MonthFirstDate'=>$last12MonthFirstDate,'thisMonthLastDate'=>$thisMonthLastDate,'last6MonthFirstDate'=>$last6MonthFirstDate,'lastMonth'=>$lastMonth,'thisMonth'=>$thisMonth,'startOfSubWeek'=>$startOfSubWeek,'endOfSubWeek'=>$endOfSubWeek,'startOfWeek'=>$startOfWeek,'endOfWeek'=>$endOfWeek,'yesterday'=>$yesterday,'today'=>$today);

        $diagnosticUpdateStatus = $this->getDiagnostic($diagnosticUpdatedPerArr);

        $diagnosticUpdateCount = count($diagnosticUpdateStatus);

        $resultArray['diagnosticUpdated'] = "0";
        if (!empty($diagnosticUpdateCount) && !empty($userCount))
        {
            $resultArray['diagnosticUpdated'] = number_format(($diagnosticUpdateCount/$userCount)*100,2);
        }

        //Diagnostic MM and MA count
        $diagnosticUpdateArr = array('orgId'=>$orgId,'date'=>'updated_at'); 

        //$resultArray['diagnosticUpdatedMAMM'] = $this->getDiagnosticMAMMCount($diagnosticUpdateArr);

        $resultArray['diagnosticUpdatedMAMM'] = 0;
        if (!empty($staff)) {
          $resultArray['diagnosticUpdatedMAMM'] = $this->getDiagnosticMAMMCount($diagnosticUpdateArr);
        }
        else
        {
          $resultArray['diagnosticUpdatedMAMM']['MM'] == 0;
          $resultArray['diagnosticUpdatedMAMM']['MA'] == 0;
        }

        //for tribeometer updated
        $tribeometerUpdatedPerArr = array('orgId' => $orgId,'updateAt'=>true,'last12MonthFirstDate'=>$last12MonthFirstDate,'thisMonthLastDate'=>$thisMonthLastDate,'last6MonthFirstDate'=>$last6MonthFirstDate,'lastMonth'=>$lastMonth,'thisMonth'=>$thisMonth,'startOfSubWeek'=>$startOfSubWeek,'endOfSubWeek'=>$endOfSubWeek,'startOfWeek'=>$startOfWeek,'endOfWeek'=>$endOfWeek,'yesterday'=>$yesterday,'today'=>$today);

        $tribeometerUpdateStatus = $this->getTribeometer($tribeometerUpdatedPerArr);

        $tribeometerUpdateCount = count($tribeometerUpdateStatus);

        $resultArray['tribeometerUpdated'] = "0";
        if (!empty($tribeometerUpdateCount) && !empty($userCount))
        {
            $resultArray['tribeometerUpdated'] = number_format(($tribeometerUpdateCount/$userCount)*100,2);
        }

        //Tribeometer MM and MA count
        $tribeometerUpdateArr = array('orgId'=>$orgId,'date'=>'updated_at'); 

        //$resultArray['tribeometerUpdatedMAMM'] = $this->getTribeometerMAMMCount($tribeometerUpdateArr);

        $resultArray['tribeometerUpdatedMAMM'] = 0;
        if (!empty($staff)) {
          $resultArray['tribeometerUpdatedMAMM'] = $this->getTribeometerMAMMCount($tribeometerUpdateArr);
        }
        else
        {
          $resultArray['tribeometerUpdatedMAMM']['MM'] == 0;
          $resultArray['tribeometerUpdatedMAMM']['MA'] == 0;
        }

        return view('admin/performance/performance',compact('resultArray','organisations','orgId','reportType','date'));
    }

    protected function lastMonthStaffCount($orgId)
    {
        $lastMonthStaffQuery = DB::table('users')
          ->where('status','Active')
          ->whereDate('created_at','<',date('Y-m'."-1"))
          ->where('roleId',3);
          if (!empty($orgId)) {
            $lastMonthStaffQuery->where('orgId',$orgId);
          }
        $lastMonthStaffCount = $lastMonthStaffQuery->count();

        return $lastMonthStaffCount;
    }
    protected function thisMonthStaffCount($orgId)
    {
        $thisMonthStaffQuery = DB::table('users')
          ->where('status','Active')
          ->where('roleId',3);
          //->whereDate('created_at','<',date('Y-m'."-1"));
          if (!empty($orgId)) {
            $thisMonthStaffQuery->where('orgId',$orgId);
          }
        $thisMonthStaffCount = $thisMonthStaffQuery->count();

        return $thisMonthStaffCount;
    }

    //Get MM and Moving average for bubble count
    public function getBubbleRatingCount($bubbleRatingData = array())
    {
        $orgId = $bubbleRatingData["orgId"];

        // $lastMonthStaffQuery = DB::table('users')
        // ->where('status','Active')
        // ->whereDate('created_at','<',date('Y-m'."-1"));
        // if (!empty($orgId)) {
        //   $lastMonthStaffQuery->where('orgId',$orgId);
        // }
        // $lastMonthStaffCount = $lastMonthStaffQuery->count();

        $lastMonthStaffCount = $this->lastMonthStaffCount($orgId);

        //Last month query
        $lastMonthQuery = DB::table('dot_bubble_rating_records')
        ->select('dot_bubble_rating_records.id','dot_bubble_rating_records.created_at')
        ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
        ->where('dot_bubble_rating_records.status','Active')
        ->where('dot_bubble_rating_records.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        if(!empty($orgId))
        {
            $lastMonthQuery->where('dots.orgId',$orgId);
        }
        $lastMonthBubbleCount = $lastMonthQuery->count('bubble_flag');

        //Last Month value
        $lastMonthBubble = 0;
        if (!empty($lastMonthBubbleCount) && !empty($lastMonthStaffCount)) 
        {
          $lastMonthBubble = ($lastMonthBubbleCount)/($lastMonthStaffCount);
        }

        // $thisMonthStaffQuery = DB::table('users')
        // ->where('status','Active');
        // if (!empty($orgId)) {
        //   $thisMonthStaffQuery->where('orgId',$orgId);
        // }
        // $thisMonthStaffCount = $thisMonthStaffQuery->count();

        $thisMonthStaffCount = $this->thisMonthStaffCount($orgId);

        //This month query
        $thisMonthQuery = DB::table('dot_bubble_rating_records')
        ->select('dot_bubble_rating_records.id','dot_bubble_rating_records.created_at')
        ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
        ->where('dot_bubble_rating_records.status','Active')
        ->where('dot_bubble_rating_records.created_at','LIKE',date("Y-m")."%");
        if(!empty($orgId))
        {
            $thisMonthQuery->where('dots.orgId',$orgId);
        }
        $thisMonthBubbleCount = $thisMonthQuery->count('bubble_flag');

        //This Month value
        $thisMonthBubble = 0;
        if (!empty($thisMonthBubbleCount) && !empty($thisMonthStaffCount)) 
        {
          $thisMonthBubble = ($thisMonthBubbleCount)/($thisMonthStaffCount);
        }

        $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
        $createdDateStaff = $staff->created_at;
        
        //All previous month data till last month
        $allPreviousMonthDataQuery = DB::table('dot_bubble_rating_records')
        ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
        ->where('dot_bubble_rating_records.status','Active')
        ->where('dot_bubble_rating_records.created_at','>=',$createdDateStaff)
        ->whereDate('dot_bubble_rating_records.created_at','<',date('Y-m'."-1"));
        //->where('dot_bubble_rating_records.created_at', '<', date('Y-m'."-1"));
        if(!empty($orgId))
        {
            $allPreviousMonthDataQuery->where('dots.orgId',$orgId);
        }
        $allPreviousMonthDataCount = $allPreviousMonthDataQuery->count('bubble_flag');
        //print_r($allPreviousMonthDataCount);die();

        //all previous Month value
        $allPreviousMonthData = 0;
        if (!empty($allPreviousMonthDataCount) && !empty($lastMonthStaffCount)) 
        {
          $allPreviousMonthData = ($allPreviousMonthDataCount)/($lastMonthStaffCount);
        }

     //   print_r($allPreviousMonthData);die();


        //Count all months till last month
       // $thumbsupFirstData = DB::table('dot_bubble_rating_records')->select('created_at')->orderBy('created_at','ASC')->first();
        // $thumbsupFirstData = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
        
        // $firstDateOfOrg = date('Y-m-d',strtotime($thumbsupFirstData->created_at));
        // $lastMonthDate = date('Y-m-d',strtotime('-1 month'));

        $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));
        //print_r($noOfPreviousMonths);die();

        // $date1 = new DateTime($thumbsupFirstData->created_at);
        // $date2 = $date1->diff(new DateTime(Carbon::now()->subMonth()));
        // $allMonthcount = ($date2->y*12) + $date2->m + 2;

        //$bubbleRatingCount = array();
        $bubbleRatingCount['MM'] = 0;
        $bubbleRatingCount['MA'] = 0;
        $bubbleCountMA = 0;
        
        if (!empty($lastMonthBubble)) {

            $bubbleRatingCount['MM'] = number_format(((-1*($lastMonthBubble - $thisMonthBubble))/$lastMonthBubble)*100,2);
        }
        // if (!empty($allPreviousMonthDataCount) && !empty($allMonthcount)) {

        //     $bubbleCountMA = ($allPreviousMonthDataCount)/($allMonthcount);
        //     if (!empty($bubbleCountMA)) {
                
        //         $bubbleRatingCount['MA'] = number_format(((-1*($bubbleCountMA - $thisMonthBubbleCount))/$bubbleCountMA)*100,2);
        //     }
        // }
        if (!empty($allPreviousMonthData) && !empty($noOfPreviousMonths)) {

            $bubbleCountMA = ($allPreviousMonthData)/($noOfPreviousMonths);
            if (!empty($bubbleCountMA)) {
                
                $bubbleRatingCount['MA'] = number_format(((-1*($bubbleCountMA - $thisMonthBubble))/$bubbleCountMA)*100,2);
            }
        }
        return $bubbleRatingCount;
    }

    // public function getBubbleRatingCount($bubbleRatingData = array())
    // {
    //     $orgId = $bubbleRatingData["orgId"];

    //     //Last month query
    //     $lastMonthQuery = DB::table('dot_bubble_rating_records')
    //     ->select('dot_bubble_rating_records.id','dot_bubble_rating_records.created_at')
    //     ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
    //     ->where('dot_bubble_rating_records.status','Active')
    //     ->where('dot_bubble_rating_records.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
     
    //     if(!empty($orgId))
    //     {
    //         $lastMonthQuery->where('dots.orgId',$orgId);
    //     }
    //     $lastMonthBubbleCount = $lastMonthQuery->count('bubble_flag');

    //     //Last to last month query
    //     $lastToLastMonthQuery = DB::table('dot_bubble_rating_records')
    //     ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
    //     ->where('dot_bubble_rating_records.status','Active')
    //     ->where('dot_bubble_rating_records.created_at','LIKE',date('Y-m', strtotime('-2 months'))."%");
                  
    //     if(!empty($orgId))
    //     {
    //         $lastToLastMonthQuery->where('dots.orgId',$orgId);
    //     }
    //     $lastToLastMonthBubbleCount = $lastToLastMonthQuery->count('bubble_flag');

    //     //All previous month data till last month

    //     $allPreviousMonthDataQuery = DB::table('dot_bubble_rating_records')
    //     ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id');
    //     if(!empty($orgId))
    //     {
    //         $allPreviousMonthDataQuery->where('dots.orgId',$orgId);
    //     }
    //     $allPreviousMonthDataCount = $allPreviousMonthDataQuery
    //     ->where('dot_bubble_rating_records.status','Active')
    //     ->where('dot_bubble_rating_records.created_at', '<', date('Y-m'."-1"))
    //     ->count('bubble_flag');

    //     //Count all months till last month
    //     $thumbsupFirstData = DB::table('dot_bubble_rating_records')->select('created_at')->orderBy('created_at','ASC')->first();

    //     $date1 = new DateTime($thumbsupFirstData->created_at);
    //     $date2 = $date1->diff(new DateTime(Carbon::today()));
    //     $allMonthcount = ($date2->y*12) + $date2->m;

    //     $bubbleRatingCount = array();
    //     $bubbleRatingCount['MM'] = 0;
    //     $bubbleRatingCount['MA'] = 0;
        
    //     if (!empty($lastToLastMonthBubbleCount)) {

    //         $bubbleRatingCount['MM'] = number_format(-1*($lastToLastMonthBubbleCount - $lastMonthBubbleCount)/($lastToLastMonthBubbleCount),2);
    //     }
        
    //     if (!empty($allPreviousMonthDataCount) && !empty($allMonthcount)) {

    //         $bubbleCountMA = ($allPreviousMonthDataCount)/($allMonthcount);
    //         if (!empty($bubbleCountMA)) {
                
    //             $bubbleRatingCount['MA'] = number_format(-1*($bubbleCountMA - $lastMonthBubbleCount)/($bubbleCountMA),2);
    //         }
    //     }
    //     return $bubbleRatingCount;
    // }

    //Get MM and Moving average for improvement
    public function getImprovementsMMMA($improvementArr = array())
    {
        $orgId = $improvementArr["orgId"];

        // $lastMonthStaffQuery = DB::table('users')
        // ->where('status','Active')
        // ->whereDate('created_at','<',date('Y-m'."-1"));
        // if (!empty($orgId)) {
        //   $lastMonthStaffQuery->where('orgId',$orgId);
        // }
        // $lastMonthStaffCount = $lastMonthStaffQuery->count();

        $lastMonthStaffCount = $this->lastMonthStaffCount($orgId);

        //Last month query
        $lastMonthQuery = DB::table('iot_feedbacks')
        ->where('iot_feedbacks.status','!=','Inactive')
        ->where('iot_feedbacks.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        if(!empty($orgId))
        {
            $lastMonthQuery->where('orgId',$orgId);
        }
        $lastMonthImprovementCount = $lastMonthQuery->count();

        //Last Month value
        $lastMonthImprovement = 0;
        if (!empty($lastMonthImprovementCount) && !empty($lastMonthStaffCount)) 
        {
          $lastMonthImprovement = ($lastMonthImprovementCount)/($lastMonthStaffCount);
        }

        // $thisMonthStaffQuery = DB::table('users')
        // ->where('status','Active');
        // if (!empty($orgId)) {
        //   $thisMonthStaffQuery->where('orgId',$orgId);
        // }
        // $thisMonthStaffCount = $thisMonthStaffQuery->count();

        $thisMonthStaffCount = $this->thisMonthStaffCount($orgId);

        //This month query
        $thisMonthQuery = DB::table('iot_feedbacks')
        ->where('iot_feedbacks.status','!=','Inactive')
        ->where('iot_feedbacks.created_at','LIKE',date("Y-m")."%");
        if(!empty($orgId))
        {
            $thisMonthQuery->where('orgId',$orgId);
        }
        $thisMonthImprovementCount = $thisMonthQuery->count();

        //This Month value
        $thisMonthImprovement = 0;
        if (!empty($thisMonthImprovementCount) && !empty($thisMonthStaffCount)) 
        {
          $thisMonthImprovement = ($thisMonthImprovementCount)/($thisMonthStaffCount);
        }

        $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
        $createdDateStaff = $staff->created_at;

        //All previous month data till last month
        $allPreviousMonthDataQuery = DB::table('iot_feedbacks')
        ->where('iot_feedbacks.status','!=','Inactive')
        ->where('iot_feedbacks.created_at','>=',$createdDateStaff)
        ->whereDate('iot_feedbacks.created_at','<',date('Y-m'."-1"));
        //->where('iot_feedbacks.created_at', '<', date('Y-m'."-1"));
        if(!empty($orgId))
        {
            $allPreviousMonthDataQuery->where('orgId',$orgId);
        }
        $allPreviousMonthDataCount = $allPreviousMonthDataQuery->count();

        //all previous Month value
        $allPreviousMonthData = 0;
        if (!empty($allPreviousMonthDataCount) && !empty($lastMonthStaffCount)) 
        {
          $allPreviousMonthData = ($allPreviousMonthDataCount)/($lastMonthStaffCount);
        }

        //Count all months till last month
        // $improvementFirstData = DB::table('iot_feedbacks')->select('created_at')->orderBy('created_at','ASC')->first();

        // $date1 = new DateTime($improvementFirstData->created_at);
        // $date2 = $date1->diff(new DateTime(Carbon::now()->subMonth()));
        // $allMonthcount = ($date2->y*12) + $date2->m + 2;


        $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));

        $improvementCount = array();
        $improvementCount['MM'] = 0;
        $improvementCount['MA'] = 0;
        $improvementMA = 0;
        
        if (!empty($lastMonthImprovement)) 
        {
          $improvementCount['MM'] = number_format(((-1*($lastMonthImprovement - $thisMonthImprovement))/$lastMonthImprovement)*100,2);
        }
        
        // if (!empty($allPreviousMonthDataCount) && !empty($allMonthcount)) {

        //     $improvementMA = ($allPreviousMonthDataCount)/($allMonthcount);
        //     if (!empty($improvementMA)) {
                
        //         $improvementCount['MA'] = number_format(((-1*($improvementMA - $thisMonthImprovement))/$improvementMA)*100,2);
        //     }
        // }
        if (!empty($allPreviousMonthData) && !empty($noOfPreviousMonths)) {

            $improvementMA = ($allPreviousMonthData)/($noOfPreviousMonths);
            if (!empty($improvementMA)) {
                
                $improvementCount['MA'] = number_format(((-1*($improvementMA - $thisMonthImprovement))/$improvementMA)*100,2);
            }
        }
        return $improvementCount;
    }

// public function getImprovementsMMMA($improvementArr = array())
// {
//     $orgId = $improvementArr["orgId"];

//     //Last month query
//     $lastMonthQuery = DB::table('iot_feedbacks')
//     ->where('iot_feedbacks.status','!=','Inactive')
//     ->where('iot_feedbacks.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
      
//     if(!empty($orgId))
//     {
//         $lastMonthQuery->where('orgId',$orgId);
//     }
//     $lastMonthImprovement = $lastMonthQuery->count();

//     //Last to last month query
//     $lastToLastMonthQuery = DB::table('iot_feedbacks')
//     ->where('iot_feedbacks.status','!=','Inactive')
//     ->where('iot_feedbacks.created_at','LIKE',date('Y-m', strtotime('-2 months'))."%");

//     if(!empty($orgId))
//     {
//         $lastToLastMonthQuery->where('orgId',$orgId);
//     }
//     $lastToLastMonthImprovement = $lastToLastMonthQuery->count();

//     //All previous month data till last month

//     $allPreviousMonthDataQuery = DB::table('iot_feedbacks')
//     ->where('iot_feedbacks.status','!=','Inactive');
//     if(!empty($orgId))
//     {
//         $allPreviousMonthDataQuery->where('orgId',$orgId);
//     }
//     $allPreviousMonthDataCount = $allPreviousMonthDataQuery
//     ->where('iot_feedbacks.created_at', '<', date('Y-m'."-1"))
//     ->count();

//     //Count all months till last month
//     $improvementFirstData = DB::table('iot_feedbacks')->select('created_at')->orderBy('created_at','ASC')->first();

//     $date1 = new DateTime($improvementFirstData->created_at);
//     $date2 = $date1->diff(new DateTime(Carbon::today()));
//     $allMonthcount = ($date2->y*12) + $date2->m;

//     $improvementCount = array();
//     $improvementCount['MM'] = 0;
//     $improvementCount['MA'] = 0;
    
//     if (!empty($lastToLastMonthImprovement)) {

//         $improvementCount['MM'] = number_format(-1*($lastToLastMonthImprovement - $lastMonthImprovement)/($lastToLastMonthImprovement),2);
//     }
    
//     if (!empty($allPreviousMonthDataCount) && !empty($allMonthcount)) {

//         $improvementMA = ($allPreviousMonthDataCount)/($allMonthcount);
//         if (!empty($improvementMA)) {
            
//             $improvementCount['MA'] = number_format(-1*($improvementMA - $lastMonthImprovement)/($improvementMA),2);
//         }
//     }
//     return $improvementCount;
// }

    //Get MM and Moving average for Dot Completed and updated
    public function getDotRatingCount($dotRatingData = array())
    {

      $orgId    = $dotRatingData["orgId"];
      $userId   = $dotRatingData["userId"];
      $date   = $dotRatingData["date"];

      //print_r($date);die();

      if(!$orgId)
      {
          $user = DB::table('users')->where('status','Active')->where('id',$userId)->first();
          $orgId ='';
          if($user)
          {
              $orgId = $user->orgId;
          }           
      }

      $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
     // print_r($organisation);die();
      $createdDateStaff = $staff->created_at;
      //print_r($organisation->created_at);die();

      $query = DB::table('dots_values')  
      ->select('dots_values.id AS id')     
      ->leftjoin('dots_beliefs','dots_beliefs.id','dots_values.beliefId')
      ->leftjoin('dots','dots.id','dots_beliefs.dotId')
      ->where('dots_beliefs.status','Active')
      ->where('dots.status','Active')
      ->where('dots_values.status','Active');
      if($orgId) 
      {
          $query->where('dots.orgId', $orgId);
      }
      if ($date == 'thisMonth') {
        $query->where('dots_values.created_at','LIKE',date("Y-m")."%");
      }
      elseif ($date == 'lastMonth') {
        $query->where('dots_values.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
      }
      elseif ($date == 'allPreviousMonth') {
        $query->where('dots_values.created_at','>=',$createdDateStaff);
        $query->whereDate('dots_values.created_at','<',date('Y-m'."-1"));
        //$query->whereBetween('dots_values.created_at',array($createdDateStaff,date('Y-m'."-1")));
        //$query->whereBetween('dot_values_ratings.created_at', '<', date('Y-m'."-1"));
      }
      $dotValues = $query->get();
      //print_r($query->toSql()."<br>");
      //echo "<pre>";print_r($query->getBindings());
    //  echo "Dot values1=> ".$date."  ";print_r($dotValues);//die();
      //die();
      // echo "<pre> User id";print_r($userId);
     //  echo "<pre> Dot values";print_r($dotValues);
      foreach($dotValues as $value)
      {
          $dvValue = DB::table('dot_values_ratings')
          ->rightjoin('users','users.id','dot_values_ratings.userId')
          ->where('users.status','Active')
          ->where('dot_values_ratings.userId',$userId)
          ->where('dot_values_ratings.valueId',$value->id)
          ->where('dot_values_ratings.status','Active');
          //->where('dot_values_ratings.created_at',$date);
          if ($date == 'thisMonth') {
            $dvValue->where('dot_values_ratings.created_at','LIKE',date("Y-m")."%");
          }
          elseif ($date == 'lastMonth') {
            $dvValue->where('dot_values_ratings.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
          }
          elseif ($date == 'allPreviousMonth') {
            $dvValue->where('dot_values_ratings.created_at','>=',$createdDateStaff);
            $dvValue->whereDate('dot_values_ratings.created_at','<',date('Y-m'."-1"));
            //$dvValue->whereBetween('dot_values_ratings.created_at',array($createdDateStaff,date('Y-m'."-1")));
            //$dvValue->whereBetween('dot_values_ratings.created_at', '<', date('Y-m'."-1"));
          }
          $isDotValueRating = $dvValue->first();

          //echo "ratings => ";print_r($isDotValueRating);
          //print_r($dvValue->getBindings());
          if(!$isDotValueRating)
          {
              return false;
          }
      }
      //print_r(count($dotValues));
      //echo "Dot values2=> ".$date."  ";print_r($dotValues);//die();
      if (count($dotValues) == 0) {
      //  print_r("exp");
        return false;
      }
      else
      {
       // print_r('expression');
        return true;
      }

    /*
        $orgId = $dotRatingData["orgId"];
        $date = $dotRatingData["date"];

        //Last month query
        $lastMonthDotValueRatingQuery = DB::table('dot_values_ratings')
            ->leftjoin('dots','dots.id','dot_values_ratings.dotId')
            ->where('dot_values_ratings.status','Active')
            ->where('dot_values_ratings.'.$date,'LIKE',date("Y-m", strtotime("-1 month"))."%");
            if(!empty($orgId))
            {
            $lastMonthDotValueRatingQuery->where('dots.orgId',$orgId);
            }
        $lastMonthDotValueRating = $lastMonthDotValueRatingQuery->sum('ratings');

        //This month query
        $thisMonthDotValueRatingQuery = DB::table('dot_values_ratings')
            ->leftjoin('dots','dots.id','dot_values_ratings.dotId')
            ->where('dot_values_ratings.status','Active')
            ->where('dot_values_ratings.'.$date,'LIKE',date("Y-m")."%");
            if(!empty($orgId))
            {
            $thisMonthDotValueRatingQuery->where('dots.orgId',$orgId);
            }
        $thisMonthDotValueRating = $thisMonthDotValueRatingQuery->sum('ratings');

        //All previous month data till last month
        $allPreviousMonthDotDataQuery = DB::table('dot_values_ratings')
            ->select('dot_values_ratings.*')
            ->leftjoin('dots','dots.id','dot_values_ratings.dotId')
            ->where('dot_values_ratings.status','Active')
            ->where('dot_values_ratings.created_at', '<', date('Y-m'."-1"));
            if(!empty($orgId))
            {
                $allPreviousMonthDotDataQuery->where('dots.orgId',$orgId);
            }
        $allPreviousMonthDotDataSum = $allPreviousMonthDotDataQuery->sum('ratings');
        
        //Count all months till last month
        $dotValueRatingFirstData = DB::table('dot_values_ratings')->select('created_at')->orderBy('created_at','ASC')->first();
        $date1 = new DateTime($dotValueRatingFirstData->created_at);
        $date2 = $date1->diff(new DateTime(Carbon::now()->subMonth()));
        $allMonthcount = ($date2->y*12) + $date2->m + 2;

        $dotRatingCount = array();
        $dotRatingCount['MM'] = 0;
        $dotRatingCount['MA'] = 0;
        $dotCountMA = 0;
        
        if (!empty($lastMonthDotValueRating)) {

            $dotRatingCount['MM'] = number_format(((-1*($lastMonthDotValueRating - $thisMonthDotValueRating))/$lastMonthDotValueRating)*100,2);
        }
        
        if (!empty($allPreviousMonthDotDataSum) && !empty($allMonthcount)) {

            $dotCountMA = ($allPreviousMonthDotDataSum)/($allMonthcount);
            if (!empty($dotCountMA)) 
            {    
                $dotRatingCount['MA'] = number_format(((-1*($dotCountMA - $thisMonthDotValueRating))/$dotCountMA)*100,2);
            }
        }
        return $dotRatingCount;
    */
      }
// public function getDotRatingCount($dotRatingData = array())
// {
//     $orgId = $dotRatingData["orgId"];

//     $date = $dotRatingData["date"];

//     //Last month query

//     $lastMonthDotValueRatingQuery = DB::table('dot_values_ratings')
//         ->leftjoin('dots','dots.id','dot_values_ratings.dotId')
//         ->where('dot_values_ratings.status','Active')
//         ->where('dot_values_ratings.'.$date,'LIKE',date("Y-m", strtotime("-1 month"))."%");
        
//         if(!empty($orgId))
//         {
//         $lastMonthDotValueRatingQuery->where('dots.orgId',$orgId);
//         }
//     $lastMonthDotValueRating = $lastMonthDotValueRatingQuery->sum('ratings');

  
//     //Last to last month query

//     $lastToLastMonthDotValueRatingQuery = DB::table('dot_values_ratings')
//         ->leftjoin('dots','dots.id','dot_values_ratings.dotId')
//         ->where('dot_values_ratings.status','Active')
//         ->where('dot_values_ratings.'.$date,'LIKE',date('Y-m', strtotime('-2 months'))."%");

//         if(!empty($orgId))
//         {
//             $lastToLastMonthDotValueRatingQuery->where('dots.orgId',$orgId);
//         }
//     $lastToLastMonthDotValueRating = $lastToLastMonthDotValueRatingQuery->sum('ratings');
 
//     //All previous month data till last month

//     $allPreviousMonthDotDataQuery = DB::table('dot_values_ratings')
//         ->select('dot_values_ratings.*')
//         ->leftjoin('dots','dots.id','dot_values_ratings.dotId')
//         ->where('dot_values_ratings.status','Active')
//         ->where('dot_values_ratings.created_at', '<', date('Y-m'."-1"));

//         if(!empty($orgId))
//         {
//             $allPreviousMonthDotDataQuery->where('dots.orgId',$orgId);
//         }
//     $allPreviousMonthDotDataSum = $allPreviousMonthDotDataQuery->sum('ratings');
    
//     //Count all months till last month

//     $dotValueRatingFirstData = DB::table('dot_values_ratings')->select('created_at')->orderBy('created_at','ASC')->first();

//     $date1 = new DateTime($dotValueRatingFirstData->created_at);
//     $date2 = $date1->diff(new DateTime(Carbon::today()));
//     $allMonthcount = ($date2->y*12) + $date2->m;

//     $dotRatingCount = array();
//     $dotRatingCount['MM'] = 0;
//     $dotRatingCount['MA'] = 0;
    
//     if (!empty($lastToLastMonthDotValueRating)) {

//         $dotRatingCount['MM'] = number_format(-1*($lastToLastMonthDotValueRating - $lastMonthDotValueRating)/($lastToLastMonthDotValueRating),2);
//     }
    
//     if (!empty($allPreviousMonthDotDataSum) && !empty($allMonthcount)) {

//         $dotCountMA = ($allPreviousMonthDotDataSum)/($allMonthcount);
//         if (!empty($dotCountMA)) {
            
//             $dotRatingCount['MA'] = number_format(-1*($dotCountMA - $lastMonthDotValueRating)/($dotCountMA),2);
//         }
//     }
//     return $dotRatingCount;
// }

    //Get MM and Moving average for Team role Completed and updated
    public function getTeamroleMAMMCount($teamRoleArr = array())
    {
        $orgId = $teamRoleArr["orgId"];
        $date = $teamRoleArr["date"];

        $lastMonthStaffCount = $this->lastMonthStaffCount($orgId);

        //Last month query
        $lastMonthTeamRoleQuery = DB::table('cot_answers')
            ->leftjoin('users','users.id','cot_answers.userId')
            ->where('users.status','Active')
            ->where('cot_answers.status','Active')
            ->where('cot_answers.'.$date,'LIKE',date("Y-m", strtotime("-1 month"))."%");
            if(!empty($orgId))
            {
            $lastMonthTeamRoleQuery->where('cot_answers.orgId',$orgId);
            }
        $lastMonthTeamRoleCount = $lastMonthTeamRoleQuery->count();

        //Last Month value
        $lastMonthTeamRole = 0;
        if (!empty($lastMonthTeamRoleCount) && !empty($lastMonthStaffCount)) 
        {
          $lastMonthTeamRole = (($lastMonthTeamRoleCount)/($lastMonthStaffCount))*100;
        }

        $thisMonthStaffCount = $this->thisMonthStaffCount($orgId);

        //This month query
        $thisMonthTeamRoleQuery = DB::table('cot_answers')
            ->leftjoin('users','users.id','cot_answers.userId')
            ->where('users.status','Active')
            ->where('cot_answers.status','Active')
            ->where('cot_answers.'.$date,'LIKE',date("Y-m")."%");
            if(!empty($orgId))
            {
            $thisMonthTeamRoleQuery->where('cot_answers.orgId',$orgId);
            }
        $thisMonthTeamRoleCount = $thisMonthTeamRoleQuery->count();

        //This Month value
        $thisMonthTeamRole = 0;
        if (!empty($thisMonthTeamRoleCount) && !empty($thisMonthStaffCount)) 
        {
          $thisMonthTeamRole = (($thisMonthTeamRoleCount)/($thisMonthStaffCount))*100;
        }

        $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
        $createdDateStaff = $staff->created_at;

        //All previous month data till last month
        $allPreviousMonthTeamRoleQuery = DB::table('cot_answers')
            ->leftjoin('users','users.id','cot_answers.userId')
            ->where('users.status','Active')
            ->where('cot_answers.status','Active')
            ->where('cot_answers.'.$date,'>=',$createdDateStaff)
            ->whereDate('cot_answers.'.$date,'<',date('Y-m'."-1"));
            //->where('cot_answers.'.$date, '<', date('Y-m'."-1"));
            if(!empty($orgId))
            {
                $allPreviousMonthTeamRoleQuery->where('cot_answers.orgId',$orgId);
            }
        $allPreviousMonthTeamRoleCount = $allPreviousMonthTeamRoleQuery->count();

        //all previous Month value
        $allPreviousMonthTeamRole = 0;
        if (!empty($allPreviousMonthTeamRoleCount) && !empty($lastMonthStaffCount)) 
        {
          $allPreviousMonthTeamRole = (($allPreviousMonthTeamRoleCount)/($lastMonthStaffCount))*100;
        }
       
        //Count all months till last month
        // $TeamRoleFirstData = DB::table('cot_answers')->select('created_at')->orderBy('created_at','ASC')->first();

        // $date1 = new DateTime($TeamRoleFirstData->created_at);
        // $date2 = $date1->diff(new DateTime(Carbon::now()->subMonth()));
        // $allMonthcount = ($date2->y*12) + $date2->m + 2;


        $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));

        $TeamRoleCount = array();
        $TeamRoleCount['MM'] = 0;
        $TeamRoleCount['MA'] = 0;
        $TeamRoleCountMA = 0;
        
        if (!empty($lastMonthTeamRole)) {

            $TeamRoleCount['MM'] = number_format(((-1*($lastMonthTeamRole - $thisMonthTeamRole))/$lastMonthTeamRole),2);
        }
        // if (!empty($allPreviousMonthTeamRoleCount) && !empty($allMonthcount)) {

        //     $TeamRoleCountMA = ($allPreviousMonthTeamRoleCount)/($allMonthcount);
        //     if (!empty($TeamRoleCountMA)) 
        //     {
        //         $TeamRoleCount['MA'] = number_format(((-1*($TeamRoleCountMA - $thisMonthTeamRoleCount))/$TeamRoleCountMA)*100,2);
        //     }
        // }
        if (!empty($allPreviousMonthTeamRole) && !empty($noOfPreviousMonths)) {

            $TeamRoleCountMA = ($allPreviousMonthTeamRole)/($noOfPreviousMonths);
            if (!empty($TeamRoleCountMA)) 
            {
                $TeamRoleCount['MA'] = number_format(((-1*($TeamRoleCountMA - $thisMonthTeamRole))/$TeamRoleCountMA),2);
            }
        }
        return $TeamRoleCount;
    }
// public function getTeamroleMAMMCount($teamRoleArr = array())
// {
//     $orgId = $teamRoleArr["orgId"];

//     $date = $teamRoleArr["date"];

//     //Last month query

//     $lastMonthTeamRoleQuery = DB::table('cot_answers')
//         ->leftjoin('users','users.id','cot_answers.userId')
//         ->where('users.status','Active')
//         ->where('cot_answers.status','Active')
//         ->where('cot_answers.'.$date,'LIKE',date("Y-m", strtotime("-1 month"))."%");

//         if(!empty($orgId))
//         {
//         $lastMonthTeamRoleQuery->where('cot_answers.orgId',$orgId);
//         }
//     $lastMonthTeamRoleCount = $lastMonthTeamRoleQuery->count();

//     //Last to last month query

//     $lastToLastMonthTeamRoleQuery = DB::table('cot_answers')
//         ->leftjoin('users','users.id','cot_answers.userId')
//         ->where('users.status','Active')
//         ->where('cot_answers.status','Active')
//         ->where('cot_answers.'.$date,'LIKE',date("Y-m", strtotime("-2 month"))."%");

//         if(!empty($orgId))
//         {
//             $lastToLastMonthTeamRoleQuery->where('cot_answers.orgId',$orgId);
//         }
//     $lastToLastMonthTeamRoleCount = $lastToLastMonthTeamRoleQuery->count();
 
//     //All previous month data till last month

//     $allPreviousMonthTeamRoleQuery = DB::table('cot_answers')
//         ->leftjoin('users','users.id','cot_answers.userId')
//         ->where('users.status','Active')
//         ->where('cot_answers.status','Active')
//         ->where('cot_answers.'.$date, '<', date('Y-m'."-1"));

//         if(!empty($orgId))
//         {
//             $allPreviousMonthTeamRoleQuery->where('cot_answers.orgId',$orgId);
//         }
//     $allPreviousMonthTeamRoleCount = $allPreviousMonthTeamRoleQuery->count();
    
//     //Count all months till last month

//     $TeamRoleFirstData = DB::table('cot_answers')->select('created_at')->orderBy('created_at','ASC')->first();

//     $date1 = new DateTime($TeamRoleFirstData->created_at);
//     $date2 = $date1->diff(new DateTime(Carbon::today()));
//     $allMonthcount = ($date2->y*12) + $date2->m;

//     $TeamRoleCount = array();
//     $TeamRoleCount['MM'] = 0;
//     $TeamRoleCount['MA'] = 0;
    
//     if (!empty($lastToLastMonthTeamRoleCount)) {

//         $TeamRoleCount['MM'] = number_format(-1*($lastToLastMonthTeamRoleCount - $lastMonthTeamRoleCount)/($lastToLastMonthTeamRoleCount),2);
//     }
   
//     if (!empty($allMonthcount)) {

//         $TeamRoleCountMA = ($allPreviousMonthTeamRoleCount)/($allMonthcount);

//         if (!empty($TeamRoleCountMA)) {
            
//             $TeamRoleCount['MA'] = number_format(-1*($TeamRoleCountMA - $lastMonthTeamRoleCount)/($TeamRoleCountMA),2);
//         }
//     }
//     return $TeamRoleCount;
// }

    //Get MM and Moving average for Personality type Completed and updated
    public function getPersonalityTypeMAMMCount($personalityTypeArr = array())
    {
        $orgId = $personalityTypeArr["orgId"];
        $date = $personalityTypeArr["date"];

        $lastMonthStaffCount = $this->lastMonthStaffCount($orgId);

        //Last month query
        $lastMonthPersonalityTypeQuery = DB::table('cot_functional_lens_answers')
            ->leftjoin('users','users.id','cot_functional_lens_answers.userId')
            ->where('users.status','Active')
            ->where('cot_functional_lens_answers.status','Active')
            ->where('cot_functional_lens_answers.'.$date,'LIKE',date("Y-m", strtotime("-1 month"))."%");
            if(!empty($orgId))
            {
            $lastMonthPersonalityTypeQuery->where('cot_functional_lens_answers.orgId',$orgId);
            }
        $lastMonthPersonalityTypeCount = $lastMonthPersonalityTypeQuery->count();

        //Last Month value
        $lastMonthPersonalityType = 0;
        if (!empty($lastMonthPersonalityTypeCount) && !empty($lastMonthStaffCount)) 
        {
          $lastMonthPersonalityType = (($lastMonthPersonalityTypeCount)/($lastMonthStaffCount))*100;
        }

        $thisMonthStaffCount = $this->thisMonthStaffCount($orgId);

        //This month query
        $thisMonthPersonalityTypeQuery = DB::table('cot_functional_lens_answers')
            ->leftjoin('users','users.id','cot_functional_lens_answers.userId')
            ->where('users.status','Active')
            ->where('cot_functional_lens_answers.status','Active')
            ->where('cot_functional_lens_answers.'.$date,'LIKE',date("Y-m")."%");
            if(!empty($orgId))
            {
            $thisMonthPersonalityTypeQuery->where('cot_functional_lens_answers.orgId',$orgId);
            }
        $thisMonthPersonalityTypeCount = $thisMonthPersonalityTypeQuery->count();

        //This Month value
        $thisMonthPersonalityType = 0;
        if (!empty($thisMonthPersonalityTypeCount) && !empty($thisMonthStaffCount)) 
        {
          $thisMonthPersonalityType = (($thisMonthPersonalityTypeCount)/($thisMonthStaffCount))*100;
        }

        $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
        $createdDateStaff = $staff->created_at;
    
        //All previous month data till last month
        $allPreviousMonthPersonalityTypeQuery = DB::table('cot_functional_lens_answers')
            ->leftjoin('users','users.id','cot_functional_lens_answers.userId')
            ->where('users.status','Active')
            ->where('cot_functional_lens_answers.status','Active')
            ->where('cot_functional_lens_answers.'.$date,'>=',$createdDateStaff)
            ->whereDate('cot_functional_lens_answers.'.$date,'<',date('Y-m'."-1"));
            //->where('cot_functional_lens_answers.'.$date, '<', date('Y-m'."-1"));
            if(!empty($orgId))
            {
                $allPreviousMonthPersonalityTypeQuery->where('cot_functional_lens_answers.orgId',$orgId);
            }
        $allPreviousMonthPersonalityTypeCount = $allPreviousMonthPersonalityTypeQuery->count();

        //all previous Month value
        $allPreviousMonthPersonalityType = 0;
        if (!empty($allPreviousMonthPersonalityTypeCount) && !empty($lastMonthStaffCount)) 
        {
          $allPreviousMonthPersonalityType = (($allPreviousMonthPersonalityTypeCount)/($lastMonthStaffCount))*100;
        }
       
        //Count all months till last month
        // $PersonalityTypeFirstData = DB::table('cot_functional_lens_answers')->select('created_at')->orderBy('created_at','ASC')->first();

        // $date1 = new DateTime($PersonalityTypeFirstData->created_at);
        // $date2 = $date1->diff(new DateTime(Carbon::now()->subMonth()));
        // $allMonthcount = ($date2->y*12) + $date2->m + 2;

        $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));

        $PersonalityTypeCount = array();
        $PersonalityTypeCount['MM'] = 0;
        $PersonalityTypeCount['MA'] = 0;
        $PersonalityTypeCountMA = 0;

        if (!empty($lastMonthPersonalityType)) {

            $PersonalityTypeCount['MM'] = number_format(((-1*($lastMonthPersonalityType - $thisMonthPersonalityType))/$lastMonthPersonalityType),2);
        }
        // if (!empty($allPreviousMonthPersonalityTypeCount) && !empty($allMonthcount)) 
        // {
        //     $PersonalityTypeCountMA = ($allPreviousMonthPersonalityTypeCount)/($allMonthcount);
        //     if (!empty($TeamRoleCountMA)) 
        //     {
        //         $PersonalityTypeCount['MA'] = number_format(((-1*($PersonalityTypeCountMA - $thisMonthPersonalityTypeCount))/$PersonalityTypeCountMA)*100,2);
        //     }
        // }
        if (!empty($allPreviousMonthPersonalityType) && !empty($noOfPreviousMonths)) 
        {
            $PersonalityTypeCountMA = ($allPreviousMonthPersonalityType)/($noOfPreviousMonths);
            if (!empty($TeamRoleCountMA)) 
            {
                $PersonalityTypeCount['MA'] = number_format(((-1*($PersonalityTypeCountMA - $thisMonthPersonalityType))/$PersonalityTypeCountMA),2);
            }
        }
        return $PersonalityTypeCount;
    }
// public function getPersonalityTypeMAMMCount($personalityTypeArr = array())
// {
//     $orgId = $personalityTypeArr["orgId"];

//     $date = $personalityTypeArr["date"];

//     //Last month query

//     $lastMonthPersonalityTypeQuery = DB::table('cot_functional_lens_answers')
//         ->leftjoin('users','users.id','cot_functional_lens_answers.userId')
//         ->where('users.status','Active')
//         ->where('cot_functional_lens_answers.status','Active')
//         ->where('cot_functional_lens_answers.'.$date,'LIKE',date("Y-m", strtotime("-1 month"))."%");

//         if(!empty($orgId))
//         {
//         $lastMonthPersonalityTypeQuery->where('cot_functional_lens_answers.orgId',$orgId);
//         }
//     $lastMonthPersonalityTypeCount = $lastMonthPersonalityTypeQuery->count();

//     //Last to last month query

//     $lastToLastMonthPersonalityTypeQuery = DB::table('cot_functional_lens_answers')
//         ->leftjoin('users','users.id','cot_functional_lens_answers.userId')
//         ->where('users.status','Active')
//         ->where('cot_functional_lens_answers.status','Active')
//         ->where('cot_functional_lens_answers.'.$date,'LIKE',date("Y-m", strtotime("-2 month"))."%");

//         if(!empty($orgId))
//         {
//             $lastToLastMonthPersonalityTypeQuery->where('cot_functional_lens_answers.orgId',$orgId);
//         }
//     $lastToLastMonthPersonalityTypeCount = $lastToLastMonthPersonalityTypeQuery->count();

//     //All previous month data till last month

//     $allPreviousMonthPersonalityTypeQuery = DB::table('cot_functional_lens_answers')
//         ->leftjoin('users','users.id','cot_functional_lens_answers.userId')
//         ->where('users.status','Active')
//         ->where('cot_functional_lens_answers.status','Active')
//         ->where('cot_functional_lens_answers.'.$date, '<', date('Y-m'."-1"));

//         if(!empty($orgId))
//         {
//             $allPreviousMonthPersonalityTypeQuery->where('cot_functional_lens_answers.orgId',$orgId);
//         }
//     $allPreviousMonthPersonalityTypeCount = $allPreviousMonthPersonalityTypeQuery->count();
    
//     //Count all months till last month

//     $PersonalityTypeFirstData = DB::table('cot_functional_lens_answers')->select('created_at')->orderBy('created_at','ASC')->first();

//     $date1 = new DateTime($PersonalityTypeFirstData->created_at);
//     $date2 = $date1->diff(new DateTime(Carbon::today()));
//     $allMonthcount = ($date2->y*12) + $date2->m;

//     $PersonalityTypeCount = array();
//     $PersonalityTypeCount['MM'] = 0;
//     $PersonalityTypeCount['MA'] = 0;
    
//     if (!empty($lastToLastMonthPersonalityTypeCount)) {

//         $PersonalityTypeCount['MM'] = number_format(-1*($lastToLastMonthPersonalityTypeCount - $lastMonthPersonalityTypeCount)/($lastToLastMonthPersonalityTypeCount),2);
//     }
   
//     if (!empty($allMonthcount)) {

//         $PersonalityTypeCountMA = ($allPreviousMonthPersonalityTypeCount)/($allMonthcount);

//         if (!empty($TeamRoleCountMA)) {
            
//             $PersonalityTypeCount['MA'] = number_format(-1*($PersonalityTypeCountMA - $lastMonthPersonalityTypeCount)/($PersonalityTypeCountMA),2);
//         }
//     }
//     return $PersonalityTypeCount;
// }

    //Get MM and Moving average for Culture Structure Completed and updated
    public function getCultureStructureMAMMCount($cultureStructureArr = array())
    {
        $orgId = $cultureStructureArr["orgId"];
        $date = $cultureStructureArr["date"];

        $lastMonthStaffCount = $this->lastMonthStaffCount($orgId);

        //Last month query
        $lastMonthCultureStructureQuery = DB::table('sot_answers')
            ->leftjoin('users','users.id','sot_answers.userId')
            ->where('users.status','Active')
            ->where('sot_answers.status','Active')
            ->where('sot_answers.'.$date,'LIKE',date("Y-m", strtotime("-1 month"))."%");
            if(!empty($orgId))
            {
            $lastMonthCultureStructureQuery->where('users.orgId',$orgId);
            }
        $lastMonthCultureStructureCount = $lastMonthCultureStructureQuery->count();

        //Last Month value
        $lastMonthCultureStructure = 0;
        if (!empty($lastMonthCultureStructureCount) && !empty($lastMonthStaffCount)) 
        {
          $lastMonthCultureStructure = (($lastMonthCultureStructureCount)/($lastMonthStaffCount))*100;
        }

        $thisMonthStaffCount = $this->thisMonthStaffCount($orgId);

        //This month query
        $thisMonthCultureStructureQuery = DB::table('sot_answers')
            ->leftjoin('users','users.id','sot_answers.userId')
            ->where('users.status','Active')
            ->where('sot_answers.status','Active')
            ->where('sot_answers.'.$date,'LIKE',date("Y-m")."%");
            if(!empty($orgId))
            {
            $thisMonthCultureStructureQuery->where('users.orgId',$orgId);
            }
        $thisMonthCultureStructureCount = $thisMonthCultureStructureQuery->count();

        //This Month value
        $thisMonthCultureStructure = 0;
        if (!empty($thisMonthCultureStructureCount) && !empty($thisMonthStaffCount)) 
        {
          $thisMonthCultureStructure = (($thisMonthCultureStructureCount)/($thisMonthStaffCount))*100;
        }

        $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
        $createdDateStaff = $staff->created_at;

        //All previous month data till last month
        $allPreviousMonthCultureStructureQuery = DB::table('sot_answers')
            ->leftjoin('users','users.id','sot_answers.userId')
            ->where('users.status','Active')
            ->where('sot_answers.status','Active')
            ->where('sot_answers.'.$date,'>=',$createdDateStaff)
            ->whereDate('sot_answers.'.$date,'<',date('Y-m'."-1"));
            //->where('sot_answers.'.$date, '<', date('Y-m'."-1"));
            if(!empty($orgId))
            {
                $allPreviousMonthCultureStructureQuery->where('users.orgId',$orgId);
            }
        $allPreviousMonthCultureStructureCount = $allPreviousMonthCultureStructureQuery->count();

        //all previous Month value
        $allPreviousMonthCultureStructure = 0;
        if (!empty($allPreviousMonthCultureStructureCount) && !empty($lastMonthStaffCount)) 
        {
          $allPreviousMonthCultureStructure = (($allPreviousMonthCultureStructureCount)/($lastMonthStaffCount))*100;
        }
      
        //Count all months till last month
        // $cultureStructureFirstData = DB::table('sot_answers')->select('created_at')->orderBy('created_at','ASC')->first();

        // $date1 = new DateTime($cultureStructureFirstData->created_at);
        // $date2 = $date1->diff(new DateTime(Carbon::now()->subMonth()));
        // $allMonthcount = ($date2->y*12) + $date2->m + 2;

        $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));
       
        $cultureStructureCount = array();
        $cultureStructureCount['MM'] = 0;
        $cultureStructureCount['MA'] = 0;
        $cultureStructureCountMA = 0;
        
        if (!empty($lastMonthCultureStructure)) 
        {
            $cultureStructureCount['MM'] = number_format(((-1*($lastMonthCultureStructure - $thisMonthCultureStructure))/$lastMonthCultureStructure),2);
        }
        // if (!empty($allPreviousMonthCultureStructureCount) && !empty($allMonthcount)) 
        // {
        //     $cultureStructureCountMA = ($allPreviousMonthCultureStructureCount)/($allMonthcount);
        //     if (!empty($cultureStructureCountMA)) 
        //     {
        //         $cultureStructureCount['MA'] = number_format(((-1*($cultureStructureCountMA - $thisMonthCultureStructureCount))/$cultureStructureCountMA)*100,2);
        //     }
        // }
        if (!empty($allPreviousMonthCultureStructure) && !empty($noOfPreviousMonths)) 
        {
            $cultureStructureCountMA = ($allPreviousMonthCultureStructure)/($noOfPreviousMonths);
            if (!empty($cultureStructureCountMA)) 
            {
                $cultureStructureCount['MA'] = number_format(((-1*($cultureStructureCountMA - $thisMonthCultureStructure))/$cultureStructureCountMA),2);
            }
        }
        return $cultureStructureCount;
    }
// public function getCultureStructureMAMMCount($cultureStructureArr = array())
// {
//     $orgId = $cultureStructureArr["orgId"];

//     $date = $cultureStructureArr["date"];

//     //Last month query

//     $lastMonthCultureStructureQuery = DB::table('sot_answers')
//         ->leftjoin('users','users.id','sot_answers.userId')
//         ->where('users.status','Active')
//         ->where('sot_answers.status','Active')
//         ->where('sot_answers.'.$date,'LIKE',date("Y-m", strtotime("-1 month"))."%");

//         if(!empty($orgId))
//         {
//         $lastMonthCultureStructureQuery->where('users.orgId',$orgId);
//         }
//     $lastMonthCultureStructureCount = $lastMonthCultureStructureQuery->count();

//     //Last to last month query

//     $lastToLastMonthCultureStructureQuery = DB::table('sot_answers')
//         ->leftjoin('users','users.id','sot_answers.userId')
//         ->where('users.status','Active')
//         ->where('sot_answers.status','Active')
//         ->where('sot_answers.'.$date,'LIKE',date("Y-m", strtotime("-2 month"))."%");

//         if(!empty($orgId))
//         {
//             $lastToLastMonthCultureStructureQuery->where('users.orgId',$orgId);
//         }
//     $lastToLastMonthCultureStructureCount = $lastToLastMonthCultureStructureQuery->count();
 
//     //All previous month data till last month

//     $allPreviousMonthCultureStructureQuery = DB::table('sot_answers')
//         ->leftjoin('users','users.id','sot_answers.userId')
//         ->where('users.status','Active')
//         ->where('sot_answers.status','Active')
//         ->where('sot_answers.'.$date, '<', date('Y-m'."-1"));
//         if(!empty($orgId))
//         {
//             $allPreviousMonthCultureStructureQuery->where('users.orgId',$orgId);
//         }
//     $allPreviousMonthCultureStructureCount = $allPreviousMonthCultureStructureQuery->count();
    
//     //Count all months till last month

//     $cultureStructureFirstData = DB::table('sot_answers')->select('created_at')->orderBy('created_at','ASC')->first();

//     $date1 = new DateTime($cultureStructureFirstData->created_at);
//     $date2 = $date1->diff(new DateTime(Carbon::today()));
//     $allMonthcount = ($date2->y*12) + $date2->m;

//     $cultureStructureCount = array();
//     $cultureStructureCount['MM'] = 0;
//     $cultureStructureCount['MA'] = 0;
    
//     if (!empty($lastToLastMonthCultureStructureCount)) {

//         $cultureStructureCount['MM'] = number_format(-1*($lastToLastMonthCultureStructureCount - $lastMonthCultureStructureCount)/($lastToLastMonthCultureStructureCount),2);
//     }
//     if (!empty($allMonthcount)) {

//         $cultureStructureCountMA = ($allPreviousMonthCultureStructureCount)/($allMonthcount);
//         if (!empty($cultureStructureCountMA)) {
            
//             $cultureStructureCount['MA'] = number_format(-1*($cultureStructureCountMA - $lastMonthCultureStructureCount)/($cultureStructureCountMA),2);
//         }
//     }
//     return $cultureStructureCount;
// }

    //Get MM and Moving average for Motivation Completed and updated
    public function getMotivationMAMMCount($motivationArr = array())
    {
        $orgId = $motivationArr["orgId"];
        $date = $motivationArr["date"];

        $lastMonthStaffCount = $this->lastMonthStaffCount($orgId);

        //Last month query
        $lastMonthMotivationQuery = DB::table('sot_motivation_answers')
            ->leftjoin('users','users.id','sot_motivation_answers.userId')
            ->where('users.status','Active')
            ->where('sot_motivation_answers.status','Active')
            ->where('sot_motivation_answers.'.$date,'LIKE',date("Y-m", strtotime("-1 month"))."%");
            if(!empty($orgId))
            {
            $lastMonthMotivationQuery->where('sot_motivation_answers.orgId',$orgId);
            }
        $lastMonthMotivationCount = $lastMonthMotivationQuery->count();

        //Last Month value
        $lastMonthMotivation = 0;
        if (!empty($lastMonthMotivationCount) && !empty($lastMonthStaffCount)) 
        {
          $lastMonthMotivation = (($lastMonthMotivationCount)/($lastMonthStaffCount))*100;
        }

        $thisMonthStaffCount = $this->thisMonthStaffCount($orgId);

        //This month query
        $thisMonthMotivationQuery = DB::table('sot_motivation_answers')
            ->leftjoin('users','users.id','sot_motivation_answers.userId')
            ->where('users.status','Active')
            ->where('sot_motivation_answers.status','Active')
            ->where('sot_motivation_answers.'.$date,'LIKE',date("Y-m")."%");
            if(!empty($orgId))
            {
            $thisMonthMotivationQuery->where('sot_motivation_answers.orgId',$orgId);
            }
        $thisMonthMotivationCount = $thisMonthMotivationQuery->count();

        //This Month value
        $thisMonthMotivation = 0;
        if (!empty($thisMonthMotivationCount) && !empty($thisMonthStaffCount)) 
        {
          $thisMonthMotivation = (($thisMonthMotivationCount)/($thisMonthStaffCount))*100;
        }

        $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
        $createdDateStaff = $staff->created_at;

        //All previous month data till last month
        $allPreviousMonthMotivationQuery = DB::table('sot_motivation_answers')
            ->leftjoin('users','users.id','sot_motivation_answers.userId')
            ->where('users.status','Active')
            ->where('sot_motivation_answers.status','Active')
            ->where('sot_motivation_answers.'.$date,'>=',$createdDateStaff)
            ->whereDate('sot_motivation_answers.'.$date,'<',date('Y-m'."-1"));
            //->where('sot_motivation_answers.'.$date, '<', date('Y-m'."-1"));
            if(!empty($orgId))
            {
                $allPreviousMonthMotivationQuery->where('sot_motivation_answers.orgId',$orgId);
            }
        $allPreviousMonthMotivationCount = $allPreviousMonthMotivationQuery->count();

        //all previous Month value
        $allPreviousMonthMotivation = 0;
        if (!empty($allPreviousMonthMotivationCount) && !empty($lastMonthStaffCount)) 
        {
          $allPreviousMonthMotivation = (($allPreviousMonthMotivationCount)/($lastMonthStaffCount))*100;
        }
     
        //Count all months till last month
        // $motivationFirstData = DB::table('sot_motivation_answers')->select('created_at')->orderBy('created_at','ASC')->first();

        // $date1 = new DateTime($motivationFirstData->created_at);
        // $date2 = $date1->diff(new DateTime(Carbon::now()->subMonth()));
        // $allMonthcount = ($date2->y*12) + $date2->m + 2;

        $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));
      
        $motivationCount = array();
        $motivationCount['MM'] = 0;
        $motivationCount['MA'] = 0;
        $motivationCountMA = 0;
        
        if (!empty($lastMonthMotivation)) {

            $motivationCount['MM'] = number_format(((-1*($lastMonthMotivation - $thisMonthMotivation))/$lastMonthMotivation),2);
        }
        // if (!empty($allPreviousMonthMotivationCount) && !empty($allMonthcount)) 
        // {
        //     $motivationCountMA = ($allPreviousMonthMotivationCount)/($allMonthcount);
        //     if (!empty($motivationCountMA)) 
        //     {
        //         $motivationCount['MA'] = number_format(((-1*($motivationCountMA - $thisMonthMotivationCount))/$motivationCountMA)*100,2);
        //     }
        // }
        if (!empty($allPreviousMonthMotivation) && !empty($noOfPreviousMonths)) 
        {
            $motivationCountMA = ($allPreviousMonthMotivation)/($noOfPreviousMonths);
            if (!empty($motivationCountMA)) 
            {
                $motivationCount['MA'] = number_format(((-1*($motivationCountMA - $thisMonthMotivation))/$motivationCountMA),2);
            }
        }
        return $motivationCount;
    }

// public function getMotivationMAMMCount($motivationArr = array())
// {
//     $orgId = $motivationArr["orgId"];

//     $date = $motivationArr["date"];

//     //Last month query

//     $lastMonthMotivationQuery = DB::table('sot_motivation_answers')
//         ->leftjoin('users','users.id','sot_motivation_answers.userId')
//         ->where('users.status','Active')
//         ->where('sot_motivation_answers.status','Active')
//         ->where('sot_motivation_answers.'.$date,'LIKE',date("Y-m", strtotime("-1 month"))."%");

//         if(!empty($orgId))
//         {
//         $lastMonthMotivationQuery->where('sot_motivation_answers.orgId',$orgId);
//         }
//     $lastMonthMotivationCount = $lastMonthMotivationQuery->count();

//     //Last to last month query

//     $lastToLastMonthMotivationQuery = DB::table('sot_motivation_answers')
//         ->leftjoin('users','users.id','sot_motivation_answers.userId')
//         ->where('users.status','Active')
//         ->where('sot_motivation_answers.status','Active')
//         ->where('sot_motivation_answers.'.$date,'LIKE',date("Y-m", strtotime("-2 month"))."%");

//         if(!empty($orgId))
//         {
//             $lastToLastMonthMotivationQuery->where('sot_motivation_answers.orgId',$orgId);
//         }
//     $lastToLastMonthMotivationCount = $lastToLastMonthMotivationQuery->count();

//     //All previous month data till last month

//     $allPreviousMonthMotivationQuery = DB::table('sot_motivation_answers')
//         ->leftjoin('users','users.id','sot_motivation_answers.userId')
//         ->where('users.status','Active')
//         ->where('sot_motivation_answers.status','Active')
//         ->where('sot_motivation_answers.'.$date, '<', date('Y-m'."-1"));

//         if(!empty($orgId))
//         {
//             $allPreviousMonthMotivationQuery->where('sot_motivation_answers.orgId',$orgId);
//         }
//     $allPreviousMonthMotivationCount = $allPreviousMonthMotivationQuery->count();

//     //Count all months till last month

//     $motivationFirstData = DB::table('sot_motivation_answers')->select('created_at')->orderBy('created_at','ASC')->first();

//     $date1 = new DateTime($motivationFirstData->created_at);
//     $date2 = $date1->diff(new DateTime(Carbon::today()));
//     $allMonthcount = ($date2->y*12) + $date2->m;
    
//     $motivationCount = array();
//     $motivationCount['MM'] = 0;
//     $motivationCount['MA'] = 0;
    
//     if (!empty($lastToLastMonthMotivationCount)) {

//         $motivationCount['MM'] = number_format(-1*($lastToLastMonthMotivationCount - $lastMonthMotivationCount)/($lastToLastMonthMotivationCount),2);
//     }
    
//     if (!empty($allMonthcount)) {

//         $motivationCountMA = ($allPreviousMonthMotivationCount)/($allMonthcount);
//         if (!empty($motivationCountMA)) {
            
//             $motivationCount['MA'] = number_format(-1*($motivationCountMA - $lastMonthMotivationCount)/($motivationCountMA),2);
//         }
//     }
//     return $motivationCount;
// }
    //Get MM and Moving average for Diagnostic updated
public function getDiagnosticMAMMCount($diagnosticArr = array())
{
    $orgId = $diagnosticArr["orgId"];
    $date = $diagnosticArr["date"];

    $lastMonthStaffCount = $this->lastMonthStaffCount($orgId);

    //Last month query
    $lastMonthDiagnosticQuery = DB::table('diagnostic_answers')
        ->leftjoin('users','users.id','diagnostic_answers.userId')
        ->where('users.status','Active')
        ->where('diagnostic_answers.status','Active')
        ->where('diagnostic_answers.'.$date,'LIKE',date("Y-m", strtotime("-1 month"))."%");
        if(!empty($orgId))
        {
        $lastMonthDiagnosticQuery->where('diagnostic_answers.orgId',$orgId);
        }
    $lastMonthDiagnosticCount = $lastMonthDiagnosticQuery->count();

    //Last Month value
    $lastMonthDiagnostic = 0;
    if (!empty($lastMonthDiagnosticCount) && !empty($lastMonthStaffCount)) 
    {
      $lastMonthDiagnostic = (($lastMonthDiagnosticCount)/($lastMonthStaffCount))*100;
    }

    $thisMonthStaffCount = $this->thisMonthStaffCount($orgId);

    //This month query
    $thisMonthDiagnosticQuery = DB::table('diagnostic_answers')
        ->leftjoin('users','users.id','diagnostic_answers.userId')
        ->where('users.status','Active')
        ->where('diagnostic_answers.status','Active')
        ->where('diagnostic_answers.'.$date,'LIKE',date("Y-m")."%");
        if(!empty($orgId))
        {
        $thisMonthDiagnosticQuery->where('diagnostic_answers.orgId',$orgId);
        }
    $thisMonthDiagnosticCount = $thisMonthDiagnosticQuery->count();

    //This Month value
    $thisMonthDiagnostic = 0;
    if (!empty($thisMonthDiagnosticCount) && !empty($thisMonthStaffCount)) 
    {
      $thisMonthDiagnostic = (($thisMonthDiagnosticCount)/($thisMonthStaffCount))*100;
    }

    $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    $createdDateStaff = $staff->created_at;

    //All previous month data till last month
    $allPreviousMonthDiagnosticQuery = DB::table('diagnostic_answers')
        ->leftjoin('users','users.id','diagnostic_answers.userId')
        ->where('users.status','Active')
        ->where('diagnostic_answers.status','Active')
        ->where('diagnostic_answers.'.$date,'>=',$createdDateStaff)
        ->whereDate('diagnostic_answers.'.$date,'<',date('Y-m'."-1"));
        //->where('diagnostic_answers.'.$date, '<', date('Y-m'."-1"));
        if(!empty($orgId))
        {
            $allPreviousMonthDiagnosticQuery->where('diagnostic_answers.orgId',$orgId);
        }
    $allPreviousMonthDiagnosticCount = $allPreviousMonthDiagnosticQuery->count();

    //all previous Month value
    $allPreviousMonthDiagnostic = 0;
    if (!empty($allPreviousMonthDiagnosticCount) && !empty($lastMonthStaffCount)) 
    {
      $allPreviousMonthDiagnostic = (($allPreviousMonthDiagnosticCount)/($lastMonthStaffCount))*100;
    }
 
    //Count all months till last month
    // $diagnosticFirstData = DB::table('diagnostic_answers')->select('created_at')->orderBy('created_at','ASC')->first();

    // $date1 = new DateTime($diagnosticFirstData->created_at);
    // $date2 = $date1->diff(new DateTime(Carbon::now()->subMonth()));
    // $allMonthcount = ($date2->y*12) + $date2->m + 2;

    $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));
  
    $diagnosticCount = array();
    $diagnosticCount['MM'] = 0;
    $diagnosticCount['MA'] = 0;
    $diagnosticCountMA = 0;
    
    if (!empty($lastMonthDiagnostic)) {

        $diagnosticCount['MM'] = number_format(((-1*($lastMonthDiagnostic - $thisMonthDiagnostic))/$lastMonthDiagnostic),2);
    }
    // if (!empty($allPreviousMonthDiagnosticCount) && !empty($allMonthcount)) 
    // {
    //     $diagnosticCountMA = ($allPreviousMonthDiagnosticCount)/($allMonthcount);
    //     if (!empty($diagnosticCountMA)) 
    //     {
    //         $diagnosticCount['MA'] = number_format(((-1*($diagnosticCountMA - $thisMonthDiagnosticCount))/$diagnosticCountMA)*100,2);
    //     }
    // }
    if (!empty($allPreviousMonthDiagnostic) && !empty($noOfPreviousMonths)) 
    {
        $diagnosticCountMA = ($allPreviousMonthDiagnostic)/($noOfPreviousMonths);
        if (!empty($diagnosticCountMA)) 
        {
            $diagnosticCount['MA'] = number_format(((-1*($diagnosticCountMA - $thisMonthDiagnostic))/$diagnosticCountMA),2);
        }
    }
    return $diagnosticCount;
}

//Get MM and Moving average for Tribeometer updated
public function getTribeometerMAMMCount($tribeometerArr = array())
{
    $orgId = $tribeometerArr["orgId"];
    $date = $tribeometerArr["date"];

    $lastMonthStaffCount = $this->lastMonthStaffCount($orgId);

    //Last month query
    $lastMonthTribeometerQuery = DB::table('tribeometer_answers')
        ->leftjoin('users','users.id','tribeometer_answers.userId')
        ->where('users.status','Active')
        ->where('tribeometer_answers.status','Active')
        ->where('tribeometer_answers.'.$date,'LIKE',date("Y-m", strtotime("-1 month"))."%");
        if(!empty($orgId))
        {
        $lastMonthTribeometerQuery->where('tribeometer_answers.orgId',$orgId);
        }
    $lastMonthTribeometerCount = $lastMonthTribeometerQuery->count();

    //Last Month value
    $lastMonthTribeometer = 0;
    if (!empty($lastMonthTribeometerCount) && !empty($lastMonthStaffCount)) 
    {
      $lastMonthTribeometer = (($lastMonthTribeometerCount)/($lastMonthStaffCount))*100;
    }

    $thisMonthStaffCount = $this->thisMonthStaffCount($orgId);

    //This month query
    $thisMonthTribeometerQuery = DB::table('tribeometer_answers')
        ->leftjoin('users','users.id','tribeometer_answers.userId')
        ->where('users.status','Active')
        ->where('tribeometer_answers.status','Active')
        ->where('tribeometer_answers.'.$date,'LIKE',date("Y-m")."%");
        if(!empty($orgId))
        {
        $thisMonthTribeometerQuery->where('tribeometer_answers.orgId',$orgId);
        }
    $thisMonthTribeometerCount = $thisMonthTribeometerQuery->count();

    //This Month value
    $thisMonthTribeometer = 0;
    if (!empty($thisMonthTribeometerCount) && !empty($thisMonthStaffCount)) 
    {
      $thisMonthTribeometer = (($thisMonthTribeometerCount)/($thisMonthStaffCount))*100;
    }

    $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    $createdDateStaff = $staff->created_at;

    //All previous month data till last month
    $allPreviousMonthTribeometerQuery = DB::table('tribeometer_answers')
        ->leftjoin('users','users.id','tribeometer_answers.userId')
        ->where('users.status','Active')
        ->where('tribeometer_answers.status','Active')
        ->where('tribeometer_answers.'.$date,'>=',$createdDateStaff)
        ->whereDate('tribeometer_answers.'.$date,'<',date('Y-m'."-1"));
        //->where('tribeometer_answers.'.$date, '<', date('Y-m'."-1"));
        if(!empty($orgId))
        {
            $allPreviousMonthTribeometerQuery->where('tribeometer_answers.orgId',$orgId);
        }
    $allPreviousMonthTribeometerCount = $allPreviousMonthTribeometerQuery->count();

    //all previous Month value
    $allPreviousMonthTribeometer = 0;
    if (!empty($allPreviousMonthTribeometerCount) && !empty($lastMonthStaffCount)) 
    {
      $allPreviousMonthTribeometer = (($allPreviousMonthTribeometerCount)/($lastMonthStaffCount))*100;
    }
 
    //Count all months till last month
    // $tribeometerFirstData = DB::table('tribeometer_answers')->select('created_at')->orderBy('created_at','ASC')->first();

    // $date1 = new DateTime($tribeometerFirstData->created_at);
    // $date2 = $date1->diff(new DateTime(Carbon::now()->subMonth()));
    // $allMonthcount = ($date2->y*12) + $date2->m + 2;

    $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));
  
    $tribeometerCount = array();
    $tribeometerCount['MM'] = 0;
    $tribeometerCount['MA'] = 0;
    $tribeometerCountMA = 0;
    
    if (!empty($lastMonthTribeometer)) {

        $tribeometerCount['MM'] = number_format(((-1*($lastMonthTribeometer - $thisMonthTribeometer))/$lastMonthTribeometer),2);
    }
    // if (!empty($allPreviousMonthTribeometerCount) && !empty($allMonthcount)) 
    // {
    //     $tribeometerCountMA = ($allPreviousMonthTribeometerCount)/($allMonthcount);
    //     if (!empty($tribeometerCountMA)) 
    //     {
    //         $tribeometerCount['MA'] = number_format(((-1*($tribeometerCountMA - $thisMonthTribeometerCount))/$tribeometerCountMA)*100,2);
    //     }
    // }
    if (!empty($allPreviousMonthTribeometer) && !empty($noOfPreviousMonths)) 
    {
        $tribeometerCountMA = ($allPreviousMonthTribeometer)/($noOfPreviousMonths);
        if (!empty($tribeometerCountMA)) 
        {
            $tribeometerCount['MA'] = number_format(((-1*($tribeometerCountMA - $thisMonthTribeometer))/$tribeometerCountMA),2);
        }
    }
    return $tribeometerCount;
}

//     public function getPerformance()
//     {

//        $resultArray = array();

//        $orgId = Input::get('orgId');
//        $reportType = Input::get('reportType');
//        $date = Input::get('date');

//        $month ='';
//        $year  ='';
//        $currentDate = '';
//        if($reportType == 'daily') 
//        {
//         $currentDate = date('Y-m-d');          
//     }       
//     else if($reportType == 'monthly')
//     {
//        $month = date('m', strtotime($date));
//        $year  = date('Y', strtotime($date));
//    }


//    $organisations = DB::table('organisations')->where('status','Active')->orderBy('organisation','ASC')->get();

//    $usersQuery = DB::table('users')->where('status','Active');

//    if(!empty($orgId))
//    {
//     $usersQuery->where('orgId',$orgId);
// }

// $users = $usersQuery->get();

// $userCount = count($users);



// $query = DB::table('dot_bubble_rating_records')
// ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id');                  

// if(!empty($orgId))
// {
//     $query->where('dots.orgId',$orgId);
// }

// if(!empty($currentDate))
// {
//    $query->whereDate('dot_bubble_rating_records.created_at',$currentDate);
// }

// if(!empty($month) && !empty($year))
// { 
//     $query->whereMonth('dot_bubble_rating_records.created_at', $month);
//     $query->whereYear('dot_bubble_rating_records.created_at', $year);
// }

// $bubbleCount = $query->count('bubble_flag');

// $resultArray['thumbCompleted'] = "0";
// if(!empty($bubbleCount) && !empty($userCount))
// {
//     $resultArray['thumbCompleted'] = number_format(($bubbleCount/$userCount), 2);
// }



// /*DOT value complete*/
// $dotValueRatingCompletedUserArr = array();
// $dotValueRatingUpdatedUserArr   = array();

// foreach($users as $duValue)
// {

//     //check DOT rating is given by user on every dot value if it is yes then true
//     $dotPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);

//     $status = $this->getDotPerformance($dotPerArr);

//     if($status)
//     {
//         array_push($dotValueRatingCompletedUserArr, $duValue->id);      
//     }

//     //for updated
//     $dotPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);

//     $uStatus = $this->getDotPerformance($dotPerArr);

//     if($uStatus)
//     {
//         array_push($dotValueRatingUpdatedUserArr, $duValue->id);        
//     }
// }



//         //for complete
// $dotValueRatingCompletedUserArrCount = count($dotValueRatingCompletedUserArr);

// $resultArray['dotCompleted'] = "0";
// if(!empty($dotValueRatingCompletedUserArrCount) && !empty($userCount))
// {
//     $resultArray['dotCompleted'] = number_format(($dotValueRatingCompletedUserArrCount/$userCount)*100,2);
// }

//         //for update
// $dotValueRatingUpdatedUserArrCount = count($dotValueRatingUpdatedUserArr);

// $resultArray['dotUpdated'] = "0";
// if(!empty($dotValueRatingUpdatedUserArrCount) && !empty($userCount))
// {
//     $resultArray['dotUpdated'] = number_format(($dotValueRatingUpdatedUserArrCount/$userCount)*100,2);
// }



//         //for team role map comple
// $teamPerArr = array('orgId' => $orgId,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
// $teamRoleStatus = $this->getTeamRoleMap($teamPerArr);

// $temRoleMapCount = count($teamRoleStatus);

// $resultArray['tealRoleMapCompleted'] = "0";
// if (!empty($temRoleMapCount) && !empty($userCount))
// {
//     $resultArray['tealRoleMapCompleted'] = number_format(($temRoleMapCount/$userCount)*100,2);
// }

//         //for team role map update
// $teamPerUpdateArr = array('orgId' => $orgId,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
// $teamRoleUpdateStatus = $this->getTeamRoleMap($teamPerUpdateArr);

// $temRoleMapUpdateCount = count($teamRoleUpdateStatus);

// $resultArray['tealRoleMapUpdated'] = "0";
// if (!empty($temRoleMapUpdateCount) && !empty($userCount))
// {
//     $resultArray['tealRoleMapUpdated'] = number_format(($temRoleMapUpdateCount/$userCount)*100,2);
// }



//         //for personality type
// $personalityPerArr = array('orgId' => $orgId,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
// $personalityTypeStatus = $this->getPersonalityType($personalityPerArr);

// $personalityTypeCount = count($personalityTypeStatus);

// $resultArray['personalityTypeCompleted'] = "0";
// if (!empty($personalityTypeCount) && !empty($userCount))
// {
//     $resultArray['personalityTypeCompleted'] = number_format(($personalityTypeCount/$userCount)*100,2);
// }

//         //for personality type updated
// $personalityUpdatePerArr = array('orgId' => $orgId,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
// $personalityTypeUpdateStatus = $this->getPersonalityType($personalityUpdatePerArr);

// $personalityTypeUpdateCount = count($personalityTypeUpdateStatus);

// $resultArray['personalityTypeUpdated'] = "0";
// if (!empty($personalityTypeUpdateCount) && !empty($userCount))
// {
//     $resultArray['personalityTypeUpdated'] = number_format(($personalityTypeUpdateCount/$userCount)*100,2);
// }


//         //for culture structure comleted        
// $cultureStructurePerArr = array('orgId' => $orgId,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
// $cultureStructureStatus = $this->getCultureStructure($cultureStructurePerArr);


// $cultureStructureCount = count($cultureStructureStatus);

// $resultArray['cultureStructureCompleted'] = "0";
// if (!empty($cultureStructureCount) && !empty($userCount))
// {
//     $resultArray['cultureStructureCompleted'] = number_format(($cultureStructureCount/$userCount)*100,2);
// }


//         //for personality type updated
// $cultureStructureUpdatePerArr = array('orgId' => $orgId,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
// $cultureStructureUpdateStatus = $this->getCultureStructure($cultureStructureUpdatePerArr);

// $cultureStructureUpdateCount = count($cultureStructureUpdateStatus);

// $resultArray['cultureStructureUpdated'] = "0";
// if (!empty($cultureStructureUpdateCount) && !empty($userCount))
// {
//     $resultArray['cultureStructureUpdated'] = number_format(($cultureStructureUpdateCount/$userCount)*100,2);
// }



//         //for motivation comleted       
// $motivationPerArr = array('orgId' => $orgId,'updateAt'=>false,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
// $motivationStatus = $this->getMotivation($motivationPerArr);

// $motivationCount = count($motivationStatus);

// $resultArray['motivationCompleted'] = "0";
// if (!empty($motivationCount) && !empty($userCount))
// {
//     $resultArray['motivationCompleted'] = number_format(($motivationCount/$userCount)*100,2);
// }

//         //for motivation type updated
// $motivationCountPerArr = array('orgId' => $orgId,'updateAt'=>true,'currentDate'=>$currentDate,'month'=>$month,'year'=>$year);
// $motivationCountUpdateStatus = $this->getMotivation($motivationCountPerArr);

// $motivationCountUpdateCount = count($motivationCountUpdateStatus);

// $resultArray['motivationCountUpdated'] = "0";
// if (!empty($motivationCountUpdateCount) && !empty($userCount))
// {
//     $resultArray['motivationCountUpdated'] = number_format(($motivationCountUpdateCount/$userCount)*100,2);
// }


// $data_array =  array("app_name"=>'Tribe365', "org_id"=> $orgId);

// $make_call = $this->getImprovements('POST', 'http://tellsid.softintelligence.co.uk/index.php/apitellsid/getIOTdetail', json_encode($data_array));

// $response = json_decode($make_call, true);

// $imprUserArray = array();
// foreach($response['response'] as $value)
// {           
//     if($value['app_name']=='Tribe365')
//     {
//         $email = $value['email_id'];
//         $user = DB::table('users')->where('id',$email)->where('status','Active')->first();

//         $userOrgId ='';
//         $userId ='';
//         if($user)
//         {
//             $userOrgId = $user->orgId;
//             $userId    = $user->id;
//         }

//         if($orgId)
//         {               
//             if($userOrgId==$orgId)
//             {
//                 array_push($imprUserArray, $userId);
//             }
//         }
//         else
//         {
//             array_push($imprUserArray, $userId);
//         }
//     }
// }

// $improvementsCount = count(array_unique($imprUserArray));

// $resultArray['improvements'] = "0";
// if(!empty($improvementsCount) && !empty($userCount))
// {
//     $resultArray['improvements'] = number_format(($improvementsCount/$userCount),2);
// }

// /*return response()->json(['code'=>200,'status'=>true,'service_name'=>'get-performance','message'=>'','data'=>$resultArray]);*/

// return view('admin/performance/performance',compact('resultArray','organisations','orgId','reportType','date'));
// }

    //check user given rating to the each value of dot
    protected function getDotPerformance($perArr = array())
    {

      //print_r(date('Y-m-d',strtotime('last day of last month')));die();
        $orgId    = $perArr["orgId"];
        $userId   = $perArr["userId"];
        $updateAt = $perArr["updateAt"];
        $last12MonthFirstDate = $perArr["last12MonthFirstDate"];
        $thisMonthLastDate = $perArr["thisMonthLastDate"];
        $last6MonthFirstDate = $perArr["last6MonthFirstDate"];
        $lastMonth = $perArr["lastMonth"];
        $thisMonth = $perArr["thisMonth"];
        $startOfSubWeek = $perArr["startOfSubWeek"];
        $endOfSubWeek = $perArr["endOfSubWeek"];
        $startOfWeek = $perArr["startOfWeek"];
        $endOfWeek = $perArr["endOfWeek"];
        $yesterday = $perArr["yesterday"];
        $today = $perArr["today"];
       
        if(!$orgId)
        {
            $user = DB::table('users')->where('status','Active')->where('id',$userId)->first();

            $orgId ='';
            if($user)
            {
                $orgId = $user->orgId;
            }           
        }

        $query = DB::table('dots_values')  
        ->select('dots_values.id AS id')     
        ->leftjoin('dots_beliefs','dots_beliefs.id','dots_values.beliefId')
        ->leftjoin('dots','dots.id','dots_beliefs.dotId')
        ->where('dots_beliefs.status','Active')
        ->where('dots.status','Active')
        ->where('dots_values.status','Active');

        if($orgId) 
        {
            $query->where('dots.orgId', $orgId);
        }

        $dotValues = $query->get();
        foreach($dotValues as $value)
        {
            $dvValue = DB::table('dot_values_ratings')
            ->rightjoin('users','users.id','dot_values_ratings.userId')
            ->where('users.status','Active')
            ->where('dot_values_ratings.userId',$userId)
            ->where('dot_values_ratings.valueId',$value->id)
            ->where('dot_values_ratings.status','Active');

            if($updateAt)
            {
                $dvValue->whereNotNull('dot_values_ratings.updated_at');
                if(!empty($currentDate))
                {
                    $dvValue->whereDate('dot_values_ratings.updated_at',$currentDate);

                    $isDotValueRating = $dvValue->first();

                    if($isDotValueRating)
                    {
                        return true;
                    }
                }
                if(!empty($month) && !empty($year))
                { 
                    $dvValue->whereMonth('dot_values_ratings.updated_at', $month);
                    $dvValue->whereYear('dot_values_ratings.updated_at', $year);

                    $isDotValueRating = $dvValue->first();

                    if($isDotValueRating)
                    {
                        return true;
                    }
                }
            }
            else
            {
                if (!empty($last12MonthFirstDate) && !empty($thisMonthLastDate)) {
                    $dvValue->whereDate('dot_values_ratings.created_at','<=',$thisMonthLastDate);
                    //$dvValue->whereBetween('dot_values_ratings.created_at',array($last12MonthFirstDate,$thisMonthLastDate));
                }
                elseif (!empty($last6MonthFirstDate) && !empty($thisMonthLastDate)) {
                    $dvValue->whereDate('dot_values_ratings.created_at','<=',$thisMonthLastDate);
                    //$dvValue->whereBetween('dot_values_ratings.created_at',array($last6MonthFirstDate,$thisMonthLastDate));
                }
                elseif (!empty($lastMonth)) {
                    $dvValue->whereDate('dot_values_ratings.created_at','<=',$thisMonthLastDate);
                    //$dvValue->where('dot_values_ratings.created_at','LIKE',$lastMonth);
                }
                elseif (!empty($thisMonth)) {
                    $dvValue->whereDate('dot_values_ratings.created_at','<=',date('Y-m-d'));
                    //$dvValue->whereDate('dot_values_ratings.created_at','<=',date('Y-m-d',strtotime('last day of this month')));
                    //$dvValue->where('dot_values_ratings.created_at','LIKE',$thisMonth);
                }
                elseif (!empty($startOfSubWeek) && !empty($endOfSubWeek)) {
                    $dvValue->whereDate('dot_values_ratings.created_at','<=',$endOfSubWeek);
                    //$dvValue->whereBetween('dot_values_ratings.created_at',array($startOfSubWeek,$endOfSubWeek));
                }
                elseif (!empty($startOfWeek) && !empty($endOfWeek)) {
                    $dvValue->whereDate('dot_values_ratings.created_at','<=',date('Y-m-d'));
                    //$dvValue->whereDate('dot_values_ratings.created_at','<=',$endOfWeek);
                    //$dvValue->whereBetween('dot_values_ratings.created_at',array($startOfWeek,$endOfWeek));
                }
                elseif (!empty($yesterday)) {
                    $dvValue->whereDate('dot_values_ratings.created_at','<=',$yesterday);
                }
                elseif (!empty($today)) {
                    $dvValue->whereDate('dot_values_ratings.created_at','<=',$today);
                }
            }
            $isDotValueRating = $dvValue->first();
            // print_r($dvValue->toSql());
            // print_r($dvValue->getBindings());
            if(!$isDotValueRating)
            {
                return false;
            }
        }
        //return true;
        if (count($dotValues) == 0) {
          return false;
        }
        else
        {
          return true;
        }
    }
// protected function getDotPerformance($perArr = array())
// {

//     $orgId    = $perArr["orgId"];
//     $userId   = $perArr["userId"];
//     $updateAt = $perArr["updateAt"];
//     $currentDate = $perArr["currentDate"];
//     $month        = $perArr["month"];
//     $year         = $perArr["year"];

//     if(!$orgId)
//     {
//         $user = DB::table('users')->where('status','Active')->where('id',$userId)->first();

//         $orgId ='';
//         if($user)
//         {
//             $orgId = $user->orgId;
//         }           
//     }

//     $query = DB::table('dots_values')  
//     ->select('dots_values.id AS id')     
//     ->leftjoin('dots_beliefs','dots_beliefs.id','dots_values.beliefId')
//     ->leftjoin('dots','dots.id','dots_beliefs.dotId')
//     ->where('dots_beliefs.status','Active')
//     ->where('dots.status','Active')
//     ->where('dots_values.status','Active');

//     if($orgId) 
//     {
//         $query->where('dots.orgId', $orgId);
//     }

//     $dotValues = $query->get();

//     foreach($dotValues as $value)
//     {
//         $dvValue = DB::table('dot_values_ratings')
//         ->rightjoin('users','users.id','dot_values_ratings.userId')
//         ->where('users.status','Active')
//         ->where('dot_values_ratings.userId',$userId)
//         ->where('dot_values_ratings.valueId',$value->id)
//         ->where('dot_values_ratings.status','Active');

//         if($updateAt)
//         {
//             $dvValue->whereNotNull('dot_values_ratings.updated_at');
//             if(!empty($currentDate))
//             {
//                 $dvValue->whereDate('dot_values_ratings.updated_at',$currentDate);

//                 $isDotValueRating = $dvValue->first();

//                 if($isDotValueRating)
//                 {
//                     return true;
//                 }
//             }
//             if(!empty($month) && !empty($year))
//             { 
//                 $dvValue->whereMonth('dot_values_ratings.updated_at', $month);
//                 $dvValue->whereYear('dot_values_ratings.updated_at', $year);

//                 $isDotValueRating = $dvValue->first();

//                 if($isDotValueRating)
//                 {
//                     return true;
//                 }
//             }
//         }
//         else
//         {

//             if(!empty($month) && !empty($year))
//             { 
//                 $dvValue->whereMonth('dot_values_ratings.created_at', $month);
//                 $dvValue->whereYear('dot_values_ratings.created_at', $year);
//             }

//         }


//         $isDotValueRating = $dvValue->first();

//         if(!$isDotValueRating)
//         {
//             return false;
//         }
//     }

//     return true;
// }

    //get team role performance
    protected function getTeamRoleMap($perArr = array())
    {
        $orgId    = $perArr["orgId"];
        $updateAt = $perArr["updateAt"];
        $last12MonthFirstDate = $perArr["last12MonthFirstDate"];
        $thisMonthLastDate = $perArr["thisMonthLastDate"];
        $last6MonthFirstDate = $perArr["last6MonthFirstDate"];
        $lastMonth = $perArr["lastMonth"];
        $thisMonth = $perArr["thisMonth"];
        $startOfSubWeek = $perArr["startOfSubWeek"];
        $endOfSubWeek = $perArr["endOfSubWeek"];
        $startOfWeek = $perArr["startOfWeek"];
        $endOfWeek = $perArr["endOfWeek"];
        $yesterday = $perArr["yesterday"];
        $today = $perArr["today"];

        $query = DB::table('cot_answers') 
        ->select('cot_answers.userId')
        ->distinct()
        ->rightjoin('users','users.id','cot_answers.userId')
        ->where('users.status','Active')
        ->where('cot_answers.status','Active');

        if($orgId)
        {
            $query->where('cot_answers.orgId',$orgId);
        }
        if($updateAt)
        {
            $query->whereNotNull('cot_answers.updated_at');
            if(!empty($currentDate))
            {
                $query->whereDate('cot_answers.updated_at',$currentDate);
            }
            if(!empty($month) && !empty($year))
            { 
                $query->whereMonth('cot_answers.updated_at', $month);
                $query->whereYear('cot_answers.updated_at', $year);
            }
        }
        else
        {
            // if (!empty($last12MonthFirstDate) && !empty($thisMonthLastDate)) {
            //     $dvValue->whereDate('cot_answers.created_at','<=',$thisMonthLastDate);
            //     // $query->whereBetween('cot_answers.created_at',array($last12MonthFirstDate,$thisMonthLastDate));
            // }
            // elseif (!empty($last6MonthFirstDate) && !empty($thisMonthLastDate)) {
            //   $dvValue->whereDate('dot_values_ratings.created_at','<=',$thisMonthLastDate);
            //     // $query->whereBetween('cot_answers.created_at',array($last6MonthFirstDate,$thisMonthLastDate));
            // }
            // elseif (!empty($lastMonth)) {
            //     $query->where('cot_answers.created_at','LIKE',$lastMonth);
            // }
            // elseif (!empty($thisMonth)) {
            //     $query->where('cot_answers.created_at','LIKE',$thisMonth);
            // }
            // elseif (!empty($startOfSubWeek) && !empty($endOfSubWeek)) {
            //     $query->whereBetween('cot_answers.created_at',array($startOfSubWeek,$endOfSubWeek));
            // }
            // elseif (!empty($startOfWeek) && !empty($endOfWeek)) {
            //     $query->whereBetween('cot_answers.created_at',array($startOfWeek,$endOfWeek));
            // }
            // elseif (!empty($yesterday)) {
            //     $query->whereDate('cot_answers.created_at',$yesterday);
            // }
            // elseif (!empty($today)) {
            //     $query->whereDate('cot_answers.created_at',$today);
            // }

          if (!empty($last12MonthFirstDate) && !empty($thisMonthLastDate)) {
              $query->whereDate('cot_answers.created_at','<=',$thisMonthLastDate);
              //$query->whereBetween('cot_answers.created_at',array($last12MonthFirstDate,$thisMonthLastDate));
          }
          elseif (!empty($last6MonthFirstDate) && !empty($thisMonthLastDate)) {
              $query->whereDate('cot_answers.created_at','<=',$thisMonthLastDate);
              //$query->whereBetween('cot_answers.created_at',array($last6MonthFirstDate,$thisMonthLastDate));
          }
          elseif (!empty($lastMonth)) {
              $query->whereDate('cot_answers.created_at','<=',$thisMonthLastDate);
              //$query->where('cot_answers.created_at','LIKE',$lastMonth);
          }
          elseif (!empty($thisMonth)) {
              $query->whereDate('cot_answers.created_at','<=',date('Y-m-d'));
              //$query->whereDate('cot_answers.created_at','<=',date('Y-m-d',strtotime('last day of this month')));
              //$query->where('cot_answers.created_at','LIKE',$thisMonth);
          }
          elseif (!empty($startOfSubWeek) && !empty($endOfSubWeek)) {
              $query->whereDate('cot_answers.created_at','<=',$endOfSubWeek);
              //$query->whereBetween('cot_answers.created_at',array($startOfSubWeek,$endOfSubWeek));
          }
          elseif (!empty($startOfWeek) && !empty($endOfWeek)) {
              $query->whereDate('cot_answers.created_at','<=',date('Y-m-d'));
              //$query->whereDate('cot_answers.created_at','<=',$endOfWeek);
              //$query->whereBetween('cot_answers.created_at',array($startOfWeek,$endOfWeek));
          }
          elseif (!empty($yesterday)) {
              $query->whereDate('cot_answers.created_at','<=',$yesterday);
          }
          elseif (!empty($today)) {
              $query->whereDate('cot_answers.created_at','<=',$today);
          }
        }
        $result = $query->get();

        return $result;
    }
// protected function getTeamRoleMap($perArr = array())
// {
//     $orgId    = $perArr['orgId'];
//     $updateAt = $perArr["updateAt"];
//     $currentDate = $perArr["currentDate"];
//     $month        = $perArr["month"];
//     $year         = $perArr["year"];


//     $query = DB::table('cot_answers') 
//     ->select('cot_answers.userId')
//     ->distinct()
//     ->rightjoin('users','users.id','cot_answers.userId')
//     ->where('users.status','Active')
//     ->where('cot_answers.status','Active');

//     if($orgId)
//     {
//         $query->where('cot_answers.orgId',$orgId);
//     }

//     if($updateAt)
//     {
//         $query->whereNotNull('cot_answers.updated_at');
//         if(!empty($currentDate))
//         {
//             $query->whereDate('cot_answers.updated_at',$currentDate);
//         }
//         if(!empty($month) && !empty($year))
//         { 
//             $query->whereMonth('cot_answers.updated_at', $month);
//             $query->whereYear('cot_answers.updated_at', $year);
//         }
//     }
//     else
//     {

//         if(!empty($month) && !empty($year))
//         { 
//             $query->whereMonth('cot_answers.created_at', $month);
//             $query->whereYear('cot_answers.created_at', $year);
//         }

//     }

//     $result = $query->get();

//     return $result;
// }

    //for personality type 
    protected function getPersonalityType($perArr = array())
    {
        $orgId    = $perArr["orgId"];
        $updateAt = $perArr["updateAt"];
        $last12MonthFirstDate = $perArr["last12MonthFirstDate"];
        $thisMonthLastDate = $perArr["thisMonthLastDate"];
        $last6MonthFirstDate = $perArr["last6MonthFirstDate"];
        $lastMonth = $perArr["lastMonth"];
        $thisMonth = $perArr["thisMonth"];
        $startOfSubWeek = $perArr["startOfSubWeek"];
        $endOfSubWeek = $perArr["endOfSubWeek"];
        $startOfWeek = $perArr["startOfWeek"];
        $endOfWeek = $perArr["endOfWeek"];
        $yesterday = $perArr["yesterday"];
        $today = $perArr["today"];

        $query = DB::table('cot_functional_lens_answers')
        ->select('cot_functional_lens_answers.userId')
        ->distinct()
        ->rightjoin('users','users.id','cot_functional_lens_answers.userId')
        ->where('users.status','Active')
        ->where('cot_functional_lens_answers.status','Active');

        if($orgId)
        {
            $query->where('cot_functional_lens_answers.orgId',$orgId);
        }
        if($updateAt)
        {
            $query->whereNotNull('cot_functional_lens_answers.updated_at');
            if(!empty($currentDate))
            {
                $query->whereDate('cot_functional_lens_answers.updated_at',$currentDate);
            }
            if(!empty($month) && !empty($year))
            { 
                $query->whereMonth('cot_functional_lens_answers.updated_at', $month);
                $query->whereYear('cot_functional_lens_answers.updated_at', $year);
            }
        }
        else
        {
            // if (!empty($last12MonthFirstDate) && !empty($thisMonthLastDate)) {
            //     $query->whereBetween('cot_functional_lens_answers.created_at',array($last12MonthFirstDate,$thisMonthLastDate));
            // }
            // elseif (!empty($last6MonthFirstDate) && !empty($thisMonthLastDate)) {
            //     $query->whereBetween('cot_functional_lens_answers.created_at',array($last6MonthFirstDate,$thisMonthLastDate));
            // }
            // elseif (!empty($lastMonth)) {
            //     $query->where('cot_functional_lens_answers.created_at','LIKE',$lastMonth);
            // }
            // elseif (!empty($thisMonth)) {
            //     $query->where('cot_functional_lens_answers.created_at','LIKE',$thisMonth);
            // }
            // elseif (!empty($startOfSubWeek) && !empty($endOfSubWeek)) {
            //     $query->whereBetween('cot_functional_lens_answers.created_at',array($startOfSubWeek,$endOfSubWeek));
            // }
            // elseif (!empty($startOfWeek) && !empty($endOfWeek)) {
            //     $query->whereBetween('cot_functional_lens_answers.created_at',array($startOfWeek,$endOfWeek));
            // }
            // elseif (!empty($yesterday)) {
            //     $query->whereDate('cot_functional_lens_answers.created_at',$yesterday);
            // }
            // elseif (!empty($today)) {
            //     $query->whereDate('cot_functional_lens_answers.created_at',$today);
            // }
          if (!empty($last12MonthFirstDate) && !empty($thisMonthLastDate)) {
              $query->whereDate('cot_functional_lens_answers.created_at','<=',$thisMonthLastDate);
              //$query->whereBetween('cot_functional_lens_answers.created_at',array($last12MonthFirstDate,$thisMonthLastDate));
          }
          elseif (!empty($last6MonthFirstDate) && !empty($thisMonthLastDate)) {
              $query->whereDate('cot_functional_lens_answers.created_at','<=',$thisMonthLastDate);
              //$query->whereBetween('cot_functional_lens_answers.created_at',array($last6MonthFirstDate,$thisMonthLastDate));
          }
          elseif (!empty($lastMonth)) {
              $query->whereDate('cot_functional_lens_answers.created_at','<=',$thisMonthLastDate);
              //$query->where('cot_functional_lens_answers.created_at','LIKE',$lastMonth);
          }
          elseif (!empty($thisMonth)) {
              $query->whereDate('cot_functional_lens_answers.created_at','<=',date('Y-m-d'));
              //$query->whereDate('cot_functional_lens_answers.created_at','<=',date('Y-m-d',strtotime('last day of this month')));
              //$query->where('cot_functional_lens_answers.created_at','LIKE',$thisMonth);
          }
          elseif (!empty($startOfSubWeek) && !empty($endOfSubWeek)) {
              $query->whereDate('cot_functional_lens_answers.created_at','<=',$endOfSubWeek);
              //$query->whereBetween('cot_functional_lens_answers.created_at',array($startOfSubWeek,$endOfSubWeek));
          }
          elseif (!empty($startOfWeek) && !empty($endOfWeek)) {
              $query->whereDate('cot_functional_lens_answers.created_at','<=',date('Y-m-d'));
              //$query->whereDate('cot_functional_lens_answers.created_at','<=',$endOfWeek);
              //$query->whereBetween('cot_functional_lens_answers.created_at',array($startOfWeek,$endOfWeek));
          }
          elseif (!empty($yesterday)) {
              $query->whereDate('cot_functional_lens_answers.created_at','<=',$yesterday);
          }
          elseif (!empty($today)) {
              $query->whereDate('cot_functional_lens_answers.created_at','<=',$today);
          }
        }

        $result = $query->get();

        return $result;
    }
// protected function getPersonalityType($perArr = array())
// {

//     $orgId    = $perArr['orgId'];
//     $updateAt = $perArr["updateAt"];
//     $currentDate = $perArr["currentDate"];
//     $month        = $perArr["month"];
//     $year         = $perArr["year"];

//     $query = DB::table('cot_functional_lens_answers')
//     ->select('cot_functional_lens_answers.userId')
//     ->distinct()
//     ->rightjoin('users','users.id','cot_functional_lens_answers.userId')
//     ->where('users.status','Active')
//     ->where('cot_functional_lens_answers.status','Active');


//     if($orgId)
//     {
//         $query->where('cot_functional_lens_answers.orgId',$orgId);
//     }
//     if($updateAt)
//     {
//         $query->whereNotNull('cot_functional_lens_answers.updated_at');
//         if(!empty($currentDate))
//         {
//             $query->whereDate('cot_functional_lens_answers.updated_at',$currentDate);
//         }
//         if(!empty($month) && !empty($year))
//         { 
//             $query->whereMonth('cot_functional_lens_answers.updated_at', $month);
//             $query->whereYear('cot_functional_lens_answers.updated_at', $year);
//         }
//     }
//     else
//     {
//         if(!empty($month) && !empty($year))
//         { 
//             $query->whereMonth('cot_functional_lens_answers.created_at', $month);
//             $query->whereYear('cot_functional_lens_answers.created_at', $year);
//         }

//     }

//     $result = $query->get();

//     return $result;
// }

    //for SOT culture structure 
    protected function getCultureStructure($perArr = array())
    {
        $orgId    = $perArr["orgId"];
        $updateAt = $perArr["updateAt"];
        $last12MonthFirstDate = $perArr["last12MonthFirstDate"];
        $thisMonthLastDate = $perArr["thisMonthLastDate"];
        $last6MonthFirstDate = $perArr["last6MonthFirstDate"];
        $lastMonth = $perArr["lastMonth"];
        $thisMonth = $perArr["thisMonth"];
        $startOfSubWeek = $perArr["startOfSubWeek"];
        $endOfSubWeek = $perArr["endOfSubWeek"];
        $startOfWeek = $perArr["startOfWeek"];
        $endOfWeek = $perArr["endOfWeek"];
        $yesterday = $perArr["yesterday"];
        $today = $perArr["today"];

        $query = DB::table('sot_answers')   
        ->select('sot_answers.userId')
        ->distinct()      
        ->leftJoin('users','users.id','=','sot_answers.userId')
        ->where('sot_answers.status','Active')
        ->where('users.status','Active');     

        if($orgId)
        {
            $query->where('users.orgId',$orgId);
        }

        if($updateAt)
        {
            $query->whereNotNull('sot_answers.updated_at');
            if (!empty($last12MonthFirstDate) && !empty($thisMonthLastDate)) {
                $query->whereBetween('sot_answers.updated_at',array($last12MonthFirstDate,$thisMonthLastDate));
            }
            elseif (!empty($last6MonthFirstDate) && !empty($thisMonthLastDate)) {
                $query->whereBetween('sot_answers.updated_at',array($last6MonthFirstDate,$thisMonthLastDate));
            }
            elseif (!empty($lastMonth)) {
                $query->where('sot_answers.updated_at','LIKE',$lastMonth);
            }
            elseif (!empty($thisMonth)) {
                $query->where('sot_answers.updated_at','LIKE',$thisMonth);
            }
            elseif (!empty($startOfSubWeek) && !empty($endOfSubWeek)) {
                $query->whereBetween('sot_answers.updated_at',array($startOfSubWeek,$endOfSubWeek));
            }
            elseif (!empty($startOfWeek) && !empty($endOfWeek)) {
                $query->whereBetween('sot_answers.updated_at',array($startOfWeek,$endOfWeek));
            }
            elseif (!empty($yesterday)) {
                $query->whereDate('sot_answers.updated_at',$yesterday);
            }
            elseif (!empty($today)) {
                $query->whereDate('sot_answers.updated_at',$today);
            }
        }
        else
        {     
            if (!empty($last12MonthFirstDate) && !empty($thisMonthLastDate)) {
                $query->whereBetween('sot_answers.created_at',array($last12MonthFirstDate,$thisMonthLastDate));
            }
            elseif (!empty($last6MonthFirstDate) && !empty($thisMonthLastDate)) {
                $query->whereBetween('sot_answers.created_at',array($last6MonthFirstDate,$thisMonthLastDate));
            }
            elseif (!empty($lastMonth)) {
                $query->where('sot_answers.created_at','LIKE',$lastMonth);
            }
            elseif (!empty($thisMonth)) {
                $query->where('sot_answers.created_at','LIKE',$thisMonth);
            }
            elseif (!empty($startOfSubWeek) && !empty($endOfSubWeek)) {
                $query->whereBetween('sot_answers.created_at',array($startOfSubWeek,$endOfSubWeek));
            }
            elseif (!empty($startOfWeek) && !empty($endOfWeek)) {
                $query->whereBetween('sot_answers.created_at',array($startOfWeek,$endOfWeek));
            }
            elseif (!empty($yesterday)) {
                $query->whereDate('sot_answers.created_at',$yesterday);
            }
            elseif (!empty($today)) {
                $query->whereDate('sot_answers.created_at',$today);
            }
        }

        $result = $query->get();

        return $result;
    }
// protected function getCultureStructure($perArr = array())
// {

//     $orgId    = $perArr['orgId'];
//     $updateAt = $perArr["updateAt"];
//     $currentDate = $perArr["currentDate"];
//     $month        = $perArr["month"];
//     $year         = $perArr["year"];

//     $query = DB::table('sot_answers')   
//     ->select('sot_answers.userId')
//     ->distinct()      
//     ->leftJoin('users','users.id','=','sot_answers.userId')
//     ->where('sot_answers.status','Active')
//     ->where('users.status','Active');     

//     if($orgId)
//     {
//         $query->where('users.orgId',$orgId);
//     }

//     if($updateAt)
//     {
//         $query->whereNotNull('sot_answers.updated_at');
//         if(!empty($currentDate))
//         {
//             $query->whereDate('sot_answers.updated_at',$currentDate);
//         }
//         if(!empty($month) && !empty($year))
//         { 
//             $query->whereMonth('sot_answers.updated_at', $month);
//             $query->whereYear('sot_answers.updated_at', $year);
//         }
//     }
//     else
//     {     
//         if(!empty($month) && !empty($year))
//         { 
//             $query->whereMonth('sot_answers.created_at', $month);
//             $query->whereYear('sot_answers.created_at', $year);
//         }
//     }

//     $result = $query->get();

//     return $result;
// }

    //for SOT Motivation
    protected function getMotivation($perArr = array())
    {
        $orgId    = $perArr["orgId"];
        $updateAt = $perArr["updateAt"];
        $last12MonthFirstDate = $perArr["last12MonthFirstDate"];
        $thisMonthLastDate = $perArr["thisMonthLastDate"];
        $last6MonthFirstDate = $perArr["last6MonthFirstDate"];
        $lastMonth = $perArr["lastMonth"];
        $thisMonth = $perArr["thisMonth"];
        $startOfSubWeek = $perArr["startOfSubWeek"];
        $endOfSubWeek = $perArr["endOfSubWeek"];
        $startOfWeek = $perArr["startOfWeek"];
        $endOfWeek = $perArr["endOfWeek"];
        $yesterday = $perArr["yesterday"];
        $today = $perArr["today"];

        $query = DB::table('sot_motivation_answers')
        ->select('sot_motivation_answers.userId')
        ->distinct()      
        ->leftJoin('users','users.id','=','sot_motivation_answers.userId')
        ->where('sot_motivation_answers.status','Active')
        ->where('users.status','Active');     
        if($orgId)
        {
            $query->where('sot_motivation_answers.orgId',$orgId);
        }
        if($updateAt)
        {
            $query->whereNotNull('sot_motivation_answers.updated_at');
            if(!empty($currentDate))
            {
                $query->whereDate('sot_motivation_answers.updated_at',$currentDate);
            }
            if(!empty($month) && !empty($year))
            { 
                $query->whereMonth('sot_motivation_answers.updated_at', $month);
                $query->whereYear('sot_motivation_answers.updated_at', $year);
            }
        }
        else
        {
            // if (!empty($last12MonthFirstDate) && !empty($thisMonthLastDate)) {
            //     $query->whereBetween('sot_motivation_answers.created_at',array($last12MonthFirstDate,$thisMonthLastDate));
            // }
            // elseif (!empty($last6MonthFirstDate) && !empty($thisMonthLastDate)) {
            //     $query->whereBetween('sot_motivation_answers.created_at',array($last6MonthFirstDate,$thisMonthLastDate));
            // }
            // elseif (!empty($lastMonth)) {
            //     $query->where('sot_motivation_answers.created_at','LIKE',$lastMonth);
            // }
            // elseif (!empty($thisMonth)) {
            //     $query->where('sot_motivation_answers.created_at','LIKE',$thisMonth);
            // }
            // elseif (!empty($startOfSubWeek) && !empty($endOfSubWeek)) {
            //     $query->whereBetween('sot_motivation_answers.created_at',array($startOfSubWeek,$endOfSubWeek));
            // }
            // elseif (!empty($startOfWeek) && !empty($endOfWeek)) {
            //     $query->whereBetween('sot_motivation_answers.created_at',array($startOfWeek,$endOfWeek));
            // }
            // elseif (!empty($yesterday)) {
            //     $query->whereDate('sot_motivation_answers.created_at',$yesterday);
            // }
            // elseif (!empty($today)) {
            //     $query->whereDate('sot_motivation_answers.created_at',$today);
            // }

          if (!empty($last12MonthFirstDate) && !empty($thisMonthLastDate)) {
              $query->whereDate('sot_motivation_answers.created_at','<=',$thisMonthLastDate);
              //$query->whereBetween('sot_motivation_answers.created_at',array($last12MonthFirstDate,$thisMonthLastDate));
          }
          elseif (!empty($last6MonthFirstDate) && !empty($thisMonthLastDate)) {
              $query->whereDate('sot_motivation_answers.created_at','<=',$thisMonthLastDate);
              //$query->whereBetween('sot_motivation_answers.created_at',array($last6MonthFirstDate,$thisMonthLastDate));
          }
          elseif (!empty($lastMonth)) {
              $query->whereDate('sot_motivation_answers.created_at','<=',$thisMonthLastDate);
              //$query->where('sot_motivation_answers.created_at','LIKE',$lastMonth);
          }
          elseif (!empty($thisMonth)) {
              $query->whereDate('sot_motivation_answers.created_at','<=',date('Y-m-d'));
              //$query->whereDate('sot_motivation_answers.created_at','<=',date('Y-m-d',strtotime('last day of this month')));
              //$query->where('sot_motivation_answers.created_at','LIKE',$thisMonth);
          }
          elseif (!empty($startOfSubWeek) && !empty($endOfSubWeek)) {
              $query->whereDate('sot_motivation_answers.created_at','<=',$endOfSubWeek);
              //$query->whereBetween('sot_motivation_answers.created_at',array($startOfSubWeek,$endOfSubWeek));
          }
          elseif (!empty($startOfWeek) && !empty($endOfWeek)) {
              $query->whereDate('sot_motivation_answers.created_at','<=',date('Y-m-d'));
              //$query->whereDate('sot_motivation_answers.created_at','<=',$endOfWeek);
              //$query->whereBetween('sot_motivation_answers.created_at',array($startOfWeek,$endOfWeek));
          }
          elseif (!empty($yesterday)) {
              $query->whereDate('sot_motivation_answers.created_at','<=',$yesterday);
          }
          elseif (!empty($today)) {
              $query->whereDate('sot_motivation_answers.created_at','<=',$today);
          }
        }

        $result = $query->get();

        return $result;
    }
// protected function getMotivation($perArr = array())
// {

//     $orgId    = $perArr['orgId'];
//     $updateAt = $perArr["updateAt"];
//     $currentDate = $perArr["currentDate"];
//     $month        = $perArr["month"];
//     $year         = $perArr["year"];

//     $query = DB::table('sot_motivation_answers')
//     ->select('sot_motivation_answers.userId')
//     ->distinct()      
//     ->leftJoin('users','users.id','=','sot_motivation_answers.userId')
//     ->where('sot_motivation_answers.status','Active')
//     ->where('users.status','Active');     
    

//     if($orgId)
//     {
//         $query->where('sot_motivation_answers.orgId',$orgId);
//     }
//     if($updateAt)
//     {
//         $query->whereNotNull('sot_motivation_answers.updated_at');
//         if(!empty($currentDate))
//         {
//             $query->whereDate('sot_motivation_answers.updated_at',$currentDate);
//         }
//         if(!empty($month) && !empty($year))
//         { 
//             $query->whereMonth('sot_motivation_answers.updated_at', $month);
//             $query->whereYear('sot_motivation_answers.updated_at', $year);
//         }
//     }
//     else
//     {

//         if(!empty($month) && !empty($year))
//         { 
//             $query->whereMonth('sot_motivation_answers.created_at', $month);
//             $query->whereYear('sot_motivation_answers.created_at', $year);
//         }
//     }

//     $result = $query->get();

//     return $result;
// }

    //for diagnostic 
    protected function getDiagnostic($perArr = array())
    {
        $orgId    = $perArr["orgId"];
        $updateAt = $perArr["updateAt"];
        $last12MonthFirstDate = $perArr["last12MonthFirstDate"];
        $thisMonthLastDate = $perArr["thisMonthLastDate"];
        $last6MonthFirstDate = $perArr["last6MonthFirstDate"];
        $lastMonth = $perArr["lastMonth"];
        $thisMonth = $perArr["thisMonth"];
        $startOfSubWeek = $perArr["startOfSubWeek"];
        $endOfSubWeek = $perArr["endOfSubWeek"];
        $startOfWeek = $perArr["startOfWeek"];
        $endOfWeek = $perArr["endOfWeek"];
        $yesterday = $perArr["yesterday"];
        $today = $perArr["today"];

        $query = DB::table('diagnostic_answers')
        ->select('diagnostic_answers.userId')
        ->distinct()      
        ->leftJoin('users','users.id','=','diagnostic_answers.userId')
        ->where('diagnostic_answers.status','Active')
        ->where('users.status','Active');     
        if($orgId)
        {
            $query->where('diagnostic_answers.orgId',$orgId);
        }
        if($updateAt)
        {
            $query->whereNotNull('diagnostic_answers.updated_at');
            if (!empty($last12MonthFirstDate) && !empty($thisMonthLastDate)) {
                $query->whereBetween('diagnostic_answers.updated_at',array($last12MonthFirstDate,$thisMonthLastDate));
            }
            elseif (!empty($last6MonthFirstDate) && !empty($thisMonthLastDate)) {
                $query->whereBetween('diagnostic_answers.updated_at',array($last6MonthFirstDate,$thisMonthLastDate));
            }
            elseif (!empty($lastMonth)) {
                $query->where('diagnostic_answers.updated_at','LIKE',$lastMonth);
            }
            elseif (!empty($thisMonth)) {
                $query->where('diagnostic_answers.updated_at','LIKE',$thisMonth);
            }
            elseif (!empty($startOfSubWeek) && !empty($endOfSubWeek)) {
                $query->whereBetween('diagnostic_answers.updated_at',array($startOfSubWeek,$endOfSubWeek));
            }
            elseif (!empty($startOfWeek) && !empty($endOfWeek)) {
                $query->whereBetween('diagnostic_answers.updated_at',array($startOfWeek,$endOfWeek));
            }
            elseif (!empty($yesterday)) {
                $query->whereDate('diagnostic_answers.updated_at',$yesterday);
            }
            elseif (!empty($today)) {
                $query->whereDate('diagnostic_answers.updated_at',$today);
            }
        }
        
        $result = $query->get();

        return $result;
    }

    //for tribeometer 
    protected function getTribeometer($perArr = array())
    {
        $orgId    = $perArr["orgId"];
        $updateAt = $perArr["updateAt"];
        $last12MonthFirstDate = $perArr["last12MonthFirstDate"];
        $thisMonthLastDate = $perArr["thisMonthLastDate"];
        $last6MonthFirstDate = $perArr["last6MonthFirstDate"];
        $lastMonth = $perArr["lastMonth"];
        $thisMonth = $perArr["thisMonth"];
        $startOfSubWeek = $perArr["startOfSubWeek"];
        $endOfSubWeek = $perArr["endOfSubWeek"];
        $startOfWeek = $perArr["startOfWeek"];
        $endOfWeek = $perArr["endOfWeek"];
        $yesterday = $perArr["yesterday"];
        $today = $perArr["today"];

        $query = DB::table('tribeometer_answers')
        ->select('tribeometer_answers.userId')
        ->distinct()      
        ->leftJoin('users','users.id','=','tribeometer_answers.userId')
        ->where('tribeometer_answers.status','Active')
        ->where('users.status','Active');     
        if($orgId)
        {
            $query->where('tribeometer_answers.orgId',$orgId);
        }
        if($updateAt)
        {
            $query->whereNotNull('tribeometer_answers.updated_at');
            if (!empty($last12MonthFirstDate) && !empty($thisMonthLastDate)) {
                $query->whereBetween('tribeometer_answers.updated_at',array($last12MonthFirstDate,$thisMonthLastDate));
            }
            elseif (!empty($last6MonthFirstDate) && !empty($thisMonthLastDate)) {
                $query->whereBetween('tribeometer_answers.updated_at',array($last6MonthFirstDate,$thisMonthLastDate));
            }
            elseif (!empty($lastMonth)) {
                $query->where('tribeometer_answers.updated_at','LIKE',$lastMonth);
            }
            elseif (!empty($thisMonth)) {
                $query->where('tribeometer_answers.updated_at','LIKE',$thisMonth);
            }
            elseif (!empty($startOfSubWeek) && !empty($endOfSubWeek)) {
                $query->whereBetween('tribeometer_answers.updated_at',array($startOfSubWeek,$endOfSubWeek));
            }
            elseif (!empty($startOfWeek) && !empty($endOfWeek)) {
                $query->whereBetween('tribeometer_answers.updated_at',array($startOfWeek,$endOfWeek));
            }
            elseif (!empty($yesterday)) {
                $query->whereDate('tribeometer_answers.updated_at',$yesterday);
            }
            elseif (!empty($today)) {
                $query->whereDate('tribeometer_answers.updated_at',$today);
            }
        }
        
        $result = $query->get();

        return $result;
    }

    //for improvements 

function getImprovements($method, $url, $data){
    $curl = curl_init();

    switch ($method)
    {
        case "POST":
        curl_setopt($curl, CURLOPT_POST, 1);
        if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        break;
        case "PUT":
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);                              
        break;
        default:
        if ($data)
            $url = sprintf("%s?%s", $url, http_build_query($data));
    }

   // OPTIONS:
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'APIKEY: 111111111111111111111',
        'Content-Type: application/json',
    ));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

   // EXECUTE:
    $result = curl_exec($curl);
    if(!$result){die("Connection Failure");}
    curl_close($curl);
    return $result;
}

function countNumberOfMonths($orgId)
{
    $firstStaffOfOrg = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
          
    // $firstDateOfOrg = date('Y-m-d',strtotime($firstStaffOfOrg->created_at));
    // $lastMonthDate = date('Y-m-d',strtotime('-1 month'));
    $firstDateOfOrg = date('Y-m',strtotime($firstStaffOfOrg->created_at));
    $lastMonthDate = date('Y-m',strtotime('-1 month'));

    $time1 = strtotime($firstDateOfOrg);
    $time2 = strtotime($lastMonthDate);
    if(date('m', $time1)==date('m', $time2) && date('Y', $time1)==date('Y', $time2)){
      //$noOfPreviousMonths = 1;
      return 1;
    }
    else{
      if ($time1 < $time2) 
      {
        $my = date('mY', $time2);
        $noOfPreviousMonths = array(date('F', $time1));
        while($time1 < $time2) {
          $time1 = strtotime(date('Y-m-d', $time1).' +1 month');
          if(date('mY', $time1) != $my && ($time1 < $time2))
            $noOfPreviousMonths[] = date('F', $time1);
        }
        $noOfPreviousMonths[] = date('F', $time2);
        return $noOfPreviousMonths;
      }
      else
      {
        //$noOfPreviousMonths = 0;
        return 0;
      } 
    }
}


}
