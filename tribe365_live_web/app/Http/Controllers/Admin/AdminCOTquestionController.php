<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;
use Validator;
use Hash;
use Config;
use App\Organisation;

class AdminCOTquestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $a = app('App\Http\Controllers\Admin\CommonController')->checkUser(Auth::user());

        if (!empty($a)) {
          return redirect('/admin');
        }
        
        $cotQuestions = DB::table('cot_questions')
        ->where('status','Active')
        ->orderBy('id','ASC')
        ->get();

        return view('admin/COT/question/listCOTquestions',compact('cotQuestions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/COT/question/addCOTquestion');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $options = Input::get('option');

        $insertArray = array(
            'question'  => Input::get('question'),
            'status'    => 'Active', 
            'created_at'=> date('Y-m-d H:i:s')
        );

        $questionId = DB::table('cot_questions')->insertGetId($insertArray);

        if(!empty($questionId))
        {

            foreach ($options as $value) {

                $optInsertArray = array(
                    'option_name'    => $value,
                    'cot_question_id'=> $questionId, 
                    'status'         => 'Active', 
                    'created_at'     => date('Y-m-d H:i:s')
                );

                DB::table('cot_options')->insertGetId($optInsertArray);
            }
        }

        return redirect('/admin/cot-question')->with('message','Record Added Successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $id = base64_decode($id);

      $cotQuestions = DB::table('cot_questions')    
      ->select('cot_options.id','cot_options.cot_question_id','cot_questions.question','cot_options.option_name','cot_role_map_options.maper')    
      ->leftJoin('cot_options', 'cot_questions.id', '=', 'cot_options.cot_question_id') 
      ->leftJoin('cot_role_map_options', 'cot_role_map_options.id', '=', 'cot_options.maper') 
      ->where('cot_questions.id',$id)      
      ->where('cot_options.status','Active')    
      ->get();

      return view('admin/COT/question/editCOTquestion',compact('cotQuestions'));

  }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $queId = base64_decode($id);

        $question   = Input::get('question');
        $option     = Input::get('option');
        $optionId   = Input::get('optionId');

        $updateArray = array('question'=>$question,'updated_at'=>date('Y-m-d H:i:s'));

        DB::table('cot_questions')
        ->where('id',$queId)
        ->update($updateArray);


        for ($i=0; $i <count($optionId); $i++) 
        { 

            $optUpdateArray = array('option_name'=>$option[$i],'updated_at'=>date('Y-m-d H:i:s'));

            DB::table('cot_options')
            ->where('id',$optionId[$i])
            ->update($optUpdateArray);
        }

        return redirect()->back()->with('message','Record Updated Successfully.');
        // return redirect('/admin/cot-question')->with('message','Record Updated Successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = base64_decode($id);

        $updateArray = array('status'=>'Inactive','updated_at'=>date('Y-m-d H:i:s'));

        DB::table('cot_questions')
        ->where('id',$id)
        ->update($updateArray);

        DB::table('cot_options')
        ->where('cot_question_id',$id)
        ->update($updateArray);

        return redirect('/admin/cot-question')->with('message','Record Deleted Successfully.');

    }

    /*cot functional lens questions list*/
    public function getCOTfunctionalLensQuestionList()
    {
        $cotFuncQuestions = DB::table('cot_functional_lens_questions')->where('status','Active')->paginate(10);

        return view('admin/COT/question/listCOTfunctionalLensQuestion',compact('cotFuncQuestions'));
    }

    /*cot functinal lens questions list*/
    public function getCOTfunctionalLensQuestionOptionList(Request $request)
    {
        $questionsId  = base64_decode($request->id);

        $cotFuncQuestionsOpt = DB::table('cot_functional_lens_questions AS q')
        ->select('o.id','o.question_id','q.question','o.option_name','o.value_name')
        ->leftJoin('cot_functional_lens_question_options AS o', 'q.id', '=', 'o.question_id')
        ->where('q.id',$questionsId)
        ->where('o.status','Active')
        ->get();
       
        return view('admin/COT/question/listCOTfunctionalLensQuestionOpt',compact('cotFuncQuestionsOpt'));

    }

    /*update functional lens question and option*/
    public function updateFunctionalLensQueOpt()
    {       
        $questionsId= Input::get('questionId');
        $question   = Input::get('question');
        $option     = Input::get('optionName');
        $optionId   = Input::get('optionId');

        $updateArray = array('question'=>$question,'updated_at'=>date('Y-m-d H:i:s'));

        DB::table('cot_functional_lens_questions')->where('id',$questionsId)->update($updateArray);

        for ($i=0; $i <count($optionId); $i++) 
        { 
            $optUpdateArray = array('option_name'=>$option[$i],'updated_at'=>date('Y-m-d H:i:s'));

            DB::table('cot_functional_lens_question_options')->where('id',$optionId[$i])->update($optUpdateArray);
        }

        return redirect()->back()->with('message','Record Updated Successfully.');
    }
}
