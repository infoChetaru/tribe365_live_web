<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use DateTime;
use Mail;
use Config;
use DatePeriod;
use DateInterval;

class AdminDashboardController extends Controller
{
  public function index(Request $request) {

    $a = app('App\Http\Controllers\Admin\CommonController')->checkUser(Auth::user());

    if (!empty($a)) {
      return redirect('/admin');
    }
    // $a = app('App\Http\Controllers\Admin\CommonController')->checkUser(Auth::user());

    // if (!empty($a)) {
    //   return redirect('/admin');
    // }

    $resultArray = array();

    $orgId = $request->orgId;
    $officeId = $request->officeId;
    $departmentId = $request->departmentId;

    if(isset($request->ispdf)){
      $ispdf = $request->ispdf;  
    }else{
      $ispdf = 2;
    }

    $organisations = DB::table('organisations')->where('status','Active')->orderBy('organisation','ASC')->get();

    //By default 1st organisation selected
    $organisation = DB::table('organisations')->where('status','Active')->orderBy('organisation','ASC')->first();
    if (empty($orgId)) {
        $orgId = $organisation->id;   
    }

    // $orgId = 8;

    $organisationDetails = DB::table('organisations')->where('id',$orgId)->orderBy('organisation','ASC')->first();

    //Get weekend flag from organisation
    $includeWeekend = DB::table('organisations')->where('status','Active')->where('id',$orgId)->first();

    $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get();

    $all_department = DB::table('all_department')->where('status','Active')->get();

    $departments = [];

    if (!empty($officeId)) {
      $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->leftJoin('users','users.departmentId','departments.id')
        ->where('departments.officeId',$officeId) 
        ->where('all_department.status','Active') 
        ->where('departments.status','Active') 
        ->where('users.status','Active') 
        ->orderBy('all_department.department','ASC')
        ->groupBy('all_department.department','departments.id')->get();
    }elseif (empty($officeId)) {
      $all_department = DB::table('departments')
        ->select('all_department.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->leftJoin('users','users.departmentId','departments.id')
        ->where('departments.orgId',$orgId) 
        ->where('departments.status','Active') 
        ->where('all_department.status','Active') 
        ->where('users.status','Active') 
        ->orderBy('all_department.department','ASC')
        ->groupBy('all_department.department','all_department.id')->get();
    }

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }

    $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();

    //Culture Index graph
    $cultureIndexPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend->include_weekend, 'months'=>$months,'index'=>1,'ispdf'=>$ispdf);
    $resultArray['cultureIndexArray'] = $this->getCultureIndexValues($cultureIndexPer);

    //Engagement Index graph
    $engagementIndexPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend->include_weekend, 'months'=>$months,'index'=>2,'ispdf'=>$ispdf);
    $resultArray['engagementIndexArray'] = $this->getCultureIndexValues($engagementIndexPer);

    //Happy Index graph
    $happyIndexPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend->include_weekend, 'months'=>$months,'ispdf'=>$ispdf);
    $resultArray['happyIndexArray'] = $this->getHappyIndexValues($happyIndexPer);

    //Diagnostic graph
    $diagnosticPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend->include_weekend, 'months'=>$months,'ispdf'=>$ispdf);
    $resultArray['diagnosticArray'] = $this->getDiagnosticValues($diagnosticPer);

    // $diagnosticQueCatTbl = DB::table('diagnostic_questions_category')->get();

    //DOT graph
    $dotPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend->include_weekend,'months'=>$months,'ispdf'=>$ispdf);
    $resultArray['dotArray'] = $this->getDOTBeliefs($dotPer);

    //Thumbsup graph
    $thumbsupPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend->include_weekend,'months'=>$months);
    $resultArray['thumbsupArray'] = $this->getThumbsupBeliefs($thumbsupPer);

    //Team Role graph
    $teamRolePer = array('orgId'=>$orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend,'months'=>$months);
    $resultArray['teamRoleArray'] = $this->getTeamRoleValues($teamRolePer);

    //Tribeometer graph
    $tribeometerPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend->include_weekend,'months'=>$months,'ispdf'=>$ispdf);
    $resultArray['tribeometerArray'] = $this->getTribeometerValues($tribeometerPer);

    // $queCatTbl      = DB::table('tribeometer_questions_category')->get();

    //Motivation graph
    $motivationPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend->include_weekend,'months'=>$months,'ispdf'=>$ispdf);
    $resultArray['motivationArray'] = $this->getMotivationValues($motivationPer);

    // $categoryTbl   = DB::table('sot_motivation_value_records')->where('status','Active')->get();

    //Personality Type graph
    $personalityTypePer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend->include_weekend,'months'=>$months,'ispdf'=>$ispdf);
    $resultArray['personalityTypeArray'] = $this->getPersonalityTypeValues($personalityTypePer);

    //Culture structure graph
    $cultureStructurePer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend->include_weekend, 'months'=>$months,'ispdf'=>$ispdf);
    $resultArray['cultureStructureArray'] = $this->getCultureStructureValues($cultureStructurePer);

    //Happy index responders graph
    $happyIndexResPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId, 'includeWeekend'=>$includeWeekend->include_weekend, 'months'=>$months,'ispdf'=>$ispdf);
    $resultArray['happyIndexResArray'] = $this->getHappyIndexRespValues($happyIndexResPer);

    //Dot Values Completed
    $dotValuesPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);
    $dotComplete = $this->dotValuesComplete($dotValuesPer);

    $resultArray['dotComplete'] = $dotComplete['dotCompleted'];
    $resultArray['dotRatingCount'] = $dotComplete['dotRatingCount'];

    //Team Role Completed
    $teamRolePerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);
    $resultArray['teamRoleComplete'] = $this->teamRoleComplete($teamRolePerArr);

    //Teamrole MM and MA count
    $resultArray['teamRoleCompletedMMMA'] = 0;
    if (!empty($staff)) {
      $resultArray['teamRoleCompletedMMMA'] = $this->getTeamroleMAMMCount($teamRolePerArr);
    }
    // else
    // {
    //   $resultArray['teamRoleCompletedMMMA']['MM'] = 0;
    //   $resultArray['teamRoleCompletedMMMA']['MA'] = 0;
    // }

    //Personality Type Completed
    $personalityTypePerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);
    $resultArray['personalityTypeComplete'] = $this->personalityTypeComplete($personalityTypePerArr);

    //Personality type MM and MA count
    $personalityTypeCompletedArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);

    $resultArray['personalityTypeCompletedMAMM'] = 0;
    if (!empty($staff)) {
      $resultArray['personalityTypeCompletedMAMM'] = $this->getPersonalityTypeMAMMCount($personalityTypeCompletedArr);
    }
    else
    {
      $resultArray['personalityTypeCompletedMAMM']['MM'] == 0;
      $resultArray['personalityTypeCompletedMAMM']['MA'] == 0;
    }

    //Motivation Completed
    $motivationPerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);
    $resultArray['motivationComplete'] = $this->motivationComplete($motivationPerArr);

    //Motivation MM and MA count
    $motivationCompletedArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);

    $resultArray['motivationCompletedMAMM'] = 0;
    if (!empty($staff)) {
      $resultArray['motivationCompletedMAMM'] = $this->getMotivationMAMMCount($motivationCompletedArr);
    }
    else
    {
      $resultArray['motivationCompletedMAMM']['MM'] == 0;
      $resultArray['motivationCompletedMAMM']['MA'] == 0;
    }

    //Kudos Completed yesterday
    $kudosPerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);
    $resultArray['kudosComplete'] = $this->kudosComplete($kudosPerArr);

    //MA and MM for bubble rating
    $bubbleRatingData = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);

    $resultArray['bubbleRating'] = 0;
    if (!empty($staff)) {
      $resultArray['bubbleRating'] = $this->getBubbleRatingCount($bubbleRatingData);
    }
    else
    {
      $resultArray['bubbleRating']['DD'] == 0;
      $resultArray['bubbleRating']['MA'] == 0;
    }

    //Happy index Completed yesterday
    $happyIndexPerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);
    $resultArray['happyIndexComplete'] = $this->happyIndexComplete($happyIndexPerArr);

    //MA and MM for Happy Index
    $happyIndexArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);

    $resultArray['happyIndexDDMA'] = 0;
    if (!empty($staff)) {
      $resultArray['happyIndexDDMA'] = $this->getHappyIndexMMMA($happyIndexArr);
    }
    else
    {
      $resultArray['happyIndexDDMA']['DD'] == 0;
      $resultArray['happyIndexDDMA']['MA'] == 0;
    }

    //Improvements Last month
    $improvementPerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);
    $resultArray['improvements'] = $this->getImprovements($improvementPerArr);

    //MA and MM for improvements
    $improvementArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);

    $resultArray['improvementsMAMM'] = 0;
    if (!empty($staff)) {
      $resultArray['improvementsMAMM'] = $this->getImprovementsMMMA($improvementArr);
    }else{
      $resultArray['improvementsMAMM']['MM'] == 0;
      $resultArray['improvementsMAMM']['MA'] == 0;
    }

    //Culture Structure Updated Last month
    $cultureStructurePerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);
    $resultArray['cultureStructure'] = $this->getCultureStructure($cultureStructurePerArr);

    //culture  structure MM and MA count
    $cultureStructureCompletedArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);

    $resultArray['cultureStructureCompletedMAMM'] = 0;
    if (!empty($staff)) {
      $resultArray['cultureStructureCompletedMAMM'] = $this->getCultureStructureMAMMCount($cultureStructureCompletedArr);
    }else{
      $resultArray['cultureStructureCompletedMAMM']['MM'] == 0;
      $resultArray['cultureStructureCompletedMAMM']['MA'] == 0;
    }

    //Tribeometer Updated Last month
    $tribeometerPerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);
    $resultArray['tribeometer'] = $this->getTribeometer($tribeometerPerArr);

    //Tribeometer MM and MA count
    $tribeometerUpdateArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);

    $resultArray['tribeometerUpdatedMAMM'] = 0;
    if (!empty($staff)) {
      $resultArray['tribeometerUpdatedMAMM'] = $this->getTribeometerMAMMCount($tribeometerUpdateArr);
    }
    else
    {
      $resultArray['tribeometerUpdatedMAMM']['MM'] == 0;
      $resultArray['tribeometerUpdatedMAMM']['MA'] == 0;
    }

    //Diagnostic Updated Last month
    $diagnosticPerArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);
    $resultArray['diagnostic'] = $this->getDiagnostics($diagnosticPerArr);

    //Diagnostic MM and MA count
    $diagnosticUpdateArr = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend->include_weekend);

    $resultArray['diagnosticUpdatedMAMM'] = 0;
    if (!empty($staff)) {
      $resultArray['diagnosticUpdatedMAMM'] = $this->getDiagnosticMAMMCount($diagnosticUpdateArr);
    }
    else
    {
      $resultArray['diagnosticUpdatedMAMM']['MM'] == 0;
      $resultArray['diagnosticUpdatedMAMM']['MA'] == 0;
    }

    if($ispdf==1){
      return view('admin/adminDashboardPdf',compact('organisationDetails','ispdf','resultArray','organisations','orgId','offices','all_department','departments','departmentId','officeId','includeWeekend'));
    }else{
      return view('admin/adminDashboard',compact('ispdf','resultArray','organisations','orgId','offices','all_department','departments','departmentId','officeId','includeWeekend'/*,'diagnosticQueCatTbl'*//*,'queCatTbl','categoryTbl'*/));
    }

  }

  //Dashboard Culture Index and Engagement Index
  public function getCultureIndexValues($perArr = array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];
    $months   = $perArr["months"];
    //$year   = $perArr["year"];
    $index   = $perArr["index"];

    $cultureIndexArray = array();
    $max = 0;
    foreach ($months as $key => $month) {
      if ($key <=9) {
          $key = "0".$key;
      }
      $indexDataQuery = DB::table('indexReportRecords')
      ->where('status','Active')
      ->where('date','LIKE',$month."%");
      // ->where('date','LIKE',$year."-".$key."%");
      if(!empty($orgId))
      {
          $indexDataQuery->where('orgId',$orgId);
      }
      if(empty($officeId) && empty($departmentId))
      {
        $indexDataQuery->whereNull('officeId');
        $indexDataQuery->whereNull('departmentId');
      }
      elseif(!empty($officeId) && empty($departmentId))
      {
        $indexDataQuery->where('officeId',$officeId);
        $indexDataQuery->whereNull('departmentId');
      }
      elseif(!empty($officeId) && !empty($departmentId))
      {
        $indexDataQuery->where('officeId',$officeId);
        $indexDataQuery->where('departmentId',$departmentId);
      }
      elseif(empty($officeId) && !empty($departmentId))
      {
        $indexDataQuery->whereNull('officeId');
        $indexDataQuery->where('departmentId',$departmentId);
      }
      if($includeWeekend != 1){ //Exclude Weekend Days
        //Get weekends
        $YearMonth = $month;
        // $YearMonth = $year."-".$key;
        $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);

        $indexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(indexReportRecords.date,'%Y-%m-%d'))"),$getWeekendDates);
      }
      $indexData = $indexDataQuery->get();
      
      $c = 0;
      $totalCount = 0;
      $totalIndex = 0;

      if($indexData){
        foreach($indexData as $indexRecord){
          if($includeWeekend != 1){ //Exclude Weekend Days
            if ($index == 1) {
              $totalCount = $indexRecord->total_count_exclude_weekend;
            }elseif ($index == 2) {
              $totalCount = $indexRecord->engage_without_weekend;
            }
          }else{
            if ($index == 1) {
              $totalCount = $indexRecord->total_count;
            }elseif ($index == 2) {
              $totalCount = $indexRecord->engage_with_weekend;
            }
          }
          if($totalCount > 0){
            $c++;
          }  
          $totalIndex+=$totalCount;
        }
      }
      if($c>0){
        $totalIndex = round(($totalIndex/$c),2);    
        $cultureIndexData['data'] = $totalIndex;
      }else{
        $cultureIndexData['data'] = $totalIndex;
      }
      if ($totalIndex != 0) {
        $max++;
      }

      $cultureIndexData['monthName'] = date('My',strtotime($month));   

      array_push($cultureIndexArray, $cultureIndexData);
    }
    return array('indexArray'=>$cultureIndexArray,'maxCount'=>$max);
  }

  //Dashboard Happy Index
  public function getHappyIndexValues($perArr = array()){
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend = $perArr["includeWeekend"];
    $months         = $perArr["months"];
    $ispdf          = $perArr["ispdf"];

    $arrMain   = array();
    $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();
    $moodValue = array("");   //first index
    foreach ($happyIndexMoodValues as $key => $value) {
      $moodValue[$value->id] = ucfirst($value->moodName)."(%)";
    }
    array_push($arrMain, $moodValue);
    
    $max = 0;
    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));   //first index of month

      foreach ($happyIndexMoodValues as $moodVal) {

        $happyIndexDataQuery = DB::table('happy_index_dashboard_graph')
          ->where('status','Active')
          ->where('categoryId',$moodVal->id)
          ->where('date','LIKE',$month."%");
        if(!empty($orgId)) {
            $happyIndexDataQuery->where('orgId',$orgId);
        }
        if(empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->where('departmentId',$departmentId);
        } elseif(empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->where('departmentId',$departmentId);
        }
        if($includeWeekend != 1){ //Exclude Weekend Days
          //Get weekends
          $YearMonth = $month;
          $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);
          $happyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
        }
        $happyIndexData = $happyIndexDataQuery->get();

        if ($month != date('Y-m')) {
          $yearMonth = explode('-', $month);
          $weekdaysCount = 0;
          if ($yearMonth) {  
            $currentYear = $yearMonth[0]; 
            $currentMonth = $yearMonth[1]; 
            // $weekdaysCount = $this->countDays($currentYear, $currentMonth, array(0, 6)); //21
            $weekdaysCount = app('App\Http\Controllers\Admin\CommonController')->countDays($currentYear, $currentMonth, array(0, 6)); //21
          }
        }else{
          $start = new DateTime(date('Y-m-01'));
          $end = new DateTime(date('Y-m-d',strtotime('-1 days')));
          // otherwise the  end date is excluded (bug?)
          $end->modify('+1 day');
          $interval = $end->diff($start);
          // total days
          $days = $interval->days;
          // create an iterateable period of date (P1D equates to 1 day)
          $period = new DatePeriod($start, new DateInterval('P1D'), $end);
          // best stored as array, so you can add more than one
          $holidays = [];
          foreach($period as $dt) {
              $curr = $dt->format('D');
              // substract if Saturday or Sunday
              if ($curr == 'Sat' || $curr == 'Sun') {
                  $days--;
              }
              // (optional) for the updated question
              elseif (in_array($dt->format('Y-m-d'), $holidays)) {
                  $days--;
              }
          }
          $weekdaysCount  = $days;
        }
        $a = 0;
        $totalCount = 0;
        $totalHappyIndex = 0;

        if(count($happyIndexData)){
          foreach($happyIndexData as $happyIndexRecord){
            if($includeWeekend != 1){ //Exclude Weekend Days
                $totalCount = $happyIndexRecord->without_weekend;
            }else{
                $totalCount = $happyIndexRecord->with_weekend;
            }
            // if($totalCount > 0){
            //   $a++;
            // }  
            $totalHappyIndex+=$totalCount;
          }
        }
        // if($a>0){
        $happyIndexData1 = 0;
        if (!empty($totalHappyIndex) && !empty($weekdaysCount)) {
          $totalHappyIndex = round(($totalHappyIndex/$weekdaysCount),2);    
          $happyIndexData1= $totalHappyIndex;
        }
        if ($happyIndexData1 != 0) {
            $max++;
        }
        // echo "<pre>";print_r($happyIndexData1);
        // }else{
        //   $happyIndexData['data'] = $totalHappyIndex;
        // }
        array_push($newArr,$happyIndexData1);
      }





      // $totalUsersQuery = DB::table('happy_indexes')
      // ->leftjoin('users','users.id','happy_indexes.userId')
      // // ->where('users.status','Active')
      // ->where('happy_indexes.status','Active')
      // ->where('happy_indexes.created_at','LIKE',$month.'%');
      // if(!empty($orgId)){
      //     $totalUsersQuery->where('users.orgId',$orgId);
      // }
      // if(!empty($officeId) && empty($departmentId)) {
      //     $totalUsersQuery->where('users.officeId',$officeId);
      // }elseif(!empty($officeId) && !empty($departmentId)) {
      //     $totalUsersQuery->where('users.officeId',$officeId);
      //     $totalUsersQuery->where('users.departmentId',$departmentId);
      // }elseif(empty($officeId) && !empty($departmentId)) {
      //     $totalUsersQuery->leftJoin('departments','departments.id','users.departmentId')
      //         ->where('departments.status','Active')
      //         ->where('departments.departmentId',$departmentId);
      // }
      // if($includeWeekend != 1){ //Exclude Weekend Days
      //   //Get weekends
      //   $YearMonth = $month;
      //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);

      //   $totalUsersQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      // }
      // $totalUsers = $totalUsersQuery->count();

      // foreach ($happyIndexMoodValues as $moodVal) {
      //   $monthCountQuery = DB::table('happy_indexes')
      //       ->leftjoin('users','users.id','happy_indexes.userId')
      //       // ->where('users.status','Active')
      //       ->where('happy_indexes.status','Active')
      //       ->where('happy_indexes.moodValue',$moodVal->id)
      //       ->where('happy_indexes.created_at','LIKE',$month.'%');
      //   if(!empty($orgId)){
      //       $monthCountQuery->where('users.orgId',$orgId);
      //   }
      //   if(!empty($officeId) && empty($departmentId)){
      //     $monthCountQuery->where('users.officeId',$officeId);
      //   }elseif(!empty($officeId) && !empty($departmentId)){
      //     $monthCountQuery->where('users.officeId',$officeId);
      //     $monthCountQuery->where('users.departmentId',$departmentId);
      //   }elseif(empty($officeId) && !empty($departmentId)){
      //     $monthCountQuery->leftJoin('departments','departments.id','users.departmentId')
      //         ->where('departments.status','Active')
      //         ->where('departments.departmentId',$departmentId);
      //   }
      //   if($includeWeekend != 1){ //Exclude Weekend Days
      //     //Get weekends
      //     $YearMonth = $month;
      //     $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);

      //     $monthCountQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      //   }
      //   $moodCount = $monthCountQuery->count();

      //   $moodCountUsers = 0;
      //   if (!empty($moodCount) && !empty($totalUsers)) {
      //     $moodCountUsers = round((($moodCount/$totalUsers)*100),2);
      //   }
      //   array_push($newArr,$moodCountUsers);
      // }
      array_push($arrMain, $newArr);
    }
    // die();
    // echo "<pre>";print_r($max);die;
    return array('arrMain'=>$arrMain,'maxCount'=>$max);
  }

  // function countDays($year, $month, $ignore) {
  //   $count = 0;
  //   $counter = mktime(0, 0, 0, $month, 1, $year);
  //   while (date("n", $counter) == $month) {
  //       if (in_array(date("w", $counter), $ignore) == false) {
  //           $count++;
  //       }
  //       $counter = strtotime("+1 day", $counter);
  //   }
  //   return $count;
  // }

  public function getHappyIndexCat(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $moodId           = Input::get('moodId');
    $columnColor      = Input::get('columnColor');

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }

    $org = DB::table('organisations')->where('id',$orgId)->first();
    $includeWeekend = $org->include_weekend;

    $arrMain = array();
    if ($moodId) {
      $happyIndexMoodValues = DB::table('happy_index_mood_values')->where('id',$moodId)->get();
    }else{
      $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();
    }

    $moodValue = array("");   //first index
    foreach ($happyIndexMoodValues as $key => $value) {
      $moodValue[] = ucfirst($value->moodName)."(%)";
    }
    array_push($arrMain, $moodValue);

    $happyIndexCountMonth = array();
    $max = 0;
    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));   //first index of month

      foreach ($happyIndexMoodValues as $moodVal) {

        $happyIndexDataQuery = DB::table('happy_index_dashboard_graph')
          ->where('status','Active')
          ->where('categoryId',$moodVal->id)
          ->where('date','LIKE',$month."%");
        if(!empty($orgId)) {
            $happyIndexDataQuery->where('orgId',$orgId);
        }
        if(empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->where('departmentId',$departmentId);
        } elseif(empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->where('departmentId',$departmentId);
        }
        if($includeWeekend != 1){ //Exclude Weekend Days
          //Get weekends
          $YearMonth = $month;
          $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);
          $happyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
        }
        $happyIndexData = $happyIndexDataQuery->get();

        if ($month != date('Y-m')) {
          $yearMonth = explode('-', $month);
          $weekdaysCount = 0;
          if ($yearMonth) {  
            $currentYear = $yearMonth[0]; 
            $currentMonth = $yearMonth[1]; 
            $weekdaysCount = app('App\Http\Controllers\Admin\CommonController')->countDays($currentYear, $currentMonth, array(0, 6)); //21
          }
        }else{
          $start = new DateTime(date('Y-m-01'));
          $end = new DateTime(date('Y-m-d',strtotime('-1 days')));
          // otherwise the  end date is excluded (bug?)
          $end->modify('+1 day');
          $interval = $end->diff($start);
          // total days
          $days = $interval->days;
          // create an iterateable period of date (P1D equates to 1 day)
          $period = new DatePeriod($start, new DateInterval('P1D'), $end);
          // best stored as array, so you can add more than one
          $holidays = [];
          foreach($period as $dt) {
              $curr = $dt->format('D');
              // substract if Saturday or Sunday
              if ($curr == 'Sat' || $curr == 'Sun') {
                  $days--;
              }
              // (optional) for the updated question
              elseif (in_array($dt->format('Y-m-d'), $holidays)) {
                  $days--;
              }
          }
          $weekdaysCount  = $days;
        }
        $a = 0;
        $totalCount = 0;
        $totalHappyIndex = 0;

        if(count($happyIndexData)){
          foreach($happyIndexData as $happyIndexRecord){
            if($includeWeekend != 1){ //Exclude Weekend Days
                $totalCount = $happyIndexRecord->without_weekend;
            }else{
                $totalCount = $happyIndexRecord->with_weekend;
            }
            // if($totalCount > 0){
            //   $a++;
            // }  
            $totalHappyIndex+=$totalCount;
          }
        }
        // if($a>0){
        $happyIndexData = 0;
        if (!empty($totalHappyIndex) && !empty($weekdaysCount)) {
          $totalHappyIndex = round(($totalHappyIndex/$weekdaysCount),2);    
          $happyIndexData = $totalHappyIndex;
        }

        if ($happyIndexData != 0) {
            $max++;
        }
        // }else{
        //   $happyIndexData['data'] = $totalHappyIndex;
        // }
        array_push($newArr,$happyIndexData);
      }
      array_push($arrMain, $newArr);
    }
    // echo "<pre>";print_r($arrMain);die();
    return array('status'=>true,'arrMain'=>$arrMain,'columnColor'=>$columnColor,'maxCount'=>$max);
  }

  public function getHappyIndexWeekGraph(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $monthNum         = Input::get('monthNum');

    $org = DB::table('organisations')->where('id',$orgId)->first();
    $includeWeekend = $org->include_weekend;

    $arrMain = array();

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }
    $yearMonth    = $months[$monthNum]; //2019-11
    $yearMonthExp = explode("-",$yearMonth);

    $year  = $yearMonthExp[0];
    $month = $yearMonthExp[1];

    $monthCount = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    //echo $totalDays;die;
    $weeks = $monthCount%7;

    if ($weeks) {
        $arrayCount = array("Week 1","Week 2","Week 3","Week 4","Week 5");
    }else{
        $arrayCount = array("Week 1","Week 2","Week 3","Week 4");
    }

    $lastDay = date('t',strtotime($year."-".$month."-1"));

    $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();
    $moodValue = array("");   //first index
    foreach ($happyIndexMoodValues as $key => $value) {
      $moodValue[] = ucfirst($value->moodName)."(%)";
    }
    array_push($arrMain, $moodValue);

    $i = 1;$j = 7;
    $max = 0;
    foreach ($arrayCount as $key => $value) {
      $newArr = array($value);   //first index of week

      foreach ($happyIndexMoodValues as $moodVal) {

        $happyIndexDataQuery = DB::table('happy_index_dashboard_graph')
          ->where('status','Active')
          ->where('categoryId',$moodVal->id);
        if(!empty($orgId)) {
            $happyIndexDataQuery->where('orgId',$orgId);
        }
        if(empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->where('departmentId',$departmentId);
        } elseif(empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->where('departmentId',$departmentId);
        }
        if($includeWeekend != 1){ //Exclude Weekend Days
          //Get weekends
          // $YearMonth = $month;
          $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($yearMonth,2);
          $happyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
        }
        if ($i==29) {
            $happyIndexDataQuery->whereBetween('happy_index_dashboard_graph.date',[$year."-".$month."-".$i,$year."-".$month."-".$lastDay]);
        }else{
            $happyIndexDataQuery->whereBetween('happy_index_dashboard_graph.date',[$year."-".$month."-".$i,$year."-".$month."-".$j]);
        }
        $happyIndexData = $happyIndexDataQuery->get();
        
        $a = 0;
        $totalCount = 0;
        $totalHappyIndex = 0;

        if(count($happyIndexData)){
          foreach($happyIndexData as $happyIndexRecord){
            if($includeWeekend != 1){ //Exclude Weekend Days
                $totalCount = $happyIndexRecord->without_weekend;
                if ($happyIndexRecord->without_weekend != 0) {
                    $max++;
                }
            }else{
                $totalCount = $happyIndexRecord->with_weekend;
                if ($happyIndexRecord->with_weekend != 0) {
                    $max++;
                }
            }
            $totalHappyIndex+=$totalCount;
          }
        }

        $happyIndexData1 = 0;
        if (!empty($totalHappyIndex)) {
          $finalHappyIndex = 0;
          if ($i==29) {
            if (date('Y-m-d',strtotime($year."-".$month)) == date('Y-m')) { //21<5
              $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, date('y-m-d',strtotime('-1 day', strtotime($year."-".$month."-".date('d')))));
              // $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, $year."-".$month."-".date('d'));
            }else{
              $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, $year."-".$month."-".$lastDay);
            }
            // $finalHappyIndex = 0;
            if(!empty($totalHappyIndex) && !empty($weekdays)){
              $finalHappyIndex = round(($totalHappyIndex/$weekdays),2);   
            }
            // if ($lastDay < date('d')) { //30<31
            // }else{
            //   $totalHappyIndex = round(($totalHappyIndex/$weekdays),2);   
            // }
          }else{
            if (date('Y-m-d',strtotime($year."-".$month."-".$j)) < date('Y-m-d')) { //21<5
              $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, $year."-".$month."-".$j);
              if(!empty($totalHappyIndex) && !empty($weekdays)){
                $finalHappyIndex = round(($totalHappyIndex/$weekdays),2);   
              } 

            }else{
              if (date('Y-m-d',strtotime($year."-".$month."-".$i)) < date('Y-m-d')) { // 15>5
                $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, date('y-m-d',strtotime('-1 day', strtotime($year."-".$month."-".date('d')))));
              }else{
                $weekdays = 0;  
              }
              
              if (!empty($totalHappyIndex) && !empty($weekdays)) {
                $finalHappyIndex = round(($totalHappyIndex/$weekdays),2);   
              }
            }
          }
          $happyIndexData1 = $finalHappyIndex;
        }
        array_push($newArr,$happyIndexData1);
      }
      array_push($arrMain, $newArr);
      $i+=7;
      $j+=7;
    }
    return array('status'=>true,'arrMain'=>$arrMain,'yearMonth'=>$yearMonth,'maxCount'=>$max);
  }

  // function number_of_working_days($from, $to) {
  //   $workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
  //   $holidayDays = ['*-12-25', '*-01-01', '2013-12-23']; # variable and fixed holidays

  //   $from = new DateTime($from);
  //   $to = new DateTime($to);
  //   $to->modify('+1 day');
  //   $interval = new DateInterval('P1D');
  //   $periods = new DatePeriod($from, $interval, $to);

  //   $days = 0;
  //   foreach ($periods as $period) {
  //       if (!in_array($period->format('N'), $workingDays)) continue;
  //       if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
  //       if (in_array($period->format('*-m-d'), $holidayDays)) continue;
  //       $days++;
  //   }
  //   return $days;
  // }

  public function getHappyIndexWeekCat(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $moodId           = Input::get('moodId');
    $columnColor      = Input::get('columnColor');
    $monthNum         = Input::get('monthNum');

    $org = DB::table('organisations')->where('id',$orgId)->first();
    $includeWeekend = $org->include_weekend;

    $arrMain = array();

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }
    $yearMonth    = $months[$monthNum]; //2019-11
    $yearMonthExp = explode("-",$yearMonth);

    $year  = $yearMonthExp[0];
    $month = $yearMonthExp[1];

    $monthCount = cal_days_in_month(CAL_GREGORIAN,$month,$year);

    $weeks = $monthCount%7;

    if ($weeks) {
        $arrayCount = array("Week 1","Week 2","Week 3","Week 4","Week 5");
    }else{
        $arrayCount = array("Week 1","Week 2","Week 3","Week 4");
    }

    $lastDay = date('t',strtotime($year."-".$month."-1"));
    
    if ($moodId) {
      $happyIndexMoodValues = DB::table('happy_index_mood_values')->where('id',$moodId)->get();
    }else{
      $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();
    }

    $moodValue = array("");   //first index
    foreach ($happyIndexMoodValues as $key => $value) {
      $moodValue[] = ucfirst($value->moodName)."(%)";
    }
    array_push($arrMain, $moodValue);

    $i = 1;$j = 7;
    $max = 0;
    foreach ($arrayCount as $key => $value) {
      $newArr = array($value);   //first index of week

      foreach ($happyIndexMoodValues as $moodVal) {

        $happyIndexDataQuery = DB::table('happy_index_dashboard_graph')
          ->where('status','Active')
          ->where('categoryId',$moodVal->id);
        if(!empty($orgId)) {
            $happyIndexDataQuery->where('orgId',$orgId);
        }
        if(empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->where('departmentId',$departmentId);
        } elseif(empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->where('departmentId',$departmentId);
        }
        if($includeWeekend != 1){ //Exclude Weekend Days
          //Get weekends
          // $YearMonth = $month;
          $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($yearMonth,2);
          $happyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
        }
        if ($i==29) {
            $happyIndexDataQuery->whereBetween('happy_index_dashboard_graph.date',[$year."-".$month."-".$i,$year."-".$month."-".$lastDay]);
        }else{
            $happyIndexDataQuery->whereBetween('happy_index_dashboard_graph.date',[$year."-".$month."-".$i,$year."-".$month."-".$j]);
        }
        $happyIndexData = $happyIndexDataQuery->get();
        
        $a = 0;
        $totalCount = 0;
        $totalHappyIndex = 0;

        if(count($happyIndexData)){
          foreach($happyIndexData as $happyIndexRecord){
            if($includeWeekend != 1){ //Exclude Weekend Days
                $totalCount = $happyIndexRecord->without_weekend;
            }else{
                $totalCount = $happyIndexRecord->with_weekend;
            }
            $totalHappyIndex+=$totalCount;
          }
        }

        $happyIndexData1 = 0;
        if (!empty($totalHappyIndex)) {
          $finalHappyIndex = 0;
          if ($i==29) {
            if (date('Y-m-d',strtotime($year."-".$month)) == date('Y-m')) { //21<5
              $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, date('y-m-d',strtotime('-1 day', strtotime($year."-".$month."-".date('d')))));
              // $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, $year."-".$month."-".date('d'));
            }else{
              $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, $year."-".$month."-".$lastDay);
            }            
            $finalHappyIndex = round(($totalHappyIndex/$weekdays),2);   
            // if ($lastDay < date('d')) { //30<31
            // }else{
            //   $totalHappyIndex = round(($totalHappyIndex/$weekdays),2);   
            // }

          }else{
            if (date('Y-m-d',strtotime($year."-".$month."-".$j)) < date('Y-m-d')) { //21<5
              $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, $year."-".$month."-".$j);
              $finalHappyIndex = round(($totalHappyIndex/$weekdays),2);   

            }else{
              if (date('Y-m-d',strtotime($year."-".$month."-".$i)) < date('Y-m-d')) { // 15>5
                $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, date('y-m-d',strtotime('-1 day', strtotime($year."-".$month."-".date('d')))));
              }else{
                $weekdays = 0;  
              }
              
              if (!empty($totalHappyIndex) && !empty($weekdays)) {
                $finalHappyIndex = round(($totalHappyIndex/$weekdays),2);   
              }
            }
          }
          $happyIndexData1 = $finalHappyIndex;
        }
        if ($happyIndexData1 != 0) {
            $max++;
        }
        array_push($newArr,$happyIndexData1);
      }
      array_push($arrMain, $newArr);
      $i+=7;
      $j+=7;
    }
    return array('status'=>true,'arrMain'=>$arrMain,'columnColor'=>$columnColor,'yearMonth'=>$yearMonth,'maxCount'=>$max);
  }

  public function getHappyIndexDaysGraph(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $weekNum          = Input::get('weekNum');  // 0=>'Week 1',1=>'Week 2',2=>'Week 3',3=>'Week 4',4=>'Week 5'
    $yearMonth        = Input::get('yearMonth'); //2019-11
    $index            = Input::get('index'); //1 for main graph, 2 for sub cate graph

    if (!empty($index) && $index == 2) {
      $moodId           = Input::get('moodId');
      $columnColor      = Input::get('columnColor');
    }

    $org = DB::table('organisations')->where('id',$orgId)->first();
    $includeWeekend = $org->include_weekend;

    $arrMain = array();

    $yearMonthExp = explode("-",$yearMonth);

    $year  = $yearMonthExp[0];
    $month = $yearMonthExp[1];
    $monthCount = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    $remainingWeeks = $monthCount%7;

    if ($month == 1) {
        $monthName = "Jan";
    }elseif ($month == 2) {
        $monthName = "Feb";
    }elseif ($month == 3) {
        $monthName = "Mar";
    }elseif ($month == 4) {
        $monthName = "Apr";
    }elseif ($month == 5) {
        $monthName = "May";
    }elseif ($month == 6) {
        $monthName = "Jun";
    }elseif ($month == 7) {
        $monthName = "Jul";
    }elseif ($month == 8) {
        $monthName = "Aug";
    }elseif ($month == 9) {
        $monthName = "Sep";
    }elseif ($month == 10) {
        $monthName = "Oct";
    }elseif ($month == 11) {
        $monthName = "Nov";
    }elseif ($month == 12) {
        $monthName = "Dec";
    }

    if ($weekNum == 0) { // For Week 1
        $i=1;
        $days = array($monthName." 1 ".$year,$monthName." 2 ".$year,$monthName." 3 ".$year,$monthName." 4 ".$year,$monthName." 5 ".$year,$monthName." 6 ".$year,$monthName." 7 ".$year);
    }else if ($weekNum == 1) { // For Week 2
        $i=8;
        $days = array($monthName." 8 ".$year,$monthName." 9 ".$year,$monthName." 10 ".$year,$monthName." 11 ".$year,$monthName." 12 ".$year,$monthName." 13 ".$year,$monthName." 14 ".$year);
    }else if ($weekNum == 2) { // For Week 3
        $i=15;
        $days = array($monthName." 15 ".$year,$monthName." 16 ".$year,$monthName." 17 ".$year,$monthName." 18 ".$year,$monthName." 19 ".$year,$monthName." 20 ".$year,$monthName." 21 ".$year);
    }else if ($weekNum == 3) { // For Week 4
        $i=22;
        $days = array($monthName." 22 ".$year,$monthName." 23 ".$year,$monthName." 24 ".$year,$monthName." 25 ".$year,$monthName." 26 ".$year,$monthName." 27 ".$year,$monthName." 28 ".$year);
    }else if ($weekNum == 4) { // For Week 5
        $i=29;
        if ($remainingWeeks == 1) {
            $days = array($monthName." 29 ".$year);
        }elseif ($remainingWeeks == 2) {
            $days = array($monthName." 29 ".$year,$monthName." 30 ".$year);
        }elseif ($remainingWeeks == 3) {
            $days = array($monthName." 29 ".$year,$monthName." 30 ".$year,$monthName." 31 ".$year);
        }
    }

    if (isset($moodId) && !empty($moodId) && $index == 2) {
      $happyIndexMoodValues = DB::table('happy_index_mood_values')->where('id',$moodId)->get();
    }else{
      $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();
    }
    $moodValue = array("");   //first index
    foreach ($happyIndexMoodValues as $key => $value) {
      $moodValue[] = ucfirst($value->moodName)."(%)";
    }
    array_push($arrMain, $moodValue);

    $max = 0;
    foreach ($days as $key => $value) {
      $newArr = array($value);   //first index of days

      foreach ($happyIndexMoodValues as $moodVal) {

        $happyIndexDataQuery = DB::table('happy_index_dashboard_graph')
          ->where('status','Active')
          ->where('categoryId',$moodVal->id)
          ->whereDate('date',$yearMonth."-".$i);
        if(!empty($orgId)) {
            $happyIndexDataQuery->where('orgId',$orgId);
        }
        if(empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->where('departmentId',$departmentId);
        } elseif(empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->where('departmentId',$departmentId);
        }
        if($includeWeekend != 1){ //Exclude Weekend Days
            //Get weekends
            $YearMonth = $yearMonth;
            $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);
            $happyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
        }
        $happyIndexData = $happyIndexDataQuery->first();   
        if (!empty($happyIndexData)) {
          if($includeWeekend != 1){ //Exclude Weekend Days
            $newArr[] = $happyIndexData->without_weekend;
            if ($happyIndexData->without_weekend != 0) {
                $max++;
            }
          }else{
              $newArr[] = $happyIndexData->with_weekend;
              if ($happyIndexData->with_weekend != 0) {
                  $max++;
              }
          }
        }else{
            $newArr[] = 0;
        }
      }
      // array_push($newArr,$happyIndexData);
    // }
// die();





      // $totalUsersQuery = DB::table('happy_indexes')
      //     ->leftjoin('users','users.id','happy_indexes.userId')
      //     ->where('users.orgId',$orgId)
      //     // ->where('users.status','Active')
      //     ->where('happy_indexes.status','Active')
      //     ->whereDate('happy_indexes.created_at',$yearMonth."-".$i);
      // if(!empty($orgId)){
      //     $totalUsersQuery->where('users.orgId',$orgId);
      // }
      // if(!empty($officeId) && empty($departmentId)) {
      //     $totalUsersQuery->where('users.officeId',$officeId);
      // }elseif(!empty($officeId) && !empty($departmentId)) {
      //     $totalUsersQuery->where('users.officeId',$officeId);
      //     $totalUsersQuery->where('users.departmentId',$departmentId);
      // }elseif(empty($officeId) && !empty($departmentId)) {
      //     $totalUsersQuery->leftJoin('departments','departments.id','users.departmentId')
      //         ->where('departments.status','Active')
      //         ->where('departments.departmentId',$departmentId);
      // }
      // if($includeWeekend != 1){ //Exclude Weekend Days
      //     //Get weekends
      //     $YearMonth = $yearMonth;
      //     $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);
      //     $totalUsersQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      // }
      // $totalUsers = $totalUsersQuery->count();

      // foreach ($happyIndexMoodValues as $mValue) {
      //     $happyUsersWeeklyQuery = DB::table('happy_indexes')
      //         ->select('happy_indexes.created_at')
      //         ->leftjoin('users','users.id','happy_indexes.userId')
      //         ->where('users.orgId',$orgId)
      //         // ->where('users.status','Active')
      //         ->where('happy_indexes.status','Active')
      //         ->where('happy_indexes.moodValue',$mValue->id)
      //         ->whereDate('happy_indexes.created_at',$yearMonth."-".$i);
      //     if(!empty($orgId)){
      //         $happyUsersWeeklyQuery->where('users.orgId',$orgId);
      //     }
      //     if(!empty($officeId) && empty($departmentId)) {
      //         $happyUsersWeeklyQuery->where('users.officeId',$officeId);
      //     }elseif(!empty($officeId) && !empty($departmentId)) {
      //         $happyUsersWeeklyQuery->where('users.officeId',$officeId);
      //         $happyUsersWeeklyQuery->where('users.departmentId',$departmentId);
      //     }elseif(empty($officeId) && !empty($departmentId)) {
      //         $happyUsersWeeklyQuery->leftJoin('departments','departments.id','users.departmentId')
      //             ->where('departments.status','Active')
      //             ->where('departments.departmentId',$departmentId);
      //     }
      //     if($includeWeekend != 1){ //Exclude Weekend Days
      //         //Get weekends
      //         $YearMonth = $yearMonth;
      //         $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);
      //         $happyUsersWeeklyQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      //     }
      //     $moodCount = $happyUsersWeeklyQuery->count();

      //     $moodCountUsers = 0;
      //     if (!empty($moodCount) && !empty($totalUsers)) {
      //         $moodCountUsers = round((($moodCount/$totalUsers)*100),2);
      //     }
      //     array_push($newArr, $moodCountUsers);
      // }
      array_push($arrMain, $newArr);
      $i++;
    }

    if (!empty($index) && $index==1) {
      return array('status'=>true,'arrMain'=>$arrMain,'maxCount'=>$max);
    }else if(!empty($index) && $index==2) {
      return array('status'=>true,'arrMain'=>$arrMain,'columnColor'=>$columnColor,'maxCount'=>$max);
    }
  }

  //Happy index responders month graph
  public function getHappyIndexRespValues($perArr=array()) {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend = $perArr["includeWeekend"];
    $months         = $perArr["months"];
    $ispdf          = $perArr["ispdf"];

    $arrMain   = array();
    $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();
    $moodValue = array("","User Count","Good/Bad Day Index Responders");   //first index
    // foreach ($happyIndexMoodValues as $key => $value) {
    //   $moodValue[$value->id] = ucfirst($value->moodName)."(%)";
    // }
    array_push($arrMain, $moodValue);
    
    $max = 0;
    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));   //first index of month
      //Get weekends
      $YearMonth = $month;
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);

      $totalCount = 0;
      foreach ($happyIndexMoodValues as $moodVal) {
        $happyIndexDataQuery = DB::table('happy_index_dashboard_responders_graph')
          ->where('status','Active')
          ->where('categoryId',$moodVal->id)
          ->where('date','LIKE',$month."%")
          ->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_responders_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
        if(!empty($orgId)) {
            $happyIndexDataQuery->where('orgId',$orgId);
        }
        if(empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->where('departmentId',$departmentId);
        } elseif(empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->where('departmentId',$departmentId);
        }
        // if($includeWeekend != 1){ //Exclude Weekend Days
        //   //Get weekends
        //   $YearMonth = $month;
        //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);
        //   $happyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_responders_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
        // }
        if($includeWeekend != 1){ //Exclude Weekend Days
          $happyIndexData = $happyIndexDataQuery->sum('without_weekend');
        }else{
          $happyIndexData = $happyIndexDataQuery->sum('with_weekend');
        }
        $totalCount += $happyIndexData; 
      }

      if ($month != date('Y-m')) {
        $yearMonth = explode('-', $month);
        $weekdaysCount = 0;
        if ($yearMonth) {  
          $currentYear = $yearMonth[0]; 
          $currentMonth = $yearMonth[1]; 
          // $weekdaysCount = $this->countDays($currentYear, $currentMonth, array(0, 6)); //21
          $weekdaysCount = app('App\Http\Controllers\Admin\CommonController')->countDays($currentYear, $currentMonth, array(0, 6)); //21
        }
      }else{
        $start = new DateTime(date('Y-m-01'));
        $end = new DateTime(date('Y-m-d',strtotime('-1 days')));
        // otherwise the  end date is excluded (bug?)
        $end->modify('+1 day');
        $interval = $end->diff($start);
        // total days
        $days = $interval->days;
        // create an iterateable period of date (P1D equates to 1 day)
        $period = new DatePeriod($start, new DateInterval('P1D'), $end);
        // best stored as array, so you can add more than one
        $holidays = [];
        foreach($period as $dt) {
            $curr = $dt->format('D');
            // substract if Saturday or Sunday
            if ($curr == 'Sat' || $curr == 'Sun') {
                $days--;
            }
            // (optional) for the updated question
            elseif (in_array($dt->format('Y-m-d'), $holidays)) {
                $days--;
            }
        }
        $weekdaysCount  = $days;
      }

      //Avg happy index count
      $avgCount = 0;
      if (!empty($totalCount) && !empty($weekdaysCount)) {
        $avgCount = round(($totalCount/$weekdaysCount),2);
      }

      $userCountQuery = DB::table('users_daily_count')
        ->where('status','Active')
        ->where('date','LIKE',$month."%")
        ->whereNotIn(DB::raw("(DATE_FORMAT(users_daily_count.date,'%Y-%m-%d'))"),$getWeekendDates);
      if(!empty($orgId)) {
          $userCountQuery->where('orgId',$orgId);
      }
      if(empty($officeId) && empty($departmentId)) {
        $userCountQuery->whereNull('officeId');
        $userCountQuery->whereNull('departmentId');
      } elseif(!empty($officeId) && empty($departmentId)) {
        $userCountQuery->where('officeId',$officeId);
        $userCountQuery->whereNull('departmentId');
      } elseif(!empty($officeId) && !empty($departmentId)) {
        $userCountQuery->where('officeId',$officeId);
        $userCountQuery->where('departmentId',$departmentId);
      } elseif(empty($officeId) && !empty($departmentId)) {
        $userCountQuery->whereNull('officeId');
        $userCountQuery->where('departmentId',$departmentId);
      }
      // if($includeWeekend != 1){ //Exclude Weekend Days
      //   //Get weekends
      //   $YearMonth = $month;
      //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);
      //   $userCountQuery->whereNotIn(DB::raw("(DATE_FORMAT(users_daily_count.date,'%Y-%m-%d'))"),$getWeekendDates);
      // }
      $userCount = $userCountQuery->sum('totalCount');
      //Avg user count
      $avgUserCount = 0;
      if (!empty($userCount) && !empty($weekdaysCount)) {
        $avgUserCount = round(($userCount/$weekdaysCount),2);
      }

      if ($avgCount != 0 || $avgUserCount != 0) {
        $max++;
      }
    
      array_push($newArr,$avgUserCount);
      array_push($newArr,$avgCount);
      array_push($arrMain, $newArr);
    }

    return array('arrMain'=>$arrMain,'maxCount'=>$max);
  }

  //Happy index reponders month graph for single category
  public function getHappyIndexResCat(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $moodId           = Input::get('moodId');
    $columnColor      = Input::get('columnColor');

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }

    $org = DB::table('organisations')->where('id',$orgId)->first();
    $includeWeekend = $org->include_weekend;

    $arrMain = array();
    
    $arrMain   = array();
    $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();
    if (!empty($moodId) && $moodId == 1) {
      $moodValue = array("","User Count");   //first index
    }elseif (!empty($moodId) && $moodId == 2) {
      $moodValue = array("","Good/Bad Day Index Responders");   //first index
    }else{
      $moodValue = array("","User Count","Good/Bad Day Index Responders");   //first index
    }
    array_push($arrMain, $moodValue);

    $max = 0;
    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));   //first index of month
      //Get weekends
      $YearMonth = $month;
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);

      if ($month != date('Y-m')) {
        $yearMonth = explode('-', $month);
        $weekdaysCount = 0;
        if ($yearMonth) {  
          $currentYear = $yearMonth[0]; 
          $currentMonth = $yearMonth[1]; 
          // $weekdaysCount = $this->countDays($currentYear, $currentMonth, array(0, 6)); //21
          $weekdaysCount = app('App\Http\Controllers\Admin\CommonController')->countDays($currentYear, $currentMonth, array(0, 6)); //21
        }
      }else{
        $start = new DateTime(date('Y-m-01'));
        $end = new DateTime(date('Y-m-d',strtotime('-1 days')));
        // otherwise the  end date is excluded (bug?)
        $end->modify('+1 day');
        $interval = $end->diff($start);
        // total days
        $days = $interval->days;
        // create an iterateable period of date (P1D equates to 1 day)
        $period = new DatePeriod($start, new DateInterval('P1D'), $end);
        // best stored as array, so you can add more than one
        $holidays = [];
        foreach($period as $dt) {
            $curr = $dt->format('D');
            // substract if Saturday or Sunday
            if ($curr == 'Sat' || $curr == 'Sun') {
                $days--;
            }
            // (optional) for the updated question
            elseif (in_array($dt->format('Y-m-d'), $holidays)) {
                $days--;
            }
        }
        $weekdaysCount  = $days;
      }

      if ((!empty($moodId) && $moodId == 1) || empty($moodId)) {
        $userCountQuery = DB::table('users_daily_count')
          ->where('status','Active')
          ->where('date','LIKE',$month."%")
          ->whereNotIn(DB::raw("(DATE_FORMAT(users_daily_count.date,'%Y-%m-%d'))"),$getWeekendDates);
        if(!empty($orgId)) {
            $userCountQuery->where('orgId',$orgId);
        }
        if(empty($officeId) && empty($departmentId)) {
          $userCountQuery->whereNull('officeId');
          $userCountQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && empty($departmentId)) {
          $userCountQuery->where('officeId',$officeId);
          $userCountQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && !empty($departmentId)) {
          $userCountQuery->where('officeId',$officeId);
          $userCountQuery->where('departmentId',$departmentId);
        } elseif(empty($officeId) && !empty($departmentId)) {
          $userCountQuery->whereNull('officeId');
          $userCountQuery->where('departmentId',$departmentId);
        }
        // if($includeWeekend != 1){ //Exclude Weekend Days
        //   //Get weekends
        //   $YearMonth = $month;
        //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);
        //   $userCountQuery->whereNotIn(DB::raw("(DATE_FORMAT(users_daily_count.date,'%Y-%m-%d'))"),$getWeekendDates);
        // }
        $userCount = $userCountQuery->sum('totalCount');
        //Avg user count
        $avgUserCount = 0;
        if (!empty($userCount) && !empty($weekdaysCount)) {
          $avgUserCount = round(($userCount/$weekdaysCount),2);
        }
        if ($avgUserCount != 0) {
          $max++;
        }
        array_push($newArr,$avgUserCount);
      }
      if ((!empty($moodId) && $moodId == 2) || empty($moodId)) {
        $totalCount = 0;
        foreach ($happyIndexMoodValues as $moodVal) {
          $happyIndexDataQuery = DB::table('happy_index_dashboard_responders_graph')
            ->where('status','Active')
            ->where('categoryId',$moodVal->id)
            ->where('date','LIKE',$month."%")
            ->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_responders_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
          if(!empty($orgId)) {
              $happyIndexDataQuery->where('orgId',$orgId);
          }
          if(empty($officeId) && empty($departmentId)) {
            $happyIndexDataQuery->whereNull('officeId');
            $happyIndexDataQuery->whereNull('departmentId');
          } elseif(!empty($officeId) && empty($departmentId)) {
            $happyIndexDataQuery->where('officeId',$officeId);
            $happyIndexDataQuery->whereNull('departmentId');
          } elseif(!empty($officeId) && !empty($departmentId)) {
            $happyIndexDataQuery->where('officeId',$officeId);
            $happyIndexDataQuery->where('departmentId',$departmentId);
          } elseif(empty($officeId) && !empty($departmentId)) {
            $happyIndexDataQuery->whereNull('officeId');
            $happyIndexDataQuery->where('departmentId',$departmentId);
          }
          // if($includeWeekend != 1){ //Exclude Weekend Days
          //   //Get weekends
          //   $YearMonth = $month;
          //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);
          //   $happyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_responders_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
          // }
          if($includeWeekend != 1){ //Exclude Weekend Days
            $happyIndexData = $happyIndexDataQuery->sum('without_weekend');
          }else{
            $happyIndexData = $happyIndexDataQuery->sum('with_weekend');
          }
          $totalCount += $happyIndexData; 
        }
        //Avg happy index count
        $avgCount = 0;
        if (!empty($totalCount) && !empty($weekdaysCount)) {
          $avgCount = round(($totalCount/$weekdaysCount),2);
        }
        if ($avgCount != 0) {
          $max++;
        }
        array_push($newArr,$avgCount);
      }
      array_push($arrMain, $newArr);
    }
    return array('status'=>true,'arrMain'=>$arrMain,'columnColor'=>$columnColor,'maxCount'=>$max);
  }

  //Happy index responders week graph
  public function getHappyIndexRespWeekGraph(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $monthNum         = Input::get('monthNum');

    $org = DB::table('organisations')->where('id',$orgId)->first();
    $includeWeekend = $org->include_weekend;

    $arrMain = array();

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }
    $yearMonth    = $months[$monthNum]; //2019-11
    $yearMonthExp = explode("-",$yearMonth);

    $year  = $yearMonthExp[0];
    $month = $yearMonthExp[1];

    $monthCount = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    //echo $totalDays;die;
    $weeks = $monthCount%7;

    if ($weeks) {
        $arrayCount = array("Week 1","Week 2","Week 3","Week 4","Week 5");
    }else{
        $arrayCount = array("Week 1","Week 2","Week 3","Week 4");
    }

    $lastDay = date('t',strtotime($year."-".$month."-1"));

    $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();
    $moodValue = array("","User Count","Good/Bad Day Index Responders");   //first index
    // $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();
    // $moodValue = array("");   //first index
    // foreach ($happyIndexMoodValues as $key => $value) {
    //   $moodValue[] = ucfirst($value->moodName)."(%)";
    // }
    array_push($arrMain, $moodValue);

    $i = 1;$j = 7;
    $max = 0;
    foreach ($arrayCount as $key => $value) {
      $newArr = array($value);   //first index of week
      //Get weekends
      // $YearMonth = $month;
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($yearMonth,2);

      $totalCount = 0;
      foreach ($happyIndexMoodValues as $moodVal) {
        $happyIndexDataQuery = DB::table('happy_index_dashboard_responders_graph')
          ->where('status','Active')
          ->where('categoryId',$moodVal->id)
          ->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_responders_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
        if(!empty($orgId)) {
            $happyIndexDataQuery->where('orgId',$orgId);
        }
        if(empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->where('departmentId',$departmentId);
        } elseif(empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->where('departmentId',$departmentId);
        }
        // if($includeWeekend != 1){ //Exclude Weekend Days
        //   //Get weekends
        //   // $YearMonth = $month;
        //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($yearMonth,2);
        //   $happyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_responders_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
        // }
        if ($i==29) {
            $happyIndexDataQuery->whereBetween('happy_index_dashboard_responders_graph.date',[$year."-".$month."-".$i,$year."-".$month."-".$lastDay]);
        }else{
            $happyIndexDataQuery->whereBetween('happy_index_dashboard_responders_graph.date',[$year."-".$month."-".$i,$year."-".$month."-".$j]);
        }
        if($includeWeekend != 1){ //Exclude Weekend Days
          $happyIndexData = $happyIndexDataQuery->sum('without_weekend');
        }else{
          $happyIndexData = $happyIndexDataQuery->sum('with_weekend');
        }
        $totalCount += $happyIndexData; 
      }

      $weekdays = 0;  
      // if (!empty($totalCount)) {
        if ($i==29) {
          if (date('Y-m-d',strtotime($year."-".$month)) == date('Y-m')) { //21<5
              $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, date('y-m-d',strtotime('-1 day', strtotime($year."-".$month."-".date('d')))));
              // $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, $year."-".$month."-".date('d'));
            }else{
              $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, $year."-".$month."-".$lastDay);
            }  
        }else{
          if (date('Y-m-d',strtotime($year."-".$month."-".$j)) < date('Y-m-d')) { //21<5
            $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, $year."-".$month."-".$j);
          }else{
            if (date('Y-m-d',strtotime($year."-".$month."-".$i)) < date('Y-m-d')) { // 15>5
              $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, date('y-m-d',strtotime('-1 day', strtotime($year."-".$month."-".date('d')))));
            }else{
              $weekdays = 0;  
            }
          }
        }
      // }

      //Avg happy index count
      $avgCount = 0;
      if (!empty($totalCount) && !empty($weekdays)) {
        $avgCount = round(($totalCount/$weekdays),2);
        if ($avgCount != 0) {
            $max++;
        }
      }

      $userCountQuery = DB::table('users_daily_count')
        ->where('status','Active')
        ->whereNotIn(DB::raw("(DATE_FORMAT(users_daily_count.date,'%Y-%m-%d'))"),$getWeekendDates);
      if(!empty($orgId)) {
          $userCountQuery->where('orgId',$orgId);
      }
      if(empty($officeId) && empty($departmentId)) {
        $userCountQuery->whereNull('officeId');
        $userCountQuery->whereNull('departmentId');
      } elseif(!empty($officeId) && empty($departmentId)) {
        $userCountQuery->where('officeId',$officeId);
        $userCountQuery->whereNull('departmentId');
      } elseif(!empty($officeId) && !empty($departmentId)) {
        $userCountQuery->where('officeId',$officeId);
        $userCountQuery->where('departmentId',$departmentId);
      } elseif(empty($officeId) && !empty($departmentId)) {
        $userCountQuery->whereNull('officeId');
        $userCountQuery->where('departmentId',$departmentId);
      }
      // if($includeWeekend != 1){ //Exclude Weekend Days
      //   //Get weekends
      //   // $YearMonth = $month;
      //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($yearMonth,2);
      //   $userCountQuery->whereNotIn(DB::raw("(DATE_FORMAT(users_daily_count.date,'%Y-%m-%d'))"),$getWeekendDates);
      // }
      if ($i==29) {
          $userCountQuery->whereBetween('users_daily_count.date',[$year."-".$month."-".$i,$year."-".$month."-".$lastDay]);
      }else{
          $userCountQuery->whereBetween('users_daily_count.date',[$year."-".$month."-".$i,$year."-".$month."-".$j]);
      }
      $userCount = $userCountQuery->sum('totalCount');
      //Avg user count
      $avgUserCount = 0;
      if (!empty($userCount) && !empty($weekdays)) {
        $avgUserCount = round(($userCount/$weekdays),2);
        if ($avgUserCount != 0) {
            $max++;
        }
      }

      array_push($newArr,$avgUserCount);
      array_push($newArr,$avgCount);
      array_push($arrMain, $newArr);

      // array_push($newArr,$totalCount);
      // array_push($arrMain, $newArr);
      $i+=7;
      $j+=7;
    }
    return array('status'=>true,'arrMain'=>$arrMain,'yearMonth'=>$yearMonth,'maxCount'=>$max);
  }

   //Happy index responders week graph for single category
  public function getHappyIndexRespWeekCat(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $moodId           = Input::get('moodId');
    $columnColor      = Input::get('columnColor');
    $monthNum         = Input::get('monthNum');

    $org = DB::table('organisations')->where('id',$orgId)->first();
    $includeWeekend = $org->include_weekend;

    $arrMain = array();

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }
    $yearMonth    = $months[$monthNum]; //2019-11
    $yearMonthExp = explode("-",$yearMonth);

    $year  = $yearMonthExp[0];
    $month = $yearMonthExp[1];

    $monthCount = cal_days_in_month(CAL_GREGORIAN,$month,$year);

    $weeks = $monthCount%7;

    if ($weeks) {
        $arrayCount = array("Week 1","Week 2","Week 3","Week 4","Week 5");
    }else{
        $arrayCount = array("Week 1","Week 2","Week 3","Week 4");
    }

    $lastDay = date('t',strtotime($year."-".$month."-1"));
    
    $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();
    if (!empty($moodId) && $moodId == 1) {
      $moodValue = array("","User Count");   //first index
    }elseif (!empty($moodId) && $moodId == 2) {
      $moodValue = array("","Good/Bad Day Index Responders");   //first index
    }else{
      $moodValue = array("","User Count","Good/Bad Day Index Responders");   //first index
    }
    array_push($arrMain, $moodValue);

    $i = 1;$j = 7;
    $max = 0;
    foreach ($arrayCount as $key => $value) {
      $newArr = array($value);   //first index of week
      //Get weekends
      // $YearMonth = $month;
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($yearMonth,2);

      $weekdays = 0;  
      if ($i==29) {
        if (date('Y-m-d',strtotime($year."-".$month)) == date('Y-m')) { //21<5
            $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, date('y-m-d',strtotime('-1 day', strtotime($year."-".$month."-".date('d')))));
            // $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, $year."-".$month."-".date('d'));
          }else{
            $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, $year."-".$month."-".$lastDay);
          }  
      }else{
        if (date('Y-m-d',strtotime($year."-".$month."-".$j)) < date('Y-m-d')) { //21<5
          $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, $year."-".$month."-".$j);
        }else{
          if (date('Y-m-d',strtotime($year."-".$month."-".$i)) < date('Y-m-d')) { // 15>5
            $weekdays = app('App\Http\Controllers\Admin\CommonController')->number_of_working_days($year."-".$month."-".$i, date('y-m-d',strtotime('-1 day', strtotime($year."-".$month."-".date('d')))));
          }else{
            $weekdays = 0;  
          }
        }
      }

      if ((!empty($moodId) && $moodId == 1) || empty($moodId)) {
        $userCountQuery = DB::table('users_daily_count')
          ->where('status','Active')
          ->whereNotIn(DB::raw("(DATE_FORMAT(users_daily_count.date,'%Y-%m-%d'))"),$getWeekendDates);
        if(!empty($orgId)) {
            $userCountQuery->where('orgId',$orgId);
        }
        if(empty($officeId) && empty($departmentId)) {
          $userCountQuery->whereNull('officeId');
          $userCountQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && empty($departmentId)) {
          $userCountQuery->where('officeId',$officeId);
          $userCountQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && !empty($departmentId)) {
          $userCountQuery->where('officeId',$officeId);
          $userCountQuery->where('departmentId',$departmentId);
        } elseif(empty($officeId) && !empty($departmentId)) {
          $userCountQuery->whereNull('officeId');
          $userCountQuery->where('departmentId',$departmentId);
        }
        // if($includeWeekend != 1){ //Exclude Weekend Days
        //   //Get weekends
        //   // $YearMonth = $month;
        //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($yearMonth,2);
        //   $userCountQuery->whereNotIn(DB::raw("(DATE_FORMAT(users_daily_count.date,'%Y-%m-%d'))"),$getWeekendDates);
        // }
        if ($i==29) {
            $userCountQuery->whereBetween('users_daily_count.date',[$year."-".$month."-".$i,$year."-".$month."-".$lastDay]);
        }else{
            $userCountQuery->whereBetween('users_daily_count.date',[$year."-".$month."-".$i,$year."-".$month."-".$j]);
        }
        $userCount = $userCountQuery->sum('totalCount');
        //Avg user count
        $avgUserCount = 0;
        if (!empty($userCount) && !empty($weekdays)) {
          $avgUserCount = round(($userCount/$weekdays),2);
        }
        if ($avgUserCount != 0) {
          $max++;
        }
        array_push($newArr,$avgUserCount);
      }
      if ((!empty($moodId) && $moodId == 2) || empty($moodId)) {
        $totalCount = 0;
        foreach ($happyIndexMoodValues as $moodVal) {

          $happyIndexDataQuery = DB::table('happy_index_dashboard_responders_graph')
            ->where('status','Active')
            ->where('categoryId',$moodVal->id)
            ->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_responders_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
          if(!empty($orgId)) {
              $happyIndexDataQuery->where('orgId',$orgId);
          }
          if(empty($officeId) && empty($departmentId)) {
            $happyIndexDataQuery->whereNull('officeId');
            $happyIndexDataQuery->whereNull('departmentId');
          } elseif(!empty($officeId) && empty($departmentId)) {
            $happyIndexDataQuery->where('officeId',$officeId);
            $happyIndexDataQuery->whereNull('departmentId');
          } elseif(!empty($officeId) && !empty($departmentId)) {
            $happyIndexDataQuery->where('officeId',$officeId);
            $happyIndexDataQuery->where('departmentId',$departmentId);
          } elseif(empty($officeId) && !empty($departmentId)) {
            $happyIndexDataQuery->whereNull('officeId');
            $happyIndexDataQuery->where('departmentId',$departmentId);
          }
          // if($includeWeekend != 1){ //Exclude Weekend Days
          //   //Get weekends
          //   // $YearMonth = $month;
          //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($yearMonth,2);
          //   $happyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_responders_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
          // }
          if ($i==29) {
              $happyIndexDataQuery->whereBetween('happy_index_dashboard_responders_graph.date',[$year."-".$month."-".$i,$year."-".$month."-".$lastDay]);
          }else{
              $happyIndexDataQuery->whereBetween('happy_index_dashboard_responders_graph.date',[$year."-".$month."-".$i,$year."-".$month."-".$j]);
          }
          if($includeWeekend != 1){ //Exclude Weekend Days
            $happyIndexData = $happyIndexDataQuery->sum('without_weekend');
          }else{
            $happyIndexData = $happyIndexDataQuery->sum('with_weekend');
          }
          $totalCount += $happyIndexData; 
        }

        //Avg happy index count
        $avgCount = 0;
        if (!empty($totalCount) && !empty($weekdays)) {
          $avgCount = round(($totalCount/$weekdays),2);
        }
        if ($avgCount != 0) {
          $max++;
        }
        array_push($newArr,$avgCount);
      }
      array_push($arrMain, $newArr);

      $i+=7;
      $j+=7;
    }
    return array('status'=>true,'arrMain'=>$arrMain,'columnColor'=>$columnColor,'yearMonth'=>$yearMonth,'maxCount'=>$max);
  }

  //Happy index responders days graph
  public function getHappyIndexRespDaysGraph(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $weekNum          = Input::get('weekNum');  // 0=>'Week 1',1=>'Week 2',2=>'Week 3',3=>'Week 4',4=>'Week 5'
    $yearMonth        = Input::get('yearMonth'); //2019-11
    $index            = Input::get('index'); //1 for main graph, 2 for sub cate graph

    if (!empty($index) && $index == 2) {
      $moodId           = Input::get('moodId');
      $columnColor      = Input::get('columnColor');
    }

    $org = DB::table('organisations')->where('id',$orgId)->first();
    $includeWeekend = $org->include_weekend;

    $arrMain = array();

    $yearMonthExp = explode("-",$yearMonth);

    $year  = $yearMonthExp[0];
    $month = $yearMonthExp[1];
    $monthCount = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    $remainingWeeks = $monthCount%7;

    if ($month == 1) {
        $monthName = "Jan";
    }elseif ($month == 2) {
        $monthName = "Feb";
    }elseif ($month == 3) {
        $monthName = "Mar";
    }elseif ($month == 4) {
        $monthName = "Apr";
    }elseif ($month == 5) {
        $monthName = "May";
    }elseif ($month == 6) {
        $monthName = "Jun";
    }elseif ($month == 7) {
        $monthName = "Jul";
    }elseif ($month == 8) {
        $monthName = "Aug";
    }elseif ($month == 9) {
        $monthName = "Sep";
    }elseif ($month == 10) {
        $monthName = "Oct";
    }elseif ($month == 11) {
        $monthName = "Nov";
    }elseif ($month == 12) {
        $monthName = "Dec";
    }

    if ($weekNum == 0) { // For Week 1
        $i=1;
        $days = array($monthName." 1 ".$year,$monthName." 2 ".$year,$monthName." 3 ".$year,$monthName." 4 ".$year,$monthName." 5 ".$year,$monthName." 6 ".$year,$monthName." 7 ".$year);
    }else if ($weekNum == 1) { // For Week 2
        $i=8;
        $days = array($monthName." 8 ".$year,$monthName." 9 ".$year,$monthName." 10 ".$year,$monthName." 11 ".$year,$monthName." 12 ".$year,$monthName." 13 ".$year,$monthName." 14 ".$year);
    }else if ($weekNum == 2) { // For Week 3
        $i=15;
        $days = array($monthName." 15 ".$year,$monthName." 16 ".$year,$monthName." 17 ".$year,$monthName." 18 ".$year,$monthName." 19 ".$year,$monthName." 20 ".$year,$monthName." 21 ".$year);
    }else if ($weekNum == 3) { // For Week 4
        $i=22;
        $days = array($monthName." 22 ".$year,$monthName." 23 ".$year,$monthName." 24 ".$year,$monthName." 25 ".$year,$monthName." 26 ".$year,$monthName." 27 ".$year,$monthName." 28 ".$year);
    }else if ($weekNum == 4) { // For Week 5
        $i=29;
        if ($remainingWeeks == 1) {
            $days = array($monthName." 29 ".$year);
        }elseif ($remainingWeeks == 2) {
            $days = array($monthName." 29 ".$year,$monthName." 30 ".$year);
        }elseif ($remainingWeeks == 3) {
            $days = array($monthName." 29 ".$year,$monthName." 30 ".$year,$monthName." 31 ".$year);
        }
    }

    $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();
    if (!empty($moodId) && $moodId == 1) {
      $moodValue = array("","User Count");   //first index
    }elseif (!empty($moodId) && $moodId == 2) {
      $moodValue = array("","Good/Bad Day Index Responders");   //first index
    }else{
      $moodValue = array("","User Count","Good/Bad Day Index Responders");   //first index
    }
    // $moodValue = array("","User Count","Happy Index Responders");   //first index
    // foreach ($happyIndexMoodValues as $key => $value) {
    //   $moodValue[] = ucfirst($value->moodName)."(%)";
    // }
    array_push($arrMain, $moodValue);

    $max = 0;
    foreach ($days as $key => $value) {
      $newArr = array($value);   //first index of days

      $totalCount = 0;
      foreach ($happyIndexMoodValues as $moodVal) {
        $happyIndexDataQuery = DB::table('happy_index_dashboard_responders_graph')
          ->where('status','Active')
          ->where('categoryId',$moodVal->id)
          ->whereDate('date',$yearMonth."-".$i);
        if(!empty($orgId)) {
            $happyIndexDataQuery->where('orgId',$orgId);
        }
        if(empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->where('departmentId',$departmentId);
        } elseif(empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->where('departmentId',$departmentId);
        }
        // if($includeWeekend != 1){ //Exclude Weekend Days
        //     //Get weekends
        //     $YearMonth = $yearMonth;
        //     $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);
        //     $happyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_responders_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
        // }
        if($includeWeekend != 1){ //Exclude Weekend Days
          $happyIndexData = $happyIndexDataQuery->sum('without_weekend');
        }else{
          $happyIndexData = $happyIndexDataQuery->sum('with_weekend');
        }
        $totalCount += $happyIndexData; 
      }

      $userCountQuery = DB::table('users_daily_count')
        ->where('status','Active')
        ->whereDate('date',$yearMonth."-".$i);
      if(!empty($orgId)) {
          $userCountQuery->where('orgId',$orgId);
      }
      if(empty($officeId) && empty($departmentId)) {
        $userCountQuery->whereNull('officeId');
        $userCountQuery->whereNull('departmentId');
      } elseif(!empty($officeId) && empty($departmentId)) {
        $userCountQuery->where('officeId',$officeId);
        $userCountQuery->whereNull('departmentId');
      } elseif(!empty($officeId) && !empty($departmentId)) {
        $userCountQuery->where('officeId',$officeId);
        $userCountQuery->where('departmentId',$departmentId);
      } elseif(empty($officeId) && !empty($departmentId)) {
        $userCountQuery->whereNull('officeId');
        $userCountQuery->where('departmentId',$departmentId);
      }
      // if($includeWeekend != 1){ //Exclude Weekend Days
      //     //Get weekends
      //     $YearMonth = $yearMonth;
      //     $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);
      //     $userCountQuery->whereNotIn(DB::raw("(DATE_FORMAT(users_daily_count.date,'%Y-%m-%d'))"),$getWeekendDates);
      // }
      $userCount = $userCountQuery->sum('totalCount');

      if (!empty($moodId) && $moodId == 1) {
        if ($userCount != 0) {
          $max++;
        }
        array_push($newArr, $userCount);
      }elseif (!empty($moodId) && $moodId == 2) {
        if ($totalCount != 0) {
          $max++;
        }
        array_push($newArr, $totalCount);
      }else{
        if ($totalCount != 0 || $userCount != 0) {
          $max++;
        }
        array_push($newArr, $userCount);
        array_push($newArr, $totalCount);
      }
      $i++;
      array_push($arrMain, $newArr);
    }
    // echo "<pre>";print_r($arrMain);die();
    if (!empty($index) && $index==1) {
      return array('status'=>true,'arrMain'=>$arrMain,'maxCount'=>$max);
    }else if(!empty($index) && $index==2) {
      return array('status'=>true,'arrMain'=>$arrMain,'columnColor'=>$columnColor,'maxCount'=>$max);
    }
  }

  // public function getHappyIndexValues($perArr = array()){
  //   $orgId          = $perArr["orgId"];
  //   $officeId       = $perArr["officeId"];
  //   $departmentId   = $perArr["departmentId"];
  //   $includeWeekend = $perArr["includeWeekend"];
  //   $months         = $perArr["months"];
  //   $ispdf          = $perArr["ispdf"];

  //   $arrMain   = array();
  //   $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();
  //   $moodValue = array("");   //first index
  //   foreach ($happyIndexMoodValues as $key => $value) {
  //     $moodValue[$value->id] = ucfirst($value->moodName)."(%)";
  //   }
  //   array_push($arrMain, $moodValue);
    
  //   $happyIndexCountMonth = array();
  //   foreach ($months as $key => $month) {
  //     $newArr = array(date('My',strtotime($month)));   //first index of month

  //     $totalUsersQuery = DB::table('happy_indexes')
  //     ->leftjoin('users','users.id','happy_indexes.userId')
  //     // ->where('users.status','Active')
  //     ->where('happy_indexes.status','Active')
  //     ->where('happy_indexes.created_at','LIKE',$month.'%');
  //     if(!empty($orgId)){
  //         $totalUsersQuery->where('users.orgId',$orgId);
  //     }
  //     if(!empty($officeId) && empty($departmentId)) {
  //         $totalUsersQuery->where('users.officeId',$officeId);
  //     }elseif(!empty($officeId) && !empty($departmentId)) {
  //         $totalUsersQuery->where('users.officeId',$officeId);
  //         $totalUsersQuery->where('users.departmentId',$departmentId);
  //     }elseif(empty($officeId) && !empty($departmentId)) {
  //         $totalUsersQuery->leftJoin('departments','departments.id','users.departmentId')
  //             ->where('departments.status','Active')
  //             ->where('departments.departmentId',$departmentId);
  //     }
  //     if($includeWeekend != 1){ //Exclude Weekend Days
  //       //Get weekends
  //       $YearMonth = $month;
  //       $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);

  //       $totalUsersQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //     }
  //     $totalUsers = $totalUsersQuery->count();

  //     foreach ($happyIndexMoodValues as $moodVal) {
  //       $monthCountQuery = DB::table('happy_indexes')
  //           ->leftjoin('users','users.id','happy_indexes.userId')
  //           // ->where('users.status','Active')
  //           ->where('happy_indexes.status','Active')
  //           ->where('happy_indexes.moodValue',$moodVal->id)
  //           ->where('happy_indexes.created_at','LIKE',$month.'%');
  //       if(!empty($orgId)){
  //           $monthCountQuery->where('users.orgId',$orgId);
  //       }
  //       if(!empty($officeId) && empty($departmentId)){
  //         $monthCountQuery->where('users.officeId',$officeId);
  //       }elseif(!empty($officeId) && !empty($departmentId)){
  //         $monthCountQuery->where('users.officeId',$officeId);
  //         $monthCountQuery->where('users.departmentId',$departmentId);
  //       }elseif(empty($officeId) && !empty($departmentId)){
  //         $monthCountQuery->leftJoin('departments','departments.id','users.departmentId')
  //             ->where('departments.status','Active')
  //             ->where('departments.departmentId',$departmentId);
  //       }
  //       if($includeWeekend != 1){ //Exclude Weekend Days
  //         //Get weekends
  //         $YearMonth = $month;
  //         $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);

  //         $monthCountQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //       }
  //       $moodCount = $monthCountQuery->count();

  //       $moodCountUsers = 0;
  //       if (!empty($moodCount) && !empty($totalUsers)) {
  //         $moodCountUsers = round((($moodCount/$totalUsers)*100),2);
  //       }
  //       array_push($newArr,$moodCountUsers);
  //     }
  //     array_push($arrMain, $newArr);
  //   }
  //   //print_r($arrMain);die;
  //   return $arrMain;
  // }

  // public function getHappyIndexCat(Request $request)
  // {
  //   $orgId            = Input::get('orgId');
  //   $officeId         = Input::get('officeId');
  //   $departmentId     = Input::get('departmentId');
  //   $moodId           = Input::get('moodId');
  //   $columnColor      = Input::get('columnColor');

  //   for ($i = 11; $i >= 0; $i--) {
  //     $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
  //   }

  //   $org = DB::table('organisations')->where('id',$orgId)->first();
  //   $includeWeekend = $org->include_weekend;

  //   $arrMain = array();
  //   if ($moodId) {
  //     $happyIndexMoodValues = DB::table('happy_index_mood_values')->where('id',$moodId)->get();
  //   }else{
  //     $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();
  //   }

  //   $moodValue = array("");   //first index
  //   foreach ($happyIndexMoodValues as $key => $value) {
  //     $moodValue[] = ucfirst($value->moodName)."(%)";
  //   }
  //   array_push($arrMain, $moodValue);

  //   $happyIndexCountMonth = array();
  //   foreach ($months as $key => $month) {
  //     $newArr = array(date('My',strtotime($month)));   //first index of month

  //     $totalUsersQuery = DB::table('happy_indexes')
  //     ->leftjoin('users','users.id','happy_indexes.userId')
  //     // ->where('users.status','Active')
  //     ->where('happy_indexes.status','Active')
  //     ->where('happy_indexes.created_at','LIKE',$month.'%');
  //     if(!empty($orgId)){
  //         $totalUsersQuery->where('users.orgId',$orgId);
  //     }
  //     if(!empty($officeId) && empty($departmentId)) {
  //         $totalUsersQuery->where('users.officeId',$officeId);
  //     }elseif(!empty($officeId) && !empty($departmentId)) {
  //         $totalUsersQuery->where('users.officeId',$officeId);
  //         $totalUsersQuery->where('users.departmentId',$departmentId);
  //     }elseif(empty($officeId) && !empty($departmentId)) {
  //         $totalUsersQuery->leftJoin('departments','departments.id','users.departmentId')
  //             ->where('departments.status','Active')
  //             ->where('departments.departmentId',$departmentId);
  //     }
  //     if($includeWeekend != 1){ //Exclude Weekend Days
  //       //Get weekends
  //       $YearMonth = $month;
  //       $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);

  //       $totalUsersQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //     }
  //     $totalUsers = $totalUsersQuery->count();

  //     foreach ($happyIndexMoodValues as $moodVal) {
  //       $monthCountQuery = DB::table('happy_indexes')
  //           ->leftjoin('users','users.id','happy_indexes.userId')
  //           // ->where('users.status','Active')
  //           ->where('happy_indexes.status','Active')
  //           ->where('happy_indexes.moodValue',$moodVal->id)
  //           ->where('happy_indexes.created_at','LIKE',$month.'%');
  //       if(!empty($orgId)){
  //           $monthCountQuery->where('users.orgId',$orgId);
  //       }
  //       if(!empty($officeId) && empty($departmentId)){
  //         $monthCountQuery->where('users.officeId',$officeId);
  //       }elseif(!empty($officeId) && !empty($departmentId)){
  //         $monthCountQuery->where('users.officeId',$officeId);
  //         $monthCountQuery->where('users.departmentId',$departmentId);
  //       }elseif(empty($officeId) && !empty($departmentId)){
  //         $monthCountQuery->leftJoin('departments','departments.id','users.departmentId')
  //             ->where('departments.status','Active')
  //             ->where('departments.departmentId',$departmentId);
  //       }
  //       if($includeWeekend != 1){ //Exclude Weekend Days
  //         //Get weekends
  //         $YearMonth = $month;
  //         $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);

  //         $monthCountQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //       }
  //       $moodCount = $monthCountQuery->count();

  //       $moodCountUsers = 0;
  //       if (!empty($moodCount) && !empty($totalUsers)) {
  //         $moodCountUsers = round((($moodCount/$totalUsers)*100),2);
  //       }
  //       array_push($newArr,$moodCountUsers);
  //     }
  //     array_push($arrMain, $newArr);
  //   }
  //   return array('status'=>true,'arrMain'=>$arrMain,'columnColor'=>$columnColor);
  // }

  // public function getHappyIndexWeekGraph(Request $request)
  // {
  //   $orgId            = Input::get('orgId');
  //   $officeId         = Input::get('officeId');
  //   $departmentId     = Input::get('departmentId');
  //   $monthNum         = Input::get('monthNum');

  //   $org = DB::table('organisations')->where('id',$orgId)->first();
  //   $includeWeekend = $org->include_weekend;

  //   $arrMain = array();

  //   for ($i = 11; $i >= 0; $i--) {
  //     $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
  //   }
  //   $yearMonth    = $months[$monthNum]; //2019-11
  //   $yearMonthExp = explode("-",$yearMonth);

  //   $year  = $yearMonthExp[0];
  //   $month = $yearMonthExp[1];

  //   $monthCount = cal_days_in_month(CAL_GREGORIAN,$month,$year);
  //   //echo $totalDays;die;

  //   $weeks = $monthCount%7;

  //   if ($weeks) {
  //       $arrayCount = array("Week 1","Week 2","Week 3","Week 4","Week 5");
  //   }else{
  //       $arrayCount = array("Week 1","Week 2","Week 3","Week 4");
  //   }

  //   $lastDay = date('t',strtotime($year."-".$month."-1"));

  //   $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();
  //   $moodValue = array("");   //first index
  //   foreach ($happyIndexMoodValues as $key => $value) {
  //     $moodValue[] = ucfirst($value->moodName)."(%)";
  //   }
  //   array_push($arrMain, $moodValue);

  //   $i = 1;$j = 7;
  //   foreach ($arrayCount as $key => $value) {
  //     $newArr = array($value);   //first index of week

  //     $totalUsersQuery = DB::table('happy_indexes')
  //         ->leftjoin('users','users.id','happy_indexes.userId')
  //         ->where('users.orgId',$orgId)
  //         // ->where('users.status','Active')
  //         ->where('happy_indexes.status','Active');
  //     if(!empty($orgId)){
  //         $totalUsersQuery->where('users.orgId',$orgId);
  //     }
  //     if(!empty($officeId) && empty($departmentId)) {
  //         $totalUsersQuery->where('users.officeId',$officeId);
  //     }elseif(!empty($officeId) && !empty($departmentId)) {
  //         $totalUsersQuery->where('users.officeId',$officeId);
  //         $totalUsersQuery->where('users.departmentId',$departmentId);
  //     }elseif(empty($officeId) && !empty($departmentId)) {
  //         $totalUsersQuery->leftJoin('departments','departments.id','users.departmentId')
  //             ->where('departments.status','Active')
  //             ->where('departments.departmentId',$departmentId);
  //     }
  //     if($includeWeekend != 1){ //Exclude Weekend Days
  //         //Get weekends
  //         $YearMonth = $yearMonth;
  //         $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);

  //         $totalUsersQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //     }
  //     if ($i==29) {
  //         $totalUsersQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$lastDay." "."23:59:59"]);
  //     }else{
  //         $totalUsersQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$j." "."23:59:59"]);
  //     }
  //     $totalUsers = $totalUsersQuery->count();

  //     foreach ($happyIndexMoodValues as $mValue) {
  //         $happyUsersWeeklyQuery = DB::table('happy_indexes')
  //             ->select('happy_indexes.created_at')
  //             ->leftjoin('users','users.id','happy_indexes.userId')
  //             ->where('users.orgId',$orgId)
  //             // ->where('users.status','Active')
  //             ->where('happy_indexes.status','Active')
  //             ->where('happy_indexes.moodValue',$mValue->id);
  //         if(!empty($orgId)){
  //             $happyUsersWeeklyQuery->where('users.orgId',$orgId);
  //         }
  //         if(!empty($officeId) && empty($departmentId)) {
  //             $happyUsersWeeklyQuery->where('users.officeId',$officeId);
  //         }elseif(!empty($officeId) && !empty($departmentId)) {
  //             $happyUsersWeeklyQuery->where('users.officeId',$officeId);
  //             $happyUsersWeeklyQuery->where('users.departmentId',$departmentId);
  //         }elseif(empty($officeId) && !empty($departmentId)) {
  //             $happyUsersWeeklyQuery->leftJoin('departments','departments.id','users.departmentId')
  //                 ->where('departments.status','Active')
  //                 ->where('departments.departmentId',$departmentId);
  //         }
  //         if($includeWeekend != 1){ //Exclude Weekend Days
  //             //Get weekends
  //             $YearMonth = $yearMonth;
  //             $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);

  //             $happyUsersWeeklyQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //         }
  //         if ($i==29) {
  //             $happyUsersWeeklyQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$lastDay." "."23:59:59"]);
  //         }else{
  //             $happyUsersWeeklyQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$j." "."23:59:59"]);
  //         }
  //         $moodCount = $happyUsersWeeklyQuery->count();

  //         $moodCountUsers = 0;
  //         if (!empty($moodCount) && !empty($totalUsers)) {
  //             $moodCountUsers = round((($moodCount/$totalUsers)*100),2);
  //         }
  //         array_push($newArr, $moodCountUsers);
  //     }
  //     array_push($arrMain, $newArr);
  //     $i+=7;
  //     $j+=7;
  //   }
  //   return array('status'=>true,'arrMain'=>$arrMain,'yearMonth'=>$yearMonth);
  // }

  // public function getHappyIndexWeekCat(Request $request)
  // {
  //   $orgId            = Input::get('orgId');
  //   $officeId         = Input::get('officeId');
  //   $departmentId     = Input::get('departmentId');
  //   $moodId           = Input::get('moodId');
  //   $columnColor      = Input::get('columnColor');
  //   $monthNum         = Input::get('monthNum');

  //   $org = DB::table('organisations')->where('id',$orgId)->first();
  //   $includeWeekend = $org->include_weekend;

  //   $arrMain = array();

  //   for ($i = 11; $i >= 0; $i--) {
  //     $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
  //   }
  //   $yearMonth    = $months[$monthNum]; //2019-11
  //   $yearMonthExp = explode("-",$yearMonth);

  //   $year  = $yearMonthExp[0];
  //   $month = $yearMonthExp[1];

  //   $monthCount = cal_days_in_month(CAL_GREGORIAN,$month,$year);

  //   $weeks = $monthCount%7;

  //   if ($weeks) {
  //       $arrayCount = array("Week 1","Week 2","Week 3","Week 4","Week 5");
  //   }else{
  //       $arrayCount = array("Week 1","Week 2","Week 3","Week 4");
  //   }

  //   $lastDay = date('t',strtotime($year."-".$month."-1"));
    
  //   if ($moodId) {
  //     $happyIndexMoodValues = DB::table('happy_index_mood_values')->where('id',$moodId)->get();
  //   }else{
  //     $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();
  //   }

  //   $moodValue = array("");   //first index
  //   foreach ($happyIndexMoodValues as $key => $value) {
  //     $moodValue[] = ucfirst($value->moodName)."(%)";
  //   }
  //   array_push($arrMain, $moodValue);

  //   $i = 1;$j = 7;
  //   foreach ($arrayCount as $key => $value) {
  //     $newArr = array($value);   //first index of week

  //     $totalUsersQuery = DB::table('happy_indexes')
  //         ->leftjoin('users','users.id','happy_indexes.userId')
  //         ->where('users.orgId',$orgId)
  //         // ->where('users.status','Active')
  //         ->where('happy_indexes.status','Active');
  //     if(!empty($orgId)){
  //         $totalUsersQuery->where('users.orgId',$orgId);
  //     }
  //     if(!empty($officeId) && empty($departmentId)) {
  //         $totalUsersQuery->where('users.officeId',$officeId);
  //     }elseif(!empty($officeId) && !empty($departmentId)) {
  //         $totalUsersQuery->where('users.officeId',$officeId);
  //         $totalUsersQuery->where('users.departmentId',$departmentId);
  //     }elseif(empty($officeId) && !empty($departmentId)) {
  //         $totalUsersQuery->leftJoin('departments','departments.id','users.departmentId')
  //             ->where('departments.status','Active')
  //             ->where('departments.departmentId',$departmentId);
  //     }
  //     if($includeWeekend != 1){ //Exclude Weekend Days
  //         //Get weekends
  //         $YearMonth = $yearMonth;
  //         $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);

  //         $totalUsersQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //     }
  //     if ($i==29) {
  //         $totalUsersQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$lastDay." "."23:59:59"]);
  //     }else{
  //         $totalUsersQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$j." "."23:59:59"]);
  //     }
  //     $totalUsers = $totalUsersQuery->count();

  //     foreach ($happyIndexMoodValues as $mValue) {
  //         $happyUsersWeeklyQuery = DB::table('happy_indexes')
  //             ->select('happy_indexes.created_at')
  //             ->leftjoin('users','users.id','happy_indexes.userId')
  //             ->where('users.orgId',$orgId)
  //             // ->where('users.status','Active')
  //             ->where('happy_indexes.status','Active')
  //             ->where('happy_indexes.moodValue',$mValue->id);
  //         if(!empty($orgId)){
  //             $happyUsersWeeklyQuery->where('users.orgId',$orgId);
  //         }
  //         if(!empty($officeId) && empty($departmentId)) {
  //             $happyUsersWeeklyQuery->where('users.officeId',$officeId);
  //         }elseif(!empty($officeId) && !empty($departmentId)) {
  //             $happyUsersWeeklyQuery->where('users.officeId',$officeId);
  //             $happyUsersWeeklyQuery->where('users.departmentId',$departmentId);
  //         }elseif(empty($officeId) && !empty($departmentId)) {
  //             $happyUsersWeeklyQuery->leftJoin('departments','departments.id','users.departmentId')
  //                 ->where('departments.status','Active')
  //                 ->where('departments.departmentId',$departmentId);
  //         }
  //         if($includeWeekend != 1){ //Exclude Weekend Days
  //             //Get weekends
  //             $YearMonth = $yearMonth;
  //             $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);

  //             $happyUsersWeeklyQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //         }
  //         if ($i==29) {
  //             $happyUsersWeeklyQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$lastDay." "."23:59:59"]);
  //         }else{
  //             $happyUsersWeeklyQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$j." "."23:59:59"]);
  //         }
  //         $moodCount = $happyUsersWeeklyQuery->count();

  //         $moodCountUsers = 0;
  //         if (!empty($moodCount) && !empty($totalUsers)) {
  //             $moodCountUsers = round((($moodCount/$totalUsers)*100),2);
  //         }
  //         array_push($newArr, $moodCountUsers);
  //     }
  //     array_push($arrMain, $newArr);
  //     $i+=7;
  //     $j+=7;
  //   }
  //   return array('status'=>true,'arrMain'=>$arrMain,'columnColor'=>$columnColor,'yearMonth'=>$yearMonth);
  // }

  // public function getHappyIndexDaysGraph(Request $request)
  // {
  //   $orgId            = Input::get('orgId');
  //   $officeId         = Input::get('officeId');
  //   $departmentId     = Input::get('departmentId');
  //   $weekNum          = Input::get('weekNum');  // 0=>'Week 1',1=>'Week 2',2=>'Week 3',3=>'Week 4',4=>'Week 5'
  //   $yearMonth        = Input::get('yearMonth'); //2019-11
  //   $index            = Input::get('index'); //1 for main graph, 2 for sub cate graph

  //   if (!empty($index) && $index == 2) {
  //     $moodId           = Input::get('moodId');
  //     $columnColor      = Input::get('columnColor');
  //   }

  //   $org = DB::table('organisations')->where('id',$orgId)->first();
  //   $includeWeekend = $org->include_weekend;

  //   $arrMain = array();

  //   $yearMonthExp = explode("-",$yearMonth);

  //   $year  = $yearMonthExp[0];
  //   $month = $yearMonthExp[1];
  //   $monthCount = cal_days_in_month(CAL_GREGORIAN,$month,$year);
  //   $remainingWeeks = $monthCount%7;

  //   if ($month == 1) {
  //       $monthName = "Jan";
  //   }elseif ($month == 2) {
  //       $monthName = "Feb";
  //   }elseif ($month == 3) {
  //       $monthName = "Mar";
  //   }elseif ($month == 4) {
  //       $monthName = "Apr";
  //   }elseif ($month == 5) {
  //       $monthName = "May";
  //   }elseif ($month == 6) {
  //       $monthName = "Jun";
  //   }elseif ($month == 7) {
  //       $monthName = "Jul";
  //   }elseif ($month == 8) {
  //       $monthName = "Aug";
  //   }elseif ($month == 9) {
  //       $monthName = "Sep";
  //   }elseif ($month == 10) {
  //       $monthName = "Oct";
  //   }elseif ($month == 11) {
  //       $monthName = "Nov";
  //   }elseif ($month == 12) {
  //       $monthName = "Dec";
  //   }

  //   if ($weekNum == 0) { // For Week 1
  //       $i=1;
  //       $days = array($monthName." 1 ".$year,$monthName." 2 ".$year,$monthName." 3 ".$year,$monthName." 4 ".$year,$monthName." 5 ".$year,$monthName." 6 ".$year,$monthName." 7 ".$year);
  //   }else if ($weekNum == 1) { // For Week 2
  //       $i=8;
  //       $days = array($monthName." 8 ".$year,$monthName." 9 ".$year,$monthName." 10 ".$year,$monthName." 11 ".$year,$monthName." 12 ".$year,$monthName." 13 ".$year,$monthName." 14 ".$year);
  //   }else if ($weekNum == 2) { // For Week 3
  //       $i=15;
  //       $days = array($monthName." 15 ".$year,$monthName." 16 ".$year,$monthName." 17 ".$year,$monthName." 18 ".$year,$monthName." 19 ".$year,$monthName." 20 ".$year,$monthName." 21 ".$year);
  //   }else if ($weekNum == 3) { // For Week 4
  //       $i=22;
  //       $days = array($monthName." 22 ".$year,$monthName." 23 ".$year,$monthName." 24 ".$year,$monthName." 25 ".$year,$monthName." 26 ".$year,$monthName." 27 ".$year,$monthName." 28 ".$year);
  //   }else if ($weekNum == 4) { // For Week 5
  //       $i=29;
  //       if ($remainingWeeks == 1) {
  //           $days = array($monthName." 29 ".$year);
  //       }elseif ($remainingWeeks == 2) {
  //           $days = array($monthName." 29 ".$year,$monthName." 30 ".$year);
  //       }elseif ($remainingWeeks == 3) {
  //           $days = array($monthName." 29 ".$year,$monthName." 30 ".$year,$monthName." 31 ".$year);
  //       }
  //   }

  //   if (isset($moodId) && !empty($moodId) && $index == 2) {
  //     $happyIndexMoodValues = DB::table('happy_index_mood_values')->where('id',$moodId)->get();
  //   }else{
  //     $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();
  //   }
  //   $moodValue = array("");   //first index
  //   foreach ($happyIndexMoodValues as $key => $value) {
  //     $moodValue[] = ucfirst($value->moodName)."(%)";
  //   }
  //   array_push($arrMain, $moodValue);

  //   foreach ($days as $key => $value) {
  //     $newArr = array($value);   //first index of days

  //     $totalUsersQuery = DB::table('happy_indexes')
  //         ->leftjoin('users','users.id','happy_indexes.userId')
  //         ->where('users.orgId',$orgId)
  //         // ->where('users.status','Active')
  //         ->where('happy_indexes.status','Active')
  //         ->whereDate('happy_indexes.created_at',$yearMonth."-".$i);
  //     if(!empty($orgId)){
  //         $totalUsersQuery->where('users.orgId',$orgId);
  //     }
  //     if(!empty($officeId) && empty($departmentId)) {
  //         $totalUsersQuery->where('users.officeId',$officeId);
  //     }elseif(!empty($officeId) && !empty($departmentId)) {
  //         $totalUsersQuery->where('users.officeId',$officeId);
  //         $totalUsersQuery->where('users.departmentId',$departmentId);
  //     }elseif(empty($officeId) && !empty($departmentId)) {
  //         $totalUsersQuery->leftJoin('departments','departments.id','users.departmentId')
  //             ->where('departments.status','Active')
  //             ->where('departments.departmentId',$departmentId);
  //     }
  //     if($includeWeekend != 1){ //Exclude Weekend Days
  //         //Get weekends
  //         $YearMonth = $yearMonth;
  //         $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);
  //         $totalUsersQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //     }
  //     $totalUsers = $totalUsersQuery->count();

  //     foreach ($happyIndexMoodValues as $mValue) {
  //         $happyUsersWeeklyQuery = DB::table('happy_indexes')
  //             ->select('happy_indexes.created_at')
  //             ->leftjoin('users','users.id','happy_indexes.userId')
  //             ->where('users.orgId',$orgId)
  //             // ->where('users.status','Active')
  //             ->where('happy_indexes.status','Active')
  //             ->where('happy_indexes.moodValue',$mValue->id)
  //             ->whereDate('happy_indexes.created_at',$yearMonth."-".$i);
  //         if(!empty($orgId)){
  //             $happyUsersWeeklyQuery->where('users.orgId',$orgId);
  //         }
  //         if(!empty($officeId) && empty($departmentId)) {
  //             $happyUsersWeeklyQuery->where('users.officeId',$officeId);
  //         }elseif(!empty($officeId) && !empty($departmentId)) {
  //             $happyUsersWeeklyQuery->where('users.officeId',$officeId);
  //             $happyUsersWeeklyQuery->where('users.departmentId',$departmentId);
  //         }elseif(empty($officeId) && !empty($departmentId)) {
  //             $happyUsersWeeklyQuery->leftJoin('departments','departments.id','users.departmentId')
  //                 ->where('departments.status','Active')
  //                 ->where('departments.departmentId',$departmentId);
  //         }
  //         if($includeWeekend != 1){ //Exclude Weekend Days
  //             //Get weekends
  //             $YearMonth = $yearMonth;
  //             $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);
  //             $happyUsersWeeklyQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //         }
  //         $moodCount = $happyUsersWeeklyQuery->count();

  //         $moodCountUsers = 0;
  //         if (!empty($moodCount) && !empty($totalUsers)) {
  //             $moodCountUsers = round((($moodCount/$totalUsers)*100),2);
  //         }
  //         array_push($newArr, $moodCountUsers);
  //     }
  //     array_push($arrMain, $newArr);
  //     $i++;
  //   }

  //   if (!empty($index) && $index==1) {
  //     return array('status'=>true,'arrMain'=>$arrMain);
  //   }else if(!empty($index) && $index==2) {
  //     return array('status'=>true,'arrMain'=>$arrMain,'columnColor'=>$columnColor);
  //   }
  // }

  public function getDiagnosticValues($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend = $perArr["includeWeekend"];
    $months         = $perArr["months"];
    $ispdf          = $perArr["ispdf"];

    // $diagnosticFinalArray   = array();
    $arrMain = array();

    $diagnosticQueCatTbl = DB::table('diagnostic_questions_category')->get();

    $categoryName = array("");   //first index
    foreach ($diagnosticQueCatTbl as $key => $value) {
      $categoryName[$value->id] = $value->title."(%)";
    }
    array_push($arrMain, $categoryName);

    $max = 0;
    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));   //first index of month

      if ($month != date('Y-m')) {
        foreach ($diagnosticQueCatTbl as $cate) {
          $diagDataQuery = DB::table('diagnostic_report_graph')
              ->where('categoryId',$cate->id)
              ->where('date','LIKE',$month."%");
          if (!empty($orgId)) {
            $diagDataQuery->where('orgId',$orgId);  
          }
          if(empty($officeId) && empty($departmentId))
          {
              $diagDataQuery->whereNull('officeId');
              $diagDataQuery->whereNull('departmentId');
          }
          elseif(!empty($officeId) && empty($departmentId))
          {
              $diagDataQuery->where('officeId',$officeId);
              $diagDataQuery->whereNull('departmentId');
          }
          elseif(!empty($officeId) && !empty($departmentId))
          {
              $diagDataQuery->where('officeId',$officeId);
              $diagDataQuery->where('departmentId',$departmentId);
          }
          elseif(empty($officeId) && !empty($departmentId))
          {
              $diagDataQuery->whereNull('officeId');
              $diagDataQuery->where('departmentId',$departmentId);
          }
          $diagData = $diagDataQuery->first();
          if($diagData){
            if($includeWeekend != 1){ //Exclude Weekend Days
              $newArr[] = $diagData->without_weekend;
              if ($diagData->without_weekend != 0) {
                  $max++;
              }
            }else{
                $newArr[] = $diagData->with_weekend;
                if ($diagData->with_weekend != 0) {
                    $max++;
                }
            }
          }else{
              $newArr[] = 0;
          }
        }
      }else{
        if ($includeWeekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $diagnosticsArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateDiagnosticGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date);
          
          if (empty($diagnosticsArr)) {
            foreach ($diagnosticQueCatTbl as $diagTbl) {
                array_push($newArr, 0);
            }
          }
          foreach ($diagnosticsArr as $diagnosticsVal) { 
              array_push($newArr, $diagnosticsVal['percentage']);
              if ($diagnosticsVal['percentage'] != 0) {
                  $max++;
              }
          }
      }
      array_push($arrMain, $newArr);
    }

    return array('arrMain'=>$arrMain,'maxCount'=>$max); 
  }

  public function getDiagnosticCat(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $categoryId       = Input::get('categoryId');
    $columnColor       = Input::get('columnColor');

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }

    $org = DB::table('organisations')->where('id',$orgId)->first();
    $includeWeekend = $org->include_weekend;

    $arrMain = array();
    if ($categoryId) {
      $diagnosticQueCatTbl = DB::table('diagnostic_questions_category')->where('id',$categoryId)->get();
    }else{
      $diagnosticQueCatTbl = DB::table('diagnostic_questions_category')->get();
    }

    $categoryName = array("");   //first index
    foreach ($diagnosticQueCatTbl as $key => $value) {
      $categoryName[] = $value->title."(%)";
    }
    array_push($arrMain, $categoryName);

    $max = 0;
    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));   //first index of month
      
      if ($month != date('Y-m')) {
        foreach ($diagnosticQueCatTbl as $cate) {
          $diagDataQuery = DB::table('diagnostic_report_graph')
              ->where('date','LIKE',$month."%");
          if (!empty($categoryId)) {
            $diagDataQuery->where('categoryId',$categoryId);
          }else{
            $diagDataQuery->where('categoryId',$cate->id);
          }
          if (!empty($orgId)) {
            $diagDataQuery->where('orgId',$orgId);  
          }
          if(empty($officeId) && empty($departmentId))
          {
              $diagDataQuery->whereNull('officeId');
              $diagDataQuery->whereNull('departmentId');
          }
          elseif(!empty($officeId) && empty($departmentId))
          {
              $diagDataQuery->where('officeId',$officeId);
              $diagDataQuery->whereNull('departmentId');
          }
          elseif(!empty($officeId) && !empty($departmentId))
          {
              $diagDataQuery->where('officeId',$officeId);
              $diagDataQuery->where('departmentId',$departmentId);
          }
          elseif(empty($officeId) && !empty($departmentId))
          {
              $diagDataQuery->whereNull('officeId');
              $diagDataQuery->where('departmentId',$departmentId);
          }
          $diagData = $diagDataQuery->first();
          if($diagData){
            if($includeWeekend != 1){ //Exclude Weekend Days
                $newArr[] = $diagData->without_weekend;
                if ($diagData->without_weekend != 0) {
                    $max++;
                }
            }else{
                $newArr[] = $diagData->with_weekend;
                if ($diagData->with_weekend != 0) {
                    $max++;
                }
            }
          }else{
              $newArr[] = 0;
          }
        }
      }else{
        if ($includeWeekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $diagnosticsArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateDiagnosticGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date,$categoryId);

          if (empty($diagnosticsArr)) {
            foreach ($diagnosticQueCatTbl as $diagTbl) {
              array_push($newArr, 0);
            }
          }
          foreach ($diagnosticsArr as $diagnosticsVal) { 
            array_push($newArr, $diagnosticsVal['percentage']);
            if ($diagnosticsVal['percentage'] != 0) {
                $max++;
            }
          }
      }
      array_push($arrMain, $newArr);
    }
    return array('status'=>true,'arrMain'=>$arrMain,'columnColor'=>$columnColor,'maxCount'=>$max);
  }

  public function getTribeometerValues($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend = $perArr["includeWeekend"];
    $months         = $perArr["months"];

    // $tribeoFinalArray = array();
    $arrMain = array();
    $queCatTbl = DB::table('tribeometer_questions_category')->get();

    $categoryName = array("");   //first index
    foreach ($queCatTbl as $key => $value) {
      $categoryName[$value->id] = $value->title."(%)";
    }
    array_push($arrMain, $categoryName);

    $max = 0;
    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));   //first index of month

      if ($month != date('Y-m')) {
        foreach ($queCatTbl as $cate) {
          $triboDataQuery = DB::table('tribeometer_dashboard_graph')
              ->where('categoryId',$cate->id)
              ->where('date','LIKE',$month."%");
          if (!empty($orgId)) {
            $triboDataQuery->where('orgId',$orgId);  
          }
          if(empty($officeId) && empty($departmentId)){
              $triboDataQuery->whereNull('officeId');
              $triboDataQuery->whereNull('departmentId');
          }elseif(!empty($officeId) && empty($departmentId)){
              $triboDataQuery->where('officeId',$officeId);
              $triboDataQuery->whereNull('departmentId');
          }elseif(!empty($officeId) && !empty($departmentId)){
              $triboDataQuery->where('officeId',$officeId);
              $triboDataQuery->where('departmentId',$departmentId);
          }elseif(empty($officeId) && !empty($departmentId)){
              $triboDataQuery->whereNull('officeId');
              $triboDataQuery->where('departmentId',$departmentId);
          }
          $tribeoData = $triboDataQuery->first();
          if($tribeoData){
            if($includeWeekend!=1){ //Exclude Weekend Days
                if ($tribeoData->without_weekend>100) {
                  $newArr[] = 100;
                }else{
                  $newArr[] = $tribeoData->without_weekend;
                }
                if ($tribeoData->without_weekend != 0) {
                    $max++;
                }
            }else{
                if ($tribeoData->with_weekend>100) {
                  $newArr[] = 100;
                }else{
                  $newArr[] = $tribeoData->with_weekend;
                }
                if ($tribeoData->with_weekend != 0) {
                    $max++;
                }
            }
          }else{
              $newArr[] = 0;
          }
        }
      }else{
        if ($includeWeekend==1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $tribeometerArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateTribeometerGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date);

          foreach ($tribeometerArr as $tribeoVal) { 
            array_push($newArr, $tribeoVal['percentage']);
            if ($tribeoVal['percentage'] != 0) {
                $max++;
            }
          }
      }

      array_push($arrMain, $newArr);
      



      // $query = DB::table('tribeometer_answers')
      //     ->leftjoin('users','users.id','tribeometer_answers.userId')
      //     ->leftJoin('departments','departments.id','users.departmentId')
      //     ->select('tribeometer_answers.userId')
      //     ->where('users.status','Active')
      //     ->where('departments.status','Active')
      //     ->where('tribeometer_answers.created_at','LIKE',$month."%");
      // if ($includeWeekend != 1) {
      //   $YearMonth = $month;
      //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);
      //   $query->whereNotIn(DB::raw("(DATE_FORMAT(tribeometer_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      // }
      // if(!empty($orgId)){
      //   $query->where('users.orgId',$orgId);
      // } 
      // if(!empty($officeId) && empty($departmentId)){
      //   $query->where('users.officeId',$officeId);
      // }elseif(!empty($officeId) && !empty($departmentId)){
      //   $query->where('users.officeId',$officeId);
      //   $query->where('users.departmentId',$departmentId);
      // }elseif(empty($officeId) && !empty($departmentId)){
      //   $query->where('departments.departmentId',$departmentId);
      // }
      // $users = $query->groupBy('tribeometer_answers.userId')->get();
      // $userCount = count($users);

      // $queCatTbl      = DB::table('tribeometer_questions_category')->get();

      // $tribeometerResultArray = array();
      // if(!empty($userCount)){
      //   $optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
      //   $optCount = count($optionsTbl)-1; 

      //   foreach ($queCatTbl as $value){
      //     $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();
      //     $quecount = count($questionTbl);

      //     $perQuePercen = 0;
      //     foreach ($questionTbl as  $queValue){
      //         $diaQuery = DB::table('tribeometer_answers')           
      //         ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
      //         ->leftjoin('users','users.id','tribeometer_answers.userId')
      //         ->leftJoin('departments','departments.id','users.departmentId')
      //         ->where('users.status','Active') 
      //         ->where('departments.status','Active')
      //         ->where('tribeometer_answers.status','Active')
      //         ->where('tribeometer_questions.status','Active')
      //         ->where('tribeometer_answers.created_at','LIKE',$month."%")
      //         ->where('tribeometer_questions.id',$queValue->id)
      //         ->where('tribeometer_questions.category_id',$value->id);
      //       if ($includeWeekend != 1) {
      //         $YearMonth = $month;
      //         $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);
      //         $diaQuery->whereNotIn(DB::raw("(DATE_FORMAT(tribeometer_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      //       }
      //       if(!empty($orgId)){
      //         $diaQuery->where('users.orgId',$orgId);
      //       } 
      //       if(!empty($officeId) && empty($departmentId)){
      //         $diaQuery->where('users.officeId',$officeId);
      //       }elseif(!empty($officeId) && !empty($departmentId)){
      //         $diaQuery->where('users.officeId',$officeId);
      //         $diaQuery->where('users.departmentId',$departmentId);
      //       }elseif(empty($officeId) && !empty($departmentId)){
      //         $diaQuery->where('departments.departmentId',$departmentId);
      //       }
      //       $diaAnsTbl = $diaQuery->sum('answer');
      //       //avg of all questions
      //       $perQuePercen += ($diaAnsTbl/$userCount); 
      //     }
      //     $score = ($perQuePercen/($quecount*$optCount));
      //     $totalPercentage = ($perQuePercen/($quecount*$optCount))*100;

      //     $value1['title']      =  $value->title;
      //     $value1['score']      =  number_format((float)$score, 2, '.', '');
      //     $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');            

      //     array_push($tribeometerResultArray, $value1);
      //   }
      // }
      // $tribeDnC['data'] = $tribeometerResultArray;
      // $tribeDnC['monthName'] = date('My',strtotime($month));

      // array_push($tribeoFinalArray, $tribeDnC);
    }
    return array('arrMain'=>$arrMain,'maxCount'=>$max);
    // return $tribeoFinalArray;
  }

  public function getTribeometerCat(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $categoryId       = Input::get('categoryId');
    $columnColor       = Input::get('columnColor');

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }

    $org = DB::table('organisations')->where('id',$orgId)->first();
    $includeWeekend = $org->include_weekend;

    $arrMain = array();
    if ($categoryId) {
      $queCatTbl = DB::table('tribeometer_questions_category')->where('id',$categoryId)->get();
    }else{
      $queCatTbl = DB::table('tribeometer_questions_category')->get();
    }

    $categoryName = array("");   //first index
    foreach ($queCatTbl as $key => $value) {
      $categoryName[] = $value->title."(%)";
    }
    array_push($arrMain, $categoryName);

    $max = 0;
    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));   //first index of month

      if ($month != date('Y-m')) {
        foreach ($queCatTbl as $cate) {
          $triboDataQuery = DB::table('tribeometer_dashboard_graph')
              ->where('date','LIKE',$month."%");
          if (!empty($categoryId)) {
            $triboDataQuery->where('categoryId',$categoryId);
          }else{
            $triboDataQuery->where('categoryId',$cate->id);
          }
          if (!empty($orgId)) {
            $triboDataQuery->where('orgId',$orgId);  
          }
          if(empty($officeId) && empty($departmentId)){
              $triboDataQuery->whereNull('officeId');
              $triboDataQuery->whereNull('departmentId');
          }elseif(!empty($officeId) && empty($departmentId)){
              $triboDataQuery->where('officeId',$officeId);
              $triboDataQuery->whereNull('departmentId');
          }elseif(!empty($officeId) && !empty($departmentId)){
              $triboDataQuery->where('officeId',$officeId);
              $triboDataQuery->where('departmentId',$departmentId);
          }elseif(empty($officeId) && !empty($departmentId)){
              $triboDataQuery->whereNull('officeId');
              $triboDataQuery->where('departmentId',$departmentId);
          }
          $tribeoData = $triboDataQuery->first();
          if($tribeoData){
            if($includeWeekend != 1){ //Exclude Weekend Days
                if ($tribeoData->without_weekend>100) {
                  $newArr[] = 100;
                }else{
                  $newArr[] = $tribeoData->without_weekend;
                }
                if ($tribeoData->without_weekend != 0) {
                    $max++;
                }
            }else{
                if ($tribeoData->with_weekend>100) {
                  $newArr[] = 100;
                }else{
                  $newArr[] = $tribeoData->with_weekend;
                }
                if ($tribeoData->with_weekend != 0) {
                    $max++;
                }
            }
          }else{
              $newArr[] = 0;
          }
        }
      }else{
        if ($includeWeekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $tribeometerArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateTribeometerGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date,$categoryId);

          foreach ($tribeometerArr as $tribeoVal) { 
            array_push($newArr, $tribeoVal['percentage']);
            if ($tribeoVal['percentage'] != 0) {
                $max++;
            }
          }
      }
      array_push($arrMain, $newArr);
    }

    // echo "<pre>";print_r($arrMain);die();
    return array('status'=>true,'arrMain'=>$arrMain,'columnColor'=>$columnColor,'maxCount'=>$max);
  }

  public function getMotivationValues($perArr=array())
  {
    $orgId            = $perArr["orgId"];
    $officeId         = $perArr["officeId"];
    $departmentId     = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];
    $months           = $perArr["months"];

    // $tribeoFinalArray = array();
    $arrMain = array();
    $queCatTbl = DB::table('sot_motivation_value_records')->get();

    $categoryName = array("");   //first index
    foreach ($queCatTbl as $key => $value) {
      $categoryName[$value->id] = $value->title."(%)";
    }
    array_push($arrMain, $categoryName);

    $max = 0;
    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));   //first index of month

      if ($month != date('Y-m')) {
        foreach ($queCatTbl as $cate) {
          $motivationDataQuery = DB::table('sot_motivation_dashboard_graph')
              ->where('categoryId',$cate->id)
              ->where('date','LIKE',$month."%");
          if (!empty($orgId)) {
            $motivationDataQuery->where('orgId',$orgId);  
          }
          if(empty($officeId) && empty($departmentId)){
              $motivationDataQuery->whereNull('officeId');
              $motivationDataQuery->whereNull('departmentId');
          }elseif(!empty($officeId) && empty($departmentId)){
              $motivationDataQuery->where('officeId',$officeId);
              $motivationDataQuery->whereNull('departmentId');
          }elseif(!empty($officeId) && !empty($departmentId)){
              $motivationDataQuery->where('officeId',$officeId);
              $motivationDataQuery->where('departmentId',$departmentId);
          }elseif(empty($officeId) && !empty($departmentId)){
              $motivationDataQuery->whereNull('officeId');
              $motivationDataQuery->where('departmentId',$departmentId);
          }
          $motivationData = $motivationDataQuery->first();
          if($motivationData){
            if($includeWeekend != 1){ //Exclude Weekend Days
                $newArr[] = $motivationData->without_weekend;
                if ($motivationData->without_weekend != 0) {
                    $max++;
                }
            }else{
                $newArr[] = $motivationData->with_weekend;
                if ($motivationData->with_weekend != 0) {
                    $max++;
                }
            }
          }else{
              $newArr[] = 0;
          }
        }
      }
      else
      {
          if ($includeWeekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $motivationArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateMotivationGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date);

          foreach ($motivationArr as $motiVal) { 
            array_push($newArr, $motiVal['percentage']);
            if ($motiVal['percentage'] != 0) {
                $max++;
            }
          }
      }      
      array_push($arrMain, $newArr);
    }
    // echo "<pre>";print_r($arrMain);die();
    return array('arrMain'=>$arrMain,'maxCount'=>$max);

  //   //Get weekends
  //   $startDate=DB::table('sot_motivation_answers')
  //       ->select('created_at')
  //       ->where('orgId',$orgId)
  //       ->orderBy('created_at','ASC')
  //       ->first();

  //   if (count($startDate) == 0) {
  //       // $startDate = $date;
  //       $startDate = date('Y-m-d');
  //   }else{
  //       $startDate = date('Y-m-d',strtotime($startDate->created_at));
  //   }
  //   // $currentDate = $date;
  //   $currentDate = date('Y-m-d');
  //   $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($startDate, $currentDate);

  //   $SOTmotivationResultArray = array();
  //   $categoryTbl   = DB::table('sot_motivation_value_records')->where('status','Active')->get();

  //   $query = DB::table('sot_motivation_answers')
  //     ->select('sot_motivation_answers.userId')
  //     ->leftjoin('users','users.id','sot_motivation_answers.userId')
  //     ->leftJoin('departments','departments.id','users.departmentId')
  //     ->where('departments.status','Active')
  //     ->where('users.status','Active')
  //     ->where('sot_motivation_answers.status','Active');

  //   if($includeWeekend != 1){
  //     $query->whereNotIn(DB::raw("(DATE_FORMAT(sot_motivation_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //   }
  //   if(!empty($orgId)){
  //     $query->where('users.orgId',$orgId);
  //   } 
  //   if(!empty($officeId) && empty($departmentId)){
  //     $query->where('users.officeId',$officeId);
  //   }elseif(!empty($officeId) && !empty($departmentId)){
  //     $query->where('users.officeId',$officeId);
  //     $query->where('users.departmentId',$departmentId);
  //   }elseif(empty($officeId) && !empty($departmentId)){
  //     $query->where('departments.departmentId',$departmentId);
  //   }
  //   $query->groupBy('sot_motivation_answers.userId');
  //   $usersList = $query->get();
  //   $totalUser = count($usersList);

  //   // if (!empty($totalUser)){
  //     foreach ($categoryTbl as $sotCvalue){
  //       $query = DB::table('sot_motivation_answers AS sotans')
  //         ->leftJoin('sot_motivation_question_options AS qoption','qoption.id','sotans.optionId')
  //         ->leftjoin('users','users.id','sotans.userId')
  //         ->leftJoin('departments','departments.id','users.departmentId')
  //         ->where('departments.status','Active')
  //         ->where('users.status','Active')                     
  //         ->where('sotans.status','Active')
  //         ->where('qoption.status','Active')
  //         ->where('qoption.category_id',$sotCvalue->id);

  //       if($includeWeekend != 1){
  //         $query->whereNotIn(DB::raw("(DATE_FORMAT(sotans.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //       }
  //       if(!empty($orgId)){
  //         $query->where('sotans.orgId',$orgId);
  //       } 
  //       if(!empty($officeId) && empty($departmentId)){
  //         $query->where('users.officeId',$officeId);
  //       }elseif(!empty($officeId) && !empty($departmentId)){
  //         $query->where('users.officeId',$officeId);
  //         $query->where('users.departmentId',$departmentId);
  //       }elseif(empty($officeId) && !empty($departmentId)){
  //         $query->where('departments.departmentId',$departmentId);
  //       } 
  //       $ansTbl = $query->sum('sotans.answer'); 

  //       $result1['title']      = utf8_encode($sotCvalue->title);
  //       $result1['percentage'] = 0;
  //       if (!empty($totalUser)){
  //         $result1['percentage'] = number_format((float)($ansTbl/$totalUser), 2, '.', '');         
  //       }
  //       array_push($SOTmotivationResultArray, $result1);
  //     }
  //   // }
  //   return $SOTmotivationResultArray;
  }

  public function getMotivationCat(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $categoryId       = Input::get('categoryId');
    $columnColor       = Input::get('columnColor');

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }

    $org = DB::table('organisations')->where('id',$orgId)->first();
    $includeWeekend = $org->include_weekend;

    $arrMain = array();
    if ($categoryId) {
      $queCatTbl = DB::table('sot_motivation_value_records')->where('id',$categoryId)->get();
    }else{
      $queCatTbl = DB::table('sot_motivation_value_records')->get();
    }

    $categoryName = array("");   //first index
    foreach ($queCatTbl as $key => $value) {
      $categoryName[] = $value->title."(%)";
    }
    array_push($arrMain, $categoryName);

    $max = 0;
    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));   //first index of month

      if ($month != date('Y-m')) {
        foreach ($queCatTbl as $cate) {
          $motivationDataQuery = DB::table('sot_motivation_dashboard_graph')
              ->where('date','LIKE',$month."%");
          if (!empty($categoryId)) {
            $motivationDataQuery->where('categoryId',$categoryId);
          }else{
            $motivationDataQuery->where('categoryId',$cate->id);
          }
          if (!empty($orgId)) {
            $motivationDataQuery->where('orgId',$orgId);  
          }
          if(empty($officeId) && empty($departmentId)){
              $motivationDataQuery->whereNull('officeId');
              $motivationDataQuery->whereNull('departmentId');
          }elseif(!empty($officeId) && empty($departmentId)){
              $motivationDataQuery->where('officeId',$officeId);
              $motivationDataQuery->whereNull('departmentId');
          }elseif(!empty($officeId) && !empty($departmentId)){
              $motivationDataQuery->where('officeId',$officeId);
              $motivationDataQuery->where('departmentId',$departmentId);
          }elseif(empty($officeId) && !empty($departmentId)){
              $motivationDataQuery->whereNull('officeId');
              $motivationDataQuery->where('departmentId',$departmentId);
          }
          $motivationData = $motivationDataQuery->first();
          if($motivationData){
            if($includeWeekend != 1){ //Exclude Weekend Days
                $newArr[] = $motivationData->without_weekend;
                if ($motivationData->without_weekend != 0) {
                    $max++;
                }
            }else{
                $newArr[] = $motivationData->with_weekend;
                if ($motivationData->with_weekend != 0) {
                    $max++;
                }
            }
          }else{
              $newArr[] = 0;
          }
        }
      }else{
        if ($includeWeekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $motivationArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateMotivationGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date,$categoryId);

          foreach ($motivationArr as $motiVal) { 
            array_push($newArr, $motiVal['percentage']);
            if ($motiVal['percentage'] != 0) {
                $max++;
            }
          }
      }
      array_push($arrMain, $newArr);
    }

    return array('status'=>true,'arrMain'=>$arrMain,'columnColor'=>$columnColor,'maxCount'=>$max);
  }

  public function getPersonalityTypeValues($perArr=array())
  {
    $orgId            = $perArr["orgId"];
    $officeId         = $perArr["officeId"];
    $departmentId     = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];
    $months           = $perArr["months"];

    // $tribeoFinalArray = array();
    $arrMain = array();
    $perTypeListKey = DB::table('cot_functional_lens_tribe_tips_records')->where('status','Active')->get();

    $keyNameArray = array();
    foreach ($perTypeListKey as $value1){
        $reportKeyArray = json_decode($value1->value);
        $keyArr = array();
        foreach ($reportKeyArray as $value){
            $valuesKey = '';
            $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$value)->first();  

            if(!empty($table)){
              $valuesKey = $table->value;
            }   
            array_push($keyArr, $valuesKey); 
        }
      array_push($keyNameArray, $keyArr);
    }

    $keyRecord = array("");   //first index
    foreach ($keyNameArray as $value) {
        $valueRecord = implode(' ', $value);
        array_push($keyRecord, $valueRecord."(%)");
    }
    array_push($arrMain, $keyRecord);

    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));   //first index of month

      if ($month != date('Y-m')) {
        foreach ($perTypeListKey as $cate) {
          $perTypeDataQuery = DB::table('cot_personality_type_dashboard_graph')
              ->where('categoryId',$cate->id)
              ->where('date','LIKE',$month."%");
          if (!empty($orgId)) {
            $perTypeDataQuery->where('orgId',$orgId);  
          }
          if(empty($officeId) && empty($departmentId)){
              $perTypeDataQuery->whereNull('officeId');
              $perTypeDataQuery->whereNull('departmentId');
          }elseif(!empty($officeId) && empty($departmentId)){
              $perTypeDataQuery->where('officeId',$officeId);
              $perTypeDataQuery->whereNull('departmentId');
          }elseif(!empty($officeId) && !empty($departmentId)){
              $perTypeDataQuery->where('officeId',$officeId);
              $perTypeDataQuery->where('departmentId',$departmentId);
          }elseif(empty($officeId) && !empty($departmentId)){
              $perTypeDataQuery->whereNull('officeId');
              $perTypeDataQuery->where('departmentId',$departmentId);
          }
          $perTypeData = $perTypeDataQuery->first();
          if($perTypeData){
            if($includeWeekend != 1){ //Exclude Weekend Days
                $newArr[] = $perTypeData->without_weekend;
            }else{
                $newArr[] = $perTypeData->with_weekend;
            }
          }else{
              $newArr[] = 0;
          }
        }
      }else{
        if ($includeWeekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $personalityTypeArr = app('App\Http\Controllers\Admin\AdminReportController')->calculatePersonalityTypeGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date);

          foreach ($personalityTypeArr as $personalityTypeVal) { 
            array_push($newArr, $personalityTypeVal);
          }
      }
      array_push($arrMain, $newArr);
    }
    return $arrMain;

    // echo "<pre>";print_r($arrMain);
    // die();


    // $orgId            = $perArr["orgId"];
    // $officeId         = $perArr["officeId"];
    // $departmentId     = $perArr["departmentId"];
    // $includeWeekend   = $perArr["includeWeekend"];

    // //Get weekends
    // $startDate=DB::table('cot_functional_lens_answers')
    //     ->select('created_at')
    //     ->where('orgId',$orgId)
    //     ->orderBy('created_at','ASC')
    //     ->first();


    // if (is_array($startDate) && count($startDate) == 0) {
    //     // $startDate = $date;
    //     $startDate = date('Y-m-d');
    // }else{
    //     $startDate = date('Y-m-d',strtotime($startDate->created_at));
    // }
    // // $currentDate = $date;
    // $currentDate = date('Y-m-d');
    // $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($startDate, $currentDate);

    // $cotFunResultArray = array();

    // $query = DB::table('cot_functional_lens_answers')
    // ->select('cot_functional_lens_answers.userId AS id')
    // ->leftjoin('users','users.id','cot_functional_lens_answers.userId') 
    // ->where('users.status','Active')
    // ->where('cot_functional_lens_answers.status','Active');

    // if($includeWeekend != 1){
    //   $query->whereNotIn(DB::raw("(DATE_FORMAT(cot_functional_lens_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    // }
    // if(!empty($orgId)){
    //   $query->where('users.orgId',$orgId);
    // } 
    // if(!empty($officeId) && empty($departmentId)){
    //   $query->where('users.officeId',$officeId);
    // }elseif(!empty($officeId) && !empty($departmentId)){
    //   $query->where('users.officeId',$officeId);
    //   $query->where('users.departmentId',$departmentId);
    // }elseif(empty($officeId) && !empty($departmentId)){
    //   $query->leftJoin('departments','departments.id','users.departmentId')
    //       ->where('departments.status','Active')   
    //       ->where('departments.departmentId',$departmentId);
    // }
    // $usersList = $query->groupBy('cot_functional_lens_answers.userId')->get();

    // foreach ($usersList as $key => $userValue){

    //   $variable = app('App\Http\Controllers\Admin\CommonController')->getCotFunLensInitialValue();   

    //   $initialValEIArray = array();
    //   foreach ($variable as $value){
    //     $value1 = $value[0];
    //     $value2 = $value[1];

    //     $countEQuery = DB::table('cot_functional_lens_answers AS cfla')
    //       ->select('cflqo.option_name AS optionName')
    //       ->leftJoin('cot_functional_lens_question_options AS cflqo','cflqo.id','cfla.optionId')
    //       ->leftjoin('users','users.id','cfla.userId') 
    //       ->leftJoin('departments','departments.id','users.departmentId')
    //       ->where('departments.status','Active')   
    //       ->where('users.status','Active')
    //       ->where('cfla.status','Active')
    //       ->where('cflqo.status','Active')
    //       ->where('cfla.userId',$userValue->id)
    //       ->where('cflqo.initial_value_id',$value1);

    //     if($includeWeekend != 1){
    //       $countEQuery->whereNotIn(DB::raw("(DATE_FORMAT(cfla.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    //     }
    //     if(!empty($orgId)){
    //       $countEQuery->where('users.orgId',$orgId);
    //     } 
    //     if(!empty($officeId) && empty($departmentId)){
    //       $countEQuery->where('users.officeId',$officeId);
    //     }elseif(!empty($officeId) && !empty($departmentId)){
    //       $countEQuery->where('users.officeId',$officeId);
    //       $countEQuery->where('users.departmentId',$departmentId);
    //     }elseif(empty($officeId) && !empty($departmentId)){
    //       $countEQuery->where('departments.departmentId',$departmentId);
    //     } 
    //     $countE = $countEQuery->get();

    //     $countIQuery = DB::table('cot_functional_lens_answers AS cfla')
    //       ->select('cflqo.option_name AS optionName')
    //       ->leftJoin('cot_functional_lens_question_options AS cflqo','cflqo.id','cfla.optionId')
    //       ->leftjoin('users','users.id','cfla.userId') 
    //       ->leftJoin('departments','departments.id','users.departmentId')
    //       ->where('departments.status','Active')   
    //       ->where('users.status','Active')
    //       ->where('cfla.status','Active')
    //       ->where('cflqo.status','Active')
    //       ->where('cfla.userId',$userValue->id)
    //       ->where('cflqo.initial_value_id',$value2);

    //     if($includeWeekend != 1){
    //       $countIQuery->whereNotIn(DB::raw("(DATE_FORMAT(cfla.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    //     }
    //     if(!empty($orgId)){
    //       $countIQuery->where('users.orgId',$orgId);
    //     } 
    //     if(!empty($officeId) && empty($departmentId)){
    //       $countIQuery->where('users.officeId',$officeId);
    //     }elseif(!empty($officeId) && !empty($departmentId)){
    //       $countIQuery->where('users.officeId',$officeId);
    //       $countIQuery->where('users.departmentId',$departmentId);
    //     }elseif(empty($officeId) && !empty($departmentId)){
    //       $countIQuery->where('departments.departmentId',$departmentId);
    //     } 
    //     $countI = $countIQuery->get();

    //     $initialValEI  ='';
    //     if(count($countE) > count($countI)){
    //       $initialValEI  =  $value1;
    //     }else if (count($countE) < count($countI)){
    //       $initialValEI  =  $value2;    
    //     }else if(count($countE) == count($countI)){     
    //       $initialValEI  =  $value1;
    //     } 
    //     array_push($initialValEIArray, $initialValEI);
    //   }

    //   $valueTypeArray = array();
    //   $valueTypeArray['value'] = array();

    //   if(!empty($initialValEIArray)){
    //        $valueTypeArray = array('value'=>$initialValEIArray);
    //   }

    //   $matchValueArr = array();
    //   for($i=0; $i < count($valueTypeArray['value']); $i++){
    //       $valuesKey = '';
    //       $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$valueTypeArray['value'][$i])->first();  

    //       if(!empty($table)){
    //           $valuesKey = $table->value;
    //       }
    //       array_push($matchValueArr, $valuesKey); 
    //   }

    //   //remove last and first string
    //   $tribeMatchValue = json_encode(array_slice($valueTypeArray['value'], 1, -1));

    //   $tribeTipsList = DB::table('cot_functional_lens_tribe_tips_records')->where('value',$tribeMatchValue)->get();

    //   $tribeTipsArray = array();
    //   foreach ($tribeTipsList as $ttvalue){
    //       $tTips['value']  = $ttvalue->value;       
    //       array_push($cotFunResultArray, $tTips);
    //   }
    // }

    // $ST = 0;
    // $SF = 0;
    // $NF = 0;
    // $NT = 0;

    // foreach ($cotFunResultArray as $finalArrayValue){ 
    //     if ($finalArrayValue['value']=='["7","9"]'){
    //         $ST++;
    //     }elseif ($finalArrayValue['value']=='["7","10"]'){
    //         $SF++;
    //     }elseif ($finalArrayValue['value']=='["8","10"]'){
    //         $NF++;
    //     }elseif ($finalArrayValue['value']=='["8","9"]'){
    //         $NT++;
    //     }
    // }

    // $totalUser = count($usersList);
    // if (!empty($totalUser)){
    //     $stPercent = ($ST/$totalUser)*100;
    //     $sfPercent = ($SF/$totalUser)*100;
    //     $nfPercent = ($NF/$totalUser)*100;
    //     $ntPercent = ($NT/$totalUser)*100;
    // }else{
    //     $stPercent = 0;
    //     $sfPercent = 0;
    //     $nfPercent = 0;
    //     $ntPercent = 0;
    // }

    // $funcLensPercentageArray = array(
    //   'st' => round($stPercent, 2),
    //   'nt' => round($ntPercent, 2),
    //   'nf' => round($nfPercent, 2),
    //   'sf' => round($sfPercent, 2)
    // );

    // $tribeTipsListKey = DB::table('cot_functional_lens_tribe_tips_records')->where('status','Active')->orderBy('id','ASC')->get();

    // $keyNameArray = array();

    // foreach ($tribeTipsListKey as $value1){
    //     $reportKeyArray = json_decode($value1->value);

    //     $keyArr = array();
    //     foreach ($reportKeyArray as $value){
    //         $valuesKey = '';
    //         $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$value)->first();  

    //         if(!empty($table)){
    //           $valuesKey = $table->value;
    //         }   
    //         array_push($keyArr, $valuesKey); 
    //     }
    //   array_push($keyNameArray, $keyArr);
    // }

    // $keyRecord = array();
    // foreach ($keyNameArray as $value) {
    //   $valueRecord = implode(' ', $value);
    //   array_push($keyRecord, $valueRecord);
    // }

    // $result = array_map(function ($keys, $values) {
    //   return array_combine(
    //     ['name', 'percentage'],
    //     [$keys, $values]
    //   );
    // }, $keyRecord, $funcLensPercentageArray);

    // // echo "<pre>";print_r($result);

    // $personalityTypeFinalArray = collect($result);
    // // echo "<pre>";print_r($collection);
    // // die();
    // // $personalityTypeFinalArray = $collection->sortBy('values');

    // // $pTConditionArray = array_filter(array_column($result, 'values'));
    // return $personalityTypeFinalArray;
    // // return array('personalityTypeFinalArray'=>$personalityTypeFinalArray,'pTConditionArray'=>$pTConditionArray);
  }

  public function getPerTypeCat(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $categoryId       = Input::get('categoryId');
    $columnColor       = Input::get('columnColor');

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }

    $org = DB::table('organisations')->where('id',$orgId)->first();
    $includeWeekend = $org->include_weekend;

    $arrMain = array();
    if ($categoryId) {
      $perTypeListKey = DB::table('cot_functional_lens_tribe_tips_records')->where('status','Active')->where('id',$categoryId)->get();
    }else{
      $perTypeListKey = DB::table('cot_functional_lens_tribe_tips_records')->where('status','Active')->get();
    }

    $keyNameArray = array();
    foreach ($perTypeListKey as $value1){
        $reportKeyArray = json_decode($value1->value);
        $keyArr = array();
        foreach ($reportKeyArray as $value){
            $valuesKey = '';
            $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$value)->first();  

            if(!empty($table)){
              $valuesKey = $table->value;
            }   
            array_push($keyArr, $valuesKey); 
        }
      array_push($keyNameArray, $keyArr);
    }

    $keyRecord = array("");   //first index
    foreach ($keyNameArray as $value) {
        $valueRecord = implode(' ', $value);
        array_push($keyRecord, $valueRecord."(%)");
    }
    array_push($arrMain, $keyRecord);

    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));   //first index of month

      if ($month != date('Y-m')) {
        foreach ($perTypeListKey as $cate) {
          $perTypeDataQuery = DB::table('cot_personality_type_dashboard_graph')
              ->where('date','LIKE',$month."%");
          if (!empty($categoryId)) {
            $perTypeDataQuery->where('categoryId',$categoryId);
          }else{
            $perTypeDataQuery->where('categoryId',$cate->id);
          }
          if (!empty($orgId)) {
            $perTypeDataQuery->where('orgId',$orgId);  
          }
          if(empty($officeId) && empty($departmentId)){
              $perTypeDataQuery->whereNull('officeId');
              $perTypeDataQuery->whereNull('departmentId');
          }elseif(!empty($officeId) && empty($departmentId)){
              $perTypeDataQuery->where('officeId',$officeId);
              $perTypeDataQuery->whereNull('departmentId');
          }elseif(!empty($officeId) && !empty($departmentId)){
              $perTypeDataQuery->where('officeId',$officeId);
              $perTypeDataQuery->where('departmentId',$departmentId);
          }elseif(empty($officeId) && !empty($departmentId)){
              $perTypeDataQuery->whereNull('officeId');
              $perTypeDataQuery->where('departmentId',$departmentId);
          }
          $perTypeData = $perTypeDataQuery->first();
          if($perTypeData){
            if($includeWeekend != 1){ //Exclude Weekend Days
                $newArr[] = $perTypeData->without_weekend;
            }else{
                $newArr[] = $perTypeData->with_weekend;
            }
          }else{
              $newArr[] = 0;
          }
        }
      }else{
        if ($includeWeekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $personalityTypeArr = app('App\Http\Controllers\Admin\AdminReportController')->calculatePersonalityTypeGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date,$categoryId);

          foreach ($personalityTypeArr as $personalityTypeVal) { 
            array_push($newArr, $personalityTypeVal);
          }
      }
      array_push($arrMain, $newArr);
    }

    // echo "<pre>";print_r($arrMain);die();
    return array('status'=>true,'arrMain'=>$arrMain,'columnColor'=>$columnColor);
  }

  public function getCultureStructureValues($perArr=array())
  {
    $orgId            = $perArr["orgId"];
    $officeId         = $perArr["officeId"];
    $departmentId     = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];
    $months           = $perArr["months"];

    $arrMain = array();
    $sotCultureStrTbl = DB::table('sot_culture_structure_records')->where('status','Active')->get();

    $categoryName = array("");   //first index
    foreach ($sotCultureStrTbl as $key => $value) {
      $categoryName[$value->id] = $value->title."(%)";
    }
    array_push($arrMain, $categoryName);

    $max = 0;
    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));

      if ($month != date('Y-m')) {
        foreach ($sotCultureStrTbl as $cate) {
          $cultureStructureDataQuery = DB::table('sot_culture_structure_dashboard_graph')
                ->where('categoryId',$cate->id)
                ->where('date','LIKE',$month."%");
          if (!empty($orgId)) {
            $cultureStructureDataQuery->where('orgId',$orgId);  
          }
          if(empty($officeId) && empty($departmentId))
          {
              $cultureStructureDataQuery->whereNull('officeId');
              $cultureStructureDataQuery->whereNull('departmentId');
          }
          elseif(!empty($officeId) && empty($departmentId))
          {
              $cultureStructureDataQuery->where('officeId',$officeId);
              $cultureStructureDataQuery->whereNull('departmentId');
          }
          elseif(!empty($officeId) && !empty($departmentId))
          {
              $cultureStructureDataQuery->where('officeId',$officeId);
              $cultureStructureDataQuery->where('departmentId',$departmentId);
          }
          elseif(empty($officeId) && !empty($departmentId))
          {
              $cultureStructureDataQuery->whereNull('officeId');
              $cultureStructureDataQuery->where('departmentId',$departmentId);
          }
          $cultureStructureData = $cultureStructureDataQuery->first();
          if($cultureStructureData){
            if($includeWeekend != 1){ //Exclude Weekend Days
                $newArr[] = $cultureStructureData->without_weekend;
                if ($cultureStructureData->without_weekend != 0) {
                    $max++;
                }
            }else{
                $newArr[] = $cultureStructureData->with_weekend;
                if ($cultureStructureData->with_weekend != 0) {
                    $max++;
                }
            }
          }else{
              $newArr[] = 0;
          }
        }
      }else{
        if ($includeWeekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $cultureStructureArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateCultureStructureGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date);

          foreach ($cultureStructureArr as $culStrucVal) { 
            array_push($newArr, $culStrucVal['percentage']);
            if ($culStrucVal['percentage'] != 0) {
                $max++;
            }
          }
      }
      array_push($arrMain, $newArr);
    }
    // echo "<pre>";print_r($arrMain);die();

    return array('arrMain'=>$arrMain,'maxCount'=>$max);
    

    // //Get weekends
    // $startDate=DB::table('sot_answers')
    //     ->select('sot_answers.created_at')
    //     ->leftjoin('users','users.id','sot_answers.userId')
    //     ->where('users.status','Active')
    //     ->where('sot_answers.status','Active')
    //     ->where('users.orgId',$orgId)
    //     ->orderBy('sot_answers.created_at','ASC')
    //     ->first();

    // if (count($startDate) == 0) {
    //     $startDate = date('Y-m-d');
    // }else{
    //     $startDate = date('Y-m-d',strtotime($startDate->created_at));
    // }
    // $currentDate = date('Y-m-d');
    // $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($startDate, $currentDate);

    // $sotCultureStrTbl = DB::table('sot_culture_structure_records')->where('status','Active')->get();

    // $sotArray = array();
    // $totalCount = 0;
    // foreach ($sotCultureStrTbl as $key => $value) {
    //   $query = DB::table('sot_answers')
    //     // ->select(DB::raw('sum(score) AS count'))
    //     ->leftJoin('sot_questionnaire_records','sot_questionnaire_records.id','sot_answers.question_id')
    //     ->leftjoin('users','users.id','sot_answers.userId')
    //     ->leftjoin('departments','departments.id','users.departmentId')
    //     ->where('users.status','Active')
    //     ->where('departments.status','Active')
    //     ->where('sot_answers.status','Active')
    //     ->where('sot_questionnaire_records.status','Active')
    //     ->where('sot_questionnaire_records.type',$value->type);

    //   if ($includeWeekend != 1) {
    //     $query->whereNotIn(DB::raw("(DATE_FORMAT(sot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    //   }
    //   if(!empty($orgId)){
    //     $query->where('users.orgId',$orgId);
    //   } 
    //   if(!empty($officeId) && empty($departmentId)){
    //     $query->where('users.officeId',$officeId);
    //   }elseif(!empty($officeId) && !empty($departmentId)){
    //     $query->where('users.officeId',$officeId);
    //     $query->where('users.departmentId',$departmentId);
    //   }elseif(empty($officeId) && !empty($departmentId)){
    //     $query->where('departments.departmentId',$departmentId);
    //   }   
    //   $sotCount['count'] = $query->count();
    //   $sotCount['title'] = $value->title;

    //   $totalCount += $sotCount['count'];

    //   array_push($sotArray, $sotCount);
    // }
    // $cultureStructureArray = array();
    // foreach ($sotArray as $val) {
    //   $sot['percentage']  = round((($val['count']/$totalCount)*100),2);
    //   $sot['title']       = $val['title'];
    //   array_push($cultureStructureArray, $sot);
    // }
    // // echo "<pre>";print_r($cultureStructureArray);
    // // die();
    // // echo "<pre>";print_r($totalCount);
    // // die();

    // $collection = collect($cultureStructureArray);
    // $sotFinalArray = $collection->sortBy('percentage');

    // $CsConditionArray = array_filter(array_column($sotArray, 'count'));

    // // echo "<pre>";print_r($sotFinalArray);
    // // echo "<pre>";print_r($CsConditionArray);die();
    
    // return array('sotFinalArray'=>$sotFinalArray,'CsConditionArray'=>$CsConditionArray);
  }

  public function getCultureStructureCat(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $categoryId       = Input::get('categoryId');
    $columnColor       = Input::get('columnColor');

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }

    $org = DB::table('organisations')->where('id',$orgId)->first();
    $includeWeekend = $org->include_weekend;

    $arrMain = array();
    if ($categoryId) {
      $sotCultureStrTbl = DB::table('sot_culture_structure_records')->where('status','Active')->where('id',$categoryId)->get();
    }else{
      $sotCultureStrTbl = DB::table('sot_culture_structure_records')->where('status','Active')->get();
    }

    $categoryName = array("");   //first index
    foreach ($sotCultureStrTbl as $key => $value) {
      $categoryName[] = $value->title."(%)";
    }
    array_push($arrMain, $categoryName);

    $max = 0;
    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));

      if ($month != date('Y-m')) {
        foreach ($sotCultureStrTbl as $cate) {
          $cultureStructureDataQuery = DB::table('sot_culture_structure_dashboard_graph')
                ->where('date','LIKE',$month."%");
          if (!empty($categoryId)) {
            $cultureStructureDataQuery->where('categoryId',$categoryId);
          }else{
            $cultureStructureDataQuery->where('categoryId',$cate->id);
          }
          if (!empty($orgId)) {
            $cultureStructureDataQuery->where('orgId',$orgId);  
          }
          if(empty($officeId) && empty($departmentId))
          {
              $cultureStructureDataQuery->whereNull('officeId');
              $cultureStructureDataQuery->whereNull('departmentId');
          }
          elseif(!empty($officeId) && empty($departmentId))
          {
              $cultureStructureDataQuery->where('officeId',$officeId);
              $cultureStructureDataQuery->whereNull('departmentId');
          }
          elseif(!empty($officeId) && !empty($departmentId))
          {
              $cultureStructureDataQuery->where('officeId',$officeId);
              $cultureStructureDataQuery->where('departmentId',$departmentId);
          }
          elseif(empty($officeId) && !empty($departmentId))
          {
              $cultureStructureDataQuery->whereNull('officeId');
              $cultureStructureDataQuery->where('departmentId',$departmentId);
          }
          $cultureStructureData = $cultureStructureDataQuery->first();
          if($cultureStructureData){
            if($includeWeekend != 1){ //Exclude Weekend Days
                $newArr[] = $cultureStructureData->without_weekend;
                if ($cultureStructureData->without_weekend != 0) {
                    $max++;
                }
            }else{
                $newArr[] = $cultureStructureData->with_weekend;
                if ($cultureStructureData->with_weekend != 0) {
                    $max++;
                }
            }
          }else{
              $newArr[] = 0;
          }
        }
      }else{
        if ($includeWeekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $cultureStructureArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateCultureStructureGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date,$categoryId);

          foreach ($cultureStructureArr as $culStrucVal) { 
            array_push($newArr, $culStrucVal['percentage']);
            if ($culStrucVal['percentage'] != 0) {
                $max++;
            }
          }
      }
      array_push($arrMain, $newArr);
    }
    return array('status'=>true,'arrMain'=>$arrMain,'columnColor'=>$columnColor,'maxCount'=>$max);
  }

  public function getDashboardSubDiagGraph(Request $request)
  {
    $arrMain = array();
    /*
    $val1 = "15";
    $val1 = (int)$val1;
    $arr1 = array("","subcat1","subcat2");
    $arr2 = array("jan",$val1,56);
    $arr3 = array("feb",15,50);
    $arr4 = array("march",10,19);
    array_push($arrMain, $arr1);
    array_push($arrMain, $arr2);
    array_push($arrMain, $arr3);
    array_push($arrMain, $arr4);
    */

    //$arrMain = json_encode($arrMain);
    //return array("status"=>true,"arrMain"=>$arrMain); die;

    $orgId          = base64_decode($request->orgId); 
    $officeId       = $request->officeId;
    $departmentId   = $request->departmentId;
    $category       = $request->category;

    $organisation = DB::table('organisations')->where('id',$orgId)->where('status','Active')->first();

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }

    $diagQues = DB::table('diagnostic_questions')->where('category_id',$category)->get();
    $measures = array("");   //first index
    $qusId = array();
    foreach ($diagQues as $key => $value) {
      $measures[] = $value->measure."(%)";
      $qusId[$value->id] = $value->measure."(%)";
    }
    array_push($arrMain, $measures);
    
    $subDiagFinalArray = array();
    foreach ($months as $key => $month) {
        $newArr = array(date('My',strtotime($month)));   //first index of month
        
        if ($month != date('Y-m')) {
          foreach ($diagQues as $ques) {
              $subDiagDataQuery = DB::table('diagnostic_report_subgraph')
                      ->where('categoryId',$category)
                      ->where('quesId',$ques->id)
                      ->where('date','LIKE',$month."%");
              if (!empty($orgId)) {
                $subDiagDataQuery->where('orgId',$orgId);  
              }
              if(empty($officeId) && empty($departmentId)){
                  $subDiagDataQuery->whereNull('officeId');
                  $subDiagDataQuery->whereNull('departmentId');
              }elseif(!empty($officeId) && empty($departmentId)){
                  $subDiagDataQuery->where('officeId',$officeId);
                  $subDiagDataQuery->whereNull('departmentId');
              }elseif(!empty($officeId) && !empty($departmentId)){
                  $subDiagDataQuery->where('officeId',$officeId);
                  $subDiagDataQuery->where('departmentId',$departmentId);
              }elseif(empty($officeId) && !empty($departmentId)){
                  $subDiagDataQuery->whereNull('officeId');
                  $subDiagDataQuery->where('departmentId',$departmentId);
              }
              $subDiagData = $subDiagDataQuery->first();
              if($subDiagData){
                if($organisation->include_weekend != 1){
                    $newArr[] = $subDiagData->without_weekend;
                }else{
                    $newArr[] = $subDiagData->with_weekend;
                }
              }else{
                  $newArr[] = 0;
              }
          }
        }else{
          if ($organisation->include_weekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $subDiagnosticsArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateDiagnosticSubGraph(base64_encode($orgId),$officeId,$departmentId,$category,$isExclude,$date);

          if (empty($subDiagnosticsArr)) {
            foreach ($diagQues as $subDiagTbl) {
              array_push($newArr, 0);
            }
          }
          foreach ($subDiagnosticsArr as $subDiagnosticsVal) { 
            array_push($newArr, $subDiagnosticsVal['percentage']);
          }
        }
        array_push($arrMain, $newArr);
    }

    //echo "<pre>";print_r($subDiagFinalArray);die();
    //return array('status'=>true,'subDiagFinalArray'=>$subDiagFinalArray,'measures'=>$measure);
    return array('status'=>true,'arrMain'=>$arrMain,'qusId'=>$qusId);
  }

  public function getDiagnosticSubCat(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $qusId            = Input::get('qusId');
    $categoryId       = Input::get('categoryId');
    $columnColor      = Input::get('columnColor');

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }

    $org = DB::table('organisations')->where('id',$orgId)->where('status','Active')->first();
    $includeWeekend = $org->include_weekend;

    $arrMain = array();

    if ($qusId) {
      $diagQues = DB::table('diagnostic_questions')->where('category_id',$categoryId)->where('id',$qusId)->get();
    }else{
      $diagQues = DB::table('diagnostic_questions')->where('category_id',$categoryId)->get();
    }
    
    $measures = array("");   //first index
    foreach ($diagQues as $key => $value) {
      $measures[] = $value->measure."(%)";
    }
    array_push($arrMain, $measures);


    // $categoryName = array("");   //first index
    // foreach ($diagnosticQueCatTbl as $key => $value) {
    //   $categoryName[] = $value->title."(%)";
    // }
    // array_push($arrMain, $categoryName);
    $max = 0;
    foreach ($months as $key => $month) {
        $newArr = array(date('My',strtotime($month)));   //first index of month

        if ($month != date('Y-m')) {
          foreach ($diagQues as $ques) {
              $subDiagDataQuery = DB::table('diagnostic_report_subgraph')
                      ->where('categoryId',$categoryId)
                      ->where('date','LIKE',$month."%");
              if (!empty($qusId)) {
                $subDiagDataQuery->where('quesId',$qusId);
              }else{
                $subDiagDataQuery->where('quesId',$ques->id);
              }
              if (!empty($orgId)) {
                $subDiagDataQuery->where('orgId',$orgId);  
              }
              if(empty($officeId) && empty($departmentId)){
                  $subDiagDataQuery->whereNull('officeId');
                  $subDiagDataQuery->whereNull('departmentId');
              }elseif(!empty($officeId) && empty($departmentId)){
                  $subDiagDataQuery->where('officeId',$officeId);
                  $subDiagDataQuery->whereNull('departmentId');
              }elseif(!empty($officeId) && !empty($departmentId)){
                  $subDiagDataQuery->where('officeId',$officeId);
                  $subDiagDataQuery->where('departmentId',$departmentId);
              }elseif(empty($officeId) && !empty($departmentId)){
                  $subDiagDataQuery->whereNull('officeId');
                  $subDiagDataQuery->where('departmentId',$departmentId);
              }
              $subDiagData = $subDiagDataQuery->first();
              if($subDiagData){
                if($includeWeekend != 1){
                    $newArr[] = $subDiagData->without_weekend;
                    if ($subDiagData->without_weekend != 0) {
                        $max++;
                    }
                }else{
                    $newArr[] = $subDiagData->with_weekend;
                    if ($subDiagData->with_weekend != 0) {
                        $max++;
                    }
                }
              }else{
                  $newArr[] = 0;
              }
          }
        }else{
          if ($includeWeekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $subDiagnosticsArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateDiagnosticSubGraph(base64_encode($orgId),$officeId,$departmentId,$categoryId,$isExclude,$date,$qusId);

          if (empty($subDiagnosticsArr)) {
            foreach ($diagQues as $subDiagTbl) {
              array_push($newArr, 0);
            }
          }
          foreach ($subDiagnosticsArr as $subDiagnosticsVal) { 
            array_push($newArr, $subDiagnosticsVal['percentage']);
            if ($subDiagnosticsVal['percentage'] != 0) {
                $max++;
            }
          }
        }
        array_push($arrMain, $newArr);
    }
    return array('status'=>true,'arrMain'=>$arrMain,'columnColor'=>$columnColor,'maxCount'=>$max);
  }

  public function getDOTBeliefs($perArr=array())
  {
    $orgId            = $perArr["orgId"];
    $officeId         = $perArr["officeId"];
    $departmentId     = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];
    $months           = $perArr["months"];

    $arrMain = array();
    //Get info of org
    $org = DB::table('organisations')->where('id',$orgId)->first();

    $dots = DB::table('dots')->where('orgId',$orgId)->first();

    $categoryName = array("");   //first index

    if (empty($dots)) {
      $categoryName[] = "";
    }

    $beliefId = array();
    if (!empty($dots)) {
      $dotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->orderBy('id','ASC')
        ->get();
      foreach ($dotBeliefs as $key => $value) {
        $categoryName[]       = $value->name;
        $beliefId[$value->id] = $value->name;
      }
    }
    array_push($arrMain, $categoryName);
    $maxCount = 0;
    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));

      if ($month != date('Y-m')) {
        if (!empty($dots)) {
          $dotBeliefs = DB::table('dots_beliefs')
            ->where('status','Active')
            ->where('dotId',$dots->id)
            ->orderBy('id','ASC')
            ->get();
          foreach ($dotBeliefs as $cate) {
            $dotBeliefDataQuery = DB::table('dot_belief_graph')
                  ->where('beliefId',$cate->id)
                  ->where('date','LIKE',$month."%");
            if (!empty($orgId)) {
              $dotBeliefDataQuery->where('orgId',$orgId);  
            }
            if(empty($officeId) && empty($departmentId))
            {
                $dotBeliefDataQuery->whereNull('officeId');
                $dotBeliefDataQuery->whereNull('departmentId');
            }
            elseif(!empty($officeId) && empty($departmentId))
            {
                $dotBeliefDataQuery->where('officeId',$officeId);
                $dotBeliefDataQuery->whereNull('departmentId');
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $dotBeliefDataQuery->where('officeId',$officeId);
                $dotBeliefDataQuery->where('departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $dotBeliefDataQuery->whereNull('officeId');
                $dotBeliefDataQuery->where('departmentId',$departmentId);
            }
            $dotBeliefData = $dotBeliefDataQuery->first();
            if($dotBeliefData){
              // $maxCount++;
              if($includeWeekend != 1){ //Exclude Weekend Days
                  $newArr[] = $dotBeliefData->without_weekend;
                  if ($dotBeliefData->without_weekend != 0) {
                      $maxCount++;
                  }
              }else{
                  $newArr[] = $dotBeliefData->with_weekend;
                  if ($dotBeliefData->with_weekend != 0) {
                      $maxCount++;
                  }
              }
            }else{
                $newArr[] = 0;
            }
          }
        }
        if (empty($dots)) {
          $newArr[] = 0;
        }
      }else{
        if (!empty($dots)) {
          if ($includeWeekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $dotBeliefArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateDotBeliefGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date);

          foreach ($dotBeliefArr as $dotBeliefVal) { 
            array_push($newArr, $dotBeliefVal['rating']);
            if ($dotBeliefVal['rating'] != 0) {
                $maxCount++;
            }
          }
        }elseif (empty($dots)) {
          $newArr[] = 0;
        }
      }
      array_push($arrMain, $newArr);
    }
    return array('arrMain'=>$arrMain,'maxCount'=>$maxCount,'beliefId'=>$beliefId);
    // return $arrMain;


    // //Get info of org
    // $org = DB::table('organisations')->where('id',$orgId)->first();

    // $dots = DB::table('dots')->where('orgId',$orgId)->first();
    // $beliefArray = array();
    // if (!empty($dots)) {
    //   $dotBeliefs = DB::table('dots_beliefs')
    //     ->where('status','Active')
    //     ->where('dotId',$dots->id)
    //     ->orderBy('id','ASC')
    //     ->get();

    //   foreach ($dotBeliefs as $key => $bValue){
    //     $bquery = DB::table('dot_values_ratings')
    //             ->leftjoin('users','users.id','dot_values_ratings.userId')
    //             ->leftJoin('departments','departments.id','users.departmentId')
    //             ->where('dot_values_ratings.beliefId',$bValue->id)
    //             ->where('departments.status','Active')
    //             ->where('users.status','Active')
    //             ->where('dot_values_ratings.status','Active');

    //     if($includeWeekend != 1){
    //       $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
    //       $currentDate    = date('Y-m-d'); 
    //       //Get weekends
    //       $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
    //       $bquery->whereNotIn(DB::raw("(DATE_FORMAT(dot_values_ratings.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    //     }
    //     if(!empty($officeId) && empty($departmentId))
    //     {
    //       $bquery->where('users.officeId',$officeId);
    //     }
    //     elseif(!empty($officeId) && !empty($departmentId))
    //     {
    //       $bquery->where('users.officeId',$officeId);
    //       $bquery->where('users.departmentId',$departmentId);
    //     }
    //     elseif(empty($officeId) && !empty($departmentId))
    //     {
    //       $bquery->where('departments.departmentId',$departmentId);
    //     }
    //     $bRatings = $bquery->avg('ratings');
    //     $beliefRatings['rating'] = 0;
    //     if($bRatings){
    //       $beliefRatings['rating'] = round(($bRatings-1),2);
    //     }
    //     $beliefRatings['beliefName'] = $bValue->name;
    //     array_push($beliefArray, $beliefRatings);
    //   }
    // }
    // $maxBelief = max($beliefArray);

    // $max = $maxBelief['rating'];
    // return array($beliefArray,$max);
    // // return $beliefArray;
  }

  public function getDotBeliefCat(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $beliefId         = Input::get('categoryId');
    $columnColor      = Input::get('columnColor');

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }

    $arrMain = array();
    //Get info of org
    $org = DB::table('organisations')->where('id',$orgId)->where('status','Active')->first();
    $includeWeekend = $org->include_weekend;

    $dots = DB::table('dots')->where('orgId',$orgId)->where('status','Active')->first();

    $categoryName = array("");   //first index

    if (empty($dots)) {
      $categoryName[] = "";
    }
    $beliefName = array();
    if (!empty($dots)) {

      if ($beliefId) {
        $dotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->where('id',$beliefId)
        ->orderBy('id','ASC')
        ->get();
      }else{
        $dotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->orderBy('id','ASC')
        ->get();
      }
      // $dotBeliefs = DB::table('dots_beliefs')
      //   ->where('status','Active')
      //   ->where('dotId',$dots->id)
      //   ->orderBy('id','ASC')
      //   ->get();
      
      foreach ($dotBeliefs as $key => $value) {
        $categoryName[]       = $value->name;
        $beliefName[$value->id] = $value->name;
      }
    }
    array_push($arrMain, $categoryName);
    $maxCount = 0;
    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));

      if ($month != date('Y-m')) {
        if (!empty($dots)) {
          if (!empty($beliefId)) {
            $dotBeliefs = DB::table('dots_beliefs')
            ->where('status','Active')
            ->where('dotId',$dots->id)
            ->where('id',$beliefId)
            ->orderBy('id','ASC')
            ->get();
          }else{
            $dotBeliefs = DB::table('dots_beliefs')
            ->where('status','Active')
            ->where('dotId',$dots->id)
            ->orderBy('id','ASC')
            ->get();
          }
          foreach ($dotBeliefs as $cate) {
            $dotBeliefDataQuery = DB::table('dot_belief_graph')
                  ->where('date','LIKE',$month."%");
            if (!empty($beliefId)) {
              $dotBeliefDataQuery->where('beliefId',$beliefId);
            }else{
              $dotBeliefDataQuery->where('beliefId',$cate->id);
            }
            if (!empty($orgId)) {
              $dotBeliefDataQuery->where('orgId',$orgId);  
            }
            if(empty($officeId) && empty($departmentId))
            {
                $dotBeliefDataQuery->whereNull('officeId');
                $dotBeliefDataQuery->whereNull('departmentId');
            }
            elseif(!empty($officeId) && empty($departmentId))
            {
                $dotBeliefDataQuery->where('officeId',$officeId);
                $dotBeliefDataQuery->whereNull('departmentId');
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $dotBeliefDataQuery->where('officeId',$officeId);
                $dotBeliefDataQuery->where('departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $dotBeliefDataQuery->whereNull('officeId');
                $dotBeliefDataQuery->where('departmentId',$departmentId);
            }
            $dotBeliefData = $dotBeliefDataQuery->first();
            if($dotBeliefData){
              // $maxCount++;
              if($includeWeekend != 1){ //Exclude Weekend Days
                  $newArr[] = $dotBeliefData->without_weekend;
                  if ($dotBeliefData->without_weekend != 0) {
                      $maxCount++;
                  }
              }else{
                  $newArr[] = $dotBeliefData->with_weekend;
                  if ($dotBeliefData->with_weekend != 0) {
                      $maxCount++;
                  }
              }
            }else{
                $newArr[] = 0;
            }
          }
        }
        if (empty($dots)) {
          $newArr[] = 0;
        }
      }else{
        if (!empty($dots)) {
          if ($includeWeekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $dotBeliefArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateDotBeliefGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date,$beliefId);

          foreach ($dotBeliefArr as $dotBeliefVal) { 
            array_push($newArr, $dotBeliefVal['rating']);
            if ($dotBeliefVal['rating'] != 0) {
                $maxCount++;
            }
          }
        }elseif (empty($dots)) {
          $newArr[] = 0;
        }
      }
      array_push($arrMain, $newArr);
    }

    // echo "<pre>";print_r($arrMain);die();
    return array('arrMain'=>$arrMain,'maxCount'=>$maxCount,'beliefName'=>$beliefName,'columnColor'=>$columnColor);
  }

  public function getDashboardDotValues(Request $request)
  {
    $orgId          = base64_decode($request->orgId); 
    $officeId       = $request->officeId;
    $departmentId   = $request->departmentId;
    $belief         = trim($request->belief);

    $arrMain = array();

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }

    //Get info of org
    $org = DB::table('organisations')->where('id',$orgId)->first();

    $dots = DB::table('dots')->where('orgId',$orgId)->first();

    $beliefData = DB::table('dots_beliefs')->where('status','Active')->where('name',$belief)->first();

    if (!empty($beliefData)) {
      $beliefId = $beliefData->id;
    }
    $categoryName = array("");   //first index

    if (empty($dots)) {
      $categoryName[] = "";
    }

    if (!empty($dots)) {
      $dotValues = DB::table('dots_values')
        ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
        ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
        ->where('dots_values.status','Active')
        ->where('dot_value_list.status','Active')
        ->where('dots_values.beliefId',$beliefId)
        ->orderBy('dot_value_list.name','ASC')
        ->get();
      $valueName = array();
      foreach ($dotValues as $key => $value) {
        $categoryName[]         = $value->name;
        $valueName[$value->id]  = $value->name;
      }
    }
    array_push($arrMain, $categoryName);
    $maxCount = 0;
    foreach ($months as $month) {
      $newArr = array(date('My',strtotime($month)));

      if ($month != date('Y-m')) {
        if (!empty($dots)) {
          $dotValues = DB::table('dots_values')
            ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
            ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
            ->where('dots_values.status','Active')
            ->where('dot_value_list.status','Active')
            ->where('dots_values.beliefId',$beliefId)
            ->orderBy('dot_value_list.name','ASC')
            ->get();
          foreach ($dotValues as $val) {
            $dotValuesDataQuery = DB::table('dot_values_graph')
                  ->where('beliefId',$beliefId)
                  ->where('valueId',$val->id)
                  ->where('date','LIKE',$month."%");
            if (!empty($orgId)) {
              $dotValuesDataQuery->where('orgId',$orgId);  
            }
            if(empty($officeId) && empty($departmentId))
            {
                $dotValuesDataQuery->whereNull('officeId');
                $dotValuesDataQuery->whereNull('departmentId');
            }
            elseif(!empty($officeId) && empty($departmentId))
            {
                $dotValuesDataQuery->where('officeId',$officeId);
                $dotValuesDataQuery->whereNull('departmentId');
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $dotValuesDataQuery->where('officeId',$officeId);
                $dotValuesDataQuery->where('departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $dotValuesDataQuery->whereNull('officeId');
                $dotValuesDataQuery->where('departmentId',$departmentId);
            }
            $dotValuesData = $dotValuesDataQuery->first();
            if($dotValuesData){
              // $maxCount++;
              if($org->include_weekend != 1){
                  $newArr[] = $dotValuesData->without_weekend;
                  if ($dotValuesData->without_weekend != 0) {
                      $maxCount++;
                  }
              }else{
                  $newArr[] = $dotValuesData->with_weekend;
                  if ($dotValuesData->with_weekend != 0) {
                      $maxCount++;
                  }
              }
            }else{
                $newArr[] = 0;
            }
          }
        }
        if (empty($dots)) {
          $newArr[] = 0;
        }
      }else{
        if (!empty($dots)) {
          if ($org->include_weekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $dotBeliefArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateDotValuesGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$beliefId,$dots->id,$date);

          foreach ($dotBeliefArr as $dotBeliefVal) { 
            array_push($newArr, $dotBeliefVal['rating']);
            if ($dotBeliefVal['rating'] != 0) {
                $maxCount++;
            }
          }
        }elseif (empty($dots)) {
          $newArr[] = 0;
        }
      }
      array_push($arrMain, $newArr);
    }

    // //Get info of org
    // $org = DB::table('organisations')->where('id',$orgId)->first();

    

    // $dotValues = DB::table('dots_values')
    //             ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
    //             ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
    //             ->where('dots_values.status','Active')
    //             ->where('dot_value_list.status','Active')
    //             ->where('dots_values.beliefId',$beliefData->id)           
    //             ->orderBy('dot_value_list.name','ASC')
    //             ->get();

    // $valuesArray = array(''); //First index
    // array_push($arrMain, $valuesArray);            
    // foreach ($dotValues as $key => $vValue) {
    //     $valuesArray = array();
    //     $query = DB::table('dot_values_ratings')
    //             ->leftjoin('users','users.id','dot_values_ratings.userId')
    //             ->leftJoin('departments','departments.id','users.departmentId')
    //             ->where('departments.status','Active')
    //             ->where('users.status','Active')
    //             ->where('dot_values_ratings.status','Active')
    //             ->where('dot_values_ratings.valueId', $vValue->id)
    //             ->where('dot_values_ratings.beliefId',$beliefData->id)
    //             ->where('dot_values_ratings.dotId', $beliefData->dotId);     
    //     if($org->include_weekend != 1){
    //         $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
    //         $currentDate    = date('Y-m-d'); 
    //         //Get weekends
    //         $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
    //         $query->whereNotIn(DB::raw("(DATE_FORMAT(dot_values_ratings.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    //     }
    //     if(!empty($officeId) && empty($departmentId)){
    //       $query->where('users.officeId',$officeId);
    //     }elseif(!empty($officeId) && !empty($departmentId)){
    //       $query->where('users.officeId',$officeId);
    //       $query->where('users.departmentId',$departmentId);
    //     }elseif(empty($officeId) && !empty($departmentId)){
    //       $query->where('departments.departmentId',$departmentId);
    //     }
    //     $vRatings = $query->avg('ratings');
    //     //$vResult['valueId']      = $vValue->id;
    //     //$vResult['valueName']    = ucfirst($vValue->name);
    //     array_push($valuesArray, ucfirst($vValue->name));
    //     if($vRatings){
    //       //$vResult['valueRatings'] = $vRatings-1;
    //       $valueRatings = $vRatings-1;
    //     }else{
    //      // $vResult['valueRatings'] = 0;
    //       $valueRatings = 0;
    //     }
    //     array_push($valuesArray, $valueRatings);
    //     //array_push($valuesArray, $vResult);
    //     array_push($arrMain, $valuesArray);
    // }
    return array('status'=>true,'arrMain'=>$arrMain,'maxCount'=>$maxCount,'valueName'=>$valueName);
  }

  public function getDotValueCat(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $beliefId         = trim(Input::get('beliefId'));
    $valueId          = Input::get('valueId');
    $columnColor      = Input::get('columnColor');

    $arrMain = array();

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }

    //Get info of org
    $org = DB::table('organisations')->where('id',$orgId)->where('status','Active')->first();
    $includeWeekend = $org->include_weekend;

    $dots = DB::table('dots')->where('orgId',$orgId)->where('status','Active')->first();

    $beliefData = DB::table('dots_beliefs')->where('status','Active')->where('name',$beliefId)->first();

    if (!empty($beliefData)) {
      $beliefId = $beliefData->id;
    }
    $categoryName = array("");   //first index

    if (empty($dots)) {
      $categoryName[] = "";
      $valueName[] = "";
    }

    if (!empty($dots)) {
      if (!empty($valueId)) {
        $dotValues = DB::table('dots_values')
        ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
        ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
        ->where('dots_values.status','Active')
        ->where('dot_value_list.status','Active')
        ->where('dots_values.beliefId',$beliefId)
        ->where('dots_values.id',$valueId)
        ->orderBy('dot_value_list.name','ASC')
        ->get();
      }else{
        $dotValues = DB::table('dots_values')
        ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
        ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
        ->where('dots_values.status','Active')
        ->where('dot_value_list.status','Active')
        ->where('dots_values.beliefId',$beliefId)
        ->orderBy('dot_value_list.name','ASC')
        ->get();
      }
      $valueName = array();
      foreach ($dotValues as $key => $value) {
        $categoryName[]         = $value->name;
        $valueName[$value->id]  = $value->name;
      }
    }
    array_push($arrMain, $categoryName);

    $maxCount = 0;
    foreach ($months as $month) {
      $newArr = array(date('My',strtotime($month)));

      if ($month != date('Y-m')) {
        if (!empty($dots)) {
          if (!empty($valueId)) {
            $dotValues = DB::table('dots_values')
            ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
            ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
            ->where('dots_values.status','Active')
            ->where('dot_value_list.status','Active')
            ->where('dots_values.beliefId',$beliefId)
            ->where('dots_values.id',$valueId)
            ->orderBy('dot_value_list.name','ASC')
            ->get();
          }else{
            $dotValues = DB::table('dots_values')
            ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
            ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
            ->where('dots_values.status','Active')
            ->where('dot_value_list.status','Active')
            ->where('dots_values.beliefId',$beliefId)
            ->orderBy('dot_value_list.name','ASC')
            ->get();
          }
          foreach ($dotValues as $val) {
            $dotValuesDataQuery = DB::table('dot_values_graph')
                  ->where('beliefId',$beliefId)
                  ->where('date','LIKE',$month."%");
            if (!empty($valueId)) {
              $dotValuesDataQuery->where('valueId',$valueId);
            }else{
              $dotValuesDataQuery->where('valueId',$val->id);
            }
            if (!empty($orgId)) {
              $dotValuesDataQuery->where('orgId',$orgId);  
            }
            if(empty($officeId) && empty($departmentId))
            {
                $dotValuesDataQuery->whereNull('officeId');
                $dotValuesDataQuery->whereNull('departmentId');
            }
            elseif(!empty($officeId) && empty($departmentId))
            {
                $dotValuesDataQuery->where('officeId',$officeId);
                $dotValuesDataQuery->whereNull('departmentId');
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $dotValuesDataQuery->where('officeId',$officeId);
                $dotValuesDataQuery->where('departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $dotValuesDataQuery->whereNull('officeId');
                $dotValuesDataQuery->where('departmentId',$departmentId);
            }
            $dotValuesData = $dotValuesDataQuery->first();
            if($dotValuesData){
              // $maxCount++;
              if($org->include_weekend != 1){
                  $newArr[] = $dotValuesData->without_weekend;
                  if ($dotValuesData->without_weekend != 0) {
                      $maxCount++;
                  }
              }else{
                  $newArr[] = $dotValuesData->with_weekend;
                  if ($dotValuesData->with_weekend != 0) {
                      $maxCount++;
                  }
              }
            }else{
                $newArr[] = 0;
            }
          }
        }
        if (empty($dots)) {
          $newArr[] = 0;
        }
      }else{
        if (!empty($dots)) {
          if ($org->include_weekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $dotValuesArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateDotValuesGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$beliefId,$dots->id,$date,$valueId);

          foreach ($dotValuesArr as $dotValuesVal) { 
            array_push($newArr, $dotValuesVal['rating']);
            if ($dotValuesVal['rating'] != 0) {
                $maxCount++;
            }
          }
        }elseif (empty($dots)) {
          $newArr[] = 0;
        }
      }
      array_push($arrMain, $newArr);
    }
    // echo "<pre>";print_r($arrMain);die();
    return array('status'=>true,'arrMain'=>$arrMain,'maxCount'=>$maxCount,'valueName'=>$valueName,'columnColor'=>$columnColor);
  }

  public function getThumbsupBeliefs($perArr=array())
  {
    $orgId            = $perArr["orgId"];
    $officeId         = $perArr["officeId"];
    $departmentId     = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];
    $months           = $perArr["months"];

    $dots = DB::table('dots')->where('orgId',$orgId)->first();

    $arrMain = array();

    $dotValuesArray = array("");
    if (empty($dots)) {
      array_push($dotValuesArray,"");
    }

    $beliefsName = array();
    if (!empty($dots)) {
      $dotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->orderBy('id','ASC')
        ->get();
      foreach ($dotBeliefs as $key => $bValue){
        array_push($dotValuesArray, $bValue->name);
        $beliefsName[$bValue->id] = $bValue->name;
      }
    }
    array_push($arrMain,$dotValuesArray);

    $maxCount = 0;
    foreach ($months as $month) {
      $newArr = array(date('My',strtotime($month)));
      
      if ($month != date('Y-m')) {
        if (!empty($dots)) {
          $dotBeliefs = DB::table('dots_beliefs')
            ->where('status','Active')
            ->where('dotId',$dots->id)
            ->orderBy('id','ASC')
            ->get();
          foreach ($dotBeliefs as $cate) {
            $dotBeliefDataQuery = DB::table('kudos_belief_dashboard_graph')
                  ->where('beliefId',$cate->id)
                  ->where('date','LIKE',$month."%");
            if (!empty($orgId)) {
              $dotBeliefDataQuery->where('orgId',$orgId);  
            }
            if(empty($officeId) && empty($departmentId)){
                $dotBeliefDataQuery->whereNull('officeId');
                $dotBeliefDataQuery->whereNull('departmentId');
            }elseif(!empty($officeId) && empty($departmentId)) {
                $dotBeliefDataQuery->where('officeId',$officeId);
                $dotBeliefDataQuery->whereNull('departmentId');
            }elseif(!empty($officeId) && !empty($departmentId)) {
                $dotBeliefDataQuery->where('officeId',$officeId);
                $dotBeliefDataQuery->where('departmentId',$departmentId);
            }elseif(empty($officeId) && !empty($departmentId)) {
                $dotBeliefDataQuery->whereNull('officeId');
                $dotBeliefDataQuery->where('departmentId',$departmentId);
            }
            $dotBeliefData = $dotBeliefDataQuery->first();
            if($dotBeliefData){
              if($includeWeekend != 1){ //Exclude Weekend Days
                  // if ($maxCount < $dotBeliefData->without_weekend) {
                  //   $maxCount = $dotBeliefData->without_weekend;
                  // }
                  $newArr[] = $dotBeliefData->without_weekend;
                  if ($dotBeliefData->without_weekend != 0) {
                      $maxCount++;
                  }
              }else{
                  // if ($maxCount < $dotBeliefData->with_weekend) {
                  //   $maxCount = $dotBeliefData->with_weekend;
                  // }
                  $newArr[] = $dotBeliefData->with_weekend;
                  if ($dotBeliefData->with_weekend != 0) {
                      $maxCount++;
                  }
              }
            }else{
                $newArr[] = 0;
            }
          }
        }
        if (empty($dots)) {
          $newArr[] = 0;
        }
      }else{
        if (!empty($dots)) {
          if ($includeWeekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $dotBeliefArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateKudosBeliefGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date);

          foreach ($dotBeliefArr as $dotBeliefVal) {
            // if ($maxCount < $dotBeliefVal['count']) {
            //   $maxCount = $dotBeliefVal['count'];
            // }
            array_push($newArr, $dotBeliefVal['count']);
            if ($dotBeliefVal['count'] != 0) {
                $maxCount++;
            }
          }
        }elseif (empty($dots)) {
          $newArr[] = 0;
        }
      }
      
      array_push($arrMain, $newArr);
      // if (empty($dots)) {
      //   array_push($dotThumbsupArray,0);
      // }
      // array_push($mainArray,$dotThumbsupArray);
    }

    // echo "<pre>";print_r($arrMain);
    // echo "<pre>";print_r($maxCount);
    // echo "<pre>";print_r($beliefsName);
    // die();

    return array('arrMain'=>$arrMain,'maxCount'=>$maxCount,'beliefsName'=>$beliefsName);

    // return array($mainArray,$maxCount,$beliefsName);



    // //Get info of org
    // $org = DB::table('organisations')->where('id',$orgId)->first();

        
    // $dotThumbsupArray = array();
    // if (!empty($dots)) {
    //   $dotBeliefs = DB::table('dots_beliefs')
    //     ->where('status','Active')
    //     ->where('dotId',$dots->id)
    //     ->get();
    //   // $dotBCount = array();
    //   foreach ($dotBeliefs as $key => $bValue){

    //     $dotsValue = DB::table('dots_values')->where('beliefId',$bValue->id)->where('status','Active')->get();
    //     // $a = array();
    //     $dotValuesCount = 0;
    //     foreach ($dotsValue as $Vvalue){
    //       $upVotTblQuery = DB::table('dot_bubble_rating_records')            
    //             ->where('dot_belief_id',$bValue->id)
    //             ->where('dot_value_name_id',$Vvalue->name)
    //             ->where('status','Active')
    //             ->where('bubble_flag',"1");
    //       // if($includeWeekend != 1){
    //       //     $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
    //       //     $currentDate    = date('Y-m-d'); 
    //       //     //Get weekends
    //       //     $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
    //       //     $upVotTblQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    //       // }  
    //       if ($includeWeekend != 1) {
    //         //Get weekends
    //         $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($month,2);
    //         $upVotTblQuery->whereNotIn(DB::raw("(DATE_FORMAT(iot_feedbacks.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    //       }        
    //       $dotBCount = $upVotTblQuery->count();
    //       // array_push($a, $dotBCount);
    //       $dotValuesCount += $dotBCount; 
    //     }
    //     $dotBeliefCount['count'] = $dotValuesCount;
    //     $dotBeliefCount['name'] = $bValue->name;
    //     array_push($dotThumbsupArray, $dotBeliefCount);
    //   }
    // }
    // $maxThumbsup = max($dotThumbsupArray);
    // $max = $maxThumbsup['count'];
    // return array($dotThumbsupArray,$max);
  }

  public function getThumbsupBeliefCat(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $beliefId         = Input::get('categoryId');
    $columnColor      = Input::get('columnColor');

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }
    $arrMain = array();
    //Get info of org
    $org = DB::table('organisations')->where('id',$orgId)->where('status','Active')->first();
    $includeWeekend = $org->include_weekend;

    $dots = DB::table('dots')->where('orgId',$orgId)->where('status','Active')->first();

    $dotValuesArray = array("");
    if (empty($dots)) {
      array_push($dotValuesArray,"");
    }

    $beliefsName = array();
    if (!empty($dots)) {
      if ($beliefId) {
        $dotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->where('id',$beliefId)
        ->orderBy('id','ASC')
        ->get();
      }else{
        $dotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->orderBy('id','ASC')
        ->get();
      }
      $beliefName = array();
      foreach ($dotBeliefs as $key => $bValue){
        array_push($dotValuesArray, $bValue->name);
        $beliefsName[$bValue->id] = $bValue->name;
      }
    }
    array_push($arrMain,$dotValuesArray);
    $maxCount = 0;
    foreach ($months as $month) {
      $newArr = array(date('My',strtotime($month)));

      if ($month != date('Y-m')) {
        if (!empty($dots)) {
          if (!empty($beliefId)) {
            $dotBeliefs = DB::table('dots_beliefs')
              ->where('status','Active')
              ->where('dotId',$dots->id)
              ->where('id',$beliefId)
              ->orderBy('id','ASC')
              ->get();
          }else{
            $dotBeliefs = DB::table('dots_beliefs')
              ->where('status','Active')
              ->where('dotId',$dots->id)
              ->orderBy('id','ASC')
              ->get();
          }
          foreach ($dotBeliefs as $cate) {
            $dotBeliefDataQuery = DB::table('kudos_belief_dashboard_graph')
                  ->where('beliefId',$cate->id)
                  ->where('date','LIKE',$month."%");
            if (!empty($orgId)) {
              $dotBeliefDataQuery->where('orgId',$orgId);  
            }
            if(empty($officeId) && empty($departmentId)){
                $dotBeliefDataQuery->whereNull('officeId');
                $dotBeliefDataQuery->whereNull('departmentId');
            }elseif(!empty($officeId) && empty($departmentId)) {
                $dotBeliefDataQuery->where('officeId',$officeId);
                $dotBeliefDataQuery->whereNull('departmentId');
            }elseif(!empty($officeId) && !empty($departmentId)) {
                $dotBeliefDataQuery->where('officeId',$officeId);
                $dotBeliefDataQuery->where('departmentId',$departmentId);
            }elseif(empty($officeId) && !empty($departmentId)) {
                $dotBeliefDataQuery->whereNull('officeId');
                $dotBeliefDataQuery->where('departmentId',$departmentId);
            }
            $dotBeliefData = $dotBeliefDataQuery->first();
            if($dotBeliefData){
              if($includeWeekend != 1){ //Exclude Weekend Days
                  // if ($maxCount < $dotBeliefData->without_weekend) {
                  //   $maxCount = $dotBeliefData->without_weekend;
                  // }
                  $newArr[] = $dotBeliefData->without_weekend;
                  if ($dotBeliefData->without_weekend != 0) {
                      $maxCount++;
                  }
              }else{
                  // if ($maxCount < $dotBeliefData->with_weekend) {
                  //   $maxCount = $dotBeliefData->with_weekend;
                  // }
                  $newArr[] = $dotBeliefData->with_weekend;
                  if ($dotBeliefData->with_weekend != 0) {
                      $maxCount++;
                  }
              }
            }else{
                $newArr[] = 0;
            }
          }
        }
        if (empty($dots)) {
          $newArr[] = 0;
        }
      }else{
        if (!empty($dots)) {
          if ($includeWeekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');
          if (!empty($beliefId)) {
            $dotBeliefArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateKudosBeliefGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date,$beliefId);
          }else{
            $dotBeliefArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateKudosBeliefGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date);
          }

          foreach ($dotBeliefArr as $dotBeliefVal) { 
            // if ($maxCount < $dotBeliefVal['count']) {
            //   $maxCount = $dotBeliefVal['count'];
            // }
            array_push($newArr, $dotBeliefVal['count']);
            if ($dotBeliefVal['count'] != 0) {
                $maxCount++;
            }
          }
        }elseif (empty($dots)) {
          $newArr[] = 0;
        }
      }
      
      array_push($arrMain, $newArr);
      // $dotThumbsupArray = array();
      // array_push($dotThumbsupArray,date('My',strtotime($month)));   //first index of month
      
      // if (!empty($dots)) {
      //   if (!empty($beliefId)) {
      //     $dotBeliefs = DB::table('dots_beliefs')
      //     ->where('status','Active')
      //     ->where('dotId',$dots->id)
      //     ->where('id',$beliefId)
      //     ->orderBy('id','ASC')
      //     ->get();
      //   }else{
      //     $dotBeliefs = DB::table('dots_beliefs')
      //     ->where('status','Active')
      //     ->where('dotId',$dots->id)
      //     ->orderBy('id','ASC')
      //     ->get();
      //   }
        
      //   foreach ($dotBeliefs as $key => $bValue){
      //     $dotsValue = DB::table('dots_values')->where('beliefId',$bValue->id)->where('status','Active')->get();
      //     $dotValuesCount = 0;
          
      //     foreach ($dotsValue as $Vvalue){
      //       $upVotTblQuery = DB::table('dot_bubble_rating_records')            
      //             ->where('dot_belief_id',$bValue->id)
      //             ->where('dot_value_name_id',$Vvalue->name)
      //             ->where('status','Active')
      //             ->where('created_at','LIKE',$month."%")
      //             ->where('bubble_flag',"1");
      //       if ($includeWeekend != 1) {
      //         //Get weekends
      //         $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($month,2);
      //         $upVotTblQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      //       }        
      //       $dotBCount = $upVotTblQuery->count();
      //       $dotValuesCount += $dotBCount; 
      //     }
      //     // echo "<pre>";print_r($dotValuesCount);
      //     if ($dotValuesCount>0) {
      //       $max = $dotValuesCount;
      //       // array_push($a,$dotValuesCount);
      //     }
      //     array_push($dotThumbsupArray,$dotValuesCount);   //first index of month
      //     // $dotBeliefCount['count'] = $dotValuesCount;
      //     // $dotBeliefCount['name'] = $bValue->name;
      //     // array_push($dotThumbsupArray, $dotBeliefCount);
      //   }
      // }
      // if (empty($dots)) {
      //   array_push($dotThumbsupArray,0);
      // }
      // array_push($mainArray,$dotThumbsupArray);
    }
    return array('arrMain'=>$arrMain,'maxCount'=>$maxCount,'beliefsName'=>$beliefsName,'columnColor'=>$columnColor);
    // return array($mainArray,$max,$beliefsName,$columnColor);
  }

  public function getDashboardThumbsupValues(Request $request)
  {
      $orgId          = base64_decode($request->orgId); 
      $officeId       = $request->officeId;
      $departmentId   = $request->departmentId;
      $belief         = trim($request->belief);

      $arrMain = array();

      for ($i = 11; $i >= 0; $i--) {
        $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
      }

      //Get info of org
      $org = DB::table('organisations')->where('id',$orgId)->first();

      $dotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('name',$belief)
        ->first();

      $valuesArray = array(''); //First index
      if (empty($dotBeliefs)) {
        array_push($valuesArray,"");
      }

      if (!empty($dotBeliefs)) {
        $dotsValue = DB::table('dots_values')
              ->select('dots_values.id as id','dots_values.name as nameId','dot_value_list.name as valueName')
              ->leftjoin('dot_value_list','dots_values.name','dot_value_list.id')
              ->where('dots_values.beliefId',$dotBeliefs->id)
              ->where('dots_values.status','Active')
              ->where('dot_value_list.status','Active')
              // ->orderBy('dot_value_list.name','ASC')
              ->get();
        $thumbsupValues = array();
        foreach ($dotsValue as $key => $bValue){
          array_push($valuesArray, $bValue->valueName);
          $thumbsupValues[$bValue->id] = $bValue->valueName;
        }     
      }
      array_push($arrMain, $valuesArray);
      $max = 0;
      foreach ($months as $month) {
        // $dotThumbsupArray = array();
        // array_push($dotThumbsupArray,date('My',strtotime($month)));   //first index of month
        $newArr = array(date('My',strtotime($month)));

        if ($month != date('Y-m')) {
          if (!empty($dotBeliefs)) {
            $dotsValue = DB::table('dots_values')
                ->select('dots_values.id as id','dots_values.name as nameId','dot_value_list.name as valueName')
                ->leftjoin('dot_value_list','dots_values.name','dot_value_list.id')
                ->where('dots_values.beliefId',$dotBeliefs->id)
                ->where('dots_values.status','Active')
                ->where('dot_value_list.status','Active')
                // ->orderBy('dot_value_list.name','ASC')
                ->get();
            foreach ($dotsValue as $val) {
              $dotBeliefDataQuery = DB::table('kudos_values_dasboard_graph')
                    ->where('beliefId',$dotBeliefs->id)
                    ->where('valueId',$val->id)
                    ->where('date','LIKE',$month."%");
              if (!empty($orgId)) {
                $dotBeliefDataQuery->where('orgId',$orgId);  
              }
              if(empty($officeId) && empty($departmentId)){
                  $dotBeliefDataQuery->whereNull('officeId');
                  $dotBeliefDataQuery->whereNull('departmentId');
              }elseif(!empty($officeId) && empty($departmentId)) {
                  $dotBeliefDataQuery->where('officeId',$officeId);
                  $dotBeliefDataQuery->whereNull('departmentId');
              }elseif(!empty($officeId) && !empty($departmentId)) {
                  $dotBeliefDataQuery->where('officeId',$officeId);
                  $dotBeliefDataQuery->where('departmentId',$departmentId);
              }elseif(empty($officeId) && !empty($departmentId)) {
                  $dotBeliefDataQuery->whereNull('officeId');
                  $dotBeliefDataQuery->where('departmentId',$departmentId);
              }
              $dotBeliefData = $dotBeliefDataQuery->first();
              if($dotBeliefData){
                // $maxCount++;
                if($org->include_weekend != 1){ //Exclude Weekend Days
                    $newArr[] = $dotBeliefData->without_weekend;
                }else{
                    $newArr[] = $dotBeliefData->with_weekend;
                }
              }else{
                  $newArr[] = 0;
              }
            }
          }elseif (empty($dotBeliefs)) {
            $newArr[] = 0;
          }
        }else{
          if (!empty($dotBeliefs)) {
            if ($org->include_weekend == 1) {
              $isExclude = 2; //include weekends
            }else{
              $isExclude = 1; //exclude weekend
            }
            $date = date('Y-m-d');

            $dotBeliefArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateKudosValuesGraph(base64_encode($orgId),$officeId,$departmentId,$dotBeliefs->id,$isExclude,$date);

            foreach ($dotBeliefArr as $dotBeliefVal) { 
              array_push($newArr, $dotBeliefVal['count']);
            }
          }elseif (empty($dotBeliefs)) {
            $newArr[] = 0;
          }
        }
        array_push($arrMain, $newArr);
      }

        // if (!empty($dotBeliefs)) {
        //   $dotsValue = DB::table('dots_values')
        //         ->select('dots_values.id as id','dots_values.name as nameId','dot_value_list.name as valueName')
        //         ->leftjoin('dot_value_list','dots_values.name','dot_value_list.id')
        //         ->where('dots_values.beliefId',$dotBeliefs->id)
        //         ->where('dots_values.status','Active')
        //         ->where('dot_value_list.status','Active')
        //         ->get();
          
        //   foreach ($dotsValue as $Vvalue){
        //       // $valuesArray = array();
        //       $upVotTblQuery = DB::table('dot_bubble_rating_records')            
        //             ->where('dot_belief_id',$dotBeliefs->id)
        //             ->where('dot_value_name_id',$Vvalue->nameId)
        //             ->where('created_at','LIKE',$month."%")
        //             ->where('bubble_flag',"1");
        //       if($org->include_weekend != 1){
        //           $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($month,2);
        //           $upVotTblQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
        //       }         
              
        //       //$dotBCount['count'] = $upVotTblQuery->count();
        //       //$dotBCount['name']  = $Vvalue->valueName;
        //       //array_push($dotValuesCount, $dotBCount);
        //       // array_push($valuesArray, ucfirst($Vvalue->valueName));
        //       if($upVotTblQuery->count()>0){
        //         $max++;
        //         array_push($dotThumbsupArray, $upVotTblQuery->count());
        //       }else{
        //         array_push($dotThumbsupArray, 0);
        //       }
        //   }
        // }
        // if (empty($dotBeliefs)) {
        //   array_push($dotThumbsupArray, 0);
        // }
        // array_push($arrMain, $dotThumbsupArray);
      // }
      // echo "<pre>";print_r($arrMain);
      // echo "<pre>";print_r($max);
      // die();
      // if ($max<=0) {
      //   $max = 10;
      // }
      return array('status'=>true,'arrMain'=>$arrMain,'max'=>$max,'thumbsupValues'=>$thumbsupValues);
  }

  public function getThumbsupValuesCat(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $beliefId         = trim(Input::get('beliefId'));
    $valueId          = Input::get('valueId');
    $columnColor      = Input::get('columnColor');

    $arrMain = array();

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }

    //Get info of org
    $org = DB::table('organisations')->where('id',$orgId)->where('status','Active')->first();
    $includeWeekend = $org->include_weekend;

    $dotBeliefs = DB::table('dots_beliefs')
      ->where('status','Active')
      ->where('name',$beliefId)
      ->first();

    $valuesArray = array(''); //First index

    if (empty($dotBeliefs)) {
      array_push($valuesArray,"");
      array_push($valueName, "");
    }

    if (!empty($dotBeliefs)) {
      if (!empty($valueId)) {
        $dotsValue = DB::table('dots_values')
            ->select('dots_values.id as id','dots_values.name as nameId','dot_value_list.name as valueName')
            ->leftjoin('dot_value_list','dots_values.name','dot_value_list.id')
            ->where('dots_values.beliefId',$dotBeliefs->id)
            ->where('dots_values.id',$valueId)
            ->where('dots_values.status','Active')
            ->where('dot_value_list.status','Active')
            ->get();  
      }else{
        $dotsValue = DB::table('dots_values')
            ->select('dots_values.id as id','dots_values.name as nameId','dot_value_list.name as valueName')
            ->leftjoin('dot_value_list','dots_values.name','dot_value_list.id')
            ->where('dots_values.beliefId',$dotBeliefs->id)
            ->where('dots_values.status','Active')
            ->where('dot_value_list.status','Active')
            ->get();
      }
      $valueName = array();
      foreach ($dotsValue as $key => $bValue){
        array_push($valuesArray, $bValue->valueName);
        $valueName[$bValue->id] = $bValue->valueName;
      }     
    }
    array_push($arrMain, $valuesArray);
    $max = 0;
    foreach ($months as $month) {
      $newArr = array(date('My',strtotime($month)));

      if ($month != date('Y-m')) {
          if (!empty($dotBeliefs)) {
            if (!empty($valueId)) {
              $dotsValue = DB::table('dots_values')
                  ->select('dots_values.id as id','dots_values.name as nameId','dot_value_list.name as valueName')
                  ->leftjoin('dot_value_list','dots_values.name','dot_value_list.id')
                  ->where('dots_values.beliefId',$dotBeliefs->id)
                  ->where('dots_values.id',$valueId)
                  ->where('dots_values.status','Active')
                  ->where('dot_value_list.status','Active')
                  ->get();  
            }else{
              $dotsValue = DB::table('dots_values')
                  ->select('dots_values.id as id','dots_values.name as nameId','dot_value_list.name as valueName')
                  ->leftjoin('dot_value_list','dots_values.name','dot_value_list.id')
                  ->where('dots_values.beliefId',$dotBeliefs->id)
                  ->where('dots_values.status','Active')
                  ->where('dot_value_list.status','Active')
                  ->get();
            }
            foreach ($dotsValue as $val) {
              $dotBeliefDataQuery = DB::table('kudos_values_dasboard_graph')
                    ->where('beliefId',$dotBeliefs->id)
                    ->where('valueId',$val->id)
                    ->where('date','LIKE',$month."%");
              if (!empty($orgId)) {
                $dotBeliefDataQuery->where('orgId',$orgId);  
              }
              if(empty($officeId) && empty($departmentId)){
                  $dotBeliefDataQuery->whereNull('officeId');
                  $dotBeliefDataQuery->whereNull('departmentId');
              }elseif(!empty($officeId) && empty($departmentId)) {
                  $dotBeliefDataQuery->where('officeId',$officeId);
                  $dotBeliefDataQuery->whereNull('departmentId');
              }elseif(!empty($officeId) && !empty($departmentId)) {
                  $dotBeliefDataQuery->where('officeId',$officeId);
                  $dotBeliefDataQuery->where('departmentId',$departmentId);
              }elseif(empty($officeId) && !empty($departmentId)) {
                  $dotBeliefDataQuery->whereNull('officeId');
                  $dotBeliefDataQuery->where('departmentId',$departmentId);
              }
              $dotBeliefData = $dotBeliefDataQuery->first();
              if($dotBeliefData){
                // $maxCount++;
                if($org->include_weekend != 1){ //Exclude Weekend Days
                    $newArr[] = $dotBeliefData->without_weekend;
                    if ($dotBeliefData->without_weekend != 0) {
                        $max++;
                    }
                }else{
                    $newArr[] = $dotBeliefData->with_weekend;
                    if ($dotBeliefData->with_weekend != 0) {
                        $max++;
                    }
                }
              }else{
                  $newArr[] = 0;
              }
            }
          }elseif (empty($dotBeliefs)) {
            $newArr[] = 0;
          }
        }else{
          if (!empty($dotBeliefs)) {
            if ($org->include_weekend == 1) {
              $isExclude = 2; //include weekends
            }else{
              $isExclude = 1; //exclude weekend
            }
            $date = date('Y-m-d');
            if (!empty($valueId)) {
              $dotBeliefArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateKudosValuesGraph(base64_encode($orgId),$officeId,$departmentId,$dotBeliefs->id,$isExclude,$date,$valueId);
            }else{
              $dotBeliefArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateKudosValuesGraph(base64_encode($orgId),$officeId,$departmentId,$dotBeliefs->id,$isExclude,$date);
            }
            foreach ($dotBeliefArr as $dotBeliefVal) { 
              array_push($newArr, $dotBeliefVal['count']);
              if ($dotBeliefVal['count'] != 0) {
                  $max++;
              }
            }
          }elseif (empty($dotBeliefs)) {
            $newArr[] = 0;
          }
        }
        array_push($arrMain, $newArr);
      }










      // $dotThumbsupArray = array();
      // array_push($dotThumbsupArray,date('My',strtotime($month)));   //first index of month
      // if (!empty($dotBeliefs)) {
      //   if (!empty($valueId)) {
      //     $dotsValue = DB::table('dots_values')
      //         ->select('dots_values.id as id','dots_values.name as nameId','dot_value_list.name as valueName')
      //         ->leftjoin('dot_value_list','dots_values.name','dot_value_list.id')
      //         ->where('dots_values.beliefId',$dotBeliefs->id)
      //         ->where('dots_values.id',$valueId)
      //         ->where('dots_values.status','Active')
      //         ->where('dot_value_list.status','Active')
      //         ->get();  
      //   }else{
      //     $dotsValue = DB::table('dots_values')
      //         ->select('dots_values.id as id','dots_values.name as nameId','dot_value_list.name as valueName')
      //         ->leftjoin('dot_value_list','dots_values.name','dot_value_list.id')
      //         ->where('dots_values.beliefId',$dotBeliefs->id)
      //         ->where('dots_values.status','Active')
      //         ->where('dot_value_list.status','Active')
      //         ->get();
      //   }
        
      //   foreach ($dotsValue as $Vvalue){
      //       // $valuesArray = array();
      //       $upVotTblQuery = DB::table('dot_bubble_rating_records')            
      //             ->where('dot_belief_id',$dotBeliefs->id)
      //             ->where('dot_value_name_id',$Vvalue->nameId)
      //             ->where('created_at','LIKE',$month."%")
      //             ->where('bubble_flag',"1");
      //       if($org->include_weekend != 1){
      //           $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($month,2);
      //           $upVotTblQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      //       }         
            
      //       //$dotBCount['count'] = $upVotTblQuery->count();
      //       //$dotBCount['name']  = $Vvalue->valueName;
      //       //array_push($dotValuesCount, $dotBCount);
      //       // array_push($valuesArray, ucfirst($Vvalue->valueName));
      //       if($upVotTblQuery->count()>0){
      //         $max++;
      //         array_push($dotThumbsupArray, $upVotTblQuery->count());
      //       }else{
      //         array_push($dotThumbsupArray, 0);
      //       }
      //   }
      // }
      // if (empty($dotBeliefs)) {
      //   array_push($dotThumbsupArray, 0);
      // }
      // array_push($arrMain, $dotThumbsupArray);
    // }
    // echo "<pre>";print_r($arrMain);
    // echo "<pre>";print_r($max);
    // die();
    // if ($max<=0) {
    //   $max = 10;
    // }
    return array('status'=>true,'arrMain'=>$arrMain,'maxCount'=>$max,'valueName'=>$valueName,'columnColor'=>$columnColor);
  }

  // public function getThumbsupBeliefs($perArr=array())
  // {
  //   $orgId            = $perArr["orgId"];
  //   $officeId         = $perArr["officeId"];
  //   $departmentId     = $perArr["departmentId"];
  //   $includeWeekend   = $perArr["includeWeekend"];
  //   $months           = $perArr["months"];

  //   $dots = DB::table('dots')->where('orgId',$orgId)->first();

  //   $mainArray = array();

  //   $dotValuesArray = array("");
  //   if (empty($dots)) {
  //     array_push($dotValuesArray,"");
  //   }

  //   $beliefsName = array();
  //   if (!empty($dots)) {
  //     $dotBeliefs = DB::table('dots_beliefs')
  //       ->where('status','Active')
  //       ->where('dotId',$dots->id)
  //       ->get();

  //     foreach ($dotBeliefs as $key => $bValue){
  //       array_push($dotValuesArray, $bValue->name);
  //       $beliefsName[$bValue->id] = $bValue->name;
  //     }
  //   }
  //   array_push($mainArray,$dotValuesArray);
  //   $max = 0;
  //   foreach ($months as $month) {
  //     $dotThumbsupArray = array();
  //     array_push($dotThumbsupArray,date('My',strtotime($month)));   //first index of month
      
  //     if (!empty($dots)) {
  //       $dotBeliefs = DB::table('dots_beliefs')
  //       ->where('status','Active')
  //       ->where('dotId',$dots->id)
  //       ->get();
        
  //       foreach ($dotBeliefs as $key => $bValue){
  //         $dotsValue = DB::table('dots_values')->where('beliefId',$bValue->id)->where('status','Active')->get();
  //         $dotValuesCount = 0;
          
  //         foreach ($dotsValue as $Vvalue){
  //           $upVotTblQuery = DB::table('dot_bubble_rating_records')            
  //                 ->where('dot_belief_id',$bValue->id)
  //                 ->where('dot_value_name_id',$Vvalue->name)
  //                 ->where('status','Active')
  //                 ->where('created_at','LIKE',$month."%")
  //                 ->where('bubble_flag',"1");
  //           if ($includeWeekend != 1) {
  //             //Get weekends
  //             $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($month,2);
  //             $upVotTblQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //           }        
  //           $dotBCount = $upVotTblQuery->count();
  //           $dotValuesCount += $dotBCount; 
  //         }
  //         // echo "<pre>";print_r($dotValuesCount);
  //         if ($dotValuesCount>0) {
  //           $max = $dotValuesCount;
  //           // array_push($a,$dotValuesCount);
  //         }
  //         array_push($dotThumbsupArray,$dotValuesCount);   //first index of month
  //         // $dotBeliefCount['count'] = $dotValuesCount;
  //         // $dotBeliefCount['name'] = $bValue->name;
  //         // array_push($dotThumbsupArray, $dotBeliefCount);
  //       }
  //     }
  //     if (empty($dots)) {
  //       array_push($dotThumbsupArray,0);
  //     }
  //     array_push($mainArray,$dotThumbsupArray);
  //   }
  //   return array($mainArray,$max,$beliefsName);



  //   // //Get info of org
  //   // $org = DB::table('organisations')->where('id',$orgId)->first();

        
  //   // $dotThumbsupArray = array();
  //   // if (!empty($dots)) {
  //   //   $dotBeliefs = DB::table('dots_beliefs')
  //   //     ->where('status','Active')
  //   //     ->where('dotId',$dots->id)
  //   //     ->get();
  //   //   // $dotBCount = array();
  //   //   foreach ($dotBeliefs as $key => $bValue){

  //   //     $dotsValue = DB::table('dots_values')->where('beliefId',$bValue->id)->where('status','Active')->get();
  //   //     // $a = array();
  //   //     $dotValuesCount = 0;
  //   //     foreach ($dotsValue as $Vvalue){
  //   //       $upVotTblQuery = DB::table('dot_bubble_rating_records')            
  //   //             ->where('dot_belief_id',$bValue->id)
  //   //             ->where('dot_value_name_id',$Vvalue->name)
  //   //             ->where('status','Active')
  //   //             ->where('bubble_flag',"1");
  //   //       // if($includeWeekend != 1){
  //   //       //     $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
  //   //       //     $currentDate    = date('Y-m-d'); 
  //   //       //     //Get weekends
  //   //       //     $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
  //   //       //     $upVotTblQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //   //       // }  
  //   //       if ($includeWeekend != 1) {
  //   //         //Get weekends
  //   //         $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($month,2);
  //   //         $upVotTblQuery->whereNotIn(DB::raw("(DATE_FORMAT(iot_feedbacks.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //   //       }        
  //   //       $dotBCount = $upVotTblQuery->count();
  //   //       // array_push($a, $dotBCount);
  //   //       $dotValuesCount += $dotBCount; 
  //   //     }
  //   //     $dotBeliefCount['count'] = $dotValuesCount;
  //   //     $dotBeliefCount['name'] = $bValue->name;
  //   //     array_push($dotThumbsupArray, $dotBeliefCount);
  //   //   }
  //   // }
  //   // $maxThumbsup = max($dotThumbsupArray);
  //   // $max = $maxThumbsup['count'];
  //   // return array($dotThumbsupArray,$max);
  // }

  // public function getThumbsupBeliefCat(Request $request)
  // {
  //   $orgId            = Input::get('orgId');
  //   $officeId         = Input::get('officeId');
  //   $departmentId     = Input::get('departmentId');
  //   $beliefId         = Input::get('categoryId');
  //   $columnColor      = Input::get('columnColor');

  //   for ($i = 11; $i >= 0; $i--) {
  //     $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
  //   }
  //   $mainArray = array();
  //   //Get info of org
  //   $org = DB::table('organisations')->where('id',$orgId)->where('status','Active')->first();
  //   $includeWeekend = $org->include_weekend;

  //   $dots = DB::table('dots')->where('orgId',$orgId)->where('status','Active')->first();

  //   $dotValuesArray = array("");
  //   if (empty($dots)) {
  //     array_push($dotValuesArray,"");
  //   }

  //   $beliefsName = array();
  //   if (!empty($dots)) {
  //     if ($beliefId) {
  //       $dotBeliefs = DB::table('dots_beliefs')
  //       ->where('status','Active')
  //       ->where('dotId',$dots->id)
  //       ->where('id',$beliefId)
  //       ->orderBy('id','ASC')
  //       ->get();
  //     }else{
  //       $dotBeliefs = DB::table('dots_beliefs')
  //       ->where('status','Active')
  //       ->where('dotId',$dots->id)
  //       ->orderBy('id','ASC')
  //       ->get();
  //     }
  //     $beliefName = array();
  //     foreach ($dotBeliefs as $key => $bValue){
  //       array_push($dotValuesArray, $bValue->name);
  //       $beliefsName[$bValue->id] = $bValue->name;
  //     }
  //   }
  //   array_push($mainArray,$dotValuesArray);
  //   $max = 0;
  //   foreach ($months as $month) {
  //     $dotThumbsupArray = array();
  //     array_push($dotThumbsupArray,date('My',strtotime($month)));   //first index of month
      
  //     if (!empty($dots)) {
  //       if (!empty($beliefId)) {
  //         $dotBeliefs = DB::table('dots_beliefs')
  //         ->where('status','Active')
  //         ->where('dotId',$dots->id)
  //         ->where('id',$beliefId)
  //         ->orderBy('id','ASC')
  //         ->get();
  //       }else{
  //         $dotBeliefs = DB::table('dots_beliefs')
  //         ->where('status','Active')
  //         ->where('dotId',$dots->id)
  //         ->orderBy('id','ASC')
  //         ->get();
  //       }
        
  //       foreach ($dotBeliefs as $key => $bValue){
  //         $dotsValue = DB::table('dots_values')->where('beliefId',$bValue->id)->where('status','Active')->get();
  //         $dotValuesCount = 0;
          
  //         foreach ($dotsValue as $Vvalue){
  //           $upVotTblQuery = DB::table('dot_bubble_rating_records')            
  //                 ->where('dot_belief_id',$bValue->id)
  //                 ->where('dot_value_name_id',$Vvalue->name)
  //                 ->where('status','Active')
  //                 ->where('created_at','LIKE',$month."%")
  //                 ->where('bubble_flag',"1");
  //           if ($includeWeekend != 1) {
  //             //Get weekends
  //             $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($month,2);
  //             $upVotTblQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //           }        
  //           $dotBCount = $upVotTblQuery->count();
  //           $dotValuesCount += $dotBCount; 
  //         }
  //         // echo "<pre>";print_r($dotValuesCount);
  //         if ($dotValuesCount>0) {
  //           $max = $dotValuesCount;
  //           // array_push($a,$dotValuesCount);
  //         }
  //         array_push($dotThumbsupArray,$dotValuesCount);   //first index of month
  //         // $dotBeliefCount['count'] = $dotValuesCount;
  //         // $dotBeliefCount['name'] = $bValue->name;
  //         // array_push($dotThumbsupArray, $dotBeliefCount);
  //       }
  //     }
  //     if (empty($dots)) {
  //       array_push($dotThumbsupArray,0);
  //     }
  //     array_push($mainArray,$dotThumbsupArray);
  //   }
  //   return array($mainArray,$max,$beliefsName,$columnColor);
  // }

  // public function getDashboardThumbsupValues(Request $request)
  // {
  //     $orgId          = base64_decode($request->orgId); 
  //     $officeId       = $request->officeId;
  //     $departmentId   = $request->departmentId;
  //     $belief         = trim($request->belief);

  //     $arrMain = array();

  //     for ($i = 11; $i >= 0; $i--) {
  //       $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
  //     }

  //     //Get info of org
  //     $org = DB::table('organisations')->where('id',$orgId)->first();

  //     $dotBeliefs = DB::table('dots_beliefs')
  //       ->where('status','Active')
  //       ->where('name',$belief)
  //       ->first();

  //     $valuesArray = array(''); //First index

  //     if (empty($dotBeliefs)) {
  //       array_push($valuesArray,"");
  //     }

  //     if (!empty($dotBeliefs)) {
  //       $dotsValue = DB::table('dots_values')
  //             ->select('dots_values.id as id','dots_values.name as nameId','dot_value_list.name as valueName')
  //             ->leftjoin('dot_value_list','dots_values.name','dot_value_list.id')
  //             ->where('dots_values.beliefId',$dotBeliefs->id)
  //             ->where('dots_values.status','Active')
  //             ->where('dot_value_list.status','Active')
  //             ->get();
  //       $thumbsupValues = array();
  //       foreach ($dotsValue as $key => $bValue){
  //         array_push($valuesArray, $bValue->valueName);
  //         $thumbsupValues[$bValue->id] = $bValue->valueName;
  //       }     
  //     }
  //     array_push($arrMain, $valuesArray);
  //     $max = 0;
  //     foreach ($months as $month) {
  //       $dotThumbsupArray = array();
  //       array_push($dotThumbsupArray,date('My',strtotime($month)));   //first index of month
  //       if (!empty($dotBeliefs)) {
  //         $dotsValue = DB::table('dots_values')
  //               ->select('dots_values.id as id','dots_values.name as nameId','dot_value_list.name as valueName')
  //               ->leftjoin('dot_value_list','dots_values.name','dot_value_list.id')
  //               ->where('dots_values.beliefId',$dotBeliefs->id)
  //               ->where('dots_values.status','Active')
  //               ->where('dot_value_list.status','Active')
  //               ->get();
          
  //         foreach ($dotsValue as $Vvalue){
  //             // $valuesArray = array();
  //             $upVotTblQuery = DB::table('dot_bubble_rating_records')            
  //                   ->where('dot_belief_id',$dotBeliefs->id)
  //                   ->where('dot_value_name_id',$Vvalue->nameId)
  //                   ->where('created_at','LIKE',$month."%")
  //                   ->where('bubble_flag',"1");
  //             if($org->include_weekend != 1){
  //                 $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($month,2);
  //                 $upVotTblQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //             }         
              
  //             //$dotBCount['count'] = $upVotTblQuery->count();
  //             //$dotBCount['name']  = $Vvalue->valueName;
  //             //array_push($dotValuesCount, $dotBCount);
  //             // array_push($valuesArray, ucfirst($Vvalue->valueName));
  //             if($upVotTblQuery->count()>0){
  //               $max++;
  //               array_push($dotThumbsupArray, $upVotTblQuery->count());
  //             }else{
  //               array_push($dotThumbsupArray, 0);
  //             }
  //         }
  //       }
  //       if (empty($dotBeliefs)) {
  //         array_push($dotThumbsupArray, 0);
  //       }
  //       array_push($arrMain, $dotThumbsupArray);
  //     }
  //     // echo "<pre>";print_r($arrMain);
  //     // echo "<pre>";print_r($max);
  //     // die();
  //     // if ($max<=0) {
  //     //   $max = 10;
  //     // }

  //     // echo "<pre>";print_r($thumbsupValues);die();
  //     return array('status'=>true,'arrMain'=>$arrMain,'max'=>$max,'thumbsupValues'=>$thumbsupValues);
  // }

  // public function getThumbsupValuesCat(Request $request)
  // {
  //   $orgId            = Input::get('orgId');
  //   $officeId         = Input::get('officeId');
  //   $departmentId     = Input::get('departmentId');
  //   $beliefId         = trim(Input::get('beliefId'));
  //   $valueId          = Input::get('valueId');
  //   $columnColor      = Input::get('columnColor');

  //   $arrMain = array();

  //   for ($i = 11; $i >= 0; $i--) {
  //     $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
  //   }

  //   //Get info of org
  //   $org = DB::table('organisations')->where('id',$orgId)->where('status','Active')->first();
  //   $includeWeekend = $org->include_weekend;

  //   $dotBeliefs = DB::table('dots_beliefs')
  //     ->where('status','Active')
  //     ->where('name',$beliefId)
  //     ->first();

  //   $valuesArray = array(''); //First index

  //   if (empty($dotBeliefs)) {
  //     array_push($valuesArray,"");
  //     array_push($valueName, "");
  //   }

  //   if (!empty($dotBeliefs)) {
  //     if (!empty($valueId)) {
  //       $dotsValue = DB::table('dots_values')
  //           ->select('dots_values.id as id','dots_values.name as nameId','dot_value_list.name as valueName')
  //           ->leftjoin('dot_value_list','dots_values.name','dot_value_list.id')
  //           ->where('dots_values.beliefId',$dotBeliefs->id)
  //           ->where('dots_values.id',$valueId)
  //           ->where('dots_values.status','Active')
  //           ->where('dot_value_list.status','Active')
  //           ->get();  
  //     }else{
  //       $dotsValue = DB::table('dots_values')
  //           ->select('dots_values.id as id','dots_values.name as nameId','dot_value_list.name as valueName')
  //           ->leftjoin('dot_value_list','dots_values.name','dot_value_list.id')
  //           ->where('dots_values.beliefId',$dotBeliefs->id)
  //           ->where('dots_values.status','Active')
  //           ->where('dot_value_list.status','Active')
  //           ->get();
  //     }
  //     $valueName = array();
  //     foreach ($dotsValue as $key => $bValue){
  //       array_push($valuesArray, $bValue->valueName);
  //       $valueName[$bValue->id] = $bValue->valueName;
  //     }     
  //   }
  //   array_push($arrMain, $valuesArray);
  //   $max = 0;
  //   foreach ($months as $month) {
  //     $dotThumbsupArray = array();
  //     array_push($dotThumbsupArray,date('My',strtotime($month)));   //first index of month
  //     if (!empty($dotBeliefs)) {
  //       if (!empty($valueId)) {
  //         $dotsValue = DB::table('dots_values')
  //             ->select('dots_values.id as id','dots_values.name as nameId','dot_value_list.name as valueName')
  //             ->leftjoin('dot_value_list','dots_values.name','dot_value_list.id')
  //             ->where('dots_values.beliefId',$dotBeliefs->id)
  //             ->where('dots_values.id',$valueId)
  //             ->where('dots_values.status','Active')
  //             ->where('dot_value_list.status','Active')
  //             ->get();  
  //       }else{
  //         $dotsValue = DB::table('dots_values')
  //             ->select('dots_values.id as id','dots_values.name as nameId','dot_value_list.name as valueName')
  //             ->leftjoin('dot_value_list','dots_values.name','dot_value_list.id')
  //             ->where('dots_values.beliefId',$dotBeliefs->id)
  //             ->where('dots_values.status','Active')
  //             ->where('dot_value_list.status','Active')
  //             ->get();
  //       }
        
  //       foreach ($dotsValue as $Vvalue){
  //           // $valuesArray = array();
  //           $upVotTblQuery = DB::table('dot_bubble_rating_records')            
  //                 ->where('dot_belief_id',$dotBeliefs->id)
  //                 ->where('dot_value_name_id',$Vvalue->nameId)
  //                 ->where('created_at','LIKE',$month."%")
  //                 ->where('bubble_flag',"1");
  //           if($org->include_weekend != 1){
  //               $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($month,2);
  //               $upVotTblQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
  //           }         
            
  //           //$dotBCount['count'] = $upVotTblQuery->count();
  //           //$dotBCount['name']  = $Vvalue->valueName;
  //           //array_push($dotValuesCount, $dotBCount);
  //           // array_push($valuesArray, ucfirst($Vvalue->valueName));
  //           if($upVotTblQuery->count()>0){
  //             $max++;
  //             array_push($dotThumbsupArray, $upVotTblQuery->count());
  //           }else{
  //             array_push($dotThumbsupArray, 0);
  //           }
  //       }
  //     }
  //     if (empty($dotBeliefs)) {
  //       array_push($dotThumbsupArray, 0);
  //     }
  //     array_push($arrMain, $dotThumbsupArray);
  //   }
  //   // echo "<pre>";print_r($arrMain);
  //   // echo "<pre>";print_r($max);
  //   // die();
  //   // if ($max<=0) {
  //   //   $max = 10;
  //   // }
  //   return array('status'=>true,'arrMain'=>$arrMain,'max'=>$max,'valueName'=>$valueName,'columnColor'=>$columnColor);
  // }

  public function getTeamRoleValues($perArr=array())
  {
    $orgId            = $perArr["orgId"];
    $officeId         = $perArr["officeId"];
    $departmentId     = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];
    $months           = $perArr["months"];

    $arrMain = array();
    $cotRoleMapOptionTbl = DB::table('cot_role_map_options')->where('status','Active')->get();

    $categoryName = array("");   //first index
    foreach ($cotRoleMapOptionTbl as $key => $value) {
      $categoryName[$value->id] = $value->maper."(%)";
    }
    array_push($arrMain, $categoryName);

    $max = 0;
    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));

      if ($month != date('Y-m')) {
        foreach ($cotRoleMapOptionTbl as $cate) {
          $teamRoleDataQuery = DB::table('cot_team_role_dashboard_graph')
                ->where('categoryId',$cate->id)
                ->where('date','LIKE',$month."%");
          if (!empty($orgId)) {
            $teamRoleDataQuery->where('orgId',$orgId);  
          }
          if(empty($officeId) && empty($departmentId))
          {
              $teamRoleDataQuery->whereNull('officeId');
              $teamRoleDataQuery->whereNull('departmentId');
          }
          elseif(!empty($officeId) && empty($departmentId))
          {
              $teamRoleDataQuery->where('officeId',$officeId);
              $teamRoleDataQuery->whereNull('departmentId');
          }
          elseif(!empty($officeId) && !empty($departmentId))
          {
              $teamRoleDataQuery->where('officeId',$officeId);
              $teamRoleDataQuery->where('departmentId',$departmentId);
          }
          elseif(empty($officeId) && !empty($departmentId))
          {
              $teamRoleDataQuery->whereNull('officeId');
              $teamRoleDataQuery->where('departmentId',$departmentId);
          }
          $teamRoleData = $teamRoleDataQuery->first();
          if($teamRoleData){
            if($includeWeekend != 1){ //Exclude Weekend Days
                $newArr[] = $teamRoleData->without_weekend;
                if ($teamRoleData->without_weekend != 0) {
                    $max++;
                }
            }else{
                $newArr[] = $teamRoleData->with_weekend;
                if ($teamRoleData->with_weekend != 0) {
                    $max++;
                }
            }
          }else{
              $newArr[] = 0;
          }
        }
      }else{
        if ($includeWeekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $teamRoleArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateTeamRoleGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date);

          foreach ($teamRoleArr as $teamRoleVal) { 
            array_push($newArr, $teamRoleVal['percentage']);
            if ($teamRoleVal['percentage'] != 0) {
                $max++;
            }
          }
      }
      array_push($arrMain, $newArr);
    }
    return array('arrMain'=>$arrMain,'maxCount'=>$max);


//     $finalArray  = array();

//     $organisations = DB::table('organisations')->where('id',$orgId)->where('status','Active')->first();

//     $mapers = DB::table('cot_role_map_options')->where('status','Active')->get();

//     //Get weekends
//     $startDate=DB::table('cot_answers')
//         ->select('created_at')
//         ->where('orgId',$orgId)
//         ->orderBy('created_at','ASC')
//         ->first();

//     if (count($startDate) == 0) {
//         // $startDate = $date;
//         $startDate = date('Y-m-d');
//     }else{
//         $startDate = date('Y-m-d',strtotime($startDate->created_at));
//     }
//     // $currentDate = $date;
//     $currentDate = date('Y-m-d');
//     $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($startDate, $currentDate);

//     $query = DB::table('cot_answers')
//     ->select('users.id','users.orgId','users.name')
//     ->leftjoin('users','users.id','cot_answers.userId')
//     ->leftJoin('departments','departments.id','users.departmentId')
//     ->where('departments.status','Active')
//     ->where('users.status','Active')
//     ->where('users.orgId',$orgId);

//     if($organisations->include_weekend != 1){
//         $query->whereNotIn(DB::raw("(DATE_FORMAT(cot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
//     }
//     if(!empty($officeId) && empty($departmentId))
//     {
//       $query->where('users.officeId',$officeId);
//     }
//     elseif(!empty($officeId) && !empty($departmentId))
//     {
//       $query->where('users.officeId',$officeId);
//       $query->where('users.departmentId',$departmentId);
//     }
//     elseif(empty($officeId) && !empty($departmentId))
//     {
//       $query->where('departments.departmentId',$departmentId);
//     }

//     $users = $query->get();

//     $resultArray1 = array();

//     foreach($users as $value)
//     {
//         $data['id']    = $value->id;
//         $data['orgId'] = $value->orgId;
//         $data['name']  = $value->name;

//         array_push($resultArray1, $data);
//     }

//     $users23 = array_unique($resultArray1,SORT_REGULAR);

//     $finalUser = array();

//     foreach($users23 as  $value55)
//     {
//         $object = (object)array();    

//         $object->id = $value55['id'];
//         $object->orgId=$value55['orgId'];
//         $object->name=$value55['name'];

//         array_push($finalUser,$object);
//     }

//     $cotRoleMapOptions = DB::table('cot_role_map_options')->where('status','Active')->get();

//     $usersArray = array();
//     foreach ($finalUser as $key => $value) 
//     {
//         //echo "<pre>";print_r($value);die();
//         foreach($cotRoleMapOptions as $key => $maper)
//         {
//             $maperCountQuery = DB::table('cot_answers')
//             ->where('orgId',$value->orgId)
//             ->where('userId',$value->id)
//             ->where('cot_role_map_option_id',$maper->id)
//             ->where('status','Active');

//             if($organisations->include_weekend != 1){
//                 $maperCountQuery->whereNotIn(DB::raw("(DATE_FORMAT(cot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
//             }
//             $maperCount = $maperCountQuery->sum('answer');
//             //print_r($maperCount);die();

//             $resultArray = array();
//             $Value[$maper->maper_key] = $maperCount;    
//             array_push($resultArray, $Value);
//         }   
// //echo "<pre>";print_r($resultArray);
// //die();
//         arsort($resultArray[0]);
//   //     echo "<pre>";print_r($resultArray);die();
//         $i = 0;
//         $prev = "";
//         $j = 0;

//         $new  = array();
//         $new1 = array();

//         foreach($resultArray[0] as $key => $val)
//         { 
//             if($val != $prev)
//             { 
//               $i++; 
//             }

//             $new1['title'] = $key; 
//             $new1['value'] = $i;        
//             $prev = $val;

//             if ($key==$cotRoleMapOptions[0]->maper_key) {
//                 $new1['priority'] = 1;
//             }elseif ($key==$cotRoleMapOptions[1]->maper_key) {
//                 $new1['priority'] = 2;
//             }elseif ($key==$cotRoleMapOptions[2]->maper_key) {
//                 $new1['priority'] = 3;
//             }elseif ($key==$cotRoleMapOptions[3]->maper_key) {
//                 $new1['priority'] = 4;
//             }elseif ($key==$cotRoleMapOptions[4]->maper_key) {
//                 $new1['priority'] = 5;
//             }elseif ($key==$cotRoleMapOptions[5]->maper_key) {
//                 $new1['priority'] = 6;
//             }elseif ($key==$cotRoleMapOptions[6]->maper_key) {
//                 $new1['priority'] = 7;
//             }elseif ($key==$cotRoleMapOptions[7]->maper_key) {
//                 $new1['priority'] = 8;
//             }
//             array_push($new,$new1);
//         }

//         $value->new = $new;
//         $value->totalKeyCount = $resultArray[0];
//         array_push($usersArray, $value);

//     }
//     $usersArray = app('App\Http\Controllers\Admin\AdminReportController')->sortarr($usersArray);

//     $cotTeamRoleMapUserArray = $usersArray;

//     /*get percentage values*/
//     $shaper = 0; 
//     $coordinator = 0; 
//     $completerFinisher = 0; 
//     $teamworker = 0; 
//     $implementer = 0; 
//     $monitorEvaluator = 0; 
//     $plant = 0; 
//     $resourceInvestigator = 0; 
//     //echo "<pre>";print_r($usersArray);die();
//     foreach ($usersArray as $key1 => $value1)
//     {

//         if($value1->shaper==1 || $value1->shaper==2 || $value1->shaper==3)
//         {
//             $shaper++;     
//         }
//         if($value1->coordinator==1 || $value1->coordinator==2 || $value1->coordinator==3)
//         {
//             $coordinator++;
//         }
//         if($value1->completerFinisher==1 || $value1->completerFinisher==2 || $value1->completerFinisher==3)
//         {
//             $completerFinisher++;
//         }
//         if($value1->teamworker==1 || $value1->teamworker==2 || $value1->teamworker==3)
//         {
//             $teamworker++;
//         }
//         if($value1->implementer==1 || $value1->implementer==2 || $value1->implementer==3)
//         {
//             $implementer++;
//         }
//         if($value1->monitorEvaluator==1 || $value1->monitorEvaluator==2 || $value1->monitorEvaluator==3)
//         {
//             $monitorEvaluator++;
//         }
//         if($value1->plant==1 || $value1->plant==2 || $value1->plant==3)
//         {
//             $plant++;
//         }
//         if($value1->resourceInvestigator==1 || $value1->resourceInvestigator==2 || $value1->resourceInvestigator==3)
//         {
//             $resourceInvestigator++;
//         }
        
//     }

//     $data['shaper']              = $shaper;
//     $data['coordinator']         = $coordinator;
//     $data['completerFinisher']   = $completerFinisher;
//     $data['teamworker']          = $teamworker;
//     $data['implementer']         = $implementer;
//     $data['monitorEvaluator']    = $monitorEvaluator;
//     $data['plant']               = $plant;
//     $data['resourceInvestigator']= $resourceInvestigator;

  
//     $totalUsers = count($finalUser);

//     //get presented values
//     $cotTeamRoleMapGraphPercentage = array();

//     if (!empty($data) && (!empty($totalUsers)))
//     {

//         $data['shaper']              = number_format((($shaper/$totalUsers)*100), 2, '.', '');
//         $data['coordinator']         = number_format((($coordinator/$totalUsers)*100), 2, '.', '');
//         $data['completerFinisher']   = number_format((($completerFinisher/$totalUsers)*100), 2, '.', '');
//         $data['teamworker']          = number_format((($teamworker/$totalUsers)*100), 2, '.', '');
//         $data['implementer']         = number_format((($implementer/$totalUsers)*100), 2, '.', '');
//         $data['monitorEvaluator']    = number_format((($monitorEvaluator/$totalUsers)*100), 2, '.', '');
//         $data['plant']               = number_format((($plant/$totalUsers)*100), 2, '.', '');
//         $data['resourceInvestigator']= number_format((($resourceInvestigator/$totalUsers)*100), 2, '.', '');

//         $cotTeamRoleMapGraphPercentage['data'] = $data;

//     }
//     else
//     {

//         $data['shaper']              = 0;
//         $data['coordinator']         = 0;
//         $data['completerFinisher']   = 0;
//         $data['teamworker']          = 0;
//         $data['implementer']         = 0;
//         $data['monitorEvaluator']    = 0;
//         $data['plant']               = 0;
//         $data['resourceInvestigator']= 0;

//         $cotTeamRoleMapGraphPercentage['data'] = $data;

//     }

//     $cotOptionsArr = array();
//     $color = array();
//     foreach ($cotRoleMapOptions as $key => $value) {
//         array_push($cotOptionsArr, $value->maper);
//         if ($value->categoryId == 1) {
//             array_push($color, '#000');
//         }elseif ($value->categoryId == 2    ) {
//             array_push($color, '#e28b13');
//         }elseif ($value->categoryId == 3) {
//             array_push($color, '#eb1c24');
//         }elseif ($value->categoryId == 4) {
//             array_push($color, 'lightgray');
//         }
//     }

//     $perArr = array(
//             $data['shaper'],
//             $data['coordinator'],
//             $data['implementer'],
//             $data['completerFinisher'],
//             $data['monitorEvaluator'],
//             $data['teamworker'],
//             $data['plant'],
//             $data['resourceInvestigator']
//         );
  
//     $options = $cotOptionsArr;
//     $colors = $color;
//     $perArrs = $perArr;

//     $result = array_map(function ($option, $color, $perArr) {
//       return array_combine(
//         ['option', 'color', 'perArr'],
//         [$option, $color, $perArr]
//       );
//     }, $options, $colors, $perArrs);

//     $collection = collect($result);

//     $cotGraphArr = $collection->sortBy('option');

//     $cotRadarChartArr = $collection->sortBy('perArr');

//     $cotCategory = DB::table('cot_role_map_category')->where('status','Active')->get();

//     $cotGraphConditionArr = array_filter(array_column($result, 'perArr'));

//     return $cotGraphArr;
  }

  public function getTeamRoleCat(Request $request)
  {
    $orgId            = Input::get('orgId');
    $officeId         = Input::get('officeId');
    $departmentId     = Input::get('departmentId');
    $categoryId       = Input::get('categoryId');
    $columnColor       = Input::get('columnColor');

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }

    $org = DB::table('organisations')->where('id',$orgId)->first();
    $includeWeekend = $org->include_weekend;

    $arrMain = array();
    if ($categoryId) {
      $cotRoleMapOptionTbl = DB::table('cot_role_map_options')->where('status','Active')->where('id',$categoryId)->get();
    }else{
      $cotRoleMapOptionTbl = DB::table('cot_role_map_options')->where('status','Active')->get();
    }

    $categoryName = array("");   //first index
    foreach ($cotRoleMapOptionTbl as $key => $value) {
      $categoryName[] = $value->maper."(%)";
    }
    array_push($arrMain, $categoryName);

    $max = 0;
    foreach ($months as $key => $month) {
      $newArr = array(date('My',strtotime($month)));

      if ($month != date('Y-m')) {
        foreach ($cotRoleMapOptionTbl as $cate) {
          $teamRoleDataQuery = DB::table('cot_team_role_dashboard_graph')
                ->where('date','LIKE',$month."%");
          if (!empty($categoryId)) {
            $teamRoleDataQuery->where('categoryId',$categoryId);
          }else{
            $teamRoleDataQuery->where('categoryId',$cate->id);
          }
          if (!empty($orgId)) {
            $teamRoleDataQuery->where('orgId',$orgId);  
          }
          if(empty($officeId) && empty($departmentId))
          {
              $teamRoleDataQuery->whereNull('officeId');
              $teamRoleDataQuery->whereNull('departmentId');
          }
          elseif(!empty($officeId) && empty($departmentId))
          {
              $teamRoleDataQuery->where('officeId',$officeId);
              $teamRoleDataQuery->whereNull('departmentId');
          }
          elseif(!empty($officeId) && !empty($departmentId))
          {
              $teamRoleDataQuery->where('officeId',$officeId);
              $teamRoleDataQuery->where('departmentId',$departmentId);
          }
          elseif(empty($officeId) && !empty($departmentId))
          {
              $teamRoleDataQuery->whereNull('officeId');
              $teamRoleDataQuery->where('departmentId',$departmentId);
          }
          $teamRoleData = $teamRoleDataQuery->first();
          if($teamRoleData){
            if($includeWeekend != 1){ //Exclude Weekend Days
                $newArr[] = $teamRoleData->without_weekend;
                if ($teamRoleData->without_weekend != 0) {
                    $max++;
                }
            }else{
                $newArr[] = $teamRoleData->with_weekend;
                if ($teamRoleData->with_weekend != 0) {
                    $max++;
                }
            }
          }else{
              $newArr[] = 0;
          }
        }
      }else{
          if ($includeWeekend == 1) {
            $isExclude = 2; //include weekends
          }else{
            $isExclude = 1; //exclude weekend
          }
          $date = date('Y-m-d');

          $teamRoleArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateTeamRoleGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date,$categoryId);

          foreach ($teamRoleArr as $teamRoleVal) { 
              array_push($newArr, $teamRoleVal['percentage']);
              if ($teamRoleVal['percentage'] != 0) {
                  $max++;
              }
          }
      }
      array_push($arrMain, $newArr);
    }
    return array('status'=>true,'arrMain'=>$arrMain,'columnColor'=>$columnColor,'maxCount'=>$max);
  }

  public function userCount($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];

    $usersQuery = DB::table('users')
          ->select('users.id as id')
          ->leftjoin('departments','users.departmentId','departments.id')
          ->where('departments.status','Active')
          ->where('users.status','Active')
          ->where('users.roleId',3);
    if(!empty($orgId)){
      $usersQuery->where('users.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $usersQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $usersQuery->where('users.officeId',$officeId);
      $usersQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $usersQuery->where('departments.departmentId',$departmentId);
    }
    $users = $usersQuery->get();

    return $users;
  }

  public function dotValuesComplete($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $userPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId);
    $users = $this->userCount($userPer);
    $userCount = count($users);

    $dotValueRatingCompletedUserArr = array();
    $lastMonthUserCount = array();
    $thisMonthUserCount = array();
    $allPreviousMonthUserCount = array();
    foreach($users as $duValue)
    {
      $dotPerArr = array('orgId'=>$orgId,'userId'=>$duValue->id,'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend);
      $dotStatus = $this->getDotPerformance($dotPerArr);
      if ($dotStatus) {
        array_push($dotValueRatingCompletedUserArr, $duValue->id);
      }

      //Dot MM and MA count complete
      $dotRatingLastMonthArr = array('orgId' => $orgId,'userId'=>$duValue->id,'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend,'date'=>'lastMonth');
      $dotRatingThisMonthArr = array('orgId' => $orgId,'userId'=>$duValue->id,'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend,'date'=>'thisMonth');
      $dotRatingAllPreviousMonthArr = array('orgId' => $orgId,'userId'=>$duValue->id,'officeId'=>$officeId,'departmentId'=>$departmentId,'includeWeekend'=>$includeWeekend,'date'=>"allPreviousMonth");

      $lastMonthData = $this->getDotMMMACount($dotRatingLastMonthArr);
      $thisMonthData = $this->getDotMMMACount($dotRatingThisMonthArr);
      $allPreviousMonthData = $this->getDotMMMACount($dotRatingAllPreviousMonthArr);
      if ($lastMonthData) 
      {
          array_push($lastMonthUserCount, $duValue->id);
      }
      if ($thisMonthData) 
      {
          array_push($thisMonthUserCount, $duValue->id);
      }
      if ($allPreviousMonthData) 
      {
          array_push($allPreviousMonthUserCount, $duValue->id);
      }

    }
    $dotValueRatingCompletedUserArrCount = count($dotValueRatingCompletedUserArr);
    $dotCompleted = "0";
    if(!empty($dotValueRatingCompletedUserArrCount) && !empty($userCount))
    {
        $dotCompleted = round(($dotValueRatingCompletedUserArrCount/$userCount)*100,2);
    }

    $lastMonthUserArrCount = count($lastMonthUserCount);
    $thisMonthUserArrCount = count($thisMonthUserCount);
    $allPreviousMonthUserArrCount = count($allPreviousMonthUserCount);

    $MMArr = array('orgId'=>$orgId,'officeId'=>$officeId,'departmentId'=>$departmentId);

    $lastMonthStaffCount = $this->lastMonthStaffCount($MMArr);
    //Last Month value
    $lastMonthUserArr = 0;
    if (!empty($lastMonthUserArrCount) && !empty($lastMonthStaffCount)) 
    {
      $lastMonthUserArr = (($lastMonthUserArrCount)/($lastMonthStaffCount));
      // $lastMonthUserArr = (($lastMonthUserArrCount)/($lastMonthStaffCount))*100;
    }
    
    $thisMonthStaffCount = $this->thisMonthStaffCount($MMArr);
    //This Month value
    $thisMonthUserArr = 0;
    if (!empty($thisMonthUserArrCount) && !empty($thisMonthStaffCount)) 
    {
      $thisMonthUserArr = (($thisMonthUserArrCount)/($thisMonthStaffCount));
      // $thisMonthUserArr = (($thisMonthUserArrCount)/($thisMonthStaffCount))*100;
    }

    //all previous Month value
    $allPreviousMonthCount = 0;
    if (!empty($allPreviousMonthUserArrCount) && !empty($lastMonthStaffCount)) 
    {
      $allPreviousMonthCount = (($allPreviousMonthUserArrCount)/($lastMonthStaffCount));
      // $allPreviousMonthCount = (($allPreviousMonthUserArrCount)/($lastMonthStaffCount))*100;
    }

    $noOfPreviousMonths = 0;
    if (!empty($staff)) {
      $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));
    }else{
      $noOfPreviousMonths = 0;
    }

    $dotRatingCount = array();
    $dotRatingCount['MM'] = 0;
    $dotRatingCount['MA'] = 0;
    // $dotCountMA = 0;
    
    if (!empty($lastMonthUserArr)) {
        $dotRatingCount['MM'] = round(((-1*($lastMonthUserArr - $thisMonthUserArr))/$lastMonthUserArr),2);
    }
   
    if (!empty($allPreviousMonthCount) && !empty($noOfPreviousMonths)) {
        $dotCountMA = ($allPreviousMonthCount)/($noOfPreviousMonths);
        if (!empty($dotCountMA)) {
            $dotRatingCount['MA'] = round(((-1*($dotCountMA - $thisMonthUserArr))/$dotCountMA),2);
        }
    }

    return array('dotCompleted'=>$dotCompleted,'dotRatingCount'=>$dotRatingCount);
  }

  //check user given rating to the each value of dot
  public function getDotPerformance($perArr = array())
  {
    $orgId            = $perArr["orgId"];
    $officeId         = $perArr["officeId"];
    $departmentId     = $perArr["departmentId"];
    $userId           = $perArr["userId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $org = DB::table('organisations')->where('id',$orgId)->first();
   
    $query = DB::table('dots_values')  
        ->select('dots_values.id AS id')     
        ->leftjoin('dots_beliefs','dots_beliefs.id','dots_values.beliefId')
        ->leftjoin('dots','dots.id','dots_beliefs.dotId')
        ->where('dots_beliefs.status','Active')
        ->where('dots.status','Active')
        ->where('dots_values.status','Active');
    if($orgId) {
        $query->where('dots.orgId', $orgId);
    }
    $dotValues = $query->get();

    foreach($dotValues as $value){
      $dvValue = DB::table('dot_values_ratings')
          ->rightjoin('users','users.id','dot_values_ratings.userId')
          ->leftjoin('departments','users.departmentId','departments.id')
          ->where('departments.status','Active')
          ->where('users.status','Active')
          ->where('dot_values_ratings.userId',$userId)
          ->where('dot_values_ratings.valueId',$value->id)
          ->where('dot_values_ratings.status','Active');
      if ($includeWeekend != 1) {
        $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
        $currentDate    = date('Y-m-d'); 
        //Get weekends
        $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
        $dvValue->whereNotIn(DB::raw("(DATE_FORMAT(dot_values_ratings.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      }            
      if(!empty($officeId) && empty($departmentId)){
        $dvValue->where('users.officeId',$officeId);
      }elseif(!empty($officeId) && !empty($departmentId)) {
        $dvValue->where('users.officeId',$officeId);
        $dvValue->where('users.departmentId',$departmentId);
      }elseif(empty($officeId) && !empty($departmentId)) {
        $dvValue->where('departments.departmentId',$departmentId);
      }
      $isDotValueRating = $dvValue->first();
      if(!$isDotValueRating){
          return false;
      }
    }
    if (count($dotValues) == 0) {
      return false;
    }else {
      return true;
    }
  }

  public function getDotMMMACount($perArr=array())
  {
    $orgId            = $perArr["orgId"];
    $officeId         = $perArr["officeId"];
    $departmentId     = $perArr["departmentId"];
    $userId           = $perArr["userId"];
    $includeWeekend   = $perArr["includeWeekend"];
    $date             = $perArr["date"];

    $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    $createdDateStaff = $staff->created_at;

    $org = DB::table('organisations')->where('id',$orgId)->first();

    $query = DB::table('dots_values')  
        ->select('dots_values.id AS id')     
        ->leftjoin('dots_beliefs','dots_beliefs.id','dots_values.beliefId')
        ->leftjoin('dots','dots.id','dots_beliefs.dotId')
        ->where('dots_beliefs.status','Active')
        ->where('dots.status','Active')
        ->where('dots_values.status','Active');
    if($orgId) {
      $query->where('dots.orgId', $orgId);
    }
    if ($date == 'thisMonth') {
      // $query->where('dots_values.created_at','LIKE',date("Y-m")."%");
      $query->whereDate('dots_values.created_at','<=',date('Y-m-d'));
    }elseif ($date == 'lastMonth') {
      // $query->where('dots_values.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
      $query->whereDate('dots_values.created_at','<',date('Y-m'."-1"));
    }elseif ($date == 'allPreviousMonth') {
      // $query->where('dots_values.created_at','>=',$createdDateStaff);
      $query->whereDate('dots_values.created_at','<',date('Y-m'."-1"));
    }
    $dotValues = $query->get();
    foreach($dotValues as $value)
    {
      $dvValue = DB::table('dot_values_ratings')
          ->rightjoin('users','users.id','dot_values_ratings.userId')
          ->leftjoin('departments','users.departmentId','departments.id')
          ->where('departments.status','Active')
          ->where('users.status','Active')
          ->where('dot_values_ratings.userId',$userId)
          ->where('dot_values_ratings.valueId',$value->id)
          ->where('dot_values_ratings.status','Active');
      if ($includeWeekend != 1) {
        $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
        $currentDate    = date('Y-m-d'); 
        //Get weekends
        $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
        $dvValue->whereNotIn(DB::raw("(DATE_FORMAT(dot_values_ratings.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      }            
      if(!empty($officeId) && empty($departmentId)){
        $dvValue->where('users.officeId',$officeId);
      }elseif(!empty($officeId) && !empty($departmentId)) {
        $dvValue->where('users.officeId',$officeId);
        $dvValue->where('users.departmentId',$departmentId);
      }elseif(empty($officeId) && !empty($departmentId)) {
        $dvValue->where('departments.departmentId',$departmentId);
      }
      if ($date == 'thisMonth') {
        // $dvValue->where('dot_values_ratings.created_at','LIKE',date("Y-m")."%");
        $dvValue->whereDate('dot_values_ratings.created_at','<=',date('Y-m-d'));
      }
      elseif ($date == 'lastMonth') {
        // $dvValue->where('dot_values_ratings.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        $dvValue->whereDate('dot_values_ratings.created_at','<',date('Y-m'."-1"));
      }
      elseif ($date == 'allPreviousMonth') {
        // $dvValue->where('dot_values_ratings.created_at','>=',$createdDateStaff);
        $dvValue->whereDate('dot_values_ratings.created_at','<',date('Y-m'."-1"));
      }
      $isDotValueRating = $dvValue->first();
      if(!$isDotValueRating){
          return false;
      }
    }
    if (count($dotValues) == 0) {
      return false;
    }
    else
    {
      return true;
    }
  }

  //Team role Completed
  public function teamRoleComplete($perArr = array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $org = DB::table('organisations')->where('id',$orgId)->first();

    $userPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId);
    $users = $this->userCount($userPer);
    $userCount = count($users);

    $query = DB::table('cot_answers') 
        ->select('cot_answers.userId')
        ->distinct()
        ->rightjoin('users','users.id','cot_answers.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('cot_answers.status','Active');
    if ($includeWeekend != 1) {
      $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
      $currentDate    = date('Y-m-d'); 
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
      $query->whereNotIn(DB::raw("(DATE_FORMAT(cot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }    
    if($orgId){
      $query->where('cot_answers.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $query->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $query->where('users.officeId',$officeId);
      $query->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $query->where('departments.departmentId',$departmentId);
    }
    $teamRoleCompleteUserArr = $query->get();

    $teamRoleCompleteUserArrCount = count($teamRoleCompleteUserArr);

    $result = "0";
    if (!empty($teamRoleCompleteUserArrCount) && !empty($userCount))
    {
        $result = round(($teamRoleCompleteUserArrCount/$userCount)*100,2);
    }
    return $result;
  }

  //Team role MM and MA
  public function getTeamroleMAMMCount($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $MMArr = array('orgId'=>$orgId,'officeId'=>$officeId,'departmentId'=>$departmentId);

    $org = DB::table('organisations')->where('id',$orgId)->first();

    $lastMonthStaffCount = $this->lastMonthStaffCount($MMArr);
    // $lastMonth = date("Y-m", strtotime("-1 month"));

    //Last month query
    $lastMonthTeamRoleQuery = DB::table('cot_answers')
        ->leftjoin('users','users.id','cot_answers.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('cot_answers.status','Active')
        // ->where('cot_answers.created_at','LIKE',$lastMonth."%");
        ->whereDate('cot_answers.created_at','<',date('Y-m'."-1"));
    // if ($includeWeekend != 1) {
    //   //Get weekends
    //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($lastMonth,2);
    //   $lastMonthTeamRoleQuery->whereNotIn(DB::raw("(DATE_FORMAT(cot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    // }    
    if ($includeWeekend != 1) {
      $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
      $currentDate    = date('Y-m'."-1");
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
      $lastMonthTeamRoleQuery->whereNotIn(DB::raw("(DATE_FORMAT(cot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }   
    if(!empty($orgId)){
      $lastMonthTeamRoleQuery->where('cot_answers.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $lastMonthTeamRoleQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $lastMonthTeamRoleQuery->where('users.officeId',$officeId);
      $lastMonthTeamRoleQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $lastMonthTeamRoleQuery->where('departments.departmentId',$departmentId);
    }
    $lastMonthTeamRoleCount = $lastMonthTeamRoleQuery->count();

    //Last Month value
    $lastMonthTeamRole = 0;
    if (!empty($lastMonthTeamRoleCount) && !empty($lastMonthStaffCount)) 
    {
      $lastMonthTeamRole = (($lastMonthTeamRoleCount)/($lastMonthStaffCount));
      // $lastMonthTeamRole = (($lastMonthTeamRoleCount)/($lastMonthStaffCount))*100;
    }

    $thisMonthStaffCount = $this->thisMonthStaffCount($MMArr);
    // $thisMonth = date("Y-m");

    //This month query
    $thisMonthTeamRoleQuery = DB::table('cot_answers')
        ->leftjoin('users','users.id','cot_answers.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('cot_answers.status','Active')
        // ->where('cot_answers.created_at','LIKE',$thisMonth."%");
        ->whereDate('cot_answers.created_at','<=',date('Y-m-d'));
    // if ($includeWeekend != 1) {
    //   //Get weekends
    //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($thisMonth,2);
    //   $thisMonthTeamRoleQuery->whereNotIn(DB::raw("(DATE_FORMAT(cot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    // }    
    if ($includeWeekend != 1) {
      $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
      $currentDate    = date('Y-m-d'); 
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
      $thisMonthTeamRoleQuery->whereNotIn(DB::raw("(DATE_FORMAT(cot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }   
    if(!empty($orgId)){
      $thisMonthTeamRoleQuery->where('cot_answers.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $thisMonthTeamRoleQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $thisMonthTeamRoleQuery->where('users.officeId',$officeId);
      $thisMonthTeamRoleQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $thisMonthTeamRoleQuery->where('departments.departmentId',$departmentId);
    }
    $thisMonthTeamRoleCount = $thisMonthTeamRoleQuery->count();

    //This Month value
    $thisMonthTeamRole = 0;
    if (!empty($thisMonthTeamRoleCount) && !empty($thisMonthStaffCount)) 
    {
      $thisMonthTeamRole = (($thisMonthTeamRoleCount)/($thisMonthStaffCount));
      // $thisMonthTeamRole = (($thisMonthTeamRoleCount)/($thisMonthStaffCount))*100;
    }

    $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    $createdDateStaff = $staff->created_at;

    //All previous month data till last month
    $allPreviousMonthTeamRoleQuery = DB::table('cot_answers')
        ->leftjoin('users','users.id','cot_answers.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('cot_answers.status','Active')
        // ->where('cot_answers.created_at','>=',$createdDateStaff)
        ->whereDate('cot_answers.created_at','<',date('Y-m'."-1"));
    if ($includeWeekend != 1) {
      $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
      $currentDate    = date('Y-m'."-1"); 
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
      $allPreviousMonthTeamRoleQuery->whereNotIn(DB::raw("(DATE_FORMAT(cot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }    
    if(!empty($orgId)){
      $allPreviousMonthTeamRoleQuery->where('cot_answers.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $allPreviousMonthTeamRoleQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $allPreviousMonthTeamRoleQuery->where('users.officeId',$officeId);
      $allPreviousMonthTeamRoleQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $allPreviousMonthTeamRoleQuery->where('departments.departmentId',$departmentId);
    }
    $allPreviousMonthTeamRoleCount = $allPreviousMonthTeamRoleQuery->count();

    //all previous Month value
    $allPreviousMonthTeamRole = 0;
    if (!empty($allPreviousMonthTeamRoleCount) && !empty($lastMonthStaffCount)) 
    {
      $allPreviousMonthTeamRole = (($allPreviousMonthTeamRoleCount)/($lastMonthStaffCount));
      // $allPreviousMonthTeamRole = (($allPreviousMonthTeamRoleCount)/($lastMonthStaffCount))*100;
    }

    $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));

    $TeamRoleCount = array();
    $TeamRoleCount['MM'] = 0;
    $TeamRoleCount['MA'] = 0;
    // $TeamRoleCountMA = 0;
    
    if (!empty($lastMonthTeamRole)) {
      $TeamRoleCount['MM'] = round(((-1*($lastMonthTeamRole - $thisMonthTeamRole))/$lastMonthTeamRole),2);
    }

    if (!empty($allPreviousMonthTeamRole) && !empty($noOfPreviousMonths)) {

      $TeamRoleCountMA = ($allPreviousMonthTeamRole)/($noOfPreviousMonths);
      if (!empty($TeamRoleCountMA)) 
      {
        $TeamRoleCount['MA'] = round(((-1*($TeamRoleCountMA - $thisMonthTeamRole))/$TeamRoleCountMA),2);
      }
    }
    return $TeamRoleCount;
  }

  public function personalityTypeComplete($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $org = DB::table('organisations')->where('id',$orgId)->first();

    $userPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId);
    $users = $this->userCount($userPer);
    $userCount = count($users);

    $query = DB::table('cot_functional_lens_answers')
      ->select('cot_functional_lens_answers.userId')
      ->distinct()
      ->rightjoin('users','users.id','cot_functional_lens_answers.userId')
      ->leftjoin('departments','users.departmentId','departments.id')
      ->where('departments.status','Active')
      ->where('users.status','Active')
      ->where('cot_functional_lens_answers.status','Active');
    if ($includeWeekend != 1) {
      $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
      $currentDate    = date('Y-m-d'); 
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
      $query->whereNotIn(DB::raw("(DATE_FORMAT(cot_functional_lens_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }  
    if($orgId){
      $query->where('cot_functional_lens_answers.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $query->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $query->where('users.officeId',$officeId);
      $query->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $query->where('departments.departmentId',$departmentId);
    }
    $personalityTypeCompleteUserArr = $query->get();

    $personalityTypeCompleteUserArrcount = count($personalityTypeCompleteUserArr);

    $result = "0";
    if (!empty($personalityTypeCompleteUserArrcount) && !empty($userCount))
    {
        $result = round(($personalityTypeCompleteUserArrcount/$userCount)*100,2);
    }
    return $result;
  }

  public function getPersonalityTypeMAMMCount($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $org = DB::table('organisations')->where('id',$orgId)->first();

    $MMArr = array('orgId'=>$orgId,'officeId'=>$officeId,'departmentId'=>$departmentId);
    $lastMonthStaffCount = $this->lastMonthStaffCount($MMArr);
    // $lastMonth = date("Y-m", strtotime("-1 month"));

    //Last month query
    $lastMonthPersonalityTypeQuery = DB::table('cot_functional_lens_answers')
        ->leftjoin('users','users.id','cot_functional_lens_answers.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('cot_functional_lens_answers.status','Active')
        // ->where('cot_functional_lens_answers.created_at','LIKE',$lastMonth."%");
        ->whereDate('cot_functional_lens_answers.created_at','<',date('Y-m'."-1"));
    // if ($includeWeekend != 1) {
    //   //Get weekends
    //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($lastMonth,2);
    //   $lastMonthPersonalityTypeQuery->whereNotIn(DB::raw("(DATE_FORMAT(cot_functional_lens_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    // } 
    if ($includeWeekend != 1) {
      $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
      $currentDate    = date('Y-m'."-1");
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
      $lastMonthPersonalityTypeQuery->whereNotIn(DB::raw("(DATE_FORMAT(cot_functional_lens_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }     
    if(!empty($orgId)){
      $lastMonthPersonalityTypeQuery->where('cot_functional_lens_answers.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $lastMonthPersonalityTypeQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $lastMonthPersonalityTypeQuery->where('users.officeId',$officeId);
      $lastMonthPersonalityTypeQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $lastMonthPersonalityTypeQuery->where('departments.departmentId',$departmentId);
    }
    $lastMonthPersonalityTypeCount = $lastMonthPersonalityTypeQuery->count();

    //Last Month value
    $lastMonthPersonalityType = 0;
    if (!empty($lastMonthPersonalityTypeCount) && !empty($lastMonthStaffCount)) 
    {
      $lastMonthPersonalityType = (($lastMonthPersonalityTypeCount)/($lastMonthStaffCount));
      // $lastMonthPersonalityType = (($lastMonthPersonalityTypeCount)/($lastMonthStaffCount))*100;
    }

    $thisMonthStaffCount = $this->thisMonthStaffCount($MMArr);
    $thisMonth = date("Y-m");

    //This month query
    $thisMonthPersonalityTypeQuery = DB::table('cot_functional_lens_answers')
        ->leftjoin('users','users.id','cot_functional_lens_answers.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('cot_functional_lens_answers.status','Active')
        ->whereDate('cot_functional_lens_answers.created_at','<=',date('Y-m-d'));
        // ->where('cot_functional_lens_answers.created_at','LIKE',$thisMonth."%");
    // if ($includeWeekend != 1) {
    //   //Get weekends
    //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($thisMonth,2);
    //   $thisMonthPersonalityTypeQuery->whereNotIn(DB::raw("(DATE_FORMAT(cot_functional_lens_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    // } 
    if ($includeWeekend != 1) {
      $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
      $currentDate    = date('Y-m-d'); 
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
      $thisMonthPersonalityTypeQuery->whereNotIn(DB::raw("(DATE_FORMAT(cot_functional_lens_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }      
    if(!empty($orgId)){
      $thisMonthPersonalityTypeQuery->where('cot_functional_lens_answers.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $thisMonthPersonalityTypeQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $thisMonthPersonalityTypeQuery->where('users.officeId',$officeId);
      $thisMonthPersonalityTypeQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $thisMonthPersonalityTypeQuery->where('departments.departmentId',$departmentId);
    }
    $thisMonthPersonalityTypeCount = $thisMonthPersonalityTypeQuery->count();

    //This Month value
    $thisMonthPersonalityType = 0;
    if (!empty($thisMonthPersonalityTypeCount) && !empty($thisMonthStaffCount)) 
    {
      $thisMonthPersonalityType = (($thisMonthPersonalityTypeCount)/($thisMonthStaffCount));
      // $thisMonthPersonalityType = (($thisMonthPersonalityTypeCount)/($thisMonthStaffCount))*100;
    }

    $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    $createdDateStaff = $staff->created_at;

    //All previous month data till last month
    $allPreviousMonthPersonalityTypeQuery = DB::table('cot_functional_lens_answers')
        ->leftjoin('users','users.id','cot_functional_lens_answers.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('cot_functional_lens_answers.status','Active')
        // ->where('cot_functional_lens_answers.created_at','>=',$createdDateStaff)
        ->whereDate('cot_functional_lens_answers.created_at','<',date('Y-m'."-1"));
    if ($includeWeekend != 1) {
      //Get weekends
      $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
      $currentDate    = date('Y-m'."-1"); 
      $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
      $allPreviousMonthPersonalityTypeQuery->whereNotIn(DB::raw("(DATE_FORMAT(cot_functional_lens_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }    
    if(!empty($orgId)){
      $allPreviousMonthPersonalityTypeQuery->where('cot_functional_lens_answers.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $allPreviousMonthPersonalityTypeQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $allPreviousMonthPersonalityTypeQuery->where('users.officeId',$officeId);
      $allPreviousMonthPersonalityTypeQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $allPreviousMonthPersonalityTypeQuery->where('departments.departmentId',$departmentId);
    }
    $allPreviousMonthPersonalityTypeCount = $allPreviousMonthPersonalityTypeQuery->count();

    //all previous Month value
    $allPreviousMonthPersonalityType = 0;
    if (!empty($allPreviousMonthPersonalityTypeCount) && !empty($lastMonthStaffCount)) 
    {
      $allPreviousMonthPersonalityType = (($allPreviousMonthPersonalityTypeCount)/($lastMonthStaffCount));
      // $allPreviousMonthPersonalityType = (($allPreviousMonthPersonalityTypeCount)/($lastMonthStaffCount))*100;
    }

    $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));

    $PersonalityTypeCount = array();
    $PersonalityTypeCount['MM'] = 0;
    $PersonalityTypeCount['MA'] = 0;
    // $PersonalityTypeCountMA = 0;

    if (!empty($lastMonthPersonalityType)) {
      $PersonalityTypeCount['MM'] = round(((-1*($lastMonthPersonalityType - $thisMonthPersonalityType))/$lastMonthPersonalityType),2);
    }
    if (!empty($allPreviousMonthPersonalityType) && !empty($noOfPreviousMonths)) 
    {
        $PersonalityTypeCountMA = ($allPreviousMonthPersonalityType)/($noOfPreviousMonths);
        if (!empty($TeamRoleCountMA)) 
        {
            $PersonalityTypeCount['MA'] = round(((-1*($PersonalityTypeCountMA - $thisMonthPersonalityType))/$PersonalityTypeCountMA),2);
        }
    }
    return $PersonalityTypeCount;
  }

  public function motivationComplete($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $org = DB::table('organisations')->where('id',$orgId)->first();

    $userPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId);
    $users = $this->userCount($userPer);
    $userCount = count($users);

    $query = DB::table('sot_motivation_answers')
      ->select('sot_motivation_answers.userId')
      ->distinct()      
      ->leftJoin('users','users.id','sot_motivation_answers.userId')
      ->leftjoin('departments','users.departmentId','departments.id')
      ->where('departments.status','Active')
      ->where('sot_motivation_answers.status','Active')
      ->where('users.status','Active');
    if ($includeWeekend != 1) {
      $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
      $currentDate    = date('Y-m-d'); 
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
      $query->whereNotIn(DB::raw("(DATE_FORMAT(sot_motivation_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    } 
    if($orgId){
      $query->where('sot_motivation_answers.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $query->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $query->where('users.officeId',$officeId);
      $query->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $query->where('departments.departmentId',$departmentId);
    }
    $motivationCompleteUserArr = $query->get();

    $motivationCompleteUserArrCount = count($motivationCompleteUserArr);

    $result = "0";
    if (!empty($motivationCompleteUserArrCount) && !empty($userCount))
    {
        $result = round(($motivationCompleteUserArrCount/$userCount)*100,2);
    }
    return $result;
  }

  public function getMotivationMAMMCount($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $org = DB::table('organisations')->where('id',$orgId)->first();

    $MMArr = array('orgId'=>$orgId,'officeId'=>$officeId,'departmentId'=>$departmentId);
    $lastMonthStaffCount = $this->lastMonthStaffCount($MMArr);
    // $lastMonth = date("Y-m", strtotime("-1 month"));

    //Last month query
      $lastMonthMotivationQuery = DB::table('sot_motivation_answers')
          ->leftjoin('users','users.id','sot_motivation_answers.userId')
          ->leftjoin('departments','users.departmentId','departments.id')
          ->where('departments.status','Active')
          ->where('users.status','Active')
          ->where('sot_motivation_answers.status','Active')
          ->where('sot_motivation_answers.created_at','<',date('Y-m'."-1"));
          // ->where('sot_motivation_answers.created_at','LIKE',$lastMonth."%");
      // if ($includeWeekend != 1) {
      //   //Get weekends
      //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($lastMonth,2);
      //   $lastMonthMotivationQuery->whereNotIn(DB::raw("(DATE_FORMAT(sot_motivation_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      // }   
      if ($includeWeekend != 1) {
        $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
        $currentDate    = date('Y-m'."-1"); 
        //Get weekends
        $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
        $lastMonthMotivationQuery->whereNotIn(DB::raw("(DATE_FORMAT(sot_motivation_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      }  
      if(!empty($orgId)){
        $lastMonthMotivationQuery->where('sot_motivation_answers.orgId',$orgId);
      }
      if(!empty($officeId) && empty($departmentId)){
        $lastMonthMotivationQuery->where('users.officeId',$officeId);
      }elseif(!empty($officeId) && !empty($departmentId)) {
        $lastMonthMotivationQuery->where('users.officeId',$officeId);
        $lastMonthMotivationQuery->where('users.departmentId',$departmentId);
      }elseif(empty($officeId) && !empty($departmentId)) {
        $lastMonthMotivationQuery->where('departments.departmentId',$departmentId);
      }
      $lastMonthMotivationCount = $lastMonthMotivationQuery->count();

      //Last Month value
      $lastMonthMotivation = 0;
      if (!empty($lastMonthMotivationCount) && !empty($lastMonthStaffCount)) 
      {
        $lastMonthMotivation = (($lastMonthMotivationCount)/($lastMonthStaffCount));
        // $lastMonthMotivation = (($lastMonthMotivationCount)/($lastMonthStaffCount))*100;
      }

      $thisMonthStaffCount = $this->thisMonthStaffCount($MMArr);
      $thisMonth = date("Y-m");

      //This month query
      $thisMonthMotivationQuery = DB::table('sot_motivation_answers')
          ->leftjoin('users','users.id','sot_motivation_answers.userId')
          ->leftjoin('departments','users.departmentId','departments.id')
          ->where('departments.status','Active')
          ->where('users.status','Active')
          ->where('sot_motivation_answers.status','Active')
          ->where('sot_motivation_answers.created_at','<=',date('Y-m-d'));
          // ->where('sot_motivation_answers.created_at','LIKE',$thisMonth."%");
      // if ($includeWeekend != 1) {
      //   //Get weekends
      //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($thisMonth,2);
      //   $thisMonthMotivationQuery->whereNotIn(DB::raw("(DATE_FORMAT(sot_motivation_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      // }  
      if ($includeWeekend != 1) {
        $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
        $currentDate    = date('Y-m-d'); 
        //Get weekends
        $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
        $thisMonthMotivationQuery->whereNotIn(DB::raw("(DATE_FORMAT(sot_motivation_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      }   
      if(!empty($orgId)){
        $thisMonthMotivationQuery->where('sot_motivation_answers.orgId',$orgId);
      }
      if(!empty($officeId) && empty($departmentId)){
        $thisMonthMotivationQuery->where('users.officeId',$officeId);
      }elseif(!empty($officeId) && !empty($departmentId)) {
        $thisMonthMotivationQuery->where('users.officeId',$officeId);
        $thisMonthMotivationQuery->where('users.departmentId',$departmentId);
      }elseif(empty($officeId) && !empty($departmentId)) {
        $thisMonthMotivationQuery->where('departments.departmentId',$departmentId);
      }
      $thisMonthMotivationCount = $thisMonthMotivationQuery->count();

      //This Month value
      $thisMonthMotivation = 0;
      if (!empty($thisMonthMotivationCount) && !empty($thisMonthStaffCount)) 
      {
        $thisMonthMotivation = (($thisMonthMotivationCount)/($thisMonthStaffCount));
        // $thisMonthMotivation = (($thisMonthMotivationCount)/($thisMonthStaffCount))*100;
      }

      $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
      $createdDateStaff = $staff->created_at;

      //All previous month data till last month
      $allPreviousMonthMotivationQuery = DB::table('sot_motivation_answers')
          ->leftjoin('users','users.id','sot_motivation_answers.userId')
          ->leftjoin('departments','users.departmentId','departments.id')
          ->where('departments.status','Active')
          ->where('users.status','Active')
          ->where('sot_motivation_answers.status','Active')
          // ->where('sot_motivation_answers.created_at','>=',$createdDateStaff)
          ->whereDate('sot_motivation_answers.created_at','<',date('Y-m'."-1"));
      if ($includeWeekend != 1) {
        //Get weekends
        $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
        $currentDate    = date('Y-m'."-1"); 
        $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
        $allPreviousMonthMotivationQuery->whereNotIn(DB::raw("(DATE_FORMAT(sot_motivation_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      }    
      if(!empty($orgId)){
        $allPreviousMonthMotivationQuery->where('sot_motivation_answers.orgId',$orgId);
      }
      if(!empty($officeId) && empty($departmentId)){
        $allPreviousMonthMotivationQuery->where('users.officeId',$officeId);
      }elseif(!empty($officeId) && !empty($departmentId)) {
        $allPreviousMonthMotivationQuery->where('users.officeId',$officeId);
        $allPreviousMonthMotivationQuery->where('users.departmentId',$departmentId);
      }elseif(empty($officeId) && !empty($departmentId)) {
        $allPreviousMonthMotivationQuery->where('departments.departmentId',$departmentId);
      }
      $allPreviousMonthMotivationCount = $allPreviousMonthMotivationQuery->count();

      //all previous Month value
      $allPreviousMonthMotivation = 0;
      if (!empty($allPreviousMonthMotivationCount) && !empty($lastMonthStaffCount)) 
      {
        $allPreviousMonthMotivation = (($allPreviousMonthMotivationCount)/($lastMonthStaffCount));
        // $allPreviousMonthMotivation = (($allPreviousMonthMotivationCount)/($lastMonthStaffCount))*100;
      }
   
      $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));
    
      $motivationCount = array();
      $motivationCount['MM'] = 0;
      $motivationCount['MA'] = 0;
      // $motivationCountMA = 0;
      
      if (!empty($lastMonthMotivation)) {
        $motivationCount['MM'] = round(((-1*($lastMonthMotivation - $thisMonthMotivation))/$lastMonthMotivation),2);
      }

      if (!empty($allPreviousMonthMotivation) && !empty($noOfPreviousMonths)) {
        $motivationCountMA = ($allPreviousMonthMotivation)/($noOfPreviousMonths);
        if (!empty($motivationCountMA)) 
        {
            $motivationCount['MA'] = round(((-1*($motivationCountMA - $thisMonthMotivation))/$motivationCountMA),2);
        }
      }
      return $motivationCount;
  }

  public function kudosComplete($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $userPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId);
    $users = $this->userCount($userPer);
    $userCount = count($users);

    if ((date('D') == 'Mon') || (date('D') == 'Sun')) {
      $date = date('Y-m-d',strtotime('last Friday'));
    }else{
      $date = date('Y-m-d',strtotime(Carbon::yesterday()));
    }

    $query = DB::table('dot_bubble_rating_records')
        ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
        ->leftjoin('users','users.id','dot_bubble_rating_records.to_bubble_user_id')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->where('dots.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('dot_bubble_rating_records.status','Active')
        ->whereDate('dot_bubble_rating_records.created_at',$date);
        // ->whereDate('dot_bubble_rating_records.created_at',Carbon::yesterday());
    if ($includeWeekend != 1) {
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($date,1);
      $query->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    } 
    if(!empty($orgId)){
      $query->where('dots.orgId',$orgId);
    }
    if (!empty($officeId) && empty($departmentId)) {
      $query->where('users.officeId',$officeId);
    }
    elseif (!empty($officeId) && !empty($departmentId)) {
      $query->where('users.officeId',$officeId);
      $query->where('users.departmentId',$departmentId);
    }
    elseif (empty($officeId) && !empty($departmentId)) {
      $query->where('departments.departmentId',$departmentId);
    }  
    $thumbsupCount = $query->count('bubble_flag');

    $result = "0";
    if (!empty($thumbsupCount) && !empty($userCount))
    {
        $result = round(($thumbsupCount/$userCount),2);
    }
    return $result;

  }

  public function getBubbleRatingCount($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $org = DB::table('organisations')->where('id',$orgId)->first();

    $usersQuery = DB::table('users')
          ->select('users.id as id')
          ->leftjoin('departments','users.departmentId','departments.id')
          ->where('departments.status','Active')
          ->where('users.status','Active')
          ->where('users.roleId',3);
    if ((date('D') == 'Mon') || (date('D') == 'Sun')) {
      $usersQuery->where('users.created_at','<',date('Y-m-d', strtotime('last Friday')));
    }else{
      $usersQuery->where('users.created_at','<',date('Y-m-d', strtotime('-1 days')));
    } 
    if(!empty($orgId)){
      $usersQuery->where('users.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $usersQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $usersQuery->where('users.officeId',$officeId);
      $usersQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $usersQuery->where('departments.departmentId',$departmentId);
    }
    $lastToLastDaysUsers = $usersQuery->get();

    $userCountLastToLastDay = count($lastToLastDaysUsers);

    $userPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId);
    $users = $this->userCount($userPer);
    $lastDayUserCount = count($users);



    //Last to last day query
    $LastToLastDayQuery = DB::table('dot_bubble_rating_records')
        ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
        ->leftjoin('users','users.id','dot_bubble_rating_records.to_bubble_user_id')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->where('dots.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('dot_bubble_rating_records.status','Active');
    if ((date('D') == 'Mon') || (date('D') == 'Tue')) {
      $LastToLastDayQuery->whereDate('dot_bubble_rating_records.created_at',date('Y-m-d', strtotime('last Friday')));
    }else{
      $LastToLastDayQuery->whereDate('dot_bubble_rating_records.created_at',date('Y-m-d', strtotime('-2 days')));
    } 
    if ($includeWeekend != 1) {
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth(date('Y-m-d', strtotime('-2 days')),1);
      $LastToLastDayQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    } 
    if(!empty($orgId)){
      $LastToLastDayQuery->where('dots.orgId',$orgId);
    }
    if (!empty($officeId) && empty($departmentId)) {
      $LastToLastDayQuery->where('users.officeId',$officeId);
    }
    elseif (!empty($officeId) && !empty($departmentId)) {
      $LastToLastDayQuery->where('users.officeId',$officeId);
      $LastToLastDayQuery->where('users.departmentId',$departmentId);
    }
    elseif (empty($officeId) && !empty($departmentId)) {
      $LastToLastDayQuery->where('departments.departmentId',$departmentId);
    }  
    $LastToLastDayCount = $LastToLastDayQuery->count('bubble_flag');

    //Last to last day value
    $LastToLastDayBubble = 0;
    if (!empty($LastToLastDayCount) && !empty($userCountLastToLastDay)) 
    {
      $LastToLastDayBubble = ($LastToLastDayCount)/($userCountLastToLastDay);
    }


    //Yesterday query
    $yesterdayQuery = DB::table('dot_bubble_rating_records')
        ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
        ->leftjoin('users','users.id','dot_bubble_rating_records.to_bubble_user_id')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->where('dots.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('dot_bubble_rating_records.status','Active');
    if ((date('D') == 'Mon') || (date('D') == 'Sun')) {
      $yesterdayQuery->whereDate('dot_bubble_rating_records.created_at',date('Y-m-d', strtotime('last Friday')));
    }else{
      $yesterdayQuery->whereDate('dot_bubble_rating_records.created_at',date('Y-m-d', strtotime('-1 days')));
    } 
    if ($includeWeekend != 1) {
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth(date('Y-m-d', strtotime('-1 days')),1);
      $yesterdayQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    } 
    if(!empty($orgId)){
      $yesterdayQuery->where('dots.orgId',$orgId);
    }
    if (!empty($officeId) && empty($departmentId)) {
      $yesterdayQuery->where('users.officeId',$officeId);
    }
    elseif (!empty($officeId) && !empty($departmentId)) {
      $yesterdayQuery->where('users.officeId',$officeId);
      $yesterdayQuery->where('users.departmentId',$departmentId);
    }
    elseif (empty($officeId) && !empty($departmentId)) {
      $yesterdayQuery->where('departments.departmentId',$departmentId);
    }  
    $yesterdayCount = $yesterdayQuery->count('bubble_flag');

    //Last day value
    $yesterdayBubble = 0;
    if (!empty($yesterdayCount) && !empty($lastDayUserCount)) 
    {
      $yesterdayBubble = ($yesterdayCount)/($lastDayUserCount);
    }

    //All previous month data till last month
    $allPreviousDaysDataQuery = DB::table('dot_bubble_rating_records')
        ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
        ->leftjoin('users','users.id','dot_bubble_rating_records.to_bubble_user_id')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->where('dots.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('dot_bubble_rating_records.status','Active')
        ->whereDate('dot_bubble_rating_records.created_at','<',date('Y-m-d'));
    // if ($includeWeekend != 1) {
    //   //Get weekends
    //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth(date('Y-m-d', strtotime('-1 days')),1);
    //   $allPreviousDaysDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    // } 
    if ($includeWeekend != 1) {
      $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
      $currentDate    = date('Y-m-d'); 
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
      $allPreviousDaysDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }  

    if(!empty($orgId)){
      $allPreviousDaysDataQuery->where('dots.orgId',$orgId);
    }
    if (!empty($officeId) && empty($departmentId)) {
      $allPreviousDaysDataQuery->where('users.officeId',$officeId);
    }
    elseif (!empty($officeId) && !empty($departmentId)) {
      $allPreviousDaysDataQuery->where('users.officeId',$officeId);
      $allPreviousDaysDataQuery->where('users.departmentId',$departmentId);
    }
    elseif (empty($officeId) && !empty($departmentId)) {
      $allPreviousDaysDataQuery->where('departments.departmentId',$departmentId);
    }  
    $allPreviousDayDataCount = $allPreviousDaysDataQuery->count('bubble_flag');

    //No of days from first staff created of organisation till yesterday
    $firstStaffOfOrg = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    if (!empty($firstStaffOfOrg)) {
      $firstStaffOfOrgDate = strtotime($firstStaffOfOrg->created_at);
      $yesterdayDate = strtotime(Carbon::today());
      
      if ($firstStaffOfOrgDate < $yesterdayDate) {
        $firstStaffOfOrgDate = date_create(date('Y-m-d',strtotime($firstStaffOfOrg->created_at)));
        $yesterdayDate = date_create(Carbon::yesterday());
        
        $diff = date_diff($firstStaffOfOrgDate,$yesterdayDate);
        $noOfDays = $diff->format("%a")+1;
      }else{
        $noOfDays = 0;
      }
    }else{
      $noOfDays = 0;
    }

    $bubbleRatingCount = array();
    $bubbleRatingCount['DD'] = 0;
    $bubbleRatingCount['MA'] = 0;
    $bubbleCountMA = 0;
    
    if (!empty($LastToLastDayBubble)) {
      $bubbleRatingCount['DD'] = round(((-1*($LastToLastDayBubble - $yesterdayBubble))/$LastToLastDayBubble),2);
      // $bubbleRatingCount['DD'] = number_format(((-1*($LastToLastDayBubble - $yesterdayBubble))/$LastToLastDayBubble)*100,2);
    }
    if (!empty($allPreviousDayDataCount) && !empty($noOfDays)) {
      $bubbleCountMA = ($allPreviousDayDataCount)/($noOfDays);
      if (!empty($bubbleCountMA)) {
        $bubbleRatingCount['MA'] = round(((-1*($bubbleCountMA - $yesterdayBubble))/$bubbleCountMA),2);
        // $bubbleRatingCount['MA'] = number_format(((-1*($bubbleCountMA - $yesterdayBubble))/$bubbleCountMA)*100,2);
      }
    }
    return $bubbleRatingCount;
  }

  public function happyIndexComplete($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    if ((date('D') == 'Mon') || (date('D') == 'Sun')) {
      $date = date('Y-m-d',strtotime('last Friday'));
    }else{
      $date = date('Y-m-d',strtotime(Carbon::yesterday()));
    }

    // $userTypeArr    = array('3','2','1');  //3 for happy and 2 for avrage and 1 for sad

    $userTypeArr = DB::table('happy_index_mood_values')->where('status','Active')->get();

    $totalIndexes = 0;
    $totalIndex = 0;
    $totalUser    = 0;
    $happyIndexcount = 0;
    foreach($userTypeArr as $moodVal){      

      $happyIndexDataQuery = DB::table('happy_index_dashboard_graph')
          ->where('status','Active')
          ->where('categoryId',$moodVal->id)
          ->whereDate('date',$date);
        if(!empty($orgId)) {
            $happyIndexDataQuery->where('orgId',$orgId);
        }
        if(empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->whereNull('departmentId');
        } elseif(!empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->where('officeId',$officeId);
          $happyIndexDataQuery->where('departmentId',$departmentId);
        } elseif(empty($officeId) && !empty($departmentId)) {
          $happyIndexDataQuery->whereNull('officeId');
          $happyIndexDataQuery->where('departmentId',$departmentId);
        }
        // if ($includeWeekend != 1) {
        //   //Get weekends
        //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($date,1);
        //   $happyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
        // } 
        $happyIndexData = $happyIndexDataQuery->first();  
        if (!empty($happyIndexData)) {
          if($includeWeekend != 1){ //Exclude Weekend Days
            $happyIndexcount = $happyIndexData->without_weekend;
          }else{
              $happyIndexcount = $happyIndexData->with_weekend;
          }
        }else{
            $happyIndexcount = 0;
        }
        $totalIndex += $happyIndexcount; 
      }
      
      // $happyUserCountQuery = DB::table('happy_indexes')
      //     ->leftjoin('users','users.id','happy_indexes.userId')
      //     ->leftJoin('departments','departments.id','users.departmentId')
      //     ->where('happy_indexes.status','Active')
      //     ->where('happy_indexes.moodValue',$type->id)
      //     ->where('departments.status','Active')
      //     ->where('users.status','Active')
      //     ->whereDate('happy_indexes.created_at',Carbon::yesterday());
      // if ($includeWeekend != 1) {
      //   //Get weekends
      //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($date,1);
      //   $happyUserCountQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      // } 
      // if(!empty($orgId)){
      //   $happyUserCountQuery->where('users.orgId',$orgId);
      // }
      // if (!empty($officeId) && empty($departmentId)) {
      //     $happyUserCountQuery->where('users.officeId',$officeId);
      // }elseif (!empty($officeId) && !empty($departmentId)) {
      //     $happyUserCountQuery->where('users.officeId',$officeId);
      //     $happyUserCountQuery->where('users.departmentId',$departmentId);
      // }elseif (empty($officeId) && !empty($departmentId)) {
      //     $happyUserCountQuery->where('departments.departmentId',$departmentId);
      // }  
      // $happyUserCount = $happyUserCountQuery->count();
      // $totalUser +=$happyUserCount;
      // if($type==3){
      //     $totalIndexes += $happyUserCount;
      // }       
    // }
    // $happyIndexcount = 0;
    // if($totalIndexes !=0 && $totalUser !=0){ 
    //   $happyIndexcount = round((($totalIndexes/($totalUser))*100),2);
    // }
    return $totalIndex;
  }

  public function getHappyIndexMMMA($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    // $userTypeArr = array('3','2','1');
    $userTypeArr = DB::table('happy_index_mood_values')->where('id',3)->where('status','Active')->get();

    //get yesterday's previous day happy index
    // $totalIndexesOfyesterdaysPreviousday = 0;
    // $totalUserOfyesterdaysPreviousday    = 0;
    // $totalHappyUserYesterday = 0;
    // $totalIndexesOfyesterday = 0;

    $happyIndex = array();
    $happyIndex['DD'] = 0;
    $happyIndex['MA'] = 0;
    $yesterdayHappyIndexcount = 0;
    $previousyesHappyIndexcount = 0;
    $allPreviousDayHappyIndexData = 0;
    foreach($userTypeArr as $moodVal) {

      //Yesterday user percent
      $yesterdayHappyIndexDataQuery = DB::table('happy_index_dashboard_graph')
          ->where('status','Active')
          ->where('categoryId',$moodVal->id);
      if ((date('D') == 'Mon') || (date('D') == 'Sun')) {
        $yesterdayHappyIndexDataQuery->whereDate('date',date('Y-m-d', strtotime('last Friday')));
      }else{
        $yesterdayHappyIndexDataQuery->whereDate('date',date('Y-m-d', strtotime('-1 days')));
      }  
      if(!empty($orgId)) {
          $yesterdayHappyIndexDataQuery->where('orgId',$orgId);
      }
      if(empty($officeId) && empty($departmentId)) {
        $yesterdayHappyIndexDataQuery->whereNull('officeId');
        $yesterdayHappyIndexDataQuery->whereNull('departmentId');
      } elseif(!empty($officeId) && empty($departmentId)) {
        $yesterdayHappyIndexDataQuery->where('officeId',$officeId);
        $yesterdayHappyIndexDataQuery->whereNull('departmentId');
      } elseif(!empty($officeId) && !empty($departmentId)) {
        $yesterdayHappyIndexDataQuery->where('officeId',$officeId);
        $yesterdayHappyIndexDataQuery->where('departmentId',$departmentId);
      } elseif(empty($officeId) && !empty($departmentId)) {
        $yesterdayHappyIndexDataQuery->whereNull('officeId');
        $yesterdayHappyIndexDataQuery->where('departmentId',$departmentId);
      }
      // if ($includeWeekend != 1) {
      //   //Get weekends
      //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth(date('Y-m-d', strtotime('-1 days')),1);
      //   $yesterdayHappyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
      // } 
      $yesterdayHappyIndexData = $yesterdayHappyIndexDataQuery->first();  
      if (!empty($yesterdayHappyIndexData)) {
        if($includeWeekend != 1){ //Exclude Weekend Days
          $yesterdayHappyIndexcount = $yesterdayHappyIndexData->without_weekend;
        }else{
            $yesterdayHappyIndexcount = $yesterdayHappyIndexData->with_weekend;
        }
      }else{
          $yesterdayHappyIndexcount = 0;
      }

      //Previous Yesterday user percent
      $previousyesHappyIndexDataQuery = DB::table('happy_index_dashboard_graph')
        ->where('status','Active')
        ->where('categoryId',$moodVal->id);
      if ((date('D') == 'Mon') || (date('D') == 'Tue')) {
        $previousyesHappyIndexDataQuery->whereDate('date',date('Y-m-d', strtotime('last Friday')));
      }else{
        $previousyesHappyIndexDataQuery->whereDate('date',date('Y-m-d', strtotime('-2 days')));
      }  
      if(!empty($orgId)) {
          $previousyesHappyIndexDataQuery->where('orgId',$orgId);
      }
      if(empty($officeId) && empty($departmentId)) {
        $previousyesHappyIndexDataQuery->whereNull('officeId');
        $previousyesHappyIndexDataQuery->whereNull('departmentId');
      } elseif(!empty($officeId) && empty($departmentId)) {
        $previousyesHappyIndexDataQuery->where('officeId',$officeId);
        $previousyesHappyIndexDataQuery->whereNull('departmentId');
      } elseif(!empty($officeId) && !empty($departmentId)) {
        $previousyesHappyIndexDataQuery->where('officeId',$officeId);
        $previousyesHappyIndexDataQuery->where('departmentId',$departmentId);
      } elseif(empty($officeId) && !empty($departmentId)) {
        $previousyesHappyIndexDataQuery->whereNull('officeId');
        $previousyesHappyIndexDataQuery->where('departmentId',$departmentId);
      }
      // if ($includeWeekend != 1) {
      //   //Get weekends
      //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth(date('Y-m-d', strtotime('-2 days')),1);
      //   $previousyesHappyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
      // } 
      $previousyesHappyIndexData = $previousyesHappyIndexDataQuery->first();  
      if (!empty($previousyesHappyIndexData)) {
        if($includeWeekend != 1){ //Exclude Weekend Days
          $previousyesHappyIndexcount = $previousyesHappyIndexData->without_weekend;
        }else{
            $previousyesHappyIndexcount = $previousyesHappyIndexData->with_weekend;
        }
      }else{
          $previousyesHappyIndexcount = 0;
      }

      //Happy index DD
      $happyIndexDD = 0;
      if($previousyesHappyIndexcount !=0 && $yesterdayHappyIndexcount !=0) {
          $happyIndexDD = ((-1*($previousyesHappyIndexcount - $yesterdayHappyIndexcount))/($previousyesHappyIndexcount));
      }
      $happyIndex['DD'] =round($happyIndexDD,2);

      //Start date of happy index
      $happyIndexDashboardStartQuery = DB::table('happy_index_dashboard_graph')
        ->where('status','Active');
      if(!empty($orgId)) {
        $happyIndexDashboardStartQuery->where('orgId',$orgId);
      }
      if(empty($officeId) && empty($departmentId)) {
        $happyIndexDashboardStartQuery->whereNull('officeId');
        $happyIndexDashboardStartQuery->whereNull('departmentId');
      } elseif(!empty($officeId) && empty($departmentId)) {
        $happyIndexDashboardStartQuery->where('officeId',$officeId);
        $happyIndexDashboardStartQuery->whereNull('departmentId');
      } elseif(!empty($officeId) && !empty($departmentId)) {
        $happyIndexDashboardStartQuery->where('officeId',$officeId);
        $happyIndexDashboardStartQuery->where('departmentId',$departmentId);
      } elseif(empty($officeId) && !empty($departmentId)) {
        $happyIndexDashboardStartQuery->whereNull('officeId');
        $happyIndexDashboardStartQuery->where('departmentId',$departmentId);
      }
      $happyIndexDashboardStart = $happyIndexDashboardStartQuery->orderBy('id','ASC')->first();

      if (!empty($happyIndexDashboardStart)) {
        $startDate = $happyIndexDashboardStart->date;
      }else{
        $startDate = date('Y-m-d',strtotime('-1 days'));
      }
      $currentDate = date('Y-m-d',strtotime('-1 days'));

      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($startDate, $currentDate);

      //All previous days percent
      $allPreviousDayHappyIndexDataQuery = DB::table('happy_index_dashboard_graph')
        ->where('status','Active')
        ->where('categoryId',$moodVal->id)
        ->whereDate('date','<',date('Y-m-d'))
        ->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
      if(!empty($orgId)) {
          $allPreviousDayHappyIndexDataQuery->where('orgId',$orgId);
      }
      if(empty($officeId) && empty($departmentId)) {
        $allPreviousDayHappyIndexDataQuery->whereNull('officeId');
        $allPreviousDayHappyIndexDataQuery->whereNull('departmentId');
      } elseif(!empty($officeId) && empty($departmentId)) {
        $allPreviousDayHappyIndexDataQuery->where('officeId',$officeId);
        $allPreviousDayHappyIndexDataQuery->whereNull('departmentId');
      } elseif(!empty($officeId) && !empty($departmentId)) {
        $allPreviousDayHappyIndexDataQuery->where('officeId',$officeId);
        $allPreviousDayHappyIndexDataQuery->where('departmentId',$departmentId);
      } elseif(empty($officeId) && !empty($departmentId)) {
        $allPreviousDayHappyIndexDataQuery->whereNull('officeId');
        $allPreviousDayHappyIndexDataQuery->where('departmentId',$departmentId);
      }
      // if ($includeWeekend != 1) {
      //   // $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
      //   // $currentDate    = date('Y-m-d'); 
      //   //Get weekends
      //   $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($startDate, $currentDate);
      //   $allPreviousDayHappyIndexDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_index_dashboard_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
      // }
      if($includeWeekend != 1){ //Exclude Weekend Days
        $allPreviousDayHappyIndexData = $allPreviousDayHappyIndexDataQuery->sum('without_weekend');  
      }else{
        $allPreviousDayHappyIndexData = $allPreviousDayHappyIndexDataQuery->sum('with_weekend');  
      }
     
      //No. of weekdays
      $start = new DateTime($startDate);
      $end = new DateTime($currentDate);
      // otherwise the  end date is excluded (bug?)
      $end->modify('+1 day');
      $interval = $end->diff($start);
      // total days
      $days = $interval->days;
      // create an iterateable period of date (P1D equates to 1 day)
      $period = new DatePeriod($start, new DateInterval('P1D'), $end);
      // best stored as array, so you can add more than one
      $holidays = [];
      foreach($period as $dt) {
          $curr = $dt->format('D');
          // substract if Saturday or Sunday
          if ($curr == 'Sat' || $curr == 'Sun') {
              $days--;
          }
          // (optional) for the updated question
          elseif (in_array($dt->format('Y-m-d'), $holidays)) {
              $days--;
          }
      }
      $weekdaysCount  = $days;

      //Calculate MA for happy index
      $movingAvgIndex = 0;
      if($weekdaysCount !=0 && $allPreviousDayHappyIndexData !=0){
          $movingAvgIndex = ($allPreviousDayHappyIndexData/$weekdaysCount);  
      }

      $maValue = 0;
      if($movingAvgIndex !=0){
          //-1(MA – yesterday's H.I. value)/ MA
          $maValue = ((-1*($movingAvgIndex - $yesterdayHappyIndexcount))/($movingAvgIndex));  
          // $maValue = ((-1*($movingAvgIndex - $happyIndexPerOfyesterday))/($movingAvgIndex))*100;  
          //$maValue = -1*($movingAvgIndex - $happyIndexPer)/$movingAvgIndex;  
      }
      $happyIndex['MA'] = round($maValue,2);
    }









      // $happyUserCountQuery = DB::table('happy_indexes')
      //     ->leftjoin('users','users.id','happy_indexes.userId')
      //     ->leftJoin('departments','departments.id','users.departmentId')
      //     ->where('happy_indexes.status','Active')
      //     ->where('happy_indexes.moodValue',$type)
      //     ->where('departments.status','Active')
      //     ->where('users.status','Active')    
      //     ->whereDate('happy_indexes.created_at', date('Y-m-d', strtotime('-2 days')));
      // if ($includeWeekend != 1) {
      //   //Get weekends
      //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth(date('Y-m-d', strtotime('-2 days')),1);
      //   $happyUserCountQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      // } 
      // if(!empty($orgId)){
      //   $happyUserCountQuery->where('users.orgId',$orgId);
      // }
      // if (!empty($officeId) && empty($departmentId)) {
      //     $happyUserCountQuery->where('users.officeId',$officeId);
      // }elseif (!empty($officeId) && !empty($departmentId)) {
      //     $happyUserCountQuery->where('users.officeId',$officeId);
      //     $happyUserCountQuery->where('users.departmentId',$departmentId);
      // }elseif (empty($officeId) && !empty($departmentId)) {
      //     $happyUserCountQuery->where('departments.departmentId',$departmentId);
      // }  
      // $happyUserCount = $happyUserCountQuery->count();

      // $totalUserOfyesterdaysPreviousday +=$happyUserCount;

      // $yesterdayUserQuery = DB::table('happy_indexes')
      //     ->leftjoin('users','users.id','happy_indexes.userId')
      //     ->leftJoin('departments','departments.id','users.departmentId')
      //     ->where('happy_indexes.status','Active')
      //     ->where('happy_indexes.moodValue',$type)
      //     ->where('departments.status','Active')
      //     ->where('users.status','Active')    
      //     ->whereDate('happy_indexes.created_at', date('Y-m-d', strtotime('-1 days')));
      // if ($includeWeekend != 1) {
      //   //Get weekends
      //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth(date('Y-m-d', strtotime('-1 days')),1);
      //   $yesterdayUserQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
      // } 
      // if(!empty($orgId)){
      //   $yesterdayUserQuery->where('users.orgId',$orgId);
      // }
      // if (!empty($officeId) && empty($departmentId)) {
      //     $yesterdayUserQuery->where('users.officeId',$officeId);
      // }elseif (!empty($officeId) && !empty($departmentId)) {
      //     $yesterdayUserQuery->where('users.officeId',$officeId);
      //     $yesterdayUserQuery->where('users.departmentId',$departmentId);
      // }elseif (empty($officeId) && !empty($departmentId)) {
      //     $yesterdayUserQuery->where('departments.departmentId',$departmentId);
      // }  
      // $yesterdayUserCount = $yesterdayUserQuery->count();

      // $totalHappyUserYesterday +=$yesterdayUserCount;

      // if($type==3) 
      // {
      //   $totalIndexesOfyesterdaysPreviousday += $happyUserCount;
        
      //   $totalIndexesOfyesterday += $yesterdayUserCount;
      // }       
    

    // $happyIndexPerOfyesterdaysPreviousday = 0;
    // if($totalIndexesOfyesterdaysPreviousday !=0 && $totalUserOfyesterdaysPreviousday !=0)
    // {
    //     $happyIndexPerOfyesterdaysPreviousday = ($totalIndexesOfyesterdaysPreviousday/($totalUserOfyesterdaysPreviousday));
    // }

    // $happyIndexPerOfyesterday = 0;
    // if($totalIndexesOfyesterday !=0 && $totalHappyUserYesterday !=0)
    // {
    //     $happyIndexPerOfyesterday = ($totalIndexesOfyesterday/($totalHappyUserYesterday));       
    // }

    //((-1*(day before yesterday H.I. value – yesterday's H.I. value)) /( day before yesterday H.I. value))*100

    // $happyIndexDD = 0;
    // if($happyIndexPerOfyesterdaysPreviousday !=0) 
    // {
    //     $happyIndexDD = ((-1*($happyIndexPerOfyesterdaysPreviousday - $happyIndexPerOfyesterday))/($happyIndexPerOfyesterdaysPreviousday));
    //     // $happyIndexDD = ((-1*($happyIndexPerOfyesterdaysPreviousday - $happyIndexPerOfyesterday))/($happyIndexPerOfyesterdaysPreviousday))*100;
    // }
    // $happyIndex['DD'] =round($happyIndexDD,2);

    // $org = DB::table('organisations')->where('id',$orgId)->first();

    // $happyFacesQuery = DB::table('happy_indexes')
    //   ->leftjoin('users','users.id','happy_indexes.userId')
    //   ->where('happy_indexes.status','Active')
    //   ->where('users.status','Active')
    //   ->where('happy_indexes.moodValue','3')
    //   ->where('users.orgId',$orgId)        
    //   ->whereDate('happy_indexes.created_at', '<', date('Y-m-d'));

    //   // if ($includeWeekend != 1) {
    //   //   //Get weekends
    //   //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth(date('Y-m-d', strtotime('-1 days')),1);
    //   //   $happyFacesQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    //   // } 
    //   if ($includeWeekend != 1) {
    //     $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
    //     $currentDate    = date('Y-m-d'); 
    //     //Get weekends
    //     $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
    //     $happyFacesQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    //   }  
    //   if(!empty($orgId)){
    //     $happyFacesQuery->where('users.orgId',$orgId);
    //   }
    //   if (!empty($officeId) && empty($departmentId)) {
    //       $happyFacesQuery->where('users.officeId',$officeId);
    //   }elseif (!empty($officeId) && !empty($departmentId)) {
    //       $happyFacesQuery->where('users.officeId',$officeId);
    //       $happyFacesQuery->where('users.departmentId',$departmentId);
    //   }elseif (empty($officeId) && !empty($departmentId)) {
    //     $happyFacesQuery->leftJoin('departments','departments.id','users.departmentId')
    //       ->where('departments.status','Active')
    //       ->where('departments.departmentId',$departmentId);
    //   }  
    // $happyFacesCount = $happyFacesQuery->count();

    //No of days from first staff created of organisation till yesterday
    // $firstStaffOfOrg = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    // if (!empty($firstStaffOfOrg)) {
    //     $firstStaffOfOrgDate = strtotime($firstStaffOfOrg->created_at);
    //     $yesterdayDate = strtotime(Carbon::today());
        
    //     if ($firstStaffOfOrgDate < $yesterdayDate) {
    //         $firstStaffOfOrgDate = date_create(date('Y-m-d',strtotime($firstStaffOfOrg->created_at)));
    //         $yesterdayDate = date_create(Carbon::yesterday());
            
    //         $diff = date_diff($firstStaffOfOrgDate,$yesterdayDate);
    //         $noOfDays = $diff->format("%a")+1;
    //     }else{
    //         $noOfDays = 0;
    //     }
    // }else{
    //     $noOfDays = 0;
    // }

    // $movingAvgIndex = 0;

    // if($noOfDays !=0 && $happyFacesCount !=0){
    //     $movingAvgIndex = ($happyFacesCount/$noOfDays);  
    // }

    // $maValue = 0;
    // if($movingAvgIndex !=0){
    //     //-1(MA – yesterday's H.I. value)/ MA
    //     $maValue = ((-1*($movingAvgIndex - $happyIndexPerOfyesterday))/($movingAvgIndex));  
    //     // $maValue = ((-1*($movingAvgIndex - $happyIndexPerOfyesterday))/($movingAvgIndex))*100;  
    //     //$maValue = -1*($movingAvgIndex - $happyIndexPer)/$movingAvgIndex;  
    // }
    // $happyIndex['MA'] = round($maValue,2);

    return $happyIndex;
  }

  public function getImprovements($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $userPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId);
    $users = $this->userCount($userPer);
    $userCount = count($users);

    $lastMonth = date("Y-m", strtotime("-1 month"));

    $improvementQuery = DB::table('iot_feedbacks')
        ->leftjoin('users','users.id','iot_feedbacks.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('iot_feedbacks.status','!=','Inactive')
        ->where('iot_feedbacks.created_at','LIKE',$lastMonth."%");
    if ($includeWeekend != 1) {
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($lastMonth,2);
      $improvementQuery->whereNotIn(DB::raw("(DATE_FORMAT(iot_feedbacks.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    } 
    if(!empty($orgId)){
        $improvementQuery->where('iot_feedbacks.orgId',$orgId);
    }
    if (!empty($officeId) && empty($departmentId)) {
        $improvementQuery->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $improvementQuery->where('users.officeId',$officeId);
        $improvementQuery->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $improvementQuery->where('departments.departmentId',$departmentId);
    }  
    $improvements = $improvementQuery->count();

    $result = "0";
    if(!empty($improvements) && !empty($userCount)) {
        $result = round(($improvements/$userCount), 2);
    }
    return $result;
  }

  public function getImprovementsMMMA($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $MMArr = array('orgId'=>$orgId,'officeId'=>$officeId,'departmentId'=>$departmentId);
    $lastMonthStaffCount = $this->lastMonthStaffCount($MMArr);
    $lastMonth = date("Y-m", strtotime("-1 month"));

    //Last month query
    $lastMonthQuery = DB::table('iot_feedbacks')
        ->leftjoin('users','users.id','iot_feedbacks.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('iot_feedbacks.status','!=','Inactive')
        ->where('iot_feedbacks.created_at','LIKE',$lastMonth."%");
    if ($includeWeekend != 1) {
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($lastMonth,2);
      $lastMonthQuery->whereNotIn(DB::raw("(DATE_FORMAT(iot_feedbacks.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }    
    if(!empty($orgId)){
      $lastMonthQuery->where('iot_feedbacks.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $lastMonthQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $lastMonthQuery->where('users.officeId',$officeId);
      $lastMonthQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $lastMonthQuery->where('departments.departmentId',$departmentId);
    }
    $lastMonthImprovementCount = $lastMonthQuery->count();

    //Last Month value
    $lastMonthImprovement = 0;
    if (!empty($lastMonthImprovementCount) && !empty($lastMonthStaffCount)) 
    {
      $lastMonthImprovement = (($lastMonthImprovementCount)/($lastMonthStaffCount));
      // $lastMonthImprovement = (($lastMonthImprovementCount)/($lastMonthStaffCount))*100;
    }
    
    $thisMonthStaffCount = $this->thisMonthStaffCount($MMArr);
    $thisMonth = date("Y-m");

    //This month query
    $thisMonthQuery = DB::table('iot_feedbacks')
        ->leftjoin('users','users.id','iot_feedbacks.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('iot_feedbacks.status','!=','Inactive')
        ->where('iot_feedbacks.created_at','LIKE',$thisMonth."%");
    if ($includeWeekend != 1) {
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($thisMonth,2);
      $thisMonthQuery->whereNotIn(DB::raw("(DATE_FORMAT(iot_feedbacks.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }    
    if(!empty($orgId)){
      $thisMonthQuery->where('iot_feedbacks.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $thisMonthQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $thisMonthQuery->where('users.officeId',$officeId);
      $thisMonthQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $thisMonthQuery->where('departments.departmentId',$departmentId);
    }
    $thisMonthImprovementCount = $thisMonthQuery->count();

    //This Month value
    $thisMonthImprovement = 0;
    if (!empty($thisMonthImprovementCount) && !empty($thisMonthStaffCount)) 
    {
      $thisMonthImprovement = (($thisMonthImprovementCount)/($thisMonthStaffCount));
      // $thisMonthImprovement = (($thisMonthImprovementCount)/($thisMonthStaffCount))*100;
    }

    $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    $createdDateStaff = $staff->created_at;

    //All previous month data till last month
    $allPreviousMonthDataQuery = DB::table('iot_feedbacks')
        ->leftjoin('users','users.id','iot_feedbacks.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('iot_feedbacks.status','!=','Inactive')
        ->where('iot_feedbacks.created_at','>=',$createdDateStaff)
        ->whereDate('iot_feedbacks.created_at','<',date('Y-m'."-1"));
    if ($includeWeekend != 1) {
      //Get weekends
      $currentDate    = date('Y-m'."-1"); 
      $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($createdDateStaff, $currentDate);
      $allPreviousMonthDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(iot_feedbacks.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }    
    if(!empty($orgId)){
      $allPreviousMonthDataQuery->where('iot_feedbacks.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $allPreviousMonthDataQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $allPreviousMonthDataQuery->where('users.officeId',$officeId);
      $allPreviousMonthDataQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $allPreviousMonthDataQuery->where('departments.departmentId',$departmentId);
    }
    $allPreviousMonthDataCount = $allPreviousMonthDataQuery->count();

    //all previous Month value
    $allPreviousMonthData = 0;
    if (!empty($allPreviousMonthDataCount) && !empty($lastMonthStaffCount)) 
    {
      $allPreviousMonthData = (($allPreviousMonthDataCount)/($lastMonthStaffCount));
      // $allPreviousMonthData = (($allPreviousMonthDataCount)/($lastMonthStaffCount))*100;
    }

    $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));

    $improvementCount = array();
    $improvementCount['MM'] = 0;
    $improvementCount['MA'] = 0;
    // $improvementMA = 0;
    
    if (!empty($lastMonthImprovement)) 
    {
      $improvementCount['MM'] = round(((-1*($lastMonthImprovement - $thisMonthImprovement))/$lastMonthImprovement),2);
    }
    if(!empty($allPreviousMonthData) && !empty($noOfPreviousMonths)) {

        $improvementMA = ($allPreviousMonthData)/($noOfPreviousMonths);
        if (!empty($improvementMA)) {
            
            $improvementCount['MA'] = round(((-1*($improvementMA - $thisMonthImprovement))/$improvementMA),2);
        }
    }
    return $improvementCount;
  }

  public function getCultureStructure($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $lastMonth = date("Y-m", strtotime("-1 month"));

    $userPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId);
    $users = $this->userCount($userPer);
    $userCount = count($users);

    $query = DB::table('sot_answers')   
      ->select('sot_answers.userId')
      ->distinct()      
      ->leftJoin('users','users.id','sot_answers.userId')
      ->leftJoin('departments','departments.id','users.departmentId')
      ->where('departments.status','Active')
      ->where('sot_answers.status','Active')
      ->where('users.status','Active')
      ->where('sot_answers.updated_at','LIKE',$lastMonth."%");
      if ($includeWeekend != 1) {
        //Get weekends
        $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($lastMonth,2);
        $query->whereNotIn(DB::raw("(DATE_FORMAT(sot_answers.updated_at,'%Y-%m-%d'))"),$getWeekendDates);
      } 
      if($orgId){
          $query->where('users.orgId',$orgId);
      }
      if (!empty($officeId) && empty($departmentId)) {
        $query->where('users.officeId',$officeId);
      }elseif (!empty($officeId) && !empty($departmentId)) {
        $query->where('users.officeId',$officeId);
        $query->where('users.departmentId',$departmentId);
      }elseif (empty($officeId) && !empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
      }  
      $cultureStructureStatus = $query->get();

      $cultureStructureCount = count($cultureStructureStatus);

      $result = "0";
      if (!empty($cultureStructureCount) && !empty($userCount)) {
        $result = round(($cultureStructureCount/$userCount)*100,2);
      }
      return $result;
  }

  public function getCultureStructureMAMMCount($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $MMArr = array('orgId'=>$orgId,'officeId'=>$officeId,'departmentId'=>$departmentId);
    $lastMonthStaffCount = $this->lastMonthStaffCount($MMArr);
    $lastMonth = date("Y-m", strtotime("-1 month"));

    //Last month query
    $lastMonthCultureStructureQuery = DB::table('sot_answers')
        ->leftjoin('users','users.id','sot_answers.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('sot_answers.status','Active')
        ->where('sot_answers.created_at','LIKE',$lastMonth."%");
    if ($includeWeekend != 1) {
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($lastMonth,2);
      $lastMonthCultureStructureQuery->whereNotIn(DB::raw("(DATE_FORMAT(sot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }    
    if(!empty($orgId)){
      $lastMonthCultureStructureQuery->where('users.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $lastMonthCultureStructureQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $lastMonthCultureStructureQuery->where('users.officeId',$officeId);
      $lastMonthCultureStructureQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $lastMonthCultureStructureQuery->where('departments.departmentId',$departmentId);
    }
    $lastMonthCultureStructureCount = $lastMonthCultureStructureQuery->count();

    //Last Month value
    $lastMonthCultureStructure = 0;
    if (!empty($lastMonthCultureStructureCount) && !empty($lastMonthStaffCount)) 
    {
      $lastMonthCultureStructure = (($lastMonthCultureStructureCount)/($lastMonthStaffCount));
      // $lastMonthCultureStructure = (($lastMonthCultureStructureCount)/($lastMonthStaffCount))*100;
    }

    $thisMonthStaffCount = $this->thisMonthStaffCount($MMArr);
    $thisMonth = date("Y-m");

    //This month query
    $thisMonthCultureStructureQuery = DB::table('sot_answers')
        ->leftjoin('users','users.id','sot_answers.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('sot_answers.status','Active')
        ->where('sot_answers.created_at','LIKE',$thisMonth."%");
    if ($includeWeekend != 1) {
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($thisMonth,2);
      $thisMonthCultureStructureQuery->whereNotIn(DB::raw("(DATE_FORMAT(sot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }    
    if(!empty($orgId)){
      $thisMonthCultureStructureQuery->where('users.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $thisMonthCultureStructureQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $thisMonthCultureStructureQuery->where('users.officeId',$officeId);
      $thisMonthCultureStructureQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $thisMonthCultureStructureQuery->where('departments.departmentId',$departmentId);
    }
    $thisMonthCultureStructureCount = $thisMonthCultureStructureQuery->count();

    //This Month value
    $thisMonthCultureStructure = 0;
    if (!empty($thisMonthCultureStructureCount) && !empty($thisMonthStaffCount)) 
    {
      $thisMonthCultureStructure = (($thisMonthCultureStructureCount)/($thisMonthStaffCount));
      // $thisMonthCultureStructure = (($thisMonthCultureStructureCount)/($thisMonthStaffCount))*100;
    }

    $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    $createdDateStaff = $staff->created_at;

    //All previous month data till last month
    $allPreviousMonthCultureStructureQuery = DB::table('sot_answers')
        ->leftjoin('users','users.id','sot_answers.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('sot_answers.status','Active')
        ->where('sot_answers.created_at','>=',$createdDateStaff)
        ->whereDate('sot_answers.created_at','<',date('Y-m'."-1"));
    if ($includeWeekend != 1) {
      //Get weekends
      $currentDate    = date('Y-m'."-1"); 
      $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($createdDateStaff, $currentDate);
      $allPreviousMonthCultureStructureQuery->whereNotIn(DB::raw("(DATE_FORMAT(sot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }    
    if(!empty($orgId)){
      $allPreviousMonthCultureStructureQuery->where('users.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $allPreviousMonthCultureStructureQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $allPreviousMonthCultureStructureQuery->where('users.officeId',$officeId);
      $allPreviousMonthCultureStructureQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $allPreviousMonthCultureStructureQuery->where('departments.departmentId',$departmentId);
    }
    $allPreviousMonthCultureStructureCount = $allPreviousMonthCultureStructureQuery->count();

    //all previous Month value
    $allPreviousMonthCultureStructure = 0;
    if (!empty($allPreviousMonthCultureStructureCount) && !empty($lastMonthStaffCount)) 
    {
      $allPreviousMonthCultureStructure = (($allPreviousMonthCultureStructureCount)/($lastMonthStaffCount));
      // $allPreviousMonthCultureStructure = (($allPreviousMonthCultureStructureCount)/($lastMonthStaffCount))*100;
    }
  
    $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));
   
    $cultureStructureCount = array();
    $cultureStructureCount['MM'] = 0;
    $cultureStructureCount['MA'] = 0;
    // $cultureStructureCountMA = 0;
    
    if (!empty($lastMonthCultureStructure)) {
        $cultureStructureCount['MM'] = round(((-1*($lastMonthCultureStructure - $thisMonthCultureStructure))/$lastMonthCultureStructure),2);
    }
  
    if (!empty($allPreviousMonthCultureStructure) && !empty($noOfPreviousMonths)) 
    {
        $cultureStructureCountMA = ($allPreviousMonthCultureStructure)/($noOfPreviousMonths);
        if (!empty($cultureStructureCountMA)) 
        {
            $cultureStructureCount['MA'] = round(((-1*($cultureStructureCountMA - $thisMonthCultureStructure))/$cultureStructureCountMA),2);
        }
    }
    return $cultureStructureCount;
  }

  public function getTribeometer($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $lastMonth = date("Y-m", strtotime("-1 month"));

    $userPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId);
    $users = $this->userCount($userPer);
    $userCount = count($users);

    $query = DB::table('tribeometer_answers')
          ->select('tribeometer_answers.userId')
          ->distinct()      
          ->leftJoin('users','users.id','tribeometer_answers.userId')
          ->leftJoin('departments','departments.id','users.departmentId')
          ->where('departments.status','Active')
          ->where('tribeometer_answers.status','Active')
          ->where('users.status','Active')
          ->where('tribeometer_answers.updated_at','LIKE',$lastMonth);
    if ($includeWeekend != 1) {
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($lastMonth,2);
      $query->whereNotIn(DB::raw("(DATE_FORMAT(tribeometer_answers.updated_at,'%Y-%m-%d'))"),$getWeekendDates);
    } 
    if($orgId){
      $query->where('tribeometer_answers.orgId',$orgId);
    }
    if (!empty($officeId) && empty($departmentId)) {
      $query->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
      $query->where('users.officeId',$officeId);
      $query->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
      $query->where('departments.departmentId',$departmentId);
    }  
    $tribeometerUpdateStatus = $query->get();

    $tribeometerUpdateCount = count($tribeometerUpdateStatus);

    $result = "0";
    if (!empty($tribeometerUpdateCount) && !empty($userCount))
    {
        $result = round(($tribeometerUpdateCount/$userCount)*100,2);
    }
    return $result;
  }

  public function getTribeometerMAMMCount($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $MMArr = array('orgId'=>$orgId,'officeId'=>$officeId,'departmentId'=>$departmentId);
    $lastMonthStaffCount = $this->lastMonthStaffCount($MMArr);
    $lastMonth = date("Y-m", strtotime("-1 month"));

    //Last month query
    $lastMonthTribeometerQuery = DB::table('tribeometer_answers')
        ->leftjoin('users','users.id','tribeometer_answers.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('tribeometer_answers.status','Active')
        ->where('tribeometer_answers.created_at','LIKE',$lastMonth."%");
    if ($includeWeekend != 1) {
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($lastMonth,2);
      $lastMonthTribeometerQuery->whereNotIn(DB::raw("(DATE_FORMAT(tribeometer_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }    
    if(!empty($orgId)){
      $lastMonthTribeometerQuery->where('tribeometer_answers.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $lastMonthTribeometerQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $lastMonthTribeometerQuery->where('users.officeId',$officeId);
      $lastMonthTribeometerQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $lastMonthTribeometerQuery->where('departments.departmentId',$departmentId);
    }
    $lastMonthTribeometerCount = $lastMonthTribeometerQuery->count();

    //Last Month value
    $lastMonthTribeometer = 0;
    if (!empty($lastMonthTribeometerCount) && !empty($lastMonthStaffCount)) 
    {
      $lastMonthTribeometer = (($lastMonthTribeometerCount)/($lastMonthStaffCount));
      // $lastMonthTribeometer = (($lastMonthTribeometerCount)/($lastMonthStaffCount))*100;
    }

    $thisMonthStaffCount = $this->thisMonthStaffCount($MMArr);
    $thisMonth = date("Y-m");

    //This month query
    $thisMonthTribeometerQuery = DB::table('tribeometer_answers')
        ->leftjoin('users','users.id','tribeometer_answers.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('tribeometer_answers.status','Active')
        ->where('tribeometer_answers.created_at','LIKE',$thisMonth."%");
    if ($includeWeekend != 1) {
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($thisMonth,2);
      $thisMonthTribeometerQuery->whereNotIn(DB::raw("(DATE_FORMAT(tribeometer_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }    
    if(!empty($orgId)){
      $thisMonthTribeometerQuery->where('tribeometer_answers.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $thisMonthTribeometerQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $thisMonthTribeometerQuery->where('users.officeId',$officeId);
      $thisMonthTribeometerQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $thisMonthTribeometerQuery->where('departments.departmentId',$departmentId);
    }
    $thisMonthTribeometerCount = $thisMonthTribeometerQuery->count();

    //This Month value
    $thisMonthTribeometer = 0;
    if (!empty($thisMonthTribeometerCount) && !empty($thisMonthStaffCount)) 
    {
      $thisMonthTribeometer = (($thisMonthTribeometerCount)/($thisMonthStaffCount));
      // $thisMonthTribeometer = (($thisMonthTribeometerCount)/($thisMonthStaffCount))*100;
    }

    $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    $createdDateStaff = $staff->created_at;

    //All previous month data till last month
    $allPreviousMonthTribeometerQuery = DB::table('tribeometer_answers')
        ->leftjoin('users','users.id','tribeometer_answers.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('tribeometer_answers.status','Active')
        ->where('tribeometer_answers.created_at','>=',$createdDateStaff)
        ->whereDate('tribeometer_answers.created_at','<',date('Y-m'."-1"));
    if ($includeWeekend != 1) {
      //Get weekends
      $currentDate    = date('Y-m'."-1"); 
      $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($createdDateStaff, $currentDate);
      $allPreviousMonthTribeometerQuery->whereNotIn(DB::raw("(DATE_FORMAT(tribeometer_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }    
    if(!empty($orgId)){
      $allPreviousMonthTribeometerQuery->where('tribeometer_answers.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $allPreviousMonthTribeometerQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $allPreviousMonthTribeometerQuery->where('users.officeId',$officeId);
      $allPreviousMonthTribeometerQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $allPreviousMonthTribeometerQuery->where('departments.departmentId',$departmentId);
    }
    $allPreviousMonthTribeometerCount = $allPreviousMonthTribeometerQuery->count();

    //all previous Month value
    $allPreviousMonthTribeometer = 0;
    if (!empty($allPreviousMonthTribeometerCount) && !empty($lastMonthStaffCount)) 
    {
      $allPreviousMonthTribeometer = (($allPreviousMonthTribeometerCount)/($lastMonthStaffCount));
      // $allPreviousMonthTribeometer = (($allPreviousMonthTribeometerCount)/($lastMonthStaffCount))*100;
    }
 
    $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));
  
    $tribeometerCount = array();
    $tribeometerCount['MM'] = 0;
    $tribeometerCount['MA'] = 0;
    // $tribeometerCountMA = 0;
    
    if (!empty($lastMonthTribeometer)) {
        $tribeometerCount['MM'] = round(((-1*($lastMonthTribeometer - $thisMonthTribeometer))/$lastMonthTribeometer),2);
    }
    
    if (!empty($allPreviousMonthTribeometer) && !empty($noOfPreviousMonths)) 
    {
        $tribeometerCountMA = ($allPreviousMonthTribeometer)/($noOfPreviousMonths);
        if (!empty($tribeometerCountMA)) 
        {
            $tribeometerCount['MA'] = round(((-1*($tribeometerCountMA - $thisMonthTribeometer))/$tribeometerCountMA),2);
        }
    }
    return $tribeometerCount;
  }

  public function getDiagnostics($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $lastMonth = date("Y-m", strtotime("-1 month"));

    $userPer = array('orgId'=>$orgId, 'officeId'=>$officeId,'departmentId'=>$departmentId);
    $users = $this->userCount($userPer);
    $userCount = count($users);

    $query = DB::table('diagnostic_answers')
        ->select('diagnostic_answers.userId')
        ->distinct()      
        ->leftJoin('users','users.id','diagnostic_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->where('departments.status','Active')
        ->where('diagnostic_answers.status','Active')
        ->where('users.status','Active')
        ->where('diagnostic_answers.updated_at','LIKE',$lastMonth);
    if ($includeWeekend != 1) {
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($lastMonth,2);
      $query->whereNotIn(DB::raw("(DATE_FORMAT(diagnostic_answers.updated_at,'%Y-%m-%d'))"),$getWeekendDates);
    } 
    if($orgId){
      $query->where('diagnostic_answers.orgId',$orgId);
    }
    if (!empty($officeId) && empty($departmentId)) {
      $query->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
      $query->where('users.officeId',$officeId);
      $query->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
      $query->where('departments.departmentId',$departmentId);
    }  
    $diagnosticUpdateStatus = $query->get();

    $diagnosticUpdateCount = count($diagnosticUpdateStatus);

    $result = "0";
    if (!empty($diagnosticUpdateCount) && !empty($userCount))
    {
        $result = round(($diagnosticUpdateCount/$userCount)*100,2);
    }
    return $result;
  }

  public function getDiagnosticMAMMCount($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];
    $includeWeekend   = $perArr["includeWeekend"];

    $MMArr = array('orgId'=>$orgId,'officeId'=>$officeId,'departmentId'=>$departmentId);
    $lastMonthStaffCount = $this->lastMonthStaffCount($MMArr);
    $lastMonth = date("Y-m", strtotime("-1 month"));

    //Last month query
    $lastMonthDiagnosticQuery = DB::table('diagnostic_answers')
        ->leftjoin('users','users.id','diagnostic_answers.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('diagnostic_answers.status','Active')
        ->where('diagnostic_answers.created_at','LIKE',$lastMonth."%");
    if ($includeWeekend != 1) {
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($lastMonth,2);
      $lastMonthDiagnosticQuery->whereNotIn(DB::raw("(DATE_FORMAT(diagnostic_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }    
    if(!empty($orgId)){
      $lastMonthDiagnosticQuery->where('diagnostic_answers.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $lastMonthDiagnosticQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $lastMonthDiagnosticQuery->where('users.officeId',$officeId);
      $lastMonthDiagnosticQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $lastMonthDiagnosticQuery->where('departments.departmentId',$departmentId);
    }
    $lastMonthDiagnosticCount = $lastMonthDiagnosticQuery->count();

    //Last Month value
    $lastMonthDiagnostic = 0;
    if (!empty($lastMonthDiagnosticCount) && !empty($lastMonthStaffCount)) 
    {
      $lastMonthDiagnostic = (($lastMonthDiagnosticCount)/($lastMonthStaffCount));
      // $lastMonthDiagnostic = (($lastMonthDiagnosticCount)/($lastMonthStaffCount))*100;
    }

    $thisMonthStaffCount = $this->thisMonthStaffCount($MMArr);
    $thisMonth = date("Y-m");

    //This month query
    $thisMonthDiagnosticQuery = DB::table('diagnostic_answers')
        ->leftjoin('users','users.id','diagnostic_answers.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('diagnostic_answers.status','Active')
        ->where('diagnostic_answers.created_at','LIKE',$thisMonth."%");
    if ($includeWeekend != 1) {
      //Get weekends
      $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($thisMonth,2);
      $thisMonthDiagnosticQuery->whereNotIn(DB::raw("(DATE_FORMAT(diagnostic_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }    
    if(!empty($orgId)){
      $thisMonthDiagnosticQuery->where('diagnostic_answers.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $thisMonthDiagnosticQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $thisMonthDiagnosticQuery->where('users.officeId',$officeId);
      $thisMonthDiagnosticQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $thisMonthDiagnosticQuery->where('departments.departmentId',$departmentId);
    }
    $thisMonthDiagnosticCount = $thisMonthDiagnosticQuery->count();

    //This Month value
    $thisMonthDiagnostic = 0;
    if (!empty($thisMonthDiagnosticCount) && !empty($thisMonthStaffCount)) 
    {
      $thisMonthDiagnostic = (($thisMonthDiagnosticCount)/($thisMonthStaffCount));
      // $thisMonthDiagnostic = (($thisMonthDiagnosticCount)/($thisMonthStaffCount))*100;
    }

    $staff = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    $createdDateStaff = $staff->created_at;

    //All previous month data till last month
    $allPreviousMonthDiagnosticQuery = DB::table('diagnostic_answers')
        ->leftjoin('users','users.id','diagnostic_answers.userId')
        ->leftjoin('departments','users.departmentId','departments.id')
        ->where('departments.status','Active')
        ->where('users.status','Active')
        ->where('diagnostic_answers.status','Active')
        ->where('diagnostic_answers.created_at','>=',$createdDateStaff)
        ->whereDate('diagnostic_answers.created_at','<',date('Y-m'."-1"));
    if ($includeWeekend != 1) {
      //Get weekends
      $currentDate    = date('Y-m'."-1"); 
      $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($createdDateStaff, $currentDate);
      $allPreviousMonthDiagnosticQuery->whereNotIn(DB::raw("(DATE_FORMAT(diagnostic_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }    
    if(!empty($orgId)){
      $allPreviousMonthDiagnosticQuery->where('diagnostic_answers.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $allPreviousMonthDiagnosticQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $allPreviousMonthDiagnosticQuery->where('users.officeId',$officeId);
      $allPreviousMonthDiagnosticQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $allPreviousMonthDiagnosticQuery->where('departments.departmentId',$departmentId);
    }
    $allPreviousMonthDiagnosticCount = $allPreviousMonthDiagnosticQuery->count();

    //all previous Month value
    $allPreviousMonthDiagnostic = 0;
    if (!empty($allPreviousMonthDiagnosticCount) && !empty($lastMonthStaffCount)) 
    {
      $allPreviousMonthDiagnostic = (($allPreviousMonthDiagnosticCount)/($lastMonthStaffCount));
      // $allPreviousMonthDiagnostic = (($allPreviousMonthDiagnosticCount)/($lastMonthStaffCount))*100;
    }
 
    $noOfPreviousMonths = count($this->countNumberOfMonths($orgId));
  
    $diagnosticCount = array();
    $diagnosticCount['MM'] = 0;
    $diagnosticCount['MA'] = 0;
    // $diagnosticCountMA = 0;
    
    if (!empty($lastMonthDiagnostic)) {

        $diagnosticCount['MM'] = round(((-1*($lastMonthDiagnostic - $thisMonthDiagnostic))/$lastMonthDiagnostic),2);
    }
   
    if (!empty($allPreviousMonthDiagnostic) && !empty($noOfPreviousMonths)) 
    {
        $diagnosticCountMA = ($allPreviousMonthDiagnostic)/($noOfPreviousMonths);
        if (!empty($diagnosticCountMA)) 
        {
            $diagnosticCount['MA'] = round(((-1*($diagnosticCountMA - $thisMonthDiagnostic))/$diagnosticCountMA),2);
        }
    }
    return $diagnosticCount;
  }

  public function lastMonthStaffCount($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];

    $lastMonthStaffQuery = DB::table('users')
      ->where('users.status','Active')
      ->where('departments.status','Active')
      ->leftjoin('departments','users.departmentId','departments.id')
      ->whereDate('users.created_at','<',date('Y-m'."-1"))
      ->where('users.roleId',3);
    if (!empty($orgId)) {
      $lastMonthStaffQuery->where('users.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $lastMonthStaffQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $lastMonthStaffQuery->where('users.officeId',$officeId);
      $lastMonthStaffQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $lastMonthStaffQuery->where('departments.departmentId',$departmentId);
    }
    $lastMonthStaffCount = $lastMonthStaffQuery->count();
    return $lastMonthStaffCount;
  }
  public function thisMonthStaffCount($perArr=array())
  {
    $orgId          = $perArr["orgId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];

    $thisMonthStaffQuery = DB::table('users')
      ->leftjoin('departments','users.departmentId','departments.id')
      ->where('users.status','Active')
      ->where('departments.status','Active')
      ->where('users.roleId',3);
    if (!empty($orgId)) {
      $thisMonthStaffQuery->where('users.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
      $thisMonthStaffQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
      $thisMonthStaffQuery->where('users.officeId',$officeId);
      $thisMonthStaffQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
      $thisMonthStaffQuery->where('departments.departmentId',$departmentId);
    }
    $thisMonthStaffCount = $thisMonthStaffQuery->count();
    return $thisMonthStaffCount;
  }

  public function countNumberOfMonths($orgId)
  {
    $firstStaffOfOrg = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
          
    $firstDateOfOrg = date('Y-m',strtotime($firstStaffOfOrg->created_at));
    $lastMonthDate = date('Y-m',strtotime('-1 month'));

    $time1 = strtotime($firstDateOfOrg);
    $time2 = strtotime($lastMonthDate);
    if(date('m', $time1)==date('m', $time2) && date('Y', $time1)==date('Y', $time2)){
      //$noOfPreviousMonths = 1;
      return 1;
    }
    else{
      if ($time1 < $time2) 
      {
        $my = date('mY', $time2);
        $noOfPreviousMonths = array(date('F', $time1));
        while($time1 < $time2) {
          $time1 = strtotime(date('Y-m-d', $time1).' +1 month');
          if(date('mY', $time1) != $my && ($time1 < $time2))
            $noOfPreviousMonths[] = date('F', $time1);
        }
        $noOfPreviousMonths[] = date('F', $time2);
        return $noOfPreviousMonths;
      }
      else
      {
        //$noOfPreviousMonths = 0;
        return 0;
      } 
    }
  }

  public function kudosLeaderboard(Request $request) {

    $a = app('App\Http\Controllers\Admin\CommonController')->checkUser(Auth::user());

    if (!empty($a)) {
      return redirect('/admin');
    }

    $orgId = $request->orgId;
    $officeId = $request->officeId;
    $departmentId = $request->departmentId;
    $year = $request->year;
    $month = $request->month;

    if (empty($year)) {
      $year = date('Y');
    }
    if (empty($month)) {
      $month = date('m');
    }

    $resultArray = array();
    $bubbleCountArray = array();

    $organisations = DB::table('organisations')->where('status','Active')->orderBy('organisation','ASC')->get();

    //By default 1st organisation selected
    $organisation = DB::table('organisations')->where('status','Active')->orderBy('organisation','ASC')->first();
    if (empty($orgId)) {
        $orgId = $organisation->id;   
    }

    $org = DB::table('organisations')->where('id',$orgId)->where('status','Active')->first();
    $includeWeekend = $org->include_weekend;

    $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get();

    $all_department = DB::table('all_department')->where('status','Active')->get();

    $departments = [];

    if (!empty($officeId)) {
      $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->where('all_department.status','Active') 
        ->orderBy('all_department.department','ASC')->get();
    }elseif (empty($officeId)) {
      $all_department = DB::table('departments')
        ->select('all_department.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.orgId',$orgId) 
        ->where('departments.status','Active') 
        ->where('all_department.status','Active') 
        ->orderBy('all_department.department','ASC')
        ->groupBy('all_department.department','all_department.id')->get();
    }

    $resultArray['users'] = DB::table('users')->where('orgId',$orgId)->get();

    $bubbleYearsQuery = DB::table('dot_bubble_rating_records')
      ->select(DB::raw('YEAR(dot_bubble_rating_records.created_at) year'))
      ->leftJoin('dots','dot_bubble_rating_records.dot_id','dots.id')
      ->rightJoin('users','dot_bubble_rating_records.to_bubble_user_id','users.id');
    if(!empty($orgId)){
      $bubbleYearsQuery->where('dots.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)) {
        $bubbleYearsQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
        $bubbleYearsQuery->where('users.officeId',$officeId);
        $bubbleYearsQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
        $bubbleYearsQuery->leftJoin('departments','departments.id','users.departmentId')
            ->where('departments.status','Active')
            ->where('departments.departmentId',$departmentId);
    }
    $bubbleYears = $bubbleYearsQuery->groupBy('year')->first();

    if (!empty($bubbleYears)) {
      $years = range(date('Y'), $bubbleYears->year); //range of year
      if (empty($year)) {
        $year = date('Y');
      }
      if ($year != date('Y')) {
        $monthName = [];
        $monthKey = [];
        for ($m=1; $m<=12; $m++) {
          $monthName[] = date('M', mktime(0,0,0,$m, 1, date('Y')));
          $monthKey[] = date('m', mktime(0,0,0,$m, 1, date('Y')));
        }
        $months = array_combine($monthKey, $monthName);
      }elseif ($years[0] == date('Y')) {
        $currentMonth = date('m');
        $monthName = [];
        $monthKey = [];
        for ($m=1; $m<=12; $m++) {
          $monthName[] = date('M', mktime(0,0,0,$m, 1, date('Y')));
          $monthKey[] = date('m', mktime(0,0,0,$m, 1, date('Y')));
          if (  date('m', mktime(0,0,0,$m, 1, date('Y'))) == $currentMonth ){
            break;
          }
        }
        $months = array_combine($monthKey, $monthName);
      }else{
        $monthName = [];
        $monthKey = [];
        for ($m=1; $m<=12; $m++) {
          $monthName[] = date('M', mktime(0,0,0,$m, 1, date('Y')));
          $monthKey[] = date('m', mktime(0,0,0,$m, 1, date('Y')));
        }
        $months = array_combine($monthKey, $monthName);
      }

    }else{
      $years = range(date('Y'),date('Y')); //range of years

      if ($years[0] == date('Y')) {
        $currentMonth = date('m');
        $monthName = [];
        $monthKey = [];
        for ($m=1; $m<=12; $m++) {
          $monthName[] = date('M', mktime(0,0,0,$m, 1, date('Y')));
          $monthKey[] = date('m', mktime(0,0,0,$m, 1, date('Y')));
          if (  date('m', mktime(0,0,0,$m, 1, date('Y'))) == $currentMonth ){
            break;
          }
        }
        $months = array_combine($monthKey, $monthName);
      }else{
        $monthName = [];
        $monthKey = [];
        for ($m=1; $m<=12; $m++) {
          $monthName[] = date('M', mktime(0,0,0,$m, 1, date('Y')));
          $monthKey[] = date('m', mktime(0,0,0,$m, 1, date('Y')));
        }
        $months = array_combine($monthKey, $monthName);
      }
    }

    if ($year."-".$month != date('Y-m')) {
      $kudosDataQuery = DB::table('kudos_leaderboard_graph')
        ->where('status','Active')
        ->where('date','LIKE',$year."-".$month."%");
      if(!empty($orgId)){
          $kudosDataQuery->where('orgId',$orgId);
      }
      if(empty($officeId) && empty($departmentId)){
        $kudosDataQuery->whereNull('officeId');
        $kudosDataQuery->whereNull('departmentId');
      }elseif(!empty($officeId) && empty($departmentId)){
        $kudosDataQuery->where('officeId',$officeId);
        $kudosDataQuery->whereNull('departmentId');
      }elseif(!empty($officeId) && !empty($departmentId)){
        $kudosDataQuery->where('officeId',$officeId);
        $kudosDataQuery->where('departmentId',$departmentId);
      }elseif(empty($officeId) && !empty($departmentId)){
        $kudosDataQuery->whereNull('officeId');
        $kudosDataQuery->where('departmentId',$departmentId);
      }
      // if($includeWeekend != 1){ //Exclude Weekend Days
      //   //Get weekends
      //   $YearMonth = $year."-".$month;
      //   // $YearMonth = $year."-".$key;
      //   $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($YearMonth,2);
      //   $kudosDataQuery->whereNotIn(DB::raw("(DATE_FORMAT(kudos_leaderboard_graph.date,'%Y-%m-%d'))"),$getWeekendDates);
      // }
      $kudosData = $kudosDataQuery->get();

      $kudosFinal = [];
      $kudosresult['userName']  = '';
      $kudosresult['userEmail'] = '';
      $kudosresult['userImage'] = '';
      $kudosresult['userId']    = '';
      $kudosresult['count']     = '';

      if (count($kudosData)) {
        foreach ($kudosData as $kudosVal) {
            $kudosresult['userName']  = $kudosVal->userName;
            $kudosresult['userEmail'] = $kudosVal->userEmail;
            $kudosresult['userImage'] = $kudosVal->userImage;
            $kudosresult['userId']    = $kudosVal->userId;
            if($includeWeekend != 1){ //Exclude Weekend Days
              $kudosresult['count']     = $kudosVal->kudos_count_without_weekend;
            }else{
              $kudosresult['count']     = $kudosVal->kudos_count_with_weekend;
            }
            array_push($kudosFinal, $kudosresult);
        }
      }else{
        array_push($kudosFinal, $kudosresult);
      }      
      // if (!empty($kudosData)) {
      //   $kudosFinal = $kudosData;
      // }
    }elseif($year."-".$month == date('Y-m')){
      if ($includeWeekend == 1) {
        $isExclude = 2; //include weekends
      }else{
        $isExclude = 1; //exclude weekend
      }
      $date = date('Y-m-d');
      $kudosArr = app('App\Http\Controllers\Admin\AdminReportController')->calculateKudosChampGraph(base64_encode($orgId),$officeId,$departmentId,$isExclude,$date);

      // echo "<pre>";print_r($kudosArr);die();

      $kudosFinal = [];
      if (!empty($kudosArr)) {
        $kudosFinal = $kudosArr;
      }
    }

    // echo "<pre>";print_r($kudosFinal);die();
    // $bubbleCountUserListQuery = DB::table('dot_bubble_rating_records')
    //   ->select('dot_bubble_rating_records.to_bubble_user_id as userId')
    //   ->leftJoin('dots','dot_bubble_rating_records.dot_id','dots.id')
    //   ->rightJoin('users','dot_bubble_rating_records.to_bubble_user_id','users.id')
    //   ->where('dot_bubble_rating_records.bubble_flag','1');
    // if (!empty($orgId)) {
    //   $bubbleCountUserListQuery->where('dots.orgId',$orgId);
    // }
    // if(!empty($officeId) && empty($departmentId)) {
    //     $bubbleCountUserListQuery->where('users.officeId',$officeId);
    // }elseif(!empty($officeId) && !empty($departmentId)) {
    //     $bubbleCountUserListQuery->where('users.officeId',$officeId);
    //     $bubbleCountUserListQuery->where('users.departmentId',$departmentId);
    // }elseif(empty($officeId) && !empty($departmentId)) {
    //     $bubbleCountUserListQuery->leftJoin('departments','departments.id','users.departmentId')
    //         ->where('departments.status','Active')
    //         ->where('departments.departmentId',$departmentId);
    // }
    // if (empty($year) && empty($month)) {
    //   $yearMonth = date('Y')."-".date('m');
    //   $bubbleCountUserListQuery->where('dot_bubble_rating_records.created_at','LIKE',$yearMonth."%");
    // }else if (!empty($year) && !empty($month)) {
    //   $yearMonth = $year."-".$month;
    //   $bubbleCountUserListQuery->where('dot_bubble_rating_records.created_at','LIKE',$yearMonth."%");
    // }
    // $bubbleCountUserList = $bubbleCountUserListQuery->groupBy('userId')->get();

    // foreach ($bubbleCountUserList as $user) {
      
    //   $topBubbleCountsQuery = DB::table('dot_bubble_rating_records')
    //     ->select('dot_bubble_rating_records.id')
    //     ->leftJoin('dots','dot_bubble_rating_records.dot_id','dots.id')
    //     ->rightJoin('users','dot_bubble_rating_records.to_bubble_user_id','users.id')
    //     ->where('dot_bubble_rating_records.to_bubble_user_id',$user->userId)
    //     ->where('dot_bubble_rating_records.bubble_flag','1');
    //   if (!empty($orgId)) {
    //     $topBubbleCountsQuery->where('dots.orgId',$orgId);
    //   }
    //   if(!empty($officeId) && empty($departmentId)) {
    //       $topBubbleCountsQuery->where('users.officeId',$officeId);
    //   }elseif(!empty($officeId) && !empty($departmentId)) {
    //       $topBubbleCountsQuery->where('users.officeId',$officeId);
    //       $topBubbleCountsQuery->where('users.departmentId',$departmentId);
    //   }elseif(empty($officeId) && !empty($departmentId)) {
    //       $topBubbleCountsQuery->leftJoin('departments','departments.id','users.departmentId')
    //           ->where('departments.status','Active')
    //           ->where('departments.departmentId',$departmentId);
    //   }
    //   if (empty($year) && empty($month)) {
    //     $yearMonth = date('Y')."-".date('m');
    //     $topBubbleCountsQuery->where('dot_bubble_rating_records.created_at','LIKE',$yearMonth."%");
    //   }else if (!empty($year) && !empty($month)) {
    //     $yearMonth = $year."-".$month;
    //     $topBubbleCountsQuery->where('dot_bubble_rating_records.created_at','LIKE',$yearMonth."%");
    //   }
    //   $topBubbleCounts = $topBubbleCountsQuery->count();

    //   $bubbleCount['userId'] = $user->userId;
    //   $bubbleCount['count'] = $topBubbleCounts;

    //   array_push($bubbleCountArray, $bubbleCount);
    // }
   
    // $kudosArray = array_chunk($this->array_sort($bubbleCountArray, 'count', SORT_DESC) , 10);

    // $kudosFinal = array();
    // if (!empty($kudosArray)) {
    //   foreach ($kudosArray[0] as $kudos) {
    //     $finalUsers = DB::table('users')->select('id','name','imageUrl')->where('id',$kudos['userId'])->first();
    //     $kudos['name'] = '';
    //     if (!empty($finalUsers)) {
    //       $kudos['name'] = $finalUsers->name;
    //       $kudos['imageUrl'] = $finalUsers->imageUrl;
    //     }
    //     array_push($kudosFinal,$kudos);
    //   }
    // }
   
    return view('admin/kudosLeaderboard/kudosLeaderboard',compact('organisations','orgId','offices','all_department','departments','departmentId','officeId','resultArray','years','months','kudosFinal','bubbleYears','year','month'));
  }

  function array_sort($array, $on, $order=SORT_ASC)
  {
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
            break;
            case SORT_DESC:
                arsort($sortable_array);
            break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
  }

  public function kudosNotification() {
    $organisation = DB::table('organisations')->select('id')->where('id',8)->where('status','Active')->get();

    foreach ($organisation as $org) {

      $bubbleCountUserListQuery = DB::table('dot_bubble_rating_records')
        ->select('dot_bubble_rating_records.to_bubble_user_id as userId')
        ->leftJoin('dots','dot_bubble_rating_records.dot_id','dots.id')
        ->rightJoin('users','dot_bubble_rating_records.to_bubble_user_id','users.id')
        ->where('dots.status','Active')
        ->where('users.status','Active')
        ->where('dot_bubble_rating_records.status','Active')
        ->where('dot_bubble_rating_records.bubble_flag','1');
      if (!empty($org->id)) {
        $bubbleCountUserListQuery->where('dots.orgId',$org->id);
      }
      $bubbleCountUserList = $bubbleCountUserListQuery->groupBy('userId')->get();

      $bubbleCountArray = array();
      foreach ($bubbleCountUserList as $user) {
      
        $topBubbleCountsQuery = DB::table('dot_bubble_rating_records')
          ->select('dot_bubble_rating_records.id')
          ->leftJoin('dots','dot_bubble_rating_records.dot_id','dots.id')
          ->rightJoin('users','dot_bubble_rating_records.to_bubble_user_id','users.id')
          ->where('dot_bubble_rating_records.to_bubble_user_id',$user->userId)
          ->where('dots.status','Active')
          ->where('users.status','Active')
          ->where('dot_bubble_rating_records.status','Active')
          ->where('dot_bubble_rating_records.bubble_flag','1');
        if (!empty($org->id)) {
          $topBubbleCountsQuery->where('dots.orgId',$org->id);
        }
        $topBubbleCounts = $topBubbleCountsQuery->count();

        $bubbleCount['userId'] = $user->userId;
        $bubbleCount['count'] = $topBubbleCounts;

        array_push($bubbleCountArray, $bubbleCount);
      }

      $kudosCountArray = $this->array_sort($bubbleCountArray, 'count', SORT_DESC);
      $finalKudosArray = array_values($kudosCountArray);

      $userId = [];
      $maxKudosCount = '';
      if (!empty($finalKudosArray)) {

        foreach ($finalKudosArray as $value) {
          if($finalKudosArray[0]['count'] == $value['count']){
            $userId[]         = $value['userId'];
            $maxKudosCount  = $value['count'];
          }
        }

        if (!empty($userId) && count($userId) >1 ) {
          $isMultiple = true;
        }else{
          $isMultiple = false;
        }

        $users = DB::table('users')->where('orgId', $org->id)->where('id','335')->where('status','Active')->get(); 

        if (!empty($users) && count($users)>0) {
          foreach ($users as $user) {
            $fcmToken = '';
            if(!empty($user)) {

              $max_height =  500;
              $max_width =  500;
              
              if (!empty($user->imageUrl)) {
                $image=url('public/uploads/user_profile/'.$user->imageUrl);
              }else{
                $image=url('public/images/no-user-kudos.jpg');
              }

              list($orig_width, $orig_height) = getimagesize($image);

              $width = $orig_width;
              $height = $orig_height;

              # taller
              if ($height > $max_height) {
                  $width = ($max_height / $height) * $width;
                  $height = $max_height;
              }

              # wider
              if ($width > $max_width) {
                  $height = ($max_width / $width) * $height;
                  $width = $max_width;
              }

              $image_p = imagecreatetruecolor($width, $height);
              $image = imagecreatefromjpeg($image);

              imagecopyresampled($image_p, $image, 0, 0, 0, 0,$width, $height, $orig_width, $orig_height);

              //header('Content-Type: image/jpeg');
              $datetime = time();
              imagepng($image_p, "public/uploads/user_profile/crop-".$datetime."-" .$user->imageUrl);

              $cropImage = url("public/uploads/user_profile/crop-".$datetime."-" .$user->imageUrl);

              $data  = array('userName'=>$user->name." ".$user->lastName ,'userImage'=>$cropImage);

              // $email = 'testuser@mailinator.com';
              $email = $user->email;
              Mail::send('layouts/kudosChampion', $data, function($message) use($email) 
              {
                $message->from('shweta@chetaru.com', 'Tribe365');
                $message->to($email);
                $message->subject('Kudos Champion');
              });

              $fcmToken = $user->fcmToken;

              $userImageUrl = '';
              $userName = '';
              $userEmail = '';
              $userIds = '';
              
              if (!empty($userId)) {
                $userImageUrlArr = [];
                $userNameArr = [];
                $userEmailArr = [];
                $userIdArr = [];
                foreach ($userId as $userVal) {
                  $finalUser = DB::table('users')->where('id',$userVal)->where('status','Active')->where('users.roleId',3)->first();
                  if (!empty($finalUser)) {
                    if (!empty($finalUser->imageUrl)) {
                      $userImageUrlArr[] = url('public/uploads/user_profile/'.$finalUser->imageUrl);
                    }else{
                      $userImageUrlArr[] = url('public/images/no-user-kudos.jpg');
                    }
                    $userNameArr[] = $finalUser->name;
                    $userEmailArr[] = $finalUser->email;
                    $userIdArr[] = $finalUser->id;
                  }
                }
                $userImageUrl = implode(',', $userImageUrlArr);
                $userName     = implode(',', $userNameArr);
                $userEmail    = implode(',', $userEmailArr);
                $userIds      = implode(',', $userIdArr);
              }

              $title    = 'Kudos Champion';
              $message  = '';

              $fields = array(
                'to' => $fcmToken,
                'priority' => "high",
                'notification' => array("title" => $title, "body" => $message, "sound" => "default", 'kudosCount'=>$maxKudosCount,'userImage'=>$userImageUrl,'userName'=>$userName,'userEmail'=>$userEmail,'isMultiple'=>$isMultiple),
                'data'=> array("title" => $title, "body" => $message, "sound" => "default", 'kudosCount'=>$maxKudosCount,'userImage'=>$userImageUrl,'userName'=>$userName,'userEmail'=>$userEmail,'isMultiple'=>$isMultiple) 
              );

              $headers = array(
                'https://fcm.googleapis.com/fcm/send',
                'Content-Type: application/json',
                'Authorization: key=AAAASSh9qJM:APA91bGFC09RB3dFkD9u7L-R-hNMA7l4MWinuhfI0HspKKrNcXMdpVyQ-UKEqk71_Je91dEOb4y80Y96siJ6Z5v5sTR0Iq-yUh779ZEOqGDar71tbts9oCw4xggscbBoR0D_jOSPF_xQ'
              );

              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

              $result = curl_exec($ch);
              if($result === FALSE) {
                die('Problem occurred: ' . curl_error($ch));
              }
              curl_close($ch);
              $msg = json_decode($result);

              $notificationArray = array(
                'to_bubble_user_id'  => $user->id,
                'champId'            => trim($userIds),
                'from_bubble_user_id'=> 1,
                'title' => $title,
                'description' => $maxKudosCount,  
                'notificationType' => 'kudoschamp',
                'created_at' => date('Y-m-d H:i:s')
              );

              DB::table('iot_notifications')->insertGetId($notificationArray);
            }
          }
        }
      }
    }
  }

  public function getKudosCount($kudosArray=array()) {

    $userId       = $kudosArray['userId'];
    $orgId        = $kudosArray['orgId'];
    // $officeId     = $kudosArray['officeId'];
    // $departmentId = $kudosArray['departmentId'];
    $dotId        = $kudosArray['dotId'];
    
    $topBubbleCountsQuery = DB::table('dot_bubble_rating_records')
        ->select('dot_bubble_rating_records.id')
        ->leftJoin('dots','dot_bubble_rating_records.dot_id','dots.id')
        ->rightJoin('users','dot_bubble_rating_records.to_bubble_user_id','users.id')
        ->where('dot_bubble_rating_records.to_bubble_user_id',$userId)
        ->where('dot_bubble_rating_records.dot_id',$dotId)
        ->where('users.status','Active')
        ->where('dot_bubble_rating_records.status','Active')
        ->where('dots.status','Active')
        ->where('dot_bubble_rating_records.bubble_flag','1');
      if (!empty($orgId)) {
        $topBubbleCountsQuery->where('dots.orgId',$orgId);
      }
      // if(!empty($officeId) && empty($departmentId)) {
      //     $topBubbleCountsQuery->where('users.officeId',$officeId);
      // }elseif(!empty($officeId) && !empty($departmentId)) {
      //     $topBubbleCountsQuery->where('users.officeId',$officeId);
      //     $topBubbleCountsQuery->where('users.departmentId',$departmentId);
      // }elseif(empty($officeId) && !empty($departmentId)) {
      //     $topBubbleCountsQuery->leftJoin('departments','departments.id','users.departmentId')
      //         ->where('departments.status','Active')
      //         ->where('departments.departmentId',$departmentId);
      // }
    $topBubbleCounts = $topBubbleCountsQuery->count();

    return $topBubbleCounts;
  }

  public function sendNotificationManual()
  {
    $user = DB::table('users')->select('fcmToken')->where('email','shweta@chetaru.com')->first();
 
    $fields = array(
      'to' => $user->fcmToken,
      'priority' => "high",
      'notification' => array("title" => 'Feedback', "body" => "Hows things at work today?", "badge" => 0, "sound" => "default","feedbackId"=>''),
      'data'=> array("title" => 'test notification', "body" => "Hows things at work today?", "badge" => 0, "sound" => "default","feedbackId"=>'') 
    );

  $headers = array(
    'https://fcm.googleapis.com/fcm/send',
    'Content-Type: application/json',
    'Authorization: key=AAAASSh9qJM:APA91bGFC09RB3dFkD9u7L-R-hNMA7l4MWinuhfI0HspKKrNcXMdpVyQ-UKEqk71_Je91dEOb4y80Y96siJ6Z5v5sTR0Iq-yUh779ZEOqGDar71tbts9oCw4xggscbBoR0D_jOSPF_xQ'
  );

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

  $result = curl_exec($ch);
  if($result === FALSE)
  {
    die('Problem occurred: ' . curl_error($ch));
  }

  curl_close($ch);
  $msg = json_decode($result);    
  //return $msg;
}


  // public function index()
  // {
  //   $orgId             = Auth::guard('web')->User()->orgId;

  //   $organisation      = DB::table('organisations')->where('status','Active')->count();

  //   $user_count        = DB::table('users')->where('status','Active')->where('roleId',3)->count();

  //   $dot_count         = DB::table('dots')->count();

  //   $departmentCount   = DB::table('all_department')->where('status','Active')->count();

  //   // $iotFeedbackCount  = DB::table('iot_feedbacks')->where('status','Active')->count();
    
  //   $iotNewFeedbackQuery = DB::table('iot_feedbacks')
  //   ->leftJoin('iot_messages','iot_messages.feedbackId','iot_feedbacks.id')
  //   ->leftJoin('users','users.id','iot_feedbacks.userId')
  //   ->where('iot_feedbacks.status','!=','Completed')
  //   ->whereNull('iot_messages.feedbackId');

  //   $iotFeedbackCount = $iotNewFeedbackQuery->count();

  //   $data = array(
  //     'organisation'=> $organisation,
  //     'user_count'=> $user_count,
  //     'dot_count'=> $dot_count,
  //     'departmentCount'=> $departmentCount,
  //     'iotFeedbackCount'=> $iotFeedbackCount,
  //   );
  //   return view('admin/adminDashboard',compact('data'));
  // }
}
