<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Validator;
use Hash;
use Illuminate\Support\Facades\Input;
use Config;
use View;

class AdminActionController extends Controller
{

 public function __construct(Request $request)
 {

  $a = app('App\Http\Controllers\Admin\CommonController')->checkUser(Auth::user());

  if (!empty($a)) {
    return redirect('/admin');
  }
  
  $orgid = base64_decode($request->segment(3));

  $organisations = DB::table('organisations')
  ->select('ImageURL')
  ->where('id',$orgid)
  ->first();
  

  if(!empty($organisations->ImageURL))
  {
    $org_detail = base64_encode($orgid)."-".$organisations->ImageURL;

    View::share('org_detail', $org_detail);
  }
  else
  {
    $org_detail = "";

    View::share('org_detail', $org_detail);

  }


}


public function addAction($id)
{

  if(empty($id)){

    return view('admin/admin-organisation');
  }

  $actionResponsibles = DB::table('actionResponsibleStatus')->where('status','Active')->get();

  $resUsers = DB::table('users')
    ->where('status','Active')
    ->where('roleId',3)
    ->where('orgId',base64_decode($id))
    ->get();

  $themeList = DB::table('iot_themes')->where('status','Active')->where('orgId',base64_decode($id))->orderBy('id','DESC')->get();

  $feedbackList = DB::table('iot_feedbacks')->where('status','Active')->where('orgId',base64_decode($id))->orderBy('id','DESC')->get();

  return view('admin/actions/addAction',compact('themeList','actionResponsibles','id','resUsers','feedbackList'));
}

/*get DOT action list*/
// public function getActionList($id)
public function getActionList($id,$index=false,$feedThemeId=false)
{
  $a = app('App\Http\Controllers\Admin\CommonController')->checkUser(Auth::user());

  if (!empty($a)) {
    return redirect('/admin');
  }
  
  $feedThemeId = base64_decode($feedThemeId);

  if(empty($id)) {
    return view('admin/admin-organisation');
  }

  $orgId = base64_decode($id);

  $actionTblQuery = DB::table('actions')
  ->select('actions.themeId','actions.id','actions.userId','actions.description','actions.startedDate','actions.dueDate','actions.responsibleId','actions.orgStatus','actions.orgId','cuser.name AS cname','cuser.lastName AS clastName','offices.office','all_department.department','ruser.name AS rname','ruser.lastName AS rlastName','actionResponsibleStatus.name AS tier','actions.feedbackId')
  ->leftJoin('users AS cuser','cuser.id', '=', 'actions.userId')   
  ->leftJoin('users AS ruser','ruser.id', '=', 'actions.responsibleUserId')   
  ->leftJoin('offices','offices.id', '=', 'actions.officeId')         
  ->leftJoin('departments','departments.id', '=', 'actions.departmentId') 
  ->leftJoin('all_department','all_department.id', '=', 'departments.departmentId')   
  ->leftJoin('actionResponsibleStatus','actionResponsibleStatus.id', '=', 'actions.responsibleId')   
  ->where('actions.orgId', $orgId)
  ->where('actions.status', 'Active');
  if (!empty($feedThemeId) && $index==1) {
    $actionTblQuery->whereRaw("find_in_set(".$feedThemeId.",actions.themeId)"); 
  }elseif (!empty($feedThemeId) && $index==2) {
    $actionTblQuery->whereRaw("find_in_set(".$feedThemeId.",actions.feedbackId)"); 
  }
  $actionTbl = $actionTblQuery->orderBy('actions.id','DESC')->get();  

  $actions = array();
  foreach ($actionTbl as $value)
  {
    $obj = new Controller();

    $obj->id            = $value->id;
    $obj->userId        = $value->userId;
    $obj->description   = $value->description;
    $obj->startedDate   = $value->startedDate;
    $obj->dueDate       = $value->dueDate;
    $obj->responsibleId = $value->responsibleId;
    $obj->orgStatus     = $value->orgStatus;
    $obj->orgId         = $value->orgId;
    $obj->cname         = $value->cname;
    $obj->clastName     = $value->clastName;
    $obj->office        = $value->office;
    $obj->department    = $value->department;
    $obj->rname         = $value->rname;
    $obj->rlastName     = $value->rlastName;
    $obj->tier          = $value->tier;

    $allocatedThemearr = explode(',', $value->themeId);
    // $allocatedThemearr = json_decode($value->themeId);

    $themeTitleArr = array();
    if ($allocatedThemearr)
    {

      foreach ($allocatedThemearr as $themeId)
      {

        $theme = DB::table('iot_themes')->select('title')->where('status','Active')->where('id',$themeId)->first();

        if($theme)
        {
          array_push($themeTitleArr, $theme->title);
        }

      }

    }
    
    $obj->themeTitle  = count($themeTitleArr);
    // $obj->themeTitle  = implode(', ', $themeTitleArr);

    $allocatedFeedbackArr = explode(',', $value->feedbackId);
    $feedbackTitleArr = array();
    if ($allocatedFeedbackArr) {
      foreach ($allocatedFeedbackArr as $feedbackId) {
        $feedback = DB::table('iot_feedbacks')->select('message')->where('status','Active')->where('id',$feedbackId)->first();

        if($feedback) {
          array_push($feedbackTitleArr, $feedback->message);
        }
      }
    }

    $obj->feedbackTitle  = count($feedbackTitleArr);

    array_push($actions, $obj);
  }

  return view('admin/actions/actionList',compact('id','actions'));

}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $orgId = Input::get('orgId');
      $user = Auth::user();

      $feedbackIds = Input::get('feedbackId');

      $insertArray['startedDate']    = date('Y-m-d H:i:s', strtotime(Input::get('start_date')));
      $insertArray['dueDate']        = date('Y-m-d H:i:s', strtotime(Input::get('end_date')));
      $insertArray['description']    = Input::get('description');
      $insertArray['responsibleId']  = Input::get('responiblityValue');
      $insertArray['orgId']          = base64_decode($orgId);
      $insertArray['orgStatus']      = Input::get('status');
      $insertArray['status']         = 'Active';    
      $insertArray['created_at']     = date('Y-m-d H:i:s');
      $insertArray['userId']         = $user->id;

      $insertArray['forUserId']      = Input::get('userId');
      $insertArray['officeId']       = Input::get('officeId');   
      $insertArray['departmentId']   = Input::get('departmentId');
      $insertArray['responsibleUserId'] = Input::get('resUsers');
      if (!empty(Input::get('themeId'))) {
        $insertArray['themeId']           = implode(',', Input::get('themeId'));
      }
      if (!empty($feedbackIds)) {
        $insertArray['feedbackId']           = implode(',', $feedbackIds);
      }
      // $insertArray['themeId']           = json_encode(Input::get('themeId'));

      $actionId = DB::table('actions')->insertGetId($insertArray);

      if (!empty($feedbackIds)) {
        foreach ($feedbackIds as $feedId) {
          $feedData = DB::table('iot_feedbacks')->select('actionTaken')->where('id',$feedId)->first();

          if (!empty($feedData)) {
            if (!empty($feedData->actionTaken)) {
              $feedActions = $feedData->actionTaken.",".$actionId;

              DB::table('iot_feedbacks')->where('id',$feedId)->update([
                'actionTaken' => $feedActions,
                'updated_at'  => date('Y-m-d H:i:s')
              ]);
            }else{
              $feedActions = $actionId;

              DB::table('iot_feedbacks')->where('id',$feedId)->update([
                'actionTaken' => $feedActions,
                'updated_at'  => date('Y-m-d H:i:s')
              ]);
            }
          }
        }
      }

      $actionName = DB::table('actionResponsibleStatus')
      ->where('id',$insertArray['responsibleId'])
      ->first();

      return redirect("admin/action-list/$orgId")->with('message', $actionName->name.' - Action added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($actid)
    {
      $actionId = base64_decode($actid);

      $actions = DB::table('actions')->where('id',$actionId)->first();

      $id = "";
      $selectedThemeArr = array();
      $selectedFeedbackArr = array();
      if($actions)
      {
        $id = base64_encode($actions->orgId);
        if( !empty($actions->themeId))
        {
          $selectedThemeArr = explode(',', $actions->themeId);
        }

        if( !empty($actions->feedbackId))
        {
          $selectedFeedbackArr = explode(',', $actions->feedbackId);
        }
     }

     $actionResponsibles = DB::table('actionResponsibleStatus')->where('status','Active')->get();

     $resUsers = DB::table('users')->where('roleId',3)->where('status','Active')->where('orgId',base64_decode($id))->get();

     $themeList = DB::table('iot_themes')->where('status','Active')->where('orgId',base64_decode($id))->orderBy('id','DESC')->get();

     $feedbackList = DB::table('iot_feedbacks')->where('status','Active')->where('orgId',base64_decode($id))->orderBy('id','DESC')->get();

     return view('admin/actions/editAction',compact('themeList','actionResponsibles','id','resUsers','actions','selectedThemeArr','feedbackList','selectedFeedbackArr'));
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $actionId = base64_decode($id);
      $orgId = Input::get('orgId');
      $feedbackId = Input::get('feedbackId');

      $insertArray['startedDate']    = date('Y-m-d H:i:s', strtotime(Input::get('start_date')));
      $insertArray['dueDate']        = date('Y-m-d H:i:s', strtotime(Input::get('end_date')));
      $insertArray['description']    = Input::get('description');
      $insertArray['responsibleId']  = Input::get('responiblityValue');      
      $insertArray['orgStatus']      = Input::get('status');       
      $insertArray['updated_at']     = date('Y-m-d H:i:s');

      if(!empty(Input::get('themeId')))
      {
        $insertArray['themeId'] = implode(',', Input::get('themeId'));
      }
      if(!empty($feedbackId))
      {
        $insertArray['feedbackId'] = implode(',', $feedbackId);
      }

      if(Input::get('userId')) {
       $insertArray['forUserId']      = Input::get('userId');
     }

     if(Input::get('officeId')) {
      $insertArray['officeId']        = Input::get('officeId'); 
    }

    if (Input::get('departmentId')) {
      $insertArray['departmentId']    = Input::get('departmentId');
    }

    if(Input::get('resUsers')) {
      $insertArray['responsibleUserId'] = Input::get('resUsers');
    }

    DB::table('actions')->where('id', $actionId)->update($insertArray);

    $allFeedbacks = DB::table('iot_feedbacks')->select('id','actionTaken')->where('orgId',base64_decode($orgId))->where('status','!=','Inactive')->get();

    // echo "<pre>";print_r($allFeedbacks);die();

    foreach ($allFeedbacks as $feedbackVals) {
        $actionIds = explode(',', $feedbackVals->actionTaken);
        $mainActionIds = '';
        if(!empty($actionIds) && in_array($actionId, $actionIds)){
            $mainActionIds = array_diff($actionIds, array($actionId));
            DB::table('iot_feedbacks')->where('id',$feedbackVals->id)->update([
                'actionTaken'     => implode(',', $mainActionIds),
                'updated_at'      => date('Y-m-d H:i:s')
            ]);
        }
    }

    if (!empty($feedbackId)) {
      foreach ($feedbackId as $feedVal) {

          $feedbacks = DB::table('iot_feedbacks')->where('id',$feedVal)->first();
          
          $actionArr = [];
          $actionFeedbacks = [];        
          if (!empty($feedbacks)) {
              if (!empty($feedbacks->actionTaken)) { //If feedback exist

                  $actionFeedbacks = explode(',', $feedbacks->actionTaken);

                  if (in_array($actionId, $actionFeedbacks)) {
                      continue;
                  }else{
                      array_push($actionFeedbacks, $actionId);
                  }

                  DB::table('iot_feedbacks')->where('id',$feedVal)->update([
                      'actionTaken' => implode(',', $actionFeedbacks),
                      'updated_at' => date('Y-m-d H:i:s')
                  ]);
              }else{ //No feedbacks

                  array_push($actionArr, $actionId);

                  DB::table('iot_feedbacks')->where('id',$feedVal)->update([
                      'actionTaken' => implode(',', $actionArr),
                      'updated_at' => date('Y-m-d H:i:s')
                  ]); 
              }
          }     
      }
    }

    $actionName = DB::table('actionResponsibleStatus')->where('id',$insertArray['responsibleId'])->first();

    return redirect("admin/action-list/$orgId")->with('message', $actionName->name.' - Action updated successfully.');
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCommentsList($id)
    {
      $actionId = base64_decode($id);

      $actionTbl = DB::table('actions')
      ->select('actions.themeId','actions.id','actions.userId','actions.description','actions.startedDate','actions.dueDate','actions.responsibleId','actions.orgStatus','actions.orgId','cuser.name AS cname','cuser.lastName AS clastName','offices.office','all_department.department','ruser.name AS rname','ruser.lastName AS rlastName','actionResponsibleStatus.name AS tier','actions.feedbackId')
      ->leftJoin('users AS cuser','cuser.id', '=', 'actions.userId')   
      ->leftJoin('users AS ruser','ruser.id', '=', 'actions.responsibleUserId')  
      ->leftJoin('offices','offices.id', '=', 'actions.officeId')         
      ->leftJoin('departments','departments.id', '=', 'actions.departmentId') 
      ->leftJoin('all_department','all_department.id', '=', 'departments.departmentId')   
      ->leftJoin('actionResponsibleStatus','actionResponsibleStatus.id', '=', 'actions.responsibleId')   
      ->where('actions.id',$actionId)
      ->where('actions.status', 'Active')
      ->get();

      $actions = array();
      foreach ($actionTbl as $value)
      {
        $obj = new Controller();

        $obj->id            = $value->id;
        $obj->userId        = $value->userId;
        $obj->description   = $value->description;
        $obj->startedDate   = $value->startedDate;
        $obj->dueDate       = $value->dueDate;
        $obj->responsibleId = $value->responsibleId;
        $obj->orgStatus     = $value->orgStatus;
        $obj->orgId         = $value->orgId;
        $obj->cname         = $value->cname;
        $obj->clastName     = $value->clastName;
        $obj->office        = $value->office;
        $obj->department    = $value->department;
        $obj->rname         = $value->rname;
        $obj->rlastName     = $value->rlastName;
        $obj->tier          = $value->tier;
        
        $allocatedThemearr = explode(',', $value->themeId);
        // $allocatedThemearr = json_decode($value->themeId);

        $themeTitleArr = array();
        if ($allocatedThemearr)
        {

          foreach ($allocatedThemearr as $themeId)
          {

            $theme = DB::table('iot_themes')->select('title')->where('status','Active')->where('id',$themeId)->first();

            if($theme)
            {
              array_push($themeTitleArr, $theme->title);
            }

          }

        }

        $obj->themeTitle  = count($themeTitleArr);
        // $obj->themeTitle  = implode(', ', $themeTitleArr);

        $allocatedFeedbackArr = explode(',', $value->feedbackId);
        $feedbackTitleArr = array();
        if ($allocatedFeedbackArr) {
          foreach ($allocatedFeedbackArr as $feedbackId) {
            $feedback = DB::table('iot_feedbacks')->select('message')->where('status','Active')->where('id',$feedbackId)->first();

            if($feedback) {
              array_push($feedbackTitleArr, $feedback->message);
            }
          }
        }

        $obj->feedbackTitle  = count($feedbackTitleArr);

        array_push($actions, $obj);
      }

      $actionsComments = DB::table('actions_comment as AC')
      ->select('AC.id','AC.userId','AC.comment','AC.created_at','users.name')
      ->leftJoin('users','users.id', '=', 'AC.userId')        
      ->where('AC.actionId',$actionId)
      ->orderBy('AC.id','DESC')
      ->get();  

      return view('admin/actions/actionComments',compact('id','actions','actionsComments'));
    }

    /*add new comment in action comment list*/
    public function addComment()
    {       
      date_default_timezone_set('Europe/London');
      $user = Auth::user();
      $insertArray['actionId']       = Input::get('actionId');  
      $insertArray['userId']         = $user->id;  
      $insertArray['comment']        = Input::get('comment');
      $insertArray['created_at']     = date('Y-m-d H:i:s');

      $status = DB::table('actions_comment')->insertGetId($insertArray);

      $actionId = base64_encode(Input::get('actionId'));

      return redirect("admin/action-comments/$actionId"); 
    }

    /*update status of the action*/
    public function updateActionStatus()
    {
      $actionId = Input::get('actionId');

      $insertArray['orgStatus'] = Input::get('status');  

      DB::table('actions')
      ->where('id', $actionId)
      ->update($insertArray);

      echo "success";
    }

    public function getResUser()
    {

      $orgId = base64_decode(Input::get('orgId'));

      if(Input::get('section')=="office")
      {
       $users = DB::table('users')
       ->where('orgId',$orgId) 
       ->where('officeId',Input::get('id'))
       ->where('status','Active')
       ->where('roleId',3)
       ->get();
     }
     elseif(Input::get('section')=="department")
     {
      $users = DB::table('users')
      ->where('orgId',$orgId) 
      ->where('departmentId',Input::get('id'))
      ->where('status','Active')
      ->where('roleId',3)
      ->get();
    }
    else
    {
     $users = DB::table('users')
     ->where('orgId',$orgId) 
     ->where('status','Active')
     ->where('roleId',3)
     ->get();

   }
   
   
   
   $select = '<select id="resUsers" name="resUsers" class="form-control" required>';
   $select .=' <option value="" disabled="" selected="">Responsible Person</option>';

   foreach ($users as $value) {
    $select .='<option value="'.$value->id.'">'.ucfirst($value->name).'</option>';
  }

  $select .='</select>';

  return $select;

}
/*delete action*/
public function deleteAction($id)
{
  $actionId = base64_decode($id);

  // DB::table('actions')->where('id',base64_decode($id))->update(['status'=>'Inactive']);  
  DB::table('actions')->where('id',$actionId)->delete();  

  DB::table('actions_comment')->where('actionId',$actionId)->delete();  

  //action id delete from feedback table
  $actionFeedback = DB::table('iot_feedbacks')
      ->where('status','Active')
      ->whereRaw("find_in_set(".$actionId.",actionTaken)")
      ->get();

  foreach ($actionFeedback as $feedbackVals) {
      $actionTaken = explode(',', $feedbackVals->actionTaken);
      $mainActionIds = '';
      if(!empty($actionTaken) && in_array($actionId, $actionTaken)){
          $mainActionIds = array_diff($actionTaken, array($actionId));
          DB::table('iot_feedbacks')->where('id',$feedbackVals->id)->update([
              'actionTaken'       => implode(',', $mainActionIds),
              'updated_at'    => date('Y-m-d H:i:s')
          ]);
      }
  }

  //action id delete from themes table
  $actionThemes = DB::table('iot_themes')
      ->where('status','Active')
      ->whereRaw("find_in_set(".$actionId.",linkedAction)")
      ->get();

  foreach ($actionThemes as $themesVals) {
      $actionTaken1 = explode(',', $themesVals->linkedAction);
      $mainActionIds1 = '';
      if(!empty($actionTaken1) && in_array($actionId, $actionTaken1)){
          $mainActionIds1 = array_diff($actionTaken1, array($actionId));
          DB::table('iot_themes')->where('id',$themesVals->id)->update([
              'linkedAction'       => implode(',', $mainActionIds1),
              'updated_at'    => date('Y-m-d H:i:s')
          ]);
      }
  }

  return redirect()->back()->with('message','Action Deleted Successfully.');
}

/*get theme add modal*/
public function getActionThemeModal()
{

  $selectedThemeArr = array();
  $orgId = base64_decode(Input::get('orgId'));

  if(Input::get('selectedThemeArr'))
  {
    $selectedThemeArr = Input::get('selectedThemeArr');
  }
  

  $themeList = DB::table('iot_themes')->where('status','Active')->where('orgId',$orgId)->orderBy('id','DESC')->get();

  return view('admin.actions.actionThemeModal',compact('themeList','orgId','selectedThemeArr'));

}

/*add new theme by ajax when modal is open*/
public function addNewActionThemeByAjax()
{

  $orgId = Input::get('orgId');

  $insertArray = array(
    "dateOpened" => date('Y-m-d'),
    "title" => Input::get('themeTitle'),
    "description" => '',
    "type" => 1,
    "orgId" => $orgId, 
    "submission" => '',
    "initialLikelihood" => '0', 
    "initialConsequence" => '0',
    "currentLikelihood" => '0',
    "currentConsequence" => '0',
    "linkedAction" => '',
    "themeStatus" => 'Closed',
    "status" => 'Active',
    "created_at"=> date('Y-m-d H:i:s')
  );

  if(Input::get('themeTitle'))
  {
    DB::table('iot_themes')->insertGetId($insertArray);
  }

  $themeList = DB::table('iot_themes')->select('id','title')->where('status','Active')->where('orgId',$orgId)->orderBy('id','DESC')->get();

  $htmlContainer = '';
  $htmlOption    = '';
  foreach ($themeList as $value)
  {
    $htmlContainer.='<li><a tabindex="0"><label class="checkbox" title="'.$value->title.'"><input type="checkbox" value="'.$value->id.'">'.$value->title.'</label></a></li>';

    $htmlOption.='<option value="'.$value->id.'">'.$value->title.'</option>'; 
  }

  $htmlArr = array('htmlContainer'=>$htmlContainer, 'htmlOption'=>$htmlOption);

  print_r(json_encode($htmlArr));

}
}
