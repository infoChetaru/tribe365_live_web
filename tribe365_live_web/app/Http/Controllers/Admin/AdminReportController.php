<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Validator;
use Hash;
use Illuminate\Support\Facades\Input;
use Config;
use View;
use Dompdf\Dompdf;
use Dompdf\Options;
use Carbon\Carbon;
use DateTime;
use Mail;
use DateInterval;
use DatePeriod;

class AdminReportController extends Controller
{

  public function __construct(Request $request)
  {
    $orgid = base64_decode($request->segment(3));

    $organisations = DB::table('organisations')
    ->select('ImageURL')
    ->where('id',$orgid)
    ->first();

    if(!empty($organisations->ImageURL))
    {
      $org_detail = base64_encode($orgid)."-".$organisations->ImageURL;

      View::share('org_detail', $org_detail);
  }else {

      $org_detail = "";

      View::share('org_detail', $org_detail);
  }

}

/*get DOT reports*/
public function getDOTreports(Request $request,$id)
{
    $orgId = base64_decode($id);

    $dots = DB::table('dots')->where('orgId',$orgId)->first();

    if (empty($dots))
    {
        return redirect()->back()->with('error',"Reports not available.");
    }

    $organisations = DB::table('organisations')->where('id',$orgId)->first();

    $dotValuesArray = array();
    $beliefList = array();

    if(!empty($dots))
    {
             //for search
        $searchBeliefId = $request->beliefId;
        session()->put('searchBeliefId', $searchBeliefId);
        $beliefWr = array();
        if($searchBeliefId)
        {
            $beliefWr['id'] = $searchBeliefId;
        }

        $dotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->where($beliefWr)
        ->orderBy('id','ASC')
        ->get();

        $beliefList = DB::table('dots_beliefs')->where('status','Active')->where('dotId',$dots->id)->get();

        foreach ($dotBeliefs as $key => $bValue)
        {

            $dotValues = DB::table('dots_values')
            ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
            ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
            ->where('dots_values.status','Active')
            ->where('dots_values.beliefId',$bValue->id)
            ->orderBy('dot_value_list.id','ASC')
            ->get();

            $bRatings = DB::table('dot_values_ratings')
            ->leftjoin('users','users.id','=','dot_values_ratings.userId')
            ->where('users.status','Active')
            ->where('beliefId', $bValue->id)->avg('ratings');

            $valuesArray = array();
            foreach ($dotValues as $key => $vValue) 
            {
                $vRatings = DB::table('dot_values_ratings')
                ->leftjoin('users','users.id','=','dot_values_ratings.userId')
                ->where('users.status','Active')
                ->where('valueId', $vValue->id)->avg('ratings');

                $vResult['valueId']      = $vValue->id;
                $vResult['valueName']    = ucfirst($vValue->name);

                $vResult['valueRatings'] = 0;
                if($vRatings)
                {
                    $vResult['valueRatings'] = $vRatings-1;
                }
                array_push($valuesArray, $vResult);
            }

            $result['beliefId']      = $bValue->id;
            $result['beliefName']    = ucfirst($bValue->name);           

            $result['beliefRatings'] = 0;
            if($bRatings)
            {
                $result['beliefRatings'] =$bRatings-1;
            }

            $result['beliefValues']  = $valuesArray;

            array_push($dotValuesArray, $result);
        }
    }

        //show image of organisation
    if(!empty($organisations->ImageURL))
    {
        $org_detail = base64_encode($orgId)."-".$organisations->ImageURL;
        View::share('org_detail', $org_detail);
    }


    return view('admin/report/detailRatingReport',compact('dotValuesArray','orgId','beliefList','organisations'));
}


/*get list of average rating of belief and rating*/
public function getBeliefValueRatingList(Request $request, $id)
{

    $orgId = base64_decode($id);

    $dots = DB::table('dots')->where('orgId',$orgId)->first();

    if (empty($dots))
    {
        return redirect()->back()->with('error',"Reports not available.");
    }

    $organisations = DB::table('organisations')->where('id',$orgId)->first();

    $beliefList = DB::table('dots_beliefs')->where('status','Active')->where('dotId',$dots->id)->get();

    $dotValuesArray = array();

    if(!empty($dots))
    {
         //for search
        $searchBeliefId = $request->beliefId;
        session()->put('searchBeliefId', $searchBeliefId);
        $beliefWr = array();
        if($searchBeliefId)
        {
            $beliefWr['id'] = $searchBeliefId;
        }

        $dotBeliefs = DB::table('dots_beliefs')->where('status','Active')->where('dotId',$dots->id)
        ->where($beliefWr)->orderBy('id','ASC')->get();

        foreach($dotBeliefs as $key => $bValue)
        {

            $dotValues = DB::table('dots_values')
            ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
            ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
            ->where('dots_values.status','Active')
            ->where('dots_values.beliefId',$bValue->id)
            ->orderBy('dot_value_list.id','ASC')->get();

            $bRatings = DB::table('dot_values_ratings')
                ->leftjoin('users','users.id','=','dot_values_ratings.userId')
                ->where('dot_values_ratings.beliefId', $bValue->id)
                ->where('users.status','Active')
                ->avg('ratings');

            $valuesArray = array();
            foreach ($dotValues as $key => $vValue) 
            {
                $vRatings = DB::table('dot_values_ratings')
                ->leftjoin('users','users.id','=','dot_values_ratings.userId')
                ->where('users.status','Active')
                ->where('valueId', $vValue->id)
                ->where('beliefId',$bValue->id)->avg('ratings');

                $vResult['valueId']      = $vValue->id;
                $vResult['valueName']    = ucfirst($vValue->name);

                $vResult['valueRatings'] = 0;
                if($vRatings)
                {
                    $vResult['valueRatings'] = $vRatings-1;
                }
                array_push($valuesArray, $vResult);
            }

            $result['beliefId']      = $bValue->id;
            $result['beliefName']    = ucfirst($bValue->name);           

            $result['beliefRatings'] = 0;
            if($bRatings)
            {
                $result['beliefRatings'] = $bRatings-1;
            }

            $result['beliefValues']  = $valuesArray;

            array_push($dotValuesArray, $result);
        }
    }
         //show image of organisation
    if(!empty($organisations->ImageURL))
    {
        $org_detail = base64_encode($orgId)."-".$organisations->ImageURL;
        View::share('org_detail', $org_detail);
    }

    return view('admin/report/listBeliefValueRatingReport',compact('dotValuesArray','organisations','beliefList'));

}

/*get individual rating of values*/
public function getIndividualBeliefValueRatingList(Request $request,$id)
{

    $beliefId = base64_decode($id);

    $beliefs = DB::table('dots_beliefs')
    ->select('organisations.ImageURL','organisations.organisation','dots_beliefs.id','organisations.id AS orgId')
    ->leftjoin('dots','dots.id','dots_beliefs.dotId')
    ->leftjoin('organisations','organisations.id','dots.orgId')
    ->where('dots_beliefs.id',$beliefId)->first();

    if (empty($beliefs))
    {
        return redirect()->back()->with('error',"Reports not available.");
    }

    //search
    $valueWr =array();
    $customValueId = $request->custValueId;
    session()->put('customValueId', $customValueId);
    if ($customValueId) {
        $valueWr['dots_values.name'] = $customValueId;
    }

    $dotValues = DB::table('dots_values')   
    ->select('dots_values.id','dot_value_list.name')
    ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
    ->where('dots_values.beliefId',$beliefId)
    ->where($valueWr)
    ->where('dots_values.status','Active')
    ->get();

    $custValueList = DB::table('dot_value_list')->where('status','Active')->get();


    $valueRatingArray = array();
    foreach ($dotValues as $key => $bValue)
    {

        $ratings = DB::table('dot_values_ratings')
        ->select('users.name','dot_values_ratings.ratings','dot_values_ratings.id','dot_values_ratings.created_at')
        ->leftJoin('users','users.id','dot_values_ratings.userId')
        ->where('valueId',$bValue->id)->get();

        $vRatings = DB::table('dot_values_ratings')->where('valueId', $bValue->id)->avg('ratings');

        $bValue->valueRating = 0;
        if($vRatings)
        {
            $bValue->valueRating = $vRatings-1;
        }

        $bValue->ratingArray = $ratings;

        array_push($valueRatingArray, $bValue);
    }

        //show image of organisation
    $organisations = DB::table('organisations')->where('id',$beliefs->orgId)->first();
    if(!empty($organisations->ImageURL))
    {
        $org_detail = base64_encode($beliefs->orgId)."-".$organisations->ImageURL;
        View::share('org_detail', $org_detail);
    }

    return view('admin/report/listIndividualBeliefValueRatingReport',compact('valueRatingArray','beliefs','custValueList'));
}

/*get diagnostic reports organisations list*/
public function getDiagOrgList()
{
    $organisation = DB::table('organisations')->where('status','Active')->orderBy('id','DESC')->paginate(12);
    return View('admin/report/listDiagnosticOrganisation',compact('organisation'));
}
/*get diagnostic resport graph*/
public function getDiagGraph(Request $request)
{

    $resultArray = array();

    $orgId = base64_decode($request->id);

    $diagnosticQueCatTbl = DB::table('diagnostic_questions_category')->get();

    $orgName = DB::table('organisations')->select('organisation')->where('id',$orgId)->first();

        //get user list
    $users = DB::table('diagnostic_answers')->select('userId')->groupBy('userId')->where('orgId',$orgId)->get();
    $userCount = count($users);

    if (empty($userCount))
    {
        return redirect()->back()->with('error',"Diagnostic answers are not done yet.");
    }

    $optionsTbl= DB::table('diagnostic_question_options')->where('status','Active')->get();
    $diaOptCount = count($optionsTbl)-1; 

    foreach ($diagnosticQueCatTbl as $value)
    {
        $questionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

        $quecount = count($questionTbl);

        $perQuePercen = 0;
        foreach ($questionTbl as  $queValue)
        {
            $diaAnsTbl = DB::table('diagnostic_answers')            
            ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
            ->where('diagnostic_answers.orgId',$orgId)
            ->where('diagnostic_questions.id',$queValue->id)
            ->where('diagnostic_questions.category_id',$value->id)
            ->sum('answer');                
            $perQuePercen += ($diaAnsTbl/$userCount);               
        }


        $score = ($perQuePercen/($quecount*$diaOptCount));
        $totalPercentage = ($perQuePercen/($quecount*$diaOptCount))*100;

        $value1['title']      =  $value->title;
        $value1['score']      =  number_format((float)$score, 2, '.', '');
        $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');            

        array_push($resultArray, $value1);
    }

    return View('admin/report/detailDiagnosticReport',compact('resultArray','orgName'));
}

/*get tribeometer reports organisations list*/
public function getTribeoOrgList()
{
    $organisation = DB::table('organisations')->where('status','Active')->orderBy('id','DESC')->paginate(12);
    return View('admin/report/listTribeometerOrganisation',compact('organisation'));
}

/*get tribeometer resport graph*/
public function getTribeoGraph(Request $request)
{

    $resultArray = array();

    $orgId = base64_decode($request->id);

    $orgName  = DB::table('organisations')->select('organisation')->where('id',$orgId)->first();

    $queCatTbl = DB::table('tribeometer_questions_category')->get();

        //get user list
    $users = DB::table('tribeometer_answers')->select('userId')->groupBy('userId')->where('orgId',$orgId)->get();
    $userCount = count($users);

    if(empty($userCount))
    {
        return redirect()->back()->with('error',"Tribeometer answers are not done yet.");
    }       


    $optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
    $optCount = count($optionsTbl)-1; 

    foreach ($queCatTbl as $value)
    {
        $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();

        $quecount = count($questionTbl);

        $perQuePercen = 0;
        foreach ($questionTbl as  $queValue)
        {
            $diaAnsTbl = DB::table('tribeometer_answers')           
            ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
            ->where('tribeometer_answers.orgId',$orgId)
            ->where('tribeometer_questions.id',$queValue->id)
            ->where('tribeometer_questions.category_id',$value->id)
            ->sum('answer');

                //avg of all questions
            $perQuePercen += ($diaAnsTbl/$userCount);               
        }

        $score = ($perQuePercen/($quecount*$optCount));
        $totalPercentage = ($perQuePercen/($quecount*$optCount))*100;

        $value1['title']      =  $value->title;
        $value1['score']      =  number_format((float)$score, 2, '.', '');
        $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');            

        array_push($resultArray, $value1);
    }
    return View('admin/report/detailTribeometerReport',compact('resultArray','orgName'));
}


/*get cot functional lens detail*/
public function getCOTFunctionalLensGraph(Request $request)
{

   $orgId = base64_decode($request->orgId);

   $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get();

   $officeId     = $request->officeId;
   $departmentId = $request->departmentId;

   $customDept = DB::table('departments')
   ->select('departments.id','all_department.department')
   ->leftJoin('all_department','all_department.id','departments.departmentId')
   ->where('departments.officeId',$officeId)->get();

   session()->put('officeId', $officeId);
   session()->put('departmentId', $departmentId);
   session()->put('customDept',$customDept);

   $query = DB::table('users')
   ->select('users.id','users.name','offices.office','all_department.department')
   ->leftJoin('offices', 'offices.id', '=', 'users.officeId')   
   ->leftJoin('departments', 'departments.id', '=', 'users.departmentId')
   ->leftJoin('all_department','all_department.id', '=', 'departments.departmentId')
   ->where('users.orgId',$orgId)
   ->where('users.roleId',3)
   ->where('users.status','Active')
   ->orderBy('users.id','DESC');

   if($officeId)
   {
    $query->where('users.officeId', $officeId);
}

if($departmentId)
{
    $query->where('users.departmentId', $departmentId);
}

$userTbl = $query->paginate(10);

$variable = array('EI','SN','TF','JP');

$cotFuncUserArray = array();

foreach($userTbl as $userValue)
{

    $scoreArray   = array();

    foreach ($variable as $value)
    {

    $value1 = substr($value, -2,1);//get first char
    $value2 = substr($value, 1);//get last char

    $countE = DB::table('cot_functional_lens_answers AS cfla')
    ->select('cflqo.option_name AS optionName')
    ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
    ->where('cfla.userId',$userValue->id)->where('cflqo.initial',$value1)->get();

    $countI = DB::table('cot_functional_lens_answers AS cfla')
    ->select('cflqo.option_name AS optionName')
    ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
    ->where('cfla.userId',$userValue->id)->where('cflqo.initial',$value2)->get();

    if(count($countE) > count($countI))
    {
      $countValEI = count($countE)-count($countI);
      $initialValEI  =  $value1;
  }
  elseif (count($countE) < count($countI))
  {
      $countValEI = count($countI)-count($countE);
      $initialValEI  =  $value2;

  }elseif(count($countE) == count($countI))
  {
      $countValEI    = count($countE)-count($countI);
      $initialValEI  =  $value1;
  }
  else
  {
      $countValEI    = '-';
      $initialValEI  = '-';
  }

  array_push($scoreArray, array('key'=>$initialValEI,'score'=>$countValEI));

}

$userValue->scoreArray   = $scoreArray;

array_push($cotFuncUserArray, $userValue);

}

return view('admin/report/report_graph/cotFunctionalLensUserListGraph',compact('cotFuncUserArray','offices','orgId'));

}

/*get sot motivation data for graph*/
public function getSOTmotivationGraph(Request $request)
{

    $sotMotivationUserArray = array();

    $orgId = base64_decode($request->orgId);
    
    $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get();

    $officeId     = $request->officeId;
    $departmentId = $request->departmentId;

    $customDept1 = DB::table('departments')
    ->select('departments.id','all_department.department')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('departments.officeId',$officeId)->get();

    $customDept = array();
    foreach ($customDept1 as $value)
    {
      $users = DB::table('users')->where('status','Active')->where('departmentId',$value->id)->first();

      if($users)
      {
        array_push($customDept, $value);
    }
}

session()->put('officeId', $officeId);
session()->put('departmentId', $departmentId);
session()->put('customDept', $customDept);

$query = DB::table('users')
->select('users.id AS userId', 'users.name', 'offices.office', 'all_department.department')
->leftJoin('offices', 'offices.id', '=', 'users.officeId')   
->leftJoin('departments', 'departments.id', '=', 'users.departmentId')
->leftJoin('all_department','all_department.id', '=', 'departments.departmentId') 
->where('users.orgId',$orgId)
->where('users.roleId',3)
->where('users.status','Active')
->orderBy('users.id','DESC');

if($officeId) 
{ 
  $query->where('users.officeId',$officeId);
}
if($departmentId)
{ 
  $query->where('users.departmentId',$departmentId);
}

$userTbl = $query->get();

$userArray = array();
foreach ($userTbl as $key => $userValue)
{

  $categoryTbl = DB::table('sot_motivation_value_records')->where('status','Active')->get();

  $catArray = array();
  foreach ($categoryTbl as $value)
  {  

    $ansTbl = DB::table('sot_motivation_answers AS sotans')         

    ->leftJoin('sot_motivation_question_options AS qoption', 'qoption.id', '=', 'sotans.optionId')
    ->where('sotans.userId',$userValue->userId)
    ->where('sotans.orgId',$orgId)
    ->where('sotans.status','Active')
    ->where('qoption.category_id',$value->id)
    ->sum('sotans.answer');

    $result['title'] = $value->title;
    $result['score'] = $ansTbl;

    array_push($catArray, $result);

}

$userValue->sotMotivationValues = $catArray;

array_push($sotMotivationUserArray, $userValue);

}

return view('admin/report/report_graph/sotMotivationUserListGraph',compact('sotMotivationUserArray','offices','orgId'));

}

//get all reports organisation
public function index()
{
    $organisation = DB::table('organisations')->where('status','Active')->orderBy('id','DESC')->paginate(12);
    return View('admin/report/listOrganisation',compact('organisation'));
}


//01->31 ,02->28, 03->31, 04->30, 05->31, 06->30, 07->31, 08->31, 09->30, 10->31, 11->30, 12->31

//Cron
public function getIndexesOfAllOrganizationByPastDate(){
    $year  = '2019';
    $month = '11';  
    //Get total days of month and year
    //$totalDays = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    $totalDays = 5;
    for ($day = 1; $day <= $totalDays; $day++) {
        $makeDate = $year."-".$month."-".$day;
        $this->getIndexesOfAllOrganization($makeDate);
    }
    echo "Done for :".$month." - ".$year." days ".$totalDays;
    die('<br /> Finally Done.');
}



//This function will generate index of org by date
public function getIndexesOfAllOrganization($date=false){
    
    //Send test email
    /*
    Mail::send('welcome', [], function($message) {
        $message->to('ram@chetaru.com')->subject('Testing mails at: '.date('Y-m-d H:i:s')); 
    });
    */

    if($date){
        $date = $date;
    }else{
        $date =date('Y-m-d');
    }


    //Get all organization
    $allOrganizations = DB::table('organisations')
                            ->where('status','Active') 
                            //->where('id','42') 
                            ->select('id')
                            ->whereDate('created_at', '<=',$date)
                            ->get();
    if($allOrganizations){                                                                                                                                                                                                                      
        foreach($allOrganizations as $organization){
            $orgId = $organization->id;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
            $indexOrg        = $this->calculationIndex(base64_encode($orgId),'','',$date,'1');
            $indexOrgExclude = $this->calculationIndex(base64_encode($orgId),'','',$date,'2');

            $engageIndexOrg = app('App\Http\Controllers\Admin\AdminPerformanceController')->calculateEngageIndex(base64_encode($orgId),'','',$date,'1');
            $engageIndexOrgExclude = app('App\Http\Controllers\Admin\AdminPerformanceController')->calculateEngageIndex(base64_encode($orgId),'','',$date,'2');

            //echo $indexOrg." == ".$indexOrgExclude;die;

            //Chk duplicate record for same day
            $orgRecords = DB::table('indexReportRecords')
                    ->select('id')
                    ->where('orgId',$orgId)
                    ->whereNull('officeId')
                    ->whereNull('departmentId')
                    ->whereDate('date',$date)
                    ->first();

            if($orgRecords){
                //No need to insert
            }else{
                $insertOrgArray = array(
                    'orgId'         => $orgId,              
                    //'officeId'      => '',    
                    //'departmentId'  => '', 
                    'date'          => $date,
                    'total_count'   => $indexOrg,
                    'total_count_exclude_weekend'   => $indexOrgExclude,
                    'engage_with_weekend'   => $engageIndexOrg,
                    'engage_without_weekend'   => $engageIndexOrgExclude,
                    'created_at'    => date('Y-m-d H:i:s')
                );
                $indexOrgReportRecords = DB::table('indexReportRecords')->insertGetId($insertOrgArray);
            } 
       
            //Get offices of organization
            $offices = DB::table('offices')
                        ->select('id')
                        ->where('status','Active') 
                        ->whereDate('created_at', '<=',$date)
                        ->where('orgId',$orgId)->get();
            if($offices){
                foreach($offices as $office){
                    $officeId = $office->id;
                    $indexOffice        = $this->calculationIndex(base64_encode($orgId),$officeId,'',$date,'1');
                    $indexOfficeExclude = $this->calculationIndex(base64_encode($orgId),$officeId,'',$date,'2');

                    $engageIndexOffice = app('App\Http\Controllers\Admin\AdminPerformanceController')->calculateEngageIndex(base64_encode($orgId),$officeId,'',$date,'1');
                    $engageIndexOfficeExclude = app('App\Http\Controllers\Admin\AdminPerformanceController')->calculateEngageIndex(base64_encode($orgId),$officeId,'',$date,'2');
            
                    //Chk duplicate record for same day
                    $officeRecords = DB::table('indexReportRecords')
                            ->select('id')
                            ->where('orgId',$orgId)
                            ->where('officeId',$officeId)
                            ->whereNull('departmentId')
                            ->whereDate('date',$date)
                            ->first();

                    if($officeRecords){
                        //No need to insert
                    }else{
                        $insertOfficeArray = array(
                            'orgId'         => $orgId,              
                            'officeId'      => $officeId,    
                            //'departmentId'  => '', 
                            'date'          => $date,
                            'total_count'   => $indexOffice,
                            'total_count_exclude_weekend'   => $indexOfficeExclude,
                            'engage_with_weekend'   => $engageIndexOffice,
                            'engage_without_weekend'   => $engageIndexOfficeExclude,
                            'created_at'    => date('Y-m-d H:i:s')
                        );
                        $indexOfficeReportRecords = DB::table('indexReportRecords')->insertGetId($insertOfficeArray);
                    } 
                    //Get departments
                    $departments = DB::table('departments')
                        //->select('id')
                        ->where('status','Active') 
                        ->where('officeId',$officeId)
                        ->whereDate('created_at', '<=',$date)
                        ->where('orgId',$orgId)->get();
                    if($departments){
                        foreach ($departments as $department) {
                            $departmentId = $department->id;
                            $allDepartmentId = $department->departmentId;
                           
                            //Get index
                            $deptindex = $this->calculationIndex(base64_encode($orgId),'',$allDepartmentId,$date,'1');
                            $deptindexExclude = $this->calculationIndex(base64_encode($orgId),'',$allDepartmentId,$date,'2');
                            $engageIndexAllDept = app('App\Http\Controllers\Admin\AdminPerformanceController')->calculateEngageIndex(base64_encode($orgId),'',$allDepartmentId,$date,'1');
                            $engageIndexAllDeptExclude = app('App\Http\Controllers\Admin\AdminPerformanceController')->calculateEngageIndex(base64_encode($orgId),'',$allDepartmentId,$date,'2');
                            
                            //Chk duplicate record for same day
                            $deptRecords = DB::table('indexReportRecords')
                                    ->select('id')
                                    ->where('orgId',$orgId)
                                    ->whereNull('officeId')
                                    ->where('departmentId',$allDepartmentId)
                                    ->whereDate('date',$date)
                                    // ->where(DB::raw("(DATE_FORMAT(date,'%Y-%m-%d'))"),$date)
                                    ->first();
                            if($deptRecords){
                                //No need to insert
                            }else{
                                $insertDeptArray = array(
                                    'orgId'         => $orgId,              
                                    //'officeId'      => '',    
                                    'departmentId'  => $allDepartmentId, 
                                    'isAllDepId'    => "1",
                                    'date'          => $date,
                                    'total_count'   => $deptindex,
                                    'total_count_exclude_weekend'   => $deptindexExclude,
                                    'engage_with_weekend'   => $engageIndexAllDept,
                                    'engage_without_weekend'   => $engageIndexAllDeptExclude,
                                    'created_at'    => date('Y-m-d H:i:s')
                                );
                                $indexDeptReportRecords = DB::table('indexReportRecords')->insertGetId($insertDeptArray);
                            }

                            $index = $this->calculationIndex(base64_encode($orgId),$officeId,$departmentId,$date,'1');
                            $indexExclude = $this->calculationIndex(base64_encode($orgId),$officeId,$departmentId,$date,'2');
                            $engageIndexDept = app('App\Http\Controllers\Admin\AdminPerformanceController')->calculateEngageIndex(base64_encode($orgId),'',$allDepartmentId,$date,'1');
                            $engageIndexDeptExclude = app('App\Http\Controllers\Admin\AdminPerformanceController')->calculateEngageIndex(base64_encode($orgId),'',$allDepartmentId,$date,'2');

                            //Chk duplicate record for same day
                            $indexrecords = DB::table('indexReportRecords')
                                    ->select('id')
                                    ->where('orgId',$orgId)
                                    ->where('officeId',$officeId)
                                    ->where('departmentId',$departmentId)
                                    ->whereDate('date',$date)
                                    // ->where(DB::raw("(DATE_FORMAT(date,'%Y-%m-%d'))"),$date)
                                    ->first();

                            if($indexrecords){
                                //No need to insert
                            }else{
                                $insertArray = array(
                                    'orgId'         => $orgId,              
                                    'officeId'      => $officeId,    
                                    'departmentId'  => $departmentId, 
                                    'isAllDepId'    => "2",
                                    'date'          => $date,
                                    'total_count'   => $index,
                                    'total_count_exclude_weekend'   => $indexExclude,
                                    'engage_with_weekend'   => $engageIndexDept,
                                    'engage_without_weekend'   => $engageIndexDeptExclude,
                                    'created_at'    => date('Y-m-d H:i:s')
                                );
                                $indexReportRecords = DB::table('indexReportRecords')->insertGetId($insertArray);
                            }        
                        }   
                    }
                }
            }
        }
    }
}


public function calculationIndex($id,$officeId,$departmentId,$date,$excludeWeekend){

    //$date='2018-08-20';
    //$date='2019-11-05';

    $orgId = base64_decode($id);
    $departmentId = $departmentId;
    $officeId = $officeId;

     //Get dots of org            
    $dots = DB::table('dots')
            ->where('orgId',$orgId)->first();
            
//------------------------------------Calculation Start---------------------------------------

   //check DOT g is given by user on every dot value if it is yes then true
    $usersQuery = DB::table('users')
    ->select('users.id as id')
    ->where('users.roleId',3)
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId');
    if(!empty($orgId)){
        $usersQuery->where('users.orgId',$orgId);
    }

    if($date){
        $usersQuery->whereDate('users.created_at','<=',$date);
    }
    if($excludeWeekend!=1){
        //Get weekends
        $getWeekendDates = $this->getWeekendDaysFromMonth($date,1);
        $usersQuery->whereNotIn(DB::raw("(DATE_FORMAT(users.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }

    if (!empty($officeId) && empty($departmentId)) {
        $usersQuery->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $usersQuery->where('users.officeId',$officeId);
        $usersQuery->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $usersQuery->where('departments.departmentId',$departmentId);
    }

    $users      = $usersQuery->get();
    $userCount  = count($users);

    $dotValueRatingCompletedUserArr = array();
    foreach($users as $duValue){
        $dotPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);

        //$dotPerArr = array('orgId' => $orgId, 'userId' => $duValue->id, 'updateAt' => '', 'currentDate' => '', 'month' => '', 'year' => '');
        $status = $this->indexReportForDotCompletedValues($dotPerArr);
        //$status = app('App\Http\Controllers\Admin\AdminPerformanceController')->getDotPerformance($dotPerArr);
        if($status){
            array_push($dotValueRatingCompletedUserArr, $duValue->id);      
        }
    }
    $dotValueRatingCompletedUserArrCount = count($dotValueRatingCompletedUserArr);
    $dotCompleted = "0";
    if(!empty($dotValueRatingCompletedUserArrCount) && !empty($userCount)){
        //echo $dotValueRatingCompletedUserArrCount." = ".$userCount;die;
        $dotCompleted = round(($dotValueRatingCompletedUserArrCount/$userCount)*100,2);
    }

    //for team role map complete
    $teamPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);
    //$teamPerArr = array('orgId' => $orgId,'updateAt'=>'','currentDate'=>'','month'=>'','year'=>'');
    $teamRoleStatus = $this->indexReportForTeamroleCompleted($teamPerArr);
    //$teamRoleStatus = app('App\Http\Controllers\Admin\AdminPerformanceController')->getTeamRoleMap($teamPerArr);
    $temRoleMapCount        = count($teamRoleStatus);
    $tealRoleMapCompleted   = "0";
    if (!empty($temRoleMapCount) && !empty($userCount)){
        $tealRoleMapCompleted = round(($temRoleMapCount/$userCount)*100,2);
    }

    //for personality type completed
    $personalityPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);
    //$personalityPerArr = array('orgId' => $orgId,'updateAt'=>'','currentDate'=>'','month'=>'','year'=>'');
    $personalityTypeStatus = $this->indexReportForpersonalityTypeCompleted($personalityPerArr);
    //$personalityTypeStatus = app('App\Http\Controllers\Admin\AdminPerformanceController')->getPersonalityType($personalityPerArr);

    $personalityTypeCount       = count($personalityTypeStatus);
    $personalityTypeCompleted   = "0";
    if (!empty($personalityTypeCount) && !empty($userCount)){
        $personalityTypeCompleted = round(($personalityTypeCount/$userCount)*100,2);
    }

    //for culture structure comleted       
    $cultureStructurePerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date,'excludeWeekend' => $excludeWeekend);
    $cultureStructureStatus = $this->indexReportForCultureStructureCompleted($cultureStructurePerArr);
    //$cultureStructureStatus = app('App\Http\Controllers\Admin\AdminPerformanceController')->getCultureStructure($cultureStructurePerArr);
    $cultureStructureCount      = count($cultureStructureStatus);
    $cultureStructureCompleted  = "0";
    if (!empty($cultureStructureCount) && !empty($userCount)){
        $cultureStructureCompleted = round(($cultureStructureCount/$userCount)*100,2);
    }

    //for motivation comleted       
    $motivationPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);
    //$motivationPerArr = array('orgId' => $orgId,'updateAt'=>'','currentDate'=>'','month'=>'','year'=>'');
    $motivationStatus = $this->indexReportForMotivationCompleted($motivationPerArr);
    //$motivationStatus = app('App\Http\Controllers\Admin\AdminPerformanceController')->getMotivation($motivationPerArr);

    $motivationCount = count($motivationStatus);
    $motivationCompleted = "0";
    if (!empty($motivationCount) && !empty($userCount)){
        $motivationCompleted = round(($motivationCount/$userCount)*100,2);
    }

    //Kudos/Thumbsup Completed for yesterday
    $thumbsupPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);
    $thumbsupCount = $this->indexReportForThumbsupCompleted($thumbsupPerArr);

    $thumbCompleted = "0";
    if(!empty($thumbsupCount) && !empty($userCount)){
        $thumbCompleted = round(($thumbsupCount/$userCount), 2);
    }

    /*get previous day Good/happy index detail*/
    $happyIndexPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);
    $happyIndexCompleted = $this->indexReportForHappyIndexCompleted($happyIndexPerArr);




    //Tribeometer and diagnostics completed last month by users
    $diagnosticAnsCompletedUserArr = array();
    $tribeometerAnsCompletedUserArr = array();
    foreach($users as $duValue){


        if($date){
            //Get Previos month of date
            $timestamp = strtotime ("-1 month",strtotime ($date));
            //Convert into 'Y-m' format
            $previosMonthFromDate  =  date("Y-m",$timestamp);

            $isDiagnosticAnsDoneQuery  = DB::table('diagnostic_answers')
                ->leftjoin('users','users.id','diagnostic_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('diagnostic_answers.userId',$duValue->id)
                ->where('diagnostic_answers.orgId',$orgId)
                ->where('diagnostic_answers.status','Active')
                ->where('users.status','Active')
                ->where('departments.status','Active')
                ->where('all_department.status','Active')
                ->where('diagnostic_answers.updated_at','LIKE',$previosMonthFromDate."%");
                if($excludeWeekend!=1){
                    //Get weekends
                    $getWeekendDates = $this->getWeekendDaysFromMonth($previosMonthFromDate,2);
                    //echo '<pre />';print_r($getWeekendDates);die;
                    $isDiagnosticAnsDoneQuery->whereNotIn(DB::raw("(DATE_FORMAT(diagnostic_answers.updated_at,'%Y-%m-%d'))"),$getWeekendDates);
                }
        }else{
            $lastMonth = date("Y-m", strtotime("-1 month"));
            $isDiagnosticAnsDoneQuery  = DB::table('diagnostic_answers')
                ->leftjoin('users','users.id','diagnostic_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('diagnostic_answers.userId',$duValue->id)
                ->where('diagnostic_answers.orgId',$orgId)
                ->where('diagnostic_answers.status','Active')
                ->where('users.status','Active')
                ->where('departments.status','Active')
                ->where('all_department.status','Active')
                ->where('diagnostic_answers.updated_at','LIKE',$lastMonth."%");
                if($excludeWeekend!=1){
                    //Get weekends
                    $getWeekendDates = $this->getWeekendDaysFromMonth($lastMonth,2);
                    //echo '<pre />';print_r($getWeekendDates);die;
                    $isDiagnosticAnsDoneQuery->whereNotIn(DB::raw("(DATE_FORMAT(diagnostic_answers.updated_at,'%Y-%m-%d'))"),$getWeekendDates);
                }
        } 

        if (!empty($officeId) && empty($departmentId)) {
            $isDiagnosticAnsDoneQuery->where('users.officeId',$officeId);
        }
        elseif (!empty($officeId) && !empty($departmentId)) {
            $isDiagnosticAnsDoneQuery->where('users.officeId',$officeId);
            $isDiagnosticAnsDoneQuery->where('users.departmentId',$departmentId);
        }
        elseif (empty($officeId) && !empty($departmentId)) {
            $isDiagnosticAnsDoneQuery->where('departments.departmentId',$departmentId);
        }  
        $isDiagnosticAnsDone = $isDiagnosticAnsDoneQuery->first();


        if($date){
            //Get Previos month of date
            $timestamp = strtotime ("-1 month",strtotime ($date));
            //Convert into 'Y-m' format
            $previosMonthFromDate  =  date("Y-m",$timestamp);

            $isTribeometerAnsDoneQuery = DB::table('tribeometer_answers')
                ->leftjoin('users','users.id','tribeometer_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('tribeometer_answers.userId',$duValue->id)
                ->where('tribeometer_answers.orgId',$orgId)
                ->where('tribeometer_answers.status','Active')
                ->where('users.status','Active')
                ->where('tribeometer_answers.updated_at','LIKE',$previosMonthFromDate."%");
                if($excludeWeekend!=1){
                    //Get weekends
                    $getWeekendDates = $this->getWeekendDaysFromMonth($previosMonthFromDate,2);
                    //echo '<pre />';print_r($getWeekendDates);die;
                    $isTribeometerAnsDoneQuery->whereNotIn(DB::raw("(DATE_FORMAT(tribeometer_answers.updated_at,'%Y-%m-%d'))"),$getWeekendDates);
                }
        }else{
            $lastMonth = date("Y-m", strtotime("-1 month"));

            $isTribeometerAnsDoneQuery = DB::table('tribeometer_answers')
                ->leftjoin('users','users.id','tribeometer_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('tribeometer_answers.userId',$duValue->id)
                ->where('tribeometer_answers.orgId',$orgId)
                ->where('tribeometer_answers.status','Active')
                ->where('users.status','Active')
                ->where('tribeometer_answers.updated_at','LIKE',$lastMonth."%");
                if($excludeWeekend!=1){
                    //Get weekends
                    $getWeekendDates = $this->getWeekendDaysFromMonth($lastMonth,2);
                    //echo '<pre />';print_r($getWeekendDates);die;
                    $isTribeometerAnsDoneQuery->whereNotIn(DB::raw("(DATE_FORMAT(tribeometer_answers.updated_at,'%Y-%m-%d'))"),$getWeekendDates);
                }
        }

        if (!empty($officeId) && empty($departmentId)) {
            $isTribeometerAnsDoneQuery->where('users.officeId',$officeId);
        }elseif (!empty($officeId) && !empty($departmentId)) {
            $isTribeometerAnsDoneQuery->where('users.officeId',$officeId);
            $isTribeometerAnsDoneQuery->where('users.departmentId',$departmentId);
        }elseif (empty($officeId) && !empty($departmentId)) {
            $isTribeometerAnsDoneQuery->where('departments.departmentId',$departmentId);
        } 
        $isTribeometerAnsDone = $isTribeometerAnsDoneQuery->first();

        if(!empty($isDiagnosticAnsDone)){
            array_push($diagnosticAnsCompletedUserArr, $duValue->id);      
        }
        if($isTribeometerAnsDone){
            array_push($tribeometerAnsCompletedUserArr, $duValue->id);      
        }
    }

    $diagnosticAnsCompletedUserArrCount  = count($diagnosticAnsCompletedUserArr);
    $tribeometerAnsCompletedUserArrCount = count($tribeometerAnsCompletedUserArr);


    $diagnosticAnsCompleted = "0";
    if(!empty($diagnosticAnsCompletedUserArrCount) && !empty($userCount)){
        $diagnosticAnsCompleted = ($diagnosticAnsCompletedUserArrCount/$userCount)*100;
    }
    $tribeometerAnsCompleted = "0";
    if(!empty($tribeometerAnsCompletedUserArrCount) && !empty($userCount)){
        $tribeometerAnsCompleted = ($tribeometerAnsCompletedUserArrCount/$userCount)*100;
    }

//echo $diagnosticAnsCompleted;die;
//echo $tribeometerAnsCompleted;die;   






    //All average ratings for values by users
    $valuesPerformance = 0;
    if ($dots) {
        $allDotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->orderBy('id','ASC')
        ->get();
        $totalBeliefRatings = 0;
        $dotValuesCount = 0;
        foreach ($allDotBeliefs as $key => $bValue){
            if($date){
                $dotValues = DB::table('dots_values')
                ->where('dots_values.status','Active')
                ->where('dots_values.beliefId',$bValue->id)   
                ->whereDate('dots_values.created_at','<=',$date);
                if($excludeWeekend!=1){
                    //Get weekends
                    $getWeekendDates = $this->getWeekendDaysFromMonth($date,1);
                    //echo '<pre />';print_r($getWeekendDates);die;
                    $dotValues->whereNotIn(DB::raw("(DATE_FORMAT(dots_values.created_at,'%Y-%m-%d'))"),$getWeekendDates);
                }
                $dotValues->get();    
            }else{  
                $dotValues = DB::table('dots_values')
                ->where('dots_values.status','Active')
                ->where('dots_values.beliefId',$bValue->id);
                if($excludeWeekend!=1){
                    //Get weekends
                    $getWeekendDates = $this->getWeekendDaysFromMonth($date,1);
                    //echo '<pre />';print_r($getWeekendDates);die;
                    $dotValues->whereNotIn(DB::raw("(DATE_FORMAT(dots_values.created_at,'%Y-%m-%d'))"),$getWeekendDates);
                }
                $dotValues->get();   
            }            
            $dotValuesCount += count($dotValues);

            $beliefRatingQuery = DB::table('dot_values_ratings')
                ->leftjoin('users','users.id','dot_values_ratings.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('dot_values_ratings.beliefId',$bValue->id)
                ->where('users.status','Active')
                ->where('dot_values_ratings.status','Active');
            if($date){
                $beliefRatingQuery->whereDate('dot_values_ratings.created_at','<=',$date);
            }            

            if($excludeWeekend!=1){
                //Get weekends
                $getWeekendDates = $this->getWeekendDaysFromMonth($date,1);
                //echo '<pre />';print_r($getWeekendDates);die;
                $beliefRatingQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_values_ratings.created_at,'%Y-%m-%d'))"),$getWeekendDates);
            }

            if (!empty($officeId) && empty($departmentId)) {
                $beliefRatingQuery->where('users.officeId',$officeId);
            }elseif (!empty($officeId) && !empty($departmentId)) {
                $beliefRatingQuery->where('users.officeId',$officeId);
                $beliefRatingQuery->where('users.departmentId',$departmentId);
            }elseif (empty($officeId) && !empty($departmentId)) {
                $beliefRatingQuery->where('departments.departmentId',$departmentId);
            } 
            $beliefRating = $beliefRatingQuery->avg('ratings');
            if($beliefRating){
                $totalBeliefRatings += $beliefRating-1;
            }
        }
        $avgRatings = $totalBeliefRatings;
        if($dotValuesCount>0){
            $valuesPerformance = (($avgRatings/$dotValuesCount)/5)*100;
        }
    }



  //(Total feedback count /All active users count of organisation) /(no of month from the day of creation of first staff till now) *500

    $firstStaffOfOrg = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    if($firstStaffOfOrg){
        $firstDateOfOrg  = date('Y-m',strtotime($firstStaffOfOrg->created_at));
        $lastMonthDate   = date('Y-m');

       // $firstDateOfOrg  = "2019-02-10"; // always small
       // $lastMonthDate   = "2019-11-12";



        $time1 = strtotime($firstDateOfOrg);  // always small
        $time2 = strtotime($lastMonthDate);
        if(date('m', $time1)==date('m', $time2) && date('Y', $time1)==date('Y', $time2)){
          $noOfPreviousMonths = 1;
        }else{
          if ($time1 < $time2){
            $my = date('mY', $time2);
            $noOfPreviousMonths = array(date('F', $time1));
            while($time1 < $time2) {
              $time1 = strtotime(date('Y-m-d', $time1).' +1 month');
              if(date('mY', $time1) != $my && ($time1 < $time2))
                $noOfPreviousMonths[] = date('F', $time1);
            }
            $noOfPreviousMonths[] = date('F', $time2);
            //print_r($noOfPreviousMonths);die;
            $noOfPreviousMonths   = count($noOfPreviousMonths);                    
          }else{
            $noOfPreviousMonths = 0;
          } 
        }
    }else{
        $noOfPreviousMonths = 0;
    }


    //echo "D1->".$firstDateOfOrg." <br />D2->".$lastMonthDate." <br />Total ".$noOfPreviousMonths;die;

    //$firstStaffOrg = DB::table('offices')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    //$date1 = new DateTime($firstStaffOrg->created_at);
    //$date2 = $date1->diff(new DateTime(Carbon::today()));
    //$totalMonthcount = ($date2->y*12) + $date2->m + 2;

    //Number of reported incidents and get details of feedback of last month



    //Number of reported incidents and get details of feedback of last month
     $iotFeedbackQuery = DB::table('iot_feedbacks')
        ->select('iot_feedbacks.id')
        ->leftJoin('users','users.id','iot_feedbacks.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('iot_feedbacks.status','!=','Inactive')
        ->where('iot_feedbacks.orgId',$orgId);
       
        if($date){
            $iotFeedbackQuery->whereDate('iot_feedbacks.created_at','<=',$date);
        }

        if($excludeWeekend!=1){
            //Get weekends
            $getWeekendDates = $this->getWeekendDaysFromMonth($date,1);
            //echo '<pre />';print_r($getWeekendDates);die;
            $iotFeedbackQuery->whereNotIn(DB::raw("(DATE_FORMAT(iot_feedbacks.created_at,'%Y-%m-%d'))"),$getWeekendDates);
        }

        if (!empty($officeId) && empty($departmentId)) {
            $iotFeedbackQuery->where('users.officeId',$officeId);
        }elseif (!empty($officeId) && !empty($departmentId)) {
            $iotFeedbackQuery->where('users.officeId',$officeId);
            $iotFeedbackQuery->where('users.departmentId',$departmentId);
        }elseif (empty($officeId) && !empty($departmentId)) {
            $iotFeedbackQuery->where('departments.departmentId',$departmentId);
        } 
        $iotFeedback = $iotFeedbackQuery->count();
        $feedbacks = 0;
        if (!empty($iotFeedback) && !empty($userCount)) {
            if($userCount>0 && $noOfPreviousMonths>0){
                //$feedbacks = round((($iotFeedback/$userCount)/$totalMonthcount*500),2);

                //(Total number of feedback / Number of staff)/number of months client live *500
                //feedbacks = ($iotFeedback/$userCount)/($noOfPreviousMonths*500);
                $feedbacks = (($iotFeedback/$userCount)/($noOfPreviousMonths))*500;
            }
        }
//------------------------------------------------------------------------------------------------------------




    //Direction and connection of tribeometer
     $usersForTribeQuery = DB::table('tribeometer_answers')
        ->leftjoin('users','users.id','tribeometer_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->select('tribeometer_answers.userId')
        ->where('users.status','Active')
        ->where('departments.status','Active') 
        ->where('all_department.status','Active') 
        ->groupBy('tribeometer_answers.userId')
        ->where('users.orgId',$orgId);
    if($date){
        $usersForTribeQuery->whereDate('tribeometer_answers.created_at','<=',$date);
    }
    if (!empty($officeId) && empty($departmentId)) {
        $usersForTribeQuery->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $usersForTribeQuery->where('users.officeId',$officeId);
        $usersForTribeQuery->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $usersForTribeQuery->where('departments.departmentId',$departmentId);
    } 
    $usersForTribe = $usersForTribeQuery->get();
    $userCountForTribe = count($usersForTribe);

    //$tribeometerResultArray = array();
    $queCatTbl    = DB::table('tribeometer_questions_category')->get();
    $tribeDirectionnConnection = 0;
    if (!empty($userCountForTribe)){
        $optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
        $optCount = count($optionsTbl)-1; 

        foreach ($queCatTbl as $value){
            $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();
            $quecount = count($questionTbl);

            $perQuePercen = 0;
            foreach ($questionTbl as  $queValue){
                
                $diaQuery = DB::table('tribeometer_answers')           
                ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
                ->leftjoin('users','users.id','=','tribeometer_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.status','Active') 
                ->where('departments.status','Active') 
                ->where('all_department.status','Active') 
                ->where('tribeometer_answers.orgId',$orgId)
                ->where('tribeometer_questions.id',$queValue->id)
                ->where('tribeometer_questions.category_id',$value->id);

                if($date){
                    $diaQuery->whereDate('tribeometer_answers.created_at','<=',$date);
                }

                if (!empty($officeId) && empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                }elseif (!empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                    $diaQuery->where('users.departmentId',$departmentId);
                }elseif (empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('departments.departmentId',$departmentId);
                } 
                $diaAnsTbl = $diaQuery->sum('answer');

                //avg of all questions
                $perQuePercen += ($diaAnsTbl/$userCountForTribe); 
            }

            $score           = ($perQuePercen/($quecount*$optCount));
            $totalPercentage = (($perQuePercen/($quecount*$optCount))*100)*4;
            $tribeDirectionnConnection += $totalPercentage;
        }
    }

    //Diagnostic core 

    //$diagnosticResultArray = array();
    $diagnosticQueCatTbl    = DB::table('diagnostic_questions_category')
                            ->where('id','!=',5)->get();

    $diagQuery = DB::table('diagnostic_answers')
        ->leftjoin('users','users.id','diagnostic_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->select('diagnostic_answers.userId')
        ->where('users.status','Active')
        ->groupBy('diagnostic_answers.userId')
        ->where('users.orgId',$orgId);
    if($date){
        $diagQuery->whereDate('diagnostic_answers.created_at','<=',$date);
        // $diagQuery->where(DB::raw("(DATE_FORMAT(diagnostic_answers.created_at,'%Y-%m-%d'))"),'<=',$date);
    }

    if (!empty($officeId) && empty($departmentId)) {
        $diagQuery->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $diagQuery->where('users.officeId',$officeId);
        $diagQuery->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $diagQuery->where('departments.departmentId',$departmentId);
    } 
    $diagUsers = $diagQuery->get();
    $diagUserCount = count($diagUsers);
    $totalDiagnosticCore = 0;

    if (!empty($diagUserCount)){
        $diaOptionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
        $diaOptCount    = count($diaOptionsTbl); 

        $diagnosticCorePer = 0;

        foreach ($diagnosticQueCatTbl as $value){
            $diaQuestionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

            $diaQuecount = count($diaQuestionTbl);

            $diaPerQuePercen = 0;
            foreach ($diaQuestionTbl as  $queValue){
                $diaQuery = DB::table('diagnostic_answers')            
                    ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
                    ->leftjoin('users','users.id','=','diagnostic_answers.userId')
                    ->leftJoin('departments','departments.id','users.departmentId')
                    ->leftJoin('all_department','all_department.id','departments.departmentId')
                    ->where('users.status','Active')   
                    ->where('diagnostic_answers.orgId',$orgId)
                    ->where('diagnostic_questions.id',$queValue->id)
                    ->where('diagnostic_questions.category_id',$value->id);
                if($date){
                    $diaQuery->whereDate('diagnostic_answers.created_at','<=',$date);
                    // $diaQuery->where(DB::raw("(DATE_FORMAT(diagnostic_answers.created_at,'%Y-%m-%d'))"),'<=',$date);
                }
                if (!empty($officeId) && empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                }
                elseif (!empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                    $diaQuery->where('users.departmentId',$departmentId);
                }
                elseif (empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('departments.departmentId',$departmentId);
                } 

                $diaAnsTbl = $diaQuery->sum('answer'); 

                $diaPerQuePercen += ($diaAnsTbl/$diagUserCount);               
            }


            //$diaScore = ($diaPerQuePercen/($diaQuecount*$diaOptCount));
            $diaTotalPercentage = ($diaPerQuePercen/($diaQuecount*$diaOptCount))*100;

            // $value1['title']      =  $value->title;
            // $value1['score']      =  number_format((float)$diaScore, 2, '.', '');
            // $value1['percentage'] =  number_format((float)$diaTotalPercentage, 2, '.', '');

            $diagnosticCorePer += $diaTotalPercentage;

            //array_push($diagnosticResultArray, $value1);
        }

        $totalDiagnosticCore = round(($diagnosticCorePer/5),2);
    }

    //stress inverse of diagnostic
   // $diagnosticStressArray = array();
    $diagnosticCatTbl = DB::table('diagnostic_questions_category')->where('id',5)->get();
    $totalDiagnosticStress = 0;
    if (!empty($diagUserCount)) {
        $diagOptionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
        $diagOptCount = count($diagOptionsTbl); 

        $diagnosticStress = 0;

        foreach ($diagnosticCatTbl as $value) {
            $diagQuestionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

            $diagQuecount = count($diagQuestionTbl);

            $diagPerQuePercen = 0;
            foreach ($diagQuestionTbl as  $queValue){
                $diaQuery = DB::table('diagnostic_answers')            
                ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
                ->leftjoin('users','users.id','=','diagnostic_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.status','Active')   
                ->where('diagnostic_answers.orgId',$orgId)
                ->where('diagnostic_questions.id',$queValue->id)
                ->where('diagnostic_questions.category_id',$value->id);
                if($date){
                    $diaQuery->whereDate('diagnostic_answers.created_at','<=',$date);
                    // $diaQuery->where(DB::raw("(DATE_FORMAT(diagnostic_answers.created_at,'%Y-%m-%d'))"),'<=',$date);
                }
                if (!empty($officeId) && empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                }elseif (!empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                    $diaQuery->where('users.departmentId',$departmentId);
                }elseif (empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('departments.departmentId',$departmentId);
                } 

                $diagAnsTbl = $diaQuery->sum('answer'); 

                $diagPerQuePercen += ($diagAnsTbl/$diagUserCount);               
            }


           // $diagScore = ($diagPerQuePercen/($diagQuecount*$diagOptCount));
            $diagTotalPercentage = ($diagPerQuePercen/($diagQuecount*$diagOptCount))*100;

            //$value1['title']      =  $value->title;
            //$value1['score']      =  number_format((float)$diagScore, 2, '.', '');
           // $value1['percentage'] =  number_format((float)$diagTotalPercentage, 2, '.', '');
            $diagnosticStress = (($diagTotalPercentage-100)*(-1))*3;    

            //array_push($diagnosticStressArray, $value1);
        }
        $totalDiagnosticStress = round(($diagnosticStress),2);
    }
//    print_r($totalDiagnosticStress);die();

    $releventCompanyScores = $dotCompleted + $tealRoleMapCompleted + $personalityTypeCompleted + $cultureStructureCompleted + $motivationCompleted + $thumbCompleted + $happyIndexCompleted + $diagnosticAnsCompleted + $tribeometerAnsCompleted + $valuesPerformance + $feedbacks + $tribeDirectionnConnection + $totalDiagnosticCore + $totalDiagnosticStress;

    if (!empty($releventCompanyScores)) {
        $indexOrg = round((($releventCompanyScores/2300)*1000),2);
    }else{
        $indexOrg = 0;
    }


/*
     print_r("Final result => ".$indexOrg."<br>");
     print_r("Dot=>".$dotCompleted."<br>");
     print_r("team role=>".$tealRoleMapCompleted."<br>");
     print_r("PT=>".$personalityTypeCompleted."<br>");
     print_r("CS=>".$cultureStructureCompleted."<br>");
     print_r("motivation=>".$motivationCompleted."<br>");
     print_r("Thumpsup=>".$thumbCompleted."<br>");
     print_r("happy Index=>".$happyIndexCompleted."<br>");
     print_r("Diagnostic Ans=>".$diagnosticAnsCompleted."<br>");
     print_r("Tribeometer Ans=>".$tribeometerAnsCompleted."<br>");
     print_r("Values Performance=>".$valuesPerformance."<br>");
     print_r("feedbacks=>".$feedbacks."<br>");
     print_r("Direction and connection=>".$tribeDirectionnConnection."<br>");
     print_r("Diagnostic core =>".$totalDiagnosticCore."<br>");
     print_r("Diagnostic stress =>".$totalDiagnosticStress);
     die;
*/   

   return $indexOrg;
}


function sumOfIndex($year,$month=false,$getArr = array(),$day=false){
    $orgId          = $getArr["orgId"];
    $officeId       = $getArr["officeId"];
    $departmentId   = $getArr["departmentId"];

    //Get include weekend flag 
    $includeWeekend=DB::table('organisations')->select('include_weekend')->where('id',$orgId)->first();


    $totalIndex=0;
    $indexReportRecords = DB::table('indexReportRecords')
            ->select('*')
            ->where('indexReportRecords.orgId',$orgId);


    if($officeId=='' && $departmentId==''){
        
        $indexReportRecords->whereNull('indexReportRecords.officeId');
        $indexReportRecords->whereNull('indexReportRecords.departmentId');

        if($year!='' && $day!='' && $month!=''){ //Alrady done for weekend
            $indexReportRecords->whereDate('indexReportRecords.date',$year.'-'.$month.'-'.$day);
        }else if($year!='' && $day=='' && $month!=''){
            $indexReportRecords->where('indexReportRecords.date','LIKE',$year.'-'.$month."%");
            if($includeWeekend->include_weekend != 1){ //Exclude Weekend Days
                //Get weekends
                $YearMonth = $year."-".$month;
                $getWeekendDates = $this->getWeekendDaysFromMonth($YearMonth,2);
                //echo '<pre />';print_r($getWeekendDates);die;
                $indexReportRecords->whereNotIn(DB::raw("(DATE_FORMAT(indexReportRecords.date,'%Y-%m-%d'))"),$getWeekendDates);
            }

        }else{
            $indexReportRecords->whereYear('indexReportRecords.date',$year);
            if($includeWeekend->include_weekend != 1){ //Exclude Weekend Days
                //Get weekends
                $YearMonth = $year."-".$month;
                $getWeekendDates = $this->getWeekendDaysFromMonth($year,2);
                //echo '<pre />';print_r($getWeekendDates);die;
                $indexReportRecords->whereNotIn(DB::raw("(DATE_FORMAT(indexReportRecords.date,'%Y-%m-%d'))"),$getWeekendDates);
            }
        }

    }else{
        
        if($year!='' && $day!='' && $month!=''){
            $indexReportRecords->whereDate('indexReportRecords.date',$year.'-'.$month.'-'.$day);
        }else if($year!='' && $day=='' && $month!=''){
            $indexReportRecords->where('indexReportRecords.date','LIKE',$year.'-'.$month."%");
            if($includeWeekend->include_weekend != 1){ //Exclude Weekend Days
                //Get weekends
                $YearMonth = $year."-".$month;
                $getWeekendDates = $this->getWeekendDaysFromMonth($YearMonth,2);
                //echo '<pre />';print_r($getWeekendDates);die;
                $indexReportRecords->whereNotIn(DB::raw("(DATE_FORMAT(indexReportRecords.date,'%Y-%m-%d'))"),$getWeekendDates);
            }

        }else{
            $indexReportRecords->whereYear('indexReportRecords.date',$year);
            if($includeWeekend->include_weekend != 1){ //Exclude Weekend Days
                //Get weekends
                $YearMonth = $year."-".$month;
                $getWeekendDates = $this->getWeekendDaysFromMonth($year,2);
                //echo '<pre />';print_r($getWeekendDates);die;
                $indexReportRecords->whereNotIn(DB::raw("(DATE_FORMAT(indexReportRecords.date,'%Y-%m-%d'))"),$getWeekendDates);
            }
        }

        if($orgId!='' && $officeId!=''){  
            if($departmentId!=''){
                //match with dep tbl id
                $indexReportRecords->where('indexReportRecords.isAllDepId','2');
                $indexReportRecords->where('indexReportRecords.officeId',$officeId);
                $indexReportRecords->where('indexReportRecords.departmentId',$departmentId);
            }else{
                $indexReportRecords->whereNull('indexReportRecords.isAllDepId');
                $indexReportRecords->whereNull('indexReportRecords.departmentId');
                $indexReportRecords->where('indexReportRecords.officeId',$officeId);
            }
        }else if($orgId!='' && $officeId=='' && $departmentId!=''){   //match with all dep tbl id
             $indexReportRecords->where('indexReportRecords.isAllDepId','1');
             $indexReportRecords->where('indexReportRecords.departmentId',$departmentId);
        }
    }        


    $indexRecords = $indexReportRecords->get()->toArray();  
    $c = 0;
    $totalCount = 0;
    if($indexRecords){
        foreach($indexRecords as $indexRecord){
             if($includeWeekend->include_weekend != 1){ //Exclude Weekend Days
                $totalCount = $indexRecord->total_count_exclude_weekend;
             }else{
                $totalCount = $indexRecord->total_count;
             }
             if($year!='' && $day!='' && $month!=''){
                //
             }else{
                if($totalCount > 0){
                    $c++;
                }
             }
            $totalIndex+=$totalCount;
        }
    }

    if($c>0){
        $totalIndex = number_format($totalIndex/$c);    
        return $totalIndex;
    }else{
         return $totalIndex;
    }
    
}




public function show(Request $request,$id){
    $orgId     = base64_decode($id);
    $officeId  = $request->officeId;
    $departmentId = $request->departmentId;

    $org = DB::table('organisations')->select('id','organisation','ImageURL')->where('id',$orgId)->first();

    $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get();

    $dots = DB::table('dots')->where('orgId',$orgId)->first();

    $all_department = DB::table('all_department')->where('status','Active')->get();

    $departments = [];

    if (!empty($officeId)) {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }else{
        if(!empty($departmentId)){
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    //---------------------------------Index Graph-------------------------------------
    //Get Year used
    $yearUsed = DB::table('indexReportRecords')
                ->select(DB::raw('YEAR(date) usingyear'))    
                ->where('orgId',$orgId)
                ->whereNotNull('date')->distinct()
                ->get()->toArray();

    //Get index graph values
    $currentMonth = '';
    $currentYear  = '';
    $graphVal     = '';

    $currentMonth = $request->currentMonth;
    $currentYear  = $request->selectYear;  
  
    //make var
    $orgArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId);


    //Month of Year wise
    if($currentYear!='' && $currentMonth==''){    //Month of Year wise
        $showGraph=1;
        //$month = ['Index','Index'];
        $jan = ['Jan',$this->sumOfIndex($currentYear,'01',$orgArr,'')];  
        $feb = ['Feb',$this->sumOfIndex($currentYear,'02',$orgArr,'')];  
        $mar = ['March',$this->sumOfIndex($currentYear,'03',$orgArr,'')];  
        $apr = ['Apr',$this->sumOfIndex($currentYear,'04',$orgArr,'')];    
        $may = ['May',$this->sumOfIndex($currentYear,'05',$orgArr,'')];   
        $jun = ['Jun',$this->sumOfIndex($currentYear,'06',$orgArr,'')];    
        $jul = ['July',$this->sumOfIndex($currentYear,'07',$orgArr,'')];   
        $aug = ['Aug',$this->sumOfIndex($currentYear,'08',$orgArr,'')];     
        $sep = ['Sep',$this->sumOfIndex($currentYear,'09',$orgArr,'')];    
        $oct = ['Oct',$this->sumOfIndex($currentYear,'10',$orgArr,'')];  
        $nov = ['Nov',$this->sumOfIndex($currentYear,'11',$orgArr,'')];      
        $dec = ['Dec',$this->sumOfIndex($currentYear,'12',$orgArr,'')];  

        if ($currentYear.'-01' <= date('Y-m')){              
            $mainArray  = [$jan];
        }   
        if ($currentYear.'-02' <= date('Y-m')){              
            $mainArray  = [$jan,$feb];
        }  
        if ($currentYear.'-03' <= date('Y-m')){              
            $mainArray  = [$jan,$feb,$mar];
        }
        if ($currentYear.'-04' <= date('Y-m')){            
            $mainArray  = [$jan,$feb,$mar,$apr];
        }  
        if ($currentYear.'-05' <= date('Y-m')){            
            $mainArray  = [$jan,$feb,$mar,$apr,$may];
        }  
        if ($currentYear.'-06' <= date('Y-m')){            
            $mainArray  = [$jan,$feb,$mar,$apr,$may,$jun];
        }   
        if ($currentYear.'-07' <= date('Y-m')){           
            $mainArray  = [$jan,$feb,$mar,$apr,$may,$jun,$jul];
        }  
        if ($currentYear.'-08' <= date('Y-m')){             
            $mainArray  = [$jan,$feb,$mar,$apr,$may,$jun,$jul,$aug];
        }    
        if ($currentYear.'-09' <= date('Y-m')){           
            $mainArray  = [$jan,$feb,$mar,$apr,$may,$jun,$jul,$aug,$sep];
        } 
        if ($currentYear.'-10' <= date('Y-m')){            
            $mainArray  = [$jan,$feb,$mar,$apr,$may,$jun,$jul,$aug,$sep,$oct];
        }           
        if ($currentYear.'-11' <= date('Y-m')){           
            $mainArray  = [$jan,$feb,$mar,$apr,$may,$jun,$jul,$aug,$sep,$oct,$nov];
        }
        if ($currentYear.'-12' <= date('Y-m')){             
            $mainArray  = [$jan,$feb,$mar,$apr,$may,$jun,$jul,$aug,$sep,$oct,$nov,$dec];
        }
        $graphVal = $mainArray;
        //print_r($graphVal);die;
        //$graphVal  = json_encode($mainArray);
        //$graphVal = str_replace('"', "'", $graphVal);
        //echo $graphVal;die;

    }else if($currentYear!='' && $currentMonth!=''){   //Days wise
        $showGraph=2;
        //Days Graph shows
        $no_of_days = date('t',mktime(0,0,0,$currentMonth,1,$currentYear));
        $graphVal=[];
        for ($day = 1; $day <= $no_of_days; $day++) {

            //$rowDate = $currentYear."-".$currentMonth."-".$day;
            if($day>10 || $day==10){
                $rowDate = $currentYear."-".$currentMonth."-".$day;
            }else{
                $rowDate = $currentYear."-".$currentMonth."-0".$day;
            }
            $dateFormat = date('M/d',strtotime($rowDate));            
            // if($day>=10){
            //     $day= $day;
            // }else{
            //     $day = "0".$day;
            // }

            //Get include weekend flag 
            $includeWeekend=DB::table('organisations')->select('include_weekend')->where('id',$orgId)->first();

            if($includeWeekend->include_weekend != 1){ //Exclude Weekend Days
                if ($rowDate < date('Y-m-d')){    
                    $nDate = $currentYear."-".$currentMonth."-".$day;
                    $nameOfDay = date('D', strtotime($nDate));
                    if($nameOfDay=='Sat' || $nameOfDay=='Sun'){
                        //
                    }else{
                        $graphVal[$dateFormat] = $this->sumOfIndex($currentYear,$currentMonth,$orgArr,$day);
                    }
                }
            }else{
                if ($rowDate < date('Y-m-d')){    
                    $graphVal[$dateFormat] = $this->sumOfIndex($currentYear,$currentMonth,$orgArr,$day);
                }
            }
        }
        //print_r($graphVal);die;
    }else{    //Year wise
        $showGraph=3;
        $graphVal=[];
        if($yearUsed){
            foreach($yearUsed as $allyears){
                $year = $allyears->usingyear;
                $year = $year;
                $graphVal[$year] = $this->sumOfIndex($year,'',$orgArr,'');
            }
        }
        //print_r($graphVal);die;   
    }
    //--------------------------------Index Graph Close---------------------------------    

    /*DOT report*/
    $dotValuesArray = array();
    $beliefList     = array();

    if(!empty($dots)){
        //for search
        $searchBeliefId = $request->beliefId;
        session()->put('searchBeliefId', $searchBeliefId);
        $beliefWr = array();
        if($searchBeliefId)
        {
            $beliefWr['id'] = $searchBeliefId;
        }

        $dotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->where($beliefWr)
        ->orderBy('id','ASC')
        ->get();

        $beliefList = DB::table('dots_beliefs')->where('status','Active')->where('dotId',$dots->id)->get();

        $startDate = Input::get('startDate');
        $endDate   = Input::get('endDate');

        foreach ($dotBeliefs as $key => $bValue)
        {

            $dotValues = DB::table('dots_values')
            ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
            ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
            ->where('dots_values.status','Active')
            ->where('dots_values.beliefId',$bValue->id)           
            ->orderBy('dot_value_list.name','ASC')
            ->get();

            $bquery = DB::table('dot_values_ratings')
            ->leftjoin('users','users.id','=','dot_values_ratings.userId')
            ->where('dot_values_ratings.beliefId',$bValue->id)
            ->where('users.status','Active');
            
            $startDate = Input::get('startDate');
            $endDate   = Input::get('endDate');
            //filter acc. to start and end date
            if(!empty($startDate) && !empty($endDate)) 
            {

              $startDate = date_create($startDate);
              $startDateMonth = date_format($startDate,"Y-m-d");
              $endDate = date_create($endDate);

              $endDateYear = date_format($endDate,"Y-m-d");

              $bquery->where('users.created_at', '>=',$startDateMonth);
              $bquery->where('users.created_at', '<=',$endDateYear);

            }
            //belief rating
              $bRatings = $bquery->avg('ratings');
              
              $valuesArray = array();
              foreach ($dotValues as $key => $vValue) 
              {
                $query = DB::table('dot_values_ratings')
                ->leftjoin('users','users.id','=','dot_values_ratings.userId')
                ->where('users.status','Active')
                ->where('valueId', $vValue->id)
                ->where('beliefId',$bValue->id)
                ->where('dotId', $dots->id);

                $startDate = Input::get('startDate');
                $endDate   = Input::get('endDate');

                //filter acc. to start and end date
                if(!empty($startDate) && !empty($endDate)) 
                {

                  $startDate = date_create($startDate);
                  $startDateMonth = date_format($startDate,"Y-m-d");

                  $endDate = date_create($endDate);

                  $endDateYear = date_format($endDate,"Y-m-d");

                  $query->where('users.created_at', '>=',$startDateMonth);
                  $query->where('users.created_at', '<=',$endDateYear);
              }
              
              /*print_r($query->toSql());
              print_r($query->getBindings());
              die();*/
              $vRatings = $query->avg('ratings');

             /* echo " V ";
             print_r($vRatings);*/

             $vResult['valueId']      = $vValue->id;
             $vResult['valueName']    = ucfirst($vValue->name);

             $vResult['valueRatings'] = 0;
             if($vRatings)
             {
                $vResult['valueRatings'] = $vRatings-1;
            }
            array_push($valuesArray, $vResult);
        }
        // die();

        $result['beliefId']      = $bValue->id;
        $result['beliefName']    = ucfirst($bValue->name);           

        $result['beliefRatings'] = 0;
        if($bRatings)
        {
            $result['beliefRatings'] = $bRatings-1;
        }

        $result['beliefValues']  = $valuesArray;

                //filter by office 
        if(!empty($officeId)) {

            $bRatingsQuery = DB::table('dot_values_ratings')
            ->leftjoin('users','users.id','=','dot_values_ratings.userId')
            ->where('users.status','Active')
            ->where('dot_values_ratings.status','Active')
            ->where('dot_values_ratings.beliefId', $bValue->id);
            $bRatingsQuery->where('users.officeId', $officeId);

            $startDate = Input::get('startDate');
            $endDate   = Input::get('endDate');

                    //filter acc. to start and end date
            if(!empty($startDate) && !empty($endDate)) 
            {

              $startDate = date_create($startDate);
              $startDateMonth = date_format($startDate,"Y-m-d");

              $endDate = date_create($endDate);

              $endDateYear = date_format($endDate,"Y-m-d");

              $bRatingsQuery->where('users.created_at', '>=',$startDateMonth);
              $bRatingsQuery->where('users.created_at', '<=',$endDateYear);

          }

          $isofficeRating = $bRatingsQuery->avg('ratings');

          if($isofficeRating)
          {
            array_push($dotValuesArray, $result);
        }
    }else{
        array_push($dotValuesArray, $result);
    }

    }
          //belief for loop

    $startDate = Input::get('startDate');
    $endDate   = Input::get('endDate');

                //filter acc. to start and end date
    if(!empty($startDate) && !empty($endDate)) {
        $startDate = date_create($startDate);
        $startDateMonth = date_format($startDate,"Y-m-d");            
        $endDate = date_create($endDate);
        $endDateYear = date_format($endDate,"Y-m-d");

        $isUser = DB::table('users')
        ->where('orgId',$orgId)
        ->where('status','Active')
        ->where('roleId',3)
        ->where('created_at', '>=', $startDateMonth)           
        ->where('created_at', '<=', $endDateYear)
        ->count();

        if(empty($isUser)){
            $dotValuesArray = array();
        }
    }
}
    


   $indexOrg   = '';
    
    
   
    //for pdf
    if (empty($resquest->pdfStatus)){
        return View('admin/report/manageReport',compact('offices','beliefList','dotValuesArray','org','officeId','indexOrg','all_department','orgId','departments','departmentId','yearUsed','currentYear','currentMonth','graphVal','showGraph'));
    }else{

        return compact('dotValuesArray','org','officeId','indexOrg','orgId','yearUsed','currentYear','currentMonth','graphVal','showGraph');
    }
}


public function dotTabData(Request $request){

    $orgId          = base64_decode($request->orgId);
    $departmentId   = Input::get('departmentId');
    $officeId       = Input::get('officeId');
    $beliefId       = Input::get('beliefId');
    //$officeId  = $request->officeId;
    //$departmentId = $request->departmentId;

    //echo $orgId." = ".$officeId." = ".$officeId." = ".$departmentId;die;

    //Get info of org
    $org = DB::table('organisations')
                ->select('id','organisation','ImageURL','include_weekend','created_at')
                ->where('status','Active')
                ->where('id',$orgId)->first();

    //Get weekend flag
    $excludeWeekend = $org->include_weekend;   //1 for include weekend else not include           

    //Get offices of org            
    $offices = DB::table('offices')
                ->where('status','Active')
                ->where('orgId',$orgId)->get();

    //Get dots of org            
    $dots = DB::table('dots')
            ->where('orgId',$orgId)
            ->where('status','Active')->first();

    //Get all deps        
    $all_department = DB::table('all_department')->where('status','Active')->get();

    $departments = [];
    if (!empty($officeId)){
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')
                            ->where('id',$departmentId)
                            ->where('status','Active')->get();
    }else{
        if(!empty($departmentId)){
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }   


      /*DOT report*/
    $dotValuesArray = array();
    $beliefList     = array();

    if(!empty($dots)){
        //for search
        $searchBeliefId = $beliefId;
        session()->put('searchBeliefId', $searchBeliefId);
        $beliefWr = array();
        if($searchBeliefId){
            $beliefWr['id'] = $searchBeliefId;
        }

        $dotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->where($beliefWr)
        ->orderBy('id','ASC')
        ->get();

        $beliefList = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->get();

        foreach ($dotBeliefs as $key => $bValue){
            $dotValues = DB::table('dots_values')
                ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
                ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
                ->where('dots_values.status','Active')
                ->where('dot_value_list.status','Active')
                ->where('dots_values.beliefId',$bValue->id)           
                ->orderBy('dot_value_list.name','ASC')
                ->get();

            $bquery = DB::table('dot_values_ratings')
                ->leftjoin('users','users.id','dot_values_ratings.userId')
                ->where('dot_values_ratings.beliefId',$bValue->id)
                ->where('users.status','Active')
                ->where('dot_values_ratings.status','Active');
            if (!empty($officeId)) {
                $bquery->where('users.officeId',$officeId);
            }
            if($excludeWeekend!=1){
                $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
                $currentDate    = date('Y-m-d'); 
                //Get weekends
                $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
                $bquery->whereNotIn(DB::raw("(DATE_FORMAT(dot_values_ratings.created_at,'%Y-%m-%d'))"),$getWeekendDates);
            }
            $bRatings = $bquery->avg('ratings');   

            $valuesArray = array();
            foreach ($dotValues as $key => $vValue) {
                $query = DB::table('dot_values_ratings')
                    ->leftjoin('users','users.id','dot_values_ratings.userId')
                    ->where('users.status','Active')
                    ->where('dot_values_ratings.status','Active')
                    ->where('valueId', $vValue->id)
                    ->where('beliefId',$bValue->id)
                    ->where('dotId', $dots->id);
                if (!empty($officeId)) {
                    $query->where('users.officeId',$officeId);
                }
                if($excludeWeekend!=1){
                    $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
                    $currentDate    = date('Y-m-d'); 
                    //Get weekends
                    $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
                    $query->whereNotIn(DB::raw("(DATE_FORMAT(dot_values_ratings.created_at,'%Y-%m-%d'))"),$getWeekendDates);
                }         
                $vRatings = $query->avg('ratings');

                 /* echo " V ";
                 print_r($vRatings);*/

                 $vResult['valueId']      = $vValue->id;
                 $vResult['valueName']    = ucfirst($vValue->name);
                 $vResult['valueRatings'] = 0;
                 if($vRatings){
                    $vResult['valueRatings'] = number_format(($vRatings-1), 2);
                 }
                 array_push($valuesArray, $vResult);
            }

            $result['beliefId']      = $bValue->id;
            $result['beliefName']    = ucfirst($bValue->name); 
            $result['beliefRatings'] = 0;
            if($bRatings){
                $result['beliefRatings'] = number_format(($bRatings-1), 2);
            }
            $result['beliefValues']  = $valuesArray;

            //filter by office 
            // if(!empty($officeId)){
            //     $bRatingsQuery = DB::table('dot_values_ratings')
            //         ->leftjoin('users','users.id','dot_values_ratings.userId')
            //         ->where('users.status','Active')
            //         ->where('dot_values_ratings.status','Active')
            //         ->where('dot_values_ratings.beliefId', $bValue->id);
            //         $bRatingsQuery->where('users.officeId', $officeId);

            //     if($excludeWeekend!=1){
            //         $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
            //         $currentDate    = date('Y-m-d'); 
            //         //Get weekends
            //         $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
            //         //echo '<pre />';print_r($getWeekendDates);die;
            //         $bRatingsQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_values_ratings.created_at,'%Y-%m-%d'))"),$getWeekendDates);
            //     }         

            //    $isofficeRating = $bRatingsQuery->avg('ratings');
            //    if($isofficeRating){
            //      array_push($dotValuesArray, $result);
            //    }
            // }else{
            //     array_push($dotValuesArray, $result);
            // }
            array_push($dotValuesArray, $result);
        }
    }

   
    //for pdf
    if (empty($resquest->pdfStatus)){
        return View('admin/report/report_graph/directingGraph',compact('offices','beliefList','dotValuesArray','org','officeId','indexOrg','all_department','orgId','departments','departmentId'));
    }else{
        return compact('dotValuesArray','org','officeId','indexOrg','orgId');
    }
}





//Index report for DOT completed Values
public function indexReportForDotCompletedValues($perArr = array()){

    $orgId          = $perArr["orgId"];
    $userId         = $perArr["userId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];

    $date = $perArr["getByDate"];

    $query = DB::table('dots_values')  
        ->select('dots_values.id AS id')     
        ->leftjoin('dots_beliefs','dots_beliefs.id','dots_values.beliefId')
        ->leftjoin('dots','dots.id','dots_beliefs.dotId')
        ->where('dots_beliefs.status','Active')
        ->where('dots.status','Active')
        ->where('dots_values.status','Active');
    if($orgId){
        $query->where('dots.orgId', $orgId);
    }
    if($date){
        $query->whereDate('dots_values.created_at','<=',$date);
         // $query->where(DB::raw("(DATE_FORMAT(dots_values.created_at,'%Y-%m-%d'))"),"<=",$date);
    }
    $dotValues = $query->get();
    foreach($dotValues as $value){
        $dvValueQuery = DB::table('dot_values_ratings')
            ->rightjoin('users','users.id','dot_values_ratings.userId')
            ->leftJoin('departments','departments.id','users.departmentId')
            // ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('users.status','Active')
            ->where('departments.status','Active')
            // ->where('all_department.status','Active')
            ->where('dot_values_ratings.userId',$userId)
            ->where('dot_values_ratings.valueId',$value->id)
            ->where('dot_values_ratings.status','Active');
        if($date){
            $dvValueQuery->whereDate('dot_values_ratings.created_at','<=',$date);
            // $dvValueQuery->where(DB::raw("(DATE_FORMAT(dot_values_ratings.created_at,'%Y-%m-%d'))"),"<=",$date);
        }
        if (!empty($officeId) && empty($departmentId)) {
            $dvValueQuery->where('users.officeId',$officeId);
        }
        elseif (!empty($officeId) && !empty($departmentId)) {
            $dvValueQuery->where('users.officeId',$officeId);
            $dvValueQuery->where('users.departmentId',$departmentId);
        }
        elseif (empty($officeId) && !empty($departmentId)) {
            $dvValueQuery->where('departments.departmentId',$departmentId);
        }
        $dvValue = $dvValueQuery->first();
        if(!$dvValue){
            return false;
        }
    }
    //return true;
    if (count($dotValues) == 0) {
      return false;
    }else{
      return true;
    }
    //return true;
}





//Index report for Team Role Completed Values
public function indexReportForTeamroleCompleted($perArr = array()){
    $orgId          = $perArr['orgId'];
    $officeId       = $perArr['officeId'];
    $departmentId   = $perArr['departmentId'];

    $date = $perArr["getByDate"];
    

    $query = DB::table('cot_answers') 
        ->select('cot_answers.userId')
        ->distinct()
        ->rightjoin('users','users.id','cot_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        // ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        // ->where('all_department.status','Active')
        ->where('cot_answers.status','Active');
    if($orgId)
    {
        $query->where('cot_answers.orgId',$orgId);
    }
    if ($date) {
        $query->whereDate('cot_answers.created_at','<=',$date);
        // $query->where(DB::raw("(DATE_FORMAT(cot_answers.created_at,'%Y-%m-%d'))"),"<=",$date);
    }

    if (!empty($officeId) && empty($departmentId)) {
        $query->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
        $query->where('users.officeId',$officeId);
        $query->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }
    // if($orgId)
    // {
    //     $query->where('cot_answers.orgId',$orgId);
    // }
    $result = $query->get();
    return $result;
}

//Index report for Personality Type Completed Values
public function indexReportForpersonalityTypeCompleted($perArr = array()){

    $orgId          = $perArr['orgId'];
    $officeId       = $perArr['officeId'];
    $departmentId   = $perArr['departmentId'];

    $date = $perArr["getByDate"];

    $query = DB::table('cot_functional_lens_answers')
        ->select('cot_functional_lens_answers.userId')
        ->distinct()
        ->rightjoin('users','users.id','cot_functional_lens_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        // ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        // ->where('all_department.status','Active')
        ->where('cot_functional_lens_answers.status','Active');
        //->where('cot_functional_lens_answers.orgId',$orgId);

    if($orgId)
    {
        $query->where('cot_functional_lens_answers.orgId',$orgId);
    }
    if($date){
        $query->whereDate('cot_functional_lens_answers.created_at','<=',$date);
        // $query->where(DB::raw("(DATE_FORMAT(cot_functional_lens_answers.created_at,'%Y-%m-%d'))"),"<=",$date);
    }
    if(!empty($officeId) && empty($departmentId)) {
        $query->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
        $query->where('users.officeId',$officeId);
        $query->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }
    // if($orgId)
    // {
    //     $query->where('cot_functional_lens_answers.orgId',$orgId);
    // }
    $result = $query->get();
    return $result;
}

//Index report for Culture structure Completed
public function indexReportForCultureStructureCompleted($perArr = array()){
    $orgId          = $perArr['orgId'];
    $officeId       = $perArr['officeId'];
    $departmentId   = $perArr['departmentId'];
    $date           = $perArr["getByDate"];   //Always come 
    $excludeWeekend = $perArr["excludeWeekend"];  
    //$date='2019-11-08';
    if($date){
        //Get Previos month of date
        $timestamp = strtotime ("-1 month",strtotime ($date));
        //Convert into 'Y-m' format
        $previosMonthFromDate  =  date("Y-m",$timestamp);   //Y-m

        $query = DB::table('sot_answers')   
            ->select('sot_answers.userId')
            ->distinct()      
            ->leftJoin('users','users.id','=','sot_answers.userId')
            ->leftJoin('departments','departments.id','users.departmentId')
            // ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('sot_answers.status','Active')
            ->where('users.status','Active')
            ->where('departments.status','Active')
            // ->where('all_department.status','Active')
            ->where('sot_answers.updated_at','LIKE',$previosMonthFromDate."%");
            if (!empty($orgId)) {
                $query->where('users.orgId',$orgId);
            }
            
            if($excludeWeekend!=1){
                //Get weekends
                $getWeekendDates = $this->getWeekendDaysFromMonth($previosMonthFromDate,2);
                //echo '<pre />';print_r($getWeekendDates);die;
                $query->whereNotIn(DB::raw("(DATE_FORMAT(sot_answers.updated_at,'%Y-%m-%d'))"),$getWeekendDates);
            }
            
    }else{
        //Get last month
        $lastMonth = date("Y-m", strtotime("-1 month"));  //Y-m
        //echo $lastMonth;die('ddd');
        $query = DB::table('sot_answers')   
        ->select('sot_answers.userId')
        ->distinct()      
        ->leftJoin('users','users.id','=','sot_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        // ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('sot_answers.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        // ->where('all_department.status','Active')
        ->where('sot_answers.updated_at','LIKE',$lastMonth."%");
        if (!empty($orgId)) {
            $query->where('users.orgId',$orgId);
        }
        if($excludeWeekend!=1){
            //Get weekends
            $getWeekendDates = $this->getWeekendDaysFromMonth($lastMonth,2);
            //echo '<pre />';print_r($getWeekendDates);die;
            $query->whereNotIn(DB::raw("(DATE_FORMAT(sot_answers.updated_at,'%Y-%m-%d'))"),$getWeekendDates);
        }
    }

    if (!empty($officeId) && empty($departmentId)) {
        $query->where('users.officeId',$officeId);
    }
    elseif (!empty($officeId) && !empty($departmentId)) {
        $query->where('users.officeId',$officeId);
        $query->where('users.departmentId',$departmentId);
    }
    elseif (empty($officeId) && !empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }  
    // if($orgId)
    // {
    //     $query->where('users.orgId',$orgId);
    // }
    $result = $query->get();
    //print_r($result);die;
    return $result;
}


function getWeekendDaysFromMonth($YearOrMonth,$isSingleDate){
    $weekendDays = array();
    if($isSingleDate==1){
        //Get Day of date
        $nameOfDay = date('D', strtotime($YearOrMonth));
        if($nameOfDay=='Sat' || $nameOfDay=='Sun'){
            $weekendDays[]=$YearOrMonth;
        }
    }else{


        if($YearOrMonth){
            $YearOrMonthExp = explode("-",$YearOrMonth);
            if($YearOrMonthExp){
                $year  = $YearOrMonthExp[0];
                if(isset($YearOrMonthExp[1]) && $YearOrMonthExp[1]!=''){   //Month wise
                    $month     = $YearOrMonthExp[1];
                    $totalDays = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                    for($day=1; $day<=$totalDays; $day++){
                        if($day<=9){
                            $day="0".$day;
                        }
                        $date = $year."-".$month."-".$day;
                        //Get Day of date
                        $nameOfDay = date('D', strtotime($date));
                        if($nameOfDay=='Sat' || $nameOfDay=='Sun'){
                            $weekendDays[]=$date;
                        }
                    }
                }else{  //Year Wise
                    for($nMonth=1; $nMonth<=12; $nMonth++){
                        if($year."-".$nMonth <= date('Y-m')){  
                            $totalDays = cal_days_in_month(CAL_GREGORIAN,$nMonth,$year);
                            for($day=1; $day<=$totalDays; $day++){
                                if($day<=9){
                                    $day="0".$day;
                                }
                                if($nMonth<=9){
                                    $nMonth1="0".$nMonth;
                                }
                                $date = $year."-".$nMonth."-".$day;
                                $date1 = $year."-".$nMonth1."-".$day;
                                //Get Day of date
                                $nameOfDay = date('D', strtotime($date));
                                if($nameOfDay=='Sat' || $nameOfDay=='Sun'){
                                    $weekendDays[]=$date1;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return $weekendDays;
}

//Index report for motivation Completed
public function indexReportForMotivationCompleted($perArr = array()){
    $orgId    = $perArr['orgId'];
    $officeId = $perArr['officeId'];
    $departmentId = $perArr['departmentId'];


    $date = $perArr["getByDate"];
    //$date='2019-01-10';

    $query = DB::table('sot_motivation_answers')
        ->select('sot_motivation_answers.userId')
        ->distinct()      
        ->leftJoin('users','users.id','=','sot_motivation_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        // ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('sot_motivation_answers.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active');
        // ->where('all_department.status','Active');
    if($orgId)
    {
        $query->where('users.orgId',$orgId);
    }
    if($date){
        $query->whereDate('sot_motivation_answers.created_at','<=',$date);
        // $query->where(DB::raw("(DATE_FORMAT(sot_motivation_answers.created_at,'%Y-%m-%d'))"),"<=",$date);
    }
    if (!empty($officeId) && empty($departmentId)) {
        $query->where('users.officeId',$officeId);
    }
    elseif (!empty($officeId) && !empty($departmentId)) {
        $query->where('users.officeId',$officeId);
        $query->where('users.departmentId',$departmentId);
    }
    elseif (empty($officeId) && !empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }  
    // if($orgId)
    // {
    //     $query->where('users.orgId',$orgId);
    // }
    $result = $query->get();
    return $result;
}

//Index report for Thumbsup Completed
public function indexReportForThumbsupCompleted($perArr = array()){
    $orgId          = $perArr['orgId'];
    $officeId       = $perArr['officeId'];
    $departmentId   = $perArr['departmentId'];

    $date   = $perArr["getByDate"];
    //$date='2019-01-10';

    //echo Carbon::yesterday();die;
    //echo Carbon::yesterday()->format('Y-m-d');die;
    if($date){
        $query = DB::table('dot_bubble_rating_records')
        ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
        ->leftjoin('users','users.id','dot_bubble_rating_records.to_bubble_user_id')
        ->leftJoin('departments','departments.id','users.departmentId')
        // ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('dots.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        // ->where('all_department.status','Active')
        ->where('dot_bubble_rating_records.status','Active')
        ->whereDate('dot_bubble_rating_records.created_at',$date);
        // ->where(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$date);
        //->whereDate('dot_bubble_rating_records.updated_at',Carbon::yesterday());                 
        // ->whereDate('dot_bubble_rating_records.created_at',Carbon::yesterday());                 
    }else{
         $query = DB::table('dot_bubble_rating_records')
        ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
        ->leftjoin('users','users.id','dot_bubble_rating_records.to_bubble_user_id')
        ->leftJoin('departments','departments.id','users.departmentId')
        // ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('dots.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        // ->where('all_department.status','Active')
        ->where('dot_bubble_rating_records.status','Active')
        ->whereDate('dot_bubble_rating_records.created_at',Carbon::today());
        // ->where(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),Carbon::today()->format('Y-m-d'));
        //->where(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.updated_at,'%Y-%m-%d'))"),Carbon::yesterday()->format('Y-m-d'));
        //->whereDate('dot_bubble_rating_records.updated_at',Carbon::yesterday());                 
        // ->whereDate('dot_bubble_rating_records.created_at',Carbon::yesterday());  
    }
    if(!empty($orgId)){
        $query->where('dots.orgId',$orgId);
    }
    if (!empty($officeId) && empty($departmentId)) {
        $query->where('users.officeId',$officeId);
    }
    elseif (!empty($officeId) && !empty($departmentId)) {
        $query->where('users.officeId',$officeId);
        $query->where('users.departmentId',$departmentId);
    }
    elseif (empty($officeId) && !empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }  
    $bubbleCount = $query->count('bubble_flag');
    return $bubbleCount;
}

//Index report for happy index Completed
public function indexReportForHappyIndexCompleted($perArr = array()){
    $orgId          = $perArr['orgId'];
    $officeId       = $perArr['officeId'];
    $departmentId   = $perArr['departmentId'];
    // $userTypeArr    = array('3','2','1');  //3 for happy and 2 for avrage and 1 for sad
    $userTypeArr = DB::table('happy_index_mood_values')->where('status','Active')->where('id',3)->get();

    $totalIndexes = 0;
    $totalUser    = 0;

    $date = $perArr["getByDate"];

    $totalUsersQuery = DB::table('users')
        ->where('users.status','Active')
        ->whereDate('users.created_at','<=',$date)
        ->where('users.roleId',3);
    if(!empty($orgId)){
        $totalUsersQuery->where('users.orgId',$orgId);
    } 
    if(!empty($officeId) && empty($departmentId)){
        $totalUsersQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)){
        $totalUsersQuery->where('users.officeId',$officeId);
        $totalUsersQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)){
        $totalUsersQuery->leftJoin('departments','departments.id','users.departmentId')
            ->where('departments.status','Active')
            ->where('departments.departmentId',$departmentId);
    }
    $totalUsers = $totalUsersQuery->count();

    foreach($userTypeArr as $moodVal){
        $moodCountQuery = DB::table('happy_indexes')
            ->leftjoin('users','users.id','happy_indexes.userId')
            ->where('users.status','Active')
            ->where('happy_indexes.status','Active')
            ->where('happy_indexes.moodValue',$moodVal->id);
        if ($date) {
            $moodCountQuery->whereDate('happy_indexes.created_at',$date);
        }else{
            $moodCountQuery->whereDate('happy_indexes.created_at',Carbon::today());  
        }
        if(!empty($orgId)){
            $moodCountQuery->where('users.orgId',$orgId);
        }
        if(!empty($officeId) && empty($departmentId)){
            $moodCountQuery->where('users.officeId',$officeId);
        }elseif(!empty($officeId) && !empty($departmentId)){
            $moodCountQuery->where('users.officeId',$officeId);
            $moodCountQuery->where('users.departmentId',$departmentId);
        }elseif(empty($officeId) && !empty($departmentId)){
            $moodCountQuery->leftJoin('departments','departments.id','users.departmentId')
                ->where('departments.status','Active')
                ->where('departments.departmentId',$departmentId);
        }
        $moodCount = $moodCountQuery->count();

        $happyIndexcount = 0;
        if (!empty($moodCount) && !empty($totalUsers)) {
            $happyIndexcount = round((($moodCount/$totalUsers)*100),2);
        }
    }

    
    //     if($date){
    //          $happyUserCountQuery = DB::table('happy_indexes')
    //         ->leftjoin('users','users.id','happy_indexes.userId')
    //         ->leftJoin('departments','departments.id','users.departmentId')
    //         // ->leftJoin('all_department','all_department.id','departments.departmentId')
    //         ->where('happy_indexes.status','Active')
    //         ->where('happy_indexes.moodValue',$type)
    //         ->where('departments.status','Active')
    //         // ->where('all_department.status','Active')
    //         ->whereDate('happy_indexes.created_at',$date);
    //         // ->where(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$date);
    //         //->where(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),Carbon::yesterday()->format('Y-m-d'));
    //         //->whereDate('happy_indexes.created_at', Carbon::yesterday());
    //     }else{
    //         $happyUserCountQuery = DB::table('happy_indexes')
    //         ->leftjoin('users','users.id','happy_indexes.userId')
    //         ->leftJoin('departments','departments.id','users.departmentId')
    //         // ->leftJoin('all_department','all_department.id','departments.departmentId')
    //         ->where('happy_indexes.status','Active')
    //         ->where('happy_indexes.moodValue',$type)
    //         ->where('departments.status','Active')
    //         // ->where('all_department.status','Active')
    //         ->whereDate('happy_indexes.created_at',Carbon::today());  
    //         // ->where(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),Carbon::today()->format('Y-m-d'));  
    //         //->where(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),Carbon::yesterday()->format('Y-m-d'));
    //         //->whereDate('happy_indexes.created_at', Carbon::yesterday());
    //     }
    //     if(!empty($orgId)){
    //         $happyUserCountQuery->where('users.orgId',$orgId);
    //     }
    //     if (!empty($officeId) && empty($departmentId)) {
    //         $happyUserCountQuery->where('users.officeId',$officeId);
    //     }elseif (!empty($officeId) && !empty($departmentId)) {
    //         $happyUserCountQuery->where('users.officeId',$officeId);
    //         $happyUserCountQuery->where('users.departmentId',$departmentId);
    //     }elseif (empty($officeId) && !empty($departmentId)) {
    //         $happyUserCountQuery->where('departments.departmentId',$departmentId);
    //     }  

    //     $happyUserCount = $happyUserCountQuery->count();
    //     $totalUser +=$happyUserCount;
    //     if($type==3){
    //         $totalIndexes += $happyUserCount;
    //     }       
    // 

    // $happyIndexcount = 0;
    // if($totalIndexes !=0 && $totalUser !=0){ 
    //   $happyIndexcount = (($totalIndexes/($totalUser))*100);
    // }
    return $happyIndexcount;
}


/*get new graph data for Tribeometer*/
public function getReportTribeometerGraph(Request $request){

    $tribeometerResultArray = array();

    $orgId          = base64_decode($request->orgId); 
    $departmentId   = Input::get('departmentId');
    $queCatTbl      = DB::table('tribeometer_questions_category')->get();
    $offices        = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
    $officeId       = Input::get('officeId');
    $all_department = DB::table('all_department')->where('status','Active')->get();
    $flag           = 0 ;
    $departments    = [];

     //Get info of org
    $org = DB::table('organisations')
                ->select('include_weekend','created_at')
                ->where('id',$orgId)->first();

    //Get weekend flag
    $excludeWeekend = $org->include_weekend;   //1 for include weekend else not include           


    if (!empty($officeId)){
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }else{
        if(!empty($departmentId)){
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    $query = DB::table('tribeometer_answers')
    ->leftjoin('users','users.id','tribeometer_answers.userId')
    ->select('tribeometer_answers.userId')
    ->where('users.status','Active')
    ->groupBy('tribeometer_answers.userId')
    ->where('users.orgId',$orgId);

    if($excludeWeekend!=1){
        $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
        $currentDate    = date('Y-m-d'); 
        //Get weekends
        $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
        //echo '<pre />';print_r($getWeekendDates);die;
        $query->whereNotIn(DB::raw("(DATE_FORMAT(tribeometer_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }

    if(!empty($officeId)){
        $query->where('users.officeId',$officeId);
    }

    if(!empty($departmentId)){   
        $i = 0;

        if($departmentsNew->isEmpty()){
            $flag = 1;
        }

        foreach ($departmentsNew as $departments1){       
            if($i == 0){
                $query->where('users.departmentId',$departments1->id);
            }else{
                $query->orWhere('users.departmentId',$departments1->id);
            }
            $i++;
        }
    }

    // print_r($query->toSql());
    // print_r($query->getBindings());
    // die();
    $users = $query->get();
    $userCount = count($users);

    // reset if department is not in organisation
    if($flag==1){
        $userCount = 0;
    }

    if(!empty($userCount)){
        $optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
        $optCount = count($optionsTbl)-1; 

        foreach ($queCatTbl as $value){
            $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();

            $quecount = count($questionTbl);

            $perQuePercen = 0;
            foreach ($questionTbl as  $queValue){
                $diaQuery = DB::table('tribeometer_answers')           
                ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
                ->leftjoin('users','users.id','=','tribeometer_answers.userId')
                ->where('users.status','Active') 
                ->where('tribeometer_answers.orgId',$orgId)
                ->where('tribeometer_questions.id',$queValue->id)
                ->where('tribeometer_questions.category_id',$value->id);

                if($excludeWeekend!=1){
                    $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
                    $currentDate    = date('Y-m-d'); 
                    //Get weekends
                    $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
                    //echo '<pre />';print_r($getWeekendDates);die;
                    $diaQuery->whereNotIn(DB::raw("(DATE_FORMAT(tribeometer_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
                }

                if (!empty($officeId)){
                    $diaQuery->where('users.officeId',$officeId);                    
                }

                if(!empty($departmentId)){   
                    if($departmentsNew->isEmpty()){
                        $flag = 1;
                    }

                    $department3 = array();
                    foreach ($departmentsNew as $departments1){       
                        $departments2 = $departments1->id;
                        array_push($department3, $departments2);

                    }
                    $diaQuery->whereIn('users.departmentId',$department3);
                }

                $diaAnsTbl = $diaQuery->sum('answer');

                //avg of all questions
                $perQuePercen += ($diaAnsTbl/$userCount); 
            }

            $score = ($perQuePercen/($quecount*$optCount));
            $totalPercentage = ($perQuePercen/($quecount*$optCount))*100;


            $value1['title']      =  $value->title;
            $value1['score']      =  number_format((float)$score, 2, '.', '');
            $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');            

            array_push($tribeometerResultArray, $value1);
        }
    }


    if (empty($request->pdfStatus)){
        return view('admin/report/report_graph/reportTribeometerGraph',compact('tribeometerResultArray','offices','orgId','officeId','departmentId','departments','all_department'));
    } else {
        return compact('tribeometerResultArray');
    }
}



/*get doagnostic report graph*/
public function getReportDiagnosticGraph(Request $request)
{
    $diagnosticResultArray = array();

    $orgId        = base64_decode($request->orgId); 
    $departmentId = Input::get('departmentId');
    $officeId     = Input::get('officeId');
    $year     = Input::get('year');
    $chartId     = Input::get('chartId');

    if (empty($year)) {
        if (date('m') == 1) {
            $year = date('Y',strtotime("-1 year"));
        }
        $year = date('Y');
    }

    $offices      = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
    $all_department = DB::table('all_department')->where('status','Active')->get();
    
    $departments = [];
    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();
    }

    $organisation = DB::table('organisations')->where('id',$orgId)->first();

    $getYearsQuery = DB::table('diagnostic_report_graph')
        ->select(DB::raw('YEAR(date) year'))
        ->where('orgId',$orgId)
        ->where('status','Active');
    // if(empty($officeId) && empty($departmentId))
    // {
    //     $getYearsQuery->whereNull('officeId');
    //     $getYearsQuery->whereNull('departmentId');
    // }
    // elseif(!empty($officeId) && empty($departmentId))
    // {
    //     $getYearsQuery->where('officeId',$officeId);
    //     $getYearsQuery->whereNull('departmentId');
    // }
    // elseif(!empty($officeId) && !empty($departmentId))
    // {
    //     $getYearsQuery->where('officeId',$officeId);
    //     $getYearsQuery->where('departmentId',$departmentId);
    // }
    // elseif(empty($officeId) && !empty($departmentId))
    // {
    //     $getYearsQuery->whereNull('officeId');
    //     $getYearsQuery->where('departmentId',$departmentId);
    // }
    $getYears = $getYearsQuery->groupBy('year')->get();

    $diagnosticQueCatTbl = DB::table('diagnostic_questions_category')->get();

    if ($year == date('Y')) {

        if (date('m') == 1) {
            //$months = array('Jan');
            $months =  array(); 
        }elseif (date('m') == 2) {
            //$months = array('Jan','Feb');    
            $months =  array(1=>"Jan ".$year); 
        }elseif (date('m') == 3) {
            //$months = array('Jan','Feb','Mar');
            $months =  array(1=>"Jan ".$year,2=>"Feb ".$year); 
        }elseif (date('m') == 4) {
            //$months = array('Jan','Feb','Mar','Apr');
            $months =  array(1=>"Jan ".$year,2=>"Feb ".$year,3=>"Mar ".$year); 
        }elseif (date('m') == 5) {
            //$months = array('Jan','Feb','Mar','Apr','May');
            $months =  array(1=>"Jan ".$year,2=>"Feb ".$year,3=>"Mar ".$year,4=>"Apr ".$year); 
        }elseif (date('m') == 6) {
            //$months = array('Jan','Feb','Mar','Apr','May','June');
            $months =  array(1=>"Jan ".$year,2=>"Feb ".$year,3=>"Mar ".$year,4=>"Apr ".$year,5=>"May ".$year); 
        }elseif (date('m') == 7) {
            //$months = array('Jan','Feb','Mar','Apr','May','June','July');
            $months =  array(1=>"Jan ".$year,2=>"Feb ".$year,3=>"Mar ".$year,4=>"Apr ".$year,5=>"May ".$year,6=>"Jun ".$year); 
        }elseif (date('m') == 8) {
            //$months = array('Jan','Feb','Mar','Apr','May','June','July','Aug');
            $months =  array(1=>"Jan ".$year,2=>"Feb ".$year,3=>"Mar ".$year,4=>"Apr ".$year,5=>"May ".$year,6=>"Jun ".$year,7=>"Jul ".$year); 
        }elseif (date('m') == 9) {
            //$months = array('Jan','Feb','Mar','Apr','May','June','July','Aug','Sep');
            $months =  array(1=>"Jan ".$year,2=>"Feb ".$year,3=>"Mar ".$year,4=>"Apr ".$year,5=>"May ".$year,6=>"Jun ".$year,7=>"Jul ".$year,8=>"Aug ".$year); 
        }elseif (date('m') == 10) {
            //$months = array('Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct');
            $months =  array(1=>"Jan ".$year,2=>"Feb ".$year,3=>"Mar ".$year,4=>"Apr ".$year,5=>"May ".$year,6=>"Jun ".$year,7=>"Jul ".$year,8=>"Aug ".$year,9=>"Sep ".$year); 
        }elseif (date('m') == 11) {
            //$months = array('Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov');
            $months =  array(1=>"Jan ".$year,2=>"Feb ".$year,3=>"Mar ".$year,4=>"Apr ".$year,5=>"May ".$year,6=>"Jun ".$year,7=>"Jul ".$year,8=>"Aug ".$year,9=>"Sep ".$year,10=>"Oct ".$year); 
        }elseif (date('m') == 12) {
            //$months = array('Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec');
            $months =  array(1=>"Jan ".$year,2=>"Feb ".$year,3=>"Mar ".$year,4=>"Apr ".$year,5=>"May ".$year,6=>"Jun ".$year,7=>"Jul ".$year,8=>"Aug ".$year,9=>"Sep ".$year,10=>"Oct ".$year,11=>"Nov ".$year); 
        }
    }else{
        // $months = array('Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec');
        $months =  array(1=>"Jan ".$year,2=>"Feb ".$year,3=>"Mar ".$year,4=>"Apr ".$year,5=>"May ".$year,6=>"Jun ".$year,7=>"Jul ".$year,8=>"Aug ".$year,9=>"Sep ".$year,10=>"Oct ".$year,11=>"Nov ".$year,12=>"Dec ".$year); 
    }

    $diagnosticFinalArray   = array();
    $subDiagFinalArray      = array();

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }
        
    foreach ($diagnosticQueCatTbl as $cate) {

        $diagnosticArray = array();
        foreach ($months as $key => $month) {
            if ($key <=9) {
                $key = "0".$key;
            }
            $diagDataQuery = DB::table('diagnostic_report_graph')
            ->where('orgId',$orgId)
            ->where('categoryId',$cate->id)
            ->where('date','LIKE',$month."%");
            // ->where('date','LIKE',$year."-".$key."%");

            if(empty($officeId) && empty($departmentId))
            {
                $diagDataQuery->whereNull('officeId');
                $diagDataQuery->whereNull('departmentId');
            }
            elseif(!empty($officeId) && empty($departmentId))
            {
                $diagDataQuery->where('officeId',$officeId);
                $diagDataQuery->whereNull('departmentId');
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $diagDataQuery->where('officeId',$officeId);
                $diagDataQuery->where('departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $diagDataQuery->whereNull('officeId');
                $diagDataQuery->where('departmentId',$departmentId);
            }

            $diagData['data'] = $diagDataQuery->first();
            $diagData['monthName'] = $month;            

            array_push($diagnosticArray, $diagData);
        }
        array_push($diagnosticFinalArray, $diagnosticArray);

        $subDiagnosticArray     = array();
        $diagQues = DB::table('diagnostic_questions')->where('category_id',$cate->id)->get();

        foreach ($diagQues as $ques) {
            $sumcount = array(1,2);
            //echo "<pre>";print_r($ques->id);
            $subDiagDataQuery = DB::table('diagnostic_report_subgraph')
                ->leftjoin('diagnostic_questions','diagnostic_report_subgraph.quesId','diagnostic_questions.id')
                ->where('diagnostic_report_subgraph.orgId',$orgId)
                ->where('diagnostic_report_subgraph.categoryId',$cate->id)
                ->where('diagnostic_report_subgraph.quesId',$ques->id)
                ->whereYear('diagnostic_report_subgraph.date',$month);
                // ->whereYear('diagnostic_report_subgraph.date',$year);
            if(empty($officeId) && empty($departmentId))
            {
                $subDiagDataQuery->whereNull('diagnostic_report_subgraph.officeId');
                $subDiagDataQuery->whereNull('diagnostic_report_subgraph.departmentId');
            }
            elseif(!empty($officeId) && empty($departmentId))
            {
                $subDiagDataQuery->where('diagnostic_report_subgraph.officeId',$officeId);
                $subDiagDataQuery->whereNull('diagnostic_report_subgraph.departmentId');
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $subDiagDataQuery->where('diagnostic_report_subgraph.officeId',$officeId);
                $subDiagDataQuery->where('diagnostic_report_subgraph.departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $subDiagDataQuery->whereNull('diagnostic_report_subgraph.officeId');
                $subDiagDataQuery->where('diagnostic_report_subgraph.departmentId',$departmentId);
            }
            foreach ($sumcount as $sc) {
                if ($sc == 1) {
                    if ($organisation->include_weekend == 1) {
                        $subDiagData = $subDiagDataQuery->sum('with_weekend');
                    }
                    elseif ($organisation->include_weekend != 1) {
                        $subDiagData = $subDiagDataQuery->sum('without_weekend');
                    }
                }elseif ($sc == 2) {
                    $subDiagCount = $subDiagDataQuery->count();
                }
            }
            $subDiagFinalData['data'] = 0;
            if (!empty($subDiagData) && !empty($subDiagCount)) {
                $subDiagFinalData['data'] = number_format(($subDiagData/$subDiagCount),2);
            }
            $subDiagFinalData['measure'] = $ques->measure;
            $subDiagFinalData['measure'] = $subDiagFinalData['measure']."(%)";
            array_push($subDiagnosticArray, $subDiagFinalData);
        }
        array_push($subDiagFinalArray, $subDiagnosticArray);
    }
     
        // $subDiagDataQuery = DB::table('diagnostic_report_subgraph')
        //     ->leftjoin('diagnostic_questions','diagnostic_report_subgraph.quesId','diagnostic_questions.id')
        //     ->where('diagnostic_report_subgraph.orgId',$orgId)
        //     ->where('diagnostic_report_subgraph.categoryId',$cate->id);

        // if(empty($officeId) && empty($departmentId))
        // {
        //     $subDiagDataQuery->whereNull('diagnostic_report_subgraph.officeId');
        //     $subDiagDataQuery->whereNull('diagnostic_report_subgraph.departmentId');
        // }
        // elseif(!empty($officeId) && empty($departmentId))
        // {
        //     $subDiagDataQuery->where('diagnostic_report_subgraph.officeId',$officeId);
        //     $subDiagDataQuery->whereNull('diagnostic_report_subgraph.departmentId');
        // }
        // elseif(!empty($officeId) && !empty($departmentId))
        // {
        //     $subDiagDataQuery->where('diagnostic_report_subgraph.officeId',$officeId);
        //     $subDiagDataQuery->where('diagnostic_report_subgraph.departmentId',$departmentId);
        // }
        // elseif(empty($officeId) && !empty($departmentId))
        // {
        //     $subDiagDataQuery->whereNull('diagnostic_report_subgraph.officeId');
        //     $subDiagDataQuery->where('diagnostic_report_subgraph.departmentId',$departmentId);
        // }

        // $subDiagData = $subDiagDataQuery->get();

        // //echo "<pre>";print_r($subDiagData);die;
        // array_push($subDiagnosticArray, $subDiagData);





    if (empty($request->pdfStatus)) {

        return view('admin/report/report_graph/reportDiagnosticGraph',compact('diagnosticResultArray','offices','orgId','officeId','departmentId','departments','all_department','diagnosticQueCatTbl','getYears','year','diagnosticFinalArray','subDiagFinalArray','organisation','chartId'));
    } else {
        return compact('diagnosticResultArray','diagnosticFinalArray','subDiagFinalArray','diagnosticQueCatTbl','organisation');
    }

}
// public function getReportDiagnosticGraph(Request $request)
// {

//     $diagnosticResultArray = array();

//     $orgId        = base64_decode($request->orgId); 
//     $departmentId = Input::get('departmentId');
//     $startDate    = Input::get('startDate');
//     $endDate      = Input::get('endDate');

//     $queCatTbl    = DB::table('diagnostic_questions_category')->get();
//     $offices      = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
//     $officeId     = Input::get('officeId');
//     $all_department = DB::table('all_department')->where('status','Active')->get();
//     $flag = 0 ;
//     $departments = [];
//     if (!empty($officeId)) 
//     {
//         $departments = DB::table('departments')
//         ->select('departments.id','all_department.department')
//         ->leftJoin('all_department','all_department.id','departments.departmentId')
//         ->where('departments.officeId',$officeId) 
//         ->where('departments.status','Active') 
//         ->orderBy('all_department.department','ASC')->get();

//               //for new department
//         $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
//     }
//     else
//     {
//         if(!empty($departmentId))
//         {
//             $departmentsNew = DB::table('departments')
//             ->select('departments.id','all_department.department')
//             ->leftJoin('all_department','all_department.id','departments.departmentId')
//             ->where('departments.orgId',$orgId)
//             ->where('departments.departmentId',$departmentId) 
//             ->where('departments.status','Active') 
//             ->orderBy('all_department.department','ASC')->get();
//         }
//     }


//     $diagnosticQueCatTbl = DB::table('diagnostic_questions_category')->get();

// // $query = DB::table('users')->where('orgId',$orgId)->where('status','Active');

//     $query = DB::table('diagnostic_answers')
//     ->leftjoin('users','users.id','diagnostic_answers.userId')
//     ->select('diagnostic_answers.userId')
//     ->where('users.status','Active')
//     ->groupBy('diagnostic_answers.userId')
//     ->where('users.orgId',$orgId);

//     if(!empty($officeId))
//     {
//         $query->where('users.officeId',$officeId);
//     }

//     //filter acc. to start and end date
//     if(!empty($startDate) && !empty($endDate)) 
//     {

//       $startDate = date_create($startDate);
//       $startDateMonth = date_format($startDate,"Y-m-d");

//       $endDate = date_create($endDate);

//       $endDateYear = date_format($endDate,"Y-m-d");

//       $query->where('users.created_at', '>=',$startDateMonth);
//       $query->where('users.created_at', '<=',$endDateYear);


//   }

//   if(!empty($departmentId))
//   {   
//     $i = 0;

//     if($departmentsNew->isEmpty())
//     {
//         $flag = 1;
//     }
//     foreach ($departmentsNew as $departments1) 
//     {       
//         if($i == 0)
//         {
//             $query->where('users.departmentId',$departments1->id);
//         }
//         else
//         {
//             $query->orWhere('users.departmentId',$departments1->id);
//         }
//         $i++;
//     }
// }


// $users = $query->get();
// $userCount = count($users);

//  // reset if department is not in organisation
// if($flag==1)
// {
//     $userCount = 0;
// }

// if (!empty($userCount))
// {

//     $optionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
//     $diaOptCount = count($optionsTbl); 

//     foreach ($diagnosticQueCatTbl as $value)
//     {
//         $questionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

//         $quecount = count($questionTbl);

//         $perQuePercen = 0;
//         foreach ($questionTbl as  $queValue)
//         {
//             $diaQuery = DB::table('diagnostic_answers')            
//             ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
//             ->leftjoin('users','users.id','=','diagnostic_answers.userId')
//             ->where('users.status','Active')   
//             ->where('diagnostic_answers.orgId',$orgId)
//             ->where('diagnostic_questions.id',$queValue->id)
//             ->where('diagnostic_questions.category_id',$value->id);

//             if (!empty($officeId))
//             {
//                 $diaQuery->where('users.officeId',$officeId);                    
//             }

//             if(!empty($departmentId))
//             {   
//                 $i = 0;

//                 if($departmentsNew->isEmpty())
//                 {
//                     $flag = 1;
//                 }
//                 $department3 = array();
//                 foreach ($departmentsNew as $departments1) 
//                 {       
//                     $departments2 = $departments1->id;
//                     array_push($department3, $departments2);

//                 }

//                 $diaQuery->whereIn('users.departmentId',$department3);
//             }

//             $diaAnsTbl = $diaQuery->sum('answer'); 

//             $perQuePercen += ($diaAnsTbl/$userCount);               
//         }


//         $score = ($perQuePercen/($quecount*$diaOptCount));
//         $totalPercentage = ($perQuePercen/($quecount*$diaOptCount))*100;

//         $value1['title']      =  $value->title;
//         $value1['score']      =  number_format((float)$score, 2, '.', '');
//         $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');            

//         array_push($diagnosticResultArray, $value1);
//     }
// }

// if (empty($request->pdfStatus)) {

//     return view('admin/report/report_graph/reportDiagnosticGraph',compact('diagnosticResultArray','offices','orgId','officeId','departmentId','departments','all_department'));
// } else {
//     return compact('diagnosticResultArray');
// }

// }


public function getDiagnosticIndexReportByPastDate(){
    $year  = '2019';
    $month = '11';  
    $day = '30';
    //Get total days of month and year
    //$totalDays = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    // $totalDays = 20;
    // for ($day = 16; $day <= $totalDays; $day++) {
        $makeDate = $year."-".$month."-".$day;
        $this->getDiagnosticIndexReport($makeDate);
    // }
    echo "Done for :".$month." - ".$year." days ".$day;
    die('<br /> Finally Done.');
}

public function getDiagnosticIndexReport($date=false)
{
    if($date){
        $date = $date;
    }else{
        $date =date('Y-m-d',strtotime('last day of this month'));
    }

    //Get all organization
    $allOrganizations = DB::table('organisations')
                        ->where('status','Active') 
                        ->select('id')
                        ->whereDate('created_at', '<=',$date)
                        ->get();
    if($allOrganizations){
        foreach($allOrganizations as $organization){
            $orgId = $organization->id;

            $indexDiagOrg = $this->calculateDiagnosticGraph(base64_encode($orgId),'','',2,$date);
            $indexDiagOrgExclude = $this->calculateDiagnosticGraph(base64_encode($orgId),'','',1,$date);
            // $indexDiagOrg       = $this->calculateDiagnosticGraph(base64_encode('8'),'','',2);
            // $indexDiagOrgExclude = $this->calculateDiagnosticGraph(base64_encode('8'),'','',1);
            //print_r($indexDiagOrg);die();

            $c=0;
            foreach ($indexDiagOrg as $diagOrg) {
                    //Chk duplicate record for same day
                    $diagOrgRecords = DB::table('diagnostic_report_graph')
                            ->select('id')
                            ->whereDate('date',$date)
                            // ->whereDate('date',date('Y-m-d',strtotime('last day of this month')))
                            ->where('orgId',$orgId)
                            ->where('categoryId',$diagOrg['id'])
                            ->whereNull('officeId')
                            ->whereNull('departmentId')
                            ->first();

                    if($diagOrgRecords){
                        //No need to insert
                    }else{
                        $insertDiagOrgArray = array(
                            'date'             => $date,
                            'orgId'            => $orgId,
                            'categoryId'       => $indexDiagOrg[$c]['id'],
                            'with_weekend'     => $indexDiagOrg[$c]['percentage'],
                            'without_weekend'  => $indexDiagOrgExclude[$c]['percentage'],
                            'created_at'       => date('Y-m-d H:i:s')
                        );
                        $indexDiagOrgRecords = DB::table('diagnostic_report_graph')->insertGetId($insertDiagOrgArray);
                    }    
                $c++;
            }

            $categories = DB::table('diagnostic_questions_category')->select('id')->get();

            foreach ($categories as $cate) {
                $indexSubDiagOrg = $this->calculateDiagnosticSubGraph(base64_encode($orgId),'','',$cate->id,2,$date);
                $indexSubDiagOrgExclude = $this->calculateDiagnosticSubGraph(base64_encode($orgId),'','',$cate->id,1,$date);
                // $indexSubDiagOrg = $this->calculateDiagnosticSubGraph(base64_encode('8'),'','',$cate->id,2);
                // $indexSubDiagOrgExclude = $this->calculateDiagnosticSubGraph(base64_encode('8'),'','',$cate->id,1);
                // print_r(date('Y-m-d',strtotime('last day of this month')));
                // print_r($indexSubDiagOrg);die();

                $l=0;
                foreach ($indexSubDiagOrg as $diagSubOrg) {
                        //Chk duplicate record for same day
                        $subDiagOrgRecords = DB::table('diagnostic_report_subgraph')
                                ->select('id')
                                ->where('orgId',$orgId)
                                ->whereNull('officeId')
                                ->whereNull('departmentId')
                                ->where('categoryId',$diagSubOrg['categoryId'])
                                ->where('quesId',$diagSubOrg['qusId'])
                                ->whereDate('date',$date)
                                ->first();

                        if($subDiagOrgRecords){
                            //No need to insert
                        }else{
                            $insertSubDiagOrgArray = array(
                                'orgId'            => $orgId,
                                'categoryId'       => $indexSubDiagOrg[$l]['categoryId'],
                                'quesId'           => $indexSubDiagOrg[$l]['qusId'],
                                'date'             => $date,
                                'with_weekend'     => $indexSubDiagOrg[$l]['percentage'],
                                'without_weekend'  => $indexSubDiagOrgExclude[$l]['percentage'],
                                'created_at'       => date('Y-m-d H:i:s')
                            );
                            $indexSubDiagOrgRecords = DB::table('diagnostic_report_subgraph')->insertGetId($insertSubDiagOrgArray);
                        }    
                    $l++;
                }
            }

       
            //Get offices of organization
            $offices = DB::table('offices')
                        ->select('id')
                        ->where('status','Active') 
                        ->whereDate('created_at', '<=',$date)
                        ->where('orgId',$orgId)->get();
            if($offices){
                foreach($offices as $office){
                    $officeId = $office->id;
                    $indexDiagOffice = $this->calculateDiagnosticGraph(base64_encode($orgId),$officeId,'',2,$date);
                    $indexDiagOfficeExclude = $this->calculateDiagnosticGraph(base64_encode($orgId),$officeId,'',1,$date);
                    //$indexDiagOffice = $this->calculateDiagnosticGraph(base64_encode('9'),'7','');

                    $a=0;
                    foreach ($indexDiagOffice as $diagOffice) {
                        //Chk duplicate record for same day
                        $diagOfficeRecords = DB::table('diagnostic_report_graph')
                                ->select('id')
                                ->whereDate('date',$date)
                                ->where('orgId',$orgId)
                                ->where('categoryId',$diagOffice['id'])
                                ->where('officeId',$officeId)
                                ->whereNull('departmentId')
                                ->first();

                        if($diagOfficeRecords){
                            //No need to insert
                        }else{
                            $insertDiagOfficeArray = array(
                                'date'             => $date,
                                // 'date'          => date('Y-m-d'),
                                'orgId'         => $orgId,
                                'officeId'      => $officeId,
                                'categoryId'    => $indexDiagOffice[$a]['id'],
                                'with_weekend'     => $indexDiagOffice[$a]['percentage'],
                                'without_weekend'  => $indexDiagOfficeExclude[$a]['percentage'],
                                'created_at'    => date('Y-m-d H:i:s')
                            );
                            $indexDiagOfficeRecords = DB::table('diagnostic_report_graph')->insertGetId($insertDiagOfficeArray);
                        }
                        $a++; 
                    }

                    foreach ($categories as $cate) {
                        $indexSubDiagOffice = $this->calculateDiagnosticSubGraph(base64_encode($orgId),$officeId,'',$cate->id,2,$date);
                        $indexSubDiagOfficeExclude = $this->calculateDiagnosticSubGraph(base64_encode($orgId),$officeId,'',$cate->id,1,$date);
                        // $indexSubDiagOrg = $this->calculateDiagnosticSubGraph(base64_encode('8'),'','',$cate->id,2);
                        // $indexSubDiagOrgExclude = $this->calculateDiagnosticSubGraph(base64_encode('8'),'','',$cate->id,1);
                        // print_r(date('Y-m-d',strtotime('last day of this month')));
                        // print_r($indexSubDiagOffice);die();

                        $m=0;
                        foreach ($indexSubDiagOffice as $diagSubOffice) {
                                //Chk duplicate record for same day
                                $subDiagOfficeRecords = DB::table('diagnostic_report_subgraph')
                                        ->select('id')
                                        ->where('orgId',$orgId)
                                        ->where('officeId',$officeId)
                                        ->whereNull('departmentId')
                                        ->where('categoryId',$diagSubOffice['categoryId'])
                                        ->where('quesId',$diagSubOffice['qusId'])
                                        ->whereDate('date',$date)
                                        ->first();

                                if($subDiagOfficeRecords){
                                    //No need to insert
                                }else{
                                    $insertSubDiagOfficeArray = array(
                                        'orgId'            => $orgId,
                                        'officeId'         => $officeId,
                                        'categoryId'       => $indexSubDiagOffice[$m]['categoryId'],
                                        'quesId'           => $indexSubDiagOffice[$m]['qusId'],
                                        'date'             => $date,
                                        'with_weekend'     => $indexSubDiagOffice[$m]['percentage'],
                                        'without_weekend'  => $indexSubDiagOfficeExclude[$m]['percentage'],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexSubDiagOfficeRecords = DB::table('diagnostic_report_subgraph')->insertGetId($insertSubDiagOfficeArray);
                                }    
                            $m++;
                        }
                    }
            
                    //Get departments
                    $departments = DB::table('departments')
                        //->select('id')
                        ->where('orgId',$orgId)
                        ->where('officeId',$officeId)
                        ->whereDate('created_at', '<=',$date)
                        ->where('status','Active') 
                        ->get();
                    if($departments){
                        foreach ($departments as $department) {
                            $departmentId = $department->id;
                            $allDepartmentId = $department->departmentId;
                           
                            //Get index
                            $indexDiagAllDept = $this->calculateDiagnosticGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            $indexDiagAllDeptExclude = $this->calculateDiagnosticGraph(base64_encode($orgId),'',$allDepartmentId,1,$date);
                            //$indexDiagOffice = $this->calculateDiagnosticGraph(base64_encode('9'),'','6');

                            $b = 0;
                            foreach ($indexDiagAllDept as $diagAllDept) {
                                //Chk duplicate record for same day
                                $diagAllDeptRecords = DB::table('diagnostic_report_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('categoryId',$diagAllDept['id'])
                                        ->whereNull('officeId')
                                        ->where('departmentId',$allDepartmentId)
                                        ->first();

                                if($diagAllDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertDiagAllDeptArray = array(
                                        'date'             => $date,
                                        // 'date'          => date('Y-m-d'),
                                        'orgId'         => $orgId,
                                        'departmentId'  => $allDepartmentId, 
                                        'categoryId'    => $indexDiagAllDept[$b]['id'],
                                        'with_weekend'     => $indexDiagAllDept[$b]['percentage'],
                                        'without_weekend'  => $indexDiagAllDeptExclude[$b]['percentage'],
                                        'created_at'    => date('Y-m-d H:i:s')
                                    );
                                    $indexDiagAllDeptRecords = DB::table('diagnostic_report_graph')->insertGetId($insertDiagAllDeptArray);
                                }
                                $b++;
                            }

                            foreach ($categories as $cate) {
                                $indexSubDiagAllDept = $this->calculateDiagnosticSubGraph(base64_encode($orgId),'',$allDepartmentId,$cate->id,2,$date);
                                $indexSubDiagAllDeptExclude = $this->calculateDiagnosticSubGraph(base64_encode($orgId),'',$allDepartmentId,$cate->id,1,$date);

                                $n=0;
                                foreach ($indexSubDiagAllDept as $diagSubAllDept) {
                                        //Chk duplicate record for same day
                                        $subDiagAllDeptRecords = DB::table('diagnostic_report_subgraph')
                                                ->select('id')
                                                ->where('orgId',$orgId)
                                                ->whereNull('officeId')
                                                ->where('departmentId',$allDepartmentId)
                                                ->where('categoryId',$diagSubAllDept['categoryId'])
                                                ->where('quesId',$diagSubAllDept['qusId'])
                                                ->whereDate('date',$date)
                                                ->first();

                                        if($subDiagAllDeptRecords){
                                            //No need to insert
                                        }else{
                                            $insertSubDiagAllDeptArray = array(
                                                'orgId'            => $orgId,
                                                'departmentId'     => $allDepartmentId,
                                                'categoryId'       => $indexSubDiagAllDept[$n]['categoryId'],
                                                'quesId'           => $indexSubDiagAllDept[$n]['qusId'],
                                                'date'             => $date,
                                                'with_weekend'     => $indexSubDiagAllDept[$n]['percentage'],
                                                'without_weekend'  => $indexSubDiagAllDeptExclude[$n]['percentage'],
                                                'created_at'       => date('Y-m-d H:i:s')
                                            );
                                            $indexSubDiagAllDeptRecords = DB::table('diagnostic_report_subgraph')->insertGetId($insertSubDiagAllDeptArray);
                                        }    
                                    $n++;
                                }
                            }
                            
                            $indexdiagDept = $this->calculateDiagnosticGraph(base64_encode($orgId),$officeId,$departmentId,2,$date);
                            $indexdiagDeptExclude = $this->calculateDiagnosticGraph(base64_encode($orgId),$officeId,$departmentId,1,$date);
                            //$index = $this->calculateDiagnosticGraph(base64_encode('9'),'7','9');

                            $d = 0;
                            foreach ($indexdiagDept as $diagDept) {
                                //Chk duplicate record for same day
                                $diagAllDeptRecords = DB::table('diagnostic_report_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('officeId',$officeId)
                                        ->where('departmentId',$departmentId)
                                        ->where('categoryId',$diagDept['id'])
                                        ->first();

                                if($diagAllDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertDiagAllDeptArray = array(
                                        'date'             => $date,
                                        // 'date'          => date('Y-m-d'),
                                        'orgId'         => $orgId,
                                        'officeId'      => $officeId,    
                                        'departmentId'  => $departmentId, 
                                        'categoryId'    => $indexdiagDept[$d]['id'],
                                        'with_weekend'     => $indexdiagDept[$d]['percentage'],
                                        'without_weekend'  => $indexdiagDeptExclude[$d]['percentage'],
                                        'created_at'    => date('Y-m-d H:i:s')
                                    );
                                    $indexDiagAllDeptRecords = DB::table('diagnostic_report_graph')->insertGetId($insertDiagAllDeptArray);
                                }
                                $d++;
                            }

                            foreach ($categories as $cate) {
                                $indexSubDiagDept = $this->calculateDiagnosticSubGraph(base64_encode($orgId),$officeId,$departmentId,$cate->id,2,$date);
                                $indexSubDiagDeptExclude = $this->calculateDiagnosticSubGraph(base64_encode($orgId),$officeId,$departmentId,$cate->id,1,$date);

                                $o=0;
                                foreach ($indexSubDiagDept as $diagSubDept) {
                                        //Chk duplicate record for same day
                                        $subDiagDeptRecords = DB::table('diagnostic_report_subgraph')
                                                ->select('id')
                                                ->where('orgId',$orgId)
                                                ->where('officeId',$officeId)
                                                ->where('departmentId',$departmentId)
                                                ->where('categoryId',$diagSubDept['categoryId'])
                                                ->where('quesId',$diagSubDept['qusId'])
                                                ->whereDate('date',$date)
                                                ->first();

                                        if($subDiagDeptRecords){
                                            //No need to insert
                                        }else{
                                            $insertSubDiagDeptArray = array(
                                                'orgId'            => $orgId,
                                                'officeId'         => $officeId,
                                                'departmentId'     => $departmentId,
                                                'categoryId'       => $indexSubDiagDept[$o]['categoryId'],
                                                'quesId'           => $indexSubDiagDept[$o]['qusId'],
                                                'date'             => $date,
                                                'with_weekend'     => $indexSubDiagDept[$o]['percentage'],
                                                'without_weekend'  => $indexSubDiagDeptExclude[$o]['percentage'],
                                                'created_at'       => date('Y-m-d H:i:s')
                                            );
                                            $indexSubDiagDeptRecords = DB::table('diagnostic_report_subgraph')->insertGetId($insertSubDiagDeptArray);
                                        }    
                                    $o++;
                                }
                            }
                        }   
                    }
                }
            }
        }
    }

}

public function calculateDiagnosticGraph($orgId,$officeId,$departmentId,$isExclude,$date,$categoryId=false)
{
    $diagnosticResultArray = array();

    $orgId = base64_decode($orgId);
    $departmentId = $departmentId;
    $officeId = $officeId;

    //Get weekends
    $startDate=DB::table('diagnostic_answers')
        ->select('created_at')
        ->where('orgId',$orgId)
        ->whereDate('diagnostic_answers.created_at','<=',$date)
        ->orderBy('created_at','ASC')
        ->first();
    if(isset($startDate->created_at) && $startDate->created_at!=''){
        $startDate = date('Y-m-d',strtotime($startDate->created_at));
    }else{
        $startDate = $date;
    }
    $currentDate = $date;
    //$currentDate = date('Y-m-d');

    $getWeekendDates = $this->getDatesFromRange($startDate, $currentDate);
    
    $query = DB::table('diagnostic_answers')
    ->select('diagnostic_answers.userId')
    ->leftjoin('users','users.id','diagnostic_answers.userId')
    ->where('users.status','Active')
    ->where('diagnostic_answers.status','Active')
    ->groupBy('diagnostic_answers.userId')
    ->where('users.orgId',$orgId);
    if($date){
        $query->whereDate('diagnostic_answers.created_at','<=',$date);
    }
    if($isExclude==1){
        $query->whereNotIn(DB::raw("(DATE_FORMAT(diagnostic_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }

    if (!empty($officeId) && empty($departmentId)) {
        $query->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $query->where('users.officeId',$officeId);
        $query->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $query->leftJoin('departments','departments.id','users.departmentId');
        $query->where('departments.status','Active');
        $query->where('departments.departmentId',$departmentId);
    }
    $usersWithoutWeekend = $query->get();
    $userCountWithoutWeekend = count($usersWithoutWeekend);

    $diagnosticQueCatTblQuery = DB::table('diagnostic_questions_category');
    if (!empty($categoryId)) {
        $diagnosticQueCatTblQuery->where('id',$categoryId);
    }
    $diagnosticQueCatTbl = $diagnosticQueCatTblQuery->get();

    if (!empty($userCountWithoutWeekend))
    {
        $optionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
        $diaOptCount = count($optionsTbl); 
        foreach ($diagnosticQueCatTbl as $value)
        {
            $questionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();
            $quecount = count($questionTbl);

            $perQuePercen = 0;
            foreach ($questionTbl as  $queValue)
            {
                $diaQuery = DB::table('diagnostic_answers')            
                ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
                ->leftjoin('users','users.id','diagnostic_answers.userId')      
                ->where('users.status','Active')
                ->where('diagnostic_answers.status','Active') 
                ->where('diagnostic_questions.status','Active') 
                ->where('diagnostic_answers.orgId',$orgId)
                ->where('diagnostic_questions.id',$queValue->id)
                ->where('diagnostic_questions.category_id',$value->id);
                if($date){
                    $diaQuery->whereDate('diagnostic_answers.created_at','<=',$date);
                }
                if($isExclude==1){
                  $diaQuery->whereNotIn(DB::raw("(DATE_FORMAT(diagnostic_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
                }
                if (!empty($officeId) && empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                }
                elseif (!empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                    $diaQuery->where('users.departmentId',$departmentId);
                }
                elseif (empty($officeId) && !empty($departmentId)) {
                    $diaQuery->leftJoin('departments','departments.id','users.departmentId');
                    $diaQuery->where('departments.status','Active');
                    $diaQuery->where('departments.departmentId',$departmentId);
                }
                $diaAnsTbl = $diaQuery->sum('answer');
                // if (!empty($userCountWithoutWeekend))
                // {
                    $perQuePercen += ($diaAnsTbl/$userCountWithoutWeekend);    
                //}
            }
            $score = ($perQuePercen/($quecount*$diaOptCount));
            $totalPercentage = ($perQuePercen/($quecount*$diaOptCount))*100;

            $value1['id']         =  $value->id;
            $value1['title']      =  $value->title;
            $value1['score']      =  number_format((float)$score, 2, '.', '');
            $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');   
            array_push($diagnosticResultArray, $value1);
        }
    }
    return $diagnosticResultArray;
}

public function calculateDiagnosticSubGraph($orgId,$officeId,$departmentId,$categoryId,$isExclude,$date,$quesId=false)
{
    $diagnosticResultArray = array();

    $orgId = base64_decode($orgId);
    $departmentId = $departmentId;
    $officeId = $officeId;
    $categoryId = $categoryId;

    //Get weekends
    $startDate=DB::table('diagnostic_answers')
        ->select('created_at')
        ->where('orgId',$orgId)
        ->whereDate('diagnostic_answers.created_at','<=',$date)
        ->orderBy('created_at','ASC')
        ->first();
    if (count($startDate) == 0) {
        $startDate = $date;
        //$startDate = date('Y-m-d');
    }
    else
    {
        $startDate = date('Y-m-d',strtotime($startDate->created_at));
    }
    $currentDate = $date;
    //$currentDate = date('Y-m-d');

    $getWeekendDates = $this->getDatesFromRange($startDate, $currentDate);
    
    $query = DB::table('diagnostic_answers')
    ->select('diagnostic_answers.userId')
    ->leftjoin('users','users.id','diagnostic_answers.userId')
    ->where('users.status','Active')
    ->where('diagnostic_answers.status','Active')
    ->groupBy('diagnostic_answers.userId')
    ->where('users.orgId',$orgId);
    if($date){
        $query->whereDate('diagnostic_answers.created_at','<=',$date);
    }
    if($isExclude==1){
        $query->whereNotIn(DB::raw("(DATE_FORMAT(diagnostic_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }
    if (!empty($officeId) && empty($departmentId)) {
        $query->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $query->where('users.officeId',$officeId);
        $query->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $query->leftJoin('departments','departments.id','users.departmentId');
        $query->where('departments.status','Active');
        $query->where('departments.departmentId',$departmentId);
    }
    $usersWithoutWeekend = $query->get();
    $userCountWithoutWeekend = count($usersWithoutWeekend);

    //$diagnosticQueCatTbl = DB::table('diagnostic_questions_category')->get();
    
    $optionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
    $diaOptCount = count($optionsTbl); 
    // foreach ($diagnosticQueCatTbl as $value)
    // {
    
    $questionTblCount = DB::table('diagnostic_questions')->where('category_id',$categoryId)->where('status','Active')->get();
    $quecount = count($questionTblCount);

    $questionTblQuery = DB::table('diagnostic_questions')
        ->where('category_id',$categoryId)
        ->where('status','Active');
    if (!empty($quesId)) {
        $questionTblQuery->where('id',$quesId);
    }
    $questionTbl = $questionTblQuery->get();

    $perQuePercen = 0;
    if (!empty($userCountWithoutWeekend))
    {
        foreach ($questionTbl as  $value)
        {
            $diaQuery = DB::table('diagnostic_answers')            
            ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
            ->leftjoin('users','users.id','diagnostic_answers.userId')      
            ->where('users.status','Active')   
            ->where('diagnostic_answers.orgId',$orgId)
            ->where('diagnostic_questions.id',$value->id)
            ->where('diagnostic_questions.category_id',$categoryId);
            if($date){
                $diaQuery->whereDate('diagnostic_answers.created_at','<=',$date);
            }
            if($isExclude==1){
              $diaQuery->whereNotIn(DB::raw("(DATE_FORMAT(diagnostic_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
            }
            if (!empty($officeId) && empty($departmentId)) {
                $diaQuery->where('users.officeId',$officeId);
            }elseif (!empty($officeId) && !empty($departmentId)) {
                $diaQuery->where('users.officeId',$officeId);
                $diaQuery->where('users.departmentId',$departmentId);
            }elseif (empty($officeId) && !empty($departmentId)) {
                $diaQuery->leftJoin('departments','departments.id','users.departmentId');
                $diaQuery->where('departments.status','Active');
                $diaQuery->where('departments.departmentId',$departmentId);
            }
            $diaAnsTbl = $diaQuery->sum('answer');
            // if (!empty($userCountWithoutWeekend))
                // {
                    $perQuePercen = ($diaAnsTbl/$userCountWithoutWeekend);               
                    //$perQuePercen += ($diaAnsTbl/$userCountWithoutWeekend);    
                //}
            //}
            $score = ($perQuePercen/($quecount*$diaOptCount));
            $totalPercentage = ($perQuePercen/($quecount*$diaOptCount))*100;

            $value1['categoryId'] =  $categoryId;
            $value1['qusId']      =  $value->id;
            $value1['title']      =  ucfirst($value->measure);
            $value1['score']      =  number_format((float)$score, 2, '.', '');
            $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');   
            array_push($diagnosticResultArray, $value1);
        }
    }
    return $diagnosticResultArray;
}

// Function to get all the dates in given range 
function getDatesFromRange($start, $end, $format = 'Y-m-d') { 

    // Declare an empty array 
    $array = array(); 

    // Variable that store the date interval 
    // of period 1 day 
    $interval = new DateInterval('P1D'); 

    $realEnd = new DateTime($end); 
    $realEnd->add($interval); 

    $period = new DatePeriod(new DateTime($start), $interval, $realEnd); 

    // Use loop to store date into array 
    foreach($period as $date) {
        $getDate = $date->format($format);
        $nameOfDay = date('D', strtotime($getDate));
        if($nameOfDay=='Sat' || $nameOfDay=='Sun'){
            $array[] = $date->format($format); 
        }
    } 

    // Return the array elements 
    return $array; 
} 

public function getTribeometerIndexReportByPastDate(){
    $year  = '2019';
    $month = '11';  
    $day = '30';
    //Get total days of month and year
    //$totalDays = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    // $totalDays = 20;
    // for ($day = 16; $day <= $totalDays; $day++) {
        $makeDate = $year."-".$month."-".$day;
        $this->getTribeometerIndexReport($makeDate);
    // }
    echo "Done for :".$month." - ".$year." days ".$day;
    die('<br /> Finally Done.');
}

public function getTribeometerIndexReport($date=false){

    if($date){
        $date = $date;
    }else{
        $date =date('Y-m-d',strtotime('last day of this month'));
    }
    //Get all organization
    $allOrganizations = DB::table('organisations')
                        ->where('status','Active') 
                        ->select('id')
                        ->whereDate('created_at', '<=',$date)
                        ->get();
    if($allOrganizations){
        foreach($allOrganizations as $organization){
            $orgId = $organization->id;

            $indexTribeometerOrg = $this->calculateTribeometerGraph(base64_encode($orgId),'','',2,$date);
            $indexTribeometerOrgExclude = $this->calculateTribeometerGraph(base64_encode($orgId),'','',1,$date);
            // $indexTribeometerOrg = $this->calculateTribeometerGraph(base64_encode(8),'','',2,$date);
            // $indexTribeometerOrgExclude = $this->calculateTribeometerGraph(base64_encode(8),'','',1,$date);

            $tribeoOrgCount=0;
            foreach ($indexTribeometerOrg as $tribeoOrg) {
                //Chk duplicate record for same day
                $tribeoOrgRecords = DB::table('tribeometer_dashboard_graph')
                        ->select('id')
                        ->whereDate('date',$date)
                        ->where('orgId',$orgId)
                        ->where('categoryId',$tribeoOrg['id'])
                        ->whereNull('officeId')
                        ->whereNull('departmentId')
                        ->first();

                if($tribeoOrgRecords){
                    //No need to insert
                }else{
                    $insertTribeoOrgArray = array(
                        'date'             => $date,
                        'orgId'            => $orgId,
                        'categoryId'       => $tribeoOrg['id'],
                        'with_weekend'     => $tribeoOrg['percentage'],
                        'without_weekend'  => $indexTribeometerOrgExclude[$tribeoOrgCount]['percentage'],
                        'created_at'       => date('Y-m-d H:i:s')
                    );
                    $indexTribeoOrgRecords = DB::table('tribeometer_dashboard_graph')->insertGetId($insertTribeoOrgArray);
                }    
                $tribeoOrgCount++;
            }
       
            //Get offices of organization
            $offices = DB::table('offices')
                        ->select('id')
                        ->where('status','Active') 
                        ->whereDate('created_at', '<=',$date)
                        ->where('orgId',$orgId)->get();
            if($offices){
                foreach($offices as $office){
                    $officeId = $office->id;
                    $indexTribeometerOffice = $this->calculateTribeometerGraph(base64_encode($orgId),$officeId,'',2,$date);
                    $indexTribeometerOfficeExclude = $this->calculateTribeometerGraph(base64_encode($orgId),$officeId,'',1,$date);

                    $tribeoOfficeCount=0;
                    foreach ($indexTribeometerOffice as $tribeoOffice) {
                        //Chk duplicate record for same day
                        $tribeoOfficeRecords = DB::table('tribeometer_dashboard_graph')
                                ->select('id')
                                ->whereDate('date',$date)
                                ->where('orgId',$orgId)
                                ->where('categoryId',$tribeoOffice['id'])
                                ->where('officeId',$officeId)
                                ->whereNull('departmentId')
                                ->first();
                        if($tribeoOfficeRecords){
                            //No need to insert
                        }else{
                            $insertTribeoOfficeArray = array(
                                'date'             => $date,
                                'orgId'            => $orgId,
                                'officeId'         => $officeId,
                                'categoryId'       => $tribeoOffice['id'],
                                'with_weekend'     => $tribeoOffice['percentage'],
                                'without_weekend'  => $indexTribeometerOfficeExclude[$tribeoOfficeCount]['percentage'],
                                'created_at'       => date('Y-m-d H:i:s')
                            );
                            $indexTribeoOfficeRecords = DB::table('tribeometer_dashboard_graph')->insertGetId($insertTribeoOfficeArray);
                        }    
                        $tribeoOfficeCount++;
                    }

                    //Get departments
                    $departments = DB::table('departments')
                        //->select('id')
                        ->where('orgId',$orgId)
                        ->where('officeId',$officeId)
                        ->whereDate('created_at', '<=',$date)
                        ->where('status','Active') 
                        ->get();
                    if($departments){
                        foreach ($departments as $department) {
                            $departmentId = $department->id;
                            $allDepartmentId = $department->departmentId;
                           
                            //Get index
                            $indexTribeometerAllDept = $this->calculateTribeometerGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            $indexTribeometerAllDeptExclude = $this->calculateTribeometerGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            
                            $tribeoAllDeptCount=0;
                            foreach ($indexTribeometerAllDept as $tribeoAllDept) {
                                //Chk duplicate record for same day
                                $tribeoAllDeptRecords = DB::table('tribeometer_dashboard_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('categoryId',$tribeoAllDept['id'])
                                        ->whereNull('officeId')
                                        ->where('departmentId',$allDepartmentId)
                                        ->first();
                                if($tribeoAllDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertTribeoAllDeptArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'departmentId'     => $allDepartmentId,
                                        'categoryId'       => $tribeoAllDept['id'],
                                        'with_weekend'     => $tribeoAllDept['percentage'],
                                        'without_weekend'  => $indexTribeometerAllDeptExclude[$tribeoAllDeptCount]['percentage'],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexTribeoAllDeptRecords = DB::table('tribeometer_dashboard_graph')->insertGetId($insertTribeoAllDeptArray);
                                }    
                                $tribeoAllDeptCount++;
                            }

                            $indexTribeometerDept = $this->calculateTribeometerGraph(base64_encode($orgId),$officeId,$departmentId,2,$date);
                            $indexTribeometerDeptExclude = $this->calculateTribeometerGraph(base64_encode($orgId),$officeId,$departmentId,1,$date);

                            $tribeoDeptCount=0;
                            foreach ($indexTribeometerDept as $tribeoDept) {
                                //Chk duplicate record for same day
                                $tribeoDeptRecords = DB::table('tribeometer_dashboard_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('categoryId',$tribeoDept['id'])
                                        ->where('officeId',$officeId)
                                        ->where('departmentId',$departmentId)
                                        ->first();
                                if($tribeoDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertTribeoDeptArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'officeId'         => $officeId,
                                        'departmentId'     => $departmentId,
                                        'categoryId'       => $tribeoDept['id'],
                                        'with_weekend'     => $tribeoDept['percentage'],
                                        'without_weekend'  => $indexTribeometerDeptExclude[$tribeoDeptCount]['percentage'],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexTribeoDeptRecords = DB::table('tribeometer_dashboard_graph')->insertGetId($insertTribeoDeptArray);
                                }    
                                $tribeoDeptCount++;
                            }
                        }   
                    }
                }
            }
        }
    }
}

public function calculateTribeometerGraph($orgId,$officeId,$departmentId,$isExclude,$date,$categoryId=false)
{
    $orgId = base64_decode($orgId);
    $departmentId = $departmentId;
    $officeId = $officeId;

    $queCatTblQuery = DB::table('tribeometer_questions_category');
    if (!empty($categoryId)) {
        $queCatTblQuery->where('id',$categoryId);
    }
    $queCatTbl = $queCatTblQuery->get();

    $tribeometerResultArray = array();

    //Get weekends
    $startDate=DB::table('tribeometer_answers')
        ->select('created_at')
        ->where('orgId',$orgId)
        ->whereDate('tribeometer_answers.created_at','<=',$date)
        ->orderBy('created_at','ASC')
        ->first();
    if(isset($startDate->created_at) && $startDate->created_at!=''){
        $startDate = date('Y-m-d',strtotime($startDate->created_at));
    }else{
        $startDate = $date;
    }
    $currentDate = $date;
    //$currentDate = date('Y-m-d');

    $getWeekendDates = $this->getDatesFromRange($startDate, $currentDate);

    $query = DB::table('tribeometer_answers')
    ->select('tribeometer_answers.userId')
    ->leftjoin('users','users.id','tribeometer_answers.userId')
    ->where('users.status','Active')
    ->where('tribeometer_answers.status','Active')
    ->groupBy('tribeometer_answers.userId')
    ->where('users.orgId',$orgId);
    if($date){
        $query->whereDate('tribeometer_answers.created_at','<=',$date);
    }
    if($isExclude==1){
        $query->whereNotIn(DB::raw("(DATE_FORMAT(tribeometer_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }
    if (!empty($officeId) && empty($departmentId)) {
        $query->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $query->where('users.officeId',$officeId);
        $query->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $query->leftJoin('departments','departments.id','users.departmentId');
        $query->where('departments.status','Active');
        $query->where('departments.departmentId',$departmentId);
    }
    $users = $query->get();
    $userCount = count($users);

    if(!empty($userCount)){
        $optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
        $optCount = count($optionsTbl)-1;

        foreach ($queCatTbl as $value){
            $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();
            $quecount = count($questionTbl);

            $perQuePercen = 0;
            foreach ($questionTbl as  $queValue){
                $diaQuery = DB::table('tribeometer_answers')           
                ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
                ->leftjoin('users','users.id','tribeometer_answers.userId')
                ->where('users.status','Active') 
                ->where('tribeometer_answers.status','Active') 
                ->where('tribeometer_questions.status','Active') 
                ->where('tribeometer_answers.orgId',$orgId)
                ->where('tribeometer_questions.id',$queValue->id)
                ->where('tribeometer_questions.category_id',$value->id);
                if($date){
                    $query->whereDate('tribeometer_answers.created_at','<=',$date);
                }
                if($isExclude==1){
                    $diaQuery->whereNotIn(DB::raw("(DATE_FORMAT(tribeometer_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
                }
                if (!empty($officeId) && empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                }elseif (!empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                    $diaQuery->where('users.departmentId',$departmentId);
                }elseif (empty($officeId) && !empty($departmentId)) {
                    $diaQuery->leftJoin('departments','departments.id','users.departmentId');
                    $diaQuery->where('departments.status','Active');
                    $diaQuery->where('departments.departmentId',$departmentId);
                }
                $diaAnsTbl = $diaQuery->sum('answer');
                //avg of all questions
                $perQuePercen += ($diaAnsTbl/$userCount); 
            }
            $totalPercentage = ($perQuePercen/($quecount*$optCount))*100;
            $value1['id']      =  $value->id;
            $value1['percentage'] =  round($totalPercentage,2);  
            array_push($tribeometerResultArray, $value1);     
        }
    }else{
        foreach ($queCatTbl as $cate) {
            $value['id']      =  $cate->id;
            $value['percentage'] =  0;
            array_push($tribeometerResultArray, $value);   
        }
    }
    return $tribeometerResultArray;
}

public function getCultureStructureIndexReportByPastDate(){
    $year  = '2019';
    $month = '11';  
    $day = '30';
    //Get total days of month and year
    //$totalDays = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    // $totalDays = 20;
    // for ($day = 16; $day <= $totalDays; $day++) {
        $makeDate = $year."-".$month."-".$day;
        $this->getCultureStructureIndexReport($makeDate);
    // }
    echo "Done for :".$month." - ".$year." days ".$day;
    die('<br /> Finally Done.');
}

public function getCultureStructureIndexReport($date=false){

    if($date){
        $date = $date;
    }else{
        $date =date('Y-m-d',strtotime('last day of this month'));
    }
    //Get all organization
    $allOrganizations = DB::table('organisations')
                        ->where('status','Active') 
                        ->select('id')
                        ->whereDate('created_at', '<=',$date)
                        ->get();
    if($allOrganizations){
        foreach($allOrganizations as $organization){
            $orgId = $organization->id;

            $indexCultureStructureOrg = $this->calculateCultureStructureGraph(base64_encode($orgId),'','',2,$date);
            $indexCultureStructureOrgExclude = $this->calculateCultureStructureGraph(base64_encode($orgId),'','',1,$date);
            // $indexCultureStructureOrg = $this->calculateCultureStructureGraph(base64_encode(8),'','',2,$date);
            // $indexCultureStructureOrgExclude = $this->calculateCultureStructureGraph(base64_encode(8),'','',1,$date);

            $cultureStructureOrgCount=0;
            foreach ($indexCultureStructureOrg as $cultureStructureOrg) {
                //Chk duplicate record for same day
                $cultureStructureOrgRecords = DB::table('sot_culture_structure_dashboard_graph')
                        ->select('id')
                        ->whereDate('date',$date)
                        ->where('orgId',$orgId)
                        ->where('categoryId',$cultureStructureOrg['id'])
                        ->whereNull('officeId')
                        ->whereNull('departmentId')
                        ->first();

                if($cultureStructureOrgRecords){
                    //No need to insert
                }else{
                    $insertCultureStructureOrgArray = array(
                        'date'             => $date,
                        'orgId'            => $orgId,
                        'categoryId'       => $cultureStructureOrg['id'],
                        'with_weekend'     => $cultureStructureOrg['percentage'],
                        'without_weekend'  => $indexCultureStructureOrgExclude[$cultureStructureOrgCount]['percentage'],
                        'created_at'       => date('Y-m-d H:i:s')
                    );
                    $indexCultureStructureOrgRecords = DB::table('sot_culture_structure_dashboard_graph')->insertGetId($insertCultureStructureOrgArray);
                }    
                $cultureStructureOrgCount++;
            }
       
            //Get offices of organization
            $offices = DB::table('offices')
                        ->select('id')
                        ->where('orgId',$orgId)
                        ->where('status','Active') 
                        ->whereDate('created_at', '<=',$date)
                        ->get();
            if($offices){
                foreach($offices as $office){
                    $officeId = $office->id;
                    $indexCultureStructureOffice = $this->calculateCultureStructureGraph(base64_encode($orgId),$officeId,'',2,$date);
                    $indexCultureStructureOfficeExclude = $this->calculateCultureStructureGraph(base64_encode($orgId),$officeId,'',1,$date);

                    $cultureStructureOfficeCount=0;
                    foreach ($indexCultureStructureOffice as $cultureStructureOffice) {
                        //Chk duplicate record for same day
                        $cultureStructureOfficeRecords = DB::table('sot_culture_structure_dashboard_graph')
                                ->select('id')
                                ->whereDate('date',$date)
                                ->where('orgId',$orgId)
                                ->where('categoryId',$cultureStructureOffice['id'])
                                ->where('officeId',$officeId)
                                ->whereNull('departmentId')
                                ->first();
                        if($cultureStructureOfficeRecords){
                            //No need to insert
                        }else{
                            $insertCultureStructureOfficeArray = array(
                                'date'             => $date,
                                'orgId'            => $orgId,
                                'officeId'         => $officeId,
                                'categoryId'       => $cultureStructureOffice['id'],
                                'with_weekend'     => $cultureStructureOffice['percentage'],
                                'without_weekend'  => $indexCultureStructureOfficeExclude[$cultureStructureOfficeCount]['percentage'],
                                'created_at'       => date('Y-m-d H:i:s')
                            );
                            $indexCultureStructureOfficeRecords = DB::table('sot_culture_structure_dashboard_graph')->insertGetId($insertCultureStructureOfficeArray);
                        }    
                        $cultureStructureOfficeCount++;
                    }

                    //Get departments
                    $departments = DB::table('departments')
                        //->select('id')
                        ->where('orgId',$orgId)
                        ->where('officeId',$officeId)
                        ->whereDate('created_at', '<=',$date)
                        ->where('status','Active') 
                        ->get();
                    if($departments){
                        foreach ($departments as $department) {
                            $departmentId = $department->id;
                            $allDepartmentId = $department->departmentId;
                           
                            //Get index
                            $indexCultureStructureAllDept = $this->calculateCultureStructureGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            $indexCultureStructureAllDeptExclude = $this->calculateCultureStructureGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            
                            $cultureStructureAllDeptCount=0;
                            foreach ($indexCultureStructureAllDept as $cultureStructureAllDept) {
                                //Chk duplicate record for same day
                                $cultureStructureAllDeptRecords = DB::table('sot_culture_structure_dashboard_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('categoryId',$cultureStructureAllDept['id'])
                                        ->whereNull('officeId')
                                        ->where('departmentId',$allDepartmentId)
                                        ->first();
                                if($cultureStructureAllDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertCultureStructureAllDeptArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'departmentId'     => $allDepartmentId,
                                        'categoryId'       => $cultureStructureAllDept['id'],
                                        'with_weekend'     => $cultureStructureAllDept['percentage'],
                                        'without_weekend'  => $indexCultureStructureAllDeptExclude[$cultureStructureAllDeptCount]['percentage'],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexCultureStructureAllDeptRecords = DB::table('sot_culture_structure_dashboard_graph')->insertGetId($insertCultureStructureAllDeptArray);
                                }    
                                $cultureStructureAllDeptCount++;
                            }

                            $indexCultureStructureDept = $this->calculateCultureStructureGraph(base64_encode($orgId),$officeId,$departmentId,2,$date);
                            $indexCultureStructureDeptExclude = $this->calculateCultureStructureGraph(base64_encode($orgId),$officeId,$departmentId,1,$date);

                            $cultureStructureDeptCount=0;
                            foreach ($indexCultureStructureDept as $cultureStructureDept) {
                                //Chk duplicate record for same day
                                $cultureStructureDeptRecords = DB::table('sot_culture_structure_dashboard_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('categoryId',$cultureStructureDept['id'])
                                        ->where('officeId',$officeId)
                                        ->where('departmentId',$departmentId)
                                        ->first();
                                if($cultureStructureDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertCultureStructureDeptArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'officeId'         => $officeId,
                                        'departmentId'     => $departmentId,
                                        'categoryId'       => $cultureStructureDept['id'],
                                        'with_weekend'     => $cultureStructureDept['percentage'],
                                        'without_weekend'  => $indexCultureStructureDeptExclude[$cultureStructureDeptCount]['percentage'],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexCultureStructureDeptRecords = DB::table('sot_culture_structure_dashboard_graph')->insertGetId($insertCultureStructureDeptArray);
                                }    
                                $cultureStructureDeptCount++;
                            }
                        }   
                    }
                }
            }
        }
    }
}

public function calculateCultureStructureGraph($orgId,$officeId,$departmentId,$isExclude,$date,$categoryId=false)
{
    $orgId = base64_decode($orgId);
    $departmentId = $departmentId;
    $officeId = $officeId;

    //Get weekends
    $startDate=DB::table('sot_answers')
        ->select('sot_answers.created_at')
        ->leftjoin('users','users.id','sot_answers.userId')
        ->where('users.status','Active')
        ->where('sot_answers.status','Active')
        ->where('users.orgId',$orgId)
        ->whereDate('sot_answers.created_at','<=',$date)
        ->orderBy('sot_answers.created_at','ASC')
        ->first();

    if(isset($startDate->created_at) && $startDate->created_at!=''){
        $startDate = date('Y-m-d',strtotime($startDate->created_at));
    }else{
        $startDate = $date;
    }
    $currentDate = $date;
    $getWeekendDates = $this->getDatesFromRange($startDate, $currentDate);

    $sotCultureStrTbl = DB::table('sot_culture_structure_records')->where('status','Active')->get();

    $sotArray = array();
    $totalCount = 0;
    foreach ($sotCultureStrTbl as $key => $value) {
        $query = DB::table('sot_answers')
            ->leftJoin('sot_questionnaire_records','sot_questionnaire_records.id','sot_answers.question_id')
            ->leftjoin('users','users.id','sot_answers.userId')
            ->where('users.status','Active')
            ->where('sot_answers.status','Active')
            ->where('sot_questionnaire_records.status','Active')
            ->where('sot_questionnaire_records.type',$value->type);
        if($date){
            $query->whereDate('sot_answers.created_at','<=',$date);
        }
        if($isExclude==1){
            $query->whereNotIn(DB::raw("(DATE_FORMAT(sot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
        }
        if(!empty($orgId)){
            $query->where('users.orgId',$orgId);
        } 
        if(!empty($officeId) && empty($departmentId)){
            $query->where('users.officeId',$officeId);
        }elseif(!empty($officeId) && !empty($departmentId)){
            $query->where('users.officeId',$officeId);
            $query->where('users.departmentId',$departmentId);
        }elseif(empty($officeId) && !empty($departmentId)){
            $query->leftjoin('departments','departments.id','users.departmentId')
                ->where('departments.status','Active')
                ->where('departments.departmentId',$departmentId);
        }   
        $sotCount['count'] = $query->count();
        $sotCount['id'] = $value->id;

        $totalCount += $sotCount['count'];

        array_push($sotArray, $sotCount);
    }

    $cultureStructureArray = array();
    foreach ($sotArray as $val) {
        
        $sot['percentage'] = 0;
        if ($totalCount > 0) {
            $sot['percentage'] = round((($val['count']/$totalCount)*100),2);
        }
        $sot['id']       = $val['id'];
        array_push($cultureStructureArray, $sot);
    }

    if (!empty($categoryId)) {
        $cultureStructureArray = array();
        foreach ($sotArray as $val) {
            if ($categoryId == $val['id']) {
                $sot['percentage'] = 0;
                if ($totalCount > 0) {
                    $sot['percentage'] = round((($val['count']/$totalCount)*100),2);
                }
                $sot['id']       = $val['id'];
            array_push($cultureStructureArray, $sot);
            }
        }
    }
    return $cultureStructureArray;
}

public function getTeamRoleIndexReportByPastDate(){
    $year  = '2019';
    $month = '11';  
    $day = '30';
    //Get total days of month and year
    //$totalDays = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    // $totalDays = 20;
    // for ($day = 16; $day <= $totalDays; $day++) {
        $makeDate = $year."-".$month."-".$day;
        $this->getTeamRoleIndexReport($makeDate);
    // }
    echo "Done for :".$month." - ".$year." days ".$day;
    die('<br /> Finally Done.');
}

public function getTeamRoleIndexReport($date=false){

    if($date){
        $date = $date;
    }else{
        $date =date('Y-m-d',strtotime('last day of this month'));
    }
    //Get all organization
    $allOrganizations = DB::table('organisations')
                        ->where('status','Active') 
                        ->select('id')
                        ->whereDate('created_at', '<=',$date)
                        ->get();
    if($allOrganizations){
        foreach($allOrganizations as $organization){
            $orgId = $organization->id;
            $indexTeamRoleOrg = $this->calculateTeamRoleGraph(base64_encode($orgId),'','',2,$date);
            $indexTeamRoleOrgExclude = $this->calculateTeamRoleGraph(base64_encode($orgId),'','',1,$date);
            // $indexTeamRoleOrg = $this->calculateTeamRoleGraph(base64_encode(8),'','',2,$date);
            // $indexTeamRoleOrg = $this->calculateTeamRoleGraph(base64_encode(8),'','',1,$date);
            
            $teamRoleOrgCount=0;
            foreach ($indexTeamRoleOrg as $teamRoleOrg) {
                //Chk duplicate record for same day
                $teamRoleOrgRecords = DB::table('cot_team_role_dashboard_graph')
                        ->select('id')
                        ->whereDate('date',$date)
                        ->where('orgId',$orgId)
                        ->where('categoryId',$teamRoleOrg['id'])
                        ->whereNull('officeId')
                        ->whereNull('departmentId')
                        ->first();
                if($teamRoleOrgRecords){
                    //No need to insert
                }else{
                    $insertTeamRoleOrgArray = array(
                        'date'             => $date,
                        'orgId'            => $orgId,
                        'categoryId'       => $teamRoleOrg['id'],
                        'with_weekend'     => $teamRoleOrg['percentage'],
                        'without_weekend'  => $indexTeamRoleOrgExclude[$teamRoleOrgCount]['percentage'],
                        'created_at'       => date('Y-m-d H:i:s')
                    );
                    $indexTeamRoleOrgRecords = DB::table('cot_team_role_dashboard_graph')->insertGetId($insertTeamRoleOrgArray);
                }    
                $teamRoleOrgCount++;
            }
       
            //Get offices of organization
            $offices = DB::table('offices')
                        ->select('id')
                        ->where('orgId',$orgId)
                        ->where('status','Active') 
                        ->whereDate('created_at', '<=',$date)
                        ->get();
            if($offices){
                foreach($offices as $office){
                    $officeId = $office->id;
                    $indexTeamRoleOffice = $this->calculateTeamRoleGraph(base64_encode($orgId),$officeId,'',2,$date);
                    $indexTeamRoleOfficeExclude = $this->calculateTeamRoleGraph(base64_encode($orgId),$officeId,'',1,$date);

                    $teamRoleOfficeCount=0;
                    foreach ($indexTeamRoleOffice as $teamRoleOffice) {
                        //Chk duplicate record for same day
                        $teamRoleOfficeRecords = DB::table('cot_team_role_dashboard_graph')
                                ->select('id')
                                ->whereDate('date',$date)
                                ->where('orgId',$orgId)
                                ->where('categoryId',$teamRoleOffice['id'])
                                ->where('officeId',$officeId)
                                ->whereNull('departmentId')
                                ->first();
                        if($teamRoleOfficeRecords){
                            //No need to insert
                        }else{
                            $insertTeamRoleOfficeArray = array(
                                'date'             => $date,
                                'orgId'            => $orgId,
                                'officeId'         => $officeId,
                                'categoryId'       => $teamRoleOffice['id'],
                                'with_weekend'     => $teamRoleOffice['percentage'],
                                'without_weekend'  => $indexTeamRoleOfficeExclude[$teamRoleOfficeCount]['percentage'],
                                'created_at'       => date('Y-m-d H:i:s')
                            );
                            $indexTeamRoleOfficeRecords = DB::table('cot_team_role_dashboard_graph')->insertGetId($insertTeamRoleOfficeArray);
                        }    
                        $teamRoleOfficeCount++;
                    }

                    //Get departments
                    $departments = DB::table('departments')
                        //->select('id')
                        ->where('orgId',$orgId)
                        ->where('officeId',$officeId)
                        ->whereDate('created_at', '<=',$date)
                        ->where('status','Active') 
                        ->get();
                    if($departments){
                        foreach ($departments as $department) {
                            $departmentId = $department->id;
                            $allDepartmentId = $department->departmentId;
                           
                            //Get index
                            $indexTeamRoleAllDept = $this->calculateTeamRoleGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            $indexTeamRoleAllDeptExclude = $this->calculateTeamRoleGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            
                            $teamRoleAllDeptCount=0;
                            foreach ($indexTeamRoleAllDept as $teamRoleAllDept) {
                                //Chk duplicate record for same day
                                $teamRoleAllDeptRecords = DB::table('cot_team_role_dashboard_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('categoryId',$teamRoleAllDept['id'])
                                        ->whereNull('officeId')
                                        ->where('departmentId',$allDepartmentId)
                                        ->first();
                                if($teamRoleAllDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertTeamRoleAllDeptArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'departmentId'     => $allDepartmentId,
                                        'categoryId'       => $teamRoleAllDept['id'],
                                        'with_weekend'     => $teamRoleAllDept['percentage'],
                                        'without_weekend'  => $indexTeamRoleAllDeptExclude[$teamRoleAllDeptCount]['percentage'],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexTeamRoleAllDeptRecords = DB::table('cot_team_role_dashboard_graph')->insertGetId($insertTeamRoleAllDeptArray);
                                }    
                                $teamRoleAllDeptCount++;
                            }

                            $indexTeamRoleDept = $this->calculateTeamRoleGraph(base64_encode($orgId),$officeId,$departmentId,2,$date);
                            $indexTeamRoleDeptExclude = $this->calculateTeamRoleGraph(base64_encode($orgId),$officeId,$departmentId,1,$date);

                            $teamRoleDeptCount=0;
                            foreach ($indexTeamRoleDept as $teamRoleDept) {
                                //Chk duplicate record for same day
                                $teamRoleDeptRecords = DB::table('cot_team_role_dashboard_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('categoryId',$teamRoleDept['id'])
                                        ->where('officeId',$officeId)
                                        ->where('departmentId',$departmentId)
                                        ->first();
                                if($teamRoleDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertTeamRoleDeptArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'officeId'         => $officeId,
                                        'departmentId'     => $departmentId,
                                        'categoryId'       => $teamRoleDept['id'],
                                        'with_weekend'     => $teamRoleDept['percentage'],
                                        'without_weekend'  => $indexTeamRoleDeptExclude[$teamRoleDeptCount]['percentage'],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexTeamRoleDeptRecords = DB::table('cot_team_role_dashboard_graph')->insertGetId($insertTeamRoleDeptArray);
                                }    
                                $teamRoleDeptCount++;
                            }
                        }   
                    }
                }
            }
        }
    }
}

public function calculateTeamRoleGraph($orgId,$officeId,$departmentId,$isExclude,$date,$categoryId=false)
{
    $orgId = base64_decode($orgId);
    $departmentId = $departmentId;
    $officeId = $officeId;

    $finalArray  = array();

    $mapers = DB::table('cot_role_map_options')->where('status','Active')->get();

    //Get weekends
    $startDate=DB::table('cot_answers')
        ->select('created_at')
        ->where('orgId',$orgId)
        ->whereDate('created_at','<=',$date)
        ->orderBy('created_at','ASC')
        ->first();

    if(isset($startDate->created_at) && $startDate->created_at!=''){
        $startDate = date('Y-m-d',strtotime($startDate->created_at));
    }else{
        $startDate = $date;
    }
    $currentDate = $date;
    $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($startDate, $currentDate);

    $query = DB::table('cot_answers')
        ->select('users.id')
        ->leftjoin('users','users.id','cot_answers.userId')
        ->where('users.status','Active')
        ->where('cot_answers.status','Active');
    if($date){
        $query->whereDate('cot_answers.created_at','<=',$date);
    }
    if (!empty($orgId)) {
        $query->where('users.orgId',$orgId);  
    }
    if($isExclude==1){
        $query->whereNotIn(DB::raw("(DATE_FORMAT(cot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }
    if(!empty($officeId) && empty($departmentId)){
        $query->where('users.officeId',$officeId);
    } elseif(!empty($officeId) && !empty($departmentId)) {
        $query->where('users.officeId',$officeId);
        $query->where('users.departmentId',$departmentId);
    } elseif(empty($officeId) && !empty($departmentId)) {
        $query->leftJoin('departments','departments.id','users.departmentId')
            ->where('departments.status','Active')
            ->where('departments.departmentId',$departmentId);
    }
    $users = $query->groupBy('users.id')->get();    

    // echo "<pre>";print_r($users);die();

    // $resultArray1 = array();
    // foreach($users as $value){
    //     $data['id']    = $value->id;
    //     $data['orgId'] = $value->orgId;
    //     $data['name']  = $value->name;

    //     array_push($resultArray1, $data);
    // }
    // $users23 = array_unique($resultArray1,SORT_REGULAR);

    // $finalUser = array();
    // foreach($users23 as  $value55){
    //     $object = (object)array();    
    //     $object->id = $value55['id'];
    //     $object->orgId=$value55['orgId'];
    //     $object->name=$value55['name'];

    //     array_push($finalUser,$object);
    // }
    // echo "<pre>";print_r($finalUser);die();
    $cotRoleMapOptions = DB::table('cot_role_map_options')->where('status','Active')->get();

    $usersArray = array();
    foreach ($users as $key => $value) {
        foreach($cotRoleMapOptions as $key => $maper){
            $maperCountQuery = DB::table('cot_answers')
                ->where('userId',$value->id)
                ->where('cot_role_map_option_id',$maper->id)
                ->where('status','Active');
            if($date){
                $maperCountQuery->whereDate('cot_answers.created_at','<=',$date);
            }
            if (!empty($orgId)) {
                $maperCountQuery->where('orgId',$orgId);
            }
            if($isExclude==1){
                $maperCountQuery->whereNotIn(DB::raw("(DATE_FORMAT(cot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
            }
            $maperCount = $maperCountQuery->sum('answer');

            $resultArray = array();
            $Value[$maper->maper_key] = $maperCount;    
            array_push($resultArray, $Value);
        }   
        arsort($resultArray[0]);
        $i = 0;
        $prev = "";
        $j = 0;

        $new  = array();
        $new1 = array();

        foreach($resultArray[0] as $key => $val){ 
            if($val != $prev){ 
              $i++; 
            }

            $new1['title'] = $key; 
            $new1['value'] = $i;        
            $prev = $val;

            if ($key==$cotRoleMapOptions[0]->maper_key) {
                $new1['priority'] = 1;
            }elseif ($key==$cotRoleMapOptions[1]->maper_key) {
                $new1['priority'] = 2;
            }elseif ($key==$cotRoleMapOptions[2]->maper_key) {
                $new1['priority'] = 3;
            }elseif ($key==$cotRoleMapOptions[3]->maper_key) {
                $new1['priority'] = 4;
            }elseif ($key==$cotRoleMapOptions[4]->maper_key) {
                $new1['priority'] = 5;
            }elseif ($key==$cotRoleMapOptions[5]->maper_key) {
                $new1['priority'] = 6;
            }elseif ($key==$cotRoleMapOptions[6]->maper_key) {
                $new1['priority'] = 7;
            }elseif ($key==$cotRoleMapOptions[7]->maper_key) {
                $new1['priority'] = 8;
            }
            array_push($new,$new1);
        }
        $value->new = $new;
        $value->totalKeyCount = $resultArray[0];
        array_push($usersArray, $value);
    }
    $usersArray = app('App\Http\Controllers\Admin\AdminReportController')->sortarr($usersArray);
    $cotTeamRoleMapUserArray = $usersArray;

    /*get percentage values*/
    $shaper = 0; 
    $coordinator = 0; 
    $completerFinisher = 0; 
    $teamworker = 0; 
    $implementer = 0; 
    $monitorEvaluator = 0; 
    $plant = 0; 
    $resourceInvestigator = 0; 
    
    foreach ($usersArray as $key1 => $value1){
        if($value1->shaper==1 || $value1->shaper==2 || $value1->shaper==3){
            $shaper++;     
        }
        if($value1->coordinator==1 || $value1->coordinator==2 || $value1->coordinator==3){
            $coordinator++;
        }
        if($value1->completerFinisher==1 || $value1->completerFinisher==2 || $value1->completerFinisher==3){
            $completerFinisher++;
        }
        if($value1->teamworker==1 || $value1->teamworker==2 || $value1->teamworker==3){
            $teamworker++;
        }
        if($value1->implementer==1 || $value1->implementer==2 || $value1->implementer==3){
            $implementer++;
        }
        if($value1->monitorEvaluator==1 || $value1->monitorEvaluator==2 || $value1->monitorEvaluator==3){
            $monitorEvaluator++;
        }
        if($value1->plant==1 || $value1->plant==2 || $value1->plant==3){
            $plant++;
        }
        if($value1->resourceInvestigator==1 || $value1->resourceInvestigator==2 || $value1->resourceInvestigator==3){
            $resourceInvestigator++;
        }
    }

    $data['shaper']              = $shaper;
    $data['coordinator']         = $coordinator;
    $data['completerFinisher']   = $completerFinisher;
    $data['teamworker']          = $teamworker;
    $data['implementer']         = $implementer;
    $data['monitorEvaluator']    = $monitorEvaluator;
    $data['plant']               = $plant;
    $data['resourceInvestigator']= $resourceInvestigator;

    $totalUsers = count($users);

    //get presented values
    $cotTeamRoleMapGraphPercentage = array();

    if (!empty($data) && (!empty($totalUsers))){
        $data['shaper']              = round((($shaper/$totalUsers)*100), 2);
        $data['coordinator']         = round((($coordinator/$totalUsers)*100), 2);
        $data['completerFinisher']   = round((($completerFinisher/$totalUsers)*100), 2);
        $data['teamworker']          = round((($teamworker/$totalUsers)*100), 2);
        $data['implementer']         = round((($implementer/$totalUsers)*100), 2);
        $data['monitorEvaluator']    = round((($monitorEvaluator/$totalUsers)*100), 2);
        $data['plant']               = round((($plant/$totalUsers)*100), 2);
        $data['resourceInvestigator']= round((($resourceInvestigator/$totalUsers)*100), 2);

        $cotTeamRoleMapGraphPercentage['data'] = $data;
    }else{
        $data['shaper']              = 0;
        $data['coordinator']         = 0;
        $data['completerFinisher']   = 0;
        $data['teamworker']          = 0;
        $data['implementer']         = 0;
        $data['monitorEvaluator']    = 0;
        $data['plant']               = 0;
        $data['resourceInvestigator']= 0;

        $cotTeamRoleMapGraphPercentage['data'] = $data;
    }

    // echo "<pre>";print_r($cotTeamRoleMapGraphPercentage);

    $cotOptionsArr = array();
    $color = array();
    foreach ($cotRoleMapOptions as $key => $value) {
        array_push($cotOptionsArr, $value->id);
        if ($value->categoryId == 1) {
            array_push($color, '#000');
        }elseif ($value->categoryId == 2    ) {
            array_push($color, '#e28b13');
        }elseif ($value->categoryId == 3) {
            array_push($color, '#eb1c24');
        }elseif ($value->categoryId == 4) {
            array_push($color, 'lightgray');
        }
    }
    // echo "<pre>";print_r($cotOptionsArr);
    $perArr = array(
            $data['shaper'],
            $data['coordinator'],
            $data['implementer'],
            $data['completerFinisher'],
            $data['monitorEvaluator'],
            $data['teamworker'],
            $data['plant'],
            $data['resourceInvestigator']
        );

    $options = $cotOptionsArr;
    $perArrs = $perArr;

    $result = array_map(function ($option, $perArr) {
      return array_combine(
        ['id','percentage'],
        [$option,$perArr]
      );
    }, $options,$perArrs);
    $cotGraphArr = collect($result);

    if (!empty($categoryId)) {
        $cotGraphTempArr = array();
        foreach ($result as $key => $value) {
            if ($categoryId == $value['id']) {
                // $cotGraphArr = collect($value);
                array_push($cotGraphTempArr, $value);
            }
        }
        $cotGraphArr = collect($cotGraphTempArr);
    }
    return $cotGraphArr;
}

public function getMotivationIndexReportByPastDate(){
    $year  = '2019';
    $month = '11';  
    $day = '30';
    //Get total days of month and year
    //$totalDays = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    // $totalDays = 20;
    // for ($day = 16; $day <= $totalDays; $day++) {
        $makeDate = $year."-".$month."-".$day;
        $this->getMotivationIndexReport($makeDate);
    // }
    echo "Done for :".$month." - ".$year." days ".$day;
    die('<br /> Finally Done.');
}

public function getMotivationIndexReport($date=false){

    if($date){
        $date = $date;
    }else{
        $date =date('Y-m-d',strtotime('last day of this month'));
    }
    //Get all organization
    $allOrganizations = DB::table('organisations')
                        ->where('status','Active') 
                        ->select('id')
                        ->whereDate('created_at', '<=',$date)
                        ->get();
    if($allOrganizations){
        foreach($allOrganizations as $organization){
            $orgId = $organization->id;
            $indexMotivationOrg = $this->calculateMotivationGraph(base64_encode($orgId),'','',2,$date);
            $indexMotivationOrgExclude = $this->calculateMotivationGraph(base64_encode($orgId),'','',1,$date);
            // $indexMotivationOrg = $this->calculateMotivationGraph(base64_encode(8),'','',2,$date);
            // $indexMotivationOrgExclude = $this->calculateMotivationGraph(base64_encode(8),'','',1,$date);
            
            $motivationOrgCount=0;
            foreach ($indexMotivationOrg as $motivationOrg) {
                //Chk duplicate record for same day
                $motivationOrgRecords = DB::table('sot_motivation_dashboard_graph')
                        ->select('id')
                        ->whereDate('date',$date)
                        ->where('orgId',$orgId)
                        ->where('categoryId',$motivationOrg['id'])
                        ->whereNull('officeId')
                        ->whereNull('departmentId')
                        ->first();
                if($motivationOrgRecords){
                    //No need to insert
                }else{
                    $insertMotivationOrgArray = array(
                        'date'             => $date,
                        'orgId'            => $orgId,
                        'categoryId'       => $motivationOrg['id'],
                        'with_weekend'     => $motivationOrg['percentage'],
                        'without_weekend'  => $indexMotivationOrgExclude[$motivationOrgCount]['percentage'],
                        'created_at'       => date('Y-m-d H:i:s')
                    );
                    $indexMotivationOrgRecords = DB::table('sot_motivation_dashboard_graph')->insertGetId($insertMotivationOrgArray);
                }    
                $motivationOrgCount++;
            }
       
            //Get offices of organization
            $offices = DB::table('offices')
                        ->select('id')
                        ->where('orgId',$orgId)
                        ->where('status','Active') 
                        ->whereDate('created_at', '<=',$date)
                        ->get();
            if($offices){
                foreach($offices as $office){
                    $officeId = $office->id;
                    $indexMotivationOffice = $this->calculateMotivationGraph(base64_encode($orgId),$officeId,'',2,$date);
                    $indexMotivationOfficeExclude = $this->calculateMotivationGraph(base64_encode($orgId),$officeId,'',1,$date);

                    $motivationOfficeCount=0;
                    foreach ($indexMotivationOffice as $motivationOffice) {
                        //Chk duplicate record for same day
                        $motivationOfficeRecords = DB::table('sot_motivation_dashboard_graph')
                                ->select('id')
                                ->whereDate('date',$date)
                                ->where('orgId',$orgId)
                                ->where('categoryId',$motivationOffice['id'])
                                ->where('officeId',$officeId)
                                ->whereNull('departmentId')
                                ->first();
                        if($motivationOfficeRecords){
                            //No need to insert
                        }else{
                            $insertMotivationOfficeArray = array(
                                'date'             => $date,
                                'orgId'            => $orgId,
                                'officeId'         => $officeId,
                                'categoryId'       => $motivationOffice['id'],
                                'with_weekend'     => $motivationOffice['percentage'],
                                'without_weekend'  => $indexMotivationOfficeExclude[$motivationOfficeCount]['percentage'],
                                'created_at'       => date('Y-m-d H:i:s')
                            );
                            $indexMotivationOfficeRecords = DB::table('sot_motivation_dashboard_graph')->insertGetId($insertMotivationOfficeArray);
                        }    
                        $motivationOfficeCount++;
                    }

                    //Get departments
                    $departments = DB::table('departments')
                        //->select('id')
                        ->where('orgId',$orgId)
                        ->where('officeId',$officeId)
                        ->whereDate('created_at', '<=',$date)
                        ->where('status','Active') 
                        ->get();
                    if($departments){
                        foreach ($departments as $department) {
                            $departmentId = $department->id;
                            $allDepartmentId = $department->departmentId;
                           
                            //Get index
                            $indexMotivationAllDept = $this->calculateMotivationGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            $indexMotivationAllDeptExclude = $this->calculateMotivationGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            
                            $motivationAllDeptCount=0;
                            foreach ($indexMotivationAllDept as $motivationAllDept) {
                                //Chk duplicate record for same day
                                $motivationAllDeptRecords = DB::table('sot_motivation_dashboard_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('categoryId',$motivationAllDept['id'])
                                        ->whereNull('officeId')
                                        ->where('departmentId',$allDepartmentId)
                                        ->first();
                                if($motivationAllDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertMotivationAllDeptArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'departmentId'     => $allDepartmentId,
                                        'categoryId'       => $motivationAllDept['id'],
                                        'with_weekend'     => $motivationAllDept['percentage'],
                                        'without_weekend'  => $indexMotivationAllDeptExclude[$motivationAllDeptCount]['percentage'],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexMotivationAllDeptRecords = DB::table('sot_motivation_dashboard_graph')->insertGetId($insertMotivationAllDeptArray);
                                }    
                                $motivationAllDeptCount++;
                            }

                            $indexMotivationDept = $this->calculateMotivationGraph(base64_encode($orgId),$officeId,$departmentId,2,$date);
                            $indexMotivationDeptExclude = $this->calculateMotivationGraph(base64_encode($orgId),$officeId,$departmentId,1,$date);

                            $motivationDeptCount=0;
                            foreach ($indexMotivationDept as $motivationDept) {
                                //Chk duplicate record for same day
                                $motivationDeptRecords = DB::table('sot_motivation_dashboard_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('categoryId',$motivationDept['id'])
                                        ->where('officeId',$officeId)
                                        ->where('departmentId',$departmentId)
                                        ->first();
                                if($motivationDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertMotivationDeptArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'officeId'         => $officeId,
                                        'departmentId'     => $departmentId,
                                        'categoryId'       => $motivationDept['id'],
                                        'with_weekend'     => $motivationDept['percentage'],
                                        'without_weekend'  => $indexMotivationDeptExclude[$motivationDeptCount]['percentage'],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexMotivationDeptRecords = DB::table('sot_motivation_dashboard_graph')->insertGetId($insertMotivationDeptArray);
                                }    
                                $motivationDeptCount++;
                            }
                        }   
                    }
                }
            }
        }
    }
}

public function calculateMotivationGraph($orgId,$officeId,$departmentId,$isExclude,$date,$categoryId=false)
{
    $orgId = base64_decode($orgId);
    $departmentId = $departmentId;
    $officeId = $officeId;

    //Get weekends
    $startDate=DB::table('sot_motivation_answers')
        ->select('created_at')
        ->where('orgId',$orgId)
        ->whereDate('created_at','<=',$date)
        ->orderBy('created_at','ASC')
        ->first();

    if(isset($startDate->created_at) && $startDate->created_at!=''){
        $startDate = date('Y-m-d',strtotime($startDate->created_at));
    }else{
        $startDate = $date;
    }
    $currentDate = $date;
    // $currentDate = date('Y-m-d');
    $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($startDate, $currentDate);

    $SOTmotivationResultArray = array();
    $categoryTblQuery   = DB::table('sot_motivation_value_records')
            ->where('status','Active');
    if (!empty($categoryId)) {
        $categoryTblQuery->where('id',$categoryId);
    }
    $categoryTbl = $categoryTblQuery->get();

    $query = DB::table('sot_motivation_answers')
        ->select('sot_motivation_answers.userId')
        ->leftjoin('users','users.id','sot_motivation_answers.userId')
        ->where('users.status','Active')
        ->where('sot_motivation_answers.status','Active');
    if($date){
        $query->whereDate('sot_motivation_answers.created_at','<=',$date);
    }
    if($isExclude==1){
        $query->whereNotIn(DB::raw("(DATE_FORMAT(sot_motivation_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }
    if(!empty($orgId)){
        $query->where('users.orgId',$orgId);
    } 
    if(!empty($officeId) && empty($departmentId)){
        $query->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)){
        $query->where('users.officeId',$officeId);
        $query->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)){
        $query->leftJoin('departments','departments.id','users.departmentId')
            ->where('departments.status','Active')
            ->where('departments.departmentId',$departmentId);
    }
    $usersList = $query->groupBy('sot_motivation_answers.userId')->get();
    $totalUser = count($usersList);

    if (!empty($totalUser)){
      foreach ($categoryTbl as $sotCvalue){
        $query = DB::table('sot_motivation_answers AS sotans')
          ->leftJoin('sot_motivation_question_options AS qoption','qoption.id','sotans.optionId')
          ->leftjoin('users','users.id','sotans.userId')
          ->where('users.status','Active')                     
          ->where('sotans.status','Active')
          ->where('qoption.status','Active')
          ->where('qoption.category_id',$sotCvalue->id);
        if($date){
            $query->whereDate('sotans.created_at','<=',$date);
        }
        if($isExclude==1){
          $query->whereNotIn(DB::raw("(DATE_FORMAT(sotans.created_at,'%Y-%m-%d'))"),$getWeekendDates);
        }
        if(!empty($orgId)){
          $query->where('sotans.orgId',$orgId);
        } 
        if(!empty($officeId) && empty($departmentId)){
          $query->where('users.officeId',$officeId);
        }elseif(!empty($officeId) && !empty($departmentId)){
          $query->where('users.officeId',$officeId);
          $query->where('users.departmentId',$departmentId);
        }elseif(empty($officeId) && !empty($departmentId)){
            $query->leftJoin('departments','departments.id','users.departmentId')
                ->where('departments.status','Active')
                ->where('departments.departmentId',$departmentId);
        } 
        $ansTbl = $query->sum('sotans.answer'); 

        $result1['id']      = $sotCvalue->id;
        // $result1['title']      = utf8_encode($sotCvalue->title);
        // $result1['percentage'] = 0;
        // if (!empty($totalUser)){
          $result1['percentage'] = round(($ansTbl/$totalUser),2);         
        // }
        array_push($SOTmotivationResultArray, $result1);
      }
    }else{
        foreach ($categoryTbl as $sotCvalue){
            $value['id']      =  $sotCvalue->id;
            $value['percentage'] =  0;
            array_push($SOTmotivationResultArray, $value);  
        }
    }
    return $SOTmotivationResultArray;
}


public function getDirectingIndexReportByPastDate(){
    $year  = '2019';
    $month = '11';  
    $day = '30';
    //Get total days of month and year
    //$totalDays = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    // $totalDays = 20;
    // for ($day = 16; $day <= $totalDays; $day++) {
        $makeDate = $year."-".$month."-".$day;
        $this->getDirectingIndexReport($makeDate);
    // }
    echo "Done for :".$month." - ".$year." days ".$day;
    die('<br /> Finally Done.');
}

public function getDirectingIndexReport($date=false){

    if($date){
        $date = $date;
    }else{
        $date =date('Y-m-d',strtotime('last day of this month'));
    }
    //Get all organization
    $allOrganizations = DB::table('organisations')
                        ->where('status','Active') 
                        ->select('id')
                        ->whereDate('created_at', '<=',$date)
                        ->get();
    if($allOrganizations){
        foreach($allOrganizations as $organization){
            $orgId = $organization->id;
            $indexDotBeliefOrg = $this->calculateDotBeliefGraph(base64_encode($orgId),'','',2,$date);
            $indexDotBeliefOrgExclude = $this->calculateDotBeliefGraph(base64_encode($orgId),'','',1,$date);

            $dotBeliefOrgCount=0;
            foreach ($indexDotBeliefOrg as $dotBeliefOrg) {
                //Chk duplicate record for same day
                $dotBeliefOrgRecords = DB::table('dot_belief_graph')
                        ->select('id')
                        ->whereDate('date',$date)
                        ->where('orgId',$orgId)
                        ->where('beliefId',$dotBeliefOrg['id'])
                        ->whereNull('officeId')
                        ->whereNull('departmentId')
                        ->first();
                if($dotBeliefOrgRecords){
                    //No need to insert
                }else{
                    $insertDotBeliefOrgArray = array(
                        'date'             => $date,
                        'orgId'            => $orgId,
                        'beliefId'       => $dotBeliefOrg['id'],
                        'with_weekend'     => $dotBeliefOrg['rating'],
                        'without_weekend'  => $indexDotBeliefOrgExclude[$dotBeliefOrgCount]['rating'],
                        'created_at'       => date('Y-m-d H:i:s')
                    );
                    $indexDotBeliefOrgRecords = DB::table('dot_belief_graph')->insertGetId($insertDotBeliefOrgArray);
                }    
                $dotBeliefOrgCount++;
            }

            $dots = DB::table('dots')
                ->whereDate('created_at','<=',$date)
                ->where('orgId',$orgId)
                ->first();
            if (!empty($dots)) {
                $dotBeliefs = DB::table('dots_beliefs')
                    ->where('status','Active')
                    ->where('dotId',$dots->id)
                    ->whereDate('created_at','<=',$date)
                    ->orderBy('id','ASC')
                    ->get();
                foreach ($dotBeliefs as $bValue){
                    $beliefId   = $bValue->id;
                    $dotId      = $dots->id;
                    $indexDotValuesOrg = $this->calculateDotValuesGraph(base64_encode($orgId),'','',2,$beliefId,$dotId,$date);
                    $indexDotValuesOrgExclude = $this->calculateDotValuesGraph(base64_encode($orgId),'','',1,$beliefId,$dotId,$date);
                    $dotValuesOrgCount=0;
                    foreach ($indexDotValuesOrg as $dotValuesOrg) {
                        //Chk duplicate record for same day
                        $dotValuesOrgRecords = DB::table('dot_values_graph')
                                ->select('id')
                                ->whereDate('date',$date)
                                ->where('orgId',$orgId)
                                ->where('beliefId',$beliefId)
                                ->where('valueId',$dotValuesOrg['id'])
                                ->whereNull('officeId')
                                ->whereNull('departmentId')
                                ->first();
                        if($dotValuesOrgRecords){
                            //No need to insert
                        }else{
                            $insertDotValuesOrgArray = array(
                                'date'             => $date,
                                'orgId'            => $orgId,
                                'beliefId'         => $beliefId,
                                'valueId'          => $dotValuesOrg['id'],
                                'with_weekend'     => $dotValuesOrg['rating'],
                                'without_weekend'  => $indexDotValuesOrgExclude[$dotValuesOrgCount]['rating'],
                                'created_at'       => date('Y-m-d H:i:s')
                            );
                            $indexDotValuesOrgRecords = DB::table('dot_values_graph')->insertGetId($insertDotValuesOrgArray);
                        }    
                        $dotValuesOrgCount++;
                    }
                }
            }
       
            //Get offices of organization
            $offices = DB::table('offices')
                        ->select('id')
                        ->where('orgId',$orgId)
                        ->where('status','Active') 
                        ->whereDate('created_at', '<=',$date)
                        ->get();
            if($offices){
                foreach($offices as $office){
                    $officeId = $office->id;
                    $indexDotBeliefOffice = $this->calculateDotBeliefGraph(base64_encode($orgId),$officeId,'',2,$date);
                    $indexDotBeliefOfficeExclude = $this->calculateDotBeliefGraph(base64_encode($orgId),$officeId,'',1,$date);

                    $dotBeliefOfficeCount=0;
                    foreach ($indexDotBeliefOffice as $dotBeliefOffice) {
                        //Chk duplicate record for same day
                        $dotBeliefOfficeRecords = DB::table('dot_belief_graph')
                                ->select('id')
                                ->whereDate('date',$date)
                                ->where('orgId',$orgId)
                                ->where('beliefId',$dotBeliefOffice['id'])
                                ->where('officeId',$officeId)
                                ->whereNull('departmentId')
                                ->first();
                        if($dotBeliefOfficeRecords){
                            //No need to insert
                        }else{
                            $insertDotBeliefOfficeArray = array(
                                'date'              => $date,
                                'orgId'             => $orgId,
                                'officeId'          => $officeId,
                                'beliefId'          => $dotBeliefOffice['id'],
                                'with_weekend'      => $dotBeliefOffice['rating'],
                                'without_weekend'   => $indexDotBeliefOfficeExclude[$dotBeliefOfficeCount]['rating'],
                                'created_at'        => date('Y-m-d H:i:s')
                            );
                            $indexDotBeliefOfficeRecords = DB::table('dot_belief_graph')->insertGetId($insertDotBeliefOfficeArray);
                        }    
                        $dotBeliefOfficeCount++;
                    }

                    if (!empty($dots)) {
                        $dotBeliefs = DB::table('dots_beliefs')
                            ->where('status','Active')
                            ->where('dotId',$dots->id)
                            ->whereDate('created_at','<=',$date)
                            ->orderBy('id','ASC')
                            ->get();
                        foreach ($dotBeliefs as $bValue){
                            $beliefId   = $bValue->id;
                            $dotId      = $dots->id;
                            $indexDotValuesOffice = $this->calculateDotValuesGraph(base64_encode($orgId),$officeId,'',2,$beliefId,$dotId,$date);
                            $indexDotValuesOfficeExclude = $this->calculateDotValuesGraph(base64_encode($orgId),$officeId,'',1,$beliefId,$dotId,$date);
                            $dotValuesOfficeCount=0;
                            foreach ($indexDotValuesOffice as $dotValuesOffice) {
                                //Chk duplicate record for same day
                                $dotValuesOfficeRecords = DB::table('dot_values_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('beliefId',$beliefId)
                                        ->where('valueId',$dotValuesOffice['id'])
                                        ->where('officeId',$officeId)
                                        ->whereNull('departmentId')
                                        ->first();
                                if($dotValuesOfficeRecords){
                                    //No need to insert
                                }else{
                                    $insertDotValuesOfficeArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'officeId'         => $officeId,
                                        'beliefId'         => $beliefId,
                                        'valueId'          => $dotValuesOffice['id'],
                                        'with_weekend'     => $dotValuesOffice['rating'],
                                        'without_weekend'  => $indexDotValuesOfficeExclude[$dotValuesOfficeCount]['rating'],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexDotValuesOfficeRecords = DB::table('dot_values_graph')->insertGetId($insertDotValuesOfficeArray);
                                }    
                                $dotValuesOfficeCount++;
                            }
                        }
                    }

                    //Get departments
                    $departments = DB::table('departments')
                        //->select('id')
                        ->where('orgId',$orgId)
                        ->where('officeId',$officeId)
                        ->whereDate('created_at', '<=',$date)
                        ->where('status','Active') 
                        ->get();
                    if($departments){
                        foreach ($departments as $department) {
                            $departmentId = $department->id;
                            $allDepartmentId = $department->departmentId;
                           
                            //Get index
                            $indexDotBeliefAllDept = $this->calculateDotBeliefGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            $indexDotBeliefAllDeptExclude = $this->calculateDotBeliefGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            
                            $dotBeliefAllDeptCount=0;
                            foreach ($indexDotBeliefAllDept as $dotBeliefAllDept) {
                                //Chk duplicate record for same day
                                $dotBeliefAllDeptRecords = DB::table('dot_belief_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('beliefId',$dotBeliefAllDept['id'])
                                        ->whereNull('officeId')
                                        ->where('departmentId',$allDepartmentId)
                                        ->first();
                                if($dotBeliefAllDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertDotBeliefAllDeptArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'departmentId'     => $allDepartmentId,
                                        'beliefId'       => $dotBeliefAllDept['id'],
                                        'with_weekend'     => $dotBeliefAllDept['rating'],
                                        'without_weekend'  => $indexDotBeliefAllDeptExclude[$dotBeliefAllDeptCount]['rating'],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexDotBeliefAllDeptRecords = DB::table('dot_belief_graph')->insertGetId($insertDotBeliefAllDeptArray);
                                }    
                                $dotBeliefAllDeptCount++;
                            }

                            if (!empty($dots)) {
                                $dotBeliefs = DB::table('dots_beliefs')
                                    ->where('status','Active')
                                    ->where('dotId',$dots->id)
                                    ->whereDate('created_at','<=',$date)
                                    ->orderBy('id','ASC')
                                    ->get();
                                foreach ($dotBeliefs as $bValue){
                                    $beliefId   = $bValue->id;
                                    $dotId      = $dots->id;
                                    $indexDotValuesAllDept = $this->calculateDotValuesGraph(base64_encode($orgId),'',$allDepartmentId,2,$beliefId,$dotId,$date);
                                    $indexDotValuesAllDeptExclude = $this->calculateDotValuesGraph(base64_encode($orgId),'',$allDepartmentId,1,$beliefId,$dotId,$date);

                                    $dotValuesAllDeptCount=0;
                                    foreach ($indexDotValuesAllDept as $dotValuesAllDept) {
                                        //Chk duplicate record for same day
                                        $dotValuesAllDeptRecords = DB::table('dot_values_graph')
                                                ->select('id')
                                                ->whereDate('date',$date)
                                                ->where('orgId',$orgId)
                                                ->where('beliefId',$beliefId)
                                                ->where('valueId',$dotValuesAllDept['id'])
                                                ->whereNull('officeId')
                                                ->where('departmentId',$allDepartmentId)
                                                ->first();
                                        if($dotValuesAllDeptRecords){
                                            //No need to insert
                                        }else{
                                            $insertdotValuesAllDeptArray = array(
                                                'date'             => $date,
                                                'orgId'            => $orgId,
                                                'departmentId'     => $allDepartmentId,
                                                'beliefId'         => $beliefId,
                                                'valueId'          => $dotValuesAllDept['id'],
                                                'with_weekend'     => $dotValuesAllDept['rating'],
                                                'without_weekend'  => $indexDotValuesAllDeptExclude[$dotValuesAllDeptCount]['rating'],
                                                'created_at'       => date('Y-m-d H:i:s')
                                            );
                                            $indexDotValuesAllDeptRecords = DB::table('dot_values_graph')->insertGetId($insertdotValuesAllDeptArray);
                                        }    
                                        $dotValuesAllDeptCount++;
                                    }
                                }
                            }

                            $indexDotBeliefDept = $this->calculateDotBeliefGraph(base64_encode($orgId),$officeId,$departmentId,2,$date);
                            $indexDotBeliefDeptExclude = $this->calculateDotBeliefGraph(base64_encode($orgId),$officeId,$departmentId,1,$date);

                            $dotBeliefDeptCount=0;
                            foreach ($indexDotBeliefDept as $dotBeliefDept) {
                                //Chk duplicate record for same day
                                $dotBeliefDeptRecords = DB::table('dot_belief_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('beliefId',$dotBeliefDept['id'])
                                        ->where('officeId',$officeId)
                                        ->where('departmentId',$departmentId)
                                        ->first();
                                if($dotBeliefDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertDotBeliefDeptArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'officeId'         => $officeId,
                                        'departmentId'     => $departmentId,
                                        'beliefId'       => $dotBeliefDept['id'],
                                        'with_weekend'     => $dotBeliefDept['rating'],
                                        'without_weekend'  => $indexDotBeliefDeptExclude[$dotBeliefDeptCount]['rating'],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexDotBeliefDeptRecords = DB::table('dot_belief_graph')->insertGetId($insertDotBeliefDeptArray);
                                }    
                                $dotBeliefDeptCount++;
                            }

                            if (!empty($dots)) {
                                $dotBeliefs = DB::table('dots_beliefs')
                                    ->where('status','Active')
                                    ->where('dotId',$dots->id)
                                    ->whereDate('created_at','<=',$date)
                                    ->orderBy('id','ASC')
                                    ->get();
                                foreach ($dotBeliefs as $bValue){
                                    $beliefId   = $bValue->id;
                                    $dotId      = $dots->id;
                                    $indexDotValuesDept = $this->calculateDotValuesGraph(base64_encode($orgId),$officeId,$departmentId,2,$beliefId,$dotId,$date);
                                    $indexDotValuesDeptExclude = $this->calculateDotValuesGraph(base64_encode($orgId),$officeId,$departmentId,1,$beliefId,$dotId,$date);

                                    $dotValuesDeptCount=0;
                                    foreach ($indexDotValuesDept as $dotValuesDept) {
                                        //Chk duplicate record for same day
                                        $dotValuesDeptRecords = DB::table('dot_values_graph')
                                                ->select('id')
                                                ->whereDate('date',$date)
                                                ->where('orgId',$orgId)
                                                ->where('beliefId',$beliefId)
                                                ->where('valueId',$dotValuesDept['id'])
                                                ->where('officeId',$officeId)
                                                ->where('departmentId',$departmentId)
                                                ->first();
                                        if($dotValuesDeptRecords){
                                            //No need to insert
                                        }else{
                                            $insertdotValuesDeptArray = array(
                                                'date'             => $date,
                                                'orgId'            => $orgId,
                                                'officeId'         => $officeId,
                                                'departmentId'     => $departmentId,
                                                'beliefId'         => $beliefId,
                                                'valueId'          => $dotValuesDept['id'],
                                                'with_weekend'     => $dotValuesDept['rating'],
                                                'without_weekend'  => $indexDotValuesDeptExclude[$dotValuesDeptCount]['rating'],
                                                'created_at'       => date('Y-m-d H:i:s')
                                            );
                                            $indexDotValuesDeptRecords = DB::table('dot_values_graph')->insertGetId($insertdotValuesDeptArray);
                                        }    
                                        $dotValuesDeptCount++;
                                    }
                                }
                            }
                        }   
                    }
                }
            }
        }
    }
}

public function calculateDotBeliefGraph($orgId,$officeId,$departmentId,$isExclude,$date,$beliefId=false)
{
    $orgId = base64_decode($orgId);
    $departmentId = $departmentId;
    $officeId = $officeId;

    //Get info of org
    $org = DB::table('organisations')->where('id',$orgId)->first();

    $dots = DB::table('dots')
            ->whereDate('created_at','<=',$date)
            ->where('orgId',$orgId)
            ->first();

    $beliefArray = array();
    if (!empty($dots)) {
        $dotBeliefsQuery = DB::table('dots_beliefs')
            ->where('status','Active')
            ->where('dotId',$dots->id)
            ->whereDate('created_at','<=',$date);
        if (!empty($beliefId)) {
            $dotBeliefsQuery->where('id',$beliefId);
        }
        $dotBeliefs = $dotBeliefsQuery->orderBy('id','ASC')->get();

        foreach ($dotBeliefs as $key => $bValue){
            $bquery = DB::table('dot_values_ratings')
                    ->leftjoin('users','users.id','dot_values_ratings.userId')
                    ->where('dot_values_ratings.beliefId',$bValue->id)
                    ->where('users.status','Active')
                    ->where('dot_values_ratings.status','Active');

            if($isExclude==1){
                $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
                $currentDate = $date;
                //Get weekends
                $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
                $bquery->whereNotIn(DB::raw("(DATE_FORMAT(dot_values_ratings.created_at,'%Y-%m-%d'))"),$getWeekendDates);
            }
            if($date){
                $bquery->whereDate('dot_values_ratings.created_at','<=',$date);
            }
            if(!empty($officeId) && empty($departmentId)){
                $bquery->where('users.officeId',$officeId);
            }elseif(!empty($officeId) && !empty($departmentId)){
                $bquery->where('users.officeId',$officeId);
                $bquery->where('users.departmentId',$departmentId);
            }elseif(empty($officeId) && !empty($departmentId)){   
                $bquery->leftJoin('departments','departments.id','users.departmentId')
                    ->where('departments.status','Active')
                    ->where('departments.departmentId',$departmentId);
            }
            $bRatings = $bquery->avg('ratings');
            
            $beliefRatings['rating'] = 0;
            if($bRatings){
                $beliefRatings['rating'] = round(($bRatings-1),2);
            }
            $beliefRatings['id'] = $bValue->id;
            array_push($beliefArray, $beliefRatings);
        }
    }
    return $beliefArray;
}

public function calculateDotValuesGraph($orgId,$officeId,$departmentId,$isExclude,$beliefId,$dotId,$date,$valueId=false)
{
    $orgId = base64_decode($orgId);
    $departmentId = $departmentId;
    $officeId = $officeId;

    //Get info of org
    $org = DB::table('organisations')->where('id',$orgId)->first();

    // $dots = DB::table('dots')
    //         ->whereDate('created_at','<=',$date)
    //         ->where('orgId',$orgId)
    //         ->first();

    // $beliefArray = array();
    // if (!empty($dots)) {
    //     $dotBeliefs = DB::table('dots_beliefs')
    //         ->where('status','Active')
    //         ->where('dotId',$dots->id)
    //         ->whereDate('created_at','<=',$date)
    //         ->orderBy('id','ASC')
    //         ->get();

    //     foreach ($dotBeliefs as $key => $bValue){
    $dotValuesQuery = DB::table('dots_values')
        ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
        ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
        ->where('dots_values.status','Active')
        ->where('dot_value_list.status','Active')
        ->where('dots_values.beliefId',$beliefId)
        ->whereDate('dots_values.created_at','<=',$date);
    if (!empty($valueId)) {
        $dotValuesQuery->where('dots_values.id',$valueId);
    }
    $dotValues = $dotValuesQuery->orderBy('dot_value_list.name','ASC')->get();

    $valuesArray = array();
    foreach ($dotValues as $key => $vValue) {
        $query = DB::table('dot_values_ratings')
            ->leftjoin('users','users.id','dot_values_ratings.userId')
            ->where('users.status','Active')
            ->where('valueId', $vValue->id)
            ->where('beliefId',$beliefId)
            ->where('dotId',$dotId);     
        if($isExclude==1){
            $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
            $currentDate = $date;
            //Get weekends
            $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($orgCreatedDate, $currentDate);
            $query->whereNotIn(DB::raw("(DATE_FORMAT(dot_values_ratings.created_at,'%Y-%m-%d'))"),$getWeekendDates);
        }
        if($date){
            $query->whereDate('dot_values_ratings.created_at','<=',$date);
        }
        if(!empty($officeId) && empty($departmentId)){
            $query->where('users.officeId',$officeId);
        }elseif(!empty($officeId) && !empty($departmentId)){
            $query->where('users.officeId',$officeId);
            $query->where('users.departmentId',$departmentId);
        }elseif(empty($officeId) && !empty($departmentId)){   
            $query->leftJoin('departments','departments.id','users.departmentId')
                ->where('departments.status','Active')
                ->where('departments.departmentId',$departmentId);
        }   
        $vRatings = $query->avg('ratings');

        $vResult['id']      = $vValue->id;
        $vResult['rating'] = 0;
        if($vRatings){
            $vResult['rating'] = round(($vRatings-1),2);
        }
         array_push($valuesArray, $vResult);
    }
    // $beliefRatings['valueArray'] = $valuesArray;
    // $beliefRatings['beliefId'] = $bValue->id;
    // array_push($beliefArray, $beliefRatings);
        //}
    //}
    return $valuesArray;
}

public function getPersonalityTypeIndexReportByPastDate(){
    $year  = '2019';
    $month = '11';  
    $day = '30';
    //Get total days of month and year
    //$totalDays = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    // $totalDays = 20;
    // for ($day = 16; $day <= $totalDays; $day++) {
        $makeDate = $year."-".$month."-".$day;
        $this->getPersonalityTypeIndexReport($makeDate);
    // }
    echo "Done for :".$month." - ".$year." days ".$day;
    die('<br /> Finally Done.');
}

public function getPersonalityTypeIndexReport($date=false){

    if($date){
        $date = $date;
    }else{
        $date =date('Y-m-d',strtotime('last day of this month'));
    }
    //Get all organization
    $allOrganizations = DB::table('organisations')
                        ->where('status','Active') 
                        ->select('id')
                        ->whereDate('created_at', '<=',$date)
                        ->get();
    if($allOrganizations){
        foreach($allOrganizations as $organization){
            $orgId = $organization->id;

            $indexPersonalityTypeOrg = $this->calculatePersonalityTypeGraph(base64_encode($orgId),'','',2,$date);
            $indexPersonalityTypeOrgExclude = $this->calculatePersonalityTypeGraph(base64_encode($orgId),'','',1,$date);
            // $indexPersonalityTypeOrg = $this->calculatePersonalityTypeGraph(base64_encode(8),'','',2,$date);
            // $indexPersonalityTypeOrgExclude = $this->calculatePersonalityTypeGraph(base64_encode(8),'','',1,$date);

            // $perTypeOrgCount=0;
            foreach ($indexPersonalityTypeOrg as $perTypeOrgKey => $perTypeOrg) {
                //Chk duplicate record for same day
                $perTypeOrgRecords = DB::table('cot_personality_type_dashboard_graph')
                        ->select('id')
                        ->whereDate('date',$date)
                        ->where('orgId',$orgId)
                        ->where('categoryId',$perTypeOrgKey)
                        ->whereNull('officeId')
                        ->whereNull('departmentId')
                        ->first();

                if($perTypeOrgRecords){
                    //No need to insert
                }else{
                    $insertPerTypeOrgArray = array(
                        'date'             => $date,
                        'orgId'            => $orgId,
                        'categoryId'       => $perTypeOrgKey,
                        'with_weekend'     => $perTypeOrg,
                        'without_weekend'  => $indexPersonalityTypeOrgExclude[$perTypeOrgKey],
                        'created_at'       => date('Y-m-d H:i:s')
                    );
                    $indexPerTypeOrgRecords = DB::table('cot_personality_type_dashboard_graph')->insertGetId($insertPerTypeOrgArray);
                }    
                // $perTypeOrgCount++;
            }
       
            //Get offices of organization
            $offices = DB::table('offices')
                        ->select('id')
                        ->where('status','Active') 
                        ->whereDate('created_at', '<=',$date)
                        ->where('orgId',$orgId)->get();
            if($offices){
                foreach($offices as $office){
                    $officeId = $office->id;
                    $indexPersonalityTypeOffice = $this->calculatePersonalityTypeGraph(base64_encode($orgId),$officeId,'',2,$date);
                    $indexPersonalityTypeOfficeExclude = $this->calculatePersonalityTypeGraph(base64_encode($orgId),$officeId,'',1,$date);

                    // $perTypeOfficeCount=0;
                    foreach ($indexPersonalityTypeOffice as $perTypeOfficeKey => $perTypeOffice) {
                        //Chk duplicate record for same day
                        $perTypeOfficeRecords = DB::table('cot_personality_type_dashboard_graph')
                                ->select('id')
                                ->whereDate('date',$date)
                                ->where('orgId',$orgId)
                                ->where('categoryId',$perTypeOfficeKey)
                                ->where('officeId',$officeId)
                                ->whereNull('departmentId')
                                ->first();
                        if($perTypeOfficeRecords){
                            //No need to insert
                        }else{
                            $insertPerTypeOfficeArray = array(
                                'date'             => $date,
                                'orgId'            => $orgId,
                                'officeId'         => $officeId,
                                'categoryId'       => $perTypeOfficeKey,
                                'with_weekend'     => $perTypeOffice,
                                'without_weekend'  => $indexPersonalityTypeOfficeExclude[$perTypeOfficeKey],
                                'created_at'       => date('Y-m-d H:i:s')
                            );
                            $indexPerTypeOfficeRecords = DB::table('cot_personality_type_dashboard_graph')->insertGetId($insertPerTypeOfficeArray);
                        }    
                        // $perTypeOfficeCount++;
                    }

                    //Get departments
                    $departments = DB::table('departments')
                        //->select('id')
                        ->where('orgId',$orgId)
                        ->where('officeId',$officeId)
                        ->whereDate('created_at', '<=',$date)
                        ->where('status','Active') 
                        ->get();
                    if($departments){
                        foreach ($departments as $department) {
                            $departmentId = $department->id;
                            $allDepartmentId = $department->departmentId;
                           
                            //Get index
                            $indexPersonalityTypeAllDept = $this->calculatePersonalityTypeGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            $indexPersonalityTypeAllDeptExclude = $this->calculatePersonalityTypeGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            
                            // $perTypeAllDeptCount=0;
                            foreach ($indexPersonalityTypeAllDept as $perTypeAllDeptKey => $perTypeAllDept) {
                                //Chk duplicate record for same day
                                $perTypeAllDeptRecords = DB::table('cot_personality_type_dashboard_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('categoryId',$perTypeAllDeptKey)
                                        ->whereNull('officeId')
                                        ->where('departmentId',$allDepartmentId)
                                        ->first();
                                if($perTypeAllDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertPerTypeAllDeptArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'departmentId'     => $allDepartmentId,
                                        'categoryId'       => $perTypeAllDeptKey,
                                        'with_weekend'     => $perTypeAllDept,
                                        'without_weekend'  => $indexPersonalityTypeAllDeptExclude[$perTypeAllDeptKey],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexPerTypeAllDeptRecords = DB::table('cot_personality_type_dashboard_graph')->insertGetId($insertPerTypeAllDeptArray);
                                }    
                                // $perTypeAllDeptCount++;
                            }

                            $indexPersonalityTypeDept = $this->calculatePersonalityTypeGraph(base64_encode($orgId),$officeId,$departmentId,2,$date);
                            $indexPersonalityTypeDeptExclude = $this->calculatePersonalityTypeGraph(base64_encode($orgId),$officeId,$departmentId,1,$date);

                            // $perTypeDeptCount=0;
                            foreach ($indexPersonalityTypeDept as $perTypeDeptKey => $perTypeDept) {
                                //Chk duplicate record for same day
                                $perTypeDeptRecords = DB::table('cot_personality_type_dashboard_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('categoryId',$perTypeDeptKey)
                                        ->where('officeId',$officeId)
                                        ->where('departmentId',$departmentId)
                                        ->first();
                                if($perTypeDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertPerTypeDeptArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'officeId'         => $officeId,
                                        'departmentId'     => $departmentId,
                                        'categoryId'       => $perTypeDeptKey,
                                        'with_weekend'     => $perTypeDept,
                                        'without_weekend'  => $indexPersonalityTypeDeptExclude[$perTypeDeptKey],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexPerTypeDeptRecords = DB::table('cot_personality_type_dashboard_graph')->insertGetId($insertPerTypeDeptArray);
                                }    
                                // $perTypeDeptCount++;
                            }
                        }   
                    }
                }
            }
        }
    }
}

public function calculatePersonalityTypeGraph($orgId,$officeId,$departmentId,$isExclude,$date,$categoryId=false)
{
    $orgId = base64_decode($orgId);
    $departmentId = $departmentId;
    $officeId = $officeId;

    //Get weekends
    $startDate=DB::table('cot_functional_lens_answers')
        ->select('created_at')
        ->where('orgId',$orgId)
        ->whereDate('cot_functional_lens_answers.created_at','<=',$date)
        ->orderBy('created_at','ASC')
        ->first();
    if (count($startDate) == 0) {
        $startDate = $date;
        //$startDate = date('Y-m-d');
    }else{
        $startDate = date('Y-m-d',strtotime($startDate->created_at));
    }
    $currentDate = $date;
    //$currentDate = date('Y-m-d');

    $getWeekendDates = $this->getDatesFromRange($startDate, $currentDate);

    $cotFunResultArray = array();

    $query = DB::table('cot_functional_lens_answers')
        ->select('cot_functional_lens_answers.userId AS id')
        ->leftjoin('users','users.id','cot_functional_lens_answers.userId') 
        ->where('users.status','Active')
        ->where('cot_functional_lens_answers.status','Active');
    if($date){
        $query->whereDate('cot_functional_lens_answers.created_at','<=',$date);
    }
    if($isExclude==1){
      $query->whereNotIn(DB::raw("(DATE_FORMAT(cot_functional_lens_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }
    if(!empty($orgId)){
      $query->where('users.orgId',$orgId);
    } 
    if(!empty($officeId) && empty($departmentId)){
      $query->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)){
      $query->where('users.officeId',$officeId);
      $query->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)){
      $query->leftJoin('departments','departments.id','users.departmentId')
          ->where('departments.status','Active')   
          ->where('departments.departmentId',$departmentId);
    }
    $usersList = $query->groupBy('cot_functional_lens_answers.userId')->get();

    foreach ($usersList as $key => $userValue){

      $variable = app('App\Http\Controllers\Admin\CommonController')->getCotFunLensInitialValue();   

      $initialValEIArray = array();
      foreach ($variable as $value){
        $value1 = $value[0];
        $value2 = $value[1];

        $countEQuery = DB::table('cot_functional_lens_answers AS cfla')
          ->select('cflqo.option_name AS optionName')
          ->leftJoin('cot_functional_lens_question_options AS cflqo','cflqo.id','cfla.optionId')
          ->leftjoin('users','users.id','cfla.userId') 
          ->where('users.status','Active')
          ->where('cfla.status','Active')
          ->where('cflqo.status','Active')
          ->where('cfla.userId',$userValue->id)
          ->where('cflqo.initial_value_id',$value1);
        if($date){
            $countEQuery->whereDate('cfla.created_at','<=',$date);
        }
        if($isExclude==1){
          $countEQuery->whereNotIn(DB::raw("(DATE_FORMAT(cfla.created_at,'%Y-%m-%d'))"),$getWeekendDates);
        }
        if(!empty($orgId)){
          $countEQuery->where('users.orgId',$orgId);
        } 
        if(!empty($officeId) && empty($departmentId)){
          $countEQuery->where('users.officeId',$officeId);
        }elseif(!empty($officeId) && !empty($departmentId)){
          $countEQuery->where('users.officeId',$officeId);
          $countEQuery->where('users.departmentId',$departmentId);
        }elseif(empty($officeId) && !empty($departmentId)){
          $countEQuery->leftJoin('departments','departments.id','users.departmentId')
            ->where('departments.status','Active')
            ->where('departments.departmentId',$departmentId);
        } 
        $countE = $countEQuery->get();

        $countIQuery = DB::table('cot_functional_lens_answers AS cfla')
          ->select('cflqo.option_name AS optionName')
          ->leftJoin('cot_functional_lens_question_options AS cflqo','cflqo.id','cfla.optionId')
          ->leftjoin('users','users.id','cfla.userId') 
          ->where('users.status','Active')
          ->where('cfla.status','Active')
          ->where('cflqo.status','Active')
          ->where('cfla.userId',$userValue->id)
          ->where('cflqo.initial_value_id',$value2);
        if($date){
            $countIQuery->whereDate('cfla.created_at','<=',$date);
        }
        if($isExclude==1){
          $countIQuery->whereNotIn(DB::raw("(DATE_FORMAT(cfla.created_at,'%Y-%m-%d'))"),$getWeekendDates);
        }
        if(!empty($orgId)){
          $countIQuery->where('users.orgId',$orgId);
        } 
        if(!empty($officeId) && empty($departmentId)){
          $countIQuery->where('users.officeId',$officeId);
        }elseif(!empty($officeId) && !empty($departmentId)){
          $countIQuery->where('users.officeId',$officeId);
          $countIQuery->where('users.departmentId',$departmentId);
        }elseif(empty($officeId) && !empty($departmentId)){
          $countIQuery->leftJoin('departments','departments.id','users.departmentId')
            ->where('departments.status','Active')
            ->where('departments.departmentId',$departmentId);
        } 
        $countI = $countIQuery->get();

        $initialValEI  ='';
        if(count($countE) > count($countI)){
          $initialValEI  =  $value1;
        }else if (count($countE) < count($countI)){
          $initialValEI  =  $value2;    
        }else if(count($countE) == count($countI)){     
          $initialValEI  =  $value1;
        } 
        array_push($initialValEIArray, $initialValEI);
      }

      $valueTypeArray = array();
      $valueTypeArray['value'] = array();

      if(!empty($initialValEIArray)){
           $valueTypeArray = array('value'=>$initialValEIArray);
      }

      $matchValueArr = array();
      for($i=0; $i < count($valueTypeArray['value']); $i++){
          $valuesKey = '';
          $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$valueTypeArray['value'][$i])->first();  

          if(!empty($table)){
              $valuesKey = $table->value;
          }
          array_push($matchValueArr, $valuesKey); 
      }

      //remove last and first string
      $tribeMatchValue = json_encode(array_slice($valueTypeArray['value'], 1, -1));

      $tribeTipsList = DB::table('cot_functional_lens_tribe_tips_records')->where('value',$tribeMatchValue)->get();

      $tribeTipsArray = array();
      foreach ($tribeTipsList as $ttvalue){
          $tTips['value']  = $ttvalue->value;       
          array_push($cotFunResultArray, $tTips);
      }
    }

    $ST = 0;
    $SF = 0;
    $NF = 0;
    $NT = 0;

    foreach ($cotFunResultArray as $finalArrayValue){ 
        if ($finalArrayValue['value']=='["7","9"]'){
            $ST++;
        }elseif ($finalArrayValue['value']=='["7","10"]'){
            $SF++;
        }elseif ($finalArrayValue['value']=='["8","10"]'){
            $NF++;
        }elseif ($finalArrayValue['value']=='["8","9"]'){
            $NT++;
        }
    }

    $totalUser = count($usersList);
    if (!empty($totalUser)){
        $stPercent = ($ST/$totalUser)*100;
        $sfPercent = ($SF/$totalUser)*100;
        $nfPercent = ($NF/$totalUser)*100;
        $ntPercent = ($NT/$totalUser)*100;
    }else{
        $stPercent = 0;
        $sfPercent = 0;
        $nfPercent = 0;
        $ntPercent = 0;
    }

    // $funcLensPercentageArray = array(
    //   'st' => round($stPercent, 2),
    //   'nt' => round($ntPercent, 2),
    //   'nf' => round($nfPercent, 2),
    //   'sf' => round($sfPercent, 2)
    // );

    $funcLensPercentageArray = array(
      '1' => round($stPercent, 2),
      '2' => round($ntPercent, 2),
      '3' => round($nfPercent, 2),
      '4' => round($sfPercent, 2)
    );

    if (!empty($categoryId)) {
        $perTypeArray = array();
        foreach ($funcLensPercentageArray as $key => $value) {
            if ($categoryId == $key) {
                // $funcLensPercentageArray = $value;
                array_push($perTypeArray, $value);
            }
        }
        $funcLensPercentageArray = $perTypeArray;
    }

    // $tribeTipsListKey = DB::table('cot_functional_lens_tribe_tips_records')->where('status','Active')->orderBy('id','ASC')->get();

    // $keyNameArray = array();

    // foreach ($tribeTipsListKey as $value1){
    //     $reportKeyArray = json_decode($value1->value);

    //     $keyArr = array();
    //     foreach ($reportKeyArray as $value){
    //         $valuesKey = '';
    //         $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$value)->first();  

    //         if(!empty($table)){
    //           $valuesKey = $table->value;
    //         }   
    //         array_push($keyArr, $valuesKey); 
    //     }
    //   array_push($keyNameArray, $keyArr);
    // }

    // $keyRecord = array();
    // foreach ($keyNameArray as $value) {
    //   $valueRecord = implode(' ', $value);
    //   array_push($keyRecord, $valueRecord);
    // }

    // $result = array_map(function ($keys, $values) {
    //   return array_combine(
    //     ['name', 'percentage'],
    //     [$keys, $values]
    //   );
    // }, $keyRecord, $funcLensPercentageArray);

    // $personalityTypeFinalArray = collect($result);

    return $funcLensPercentageArray;
}

//Cron for happy index
public function getHappyIndexReportByPastDate(){
    $year  = '2019';
    $month = '10';  
    //Get total days of month and year
    // $totalDays = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    $totalDays = 31;
    for ($day = 1; $day <= $totalDays; $day++) {
        $makeDate = $year."-".$month."-".$day;
        $this->getHappyIndexReport($makeDate);
    }
    echo "Done for :".$month." - ".$year." days ".$totalDays;
    die('<br /> Finally Done.');
}

public function getHappyIndexReport($date=false){

    if($date){
        $date = $date;
    }else{
        $date =date('Y-m-d',strtotime('-1 days'));
    }

    //Get all organization
    $allOrganizations = DB::table('organisations')
                        ->where('status','Active') 
                        ->select('id')
                        ->whereDate('created_at', '<=',$date)
                        ->get();
    if($allOrganizations){
        foreach($allOrganizations as $organization){
            $orgId = $organization->id;
            $indexHappyIndexOrg = $this->calculateHappyIndexGraph(base64_encode($orgId),'','',2,$date);
            $indexHappyIndexOrgExclude = $this->calculateHappyIndexGraph(base64_encode($orgId),'','',1,$date);
                        
            $happyIndexOrgCount=0;
            foreach ($indexHappyIndexOrg as $happyIndexOrg) {
                //Chk duplicate record for same day
                $happyIndexOrgRecords = DB::table('happy_index_dashboard_graph')
                        ->select('id')
                        ->whereDate('date',$date)
                        ->where('orgId',$orgId)
                        ->where('categoryId',$happyIndexOrg['id'])
                        ->whereNull('officeId')
                        ->whereNull('departmentId')
                        ->first();
                if($happyIndexOrgRecords){
                    //No need to insert
                }else{
                    $insertHappyIndexOrgArray = array(
                        'date'             => $date,
                        'orgId'            => $orgId,
                        'categoryId'       => $happyIndexOrg['id'],
                        'with_weekend'     => $happyIndexOrg['percentage'],
                        'without_weekend'  => $indexHappyIndexOrgExclude[$happyIndexOrgCount]['percentage'],
                        'created_at'       => date('Y-m-d H:i:s')
                    );
                    $indexHappyIndexOrgRecords = DB::table('happy_index_dashboard_graph')->insertGetId($insertHappyIndexOrgArray);
                }    
                $happyIndexOrgCount++;
            }
       
            //Get offices of organization
            $offices = DB::table('offices')
                        ->select('id')
                        ->where('orgId',$orgId)
                        ->where('status','Active') 
                        ->whereDate('created_at', '<=',$date)
                        ->get();
            if($offices){
                foreach($offices as $office){
                    $officeId = $office->id;
                    $indexHappyIndexOffice = $this->calculateHappyIndexGraph(base64_encode($orgId),$officeId,'',2,$date);
                    $indexHappyIndexOfficeExclude = $this->calculateHappyIndexGraph(base64_encode($orgId),$officeId,'',1,$date);

                    $happyIndexOfficeCount=0;
                    foreach ($indexHappyIndexOffice as $happyIndexOffice) {
                        //Chk duplicate record for same day
                        $happyIndexOfficeRecords = DB::table('happy_index_dashboard_graph')
                                ->select('id')
                                ->whereDate('date',$date)
                                ->where('orgId',$orgId)
                                ->where('categoryId',$happyIndexOffice['id'])
                                ->where('officeId',$officeId)
                                ->whereNull('departmentId')
                                ->first();
                        if($happyIndexOfficeRecords){
                            //No need to insert
                        }else{
                            $insertHappyIndexOfficeArray = array(
                                'date'             => $date,
                                'orgId'            => $orgId,
                                'officeId'         => $officeId,
                                'categoryId'       => $happyIndexOffice['id'],
                                'with_weekend'     => $happyIndexOffice['percentage'],
                                'without_weekend'  => $indexHappyIndexOfficeExclude[$happyIndexOfficeCount]['percentage'],
                                'created_at'       => date('Y-m-d H:i:s')
                            );
                            $indexHappyIndexOfficeRecords = DB::table('happy_index_dashboard_graph')->insertGetId($insertHappyIndexOfficeArray);
                        }    
                        $happyIndexOfficeCount++;
                    }

                    //Get departments
                    $departments = DB::table('departments')
                        //->select('id')
                        ->where('orgId',$orgId)
                        ->where('officeId',$officeId)
                        ->whereDate('created_at', '<=',$date)
                        ->where('status','Active') 
                        ->get();
                    if($departments){
                        foreach ($departments as $department) {
                            $departmentId = $department->id;
                            $allDepartmentId = $department->departmentId;
                           
                            //Get index
                            $indexHappyIndexAllDept = $this->calculateHappyIndexGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            $indexHappyIndexAllDeptExclude = $this->calculateHappyIndexGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            
                            $happyIndexAllDeptCount=0;
                            foreach ($indexHappyIndexAllDept as $happyIndexAllDept) {
                                //Chk duplicate record for same day
                                $happyIndexAllDeptRecords = DB::table('happy_index_dashboard_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('categoryId',$happyIndexAllDept['id'])
                                        ->whereNull('officeId')
                                        ->where('departmentId',$allDepartmentId)
                                        ->first();
                                if($happyIndexAllDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertHappyIndexAllDeptArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'departmentId'     => $allDepartmentId,
                                        'categoryId'       => $happyIndexAllDept['id'],
                                        'with_weekend'     => $happyIndexAllDept['percentage'],
                                        'without_weekend'  => $indexHappyIndexAllDeptExclude[$happyIndexAllDeptCount]['percentage'],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexHappyIndexAllDeptRecords = DB::table('happy_index_dashboard_graph')->insertGetId($insertHappyIndexAllDeptArray);
                                }    
                                $happyIndexAllDeptCount++;
                            }

                            $indexHappyIndexDept = $this->calculateHappyIndexGraph(base64_encode($orgId),$officeId,$departmentId,2,$date);
                            $indexHappyIndexDeptExclude = $this->calculateHappyIndexGraph(base64_encode($orgId),$officeId,$departmentId,1,$date);

                            $happyIndexDeptCount=0;
                            foreach ($indexHappyIndexDept as $happyIndexDept) {
                                //Chk duplicate record for same day
                                $happyIndexDeptRecords = DB::table('happy_index_dashboard_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('categoryId',$happyIndexDept['id'])
                                        ->where('officeId',$officeId)
                                        ->where('departmentId',$departmentId)
                                        ->first();
                                if($happyIndexDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertHappyIndexDeptArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'officeId'         => $officeId,
                                        'departmentId'     => $departmentId,
                                        'categoryId'       => $happyIndexDept['id'],
                                        'with_weekend'     => $happyIndexDept['percentage'],
                                        'without_weekend'  => $indexHappyIndexDeptExclude[$happyIndexDeptCount]['percentage'],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexHappyIndexDeptRecords = DB::table('happy_index_dashboard_graph')->insertGetId($insertHappyIndexDeptArray);
                                }    
                                $happyIndexDeptCount++;
                            }
                        }   
                    }
                }
            }
        }
    }
}

public function calculateHappyIndexGraph($orgId,$officeId,$departmentId,$isExclude,$date,$categoryId=false)
{
    $orgId = base64_decode($orgId);
    $departmentId = $departmentId;
    $officeId = $officeId;

    //Get weekends
    $startDate=DB::table('happy_indexes')
        ->select('happy_indexes.created_at')
        ->leftJoin('users','happy_indexes.userId','users.id')
        ->where('users.orgId',$orgId)
        ->whereDate('happy_indexes.created_at','<=',$date)
        ->orderBy('happy_indexes.created_at','ASC')
        ->first();

    if(isset($startDate->created_at) && $startDate->created_at!=''){
        $startDate = date('Y-m-d',strtotime($startDate->created_at));
    }else{
        $startDate = $date;
    }
    $currentDate = $date;
    // $currentDate = date('Y-m-d');
    $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($startDate, $currentDate);

    $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();

    $totalUsersQuery = DB::table('users')
        ->where('users.status','Active')
        ->whereDate('users.created_at','<=',$date)
        ->where('users.roleId',3);
    if(!empty($orgId)){
        $totalUsersQuery->where('users.orgId',$orgId);
    } 
    if(!empty($officeId) && empty($departmentId)){
        $totalUsersQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)){
        $totalUsersQuery->where('users.officeId',$officeId);
        $totalUsersQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)){
        $totalUsersQuery->leftJoin('departments','departments.id','users.departmentId')
            ->where('departments.status','Active')
            ->where('departments.departmentId',$departmentId);
    }
    $totalUsers = $totalUsersQuery->count();

    $happyIndexResultArray = array();
    foreach ($happyIndexMoodValues as $moodVal) {
        $moodCountQuery = DB::table('happy_indexes')
            ->leftjoin('users','users.id','happy_indexes.userId')
            ->where('users.status','Active')
            ->where('happy_indexes.status','Active')
            ->where('happy_indexes.moodValue',$moodVal->id)
            ->whereDate('happy_indexes.created_at',$date);
        if(!empty($orgId)){
            $moodCountQuery->where('users.orgId',$orgId);
        }
        if(!empty($officeId) && empty($departmentId)){
            $moodCountQuery->where('users.officeId',$officeId);
        }elseif(!empty($officeId) && !empty($departmentId)){
            $moodCountQuery->where('users.officeId',$officeId);
            $moodCountQuery->where('users.departmentId',$departmentId);
        }elseif(empty($officeId) && !empty($departmentId)){
            $moodCountQuery->leftJoin('departments','departments.id','users.departmentId')
                ->where('departments.status','Active')
                ->where('departments.departmentId',$departmentId);
        }
        if($isExclude==1){
          $moodCountQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
        }
        $moodCount = $moodCountQuery->count();

        $moodCountUsers['percentage'] = 0;
        if (!empty($moodCount) && !empty($totalUsers)) {
            $moodCountUsers['percentage'] = round((($moodCount/$totalUsers)*100),2);
        }
        $moodCountUsers['id'] = $moodVal->id;
        array_push($happyIndexResultArray,$moodCountUsers);
    }
    return $happyIndexResultArray;
}

//Cron for happy index daily count
public function getHappyIndexDailyCountReportByPastDate(){
    $year  = '2020';
    $month = '02';  
    //Get total days of month and year
    // $totalDays = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    $totalDays = 5;
    for ($day = 1; $day <= $totalDays; $day++) {
        $makeDate = $year."-".$month."-".$day;
        $this->getHappyIndexDailyCountReport($makeDate);
    }
    echo "Done for :".$month." - ".$year." days ".$totalDays;
    die('<br /> Finally Done.');
}

public function getHappyIndexDailyCountReport($date=false){

    if($date){
        $date = $date;
    }else{
        $date =date('Y-m-d',strtotime('-1 days'));
    }

    //Get all organization
    $allOrganizations = DB::table('organisations')
                        ->where('status','Active') 
                        ->select('id')
                        ->whereDate('created_at', '<=',$date)
                        ->get();
    if($allOrganizations){
        foreach($allOrganizations as $organization){
            $orgId = $organization->id;
            $indexHappyIndexOrg = $this->calculateHappyIndexRespondersGraph(base64_encode($orgId),'','',2,$date);
            $indexHappyIndexOrgExclude = $this->calculateHappyIndexRespondersGraph(base64_encode($orgId),'','',1,$date);

            $happyIndexOrgCount=0;
            foreach ($indexHappyIndexOrg as $happyIndexOrg) {
                //Chk duplicate record for same day
                $happyIndexOrgRecords = DB::table('happy_index_dashboard_responders_graph')
                        ->select('id')
                        ->whereDate('date',$date)
                        ->where('orgId',$orgId)
                        ->where('categoryId',$happyIndexOrg['id'])
                        ->whereNull('officeId')
                        ->whereNull('departmentId')
                        ->first();
                if($happyIndexOrgRecords){
                    //No need to insert
                }else{
                    $insertHappyIndexOrgArray = array(
                        'date'             => $date,
                        'orgId'            => $orgId,
                        'categoryId'       => $happyIndexOrg['id'],
                        'with_weekend'     => $happyIndexOrg['percentage'],
                        'without_weekend'  => $indexHappyIndexOrgExclude[$happyIndexOrgCount]['percentage'],
                        'created_at'       => date('Y-m-d H:i:s')
                    );
                    $indexHappyIndexOrgRecords = DB::table('happy_index_dashboard_responders_graph')->insertGetId($insertHappyIndexOrgArray);
                }    
                $happyIndexOrgCount++;
            }

            //Get offices of organization
            $offices = DB::table('offices')
                        ->select('id')
                        ->where('orgId',$orgId)
                        ->where('status','Active') 
                        ->whereDate('created_at', '<=',$date)
                        ->get();
            if($offices){
                foreach($offices as $office){
                    $officeId = $office->id;
                    $indexHappyIndexOffice = $this->calculateHappyIndexRespondersGraph(base64_encode($orgId),$officeId,'',2,$date);
                    $indexHappyIndexOfficeExclude = $this->calculateHappyIndexRespondersGraph(base64_encode($orgId),$officeId,'',1,$date);

                    $happyIndexOfficeCount=0;
                    foreach ($indexHappyIndexOffice as $happyIndexOffice) {
                        //Chk duplicate record for same day
                        $happyIndexOfficeRecords = DB::table('happy_index_dashboard_responders_graph')
                                ->select('id')
                                ->whereDate('date',$date)
                                ->where('orgId',$orgId)
                                ->where('categoryId',$happyIndexOffice['id'])
                                ->where('officeId',$officeId)
                                ->whereNull('departmentId')
                                ->first();
                        if($happyIndexOfficeRecords){
                            //No need to insert
                        }else{
                            $insertHappyIndexOfficeArray = array(
                                'date'             => $date,
                                'orgId'            => $orgId,
                                'officeId'         => $officeId,
                                'categoryId'       => $happyIndexOffice['id'],
                                'with_weekend'     => $happyIndexOffice['percentage'],
                                'without_weekend'  => $indexHappyIndexOfficeExclude[$happyIndexOfficeCount]['percentage'],
                                'created_at'       => date('Y-m-d H:i:s')
                            );
                            $indexHappyIndexOfficeRecords = DB::table('happy_index_dashboard_responders_graph')->insertGetId($insertHappyIndexOfficeArray);
                        }    
                        $happyIndexOfficeCount++;
                    }

                    //Get departments
                    $departments = DB::table('departments')
                        //->select('id')
                        ->where('orgId',$orgId)
                        ->where('officeId',$officeId)
                        ->whereDate('created_at', '<=',$date)
                        ->where('status','Active') 
                        ->get();
                    if($departments){
                        foreach ($departments as $department) {
                            $departmentId = $department->id;
                            $allDepartmentId = $department->departmentId;
                           
                            //Get index
                            $indexHappyIndexAllDept = $this->calculateHappyIndexRespondersGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            $indexHappyIndexAllDeptExclude = $this->calculateHappyIndexRespondersGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            
                            $happyIndexAllDeptCount=0;
                            foreach ($indexHappyIndexAllDept as $happyIndexAllDept) {
                                //Chk duplicate record for same day
                                $happyIndexAllDeptRecords = DB::table('happy_index_dashboard_responders_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('categoryId',$happyIndexAllDept['id'])
                                        ->whereNull('officeId')
                                        ->where('departmentId',$allDepartmentId)
                                        ->first();
                                if($happyIndexAllDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertHappyIndexAllDeptArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'departmentId'     => $allDepartmentId,
                                        'categoryId'       => $happyIndexAllDept['id'],
                                        'with_weekend'     => $happyIndexAllDept['percentage'],
                                        'without_weekend'  => $indexHappyIndexAllDeptExclude[$happyIndexAllDeptCount]['percentage'],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexHappyIndexAllDeptRecords = DB::table('happy_index_dashboard_responders_graph')->insertGetId($insertHappyIndexAllDeptArray);
                                }    
                                $happyIndexAllDeptCount++;
                            }

                            $indexHappyIndexDept = $this->calculateHappyIndexRespondersGraph(base64_encode($orgId),$officeId,$departmentId,2,$date);
                            $indexHappyIndexDeptExclude = $this->calculateHappyIndexRespondersGraph(base64_encode($orgId),$officeId,$departmentId,1,$date);

                            $happyIndexDeptCount=0;
                            foreach ($indexHappyIndexDept as $happyIndexDept) {
                                //Chk duplicate record for same day
                                $happyIndexDeptRecords = DB::table('happy_index_dashboard_responders_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('orgId',$orgId)
                                        ->where('categoryId',$happyIndexDept['id'])
                                        ->where('officeId',$officeId)
                                        ->where('departmentId',$departmentId)
                                        ->first();
                                if($happyIndexDeptRecords){
                                    //No need to insert
                                }else{
                                    $insertHappyIndexDeptArray = array(
                                        'date'             => $date,
                                        'orgId'            => $orgId,
                                        'officeId'         => $officeId,
                                        'departmentId'     => $departmentId,
                                        'categoryId'       => $happyIndexDept['id'],
                                        'with_weekend'     => $happyIndexDept['percentage'],
                                        'without_weekend'  => $indexHappyIndexDeptExclude[$happyIndexDeptCount]['percentage'],
                                        'created_at'       => date('Y-m-d H:i:s')
                                    );
                                    $indexHappyIndexDeptRecords = DB::table('happy_index_dashboard_responders_graph')->insertGetId($insertHappyIndexDeptArray);
                                }    
                                $happyIndexDeptCount++;
                            }
                        }   
                    }
                }
            }
        }
    }
}

public function calculateHappyIndexRespondersGraph($orgId,$officeId,$departmentId,$isExclude,$date,$categoryId=false)
{
    $orgId = base64_decode($orgId);
    $departmentId = $departmentId;
    $officeId = $officeId;

    //Get weekends
    $startDate=DB::table('happy_indexes')
        ->select('happy_indexes.created_at')
        ->leftJoin('users','happy_indexes.userId','users.id')
        ->where('users.orgId',$orgId)
        ->whereDate('happy_indexes.created_at','<=',$date)
        ->orderBy('happy_indexes.created_at','ASC')
        ->first();

    if(isset($startDate->created_at) && $startDate->created_at!=''){
        $startDate = date('Y-m-d',strtotime($startDate->created_at));
    }else{
        $startDate = $date;
    }
    $currentDate = $date;
    // $currentDate = date('Y-m-d');
    $getWeekendDates = app('App\Http\Controllers\Admin\AdminReportController')->getDatesFromRange($startDate, $currentDate);

    $happyIndexMoodValues = DB::table('happy_index_mood_values')->get();

    $happyIndexResultArray = array();
    foreach ($happyIndexMoodValues as $moodVal) {
        $moodCountQuery = DB::table('happy_indexes')
            ->leftjoin('users','users.id','happy_indexes.userId')
            ->where('users.status','Active')
            ->where('happy_indexes.status','Active')
            ->where('happy_indexes.moodValue',$moodVal->id)
            ->whereDate('happy_indexes.created_at',$date);
        if(!empty($orgId)){
            $moodCountQuery->where('users.orgId',$orgId);
        }
        if(!empty($officeId) && empty($departmentId)){
            $moodCountQuery->where('users.officeId',$officeId);
        }elseif(!empty($officeId) && !empty($departmentId)){
            $moodCountQuery->where('users.officeId',$officeId);
            $moodCountQuery->where('users.departmentId',$departmentId);
        }elseif(empty($officeId) && !empty($departmentId)){
            $moodCountQuery->leftJoin('departments','departments.id','users.departmentId')
                ->where('departments.status','Active')
                ->where('departments.departmentId',$departmentId);
        }
        if($isExclude==1){
          $moodCountQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
        }
        $moodCount = $moodCountQuery->count();

        $moodCountUsers['percentage'] = 0;
        if (!empty($moodCount)) {
            $moodCountUsers['percentage'] = $moodCount;
        }
        $moodCountUsers['id'] = $moodVal->id;
        array_push($happyIndexResultArray,$moodCountUsers);
    }
    return $happyIndexResultArray;
}

//Cron for users daily count
public function getUsersCountReportByPastDate(){
    $year  = '2020';
    $month = '02';  
    //Get total days of month and year
    $totalDays = 5;
    for ($day = 1; $day <= $totalDays; $day++) {
        $makeDate = $year."-".$month."-".$day;
        $this->getUsersCountReport($makeDate);
    }
    echo "Done for :".$month." - ".$year." days ".$totalDays;
    die('<br /> Finally Done.');
}

public function getUsersCountReport($date=false){

    if($date){
        $date = $date;
    }else{
        $date =date('Y-m-d',strtotime('-1 days'));
    }

    //Get all organization
    $allOrganizations = DB::table('organisations')
                        ->where('status','Active') 
                        ->select('id')
                        ->whereDate('created_at', '<=',$date)
                        ->get();
    if($allOrganizations){
        foreach($allOrganizations as $organization){
            $orgId = $organization->id;
            $usersOrg = $this->calculateUsercountDaily(base64_encode($orgId),'','',$date);

            //Chk duplicate record for same day
            $userOrgRecords = DB::table('users_daily_count')
                    ->select('id')
                    ->whereDate('date',$date)
                    ->where('orgId',$orgId)
                    ->whereNull('officeId')
                    ->whereNull('departmentId')
                    ->first();
            if($userOrgRecords){
                //No need to insert
            }else{
                $insertUsersOrgArray = array(
                    'date'             => $date,
                    'orgId'            => $orgId,
                    'totalCount'       => $usersOrg,
                    'created_at'       => date('Y-m-d H:i:s')
                );
                $indexUsersOrgRecords = DB::table('users_daily_count')->insertGetId($insertUsersOrgArray);
            }    

            //Get offices of organization
            $offices = DB::table('offices')
                        ->select('id')
                        ->where('orgId',$orgId)
                        ->where('status','Active') 
                        ->whereDate('created_at', '<=',$date)
                        ->get();
            if($offices){
                foreach($offices as $office){
                    $officeId = $office->id;
                    $usersOffice = $this->calculateUsercountDaily(base64_encode($orgId),$officeId,'',$date);

                    //Chk duplicate record for same day
                    $userOfficeRecords = DB::table('users_daily_count')
                            ->select('id')
                            ->whereDate('date',$date)
                            ->where('orgId',$orgId)
                            ->where('officeId',$officeId)
                            ->whereNull('departmentId')
                            ->first();
                    if($userOfficeRecords){
                        //No need to insert
                    }else{
                        $insertUsersOfficeArray = array(
                            'date'             => $date,
                            'orgId'            => $orgId,
                            'officeId'         => $officeId,
                            'totalCount'       => $usersOffice,
                            'created_at'       => date('Y-m-d H:i:s')
                        );
                        $indexUsersOfficeRecords = DB::table('users_daily_count')->insertGetId($insertUsersOfficeArray);
                    }

                    //Get departments
                    $departments = DB::table('departments')
                        //->select('id')
                        ->where('orgId',$orgId)
                        ->where('officeId',$officeId)
                        ->whereDate('created_at', '<=',$date)
                        ->where('status','Active') 
                        ->get();
                    if($departments){
                        foreach ($departments as $department) {
                            $departmentId = $department->id;
                            $allDepartmentId = $department->departmentId;

                            //For All department
                            $usersAllDept = $this->calculateUsercountDaily(base64_encode($orgId),'',$allDepartmentId,$date);

                            //Chk duplicate record for same day
                            $userAllDeptRecords = DB::table('users_daily_count')
                                    ->select('id')
                                    ->whereDate('date',$date)
                                    ->where('orgId',$orgId)
                                    ->whereNull('officeId')
                                    ->where('departmentId',$allDepartmentId)
                                    ->first();
                            if($userAllDeptRecords){
                                //No need to insert
                            }else{
                                $insertUsersAllDeptArray = array(
                                    'date'             => $date,
                                    'orgId'            => $orgId,
                                    'departmentId'     => $allDepartmentId,
                                    'totalCount'       => $usersAllDept,
                                    'created_at'       => date('Y-m-d H:i:s')
                                );
                                $indexUsersAllDeptRecords = DB::table('users_daily_count')->insertGetId($insertUsersAllDeptArray);
                            }

                            //For office selected department
                            $usersDept = $this->calculateUsercountDaily(base64_encode($orgId),$officeId,$departmentId,$date);

                            //Chk duplicate record for same day
                            $userDeptRecords = DB::table('users_daily_count')
                                    ->select('id')
                                    ->whereDate('date',$date)
                                    ->where('orgId',$orgId)
                                    ->where('officeId',$officeId)
                                    ->where('departmentId',$departmentId)
                                    ->first();
                            if($userDeptRecords){
                                //No need to insert
                            }else{
                                $insertUsersDeptArray = array(
                                    'date'             => $date,
                                    'orgId'            => $orgId,
                                    'officeId'         => $officeId,
                                    'departmentId'     => $departmentId,
                                    'totalCount'       => $usersDept,
                                    'created_at'       => date('Y-m-d H:i:s')
                                );
                                $indexUsersDeptRecords = DB::table('users_daily_count')->insertGetId($insertUsersDeptArray);
                            }
                        }   
                    }
                }
            }
        }
    }
}

public function calculateUsercountDaily($orgId,$officeId,$departmentId,$date)
{
    $orgId = base64_decode($orgId);
    $departmentId = $departmentId;
    $officeId = $officeId;

    $usersCountQuery = DB::table('users')
        ->where('users.status','Active')
        ->where('users.roleId',3);
    if (!empty($date)) {
        $usersCountQuery->where('users.created_at','<=',$date);
    }
    if(!empty($orgId)){
        $usersCountQuery->where('users.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)){
        $usersCountQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)){
        $usersCountQuery->where('users.officeId',$officeId);
        $usersCountQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)){
        $usersCountQuery->leftJoin('departments','departments.id','users.departmentId')
            ->where('departments.status','Active')
            ->where('departments.departmentId',$departmentId);
    }
    $usersCount = $usersCountQuery->count();

    return $usersCount;
}

//Cron for kudos dashboard
public function getKudosCountReportByPastDate(){
    $year  = '2020';
    $month = '01';  
    $day = '31';
    //Get total days of month and year
    //$totalDays = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    // $totalDays = 20;
    // for ($day = 16; $day <= $totalDays; $day++) {
        $makeDate = $year."-".$month."-".$day;
        $this->getKudosCountReport($makeDate);
    // }
    echo "Done for :".$month." - ".$year." days ".$day;
    die('<br /> Finally Done.');
}

public function getKudosCountReport($date=false)
{
    if($date){
        $date = $date;
    }else{
        $date =date('Y-m-d',strtotime('last day of this month'));
    }

    //Get all organization
    $allOrganizations = DB::table('organisations')
                        ->where('status','Active') 
                        ->select('id')
                        ->whereDate('created_at', '<=',$date)
                        ->get();
    if($allOrganizations){
        foreach($allOrganizations as $organization){
            $orgId = $organization->id;

            $dots = DB::table('dots')->where('orgId',$orgId)->first();

            if (!empty($dots)) {
                $indexKudosBeliefOrg = $this->calculateKudosBeliefGraph(base64_encode($orgId),'','',2,$date);
                $indexKudosBeliefOrgExclude = $this->calculateKudosBeliefGraph(base64_encode($orgId),'','',1,$date);
                
                $c=0;
                foreach ($indexKudosBeliefOrg as $kudosBeliefOrg) {
                        //Chk duplicate record for same day
                        $kudosBeliefOrgRecords = DB::table('kudos_belief_dashboard_graph')
                                ->select('id')
                                ->whereDate('date',$date)
                                ->where('orgId',$orgId)
                                ->where('beliefId',$kudosBeliefOrg['id'])
                                ->whereNull('officeId')
                                ->whereNull('departmentId')
                                ->first();

                        if($kudosBeliefOrgRecords){
                            //No need to insert
                        }else{
                            $insertKudosBeliefOrgArray = array(
                                'date'             => $date,
                                'orgId'            => $orgId,
                                'beliefId'         => $kudosBeliefOrg['id'],
                                'with_weekend'     => $kudosBeliefOrg['count'],
                                'without_weekend'  => $indexKudosBeliefOrgExclude[$c]['count'],
                                'created_at'       => date('Y-m-d H:i:s')
                            );
                            $indexKudosBeliefOrgRecords = DB::table('kudos_belief_dashboard_graph')->insertGetId($insertKudosBeliefOrgArray);
                        }    
                    $c++;
                }

                $dotBeliefs = DB::table('dots_beliefs')
                    ->where('status','Active')
                    ->where('dotId',$dots->id)
                    ->get();
                foreach ($dotBeliefs as $belief) {
                    $indexKudosValuesOrg = $this->calculateKudosValuesGraph(base64_encode($orgId),'','',$belief->id,2,$date);
                    $indexKudosValuesOrgExclude = $this->calculateKudosValuesGraph(base64_encode($orgId),'','',$belief->id,1,$date);

                    $l=0;
                    foreach ($indexKudosValuesOrg as $kudosValuesOrg) {
                            //Chk duplicate record for same day
                            $kudosValuesOrgRecords = DB::table('kudos_values_dasboard_graph')
                                    ->select('id')
                                    ->where('orgId',$orgId)
                                    ->whereNull('officeId')
                                    ->whereNull('departmentId')
                                    ->where('beliefId',$belief->id)
                                    ->where('valueId',$kudosValuesOrg['id'])
                                    ->whereDate('date',$date)
                                    ->first();

                            if($kudosValuesOrgRecords){
                                //No need to insert
                            }else{
                                $insertKudosValuesOrgArray = array(
                                    'orgId'            => $orgId,
                                    'beliefId'         => $belief->id,
                                    'valueId'          => $kudosValuesOrg['id'],
                                    'date'             => $date,
                                    'with_weekend'     => $kudosValuesOrg['count'],
                                    'without_weekend'  => $indexKudosValuesOrgExclude[$l]['count'],
                                    'created_at'       => date('Y-m-d H:i:s')
                                );
                                $indexKudosValuesOrgRecords = DB::table('kudos_values_dasboard_graph')->insertGetId($insertKudosValuesOrgArray);
                            }    
                        $l++;
                    }
                }

                //Get offices of organization
                $offices = DB::table('offices')
                        ->select('id')
                        ->where('status','Active') 
                        ->whereDate('created_at', '<=',$date)
                        ->where('orgId',$orgId)->get();
                
                if($offices){
                    foreach($offices as $office){
                        $officeId = $office->id;
                        $indexKudosBeliefOffice = $this->calculateKudosBeliefGraph(base64_encode($orgId),$officeId,'',2,$date);
                        $indexKudosBeliefOfficeExclude = $this->calculateKudosBeliefGraph(base64_encode($orgId),$officeId,'',1,$date);

                        $a=0;
                        foreach ($indexKudosBeliefOffice as $kudosBeliefOffice) {
                            //Chk duplicate record for same day
                            $kudosBeliefOfficeRecords = DB::table('kudos_belief_dashboard_graph')
                                    ->select('id')
                                    ->whereDate('date',$date)
                                    ->where('orgId',$orgId)
                                    ->where('beliefId',$kudosBeliefOffice['id'])
                                    ->where('officeId',$officeId)
                                    ->whereNull('departmentId')
                                    ->first();

                            if($kudosBeliefOfficeRecords){
                                //No need to insert
                            }else{
                                $insertKudosBeliefOfficeArray = array(
                                    'date'              => $date,
                                    'orgId'             => $orgId,
                                    'officeId'          => $officeId,
                                    'beliefId'          => $kudosBeliefOffice['id'],
                                    'with_weekend'      => $kudosBeliefOffice['count'],
                                    'without_weekend'   => $indexKudosBeliefOfficeExclude[$a]['count'],
                                    'created_at'        => date('Y-m-d H:i:s')
                                );
                                $indexKudosBeliefOfficeRecords = DB::table('kudos_belief_dashboard_graph')->insertGetId($insertKudosBeliefOfficeArray);
                            }
                            $a++; 
                        }

                        foreach ($dotBeliefs as $belief) {
                            $indexKudosValuesOffice = $this->calculateKudosValuesGraph(base64_encode($orgId),$officeId,'',$belief->id,2,$date);
                            $indexKudosValuesOfficeExclude = $this->calculateKudosValuesGraph(base64_encode($orgId),$officeId,'',$belief->id,1,$date);

                            $m=0;
                            foreach ($indexKudosValuesOffice as $kudosValuesOffice) {
                                    //Chk duplicate record for same day
                                    $kudosValuesOfficeRecords = DB::table('kudos_values_dasboard_graph')
                                            ->select('id')
                                            ->where('orgId',$orgId)
                                            ->where('officeId',$officeId)
                                            ->whereNull('departmentId')
                                            ->where('beliefId',$belief->id)
                                            ->where('valueId',$kudosValuesOffice['id'])
                                            ->whereDate('date',$date)
                                            ->first();

                                    if($kudosValuesOfficeRecords){
                                        //No need to insert
                                    }else{
                                        $insertKudosValuesOfficeArray = array(
                                            'orgId'            => $orgId,
                                            'officeId'         => $officeId,
                                            'beliefId'         => $belief->id,
                                            'valueId'          => $kudosValuesOffice['id'],
                                            'date'             => $date,
                                            'with_weekend'     => $kudosValuesOffice['count'],
                                            'without_weekend'  => $indexKudosValuesOfficeExclude[$m]['count'],
                                            'created_at'       => date('Y-m-d H:i:s')
                                        );
                                        $indexKudosValuesOfficeRecords = DB::table('kudos_values_dasboard_graph')->insertGetId($insertKudosValuesOfficeArray);
                                    }    
                                $m++;
                            }
                        }
                
                        //Get departments
                        $departments = DB::table('departments')
                            //->select('id')
                            ->where('orgId',$orgId)
                            ->where('officeId',$officeId)
                            ->whereDate('created_at', '<=',$date)
                            ->where('status','Active') 
                            ->get();
                        if($departments){
                            foreach ($departments as $department) {
                                $departmentId = $department->id;
                                $allDepartmentId = $department->departmentId;
                               
                                //Get index
                                $indexKudosBeliefAllDept = $this->calculateKudosBeliefGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                                $indexKudosBeliefAllDeptExclude = $this->calculateKudosBeliefGraph(base64_encode($orgId),'',$allDepartmentId,1,$date);

                                $b = 0;
                                foreach ($indexKudosBeliefAllDept as $kudosBeliefAllDept) {
                                    //Chk duplicate record for same day
                                    $kudosBeliefAllDeptRecords = DB::table('kudos_belief_dashboard_graph')
                                            ->select('id')
                                            ->whereDate('date',$date)
                                            ->where('orgId',$orgId)
                                            ->where('beliefId',$kudosBeliefAllDept['id'])
                                            ->whereNull('officeId')
                                            ->where('departmentId',$allDepartmentId)
                                            ->first();

                                    if($kudosBeliefAllDeptRecords){
                                        //No need to insert
                                    }else{
                                        $insertKudosBeliefAllDeptArray = array(
                                            'date'              => $date,
                                            'orgId'             => $orgId,
                                            'departmentId'      => $allDepartmentId, 
                                            'beliefId'          => $kudosBeliefAllDept['id'],
                                            'with_weekend'      => $kudosBeliefAllDept['count'],
                                            'without_weekend'   => $indexKudosBeliefAllDeptExclude[$b]['count'],
                                            'created_at'        => date('Y-m-d H:i:s')
                                        );
                                        $indexKudosBeliefAllDeptRecords = DB::table('kudos_belief_dashboard_graph')->insertGetId($insertKudosBeliefAllDeptArray);
                                    }
                                    $b++;
                                }

                                foreach ($dotBeliefs as $belief) {
                                    $indexKudosValuesAllDept = $this->calculateKudosValuesGraph(base64_encode($orgId),'',$allDepartmentId,$belief->id,2,$date);
                                    $indexKudosValuesAllDeptExclude = $this->calculateKudosValuesGraph(base64_encode($orgId),'',$allDepartmentId,$belief->id,1,$date);

                                    $n=0;
                                    foreach ($indexKudosValuesAllDept as $kudosValuesAllDept) {
                                        //Chk duplicate record for same day
                                        $kudosValuesAllDeptRecords = DB::table('kudos_values_dasboard_graph')
                                                ->select('id')
                                                ->where('orgId',$orgId)
                                                ->whereNull('officeId')
                                                ->where('departmentId',$allDepartmentId)
                                                ->where('beliefId',$belief->id)
                                                ->where('valueId',$kudosValuesAllDept['id'])
                                                ->whereDate('date',$date)
                                                ->first();

                                        if($kudosValuesAllDeptRecords){
                                            //No need to insert
                                        }else{
                                            $insertKudosValuesAllDeptArray = array(
                                                'orgId'            => $orgId,
                                                'departmentId'     => $allDepartmentId,
                                                'beliefId'         => $belief->id,
                                                'valueId'          => $kudosValuesAllDept['id'],
                                                'date'             => $date,
                                                'with_weekend'     => $kudosValuesAllDept['count'],
                                                'without_weekend'  => $indexKudosValuesAllDeptExclude[$n]['count'],
                                                'created_at'       => date('Y-m-d H:i:s')
                                            );
                                            $indexKudosValuesAllDeptRecords = DB::table('kudos_values_dasboard_graph')->insertGetId($insertKudosValuesAllDeptArray);
                                        }    
                                        $n++;
                                    }
                                }
                                
                                $indexKudosBeliefDept = $this->calculateKudosBeliefGraph(base64_encode($orgId),$officeId,$departmentId,2,$date);
                                $indexKudosBeliefDeptExclude = $this->calculateKudosBeliefGraph(base64_encode($orgId),$officeId,$departmentId,1,$date);

                                $d = 0;
                                foreach ($indexKudosBeliefDept as $kudosBeliefDept) {
                                    //Chk duplicate record for same day
                                    $kudosBeliefDeptRecords = DB::table('kudos_belief_dashboard_graph')
                                            ->select('id')
                                            ->whereDate('date',$date)
                                            ->where('orgId',$orgId)
                                            ->where('officeId',$officeId)
                                            ->where('departmentId',$departmentId)
                                            ->where('beliefId',$kudosBeliefDept['id'])
                                            ->first();

                                    if($kudosBeliefDeptRecords){
                                        //No need to insert
                                    }else{
                                        $insertKudosBeliefDeptArray = array(
                                            'date'              => $date,
                                            'orgId'             => $orgId,
                                            'officeId'          => $officeId,    
                                            'departmentId'      => $departmentId, 
                                            'beliefId'          => $kudosBeliefDept['id'],
                                            'with_weekend'      => $kudosBeliefDept['count'],
                                            'without_weekend'   => $indexKudosBeliefDeptExclude[$d]['count'],
                                            'created_at'        => date('Y-m-d H:i:s')
                                        );
                                        $indexKudosBeliefDeptRecords = DB::table('kudos_belief_dashboard_graph')->insertGetId($insertKudosBeliefDeptArray);
                                    }
                                    $d++;
                                }

                                foreach ($dotBeliefs as $belief) {
                                    $indexKudosValuesDept = $this->calculateKudosValuesGraph(base64_encode($orgId),$officeId,$departmentId,$belief->id,2,$date);
                                    $indexKudosValuesDeptExclude = $this->calculateKudosValuesGraph(base64_encode($orgId),$officeId,$departmentId,$belief->id,1,$date);

                                    $o=0;
                                    foreach ($indexKudosValuesDept as $kudosValuesDept) {
                                            //Chk duplicate record for same day
                                            $kudosValuesDeptRecords = DB::table('kudos_values_dasboard_graph')
                                                    ->select('id')
                                                    ->where('orgId',$orgId)
                                                    ->where('officeId',$officeId)
                                                    ->where('departmentId',$departmentId)
                                                    ->where('beliefId',$belief->id)
                                                    ->where('valueId',$kudosValuesDept['id'])
                                                    ->whereDate('date',$date)
                                                    ->first();

                                            if($kudosValuesDeptRecords){
                                                //No need to insert
                                            }else{
                                                $insertKudosValuesDeptArray = array(
                                                    'orgId'            => $orgId,
                                                    'officeId'         => $officeId,
                                                    'departmentId'     => $departmentId,
                                                    'beliefId'         => $belief->id,
                                                    'valueId'           => $kudosValuesDept['id'],
                                                    'date'             => $date,
                                                    'with_weekend'     => $kudosValuesDept['count'],
                                                    'without_weekend'  => $indexKudosValuesDeptExclude[$o]['count'],
                                                    'created_at'       => date('Y-m-d H:i:s')
                                                );
                                                $indexKudosValuesDeptRecords = DB::table('kudos_values_dasboard_graph')->insertGetId($insertKudosValuesDeptArray);
                                            }    
                                        $o++;
                                    }
                                }
                            }   
                        }
                    }
                }
            }
        }
    }
}

public function calculateKudosBeliefGraph($orgId,$officeId,$departmentId,$isExclude,$date,$beliefId=false)
{
    $orgId = base64_decode($orgId);
    $departmentId = $departmentId;
    $officeId = $officeId;
    $month = date('Y-m',strtotime($date));
    // $month = date('Y-m');

    //Get weekends
    $dots = DB::table('dots')->where('orgId',$orgId)->first();
    $dotBeliefArray = array();
    if (!empty($dots)) {
        $dotBeliefsQuery = DB::table('dots_beliefs')
            ->where('status','Active')
            ->where('dotId',$dots->id)
            ->whereDate('created_at','<=',$date);
        if (!empty($beliefId)) {
            $dotBeliefsQuery->where('id',$beliefId);
        }
        $dotBeliefs = $dotBeliefsQuery->get();
        foreach ($dotBeliefs as $key => $bValue){
            $dotsValue = DB::table('dots_values')->where('beliefId',$bValue->id)->where('status','Active')->get();
            $dotValuesCount = 0;
          
            foreach ($dotsValue as $Vvalue){
                $upVotTblQuery = DB::table('dot_bubble_rating_records')       
                    ->leftJoin('users','dot_bubble_rating_records.to_bubble_user_id','users.id')     
                    ->where('dot_bubble_rating_records.dot_belief_id',$bValue->id)
                    ->where('dot_bubble_rating_records.dot_value_name_id',$Vvalue->name)
                    ->where('dot_bubble_rating_records.status','Active')
                    ->where('users.status','Active')
                    ->where('dot_bubble_rating_records.created_at','LIKE',$month."%")
                    ->where('dot_bubble_rating_records.bubble_flag',"1");
                if($date){
                    $upVotTblQuery->whereDate('dot_bubble_rating_records.created_at','<=',$date);
                }
                if($isExclude==1){
                    //Get weekends
                    $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($month,2);
                    $upVotTblQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
                }
                if (!empty($officeId) && empty($departmentId)) {
                    $upVotTblQuery->where('users.officeId',$officeId);
                }elseif (!empty($officeId) && !empty($departmentId)) {
                    $upVotTblQuery->where('users.officeId',$officeId);
                    $upVotTblQuery->where('users.departmentId',$departmentId);
                }elseif (empty($officeId) && !empty($departmentId)) {
                    $upVotTblQuery->leftJoin('departments','departments.id','users.departmentId');
                    $upVotTblQuery->where('departments.status','Active');
                    $upVotTblQuery->where('departments.departmentId',$departmentId);
                }   
                $dotBCount = $upVotTblQuery->count();
                $dotValuesCount += $dotBCount; 
            }
            $result['id'] = $bValue->id;
            $result['count'] = $dotValuesCount;
            array_push($dotBeliefArray, $result);
        }
    }   
    return $dotBeliefArray;
}

public function calculateKudosValuesGraph($orgId,$officeId,$departmentId,$beliefId,$isExclude,$date,$valueId=false)
{
    $dotValuesArray = array();
    if (!empty($beliefId)) {
        $month = date('Y-m',strtotime($date));
        // $month = date('Y-m');

        $dotBeliefs = DB::table('dots_beliefs')
            ->where('status','Active')
            ->where('id',$beliefId)
            ->get();

        // $dotsValue = DB::table('dots_values')->where('beliefId',$beliefId)->where('status','Active')->get();

        $dotValuesQuery = DB::table('dots_values')
            ->where('status','Active')
            ->where('beliefId',$beliefId)
            ->whereDate('created_at','<=',$date);
        if (!empty($valueId)) {
            $dotValuesQuery->where('id',$valueId);
        }
        $dotsValue = $dotValuesQuery->get();

        $dotValuesCount = 0;
          
        foreach ($dotsValue as $Vvalue){
            $upVotTblQuery = DB::table('dot_bubble_rating_records')       
                ->leftJoin('users','dot_bubble_rating_records.to_bubble_user_id','users.id')     
                ->where('dot_bubble_rating_records.dot_belief_id',$beliefId)
                ->where('dot_bubble_rating_records.dot_value_name_id',$Vvalue->name)
                ->where('dot_bubble_rating_records.status','Active')
                ->where('users.status','Active')
                ->where('dot_bubble_rating_records.created_at','LIKE',$month."%")
                ->where('dot_bubble_rating_records.bubble_flag',"1");
            if($date){
                $upVotTblQuery->whereDate('dot_bubble_rating_records.created_at','<=',$date);
            }
            if($isExclude==1){
                //Get weekends
                $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($month,2);
                $upVotTblQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
            }
            if (!empty($officeId) && empty($departmentId)) {
                $upVotTblQuery->where('users.officeId',$officeId);
            }elseif (!empty($officeId) && !empty($departmentId)) {
                $upVotTblQuery->where('users.officeId',$officeId);
                $upVotTblQuery->where('users.departmentId',$departmentId);
            }elseif (empty($officeId) && !empty($departmentId)) {
                $upVotTblQuery->leftJoin('departments','departments.id','users.departmentId');
                $upVotTblQuery->where('departments.status','Active');
                $upVotTblQuery->where('departments.departmentId',$departmentId);
            }   
            $dotBCount = $upVotTblQuery->count();
            $result['id'] = $Vvalue->id;
            $result['count'] = $dotBCount;
            array_push($dotValuesArray, $result);
        }
    }
    return $dotValuesArray;
}

//Cron for kudos leaderboard
public function getKudosChampLeaderboardByPastDate(){
    $year  = '2020';
    $month = '01';  
    $day = '31';
    //Get total days of month and year
    //$totalDays = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    // $totalDays = 20;
    // for ($day = 16; $day <= $totalDays; $day++) {
        $makeDate = $year."-".$month."-".$day;
        $this->getKudosChampLeaderboard($makeDate);
    // }
    echo "Done for :".$month." - ".$year." days ".$day;
    die('<br /> Finally Done.');
}

public function getKudosChampLeaderboard($date=false)
{
    if($date){
        $date = $date;
    }else{
        $date =date('Y-m-d',strtotime('last day of this month'));
    }

    //Get all organization
    $allOrganizations = DB::table('organisations')
                        ->where('status','Active') 
                        ->select('id')
                        ->whereDate('created_at', '<=',$date)
                        ->get();
    if($allOrganizations){
        foreach($allOrganizations as $organization){
            $orgId = $organization->id;

            $indexKudosChampOrg = $this->calculateKudosChampGraph(base64_encode($orgId),'','',2,$date);
            $indexKudosChampOrgExclude = $this->calculateKudosChampGraph(base64_encode($orgId),'','',1,$date);

            $userIdsOrg = [];
            foreach ($indexKudosChampOrgExclude as $valueOrg) {
                array_push($userIdsOrg, $valueOrg['userId']);
            }
            $newValOrg = '';
            foreach ($indexKudosChampOrg as $kudosOrg) {
                if (in_array($kudosOrg['userId'], $userIdsOrg)) {
                    //Nothing
                }else{
                    $newValOrg = $kudosOrg;
                    $newValOrg['count'] = 0;
                    array_push($indexKudosChampOrgExclude, $newValOrg);
                }
            }
            
            $c=0;
            foreach ($indexKudosChampOrg as $kudosChampOrg) {
                    //Chk duplicate record for same day
                    $kudosChampOrgRecords = DB::table('kudos_leaderboard_graph')
                            ->select('id')
                            ->whereDate('date',$date)
                            ->where('userId',$kudosChampOrg['userId'])
                            ->where('orgId',$orgId)
                            ->whereNull('officeId')
                            ->whereNull('departmentId')
                            ->first();

                    if($kudosChampOrgRecords){
                        //No need to insert
                    }else{
                        $kudosUserIdOrg1 = $kudosChampOrg['userId'];
                        $kudosUserIdOrg2 = $indexKudosChampOrgExclude[$c]['userId'];

                        $kudosCountWithoutWeekendOrg = '';
                        if ($kudosUserIdOrg1 == $kudosUserIdOrg2) {
                            $kudosCountWithoutWeekendOrg = $indexKudosChampOrgExclude[$c]['count'];
                        }else{
                            foreach ($indexKudosChampOrgExclude as $kudosChampOrgExcludeVal) {
                                if ($kudosChampOrgExcludeVal['userId'] == $kudosUserIdOrg1) {
                                    $kudosCountWithoutWeekendOrg = $kudosChampOrgExcludeVal['count'];
                                    break;
                                }
                            }
                        }

                        $insertKudosChampOrgArray = array(
                            'date'                          => $date,
                            'orgId'                         => $orgId,
                            'userId'                        => $kudosChampOrg['userId'],
                            'userName'                      => $kudosChampOrg['userName'],
                            'userEmail'                     => $kudosChampOrg['userEmail'],
                            'userImage'                     => $kudosChampOrg['userImage'],
                            'kudos_count_with_weekend'      => $kudosChampOrg['count'],
                            'kudos_count_without_weekend'   => $kudosCountWithoutWeekendOrg,
                            // 'kudos_count_without_weekend'   => $indexKudosChampOrgExclude[$c]['count'],
                            'created_at'                    => date('Y-m-d H:i:s')
                        );
                        $indexKudosChampOrgRecords = DB::table('kudos_leaderboard_graph')->insertGetId($insertKudosChampOrgArray);
                    }    
                $c++;
            }


            //Get offices of organization
            $offices = DB::table('offices')
                    ->select('id')
                    ->where('status','Active') 
                    ->whereDate('created_at', '<=',$date)
                    ->where('orgId',$orgId)->get();
            
            if($offices){
                foreach($offices as $office){
                    $officeId = $office->id;
                    $indexKudosChampOffice = $this->calculateKudosChampGraph(base64_encode($orgId),$officeId,'',2,$date);
                    $indexKudosChampOfficeExclude = $this->calculateKudosChampGraph(base64_encode($orgId),$officeId,'',1,$date);

                    $userIdsOffice = [];
                    foreach ($indexKudosChampOfficeExclude as $valueOffice) {
                        array_push($userIdsOffice, $valueOffice['userId']);
                    }
                    $newValOffice = '';
                    foreach ($indexKudosChampOffice as $kudosOffice) {
                        if (in_array($kudosOffice['userId'], $userIdsOffice)) {
                            //Nothing
                        }else{
                            $newValOffice = $kudosOffice;
                            $newValOffice['count'] = 0;
                            array_push($indexKudosChampOfficeExclude, $newValOffice);
                        }
                    }
                
                    $a=0;
                    foreach ($indexKudosChampOffice as $kudosChampOffice) {
                        //Chk duplicate record for same day
                        $kudosChampOfficeRecords = DB::table('kudos_leaderboard_graph')
                                ->select('id')
                                ->whereDate('date',$date)
                                ->where('userId',$kudosChampOffice['userId'])
                                ->where('orgId',$orgId)
                                ->where('officeId',$officeId)
                                ->whereNull('departmentId')
                                ->first();

                        if($kudosChampOfficeRecords){
                            //No need to insert
                        }else{

                            $kudosUserIdOffice1 = $kudosChampOffice['userId'];
                            $kudosUserIdOffice2 = $indexKudosChampOfficeExclude[$a]['userId'];

                            $kudosCountWithoutWeekendOffice = '';
                            if ($kudosUserIdOffice1 == $kudosUserIdOffice2) {
                                $kudosCountWithoutWeekendOffice = $indexKudosChampOfficeExclude[$a]['count'];
                            }else{
                                foreach ($indexKudosChampOfficeExclude as $kudosChampOfficeExcludeVal) {
                                    if ($kudosChampOfficeExcludeVal['userId'] == $kudosUserIdOffice1) {
                                        $kudosCountWithoutWeekendOffice = $kudosChampOfficeExcludeVal['count'];
                                        break;
                                    }
                                }
                            }

                            $insertKudosChampOfficeArray = array(
                                'date'                          => $date,
                                'orgId'                         => $orgId,
                                'officeId'                      => $officeId,
                                'userId'                        => $kudosChampOffice['userId'],
                                'userName'                      => $kudosChampOffice['userName'],
                                'userEmail'                     => $kudosChampOffice['userEmail'],
                                'userImage'                     => $kudosChampOffice['userImage'],
                                'kudos_count_with_weekend'      => $kudosChampOffice['count'],
                                'kudos_count_without_weekend'   => $kudosCountWithoutWeekendOffice,
                                // 'kudos_count_without_weekend'   => $indexKudosChampOfficeExclude[$a]['count'],
                                'created_at'                    => date('Y-m-d H:i:s')
                            );

                            $indexKudosChampOfficeRecords = DB::table('kudos_leaderboard_graph')->insertGetId($insertKudosChampOfficeArray);
                        }
                        $a++; 
                    }

                    //Get departments
                    $departments = DB::table('departments')
                        //->select('id')
                        ->where('orgId',$orgId)
                        ->where('officeId',$officeId)
                        ->whereDate('created_at', '<=',$date)
                        ->where('status','Active') 
                        ->get();
                    if($departments){
                        foreach ($departments as $department) {
                            $departmentId = $department->id;
                            $allDepartmentId = $department->departmentId;
                           
                            //Get index
                            $indexKudosChampAllDept = $this->calculateKudosChampGraph(base64_encode($orgId),'',$allDepartmentId,2,$date);
                            $indexKudosChampAllDeptExclude = $this->calculateKudosChampGraph(base64_encode($orgId),'',$allDepartmentId,1,$date);

                            $userIdsAllDept = [];
                            foreach ($indexKudosChampAllDeptExclude as $valueAllDept) {
                                array_push($userIdsAllDept, $valueAllDept['userId']);
                            }
                            $newValAllDept = '';
                            foreach ($indexKudosChampAllDept as $kudosAllDept) {
                                if (in_array($kudosAllDept['userId'], $userIdsAllDept)) {
                                    //Nothing
                                }else{
                                    $newValAllDept = $kudosAllDept;
                                    $newValAllDept['count'] = 0;
                                    array_push($indexKudosChampAllDeptExclude, $newValAllDept);
                                }
                            }

                            $b = 0;
                            foreach ($indexKudosChampAllDept as $kudosChampAllDept) {
                                //Chk duplicate record for same day
                                $kudosChampAllDeptRecords = DB::table('kudos_leaderboard_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('userId',$kudosChampAllDept['userId'])
                                        ->where('orgId',$orgId)
                                        ->whereNull('officeId')
                                        ->where('departmentId',$allDepartmentId)
                                        ->first();

                                if($kudosChampAllDeptRecords){
                                    //No need to insert
                                }else{
                                    $kudosUserIdAllDept1 = $kudosChampAllDept['userId'];
                                    $kudosUserIdAllDept2 = $indexKudosChampAllDeptExclude[$b]['userId'];

                                    $kudosCountWithoutWeekendAllDept = '';
                                    if ($kudosUserIdAllDept1 == $kudosUserIdAllDept2) {
                                        $kudosCountWithoutWeekendAllDept = $indexKudosChampAllDeptExclude[$b]['count'];
                                    }else{
                                        foreach ($indexKudosChampAllDeptExclude as $kudosChampAllDeptExcludeVal) {
                                            if ($kudosChampAllDeptExcludeVal['userId'] == $kudosUserIdAllDept1) {
                                                $kudosCountWithoutWeekendAllDept = $kudosChampAllDeptExcludeVal['count'];
                                                break;
                                            }
                                        }
                                    }
                                    $insertKudosChampAllDeptArray = array(
                                        'date'                          => $date,
                                        'orgId'                         => $orgId,
                                        'departmentId'                  => $allDepartmentId, 
                                        'userId'                        => $kudosChampAllDept['userId'],
                                        'userName'                      => $kudosChampAllDept['userName'],
                                        'userEmail'                     => $kudosChampAllDept['userEmail'],
                                        'userImage'                     => $kudosChampAllDept['userImage'],
                                        'kudos_count_with_weekend'      => $kudosChampAllDept['count'],
                                        'kudos_count_without_weekend'   => $kudosCountWithoutWeekendAllDept,
                                        // 'kudos_count_without_weekend'   => $indexKudosChampAllDeptExclude[$b]['count'],
                                        'created_at'                    => date('Y-m-d H:i:s')
                                    );
                                    $indexKudosChampAllDeptRecords = DB::table('kudos_leaderboard_graph')->insertGetId($insertKudosChampAllDeptArray);
                                }
                                $b++;
                            }
                            
                            $indexKudosChampDept = $this->calculateKudosChampGraph(base64_encode($orgId),$officeId,$departmentId,2,$date);
                            $indexKudosChampDeptExclude = $this->calculateKudosChampGraph(base64_encode($orgId),$officeId,$departmentId,1,$date);

                            $userIdsDept = [];
                            foreach ($indexKudosChampDeptExclude as $valueDept) {
                                array_push($userIdsDept, $valueDept['userId']);
                            }
                            $newValDept = '';
                            foreach ($indexKudosChampDept as $kudosDept) {
                                if (in_array($kudosDept['userId'], $userIdsDept)) {
                                    //Nothing
                                }else{
                                    $newValDept = $kudosDept;
                                    $newValDept['count'] = 0;
                                    array_push($indexKudosChampDeptExclude, $newValDept);
                                }
                            }

                            $d = 0;
                            foreach ($indexKudosChampDept as $kudosChampDept) {
                                //Chk duplicate record for same day
                                $kudosChampDeptRecords = DB::table('kudos_leaderboard_graph')
                                        ->select('id')
                                        ->whereDate('date',$date)
                                        ->where('userId',$kudosChampDept['userId'])
                                        ->where('orgId',$orgId)
                                        ->where('officeId',$officeId)
                                        ->where('departmentId',$departmentId)
                                        ->first();

                                if($kudosChampDeptRecords){
                                    //No need to insert
                                }else{
                                    $kudosUserIdDept1 = $kudosChampDept['userId'];
                                    $kudosUserIdDept2 = $indexKudosChampDeptExclude[$d]['userId'];

                                    $kudosCountWithoutWeekendDept = '';
                                    if ($kudosUserIdDept1 == $kudosUserIdDept2) {
                                        $kudosCountWithoutWeekendDept = $indexKudosChampDeptExclude[$d]['count'];
                                    }else{
                                        foreach ($indexKudosChampDeptExclude as $kudosChampDeptExcludeVal) {
                                            if ($kudosChampDeptExcludeVal['userId'] == $kudosUserIdDept1) {
                                                $kudosCountWithoutWeekendDept = $kudosChampDeptExcludeVal['count'];
                                                break;
                                            }
                                        }
                                    }
                                    $insertKudosChampDeptArray = array(
                                        'date'                          => $date,
                                        'orgId'                         => $orgId,
                                        'officeId'                      => $officeId,
                                        'departmentId'                  => $departmentId, 
                                        'userId'                        => $kudosChampDept['userId'],
                                        'userName'                      => $kudosChampDept['userName'],
                                        'userEmail'                     => $kudosChampDept['userEmail'],
                                        'userImage'                     => $kudosChampDept['userImage'],
                                        'kudos_count_with_weekend'      => $kudosChampDept['count'],
                                        'kudos_count_without_weekend'   => $kudosCountWithoutWeekendDept,
                                        // 'kudos_count_without_weekend'   => $indexKudosChampDeptExclude[$d]['count'],
                                        'created_at'                    => date('Y-m-d H:i:s')
                                    );
                                    $indexKudosChampDeptRecords = DB::table('kudos_leaderboard_graph')->insertGetId($insertKudosChampDeptArray);
                                }
                                $d++;
                            }
                        }   
                    }
                }
            }
        }
    }
}

public function calculateKudosChampGraph($orgId,$officeId,$departmentId,$isExclude,$date)
{
    $orgId = base64_decode($orgId);
    $departmentId = $departmentId;
    $officeId = $officeId;
    $month = date('Y-m',strtotime($date));
    // $month = date('Y-m');
    $bubbleCountArray = array();

    $bubbleCountUserListQuery = DB::table('dot_bubble_rating_records')
      ->select('dot_bubble_rating_records.to_bubble_user_id as userId')
      ->leftJoin('dots','dot_bubble_rating_records.dot_id','dots.id')
      ->rightJoin('users','dot_bubble_rating_records.to_bubble_user_id','users.id')
      ->where('dot_bubble_rating_records.status','Active')
      ->where('dots.status','Active')
      ->where('users.status','Active')
      ->where('dot_bubble_rating_records.created_at','LIKE',$month."%")
      ->where('dot_bubble_rating_records.bubble_flag','1');
    if (!empty($orgId)) {
      $bubbleCountUserListQuery->where('dots.orgId',$orgId);
    }
    if(!empty($officeId) && empty($departmentId)) {
        $bubbleCountUserListQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
        $bubbleCountUserListQuery->where('users.officeId',$officeId);
        $bubbleCountUserListQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
        $bubbleCountUserListQuery->leftJoin('departments','departments.id','users.departmentId')
            ->where('departments.status','Active')
            ->where('departments.departmentId',$departmentId);
    }
    if($date){
        $bubbleCountUserListQuery->whereDate('dot_bubble_rating_records.created_at','<=',$date);
    }
    if($isExclude==1){
        //Get weekends
        $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($month,2);
        $bubbleCountUserListQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }
    $bubbleCountUserList = $bubbleCountUserListQuery->groupBy('userId')->get();

    foreach ($bubbleCountUserList as $user) {
        $topBubbleCountsQuery = DB::table('dot_bubble_rating_records')
            ->select('dot_bubble_rating_records.id')
            ->leftJoin('dots','dot_bubble_rating_records.dot_id','dots.id')
            ->rightJoin('users','dot_bubble_rating_records.to_bubble_user_id','users.id')
            ->where('dot_bubble_rating_records.to_bubble_user_id',$user->userId)
            ->where('dot_bubble_rating_records.status','Active')
            ->where('dots.status','Active')
            ->where('users.status','Active')
            ->where('dot_bubble_rating_records.created_at','LIKE',$month."%")
            ->where('dot_bubble_rating_records.bubble_flag','1');
        if (!empty($orgId)) {
            $topBubbleCountsQuery->where('dots.orgId',$orgId);
        }
        if(!empty($officeId) && empty($departmentId)) {
            $topBubbleCountsQuery->where('users.officeId',$officeId);
        }elseif(!empty($officeId) && !empty($departmentId)) {
            $topBubbleCountsQuery->where('users.officeId',$officeId);
            $topBubbleCountsQuery->where('users.departmentId',$departmentId);
        }elseif(empty($officeId) && !empty($departmentId)) {
            $topBubbleCountsQuery->leftJoin('departments','departments.id','users.departmentId')
                ->where('departments.status','Active')
                ->where('departments.departmentId',$departmentId);
        }
        if($date){
            $topBubbleCountsQuery->whereDate('dot_bubble_rating_records.created_at','<=',$date);
        }
        if($isExclude==1){
            //Get weekends
            $getWeekendDates = app('App\Http\Controllers\Admin\CommonController')->getWeekendDaysFromMonth($month,2);
            $topBubbleCountsQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
        }
        $topBubbleCounts = $topBubbleCountsQuery->count();

        $users = DB::table('users')->where('id',$user->userId)->where('status','Active')->first();

        $bubbleCount['userName']    = '';
        $bubbleCount['userEmail']   = '';
        $bubbleCount['userImage']   = '';
        if (!empty($users)) {
            $bubbleCount['userName']    = $users->name." ".$users->lastName;
            $bubbleCount['userEmail']   = $users->email;
            $bubbleCount['userImage']   = $users->imageUrl;
        }
        $bubbleCount['userId']      = $user->userId;
        $bubbleCount['count']       = $topBubbleCounts;

        array_push($bubbleCountArray, $bubbleCount);
    }

    $kudosArray = array_chunk(app('App\Http\Controllers\Admin\AdminDashboardController')->array_sort($bubbleCountArray, 'count', SORT_DESC) , 10);

    $kudosFinalArray = [];
    if (!empty($kudosArray)) {
        $kudosFinalArray = $kudosArray[0]; 
    }
    return $kudosFinalArray;
}

/*CHART MOTIVATION */
public function getReportMotivationalGraph(Request $request){

    $SOTmotivationResultArray = array();

    $orgId        = base64_decode($request->orgId);   
    $officeId     = Input::get('officeId');
    $departmentId = Input::get('departmentId');
    $startDate    = Input::get('startDate');
    $endDate      = Input::get('endDate');

    //Get info of org
    $org = DB::table('organisations')
                ->select('include_weekend','created_at')
                ->where('id',$orgId)->first();

    //Get weekend flag
    $excludeWeekend = $org->include_weekend;   //1 for include weekend else not include           


    $categoryTbl   = DB::table('sot_motivation_value_records')->where('status','Active')->get();
    $offices       = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
    $all_department= DB::table('all_department')->where('status','Active')->get();
    $flag          = 0 ;
    $departments   = [];
    if (!empty($officeId)){
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();

    }else{
        if(!empty($departmentId)){
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    // $query = DB::table('users')->where('status','Active')->where('orgId',$orgId);

    $query = DB::table('sot_motivation_answers')
    ->leftjoin('users','users.id','sot_motivation_answers.userId')
    ->select('sot_motivation_answers.userId')
    ->where('users.status','Active')
    ->where('users.orgId',$orgId);

    if($excludeWeekend!=1){
        $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
        $currentDate    = date('Y-m-d'); 
        //Get weekends
        $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
        //echo '<pre />';print_r($getWeekendDates);die;
        $query->whereNotIn(DB::raw("(DATE_FORMAT(sot_motivation_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }

    if(!empty($officeId)){
        $query->where('officeId',$officeId);
    }

    if(!empty($departmentId)){   
        $i = 0;
        if($departmentsNew->isEmpty()){
            $flag = 1;
        }

        foreach ($departmentsNew as $departments1){       
            if($i == 0){
                $query->where('users.departmentId',$departments1->id);
            }else{
                $query->orWhere('users.departmentId',$departments1->id);
            }
            $i++;
        }
    } 
    $query->groupBy('sot_motivation_answers.userId');
    $usersList = $query->get();
    $totalUser = count($usersList);

        // reset if department is not in organisation
    if($flag==1){
        $totalUser = 0;
    }

    if (!empty($totalUser)){
        foreach ($categoryTbl as $sotCvalue){  
            $query = DB::table('sot_motivation_answers AS sotans')
            ->leftJoin('sot_motivation_question_options AS qoption', 'qoption.id', '=', 'sotans.optionId')
            ->leftjoin('users','users.id','=','sotans.userId')
            ->where('users.status','Active')                     
            ->where('sotans.orgId',$orgId)
            ->where('sotans.status','Active')
            ->where('qoption.category_id',$sotCvalue->id);

            if($excludeWeekend!=1){
                $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
                $currentDate    = date('Y-m-d'); 
                //Get weekends
                $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
                //echo '<pre />';print_r($getWeekendDates);die;
                $query->whereNotIn(DB::raw("(DATE_FORMAT(sotans.created_at,'%Y-%m-%d'))"),$getWeekendDates);
            }

            if(!empty($officeId)){
                $query->where('users.officeId',$officeId);
            }

            if(!empty($departmentId)){   
                if($departmentsNew->isEmpty()){
                    $flag = 1;
                }
                $department3 = array();
                foreach ($departmentsNew as $departments1){       
                    $departments2 = $departments1->id;
                    array_push($department3, $departments2);
                }
                $query->whereIn('users.departmentId',$department3);
            } 
            $ansTbl = $query->sum('sotans.answer'); 

            $result1['title']      = utf8_encode($sotCvalue->title);
            $result1['percentage'] = number_format((float)($ansTbl/$totalUser), 2, '.', '');         
            array_push($SOTmotivationResultArray, $result1);
        }
    }

    if (empty($request->pdfStatus)){ 
        return view('admin/report/report_graph/sotMotivationUserListGraph',compact('SOTmotivationResultArray','offices','orgId','officeId','departmentId','departments','all_department'));
    } else {
        return compact('SOTmotivationResultArray');
    }
}



public function getReportCultureGraph(Request $request){

    // dd(Input::all());

    $orgId        = base64_decode($request->orgId);  
    $officeId     = Input::get('officeId');
    $departmentId = Input::get('departmentId');


    //Get info of org
    $org = DB::table('organisations')
                ->select('include_weekend','created_at')
                ->where('id',$orgId)->first();

    //Get weekend flag
    $excludeWeekend = $org->include_weekend;   //1 for include weekend else not include           

    $all_department = DB::table('all_department')->where('status','Active')->get();
    $flag           = 0 ;
    $departments    = [];
    if (!empty($officeId)) {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();

    }else{
        if(!empty($departmentId)){
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    $sotCultureStrTbl = DB::table('sot_culture_structure_records')->select('id','type','title','imgUrl')->where('status','Active')->get();
    $offices      = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();

    $sotCountArray  = array();
    $countArr       = array();

    foreach ($sotCultureStrTbl as $value){

        $query = DB::table('sot_answers')
        ->select(DB::raw('sum(score) AS count'))
        ->leftJoin('sot_questionnaire_records','sot_questionnaire_records.id','=','sot_answers.question_id')
        ->join('users','users.id','=','sot_answers.userId')
        ->where('users.status','Active')
        ->where('sot_questionnaire_records.type',$value->type)    
        ->where('users.orgId',$orgId);

        if($excludeWeekend!=1){
            $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
            $currentDate    = date('Y-m-d'); 
            //Get weekends
            $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
            //echo '<pre />';print_r($getWeekendDates);die;
            $query->whereNotIn(DB::raw("(DATE_FORMAT(sot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
        }

        if(!empty($officeId)){
            $query->where('users.officeId',$officeId);
        }

        if(!empty($departmentId)){   
            $i = 0;
            if($departmentsNew->isEmpty()){
                $flag = 1;
            }

            foreach ($departmentsNew as $departments1){       
                if($i == 0){
                    $query->where('users.departmentId',$departments1->id);
                }else{
                    $query->orWhere('users.departmentId',$departments1->id);
                }
                $i++;
            }
        }       

        $SOTCount        = $query->first();
        $value->SOTCount = $SOTCount->count;

        // reset if department is not in organisation
        if($flag==1){    
            $value->SOTCount = 0;
        }

        array_push($countArr, $SOTCount->count);
        array_push($sotCountArray, $value);

    }

    if($flag==1){
        $sotCountArray  = array();
    }

    // find maximum value array
    $sotCountArray1 = array();
    foreach ($sotCultureStrTbl as $value){

        $query = DB::table('sot_answers')
        ->select(DB::raw('sum(score) AS count'))
        ->leftJoin('sot_questionnaire_records','sot_questionnaire_records.id','=','sot_answers.question_id')
        ->leftJoin('users','users.id','=','sot_answers.userId')
        ->where('users.status','Active')
        ->where('sot_questionnaire_records.type',$value->type)    
        ->where('users.orgId',$orgId);

        if($excludeWeekend!=1){
            $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
            $currentDate    = date('Y-m-d'); 
            //Get weekends
            $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
            //echo '<pre />';print_r($getWeekendDates);die;
            $query->whereNotIn(DB::raw("(DATE_FORMAT(sot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
        }

        if(!empty($officeId)){
            $query->where('users.officeId',$officeId);
        }
        

        if(!empty($departmentId)){   
            $i = 0;
            
            if($departmentsNew->isEmpty()){
                $flag = 1;
            }
            $department3 = array();

            foreach ($departmentsNew as $departments1){       
                $departments2 = $departments1->id;
                array_push($department3, $departments2);
            }
            $query->whereIn('users.departmentId',$department3);
        }            

        $SOTCount = $query->first();
        $value->SOTCount = $SOTCount->count;

        if (max($countArr)==$SOTCount->count){
          array_push($sotCountArray1, $value);
        }        
    }

    //detail of culture structure 
    $sotStrDetailArr = array();

    // reset if department is not in organisation
    if($flag==1){
        $sotCountArray1 = array();
    }

    foreach ($sotCountArray1 as $value){
        $sotStrDetail = DB::table('sot_culture_structure_records')
        ->where('id',$value->id)
        ->where('status','Active')
        ->first();

        $summary = DB::table('sot_culture_structure_summary_records')
        ->where('type',$value->id)
        ->where('status','Active')
        ->get();

        $value->summary = $summary;

        array_push($sotStrDetailArr, $value);
    }

    if (empty($request->pdfStatus)){
        return view('admin/report/report_graph/sotCultureGraph',compact('sotCountArray','sotStrDetailArr','offices','orgId','officeId','departmentId','departments','all_department'));
    } else {
        return compact('sotCountArray','sotStrDetailArr');
    }

}



/*get percentage of cot functional lens*/
public function getReportFunctionalGraph(Request $request){

    $orgId        = base64_decode($request->orgId);    
    $officeId     = Input::get('officeId');
    $departmentId = Input::get('departmentId');
    $startDate    = Input::get('startDate');
    $endDate      = Input::get('endDate');

    $offices        = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
    $all_department = DB::table('all_department')->where('status','Active')->get();
    $flag           = 0 ;
    $departments    = [];


    //Get info of org
    $org = DB::table('organisations')
            ->select('include_weekend','created_at')
            ->where('id',$orgId)->first();
     //Get weekend flag
    $excludeWeekend = $org->include_weekend;   //1 for include weekend else not include      


    if (!empty($officeId)){
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //For new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }else{
        if(!empty($departmentId)){
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }
    $cotFunResultArray = array();

    // $query = DB::table('users')->where('status','Active')->where('orgId',$orgId);

    $query = DB::table('cot_functional_lens_answers')
    ->select('cot_functional_lens_answers.userId AS id')
    ->leftjoin('users','users.id','cot_functional_lens_answers.userId')    
    ->where('users.status','Active')
    ->where('users.orgId',$orgId);

    if($excludeWeekend!=1){
        $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
        $currentDate    = date('Y-m-d'); 
        //Get weekends
        $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
        //echo '<pre />';print_r($getWeekendDates);die;
        $query->whereNotIn(DB::raw("(DATE_FORMAT(cot_functional_lens_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }


    if(!empty($officeId)){
        $query->where('officeId',$officeId);
    }

    //filter acc. to start and end date
    if(!empty($startDate) && !empty($endDate)) {
        $startDate = date_create($startDate);
        $startDateMonth = date_format($startDate,"Y-m-d");

        $endDate = date_create($endDate);
        $endDateYear = date_format($endDate,"Y-m-d");

        $query->where('users.created_at', '>=',$startDateMonth);
        $query->where('users.created_at', '<=',$endDateYear);
    }


    if(!empty($departmentId)){   
        $i = 0;    
        if($departmentsNew->isEmpty()){
            $flag = 1;
        }
        foreach ($departmentsNew as $departments1){
            if($i == 0){
                $query->where('users.departmentId',$departments1->id);
            }else{
                $query->orWhere('users.departmentId',$departments1->id);
            }
            $i++;
        }
    }

    $query->groupBy('cot_functional_lens_answers.userId');
    $usersList = $query->get();

    // reset if department is not in organisation
    if($flag==1){
        $usersList = array();
    }

    foreach ($usersList as $key => $userValue){

        $variable = app('App\Http\Controllers\Admin\CommonController')->getCotFunLensInitialValue();    
        $initialValEIArray = array();
        foreach ($variable as $value){

            $value1 = $value[0];
            $value2 = $value[1];

            $countE = DB::table('cot_functional_lens_answers AS cfla')
            ->select('cflqo.option_name AS optionName')
            ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
            ->where('cfla.userId',$userValue->id)->where('cflqo.initial_value_id',$value1);
            $countE = $countE->get();

            if($excludeWeekend!=1){
                $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
                $currentDate    = date('Y-m-d'); 
                //Get weekends
                $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
                //echo '<pre />';print_r($getWeekendDates);die;
                $countE->whereNotIn(DB::raw("(DATE_FORMAT(cot_functional_lens_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
            }

            $countI = DB::table('cot_functional_lens_answers AS cfla')
            ->select('cflqo.option_name AS optionName')
            ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
            ->where('cfla.userId',$userValue->id)->where('cflqo.initial_value_id',$value2);
            $countI = $countI->get();

            if($excludeWeekend!=1){
                $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
                $currentDate    = date('Y-m-d'); 
                //Get weekends
                $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
                //echo '<pre />';print_r($getWeekendDates);die;
                $countI->whereNotIn(DB::raw("(DATE_FORMAT(cot_functional_lens_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
            }

            $initialValEI  ='';
            if(count($countE) > count($countI)){
              $initialValEI  =  $value1;
            }else if (count($countE) < count($countI)){
              $initialValEI  =  $value2;    
            }else if(count($countE) == count($countI)){     
              $initialValEI  =  $value1;
            } 
            array_push($initialValEIArray, $initialValEI);
        }

        $valueTypeArray = array();
        $valueTypeArray['value'] = array();

        if(!empty($initialValEIArray)){
             $valueTypeArray = array('value'=>$initialValEIArray);
        }

        $matchValueArr = array();
        for($i=0; $i < count($valueTypeArray['value']); $i++){
            $valuesKey = '';
            $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$valueTypeArray['value'][$i])->first();  

            if(!empty($table)){
                $valuesKey = $table->value;
            }
            array_push($matchValueArr, $valuesKey); 
        }

        //remove last and first string
        $tribeMatchValue = json_encode(array_slice($valueTypeArray['value'], 1, -1));

        $tribeTipsList = DB::table('cot_functional_lens_tribe_tips_records')->where('value',$tribeMatchValue)->get();

        $tribeTipsArray = array();
        foreach ($tribeTipsList as $ttvalue){
            $tTips['value']  = $ttvalue->value;       
            array_push($cotFunResultArray, $tTips);
        }
    }

    $ST = 0;
    $SF = 0;
    $NF = 0;
    $NT = 0;


    foreach ($cotFunResultArray as $finalArrayValue){ 
        if ($finalArrayValue['value']=='["7","9"]'){
            $ST++;
        }elseif ($finalArrayValue['value']=='["7","10"]'){
            $SF++;
        }elseif ($finalArrayValue['value']=='["8","10"]'){
            $NF++;
        }elseif ($finalArrayValue['value']=='["8","9"]'){
            $NT++;
        }
    }

    $totalUser = count($usersList);
    if (!empty($totalUser)){
        $stPercent = ($ST/$totalUser)*100;
        $sfPercent = ($SF/$totalUser)*100;
        $nfPercent = ($NF/$totalUser)*100;
        $ntPercent = ($NT/$totalUser)*100;
    }else{
        $stPercent = 0;
        $sfPercent = 0;
        $nfPercent = 0;
        $ntPercent = 0;
    }

    $funcLensPercentageArray = array('st'=>round($stPercent, 2),'sf'=>round($sfPercent, 2),'nf'=>round($nfPercent, 2),'nt'=>round($ntPercent, 2));



    $tribeTipsListKey = DB::table('cot_functional_lens_tribe_tips_records')->where('status','Active')->orderBy('id','ASC')->get();

    $keyNameArray = array();
    foreach ($tribeTipsListKey as $value1){
        $reportKeyArray = json_decode($value1->value);

        $keyArr = array();
        foreach ($reportKeyArray as $value){
            $valuesKey = '';
            $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$value)->first();  

            if(!empty($table)){
              $valuesKey = $table->value;
            }   
            array_push($keyArr, $valuesKey); 
        }
      array_push($keyNameArray, $keyArr);
    }

    if (empty($request->pdfStatus)){
        return view('admin/report/report_graph/cotFunctionalLensUserListGraph',compact('funcLensPercentageArray','offices','orgId','officeId','departmentId','departments','all_department','keyNameArray'));
    } else {
        return compact('funcLensPercentageArray','keyNameArray');
    }
}



public function getFunName($value){
  $valuesKey = '';
  $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$value)->first();  
  if(!empty($table))
  {
    $valuesKey = $table->value;
}
return $valuesKey;
}



/*get COT team role mape for report graph*/
public function getCOTteamRoleMapGraph(Request $request) {



    $finalArray  = array();
    $orgId = base64_decode($request->orgId);

    $organisations = DB::table('organisations')->where('id',$orgId)->where('status','Active')->first();

    $offices = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();

    $all_department = DB::table('all_department')->where('status','Active')->get();

    $mapers = DB::table('cot_role_map_options')->where('status','Active')->get();

    $officeId     = Input::get('officeId');
    $departmentId = Input::get('departmentId');
    $startDate    = Input::get('startDate');
    $endDate      = Input::get('endDate');
    $chartId      = Input::get('chartId');

    $flag = 0 ;

    $departments = [];
    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

   //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }
    else
    {
        if(!empty($departmentId))
        {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    //Get weekends
    $startDate=DB::table('cot_answers')
        ->select('created_at')
        ->where('orgId',$orgId)
        ->orderBy('created_at','ASC')
        ->first();

    if (count($startDate) == 0) {
        // $startDate = $date;
        $startDate = date('Y-m-d');
    }
    else
    {
        $startDate = date('Y-m-d',strtotime($startDate->created_at));
    }
    // $currentDate = $date;
    $currentDate = date('Y-m-d');

    $getWeekendDates = $this->getDatesFromRange($startDate, $currentDate);

    $query = DB::table('cot_answers')
    ->select('users.id','users.orgId','users.name')
    ->leftjoin('users','users.id','cot_answers.userId')
    ->where('users.status','Active')
    ->where('users.orgId',$orgId);

    if($organisations->include_weekend != 1){
        $query->whereNotIn(DB::raw("(DATE_FORMAT(cot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }

    if(!empty($officeId))
    {
        $query->where('users.officeId',$officeId);
    }

    //filter acc. to start and end date
    if(!empty($startDate) && !empty($endDate)) 
    {

        $startDate = date_create($startDate);
        $startDateMonth = date_format($startDate,"Y-m-d");

        $endDate = date_create($endDate);

        $endDateYear = date_format($endDate,"Y-m-d");

        $query->where('users.created_at', '>=',$startDateMonth);
        $query->where('users.created_at', '<=',$endDateYear);


    }

    if(!empty($departmentId))
    {   
        $i = 0;

        if($departmentsNew->isEmpty())
        {
            $flag = 1;
        }
        foreach ($departmentsNew as $departments1) 
        {        
            if($i == 0)
            {
                $query->where('users.departmentId',$departments1->id);
            }
            else
            {
                $query->orWhere('users.departmentId',$departments1->id);
            }

            $i++;
        }
    }


    $users = $query->get();


    $resultArray1 = array();

    foreach($users as $value)
    {
        $data['id']    = $value->id;
        $data['orgId'] = $value->orgId;
        $data['name']  = $value->name;

        array_push($resultArray1, $data);
    }

    $users23 = array_unique($resultArray1,SORT_REGULAR);

    $finalUser = array();

    foreach($users23 as  $value55)
    {
        $object = (object)array();    

        $object->id = $value55['id'];
        $object->orgId=$value55['orgId'];
        $object->name=$value55['name'];

        array_push($finalUser,$object);
    }

    // reset if department is not in organisation
    if($flag==1)
    {
        $finalUser = array();
    }

    $cotRoleMapOptions = DB::table('cot_role_map_options')->where('status','Active')->get();

    $usersArray = array();
    foreach ($finalUser as $key => $value) 
    {
        //echo "<pre>";print_r($value);die();
        foreach($cotRoleMapOptions as $key => $maper)
        {
            $maperCountQuery = DB::table('cot_answers')
            ->where('orgId',$value->orgId)
            ->where('userId',$value->id)
            ->where('cot_role_map_option_id',$maper->id)
            ->where('status','Active');

            if($organisations->include_weekend != 1){
                $maperCountQuery->whereNotIn(DB::raw("(DATE_FORMAT(cot_answers.created_at,'%Y-%m-%d'))"),$getWeekendDates);
            }
            $maperCount = $maperCountQuery->sum('answer');
            //print_r($maperCount);die();

            $resultArray = array();
            $Value[$maper->maper_key] = $maperCount;    
            array_push($resultArray, $Value);
        }   
//echo "<pre>";print_r($resultArray);
//die();
        arsort($resultArray[0]);
  //     echo "<pre>";print_r($resultArray);die();
        $i = 0;
        $prev = "";
        $j = 0;

        $new  = array();
        $new1 = array();

        foreach($resultArray[0] as $key => $val)
        { 
            if($val != $prev)
            { 
              $i++; 
            }

            $new1['title'] = $key; 
            $new1['value'] = $i;        
            $prev = $val;

            if ($key==$cotRoleMapOptions[0]->maper_key) {
                $new1['priority'] = 1;
            }elseif ($key==$cotRoleMapOptions[1]->maper_key) {
                $new1['priority'] = 2;
            }elseif ($key==$cotRoleMapOptions[2]->maper_key) {
                $new1['priority'] = 3;
            }elseif ($key==$cotRoleMapOptions[3]->maper_key) {
                $new1['priority'] = 4;
            }elseif ($key==$cotRoleMapOptions[4]->maper_key) {
                $new1['priority'] = 5;
            }elseif ($key==$cotRoleMapOptions[5]->maper_key) {
                $new1['priority'] = 6;
            }elseif ($key==$cotRoleMapOptions[6]->maper_key) {
                $new1['priority'] = 7;
            }elseif ($key==$cotRoleMapOptions[7]->maper_key) {
                $new1['priority'] = 8;
            }
            array_push($new,$new1);
        }

        $value->new = $new;
        $value->totalKeyCount = $resultArray[0];
        array_push($usersArray, $value);

    }
    $usersArray = $this->sortarr($usersArray);

    $cotTeamRoleMapUserArray = $usersArray;
    //echo "<pre>";print_r($cotTeamRoleMapUserArray);die();

    /*get percentage values*/
    $shaper = 0; 
    $coordinator = 0; 
    $completerFinisher = 0; 
    $teamworker = 0; 
    $implementer = 0; 
    $monitorEvaluator = 0; 
    $plant = 0; 
    $resourceInvestigator = 0; 
    //echo "<pre>";print_r($usersArray);die();
    foreach ($usersArray as $key1 => $value1)
    {

        if($value1->shaper==1 || $value1->shaper==2 || $value1->shaper==3)
        {
            $shaper++;     
        }
        if($value1->coordinator==1 || $value1->coordinator==2 || $value1->coordinator==3)
        {
            $coordinator++;
        }
        if($value1->completerFinisher==1 || $value1->completerFinisher==2 || $value1->completerFinisher==3)
        {
            $completerFinisher++;
        }
        if($value1->teamworker==1 || $value1->teamworker==2 || $value1->teamworker==3)
        {
            $teamworker++;
        }
        if($value1->implementer==1 || $value1->implementer==2 || $value1->implementer==3)
        {
            $implementer++;
        }
        if($value1->monitorEvaluator==1 || $value1->monitorEvaluator==2 || $value1->monitorEvaluator==3)
        {
            $monitorEvaluator++;
        }
        if($value1->plant==1 || $value1->plant==2 || $value1->plant==3)
        {
            $plant++;
        }
        if($value1->resourceInvestigator==1 || $value1->resourceInvestigator==2 || $value1->resourceInvestigator==3)
        {
            $resourceInvestigator++;
        }
        
    }

    $data['shaper']              = $shaper;
    $data['coordinator']         = $coordinator;
    $data['completerFinisher']   = $completerFinisher;
    $data['teamworker']          = $teamworker;
    $data['implementer']         = $implementer;
    $data['monitorEvaluator']    = $monitorEvaluator;
    $data['plant']               = $plant;
    $data['resourceInvestigator']= $resourceInvestigator;

    // echo "<pre>";print_r($shaper);
    // echo "<pre>";print_r($coordinator);
    // echo "<pre>";print_r($completerFinisher);
    // echo "<pre>";print_r($teamworker);
    // echo "<pre>";print_r($implementer);
    // echo "<pre>";print_r($monitorEvaluator);
    // echo "<pre>";print_r($plant);
    // echo "<pre>";print_r($resourceInvestigator);
    // die();

    $totalUsers = count($finalUser);

    //echo "<pre>";print_r($totalUsers);
    //get presented values
    $cotTeamRoleMapGraphPercentage = array();

    if (!empty($data) && (!empty($totalUsers)))
    {

        $data['shaper']              = number_format((($shaper/$totalUsers)*100), 2, '.', '');
        $data['coordinator']         = number_format((($coordinator/$totalUsers)*100), 2, '.', '');
        $data['completerFinisher']   = number_format((($completerFinisher/$totalUsers)*100), 2, '.', '');
        $data['teamworker']          = number_format((($teamworker/$totalUsers)*100), 2, '.', '');
        $data['implementer']         = number_format((($implementer/$totalUsers)*100), 2, '.', '');
        $data['monitorEvaluator']    = number_format((($monitorEvaluator/$totalUsers)*100), 2, '.', '');
        $data['plant']               = number_format((($plant/$totalUsers)*100), 2, '.', '');
        $data['resourceInvestigator']= number_format((($resourceInvestigator/$totalUsers)*100), 2, '.', '');

        $cotTeamRoleMapGraphPercentage['data'] = $data;

    }
    else
    {

        $data['shaper']              = 0;
        $data['coordinator']         = 0;
        $data['completerFinisher']   = 0;
        $data['teamworker']          = 0;
        $data['implementer']         = 0;
        $data['monitorEvaluator']    = 0;
        $data['plant']               = 0;
        $data['resourceInvestigator']= 0;

        $cotTeamRoleMapGraphPercentage['data'] = $data;

    }

    $cotOptionsArr = array();
    $color = array();
    foreach ($cotRoleMapOptions as $key => $value) {
        array_push($cotOptionsArr, $value->maper);
        if ($value->categoryId == 1) {
            array_push($color, '#000');
        }elseif ($value->categoryId == 2    ) {
            array_push($color, '#e28b13');
        }elseif ($value->categoryId == 3) {
            array_push($color, '#eb1c24');
        }elseif ($value->categoryId == 4) {
            array_push($color, 'lightgray');
        }
    }
    //echo "<pre> Cot options arr=>";print_r($cotOptionsArr);
    //echo "<pre> Color=>";print_r($color);
    // $perArr = array(
    //         // $data['shaper']."~#eb1c24",
    //         // $data['coordinator']."~#000",
    //         // $data['completerFinisher']."~#e28b13",
    //         // $data['teamworker']."~#eb1c24",
    //         // $data['implementer']."~lightgray",
    //         // $data['monitorEvaluator']."~#e28b13",
    //         // $data['plant']."~lightgray",
    //         // $data['resourceInvestigator']."~#000",

    //         $data['shaper']."~#eb1c24",
    //         $data['coordinator']."~#000",
    //         $data['completerFinisher']."~#e28b13",
    //         $data['teamworker']."~lightgray",
    //         $data['implementer']."~#eb1c24",
    //         $data['monitorEvaluator']."~#000",
    //         $data['plant']."~#e28b13",
    //         $data['resourceInvestigator']."~lightgray",
    //     );

    $perArr = array(
            $data['shaper'],
            $data['coordinator'],
            $data['implementer'],
            $data['completerFinisher'],
            $data['monitorEvaluator'],
            $data['teamworker'],
            $data['plant'],
            $data['resourceInvestigator']
        );
    //$cotGraphArr = array_combine($cotOptionsArr, $perArr);

    //echo "<pre>Per aarr=>";print_r($perArr);
    //echo "<pre> final=>";print_r($cotGraphArr);
    //  die();

    // echo "<pre>";print_r($cotOptionsArr);
    // echo "<pre>";print_r($perArr);
    // die();

    $options = $cotOptionsArr;
    $colors = $color;
    $perArrs = $perArr;

    $result = array_map(function ($option, $color, $perArr) {
      return array_combine(
        ['option', 'color', 'perArr'],
        [$option, $color, $perArr]
      );
    }, $options, $colors, $perArrs);

    $collection = collect($result);

    $cotGraphArr = $collection->sortBy('option');

    $cotRadarChartArr = $collection->sortBy('perArr');

    //echo "<pre>";print_r($cotGraphArr);
    //echo "<pre>";print_r($cotRadarChartArr);die();

    $cotCategory = DB::table('cot_role_map_category')->where('status','Active')->get();

    $cotGraphConditionArr = array_filter(array_column($result, 'perArr'));

    ///print_r($cotGraphConditionArr);//die();

    // if (!empty($cotGraphArr1)) {
    //     print_r("yes");
    // }else
    // {
    //     print_r("no");
    // }
    // die();
    //echo "<pre>";print_r($cotGraphArr1);die();

    // @php
            //  $valueEpx=explode("~", $value);
            //  $val=$valueEpx[0];
            //  $color=$valueEpx[1];
            // @endphp
            // ["{{$key}}",{{ucfirst($val)}},'{{$color}}'],

    //$sorted->values()->all();


    if (empty($request->pdfStatus))
    {
        return view('admin/report/report_graph/cotTeamRoleMapGraph',compact('cotTeamRoleMapGraphPercentage','cotTeamRoleMapUserArray','offices','orgId','officeId','departmentId','departments','all_department','cotRoleMapOptions','cotGraphArr','cotCategory','cotGraphConditionArr','cotRadarChartArr','chartId'));
    } 
    else 
    {
        return compact('cotTeamRoleMapGraphPercentage','cotRoleMapOptions','cotGraphArr','cotRadarChartArr','cotGraphConditionArr');
    }



//     $finalArray  = array();
//     $orgId = base64_decode($request->orgId);

//     $organisations = DB::table('organisations')->where('id',$orgId)->where('status','Active')->first();

//     $offices = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();

//     $all_department = DB::table('all_department')->where('status','Active')->get();

//     $mapers = DB::table('cot_role_map_options')->where('status','Active')->get();

//     $officeId     = Input::get('officeId');
//     $departmentId = Input::get('departmentId');
//     $startDate    = Input::get('startDate');
//     $endDate      = Input::get('endDate');
//     $chartId      = Input::get('chartId');

//     $flag = 0 ;

//     $departments = [];
//     if (!empty($officeId)) 
//     {
//         $departments = DB::table('departments')
//         ->select('departments.id','all_department.department')
//         ->leftJoin('all_department','all_department.id','departments.departmentId')
//         ->where('departments.officeId',$officeId) 
//         ->where('departments.status','Active') 
//         ->orderBy('all_department.department','ASC')->get();

//    //for new department
//         $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
//     }
//     else
//     {
//         if(!empty($departmentId))
//         {
//             $departmentsNew = DB::table('departments')
//             ->select('departments.id','all_department.department')
//             ->leftJoin('all_department','all_department.id','departments.departmentId')
//             ->where('departments.orgId',$orgId)
//             ->where('departments.departmentId',$departmentId) 
//             ->where('departments.status','Active') 
//             ->orderBy('all_department.department','ASC')->get();
//         }
//     }

//     $query = DB::table('cot_answers')
//     ->select('users.id','users.orgId','users.name')
//     ->leftjoin('users','users.id','cot_answers.userId')
//     ->where('users.status','Active')
//     ->where('users.orgId',$orgId);

//     if(!empty($officeId))
//     {
//         $query->where('users.officeId',$officeId);
//     }

// //filter acc. to start and end date
//     if(!empty($startDate) && !empty($endDate)) 
//     {

//       $startDate = date_create($startDate);
//       $startDateMonth = date_format($startDate,"Y-m-d");

//       $endDate = date_create($endDate);

//       $endDateYear = date_format($endDate,"Y-m-d");

//       $query->where('users.created_at', '>=',$startDateMonth);
//       $query->where('users.created_at', '<=',$endDateYear);


//   }

//   if(!empty($departmentId))
//   {   
//     $i = 0;

//     if($departmentsNew->isEmpty())
//     {
//         $flag = 1;
//     }
//     foreach ($departmentsNew as $departments1) 
//     {        
//         if($i == 0)
//         {
//             $query->where('users.departmentId',$departments1->id);
//         }
//         else
//         {
//             $query->orWhere('users.departmentId',$departments1->id);
//         }

//         $i++;
//     }
// }


// $users = $query->get();

// $resultArray1 = array();

// foreach($users as $value)
// {
//     $data['id']    = $value->id;
//     $data['orgId'] = $value->orgId;
//     $data['name']  = $value->name;

//     array_push($resultArray1, $data);
// }

// $users23 = array_unique($resultArray1,SORT_REGULAR);

// $finalUser = array();

// foreach($users23 as  $value55)
// {
//     $object = (object)array();    

//     $object->id = $value55['id'];
//     $object->orgId=$value55['orgId'];
//     $object->name=$value55['name'];

//     array_push($finalUser,$object);

// }
// // reset if department is not in organisation
// if($flag==1)
// {
//     $finalUser = array();
// }

// $cotRoleMapOptions = DB::table('cot_role_map_options')->where('status','Active')->get();

// $usersArray = array();
// foreach ($finalUser as $key => $value) 
// {
//     foreach($cotRoleMapOptions as $key => $maper)
//     {
//         $maperCount = DB::table('cot_answers')
//         ->where('orgId',$value->orgId)
//         ->where('userId',$value->id)
//         ->where('cot_role_map_option_id',$maper->id)->where('status','Active')->sum('answer');

//         $resultArray = array();
//         $Value[$maper->maper_key] = $maperCount;    
//         array_push($resultArray, $Value);
//     }   

//     arsort($resultArray[0]);

//     $i = 0;
//     $prev = "";
//     $j = 0;

//     $new  = array();
//     $new1 = array();

//     foreach($resultArray[0] as $key => $val)
//     { 
//         if($val != $prev)
//         { 
//           $i++; 
//       }

//       $new1['title'] = $key; 
//       $new1['value'] = $i;        
//       $prev = $val;

//       if ($key==$cotRoleMapOptions[0]->maper_key) {
//           $new1['priority'] = 1;
//       }elseif ($key==$cotRoleMapOptions[1]->maper_key) {
//           $new1['priority'] = 2;
//       }elseif ($key==$cotRoleMapOptions[2]->maper_key) {
//           $new1['priority'] = 3;
//       }elseif ($key==$cotRoleMapOptions[3]->maper_key) {
//           $new1['priority'] = 4;
//       }elseif ($key==$cotRoleMapOptions[4]->maper_key) {
//           $new1['priority'] = 5;
//       }elseif ($key==$cotRoleMapOptions[5]->maper_key) {
//           $new1['priority'] = 6;
//       }elseif ($key==$cotRoleMapOptions[6]->maper_key) {
//           $new1['priority'] = 7;
//       }elseif ($key==$cotRoleMapOptions[7]->maper_key) {
//           $new1['priority'] = 8;
//       }
//       array_push($new,$new1);
//     } 

//     $value->new = $new;
//     $value->totalKeyCount = $resultArray[0];
//     array_push($usersArray, $value);

// }

// $usersArray = $this->sortarr($usersArray);

// $cotTeamRoleMapUserArray = $usersArray;

// /*get percentage values*/
// $shaper = 0; 
// $coordinator = 0; 
// $completerFinisher = 0; 
// $teamworker = 0; 
// $implementer = 0; 
// $monitorEvaluator = 0; 
// $plant = 0; 
// $resourceInvestigator = 0; 

// foreach ($usersArray as $key1 => $value1)
// {

//     if($value1->shaper==1 || $value1->shaper==2 || $value1->shaper==3)
//     {
//         $shaper++;     
//     }
//     if($value1->coordinator==1 || $value1->coordinator==2 || $value1->coordinator==3)
//     {
//         $coordinator++;
//     }
//     if($value1->completerFinisher==1 || $value1->completerFinisher==2 || $value1->completerFinisher==3)
//     {
//         $completerFinisher++;
//     }
//     if($value1->teamworker==1 || $value1->teamworker==2 || $value1->teamworker==3)
//     {
//         $teamworker++;
//     }
//     if($value1->implementer==1 || $value1->implementer==2 || $value1->implementer==3)
//     {
//         $implementer++;
//     }
//     if($value1->monitorEvaluator==1 || $value1->monitorEvaluator==2 || $value1->monitorEvaluator==3)
//     {
//         $monitorEvaluator++;
//     }
//     if($value1->plant==1 || $value1->plant==2 || $value1->plant==3)
//     {
//         $plant++;
//     }
//     if($value1->resourceInvestigator==1 || $value1->resourceInvestigator==2 || $value1->resourceInvestigator==3)
//     {
//         $resourceInvestigator++;
//     }
    
// }

// $data['shaper']              = $shaper;
// $data['coordinator']         = $coordinator;
// $data['completerFinisher']   = $completerFinisher;
// $data['teamworker']          = $teamworker;
// $data['implementer']         = $implementer;
// $data['monitorEvaluator']    = $monitorEvaluator;
// $data['plant']               = $plant;
// $data['resourceInvestigator']= $resourceInvestigator;

// $totalUsers = count($finalUser);
// //get presented values
// $cotTeamRoleMapGraphPercentage = array();

// if (!empty($data) && (!empty($totalUsers)))
// {

//     $data['shaper']              = number_format((($shaper/$totalUsers)*100), 2, '.', '');
//     $data['coordinator']         = number_format((($coordinator/$totalUsers)*100), 2, '.', '');
//     $data['completerFinisher']   = number_format((($completerFinisher/$totalUsers)*100), 2, '.', '');
//     $data['teamworker']          = number_format((($teamworker/$totalUsers)*100), 2, '.', '');
//     $data['implementer']         = number_format((($implementer/$totalUsers)*100), 2, '.', '');
//     $data['monitorEvaluator']    = number_format((($monitorEvaluator/$totalUsers)*100), 2, '.', '');
//     $data['plant']               = number_format((($plant/$totalUsers)*100), 2, '.', '');
//     $data['resourceInvestigator']= number_format((($resourceInvestigator/$totalUsers)*100), 2, '.', '');

//     $cotTeamRoleMapGraphPercentage['data'] = $data;

// }
// else
// {

//     $data['shaper']              = 0;
//     $data['coordinator']         = 0;
//     $data['completerFinisher']   = 0;
//     $data['teamworker']          = 0;
//     $data['implementer']         = 0;
//     $data['monitorEvaluator']    = 0;
//     $data['plant']               = 0;
//     $data['resourceInvestigator']= 0;

//     $cotTeamRoleMapGraphPercentage['data'] = $data;

// }

// $cotOptionsArr = array();
// $color = array();
// foreach ($cotRoleMapOptions as $key => $value) {
//     array_push($cotOptionsArr, $value->maper);
//     if ($value->categoryId == 1) {
//         array_push($color, '#000');
//     }elseif ($value->categoryId == 2    ) {
//         array_push($color, '#e28b13');
//     }elseif ($value->categoryId == 3) {
//         array_push($color, '#eb1c24');
//     }elseif ($value->categoryId == 4) {
//         array_push($color, 'lightgray');
//     }
// }

// $perArr = array(
//         $data['shaper'],
//         $data['coordinator'],
//         $data['completerFinisher'],
//         $data['teamworker'],
//         $data['implementer'],
//         $data['monitorEvaluator'],
//         $data['plant'],
//         $data['resourceInvestigator']
//     );

// $options = $cotOptionsArr;
// $colors = $color;
// $perArrs = $perArr;

// $result = array_map(function ($option, $color, $perArr) {
//   return array_combine(
//     ['option', 'color', 'perArr'],
//     [$option, $color, $perArr]
//   );
// }, $options, $colors, $perArrs);


// $collection = collect($result);

// $cotGraphArr = $collection->sortBy('option');

// $cotRadarChartArr = $collection->sortBy('perArr');

// $cotCategory = DB::table('cot_role_map_category')->where('status','Active')->get();

// $cotGraphConditionArr = array_filter(array_column($result, 'perArr'));

// /*print_r($cotTeamRoleMapGraphPercentage);
// die();*/
// if (empty($request->pdfStatus))
// {
//     return view('admin/report/report_graph/cotTeamRoleMapGraph',compact('cotTeamRoleMapGraphPercentage','cotTeamRoleMapUserArray','offices','orgId','officeId','departmentId','departments','all_department','cotRoleMapOptions','cotGraphArr','cotCategory','cotGraphConditionArr','cotRadarChartArr','chartId'));
// } 
// else 
// {
//     return compact('cotTeamRoleMapGraphPercentage','cotRoleMapOptions');
// }

}

// Get happy index report yearly

public function getReportHappyIndexGraphYear(Request $request){
    
    $happyIndexCountYear = array();
    $orgId          = base64_decode($request->orgId); 
    $departmentId   = Input::get('departmentId');
    $officeId       = Input::get('officeId');
    $offices        = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
    $all_department = DB::table('all_department')->where('status','Active')->get();
    //$flag = 0 ;
    $departments = [];


    //Get info of org
    $org = DB::table('organisations')
                ->select('include_weekend','created_at')
                ->where('id',$orgId)->first();

    //Get weekend flag
    $excludeWeekend = $org->include_weekend;   //1 for include weekend else not include   


    if (!empty($officeId)){
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }else{
        if(!empty($departmentId)){
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }
    //  echo "<pre>";print_r($departmentsNew);die();
    $happyUsersYearlyQuery = DB::table('happy_indexes')
    ->select(DB::raw('YEAR(happy_indexes.created_at) year'))
    ->leftjoin('users','users.id','happy_indexes.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('users.orgId',$orgId)
    ->where('users.status','Active')
    ->where('happy_indexes.status','Active')
    ->whereRaw('(happy_indexes.moodValue = 3 OR happy_indexes.moodValue = 2 OR happy_indexes.moodValue = 1)');
    // ->where('happy_indexes.moodValue',3)
    // ->orWhere('happy_indexes.moodValue',2)
    // ->orWhere('happy_indexes.moodValue',1);

    if($excludeWeekend!=1){
        $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
        $currentDate    = date('Y-m-d'); 
        //Get weekends
        $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
        //echo '<pre />';print_r($getWeekendDates);die;
        $happyUsersYearlyQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
    }         

    if(!empty($officeId) && empty($departmentId)){
        $happyUsersYearlyQuery->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)){
        $happyUsersYearlyQuery->where('users.officeId',$officeId);
        $happyUsersYearlyQuery->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)){
        $happyUsersYearlyQuery->where('departments.departmentId',$departmentId);
    }
    $happyUsersYearly = $happyUsersYearlyQuery->groupBy('year')->get();

    foreach ($happyUsersYearly as $key => $value) {

        $moodValue = array(1,2,3);
        $usersCount['indexCount'] =array();

        $totalUsersQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.orgId',$orgId)
        ->where('users.status','Active')
        ->where('happy_indexes.status','Active')
        //->where('happy_indexes.moodValue',$mValue)
        ->whereYear('happy_indexes.created_at',$value->year);

        if($excludeWeekend!=1){
            $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
            $currentDate    = date('Y-m-d'); 
            //Get weekends
            $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
            //echo '<pre />';print_r($getWeekendDates);die;
            $totalUsersQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
        }         


        if(!empty($officeId) && empty($departmentId)){
            $totalUsersQuery->where('users.officeId',$officeId);
        }elseif(!empty($officeId) && !empty($departmentId)){
            $totalUsersQuery->where('users.officeId',$officeId);
            $totalUsersQuery->where('users.departmentId',$departmentId);
        }elseif(empty($officeId) && !empty($departmentId)){
            $totalUsersQuery->where('departments.departmentId',$departmentId);
        }
        $totalUsers = $totalUsersQuery->count();

        foreach ($moodValue as $mValue) {
            $usersCountQuery = DB::table('happy_indexes')
                ->leftjoin('users','users.id','happy_indexes.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.orgId',$orgId)
                ->where('users.status','Active')
                ->where('happy_indexes.status','Active')
                ->where('happy_indexes.moodValue',$mValue)
                ->whereYear('happy_indexes.created_at',$value->year);

                if($excludeWeekend!=1){
                    $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
                    $currentDate    = date('Y-m-d'); 
                    //Get weekends
                    $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
                    //echo '<pre />';print_r($getWeekendDates);die;
                    $usersCountQuery->whereNotIn(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$getWeekendDates);
                }         

            if(!empty($officeId) && empty($departmentId)){
                $usersCountQuery->where('users.officeId',$officeId);
            }elseif(!empty($officeId) && !empty($departmentId)){
                $usersCountQuery->where('users.officeId',$officeId);
                $usersCountQuery->where('users.departmentId',$departmentId);
            }elseif(empty($officeId) && !empty($departmentId)){
                $usersCountQuery->where('departments.departmentId',$departmentId);
            }

            $moodCount = $usersCountQuery->count();

            if (!empty($totalUsers)){
                $moodCountUsers = number_format((($moodCount/$totalUsers)*100),2);
            }else{
                $moodCountUsers = 0;   
            }
            array_push($usersCount['indexCount'], $moodCountUsers);
        }
        
        $usersCount['year'] = $value->year;
        array_push($happyIndexCountYear, $usersCount);
    }

    // print_r($happyIndexCountYear);die();
    if (empty($request->pdfStatus)){
        return view('admin/report/report_graph/happyIndexGraph',compact('happyIndexCountYear','orgId','offices','all_department','departments','officeId','departmentId'));
    } else {
        return compact('happyIndexCountYear');
    }
}

// Get happy index report monthly

public function getReportHappyIndexGraphMonth(Request $request)
{
    $year = $request->year;
    $happyIndexCountMonth = array();
    $orgId = base64_decode($request->orgId);
    $departmentId = Input::get('departmentId');
    $officeId     = Input::get('officeId');

    $offices = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
    $all_department = DB::table('all_department')->where('status','Active')->get();
    //$flag = 0 ;
    $departments = [];

    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }
    else
    {
        if(!empty($departmentId))
        {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    $happyUsersYearlistQuery = DB::table('happy_indexes')
        ->select(DB::raw('YEAR(happy_indexes.created_at) year'))
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.orgId',$orgId)
        ->where('users.status','Active')
        ->where('happy_indexes.status','Active')
        ->whereRaw('(happy_indexes.moodValue = 3 OR happy_indexes.moodValue = 2 OR happy_indexes.moodValue = 1)');
        // ->where('happy_indexes.moodValue',3)
        // ->orWhere('happy_indexes.moodValue',2)
        // ->orWhere('happy_indexes.moodValue',1)
        if(!empty($officeId) && empty($departmentId))
        {
            $happyUsersYearlistQuery->where('users.officeId',$officeId);
        }
        elseif(!empty($officeId) && !empty($departmentId))
        {
            $happyUsersYearlistQuery->where('users.officeId',$officeId);
            $happyUsersYearlistQuery->where('users.departmentId',$departmentId);
        }
        elseif(empty($officeId) && !empty($departmentId))
        {
            $happyUsersYearlistQuery->where('departments.departmentId',$departmentId);
        }
        $happyUsersYearlist = $happyUsersYearlistQuery->groupBy('year')->get();
    
    //$months = array(1=>"Jan",2=>"Feb",3=>"Mar",4=>"Apr",5=>"May",6=>"Jun",7=>"Jul",8=>"Aug",9=>"Sep",10=>"Oct",11=>"Nov",12=>"Dec");
    $months = array(1=>"Jan ".$year,2=>"Feb ".$year,3=>"Mar ".$year,4=>"Apr ".$year,5=>"May ".$year,6=>"Jun ".$year,7=>"Jul ".$year,8=>"Aug ".$year,9=>"Sep ".$year,10=>"Oct ".$year,11=>"Nov ".$year,12=>"Dec ".$year);

    $monthInccount = 0;

    foreach ($months as $key => $value) {

        $moodValue = array(1,2,3);
        $monthCount['monthCount'] =array();

        $totalUsersQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.orgId',$orgId)
        ->where('users.status','Active')
        ->where('happy_indexes.status','Active')
        ->whereMonth('happy_indexes.created_at',$key)
        ->whereYear('happy_indexes.created_at',$year);

        if(!empty($officeId) && empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
        }
        elseif(!empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
            $totalUsersQuery->where('users.departmentId',$departmentId);
        }
        elseif(empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('departments.departmentId',$departmentId);
        }
        $totalUsers = $totalUsersQuery->count();

        foreach ($moodValue as $mValue) {
            $monthCountQuery = DB::table('happy_indexes')
                ->leftjoin('users','users.id','happy_indexes.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.orgId',$orgId)
                ->where('users.status','Active')
                ->where('happy_indexes.status','Active')
                ->where('happy_indexes.moodValue',$mValue)
                ->whereMonth('happy_indexes.created_at',$key)
                ->whereYear('happy_indexes.created_at',$year);
                
            if(!empty($officeId) && empty($departmentId))
            {
                $monthCountQuery->where('users.officeId',$officeId);
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $monthCountQuery->where('users.officeId',$officeId);
                $monthCountQuery->where('users.departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $monthCountQuery->where('departments.departmentId',$departmentId);
            }
            $moodCount = $monthCountQuery->count();

            if (!empty($totalUsers)) 
            {
                $moodCountUsers = number_format((($moodCount/$totalUsers)*100),2);
            }
            else
            {
                $moodCountUsers = 0;   
            }
            array_push($monthCount['monthCount'], $moodCountUsers);
        }
        $monthCount['monthName'] = $value;

        array_push($happyIndexCountMonth, $monthCount);

        if ($monthCount['monthCount'] != 0) {
            $monthInccount++;
        }
    }

    if (empty($monthInccount)) {
        $happyIndexCountMonth = array();
    }

    //get the maximum value of the happy index graph
    // $happyIndexYearMaxCount = $this->getReportHappyIndexGraphYear($request);

    // $maxValueArray = array();
    // foreach($happyIndexYearMaxCount['happyIndexCountYear'] as $value)
    // {
    //     foreach ($value['indexCount'] as $key => $value1) 
    //     {
    //         array_push($maxValueArray, $value1);
    //     }
    // }
    // if ($maxValueArray) {
    //     $max = max($maxValueArray);
    // }else
    // {
    //     $max = array();
    // }
   
    return view('admin/report/report_graph/happyIndexGraphMonth',compact('happyIndexCountMonth','year','orgId','offices','officeId','departmentId','departments','all_department','happyUsersYearlist'/*,'max'*/));
}

// Get happy index report weekly

public function getReportHappyIndexGraphWeek(Request $request)
{    
    $year = $request->year;
    $monthName = $request->month;
    $orgId = base64_decode($request->orgId); 
    $departmentId = Input::get('departmentId');
    $officeId     = Input::get('officeId');
    $happyIndexCountWeek = array();

    $offices = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
    $all_department = DB::table('all_department')->where('status','Active')->get();

    $departments = [];

    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }
    else
    {
        if(!empty($departmentId))
        {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    if ($monthName == "Jan ".$year) {
        $month = 1;
    }elseif ($monthName == "Feb ".$year) {
        $month = 2;
    }elseif ($monthName == "Mar ".$year) {
        $month = 3;
    }elseif ($monthName == "Apr ".$year) {
        $month = 4;
    }elseif ($monthName == "May ".$year) {
        $month = 5;
    }elseif ($monthName == "Jun ".$year) {
        $month = 6;
    }elseif ($monthName == "Jul ".$year) {
        $month = 7;
    }elseif ($monthName == "Aug ".$year) {
        $month = 8;
    }elseif ($monthName == "Sep ".$year) {
        $month = 9;
    }elseif ($monthName == "Oct ".$year) {
        $month = 10;
    }elseif ($monthName == "Nov ".$year) {
        $month = 11;
    }elseif ($monthName == "Dec ".$year) {
        $month = 12;
    }

    $monthCount = cal_days_in_month(CAL_GREGORIAN,$month,$year);

    $weeks = $monthCount%7;

    if ($weeks) {
        $arrayCount = array("Week 1","Week 2","Week 3","Week 4","Week 5");
    }
    else
    {
        $arrayCount = array("Week 1","Week 2","Week 3","Week 4");
    }
    $lastDay = date('t',strtotime($year."-".$month."-1"));
    $weekInccount = 0;
    $i = 1;$j = 7;
    foreach ($arrayCount as $key => $value) {
        $moodValue = array(1,2,3);
        $happyUsersWeekly['weekcount'] =array();

        $totalUsersQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.orgId',$orgId)
        ->where('users.status','Active')
        ->where('happy_indexes.status','Active')
        ->whereMonth('happy_indexes.created_at',$month)
        ->whereYear('happy_indexes.created_at',$year);

        if(!empty($officeId) && empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
        }
        elseif(!empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
            $totalUsersQuery->where('users.departmentId',$departmentId);
        }
        elseif(empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('departments.departmentId',$departmentId);
        }
        if ($i==29) {
            $totalUsersQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$lastDay." "."23:59:59"]);
        }
        else
        {
            $totalUsersQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$j." "."23:59:59"]);
        }
        $totalUsers = $totalUsersQuery->count();

        foreach ($moodValue as $mValue) {
            $happyUsersWeeklyQuery = DB::table('happy_indexes')
                ->select('happy_indexes.created_at')
                ->leftjoin('users','users.id','happy_indexes.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.orgId',$orgId)
                ->where('users.status','Active')
                ->where('happy_indexes.status','Active')
                ->where('happy_indexes.moodValue',$mValue);
            if(!empty($officeId) && empty($departmentId))
            {
                $happyUsersWeeklyQuery->where('users.officeId',$officeId);
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $happyUsersWeeklyQuery->where('users.officeId',$officeId);
                $happyUsersWeeklyQuery->where('users.departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $happyUsersWeeklyQuery->where('departments.departmentId',$departmentId);
            }
            if ($i==29) {
                $happyUsersWeeklyQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$lastDay." "."23:59:59"]);
            }
            else
            {
                $happyUsersWeeklyQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$j." "."23:59:59"]);
            }
            $moodCount = $happyUsersWeeklyQuery->count();

            if (!empty($totalUsers)) 
            {
                $moodCountUsers = number_format((($moodCount/$totalUsers)*100),2);
            }
            else
            {
                $moodCountUsers = 0;   
            }

            array_push($happyUsersWeekly['weekcount'], $moodCountUsers);
        }

        
        $i+=7;
        $j+=7;
        $happyUsersWeekly['week'] = $value;

        array_push($happyIndexCountWeek, $happyUsersWeekly);
        if ($happyUsersWeekly['weekcount'] != 0) {
            $weekInccount++;
        }
    }
    if (empty($weekInccount)) {
        $happyIndexCountWeek = array();
    }

    $monthsList = array(1=>"Jan ".$year,2=>"Feb ".$year,3=>"Mar ".$year,4=>"Apr ".$year,5=>"May ".$year,6=>"Jun ".$year,7=>"Jul ".$year,8=>"Aug ".$year,9=>"Sep ".$year,10=>"Oct ".$year,11=>"Nov ".$year,12=>"Dec ".$year);

    //get the maximum value of the happy index graph

    // $happyIndexYearMaxCount = $this->getReportHappyIndexGraphYear($request);

    // $maxValueArray = array();
    // foreach($happyIndexYearMaxCount['happyIndexCountYear'] as $value)
    // {
    //     foreach ($value['indexCount'] as $key => $value1) 
    //     {
    //         array_push($maxValueArray, $value1);
    //     }
    // }
    // if ($maxValueArray) {
    //     $max = max($maxValueArray);
    // }else
    // {
    //     $max = array();
    // }
    return view('admin/report/report_graph/happyIndexGraphWeek',compact('happyIndexCountWeek','year','orgId','month'/*,'max'*/,'monthsList','offices','officeId','departmentId','departments','all_department'));
}

// Get happy index report day wise

public function getReportHappyIndexGraphDay(Request $request)
{
    $year = $request->year;
    $month = $request->month;
    $week = $request->week;
    $happyIndexCountDay = array();
    $orgId = base64_decode($request->orgId); 
    $departmentId = Input::get('departmentId');
    $officeId     = Input::get('officeId');

    $offices = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
    $all_department = DB::table('all_department')->where('status','Active')->get();

    $departments = [];

    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }
    else
    {
        if(!empty($departmentId))
        {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    $days = array();

    $weekList =array();

    $daysInccount = 0;

    $monthCount = cal_days_in_month(CAL_GREGORIAN,$month,$year);

    $remainingWeeks = $monthCount%7;

    $totalUsers = 0;

    if ($month == 1) {
        $monthName = "Jan";
    }elseif ($month == 2) {
        $monthName = "Feb";
    }elseif ($month == 3) {
        $monthName = "Mar";
    }elseif ($month == 4) {
        $monthName = "Apr";
    }elseif ($month == 5) {
        $monthName = "May";
    }elseif ($month == 6) {
        $monthName = "Jun";
    }elseif ($month == 7) {
        $monthName = "Jul";
    }elseif ($month == 8) {
        $monthName = "Aug";
    }elseif ($month == 9) {
        $monthName = "Sep";
    }elseif ($month == 10) {
        $monthName = "Oct";
    }elseif ($month == 11) {
        $monthName = "Nov";
    }elseif ($month == 12) {
        $monthName = "Dec";
    }

    $chopYear = substr($year, -2);

    if ($week == "Week 1") {
        $i=1;
        $days = array($monthName." 1 ".$year,$monthName." 2 ".$year,$monthName." 3 ".$year,$monthName." 4 ".$year,$monthName." 5 ".$year,$monthName." 6 ".$year,$monthName." 7 ".$year);
    }
    else if ($week == "Week 2") {
        $i=8;
        $days = array($monthName." 8 ".$year,$monthName." 9 ".$year,$monthName." 10 ".$year,$monthName." 11 ".$year,$monthName." 12 ".$year,$monthName." 13 ".$year,$monthName." 14 ".$year);
        //$days = array("Day 8","Day 9","Day 10","Day 11","Day 12","Day 13","Day 14");
    }
    else if ($week == "Week 3") {
        $i=15;
        $days = array($monthName." 15 ".$year,$monthName." 16 ".$year,$monthName." 17 ".$year,$monthName." 18 ".$year,$monthName." 19 ".$year,$monthName." 20 ".$year,$monthName." 21 ".$year);
        //$days = array("Day 15","Day 16","Day 17","Day 18","Day 19","Day 20","Day 21");
    }
    else if ($week == "Week 4") {
        $i=22;
        $days = array($monthName." 22 ".$year,$monthName." 23 ".$year,$monthName." 24 ".$year,$monthName." 25 ".$year,$monthName." 26 ".$year,$monthName." 27 ".$year,$monthName." 28 ".$year);
        //$days = array("Day 22","Day 23","Day 24","Day 25","Day 26","Day 27","Day 28");
    }
    else if ($week == "Week 5") {
        $i=29;
        if ($remainingWeeks == 1) {
            $days = array($monthName." 29 ".$year);
            //$days = array("Day 29");
        }
        elseif ($remainingWeeks == 2) {
            $days = array($monthName." 29 ".$year,$monthName." 30 ".$year);
            //$days = array("Day 29","Day 30");
        }elseif ($remainingWeeks == 3) {
            $days = array($monthName." 29 ".$year,$monthName." 30 ".$year,$monthName." 31 ".$year);
            //$days = array("Day 29","Day 30","Day 31");
        }
    }

    foreach ($days as $key => $value) {
        $moodValue = array(1,2,3);
        $dayCount['dayCount'] =array();

        $totalUsersQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.orgId',$orgId)
        ->where('users.status','Active')
        ->where('happy_indexes.status','Active')
        ->whereDate('happy_indexes.created_at',$year."-".$month."-".$i);

        if(!empty($officeId) && empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
        }
        elseif(!empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
            $totalUsersQuery->where('users.departmentId',$departmentId);
        }
        elseif(empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('departments.departmentId',$departmentId);
        }
        $totalUsers = $totalUsersQuery->count();

        foreach ($moodValue as $mValue) {
            $dayCountquery = DB::table('happy_indexes')
                ->select('happy_indexes.created_at')
                ->leftjoin('users','users.id','happy_indexes.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.orgId',$orgId)
                ->where('users.status','Active')
                ->where('happy_indexes.status','Active')
                ->where('happy_indexes.moodValue',$mValue)
                ->whereDate('happy_indexes.created_at',$year."-".$month."-".$i);
            if(!empty($officeId) && empty($departmentId))
            {
                $dayCountquery->where('users.officeId',$officeId);
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $dayCountquery->where('users.officeId',$officeId);
                $dayCountquery->where('users.departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $dayCountquery->where('departments.departmentId',$departmentId);
            }

            $moodCount = $dayCountquery->count();

            if (!empty($totalUsers)) 
            {
                $moodCountUsers = number_format((($moodCount/$totalUsers)*100),2);
            }
            else
            {
                $moodCountUsers = 0;   
            }
            array_push($dayCount['dayCount'], $moodCountUsers);
        }
        $dayCount['dayName'] = $value; 

        array_push($happyIndexCountDay, $dayCount);
       
        $i++;
        if ($dayCount['dayCount'] != 0) {
            $daysInccount++;
        }
    }
    if (empty($daysInccount)) {
        $happyIndexCountDay = array();
    }

    if ($remainingWeeks) {
        $weekList = array("Week 1","Week 2","Week 3","Week 4","Week 5");
    }
    else
    {
        $weekList = array("Week 1","Week 2","Week 3","Week 4");
    }

    //get the maximum value of the happy index graph
    // $happyIndexYearMaxCount = $this->getReportHappyIndexGraphYear($request);

    // $maxValueArray = array();
    // $maxValueArray = array();
    // foreach($happyIndexYearMaxCount['happyIndexCountYear'] as $value)
    // {
    //     foreach ($value['indexCount'] as $key => $value1) 
    //     {
    //         array_push($maxValueArray, $value1);
    //     }
    // }
    // if ($maxValueArray) {
    //     $max = max($maxValueArray);
    // }else
    // {
    //     $max = array();
    // }

    if ($month == 1) {
        $monthName = "Jan ".$year;
    }elseif ($month == 2) {
        $monthName = "Feb ".$year;
    }elseif ($month == 3) {
        $monthName = "Mar ".$year;
    }elseif ($month == 4) {
        $monthName = "Apr ".$year;
    }elseif ($month == 5) {
        $monthName = "May ".$year;
    }elseif ($month == 6) {
        $monthName = "Jun ".$year;
    }elseif ($month == 7) {
        $monthName = "Jul ".$year;
    }elseif ($month == 8) {
        $monthName = "Aug ".$year;
    }elseif ($month == 9) {
        $monthName = "Sep ".$year;
    }elseif ($month == 10) {
        $monthName = "Oct ".$year;
    }elseif ($month == 11) {
        $monthName = "Nov ".$year;
    }elseif ($month == 12) {
        $monthName = "Dec ".$year;
    }

    return view('admin/report/report_graph/happyIndexGraphDay',compact('happyIndexCountDay','year','orgId','monthName','month'/*,'max'*/,'weekList','offices','officeId','departmentId','departments','all_department','week'));
}


/*sorting array by priority*/
function sortarr($arr1)
{

  $arrUsers = array();
  $newArray = array();
  $tes      = array();

  for($i=0; $i<count($arr1); $i++)
  {    
    $tes['userId']    = $arr1[$i]->id;
    // $tes['userName']  = $arr1[$i]->name;   

    $arrt = (array)$arr1[$i]->new;
    
    usort($arrt, function($x, $y)
    {

      if ($x['value']== $y['value'] ) 
      {
        if($x['priority']<$y['priority'])
        {
          return 0;
      }
      else
      {
          return 1;
      } 
  }
}); 

    $tt = array();
    foreach($arrt as $key => $value)
    {         
      if($value['value']!=0)
      {
        $tes[$value['title']] = $key+1;
    }
    else
    {
        $tes[$value['title']] = 0;
    }

}   

$tes['totalKeyCount'] = $arr1[$i]->totalKeyCount;

array_push($newArray,(object)$tes);
}

return $newArray;

}

/*generate pdf*/
public function showPdfReport(Request $request)
{
    $orgId = $request->id;
    $request->orgId = $orgId;
    $request->pdfStatus = true;
    //get data of DOT
    $dotObj = $this->show($request,$orgId);
    $org    = $dotObj->org;
    $dotValuesArray = $dotObj->dotValuesArray;

    //For index
    $indexOrg = $dotObj->indexOrg;
    $yearUsed = $dotObj->yearUsed;
    $currentYear = $dotObj->currentYear;
    $currentMonth = $dotObj->currentMonth;
    $graphVal = $dotObj->graphVal;
    $showGraph = $dotObj->showGraph;

    //get data of team role map percentage values
    $cotTeamRoleMapObj = $this->getCOTteamRoleMapGraph($request);
    $cotTeamRoleMapGraphPercentage  = $cotTeamRoleMapObj['cotTeamRoleMapGraphPercentage']['data'];
    $cotRoleMapOptions              = $cotTeamRoleMapObj['cotRoleMapOptions'];
    $cotGraphArr                    = $cotTeamRoleMapObj['cotGraphArr'];
    $cotRadarChartArr               = $cotTeamRoleMapObj['cotRadarChartArr'];
    $cotGraphConditionArr               = $cotTeamRoleMapObj['cotGraphConditionArr'];

    //get cot functional lens
    $cotFunLensObj = $this->getReportFunctionalGraph($request);
    $funcLensPercentageArray = $cotFunLensObj['funcLensPercentageArray']; 
    $keyNameArray            = $cotFunLensObj['keyNameArray'];
    //get sot culture structure
    $sotCulStrObj = $this->getReportCultureGraph($request);
    $sotCountArray = $sotCulStrObj['sotCountArray'];
    $sotStrDetailArr = $sotCulStrObj['sotStrDetailArr'];

    //get sot motivatin data
    $sotMotObj = $this->getReportMotivationalGraph($request);
    $SOTmotivationResultArray = $sotMotObj['SOTmotivationResultArray'];

    //get diagnostic data
    $diaObj = $this->getReportDiagnosticGraph($request);
    $diagnosticResultArray = $diaObj['diagnosticResultArray'];
    $diagnosticFinalArray = $diaObj['diagnosticFinalArray'];
    $subDiagFinalArray = $diaObj['subDiagFinalArray'];
    $diagnosticQueCatTbl = $diaObj['diagnosticQueCatTbl'];
    $organisation = $diaObj['organisation'];

    //get tribeometer data
    $triObj = $this->getReportTribeometerGraph($request);
    $tribeometerResultArray = $triObj['tribeometerResultArray'];

    //get happy index data
    $hapIndexObj = $this->getReportHappyIndexGraphYear($request);
    $happyIndexCountYear = $hapIndexObj['happyIndexCountYear'];

    return view('admin/report/report_graph/pdfReport',compact('org','dotValuesArray','cotTeamRoleMapGraphPercentage','funcLensPercentageArray','keyNameArray','sotCountArray','sotStrDetailArr','SOTmotivationResultArray','diagnosticResultArray','tribeometerResultArray','cotRoleMapOptions','happyIndexCountYear','indexOrg','yearUsed','currentYear','currentMonth','graphVal','showGraph','cotGraphArr','cotRadarChartArr','cotGraphConditionArr','diagnosticFinalArray','subDiagFinalArray','diagnosticQueCatTbl','organisation'));
}

public function exportPdf()
{
    $orgId  = base64_decode(Input::get('orgId'));
    if($orgId){

        //Get info of org
        $org  = DB::table('organisations')
                                ->where('id',$orgId)
                                ->where('status','Active')->first();
        $organisationName = $org->organisation;
        $organisationName = str_replace(" ", "_", $organisationName);

        $file_name    = $organisationName.'_report_'.date('MY').'.pdf'; 
        $hidden_html  = Input::get('hidden_html');

        $dompdf = new Dompdf();     
        $dompdf->loadHtml($hidden_html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();

        $font = $dompdf->getFontMetrics()->get_font("helvetica", "");
        $dompdf->getCanvas()->page_text(400, 550, "{PAGE_NUM}", $font, 10, array(0,0,0));
        $dompdf->getCanvas()->page_text(25, 550, date('M Y'), $font, 10, array(0,0,0));

        $dompdf->stream($file_name,array("Attachment"=>false));
        $output = $dompdf->output();   
        file_put_contents(public_path('uploads/pdf_report/').$file_name, $output);

        
        //Get info of org
        $org = DB::table('organization_pdf_reports')
                ->select('id')
                ->where('file_name',$file_name)
                ->where('organization_id',$orgId)
                ->first();
        if($org){
                DB::table('organization_pdf_reports')
                          ->where('organization_id',$orgId)
                          ->update(['created_at'=>date('Y-m-d H:i:s')]);
        }else{
            $insertOrgArray = array(
                'organization_id'   => $orgId,              
                'file_name'         => $file_name,
                'created_at'        => date('Y-m-d H:i:s')
            );
            $indexOrgReportRecords = DB::table('organization_pdf_reports')
                                    ->insertGetId($insertOrgArray);
        }    

    }else{
        return redirect('admin/admin-dashboard');
    }
}



/* get diagnostic sub graph values*/
public function getDiagnsticSubGraph(Request $request)
{
    $indexId      = Input::get('barIndex');
    $orgId        = Input::get('orgId');
    $departmentId = Input::get('departmentId');
    $officeId     = Input::get('officeId');
    $categoryId   = Input::get('categoryId');

    if(!$categoryId)
    {
        $categoryId = $indexId+1;
    }
    

    $diagnosticResultArray = array();

    $offices=DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();  
    $all_department = DB::table('all_department')->where('status','Active')->get();

    $diaQueCategoryList = DB::table('diagnostic_questions_category')->get();

    $flag = 0 ;
    $departments = [];
    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }
    else
    {
        if(!empty($departmentId))
        {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    $query = DB::table('diagnostic_answers')
    ->leftjoin('users','users.id','diagnostic_answers.userId')
    ->select('diagnostic_answers.userId')
    ->where('users.status','Active')
    ->groupBy('diagnostic_answers.userId')
    ->where('users.orgId',$orgId);

    if(!empty($officeId))
    {
        $query->where('users.officeId',$officeId);
    }


    if(!empty($departmentId))
    {   
        $i = 0;

        if($departmentsNew->isEmpty())
        {
            $flag = 1;
        }
        foreach ($departmentsNew as $departments1) 
        {       
            if($i == 0)
            {
                $query->where('users.departmentId',$departments1->id);
            }
            else
            {
                $query->orWhere('users.departmentId',$departments1->id);
            }
            $i++;
        }
    }


    $users = $query->get();
    $userCount = count($users);

 // reset if department is not in organisation
    if($flag==1)
    {
        $userCount = 0;
    }

    $questionTbl = DB::table('diagnostic_questions')->where('category_id',$categoryId)->where('status','Active')->get();

    $optionsTbl = DB::table('diagnostic_question_options')->where('status','Active')->get();

    $quecount    = count($questionTbl);
    $diaOptCount = count($optionsTbl);
    
    $perQuePercen = 0;

    if($userCount) 
    {
        foreach($questionTbl as $value)
        {

            $diaQuery = DB::table('diagnostic_answers')            
            ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
            ->leftjoin('users','users.id','=','diagnostic_answers.userId')
            ->where('users.status','Active')   
            ->where('diagnostic_answers.orgId',$orgId)
            ->where('diagnostic_questions.id',$value->id)
            ->where('diagnostic_questions.category_id',$categoryId);

            if(!empty($officeId))
            {
                $diaQuery->where('users.officeId',$officeId);
            }
            
            if(!empty($departmentId))
            {   
                $i = 0;

                if($departmentsNew->isEmpty())
                {
                    $flag = 1;
                }
                $department3 = array();
                foreach ($departmentsNew as $departments1) 
                {       
                    $departments2 = $departments1->id;
                    array_push($department3, $departments2);
                }
                
                $diaQuery->whereIn('users.departmentId',$department3);
            }

            $diaAnsTbl = $diaQuery->sum('answer'); 

            $perQuePercen = ($diaAnsTbl/$userCount);               

            $score = ($perQuePercen/($quecount*$diaOptCount));
            $totalPercentage = ($perQuePercen/($quecount*$diaOptCount))*100;

            $val['title']      =  ucfirst($value->measure);
            $val['score']      =  number_format((float)$score, 2, '.', '');
            $val['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');     

            array_push($diagnosticResultArray, $val);
        }
    }


    //get the maximum value of the diagnostic graph
    $request->orgId = base64_encode($orgId);
    $test = $this->getReportDiagnosticGraph($request);

    $maxValueArray = array();
    foreach($test->diagnosticResultArray as $value1)
    {
        array_push($maxValueArray, $value1['percentage']);
    }

    $max = 100;
    if(!empty($maxValueArray))
    {
        $max = max($maxValueArray);
        $max = ceil($max / 10) * 10;

        if($max >= 100)
        {
            $max = 100;
        }
    }


    return view('admin/report/report_graph/reportDiagnosticSubGraph',compact('diagnosticResultArray','offices','orgId','officeId','departmentId','departments','all_department','diaQueCategoryList','categoryId','max'));
}


/*get DOT bubble list for report*/
public function getDotThumbsUpBubbleList(){

    $resultArray = array();
    $beliefArr   = array();
    $userId      = Input::get('userId');
    $date        = Input::get('date');
    $orgId       = base64_decode(Input::get('orgId'));

     //Get info of org
    $org = DB::table('organisations')
                ->select('include_weekend','created_at')
                ->where('id',$orgId)->first();

    //Get weekend flag
    $excludeWeekend = $org->include_weekend;   //1 for include weekend else not include   



    $month = '';
    $year  = '';
    if($date){
        $dateObj = date_create($date);
        $year    = date_format($dateObj,"Y");
        $month   = date_format($dateObj,"m");
    }

    $dots      = DB::table('dots')->select('id')->where('orgId',$orgId)->first();
    $userList  = DB::table('users')->select('id','name')
                ->where('orgId',$orgId)->where('status','Active')->get(); 

    if(!empty($dots)){
        $dotId = $dots->id;
        $beliefsData = DB::table('dots_beliefs')->where('dotId',$dotId)->get();
        foreach($beliefsData as $bValue){

            $belief['id']   = $bValue->id;
            $belief['name'] = ucfirst($bValue->name);

            $dotsValue = DB::table('dots_values')->where('beliefId',$bValue->id)
                        ->where('status','Active')->get();

            $dotsValueArr = array();
            foreach ($dotsValue as $Vvalue){
                $dotValueList  = DB::table('dot_value_list')->where('id',$Vvalue->name)->first();
                $upVotTblQuery = DB::table('dot_bubble_rating_records')            
                ->where('dot_belief_id',$bValue->id)
                ->where('dot_value_name_id',$Vvalue->name)
                ->where('bubble_flag',"1");

                if($excludeWeekend!=1){
                    $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
                    $currentDate    = date('Y-m-d'); 
                    //Get weekends
                    $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
                    //echo '<pre />';print_r($getWeekendDates);die;
                    $upVotTblQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$getWeekendDates);
                }         

                if($month && $year){
                    $upVotTblQuery->whereYear('created_at', '=', $year)
                    ->whereMonth('created_at', '=', $month);
                }
                
                if($userId){
                  $upVotTblQuery->where('to_bubble_user_id',$userId);
              }

              $dotV['upVotes']   = $upVotTblQuery->count();

              $dValuesName = '';
              if($dotValueList){
                  $dValuesName = $dotValueList->name;
              }
              $dotV["id"]        = $Vvalue->id;
              $dotV['name']      = $dValuesName;          


              $valuesRatingsQuery = db::table('dot_values_ratings')->where('valueId', $Vvalue->id);
              if($excludeWeekend!=1){
                $orgCreatedDate = date('Y-m-d',strtotime($org->created_at));
                $currentDate    = date('Y-m-d'); 
                //Get weekends
                $getWeekendDates = $this->getDatesFromRange($orgCreatedDate, $currentDate);
                //echo '<pre />';print_r($getWeekendDates);die;
                $valuesRatingsQuery->whereNotIn(DB::raw("(DATE_FORMAT(dot_values_ratings.created_at,'%Y-%m-%d'))"),$getWeekendDates);
            }                   
              if($userId){
                  $valuesRatingsQuery->where('userId',$userId);
              }            

              $valuesRatings = $valuesRatingsQuery->first();
              $dotV["ratings"]='';
              if($valuesRatings){
                  $dotV["ratings"]= $valuesRatings->ratings;
              }
              array_push($dotsValueArr, $dotV);      
            }

            $belief['beliefValueArr'] = $dotsValueArr;
            array_push($beliefArr, $belief);      
        }
    }

    return view('admin/report/report_graph/dotThumbsuplist',compact('beliefArr','orgId','userList','userId','date'));
}

}