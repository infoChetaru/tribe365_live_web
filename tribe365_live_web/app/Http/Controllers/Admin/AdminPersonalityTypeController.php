<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;

class AdminPersonalityTypeController extends Controller
{
	public function index(Request $request) {
        $a = app('App\Http\Controllers\Admin\CommonController')->checkUser(Auth::user());

        if (!empty($a)) {
          return redirect('/admin');
        }
        
    	$categoryId = $request->categoryId;

    	$query = DB::table('personality_type_questions AS ptq')
    		->select('ptq.id','ptq.question','ptqc.title','ptqc.id AS category_id','ptq.summaryTrait')
	        ->leftjoin('personality_type_questions_category AS ptqc','ptqc.id','ptq.category_id')
	        ->where('ptq.status','Active')
	        ->where('ptqc.status','Active');
        if($categoryId){
            $query->where('ptq.category_id',$categoryId);
        }
        $perTypeQuestionList = $query->paginate(10);

    	$perTypeQueCategoryList = DB::table('personality_type_questions_category')->get();

    	return view('admin/personalityType/personalityTypeQuestions',compact('perTypeQuestionList','perTypeQueCategoryList','categoryId'));
    }

    public function updatePerTypeQuestions(Request $request) {
    	$perTypeCateId 	= Input::get('perTypeCateId');
    	$questionId 	= Input::get('questionId');

        if(!empty($questionId) && !empty($perTypeCateId) && trim(Input::get('perTypeQuestion')) && trim(Input::get('perTypeSummaryTrait'))){

        	DB::table('personality_type_questions')->where('id',$questionId)->update([
        		'question' 		=> trim(Input::get('perTypeQuestion')),
        		'summaryTrait' 	=> trim(Input::get('perTypeSummaryTrait')),
        		'category_id' 	=> $perTypeCateId,
        		'updated_at' 	=> date('Y-m-d H:i:s')
        	]);
        }
        return redirect()->back(); 
    }

    public function deletePerTypeQuestions() {
    	$cateId = Input::get('cateId');

    	if (!empty($cateId)) {		
            DB::table('personality_type_questions')->where('id',$cateId)->delete();
            
			DB::table('personality_type_dashboard_subgraph')->where('quesId',$cateId)->delete();
			return json_encode(array('status'=>200,'message'=>'Personality Type Question Successfully Deleted'));
    	}else{
			return json_encode(array('status'=>400,'message'=>'No Question Found.'));
    	}
    }

    public function personalityTypeOption(Request $request){
    	$perTypeOptTbl = DB::table('personality_type_question_options')->where('status','Active')->orderBy('option_rating','ASC')->paginate(10);

        $optionRating = DB::table('personality_type_question_options')->where('status','Active')->orderBy('option_rating','DESC')->first();

        $maxOptRating = 0;
        if (!empty($optionRating)) {
            $maxOptRating = $optionRating->option_rating;
            $maxOptRating++;
        }
    	return view('admin/personalityType/personalityTypeOptions',compact('perTypeOptTbl','maxOptRating'));
    }

    public function personalityTypeValue(Request $request){
    	$categoryTbl = DB::table('personality_type_questions_category')->where('status','Active')->paginate(10);
    	return view('admin/personalityType/personalityTypeValues',compact('categoryTbl'));
    }

    public function addPersonalityTypeValue(Request $request){
    	$category = Input::get('addCategory');
    	if (!empty($category)) {
    		DB::table('personality_type_questions_category')->insertGetId([
    			'title' => $category
    		]);
    	}
    	return array('status'=>'success');
    }

    public function updatePerTypeCategories(Request $request){
    	$titleId = base64_decode(Input::get('titleId'));        

        $updateArray = array();
        if(trim(Input::get('title'))) {
            $updateArray['title'] 		= trim(Input::get('title'));
            $updateArray['updated_at'] 	= date('Y-m-d H:i:s');
            
        	DB::table('personality_type_questions_category')->where('id',$titleId)->update($updateArray);
        }
        return redirect()->back(); 
    }

    public function deletePerTypeCategory(Request $request) {
    	$cateId = Input::get('cateId');

    	if (!empty($cateId)) {		
            DB::table('personality_type_questions_category')->where('id',$cateId)->delete();

            DB::table('personality_type_dashboard_graph')->where('categoryId',$cateId)->delete();

			DB::table('personality_type_dashboard_subgraph')->where('categoryId',$cateId)->delete();
			return json_encode(array('status'=>200,'message'=>'Personality Type Category Successfully Deleted'));
    	}else{
			return json_encode(array('status'=>400,'message'=>'No Category Found.'));
    	}
    }

    public function addPersonalityTypeOption(Request $request){
    	$optionName 	= trim(Input::get('optionName'));
    	$optionRating 	= Input::get('optionRating');

        $optRtg = DB::table('personality_type_question_options')->where('option_rating',$optionRating)->first();
        if (!empty($optRtg)) {
            return array('status'=>'fail');
        }

    	if (!empty($optionName) && !empty($optionName)) {
    		DB::table('personality_type_question_options')->insertGetId([
    			'option_name' 	=> $optionName,
    			'option_rating' => $optionRating
    		]);
    	}
    	return array('status'=>'success');
    }

    public function deletePersonalityTypeOption(Request $request){
    	$cateId = Input::get('cateId');

    	if (!empty($cateId)) {		
			DB::table('personality_type_question_options')->where('id',$cateId)->delete();
			return json_encode(array('status'=>200,'message'=>'Personality Type Category Successfully Deleted'));
    	}else{
			return json_encode(array('status'=>400,'message'=>'No Category Found.'));
    	}
    }

    public function updatePersonalityTypeOption(Request $request){
        $optionId       = base64_decode(Input::get('optionId'));
    	$optionRating   = Input::get('optionRating');

        $optRtg = DB::table('personality_type_question_options')->where('option_rating',$optionRating)->first();
        if (!empty($optRtg)) {
            return redirect()->back()->with('success','Value Score already given to other option.');
        }
        if (empty($optionRating)) {
            return redirect()->back();
        }

        $updateArray = array();
        // if(trim(Input::get('optionName')) && trim(Input::get('optionRating')))
        if(trim(Input::get('optionName'))){
            $updateArray['option_name'] 	= trim(Input::get('optionName'));
            $updateArray['option_rating'] 	= trim(Input::get('optionRating'));
            $updateArray['updated_at'] 	= date('Y-m-d H:i:s');
        	DB::table('personality_type_question_options')->where('id',$optionId)->update($updateArray);
        }
        return redirect()->back(); 
    }

    public function addPersonalityTypeQuestion(Request $request) {
    	$question 		= trim(Input::get('question'));
    	$summaryTrait 	= trim(Input::get('summaryTrait'));
    	$categoryId 	= Input::get('categoryId');

    	if (!empty($question) && !empty($summaryTrait) && !empty($categoryId)) {
    		DB::table('personality_type_questions')->insertGetId([
    			'question' 		=> $question,
    			'summaryTrait' 	=> $summaryTrait,
    			'category_id' 	=> $categoryId
    		]);
    		return array('status'=>'success');
    	}else{
    		return array('status'=>'fail');
    	}
    }
}

?>