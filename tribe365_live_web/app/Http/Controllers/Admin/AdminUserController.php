<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Validator;
use Hash;
use Illuminate\Support\Facades\Input;
use Config;
use View;
use PHPExcel; 
use PHPExcel_IOFactory;
use Mail;

class AdminUserController extends Controller
{

  public function __construct(Request $request)
  {
    $a = app('App\Http\Controllers\Admin\CommonController')->checkUser(Auth::user());

    if (!empty($a)) {
      return redirect('/admin');
    }
    
    $orgid = base64_decode($request->segment(3));

    $organisations = DB::table('organisations')
    ->select('ImageURL')
    ->where('id',$orgid)
    ->first();


    if(!empty($organisations->ImageURL))
    {
      $org_detail = base64_encode($orgid)."-".$organisations->ImageURL;

      View::share('org_detail', $org_detail);
    }
    else
    {
      $org_detail = "";

      View::share('org_detail', $org_detail);

    }

  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
      $users = DB::table('users')
      ->where('status','Active')
      ->where('roleId',3)
      ->paginate(3); 

      return view('admin/user/user',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewStaffList($orgId)
    {
      $orgId = base64_decode($orgId);

      $users = DB::table('users')    
      ->select('users.id','users.name','users.lastName','users.email','users.orgId','users.officeId','users.departmentId','organisations.organisation','offices.office','all_department.department') 
      ->leftJoin('offices', 'users.officeId', '=', 'offices.id')           
      ->leftJoin('departments', 'users.departmentId', '=', 'departments.id')
      ->leftJoin('all_department','departments.departmentId', '=', 'all_department.id')
      ->leftJoin('organisations','users.orgId', '=', 'organisations.id')  
      ->where('users.roleId',3)
      ->where('users.status','Active')
      ->where('users.orgId',$orgId)        
      ->orderBy('users.id','DESC')
      ->paginate(10);     
      return view('admin/user/user',compact('users'));

    }
    /**/
    public function addStaff($id)
    {

      $orgId = base64_decode($id);

      $departments = DB::table('all_department')
      ->where('status','Active')
      ->orderBy('department','ASC')
      ->get();

      $offices = DB::table('offices')
      ->where('status','Active')
      ->orderBy('office','ASC')
      ->where('orgId',$orgId)
      ->get();

      return view('admin/user/addDepartmentUser',compact('departments','offices','orgId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $postData = Input::all();

      $status = DB::table('users')->where('email',$postData['email'])->where('status','Active')->first();

      if(!empty($status))
      {
        return redirect('admin/view-staff/'.base64_encode($postData['orgId']))->with('message','Email already exists');
      }
      else
      {

        $insertArray = array(
          'departmentId'   => $postData['departmentId'],              
          'orgId'          => $postData['orgId'],    
          'officeId'       => $postData['officeId'],
          'status'         => 'Active', 
          'created_at'     => date('Y-m-d H:i:s')
        );

        $isDepartment = DB::table('departments')
        ->where('officeId',$postData['officeId'])
        ->where('departmentId',$postData['departmentId'])
        ->first(); 

        if(empty($isDepartment))    
        {    
          $departmentId = DB::table('departments')->insertGetId($insertArray);
        }
        else
        {
         $departmentId =  $isDepartment->id;
       }

       if($departmentId)
       {

        $userInsertArray = array(
          'name'           => ucfirst($postData['name']),
          'lastName'       => ucfirst($postData['lastName']),
          'email'          => $postData['email'],
          /*'password'       => bcrypt($postData['password']),
          'password2'      => base64_encode($postData['password']),*/
          'status'         => 'Active',
          'officeId'       => $postData['officeId'],
          'orgId'          => $postData['orgId'],    
          'departmentId'   => $departmentId,
          'created_at'     => date('Y-m-d H:i:s'),                
          'roleId'         => 3
        );

        $status = DB::table('users')->insertGetId($userInsertArray);
      }

      $orgId = $postData['orgId'];

      /*send email to new user*/
      $data  = array('name'=>$postData['name'], 'lastName'=>$postData['lastName'],'passwordLink'=>base64_encode($postData['email']));

      $email = $postData['email'];
      Mail::send('layouts/userPasswordSetEmail', $data, function($message) use($email) 
      {
        $message->from('notification@tribe365.co', 'Tribe365');
        $message->to($email);
        $message->subject('Verify your email address');
      });

      return redirect('admin/view-staff/'.base64_encode($postData['orgId']))->with('message','Record Added Successfully');
    }
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

     $userId = base64_decode($id);

     $users = DB::table('users')->where('id',$userId)->first();

     $organisations = DB::table('organisations')->where('status','Active')->orderBy('organisation','ASC')->get();

     $offices = DB::table('offices')->select('id','office')->where('id',$users->officeId)->first();

     $allOffices = DB::table('offices')->select('id','office')->where('orgId',$users->orgId)->get();

     $departments = DB::table('departments')->where('id',$users->departmentId)->first();

     $customeDepartments = DB::table('all_department')->where('status','Active')->get();

     return view('admin/user/editDepartmentUser',compact('users','organisations','offices','departments','customeDepartments','allOffices'));
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $userId = base64_decode($id);

      $name         = Input::get('name');
      $lastName     = Input::get('lastName');
      // $password     = Input::get('password');
      $orgId        = Input::get('orgId');
      $officeId     = Input::get('officeId');
      $departmentId = Input::get('departmentId');


      $updateArray = array();
      $updateArray['name']         = ucfirst($name);       
      $updateArray['lastName']     = ucfirst($lastName);       
      // $updateArray['password']     = bcrypt($password);
      // $updateArray['password2']    = base64_encode($password);
      $updateArray['orgId']        = $orgId;             
      $updateArray['updated_at']   = date('Y-m-d H:i:s');

      // $userStatus = DB::table('users')->where('id',$userId)->first();

      $department = DB::table('departments')->where('officeId',$officeId)->where('departmentId',$departmentId)->first();

      if (!empty($department))
      {
       $departmentId = $department->id;

     }else{

      $departmentId = DB::table('departments')
      ->insertGetId(['officeId'=>$officeId,'departmentId'=>$departmentId,'orgId'=>$orgId,'created_at'=>date('Y-m-d H:i:s')]);
    }

    $updateArray['departmentId'] = $departmentId;
    $updateArray['officeId'] = $officeId;

     /* if($userStatus)
      {
        DB::table('departments')->where('id', $userStatus->departmentId)->update(['departmentId' => $departmentId,'updated_at'=> date('Y-m-d H:i:s')]);
      }*/

      DB::table('users')->where('id', $userId)->update($updateArray);

      $orgId = base64_encode($orgId);

      return redirect("admin/view-staff/".$orgId)->with('message','Record updated Successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $userId = Input::get('userId');
      $orgId =  base64_encode(Input::get('orgId'));

      // $userId = base64_decode($id);

      DB::table('users')->where('id', $userId)->update(['status'=>'Inactive']);

      DB::table('oauth_access_tokens')->where('user_id', $userId)->update(['revoked'=>1]);

      return json_encode(array('status'=>200,'orgId'=>$orgId));
      // return redirect()->back()->with('message','User Deleted Successfully.');
    }


    /*add users by csv file*/
    public function addUserCSV(Request $request)
    {

     $orgId = Input::get('orgId');

     $objPHPExcel = new PHPExcel();

     $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');

     if(!empty($_FILES['csv_file']['name']) && in_array($_FILES['csv_file']['type'], $csvMimes))
     {

       $file = $request->file('csv_file'); 
       $imageName   = 'user_'.time().'.'.$file->getClientOriginalExtension();
       $destination = public_path('uploads/user_excel_file/');
       $file->move($destination, $imageName);
       $tmpfname = public_path('uploads/user_excel_file/'.$imageName);

       $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
       $excelObj    = $excelReader->load($tmpfname);
       $worksheet   = $excelObj->getSheet(0);
       $lastRow     = $worksheet->getHighestRow();

       $emailArray = array();
       for($row = 2; $row <= $lastRow; $row++) 
       {

        $name         = $worksheet->getCell('A'.$row)->getValue();
        $email        = $worksheet->getCell('B'.$row)->getValue();        
        $officeId     = $worksheet->getCell('C'.$row)->getValue();
        $departmentId = $worksheet->getCell('D'.$row)->getValue();

        $isUser = DB::table('users')->where('email',$email)->where('status','Active')->first();

        if(empty($isUser) && !empty($email))
        {
          $userInsertArray = array(
            'name'           => ucfirst($name),
            'email'          => $email,            
            'status'         => 'Active',
            'officeId'       => $officeId,
            'orgId'          => $orgId,    
            'departmentId'   => $departmentId,
            'created_at'     => date('Y-m-d H:i:s'),                
            'roleId'         => 3
          );

          $insertId = DB::table('users')->insertGetId($userInsertArray);


          /*send email to new user*/
          $data  = array('name'=>$name ,'passwordLink'=>base64_encode($email));
          
          Mail::send('layouts/userPasswordSetEmail', $data, function($message) use($email) 
          {
            $message->from('notification@tribe365.co', 'Tribe365');
            $message->to($email);
            $message->subject('Verify your email address');
          });

        } 
        else if(!empty($isUser->email))
        {

          $updateArray = array(
            'name'           => ucfirst($name),   
            'officeId'       => $officeId,
            'orgId'          => $orgId,    
            'departmentId'   => $departmentId,
            'updated_at'     => date('Y-m-d H:i:s'), 
          );        

          DB::table('users')->where('email',$isUser->email)->update($updateArray);

        }

        array_push($emailArray, $email);
      }

      //remove user which is not present in excel sheet
      $userList = DB::table('users')->where('status','Active')->where('orgId',$orgId)->get();

      foreach($userList as  $uValue)
      {

        if(!in_array($uValue->email, $emailArray))
        {  
          echo "inactivate  ".$uValue->email;   
          DB::table('users')->where('email',$uValue->email)->update(['status'=>'Inactive']);

          //unauthenticate token
          DB::table('oauth_access_tokens')->where('user_id', $uValue->id)->update(['revoked'=>1]);
        } 

      }

    }
    else
    {
      return array('status'=>'error','message'=>'Invalid file.');
    }


  }



  /*Export user list CSV*/
  public function exportUserList(Request $request)
  {

    $orgId = base64_decode($request->id);

    $org = DB::table('organisations')->select('organisation')->where('id',$orgId)->first();

    $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get();

    $departments =DB::table('departments')->select('departments.id','all_department.department','departments.officeId')->leftJoin('all_department','departments.departmentId', '=', 'all_department.id')     
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->where('departments.orgId',$orgId)
    ->get();  

    $orgName ='';
    if($org)
    {
      $orgName = $org->organisation;
    }

    $xls_filename = $orgName."_staff_list_" . date('Y-m-d')."_".time() . ".xls"; 
    
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);

    $heading = array(     
      'name' => 'NAME',
      'email' => 'EMAIL',    
      'officeId' => 'OFFICE ID',
      'departmentId' => 'DEPARTMENT ID',

    );

    $no_of_cols = count($heading);
    $rowNumberH = 1;
    $colH = 'A';
    $columns = array('0' => 'A', '1' => 'B', '2' => 'C', '3' => 'D', '4' => 'E', '5' => 'F', '6' => 'G', '7' => 'H', '8' => 'I', '9' => 'J', '10' => 'K', '11' => 'L', '12' => 'M', '13' => 'N', '14' => 'O', '15' => 'P', '16' => 'Q', '17' => 'R', '18' => 'S', '19' => 'T', '20' => 'U', '21' => 'V', '22' => 'W', '23' => 'X', '24' => 'Y', '25' => 'Z');

    foreach ($heading as $h)
    {

      $objPHPExcel->getActiveSheet()->setTitle("Staffs");
      $objPHPExcel->getActiveSheet()->setCellValue($colH.$rowNumberH, $h);
      $objPHPExcel->getActiveSheet()->getRowDimension($rowNumberH)->setRowHeight(20);
      $objPHPExcel->getActiveSheet()->getColumnDimension($colH)->setWidth(20);

      $colH++;
    }

    $users = DB::table('users')->where('status','Active')->where('orgId',$orgId)->get();

    $row = 2;
    foreach ($users as $value)
    {
      $name = preg_replace('/[-?]/', '', $value->name);

      $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $name);
      $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
      $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);

      $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value->email); 
      $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
      $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);

      $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value->officeId);
      $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
      $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);

      $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value->departmentId);
      $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
      $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

      $row++;
    }

    $objPHPExcel->getActiveSheet()->setTitle("Staffs");

    $setTitleArray = array('Offices','Departments');

    for ($j=0; $j <count($setTitleArray) ; $j++)
    { 

      // Add new sheet
      $objWorkSheet = $objPHPExcel->createSheet($j+1);
      //for new offices sheet
      if($j==0)
      {

        $objWorkSheet->setCellValue('A1','OFFICE ID');
         // $objWorkSheet->setCellValue()->getRowDimension('1')->setRowHeight(20);
        // $objWorkSheet->setCellValue()->getColumnDimension('A')->setWidth(20);

        $objWorkSheet->setCellValue('B1', 'OFFICE NAME');
        // $objWorkSheet->setCellValue()->getRowDimension('1')->setRowHeight(20);
        // $objWorkSheet->setCellValue()->getColumnDimension('B')->setWidth(25);

        $rowOfc = 2;
        foreach($offices as  $oValue)
        {

          $objWorkSheet->setCellValue('A'.$rowOfc, $oValue->id);
          // $objWorkSheet->setCellValue()->getRowDimension($rowOfc)->setRowHeight(20);
          // $objWorkSheet->setCellValue()->getColumnDimension('A')->setWidth(20);

          $objWorkSheet->setCellValue('B'.$rowOfc, $oValue->office);
          // $objWorkSheet->setCellValue()->getRowDimension($rowOfc)->setRowHeight(20);
          // $objWorkSheet->setCellValue()->getColumnDimension('B')->setWidth(25);

          $rowOfc++;
        }
      }
        //for departments
      else if($j==1)
      {

        $objWorkSheet->setCellValue('A1','DEPARTMENT ID');
        // $objWorkSheet->setCellValue()->getRowDimension('1')->setRowHeight(25);
        // $objWorkSheet->setCellValue()->getColumnDimension('A')->setWidth(20);

        $objWorkSheet->setCellValue('B1', 'DEPARTMENT NAME');
        // $objWorkSheet->setCellValue()->getRowDimension('1')->setRowHeight(20);
        // $objWorkSheet->setCellValue()->getColumnDimension('B')->setWidth(20);

        $objWorkSheet->setCellValue('C1', 'OFFICE ID');
        // $objWorkSheet->setCellValue()->getRowDimension('1')->setRowHeight(20);
        // $objWorkSheet->setCellValue()->getColumnDimension('C')->setWidth(20);

        $rowDept = 2;
        foreach($departments as  $dValue)
        {

          $objWorkSheet->setCellValue('A'.$rowDept, $dValue->id);
          // $objWorkSheet->setCellValue()->getRowDimension($rowDept)->setRowHeight(20);
          // $objWorkSheet->setCellValue()->getColumnDimension('A')->setWidth(20);

          $objWorkSheet->setCellValue('B'.$rowDept, $dValue->department);
          // $objWorkSheet->setCellValue()->getRowDimension($rowDept)->setRowHeight(20);
          // $objWorkSheet->setCellValue()->getColumnDimension('B')->setWidth(20);

          $objWorkSheet->setCellValue('C'.$rowDept, $dValue->officeId);
          // $objWorkSheet->setCellValue()->getRowDimension($rowDept)->setRowHeight(20);
          // $objWorkSheet->setCellValue()->getColumnDimension('C')->setWidth(20);

          $rowDept++;
        }

      }
      // Rename sheet
      $objWorkSheet->setTitle("$setTitleArray[$j]");

    }


    // ob_end_clean();
    header( "Content-type: application/vnd.ms-excel" );
    header('Content-Disposition: attachment; filename='.$xls_filename);
    header("Pragma: no-cache");
    header("Expires: 0");


    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

  }

    // /*add users by csv file*/
    // public function addUserCSV()
    // {



    //     $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');

    //     if(!empty($_FILES['csv_file']['name']) && in_array($_FILES['csv_file']['type'], $csvMimes))
    //     {

    //         if(is_uploaded_file($_FILES['csv_file']['tmp_name']))
    //         {
    //             $csvFile = fopen($_FILES['csv_file']['tmp_name'],'r');

    //             fgetcsv($csvFile);

    //             while(($line = fgetcsv($csvFile)) !== FALSE)
    //             {
    //                 $id           = $line[0];
    //                 $name         = $line[1];
    //                 $email        = $line[2];
    //                 $password     = $line[3];
    //                 $officeId     = $line[4];
    //                 $departmentId = $line[5];
    //                 $orgId        = $line[6];

    //                 $isUser = DB::table('users')->where('email',$email)->where('status','Active')->first();

    //                 if(empty($isUser))
    //                 {
    //                     $userInsertArray = array(
    //                         'name'           => ucfirst($name),
    //                         'email'          => $email,
    //                         'password'       => bcrypt($password),
    //                         'password2'      => base64_encode($password),
    //                         'status'         => 'Active',
    //                         'officeId'       => $officeId,
    //                         'orgId'          => $orgId,    
    //                         'departmentId'   => $departmentId,
    //                         'created_at'     => date('Y-m-d H:i:s'),                
    //                         'roleId'         => 3
    //                     );

    //                     $insertId = DB::table('users')->insertGetId($userInsertArray);

    //                 } else {

    //                     $updateArray = array(
    //                         'name'           => ucfirst($name),   
    //                         'officeId'       => $officeId,
    //                         'orgId'          => $orgId,    
    //                         'departmentId'   => $departmentId,
    //                         'updated_at'     => date('Y-m-d H:i:s'), 
    //                     );        

    //                     DB::table('users')->where('id',$isUser->id)->update($updateArray);
    //                 }

    //             }

    //             fclose($csvFile);

    //             return redirect()->back()->with('message','Record successfully added.');
    //         }
    //         else
    //         {
    //             return redirect()->back()->with('error','Invalid file.');
    //         }


    //     }
    //     else
    //     {
    //         return redirect()->back()->with('error','Invalid file.');
    //     }

    // }

    // /*Export user list CSV*/
    // public function exportUserList(Request $request)
    // {
    //     $orgId = base64_decode($request->id);

    //     $filename = "user_" . date('Y-m-d')."_".time() . ".csv"; 
    //     $delimiter = ","; 

    //     $f = fopen('php://memory', 'w'); 

    //     $fields = array('ID', 'NAME', 'EMAIL', 'PASSWORD', 'OFFICE ID', 'DEPARTMENT ID','ORGANISATION ID'); 

    //     fputcsv($f, $fields, ',' , chr(0));

    //     $users = DB::table('users')->where('status','Active')->where('orgId',$orgId)->orderBy('id','ASC')->get(); 


    //     foreach($users as $value) 
    //     {
    //         $lineData = array($value->id, $value->name, $value->email ,'', $value->officeId,$value->departmentId, $value->orgId); 

    //         fputcsv($f, $lineData, ',',chr(0));
    //     }       

    //     // Move back to beginning of file 
    //     fseek($f, 0); 

    //     // Set headers to download file rather than displayed 
    //     header('Content-Type: text/csv'); 
    //     header('Content-Disposition: attachment; filename="' . $filename . '";'); 

    //     // Output all remaining data on a file pointer 
    //     fpassthru($f); 
    //     // Exit from file 
    //     exit();
    // }


}
