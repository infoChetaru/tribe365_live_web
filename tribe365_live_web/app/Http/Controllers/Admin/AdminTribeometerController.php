<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;

class AdminTribeometerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $a = app('App\Http\Controllers\Admin\CommonController')->checkUser(Auth::user());

        if (!empty($a)) {
          return redirect('/admin');
        }
    
        $questionTbl = DB::table('tribeometer_questions')
        ->select('tribeometer_questions.id','tribeometer_questions.question','tqc.title')
        ->leftjoin('tribeometer_questions_category AS tqc','tqc.id','=','tribeometer_questions.category_id')
        ->where('status','Active')
        ->paginate(10);
        
        return view('admin/tribeometer/listTribeometerQuestions',compact('questionTbl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $queId    = base64_decode($id);        
        $question = Input::get('question');

        if(trim($question))
        {
           $updateArray['question'] = $question;
       }

       $updateArray['updated_at'] = date('Y-m-d H:i:s');

       DB::table('tribeometer_questions')->where('id',$queId)->update($updateArray);

       return redirect()->back();       
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*diagnostic option list*/
    public function getTribeometerOptionList()
    {
        $triOptTbl = DB::table('tribeometer_question_options')->where('status','Active')->get();

        return view('admin/tribeometer/listTribeometerQuestionOption',compact('triOptTbl'));
    }

    /*update option value*/
    public function upadateTribeometerOption(Request $request)
    {
        $optionId = base64_decode($request->optionId);        
        $option   = Input::get('option');

        if(trim($option))
        {
            $updateArray['option_name'] = $option;
        }

        $updateArray['updated_at'] = date('Y-m-d H:i:s');

        DB::table('tribeometer_question_options')->where('id',$optionId)->update($updateArray);

        return redirect()->back(); 
    }
    
    /*get tribeometer categories values list*/
    public function getTribeometerValuesList()
    {
        $categoryTbl = DB::table('tribeometer_questions_category')->get();

        return view('admin/tribeometer/listTribeometerCategoryValueList',compact('categoryTbl'));
    }
    /*update tribeometer category values*/
    public function updateTribeometerCategoryValues()
    {
        $titleId = base64_decode(Input::get('titleId'));        

        $updateArray = array();
        if(trim(Input::get('title')))
        {
            $updateArray['title'] = Input::get('title');
        }

        DB::table('tribeometer_questions_category')->where('id',$titleId)->update($updateArray);

        return redirect()->back(); 
    }
}
