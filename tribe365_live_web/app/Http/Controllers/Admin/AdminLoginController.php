<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Admin;
use DB;
use Mail;

class AdminLoginController extends Controller
{
    public function index()
    {
        if(!empty(Auth::user()))
        {
            return redirect('admin/admin-dashboard');
        }
        return view('admin/adminLogin');
    }
    
    public function login()
    {
        $email      = Input::get('email');
        $password   = Input::get('password');

        if(Auth::attempt(array('email'=>$email,'password'=>$password,'roleId'=>1,'status'=>'Active')))
        {
            return redirect('admin/admin-dashboard');
        }
        else
        {
            $errors =array('email'=>'These credentials do not match our records.');
            return redirect('/admin')->withErrors($errors);
        }
    }
    
    /*for logout admin user*/
    public function logout(Request $request)
    {
        Auth::logout(); 
        Session::put('improvementPassword', '');
        return redirect('/admin'); 
    }  

    /*get forgot password page*/
    public function getForgotPassword($userRole)
    {
        $userRole = base64_decode($userRole);
        
        return view('forgotPassword',compact('userRole'));
    }

    /*FORGOT PASSWORD FUNCTIONALITY*/

    /*For Reset Password Mail*/
    public function forgotPassword()
    {
        $userRole = Input::get('userRole');

        $email    = Input::get('email')?Input::get('email'):'';

        if (!empty($userRole) && $userRole == 1) {
            $result   = DB::table('users')->where('email',$email)->where('roleId',1)->first();
        }elseif (!empty($userRole) && $userRole == 3) {
            $result   = DB::table('users')->where('email',$email)->where('roleId',3)->first();
        }

        if($result){

          $token = array('token' => str_random(50),'created_at'=>date('Y-m-d :H-i-s'));

          $count   = DB::table('password_resets')->where('email',$email)->first();

          if($count){

            DB::table('password_resets')->where('email',$email)->update($token);

        }else{

            DB::table('password_resets')->insert(['email' => $result->email, 'token' => $token['token']]);
        }         

        $data  = array('name'=>$result->name ,'passwordLink'=>$token['token']);

        Mail::send('mail', $data, function($message) use($email) 
        {
            $message->from('notification@tribe365.co', 'Tribe365');
            $message->to($email);
            $message->subject('Reset Password');
        });

        $errors =array('email'=>'Email sent.');

        return redirect('forgot-password/'.base64_encode($userRole))->withErrors($errors);

    }else{

        $errors =array('email'=>'Email not exists');

        return redirect('forgot-password/'.base64_encode($userRole))->withErrors($errors);
    }
}


/*admin profile detail*/ 
public function getProfile()
{
    $user = Auth::user();

    return view('admin/adminViewProfile',compact('user'));
}

/*update admin profile*/
public function updateProfile(Request $request)
{
    $name    = Input::get('name'); 
    $lastName= Input::get('lastName'); 
    $contact = Input::get('contact');        
    $id      = Auth::user()->id;

    //Delete image of admin from folder
    $image = DB::table('users')->where('id',$id)->first();
    $imgUrl = $image->ImageURL;

    $imagePath = public_path('uploads/user_profile/'.$imgUrl);
    if(File::exists($imagePath)) {
        File::delete($imagePath);
    }

    if($request->hasfile('profileImg'))
    {
        $image = $request->file('profileImg'); 

        $imageName   = 'user_profile_'.time().'.'.$image->getClientOriginalExtension();
        $destination = public_path('uploads/user_profile/');
        $image->move($destination, $imageName);

        $updatetArray['imageUrl'] = $imageName;
    }

    $updatetArray['name']       = $name;
    $updatetArray['lastName']   = $lastName;
    $updatetArray['contact']    = $contact;
    $updatetArray['updated_at'] = date('Y-m-d H:i:s');


    DB::table('users')->where('id',$id)->update($updatetArray); 
    
    return redirect()->back()->with(['errors'=>'Profile updated successfully.']);
    
}

/*edit page of update password*/
public function getEditProfilePassword()
{
    $user = Auth::user();

    return view('admin/adminEditProfilePassword',compact('user'));
}

/*update user password*/
public function updateProfilePassword()
{

    $email      = Input::get('email');        
    $currentpsw = Input::get('currentpsw');
    $newpsw     = Input::get('newpsw');        
    $id         = Auth::user()->id;

    $result = DB::table('users')->where('email',$email)->where('password2',base64_encode($currentpsw))->first();

    if(!$result)
    {
        return redirect()->back()->with(['errors'=>'You have entered wrong current password.']);
    }

    $updatetArray['password']   = str_replace("&nbsp;", '',bcrypt($newpsw));
    $updatetArray['password2']  = str_replace("&nbsp;", '',base64_encode($newpsw));       
    $updatetArray['updated_at'] = date('Y-m-d H:i:s');


    DB::table('users')->where('id',$id)->update($updatetArray); 
    
    return redirect()->back()->with(['message'=>'Password updated successfully.']);
}



/*For Reset Password Mail*/
function improvementChkPass(){

    $improvementPass    = Input::get('improvementPassword');
    //Get password
    $credentials = DB::table('improvements_credentials')->where('id','1')->first();
    if(md5($improvementPass) == $credentials->password_md5){
        Session::put('improvementPassword', 'done');
        return array("status"=>200,"passwordChk"=>1);
    }else{
        return array("status"=>400,"passwordChk"=>2);
    }
    die();
    //$result   = DB::table('users')->where('email',$email)->where('roleId',1)->first();
    //if($result){
    //}else{
    //}         
}

/*For Reset Password Mail*/
public function improvementForgotPassword(){
    //$email       = Input::get('improvementEmail');

    $credentials = DB::table('improvements_credentials')->where('id','1')->first();
    $email       = $credentials->email;

    $credentials = DB::table('improvements_credentials')->where('email',$email)->first();
    if($credentials){
        $token   = array('token' => str_random(50),'created_at'=>date('Y-m-d :H-i-s'));
        $count   = DB::table('password_resets_improvement')->where('email',$email)->first();
        if($count){
            DB::table('password_resets_improvement')->where('email',$email)->update($token);
        }else{
            DB::table('password_resets_improvement')->insert(['email' => $email, 'token' => $token['token']]);
        }         

        //$data  = array('name'=>$result->name ,'passwordLink'=>$token['token']);
        $data  = array('name'=>'Admin' ,'passwordLink'=>$token['token']);

        Mail::send('improvement_mail', $data, function($message) use($email) {
            $message->from('notification@tribe365.co', 'Tribe365');
            //$message->to('ram@chetaru.com');
            $message->to($email);
            $message->subject('Forgot Password');
        });
        return array("status"=>200,"passwordChk"=>1);
    }else{
        return array("status"=>400,"passwordChk"=>2);
    }
}


/*To update password*/
public function resetPasswordImprovementSubmit(){
    $currentPassImp  = Input::get('currentPassImp')?Input::get('currentPassImp'):'';
    $newPassImp      = Input::get('newPassImp')?Input::get('newPassImp'):'';
    
    $credentials = DB::table('improvements_credentials')
                    ->where('password_md5',md5($currentPassImp))->first();
    if($credentials){      
        //Update password
        $updateArray = array('password_md5'=> md5($newPassImp));
        DB::table('improvements_credentials')->where('id','1')->update($updateArray);      
        //make response  
        return array("status"=>200,"passwordChk"=>1);
    }else{
       return array("status"=>400,"passwordChk"=>2);
    }
}

/*Add Admin view*/
public function addAdminView() {
    
    return view('admin/addAdmin');
}

//Add Admin
public function addAdmin(Request $request){
    $postData = Input::all();

    $status = DB::table('users')->where('email',$postData['email'])->where('status','Active')->first();

    if(!empty($status)) {
        return redirect('admin/add-admin-view/')->with('message','Email already exists');
    }
    else {
        $imageName = '';
        if(isset($postData['profileImg']) && !empty($postData['profileImg'])) {
            $image = $postData['profileImg']; 
            $imageName   = 'user_profile_'.time().'.'.$image->getClientOriginalExtension();
            $destination = public_path('uploads/user_profile/');
            $image->move($destination, $imageName);
        }

        $userInsertArray = array(
          'name'           => ucfirst($postData['name']),
          'lastName'       => ucfirst($postData['lastName']),
          'email'          => $postData['email'],
          'imageUrl'       => $imageName,
          'status'         => 'Active',
          'created_at'     => date('Y-m-d H:i:s'),                
          'roleId'         => 1
        );

        $status = DB::table('users')->insertGetId($userInsertArray);
      
        /*send email to new user*/
        $data  = array('name'=>$postData['name'],'lastName'=>$postData['lastName'] ,'passwordLink'=>base64_encode($postData['email']));

        $email = $postData['email'];
        Mail::send('layouts/userPasswordSetEmail', $data, function($message) use($email) 
        {
            $message->from('notification@tribe365.co', 'Tribe365');
            $message->to($email);
            $message->subject('Verify your email address');
        });

        return redirect('admin/admin-list/')->with('message','Record added successfully.');
    }
}

public function adminList() {

    $loggedInUser = Auth::user()->id;

    $users = DB::table('users')    
        ->select('users.id','users.name','users.lastName','users.email') 
        ->where('users.roleId',1)
        ->where('users.id','!=',$loggedInUser)
        ->where('users.status','Active')
        ->paginate(10);     

    return view('admin/adminList',compact('users'));
}

public function deleteAdmin() {
    $userId = Input::get('userId');

    if (!empty($userId)) {      
        DB::table('users')->where('id',$userId)->delete();
        
        return json_encode(array('status'=>200,'message'=>'Admin Successfully Deleted'));
    }else{
        return json_encode(array('status'=>400,'message'=>'No Admin Found.'));
    }
}

}
