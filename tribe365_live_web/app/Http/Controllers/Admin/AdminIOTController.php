<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;
use Validator;
use Hash;
use Config;
use View;
use Illuminate\Pagination\LengthAwarePaginator;
use DateTime;
use DateTimeZone;

class AdminIOTController extends Controller
{

   public function __construct(Request $request)
   {

    $a = app('App\Http\Controllers\Admin\CommonController')->checkUser(Auth::user());

    if (!empty($a)) {
      return redirect('/admin');
    }

    $orgId = base64_decode($request->segment(3));

    $organisations = DB::table('organisations')->select('ImageURL')->where('id',$orgId)->first();

    if(!empty($organisations->ImageURL))
    {
        $org_detail = base64_encode($orgId)."-".$organisations->ImageURL;
        View::share('org_detail', $org_detail);
    }
    else
    {
        $org_detail = "";
        View::share('org_detail', $org_detail);
    }

}


public function show($id)
{
    $orgId = base64_decode($id);

    return view('admin/IOT/listIOTdetail',compact('orgId'));
}

//get iot feedback dashboard


public function getIotDeshboard(Request $request)
{ 
    $a = app('App\Http\Controllers\Admin\CommonController')->checkUser(Auth::user());

    if (!empty($a)) {
        return redirect('/admin');
    }

    $orgId    = base64_decode($request->orgId);
    $officeId = $request->officeId;

    //when user come from organisation
    $isFromOrg = base64_decode($request->segment(3));

    if(!$officeId)
    {
        $officeId = 0;
    }
    if(!$orgId)
    {
        $orgId = 0;
    }

    $organisations = DB::table('organisations')->where('status','Active')->get(); 

    $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get(); 

    //new feedbacks
    $iotNewFeedbackQuery = DB::table('iot_feedbacks')
    ->leftJoin('iot_messages','iot_messages.feedbackId','iot_feedbacks.id')
    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->where('iot_feedbacks.status','Active')
    ->where('iot_feedbacks.status','!=','Completed')
    ->whereNull('iot_messages.feedbackId');

    if($orgId)
    {
        $iotNewFeedbackQuery->where('iot_feedbacks.orgId',$orgId);
    }
    if($officeId)
    {
        $iotNewFeedbackQuery->where('users.officeId',$officeId);
    }

    $iotNewFeedback = $iotNewFeedbackQuery->get();

    //on hold feedback count
    $iotOnHoldFeedbackArrayQuery = DB::table('iot_feedbacks')->select('*','iot_feedbacks.id as feedId')
    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->where('iot_feedbacks.status','Active')->where('iot_feedbacks.status','!=','Completed')

    ->whereIn('iot_feedbacks.id', function($query)
    {
        $query->select('feedbackId')->from('iot_messages');
    });


    if($orgId)
    {
        $iotOnHoldFeedbackArrayQuery->where('users.orgId',$orgId);
    }
    if($officeId)
    {
        $iotOnHoldFeedbackArrayQuery->where('users.officeId',$officeId);
    }

    $iotOnHoldFeedbackArray = $iotOnHoldFeedbackArrayQuery->get();

    $coun = 0;
    foreach ($iotOnHoldFeedbackArray as $key => $value) {


        $iot = DB::table('iot_messages')
        ->where('feedbackId',$value->feedId)
        ->orderBy('id','desc')
        ->first();
        if ($iot->sendFrom != 1) {
            $coun++;
        }
    }
    $iotOnHoldFeedbackArray = $coun;

    //on completed feedbacks
    $iotCompletedFeedbackQuery = DB::table('iot_feedbacks')
    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->where('iot_feedbacks.status','Completed');

    if($orgId)
    {
        $iotCompletedFeedbackQuery->where('iot_feedbacks.orgId',$orgId);
    }
    if($officeId)
    {
        $iotCompletedFeedbackQuery->where('users.officeId',$officeId);
    }

    $iotCompletedFeedback = $iotCompletedFeedbackQuery->get();

    // avaiting response
    $avaitingResponseArray = array();

    $iotFeedTblQuery = DB::table('iot_feedbacks')->select('*','iot_feedbacks.id as feedId')
    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->where('iot_feedbacks.status','Active')->where('iot_feedbacks.status','!=','Completed')

    ->whereIn('iot_feedbacks.id', function($query)
    {
        $query->select('feedbackId')->from('iot_messages');
    });

    if($orgId)
    {
        $iotFeedTblQuery->where('iot_feedbacks.orgId',$orgId);
    }
    if($officeId)
    {
        $iotFeedTblQuery->where('users.officeId',$officeId);
    }

    $iotFeedTbl = $iotFeedTblQuery->get();
   // echo "<pre>";print_r($iotFeedTbl);die();

    $coun1 = 0;
    foreach ($iotFeedTbl as $key => $value) {


        $iot = DB::table('iot_messages')
        ->where('feedbackId',$value->feedId)
        ->orderBy('id','desc')
        ->first();
          //  echo "<pre>";print_r($iot);
        if ($iot->sendFrom == 1) {
            $coun1++;
        }
    }
    $avaitingResponseArray = $coun1;

    //get themes count for dashbaord
    $themeListTbl = DB::table('iot_theme_category')->select('id','title')->where('status','Active')->get();

    $themeList = array();
    foreach($themeListTbl as $value)
    {        
        $themeCountQuery = DB::table('iot_themes')->where('type',$value->id)->where('status','Active');

        if($orgId)
        {
            $themeCountQuery->where('orgId',$orgId);
        }

        $themeCount = $themeCountQuery->count();

        $result['id']         = $value->id;
        $result['title']      = $value->title;
        $result['themeCount'] = $themeCount;

        array_push($themeList, $result);
    }

    $fromOrg = false;
    if($isFromOrg !=0 && !$officeId) 
    {
        $fromOrg = true;
    }

    return view('admin.IOT.iotDashboard',compact('organisations','iotNewFeedback','iotOnHoldFeedbackArray','iotCompletedFeedback','avaitingResponseArray','themeList','orgId','officeId','offices','fromOrg'));
}  


// public function getIotDeshboard(Request $request)
// { 

//     $orgId    = base64_decode($request->orgId);
//     $officeId = $request->officeId;

//     //when user come from organisation
//     $isFromOrg = base64_decode($request->segment(3));

//     if(!$officeId)
//     {
//         $officeId = 0;
//     }
//     if(!$orgId)
//     {
//         $orgId = 0;
//     }

//     $organisations = DB::table('organisations')->where('status','Active')->get(); 

//     $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get(); 

//     //new feedbacks
//     $iotNewFeedbackQuery = DB::table('iot_feedbacks')
//     ->leftJoin('iot_messages','iot_messages.feedbackId','iot_feedbacks.id')
//     ->leftJoin('users','users.id','iot_feedbacks.userId')
//     ->where('iot_feedbacks.status','!=','Completed')
//     ->whereNull('iot_messages.feedbackId');

//     if($orgId)
//     {
//         $iotNewFeedbackQuery->where('iot_feedbacks.orgId',$orgId);
//     }
//     if($officeId)
//     {
//         $iotNewFeedbackQuery->where('users.officeId',$officeId);
//     }

//     $iotNewFeedback = $iotNewFeedbackQuery->get();

//     //on hold feedback count
//     $iotOnHoldFeedbackArrayQuery = DB::table('iot_feedbacks')
//     ->leftJoin('users','users.id','iot_feedbacks.userId')
//     ->where('iot_feedbacks.status','Active')->where('iot_feedbacks.status','!=','Completed')

//     ->whereIn('iot_feedbacks.id', function($query)
//     {
//         $query->select('feedbackId')->from('iot_messages');
//     });


//     if($orgId)
//     {
//         $iotOnHoldFeedbackArrayQuery->where('users.orgId',$orgId);
//     }
//     if($officeId)
//     {
//         $iotOnHoldFeedbackArrayQuery->where('users.officeId',$officeId);
//     }

//     $iotOnHoldFeedbackArray = $iotOnHoldFeedbackArrayQuery->get();


//     //on completed feedbacks
//     $iotCompletedFeedbackQuery = DB::table('iot_feedbacks')
//     ->leftJoin('users','users.id','iot_feedbacks.userId')
//     ->where('iot_feedbacks.status','Completed');

//     if($orgId)
//     {
//         $iotCompletedFeedbackQuery->where('iot_feedbacks.orgId',$orgId);
//     }
//     if($officeId)
//     {
//         $iotCompletedFeedbackQuery->where('users.officeId',$officeId);
//     }

//     $iotCompletedFeedback = $iotCompletedFeedbackQuery->get();

//     // avaiting response
//     $avaitingResponseArray = array();

//     $iotFeedTblQuery = DB::table('iot_feedbacks')
//     ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.image','iot_feedbacks.created_at','users.email','organisations.organisation','offices.office','all_department.department')    
//     ->leftJoin('users','users.id','iot_feedbacks.userId')
//     ->leftJoin('organisations','organisations.id','users.orgId')
//     ->leftJoin('offices','offices.id','users.officeId')
//     ->leftJoin('departments','departments.id','users.departmentId')
//     ->leftJoin('all_department','all_department.id','departments.departmentId')    
//     ->where('iot_feedbacks.status','Active')
//     ->where('iot_feedbacks.status','!=','Completed');

//     if($orgId)
//     {
//         $iotFeedTblQuery->where('iot_feedbacks.orgId',$orgId);
//     }
//     if($officeId)
//     {
//         $iotFeedTblQuery->where('users.officeId',$officeId);
//     }

//     $iotFeedTblQuery->whereIn('iot_feedbacks.id', function($query)
//     {
//         $query->select('feedbackId')
//         ->from('iot_messages')
//         ->leftJoin('users','users.id','iot_messages.sendFrom')
//         ->where('iot_messages.status','Active')
//         ->where('users.roleId',1)
//         ->orderBy('iot_messages.id','DESC');

//     });

//     $avaitingResponseArray = $iotFeedTblQuery->get();

//     // foreach($iotFeedTbl as $fvalue)
//     // {
//     //     $iotMsgQuery = DB::table('iot_messages')
//     //     ->select('iot_messages.id')
//     //     ->leftJoin('users','users.id','iot_messages.sendFrom')
//     //     ->where('users.roleId',1)
//     //     ->where('iot_messages.feedbackId',$fvalue->id)
//     //     ->where('iot_messages.status','Active')
//     //     ->orderBy('iot_messages.id','DESC');

//     //     if($orgId)
//     //     {
//     //         $iotMsgQuery->where('users.orgId',$orgId);
//     //     }
//     //     if($officeId)
//     //     {
//     //         $iotMsgQuery->where('users.officeId',$officeId);
//     //     }

//     //     $iotMsg = $iotMsgQuery->first();

//     //     if(!$iotMsg)
//     //     {
//     //         array_push($avaitingResponseArray, $fvalue->id);
//     //     }
//     // }


//     //get themes count for dashbaord
//     $themeListTbl = DB::table('iot_theme_category')->select('id','title')->where('status','Active')->get();

//     $themeList = array();
//     foreach($themeListTbl as $value)
//     {        
//         $themeCountQuery = DB::table('iot_themes')->where('type',$value->id)->where('status','Active');

//         if($orgId)
//         {
//             $themeCountQuery->where('orgId',$orgId);
//         }

//         $themeCount = $themeCountQuery->count();

//         $result['id']         = $value->id;
//         $result['title']      = $value->title;
//         $result['themeCount'] = $themeCount;

//         array_push($themeList, $result);
//     }

//     $fromOrg = false;
//     if($isFromOrg !=0 && !$officeId) 
//     {
//         $fromOrg = true;
//     }

//     return view('admin.IOT.iotDashboard',compact('organisations','iotNewFeedback','iotOnHoldFeedbackArray','iotCompletedFeedback','avaitingResponseArray','themeList','orgId','officeId','offices','fromOrg'));
// }  




/*get feedback list*/
public function getFeedbackList(Request $request)
{
    $status   = base64_decode($request->status);
    $orgId    = base64_decode($request->orgId);
    $officeId = base64_decode($request->officeId);

    $feedbackListQuery = DB::table('iot_feedbacks')
        ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.image','iot_feedbacks.created_at','iot_feedbacks.orgId','users.email','organisations.organisation','offices.office','all_department.department','iot_feedbacks.SWOT','iot_feedbacks.themeId','iot_feedbacks.feedbackSummary','iot_feedbacks.initialRiskScore','iot_feedbacks.actionTaken','iot_feedbacks.feedbackStatus','iot_feedbacks.userId','users.name','users.lastName')
        ->leftJoin('users','users.id','iot_feedbacks.userId')
        ->leftJoin('organisations','organisations.id','users.orgId')
        ->leftJoin('offices','offices.id','users.officeId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId');    
    if($orgId) {
        $feedbackListQuery->where('users.orgId',$orgId);
    }
    if($officeId) {
        $feedbackListQuery->where('users.officeId',$officeId);
    }
    if($status=='new') {
        $feedbackListQuery->where('iot_feedbacks.status','!=','Completed')
            ->whereNull('iot_messages.feedbackId')
            ->leftJoin('iot_messages','iot_messages.feedbackId','iot_feedbacks.id')
            ->orderBy('iot_feedbacks.id','DESC');
        $feedbackListTbl =  $feedbackListQuery->paginate(10);
    }
    elseif($status=='on_hold') {
        $feedbackListQuery->where('iot_feedbacks.status','!=','Completed');
        $feedbackListQuery->where('iot_feedbacks.status','Active');
        $feedbackListQuery->whereIn('iot_feedbacks.id', function($query)
        {
            $query->select('feedbackId')
            ->from('iot_messages');
        });
        $feedbackListQuery->orderBy('iot_feedbacks.id','DESC');
        $feedbackOnholdListTbl = $feedbackListQuery->get();
        $iotData = array();

        foreach ($feedbackOnholdListTbl as $key => $value) {
            $iot = DB::table('iot_feedbacks')
                ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.image','iot_feedbacks.created_at','iot_feedbacks.orgId','users.email','organisations.organisation','offices.office','all_department.department','iot_messages.sendTo','iot_messages.sendFrom','iot_feedbacks.SWOT','iot_feedbacks.themeId','iot_feedbacks.feedbackSummary','iot_feedbacks.initialRiskScore','iot_feedbacks.actionTaken','iot_feedbacks.feedbackStatus','iot_feedbacks.userId','users.name','users.lastName')
                ->leftJoin('users','users.id','iot_feedbacks.userId')
                ->leftJoin('iot_messages','iot_feedbacks.id','iot_messages.feedbackId')
                ->leftJoin('organisations','organisations.id','users.orgId')
                ->leftJoin('offices','offices.id','users.officeId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId');
            if($orgId) {
                $iot->where('users.orgId',$orgId);
            }
            if($officeId) {
                $iot->where('users.officeId',$officeId);
            }
            $iot->where('iot_feedbacks.id',$value->id)
                ->where('iot_feedbacks.status','Active')
                ->where('iot_feedbacks.status','!=','Completed')
                ->orderBy('iot_messages.id','DESC');
            $iotMsg = $iot->first();
            if($iotMsg) {
                if($iotMsg->sendFrom != 1) {
                    array_push($iotData, $iotMsg);
                }            
            }
        }

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $perPage = 10;

        $currentItems = array_slice($iotData, $perPage * ($currentPage - 1), $perPage);

        $paginator = new LengthAwarePaginator($currentItems, count($iotData), $perPage, $currentPage);
        $feedbackListTbl = $paginator;
    }
    elseif($status=='completed'){
        $feedbackListQuery->where('iot_feedbacks.status','Completed')
            ->orderBy('iot_feedbacks.id','DESC');
        $feedbackListTbl = $feedbackListQuery->paginate(10);
    }
    else if($status=='awaiting') {
        $feedbackListQuery->where('iot_feedbacks.status','!=','Completed');
        $feedbackListQuery->whereIn('iot_feedbacks.id', function($query) {
            $query->select('feedbackId')
            ->from('iot_messages');
        });
       $feedbackListQuery->orderBy('iot_feedbacks.id','DESC');
       $feedbackAwaitingListTbl = $feedbackListQuery->get();
       $iotData = array();

       foreach ($feedbackAwaitingListTbl as $key => $value) {
            $iot = DB::table('iot_feedbacks')
                ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.image','iot_feedbacks.created_at','iot_feedbacks.orgId','users.email','organisations.organisation','offices.office','all_department.department','iot_messages.sendTo','iot_messages.sendFrom','iot_feedbacks.SWOT','iot_feedbacks.themeId','iot_feedbacks.feedbackSummary','iot_feedbacks.initialRiskScore','iot_feedbacks.actionTaken','iot_feedbacks.feedbackStatus','iot_feedbacks.userId','users.name','users.lastName')
                ->leftJoin('users','users.id','iot_feedbacks.userId')
                ->leftJoin('iot_messages','iot_feedbacks.id','iot_messages.feedbackId')
                ->leftJoin('organisations','organisations.id','users.orgId')
                ->leftJoin('offices','offices.id','users.officeId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId');

            if($orgId) {
                $iot->where('users.orgId',$orgId);
            }
            if($officeId) {
                $iot->where('users.officeId',$officeId);
            }
            $iot->where('iot_feedbacks.id',$value->id)
                ->where('iot_feedbacks.status','Active')
                ->where('iot_feedbacks.status','!=','Completed')
                ->orderBy('iot_messages.id','DESC');
            $iotMsg = $iot->first();
            if($iotMsg) {
                if($iotMsg->sendFrom == 1) {
                    array_push($iotData, $iotMsg);
                }            
            }
        }

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $perPage = 10;

        $currentItems = array_slice($iotData, $perPage * ($currentPage - 1), $perPage);

        $paginator = new LengthAwarePaginator($currentItems, count($iotData), $perPage, $currentPage);

        $feedbackListTbl = $paginator;
    }
    else {      
        $feedbackListTbl = $feedbackListQuery->paginate(10);
    }

    $objectArr = array();
    foreach ($feedbackListTbl as  $rValue)
    {
        $object    = new Controller();

        $object->id           = $rValue->id;
        $object->userName     = $rValue->name." ".$rValue->lastName;
        $object->message      = $rValue->message;
        $object->created_at   = $rValue->created_at;
        $object->organisation = $rValue->organisation;
        $object->office       = $rValue->office;
        $object->department   = $rValue->department;
        $object->orgId        = $rValue->orgId;
        $object->SWOT        = $rValue->SWOT;
        $object->themeId        = $rValue->themeId;
        $object->feedbackSummary        = $rValue->feedbackSummary;
        $object->initialRiskScore        = $rValue->initialRiskScore;
        $object->actionTaken        = $rValue->actionTaken;
        $object->feedbackStatus        = $rValue->feedbackStatus;
        $object->userId        = $rValue->userId;


        $themeListTbl = DB::table('iot_allocated_themes')
        ->select('iot_themes.id')
        ->leftJoin('iot_themes','iot_themes.id','iot_allocated_themes.themeId')
        ->where('feedbackId',$rValue->id)->where('iot_allocated_themes.status','Active')
        ->get();

        $themeListArr = array();
        foreach ($themeListTbl as $tvalue) {
            array_push($themeListArr, $tvalue->id);
        }

        $object->themes = $themeListArr;

        $actionsQuery = DB::table('actions')->where('status','Active');
        if (!empty($rValue->orgId)) {
            $actionsQuery->where('orgId',$rValue->orgId);
        }
        $actions = $actionsQuery->get();
        $object->actions = $actions;

        array_push($objectArr, $object);
    }

    $feedbackList = $objectArr;

    rsort($feedbackList);

    $themeList = DB::table('iot_themes')->where('status','Active')->where('orgId',$orgId)->get();

    $themeCateList = DB::table('iot_theme_category')->where('status','Active')->get();

    $initialRiskScore = DB::table('iot_risk_priority')->where('status','Active')->get();

    return view('admin.IOT.iotFeedbackListView',compact('iotFeedTbl','feedbackListTbl','feedbackList','themeList','themeCateList','initialRiskScore'/*,'actions'*/));
}

/*get feedback list from theme list*/
public function getFeedbackListFromThemeList(Request $request)
{
    $orgId    = base64_decode($request->orgId);
    $themeId = base64_decode($request->themeId);

    $feedbackListQuery = DB::table('iot_feedbacks')
        ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.image','iot_feedbacks.created_at','iot_feedbacks.orgId','users.email','organisations.organisation','offices.office','all_department.department','iot_feedbacks.SWOT','iot_feedbacks.themeId','iot_feedbacks.feedbackSummary','iot_feedbacks.initialRiskScore','iot_feedbacks.actionTaken','iot_feedbacks.feedbackStatus','iot_feedbacks.userId','users.name','users.lastName')
        ->leftJoin('iot_allocated_themes','iot_allocated_themes.feedbackId','iot_feedbacks.id')
        ->leftJoin('users','users.id','iot_feedbacks.userId')
        ->leftJoin('organisations','organisations.id','users.orgId')
        ->leftJoin('offices','offices.id','users.officeId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId');    
    if(!empty($orgId)) {
        $feedbackListQuery->where('users.orgId',$orgId);
    }
    if(!empty($themeId)) {
        $feedbackListQuery->where('iot_allocated_themes.themeId',$themeId);
    }
    $feedbackListQuery->where('iot_feedbacks.status','!=','Inactive')
                    ->orderBy('iot_feedbacks.id','DESC');
    $feedbackListTbl =  $feedbackListQuery->paginate(10);
    

    $objectArr = array();
    foreach ($feedbackListTbl as  $rValue)
    {
        $object    = new Controller();

        $object->id           = $rValue->id;
        $object->userName     = $rValue->name." ".$rValue->lastName;
        $object->message      = $rValue->message;
        $object->created_at   = $rValue->created_at;
        $object->organisation = $rValue->organisation;
        $object->office       = $rValue->office;
        $object->department   = $rValue->department;
        $object->orgId        = $rValue->orgId;
        $object->SWOT        = $rValue->SWOT;
        $object->themeId        = $rValue->themeId;
        $object->feedbackSummary        = $rValue->feedbackSummary;
        $object->initialRiskScore        = $rValue->initialRiskScore;
        $object->actionTaken        = $rValue->actionTaken;
        $object->feedbackStatus        = $rValue->feedbackStatus;
        $object->userId        = $rValue->userId;


        $themeListTbl = DB::table('iot_allocated_themes')
        ->select('iot_themes.id')
        ->leftJoin('iot_themes','iot_themes.id','iot_allocated_themes.themeId')
        ->where('feedbackId',$rValue->id)->where('iot_allocated_themes.status','Active')
        ->get();

        $themeListArr = array();
        foreach ($themeListTbl as $tvalue) {
            array_push($themeListArr, $tvalue->id);
        }

        $object->themes = $themeListArr;

        $actionsQuery = DB::table('actions')->where('status','Active');
        if (!empty($rValue->orgId)) {
            $actionsQuery->where('orgId',$rValue->orgId);
        }
        $actions = $actionsQuery->get();
        $object->actions = $actions;

        array_push($objectArr, $object);
    }

    $feedbackList = $objectArr;

    rsort($feedbackList);

    $themeList = DB::table('iot_themes')->where('status','Active')->where('orgId',$orgId)->get();

    $themeCateList = DB::table('iot_theme_category')->where('status','Active')->get();

    $initialRiskScore = DB::table('iot_risk_priority')->where('status','Active')->get();

    return view('admin.IOT.iotFeedbackListView',compact('iotFeedTbl','feedbackListTbl','feedbackList','themeList','themeCateList','initialRiskScore'/*,'actions'*/));
}

/*get theme from SWOT */
public function getThemesFromSWOT()
{
    $SWOTId = Input::get('SWOTId');
    $orgId = Input::get('orgId');
    
    $themeListTbl = array();
    if (!empty($SWOTId) && !empty($orgId)) {
        $themeListTblQuery = DB::table('iot_themes')
            ->select('iot_themes.id','iot_themes.title')
            ->where('iot_themes.status','Active');
        if(!empty($SWOTId)) {
            $themeListTblQuery->where('iot_themes.type',$SWOTId);
        }
        if(!empty($orgId)){
            $themeListTblQuery->where('iot_themes.orgId',$orgId);
        }
        $themeListTbl = $themeListTblQuery->get();
    }
    return json_encode(array('status'=>'success','themeListTbl'=>$themeListTbl));
}

public function updateImprovements(Request $request)
{   
    $SWOTCate           = $request->SWOTCate;
    $themesId           = $request->themesId;
    $initialText        = $request->initialText;
    $initialRiskScore   = $request->initialRiskScore;
    $actionTaken        = $request->actionTaken;
    $status             = $request->status;
    $feedbackId         = $request->feedbackId;
    $orgId              = $request->orgId;

    $swotCat = '';
    if (!empty($SWOTCate)) {
        $swotCat = trim(implode(',',$SWOTCate));
    }
    $themesIds = '';
    if (!empty($themesId)) {
        $themesIds = trim(implode(',',$themesId));
    }
    $actionIds = '';
    if (!empty($actionTaken)) {
        $actionIds = trim(implode(',',$actionTaken));
    }

    DB::table('iot_allocated_themes')->where('feedbackId',$feedbackId)->delete();
    if (!empty($themesId)) {
        foreach ($themesId as $themeVal) {
            DB::table('iot_allocated_themes')->insertGetId([
                'feedbackId' => $feedbackId,
                'themeId'    => $themeVal,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
    }

    $allActions = DB::table('actions')->select('id','feedbackId')->where('orgId',$orgId)->where('status','Active')->get();

    foreach ($allActions as $actionVals) {
        $feedbackids = explode(',', $actionVals->feedbackId);
        $mainFeedbackIds = '';
        if(!empty($feedbackids) && in_array($feedbackId, $feedbackids)){
            $mainFeedbackIds = array_diff($feedbackids, array($feedbackId));
            DB::table('actions')->where('id',$actionVals->id)->update([
                'feedbackId'       => implode(',', $mainFeedbackIds),
                // 'themeId'       => json_encode($mainThemeIds),
                'updated_at'    => date('Y-m-d H:i:s')
            ]);
        }
    }

    DB::table('iot_feedbacks')->where('id',$feedbackId)->update([
        'SWOt'              => $swotCat,
        'themeId'           => $themesIds,
        'feedbackSummary'   => $initialText,
        'initialRiskScore'  => $initialRiskScore,
        'actionTaken'       => $actionIds,
        'feedbackStatus'    => $status,
        'updated_at'        => date('Y-m-d H:i:s')
    ]);

    foreach ($actionTaken as $linkedActionVal) {

        $actions = DB::table('actions')->where('id',$linkedActionVal)->first();
        
        $feedbackArr = [];
        $actionFeedbacks = [];        
        if (!empty($actions)) {
            if (!empty($actions->feedbackId)) { //If feedback exist

                $actionFeedbacks = explode(',', $actions->feedbackId);

                if (in_array($feedbackId, $actionFeedbacks)) {
                    continue;
                }else{
                    array_push($actionFeedbacks, $feedbackId);
                }

                DB::table('actions')->where('id',$linkedActionVal)->update([
                    'feedbackId' => implode(',', $actionFeedbacks),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }else{ //No feedbacks

                array_push($feedbackArr, $feedbackId);

                DB::table('actions')->where('id',$linkedActionVal)->update([
                    'feedbackId' => implode(',', $feedbackArr),
                    'updated_at' => date('Y-m-d H:i:s')
                ]); 
            }
        }     
    }

    return redirect()->back();
}  



/*public function getFeedbackList(Request $request)
{

    $status   = base64_decode($request->status);
    $orgId    = base64_decode($request->orgId);
    $officeId = base64_decode($request->officeId);

    $feedbackListQuery = DB::table('iot_feedbacks')
    ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.image','iot_feedbacks.created_at','iot_feedbacks.orgId','users.email','organisations.organisation','offices.office','all_department.department')

    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->leftJoin('organisations','organisations.id','users.orgId')
    ->leftJoin('offices','offices.id','users.officeId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId');    

    $iotFeedTbl = '';
    if($orgId)
    {
        $feedbackListQuery->where('users.orgId',$orgId);
    }
    if($officeId)
    {
        $feedbackListQuery->where('users.officeId',$officeId);
    }

    if($status=='new')
    {
        $feedbackListQuery->where('iot_feedbacks.status','!=','Completed')
        ->whereNull('iot_messages.feedbackId')
        ->leftJoin('iot_messages','iot_messages.feedbackId','iot_feedbacks.id');

        $feedbackListTbl =  $feedbackListQuery->paginate(6);
    }
    elseif($status=='on_hold')
    {

        $feedbackListQuery->where('iot_feedbacks.status','!=','Completed');

        $feedbackListQuery->whereIn('iot_feedbacks.id', function($query)
        {
            $query->select('feedbackId')
            ->from('iot_messages');
        });

        $feedbackListTbl = $feedbackListQuery->paginate(6);

    }
    elseif($status=='completed')
    {

        $feedbackListQuery->where('iot_feedbacks.status','Completed');
        $feedbackListTbl = $feedbackListQuery->paginate(6);
    }
    else if($status=='awaiting')
    {

        // avaiting response
        $avaitingResponseArray = array();

        $feedbackListQuery->where('iot_feedbacks.status','Active');    

        if($orgId)
        {
            $feedbackListQuery->where('users.orgId',$orgId);
        }
        if($officeId)
        {
            $feedbackListQuery->where('users.officeId',$officeId);
        }  

        $feedbackListQuery->whereIn('iot_feedbacks.id', function($query)
        {
            $query->select('feedbackId')
            ->from('iot_messages')
            ->leftJoin('users','users.id','iot_messages.sendFrom')
            ->where('iot_messages.status','Active')
            ->where('users.roleId',1)
            ->orderBy('iot_messages.id','DESC');
            
        });

        $feedbackListTbl = $feedbackListQuery->paginate(6);

        // print_r($iotFeedTbl);
        // dd();

        // foreach($iotFeedTbl as $fvalue)
        // {
        //     $iotMsgQuery = DB::table('iot_messages')->select('iot_messages.id')
        //     ->leftJoin('users','users.id','iot_messages.sendFrom')
        //     ->where('users.roleId',1)
        //     ->where('iot_messages.feedbackId',$fvalue->id)
        //     ->where('iot_messages.status','Active')
        //     ->orderBy('iot_messages.id','DESC');

        //     if($orgId)
        //     {
        //         $iotMsgQuery->where('users.orgId',$orgId);
        //     }
        //     if($officeId)
        //     {
        //         $iotMsgQuery->where('users.officeId',$officeId);
        //     }

        //     $iotMsg = $iotMsgQuery->first();

        //     if(!$iotMsg)
        //     {
        //         array_push($avaitingResponseArray, $fvalue);
        //     }
        // }

        // $feedbackListTbl = $avaitingResponseArray;

    }
    else
    {      
        $feedbackListTbl = $feedbackListQuery->paginate(6);
    }


    $objectArr = array();
    foreach ($feedbackListTbl as  $rValue)
    {
        $object    = new Controller();

        $object->id           = $rValue->id;
        $object->message      = $rValue->message;
        $object->image        = $rValue->image;
        $object->created_at   = $rValue->created_at;
        $object->email        = $rValue->email;
        $object->organisation = $rValue->organisation;
        $object->office       = $rValue->office;
        $object->department   = $rValue->department;
        $object->orgId        = $rValue->orgId;
        

        $themeListTbl = DB::table('iot_allocated_themes')
        ->select('iot_themes.title')
        ->leftJoin('iot_themes','iot_themes.id','iot_allocated_themes.themeId')
        ->where('feedbackId',$rValue->id)->where('iot_allocated_themes.status','Active')
        ->get();

        $themeListArr = array();
        foreach ($themeListTbl as $tvalue)
        {
            array_push($themeListArr, $tvalue->title);
        }

        $object->themes = $themeListArr;

        array_push($objectArr, $object);
    }

    $feedbackList = $objectArr;

    $themeList = DB::table('iot_themes')->where('status','Active')->get();

    return view('admin.IOT.iotFeedbackList',compact('iotFeedTbl','feedbackListTbl','feedbackList','themeList'));

}  */


/*do complete feedback*/
public function completeFeedback(Request $request)
{
    $feedbackId = base64_decode($request->id);

    DB::table('iot_feedbacks')->where('id', $feedbackId)->update(['status'=>'Completed']);

    return redirect()->back();
}


/*get create theme page*/
public function createTheme()
{
    $organisations = DB::table('organisations')->where('status','Active')->get();

    $types = DB::table('iot_theme_category')->where('status','Active')->get();

    return view('admin.IOT.iotAddTheme',compact('organisations','types'));
}

/*get theme list*/
public function getThemeList(Request $request)
{
    $timezone = date_default_timezone_get();

    $id = base64_decode($request->id);
    $orgId = base64_decode($request->orgId);

    $themeListTblQuery = DB::table('iot_themes')
    ->select('iot_themes.id','iot_themes.dateOpened','iot_themes.title','iot_themes.description','iot_themes.submission','iot_themes.initialLikelihood','iot_themes.initialConsequence','iot_themes.currentLikelihood','iot_themes.currentConsequence','iot_themes.themeStatus','iot_themes.linkedAction','organisations.organisation','iot_theme_category.title AS categoryTitle','iot_themes.orgId','iot_theme_category.id as themeCateId','iot_themes.comment','iot_themes.updated_at')

    ->leftJoin('organisations','organisations.id','iot_themes.orgId')
    ->leftJoin('iot_theme_category','iot_theme_category.id','iot_themes.type')    
    ->where('iot_themes.status','Active');

    if($id) 
    {
        $themeListTblQuery->where('iot_themes.type',$id);
    }
    if($orgId)
    {
        $themeListTblQuery->where('iot_themes.orgId',$orgId);
    }

    $themeListTblpagi = $themeListTblQuery->orderBy('id','DESC')->paginate(6);

    $objectArr = array();
    foreach ($themeListTblpagi as  $rValue)
    {
        //date_default_timezone_set($timezone);

        // $now = new DateTime($rValue->updated_at);
        // $now->setTimezone(new DateTimeZone('Asia/Kolkata'));
        // echo "<pre>". $now->format('d/m/Y H:i');

        // echo "<pre>";print_r(date_default_timezone_get());
        // $dateTime = explode(" ", $rValue->updated_at);

        // $date = $dateTime[0];
        // $time = $dateTime[1];

        //$the_date = date('d/m/Y H:i',strtotime($rValue->updated_at));


       //$the_date = strtotime("2020-02-28 05:44:36");
        // echo(date_default_timezone_get() . "<br />");
        // echo(date("Y-d-mTG:i:sz",$the_date) . "<br />");
        // echo(date_default_timezone_set(date_default_timezone_get()) . "<br />");
        // echo(date("Y-d-mTG:i:sz", $the_date) . "<br />");


       $dt = new DateTime($rValue->updated_at, new DateTimeZone('UTC'));
       $dt->setTimezone(new DateTimeZone(date_default_timezone_get()));
       $dt->format('Y-m-d H:i:s');
// die();
        // echo "<pre>";print_r($dt);

        $object                     = new Controller();

        $object->id                 = $rValue->id;
        $object->dateOpened         = $rValue->dateOpened;
        $object->title              = $rValue->title;
        $object->description        = $rValue->description;
        $object->initialLikelihood  = $rValue->initialLikelihood;        
        $object->initialConsequence = $rValue->initialConsequence;
        $object->currentLikelihood  = $rValue->currentLikelihood;
        $object->currentConsequence = $rValue->currentConsequence;
        $object->themeStatus        = $rValue->themeStatus;
        $object->linkedAction       = $rValue->linkedAction;
        $object->organisation       = $rValue->organisation;
        $object->categoryTitle      = $rValue->categoryTitle;
        $object->orgId              = $rValue->orgId;
        $object->themeCateId        = $rValue->themeCateId;
        $object->comment            = $rValue->comment;
        $object->updated_at         = $dt->format('d/m/Y H:i');

        $object->initialRiskRating = $object->initialLikelihood*$object->initialConsequence;

        if ($object->initialRiskRating>=1 && $object->initialRiskRating<=4) {
            $object->initialRiskRating = "Low";
        }elseif ($object->initialRiskRating>=5 && $object->initialRiskRating<=12) {
            $object->initialRiskRating = "Medium";
        }elseif ($object->initialRiskRating>=13 && $object->initialRiskRating<=25) {
            $object->initialRiskRating = "High";
        }else{
            $object->initialRiskRating = "-";
        }

        $object->currentRiskRating = $object->currentLikelihood*$object->currentConsequence;

        if ($object->currentRiskRating>=1 && $object->currentRiskRating<=4) {
            $object->currentRiskRating = "Low";
        }elseif ($object->currentRiskRating>=5 && $object->currentRiskRating<=12) {
            $object->currentRiskRating = "Medium";
        }elseif ($object->currentRiskRating>=13 && $object->currentRiskRating<=25) {
            $object->currentRiskRating = "High";
        }





        // $actionsQuery = DB::table('iot_allocated_themes')->where('status','Active');
        // if (!empty($rValue->orgId)) {
        //     $actionsQuery->where('orgId',$rValue->orgId);
        // }
        // $action = $actionsQuery->get();
        
        // $object->actions  = $action;

        $feedbackList = DB::table('iot_feedbacks')
            ->select('id','message')
            ->where('status','!=','Inactive')
            ->where('orgId',$rValue->orgId)->get();
        $object->allSubmission = $feedbackList;


        $allocatedThemeListTblQuery = DB::table('iot_allocated_themes')
            ->select('iot_feedbacks.id as feedbackId','iot_feedbacks.message')
            ->leftJoin('iot_feedbacks','iot_feedbacks.id','iot_allocated_themes.feedbackId')
            ->where('iot_allocated_themes.themeId',$rValue->id)
            ->where('iot_feedbacks.status','!=','Inactive')
            ->where('iot_allocated_themes.status','Active');
        $allocatedThemeListTbl = $allocatedThemeListTblQuery->get();
        // $object->submission = $allocatedThemeListTbl;

        $feedbackIdArr = array();
        foreach ($allocatedThemeListTbl as $tvalue) {
            array_push($feedbackIdArr, $tvalue->feedbackId);
        }
        $object->submission = $feedbackIdArr;

        // $feedbackIdArr = array();
        // foreach ($themeListTbl as $tvalue)
        // {
        //     array_push($feedbackIdArr, $tvalue->feedbackId);
        // }


        //Responsible Person
        $responsiblePersonList = DB::table('users')->select('id','name','lastName')->where('orgId',$rValue->orgId)->where('status','Active')->get();

        $object->responsiblePersonList = $responsiblePersonList;

        //action list
        // $allocatedActionArr = json_decode($rValue->linkedAction);

      //   $actionTitleArr = array();
      //   if($allocatedActionArr)
      //   {
      //       foreach ($allocatedActionArr as $actionId)
      //       {
      //           $action = DB::table('actions')->select('id')->where('status','Active')->where('id',$actionId)->first();

      //           if($action)
      //           {
      //             array_push($actionTitleArr, $action->id);
      //         }

      //     }

      // }

      // $object->actions  = implode(', ', $actionTitleArr);

        $actionsQuery = DB::table('actions')->where('status','Active');
        if (!empty($rValue->orgId)) {
            $actionsQuery->where('orgId',$rValue->orgId);
        }
        $action = $actionsQuery->get();
        $object->actions  = $action;

        $adminName = DB::table('users')->select('name','lastName')->first();
        $object->adminName  = $adminName->name;

        $themeNotes = DB::table('iot_theme_notes')
            ->select('iot_theme_notes.id','iot_theme_notes.message','iot_theme_notes.themeId','iot_theme_notes.created_at','users.name','users.lastName')
            ->leftJoin('users','users.id','iot_theme_notes.userId')
            ->where('themeId',$rValue->id)->orderBy('created_at','DESC')->first();
        $object->themeNote = $themeNotes;

        $allThemeNotes = DB::table('iot_theme_notes')
            ->select('iot_theme_notes.id','iot_theme_notes.message','iot_theme_notes.themeId','iot_theme_notes.created_at','users.name','users.lastName')
            ->leftJoin('users','users.id','iot_theme_notes.userId')
            ->where('themeId',$rValue->id)->orderBy('created_at','DESC')->get();
        $object->allThemeNote = $allThemeNotes;

        array_push($objectArr, $object);
    }


// die();
    $themeListTbl = $objectArr;
    // echo "<pre>";print_r($themeListTbl);die();

    $themeCategory = DB::table('iot_theme_category')->select('id','title')->where('status','Active')->get();

    $organisations = DB::table('organisations')->select('id','organisation')->where('status','Active')->get();
  
    return view('admin.IOT.iotThemeList',compact('themeListTbl','themeListTblpagi','themeCategory','organisations'));
} 

/*get theme list from feedback list*/
public function getThemeListFromFeedbackList(Request $request)
{
    $timezone = date_default_timezone_get();

    $feedbackId = base64_decode($request->feedbackId);
    $orgId = base64_decode($request->orgId);

    $themeListTblQuery = DB::table('iot_themes')
    ->select('iot_themes.id','iot_themes.dateOpened','iot_themes.title','iot_themes.description','iot_themes.submission','iot_themes.initialLikelihood','iot_themes.initialConsequence','iot_themes.currentLikelihood','iot_themes.currentConsequence','iot_themes.themeStatus','iot_themes.linkedAction','organisations.organisation','iot_theme_category.title AS categoryTitle','iot_themes.orgId','iot_theme_category.id as themeCateId','iot_themes.comment','iot_themes.updated_at')

    ->leftJoin('iot_allocated_themes','iot_allocated_themes.themeId','iot_themes.id')
    ->leftJoin('organisations','organisations.id','iot_themes.orgId')
    ->leftJoin('iot_theme_category','iot_theme_category.id','iot_themes.type')    
    ->where('iot_themes.status','Active');

    if(!empty($feedbackId)) 
    {
        $themeListTblQuery->where('iot_allocated_themes.feedbackId',$feedbackId); 
        // $actionTblQuery->whereRaw("find_in_set(".$feedbackId.",iot_allocated_themes.feedbackId)"); 
        // $themeListTblQuery->where('iot_themes.type',$id);
    }
    if(!empty($orgId))
    {
        $themeListTblQuery->where('iot_themes.orgId',$orgId);
    }

    $themeListTblpagi = $themeListTblQuery->orderBy('id','DESC')->paginate(6);

    $objectArr = array();
    foreach ($themeListTblpagi as  $rValue)
    {
       $dt = new DateTime($rValue->updated_at, new DateTimeZone('UTC'));
       $dt->setTimezone(new DateTimeZone(date_default_timezone_get()));
       $dt->format('Y-m-d H:i:s');

        $object                     = new Controller();

        $object->id                 = $rValue->id;
        $object->dateOpened         = $rValue->dateOpened;
        $object->title              = $rValue->title;
        $object->description        = $rValue->description;
        $object->initialLikelihood  = $rValue->initialLikelihood;        
        $object->initialConsequence = $rValue->initialConsequence;
        $object->currentLikelihood  = $rValue->currentLikelihood;
        $object->currentConsequence = $rValue->currentConsequence;
        $object->themeStatus        = $rValue->themeStatus;
        $object->linkedAction       = $rValue->linkedAction;
        $object->organisation       = $rValue->organisation;
        $object->categoryTitle      = $rValue->categoryTitle;
        $object->orgId              = $rValue->orgId;
        $object->themeCateId        = $rValue->themeCateId;
        $object->comment            = $rValue->comment;
        $object->updated_at         = $dt->format('d/m/Y H:i');

        $object->initialRiskRating = $object->initialLikelihood*$object->initialConsequence;

        if ($object->initialRiskRating>=1 && $object->initialRiskRating<=4) {
            $object->initialRiskRating = "Low";
        }elseif ($object->initialRiskRating>=5 && $object->initialRiskRating<=12) {
            $object->initialRiskRating = "Medium";
        }elseif ($object->initialRiskRating>=13 && $object->initialRiskRating<=25) {
            $object->initialRiskRating = "High";
        }else{
            $object->initialRiskRating = "-";
        }

        $object->currentRiskRating = $object->currentLikelihood*$object->currentConsequence;

        if ($object->currentRiskRating>=1 && $object->currentRiskRating<=4) {
            $object->currentRiskRating = "Low";
        }elseif ($object->currentRiskRating>=5 && $object->currentRiskRating<=12) {
            $object->currentRiskRating = "Medium";
        }elseif ($object->currentRiskRating>=13 && $object->currentRiskRating<=25) {
            $object->currentRiskRating = "High";
        }





        // $actionsQuery = DB::table('iot_allocated_themes')->where('status','Active');
        // if (!empty($rValue->orgId)) {
        //     $actionsQuery->where('orgId',$rValue->orgId);
        // }
        // $action = $actionsQuery->get();
        
        // $object->actions  = $action;

        $feedbackList = DB::table('iot_feedbacks')
            ->select('id','message')
            ->where('status','!=','Inactive')
            ->where('orgId',$rValue->orgId)->get();
        $object->allSubmission = $feedbackList;


        $allocatedThemeListTblQuery = DB::table('iot_allocated_themes')
            ->select('iot_feedbacks.id as feedbackId','iot_feedbacks.message')
            ->leftJoin('iot_feedbacks','iot_feedbacks.id','iot_allocated_themes.feedbackId')
            ->where('iot_allocated_themes.themeId',$rValue->id)
            ->where('iot_feedbacks.status','!=','Inactive')
            ->where('iot_allocated_themes.status','Active');
        $allocatedThemeListTbl = $allocatedThemeListTblQuery->get();
        // $object->submission = $allocatedThemeListTbl;

        $feedbackIdArr = array();
        foreach ($allocatedThemeListTbl as $tvalue) {
            array_push($feedbackIdArr, $tvalue->feedbackId);
        }
        $object->submission = $feedbackIdArr;

        // $feedbackIdArr = array();
        // foreach ($themeListTbl as $tvalue)
        // {
        //     array_push($feedbackIdArr, $tvalue->feedbackId);
        // }


        //Responsible Person
        $responsiblePersonList = DB::table('users')->select('id','name','lastName')->where('orgId',$rValue->orgId)->where('status','Active')->get();

        $object->responsiblePersonList = $responsiblePersonList;

        //action list
        // $allocatedActionArr = json_decode($rValue->linkedAction);

      //   $actionTitleArr = array();
      //   if($allocatedActionArr)
      //   {
      //       foreach ($allocatedActionArr as $actionId)
      //       {
      //           $action = DB::table('actions')->select('id')->where('status','Active')->where('id',$actionId)->first();

      //           if($action)
      //           {
      //             array_push($actionTitleArr, $action->id);
      //         }

      //     }

      // }

      // $object->actions  = implode(', ', $actionTitleArr);

        $actionsQuery = DB::table('actions')->where('status','Active');
        if (!empty($rValue->orgId)) {
            $actionsQuery->where('orgId',$rValue->orgId);
        }
        $action = $actionsQuery->get();
        $object->actions  = $action;

        $adminName = DB::table('users')->select('name','lastName')->first();
        $object->adminName  = $adminName->name;

        $themeNotes = DB::table('iot_theme_notes')
            ->select('iot_theme_notes.id','iot_theme_notes.message','iot_theme_notes.themeId','iot_theme_notes.created_at','users.name','users.lastName')
            ->leftJoin('users','users.id','iot_theme_notes.userId')
            ->where('themeId',$rValue->id)->orderBy('created_at','DESC')->first();
        $object->themeNote = $themeNotes;

        $allThemeNotes = DB::table('iot_theme_notes')
            ->select('iot_theme_notes.id','iot_theme_notes.message','iot_theme_notes.themeId','iot_theme_notes.created_at','users.name','users.lastName')
            ->leftJoin('users','users.id','iot_theme_notes.userId')
            ->where('themeId',$rValue->id)->orderBy('created_at','DESC')->get();
        $object->allThemeNote = $allThemeNotes;

        array_push($objectArr, $object);
    }


// die();
    $themeListTbl = $objectArr;
    // echo "<pre>";print_r($themeListTbl);die();

    $themeCategory = DB::table('iot_theme_category')->select('id','title')->where('status','Active')->get();

    $organisations = DB::table('organisations')->select('id','organisation')->where('status','Active')->get();
  
    return view('admin.IOT.iotThemeList',compact('themeListTbl','themeListTblpagi','themeCategory','organisations'));
} 

/*store theme*/
public function storeCustomTheme()
{
    $dateOpened         = Input::get('date_opened');
    $title              = Input::get('title');
    $description        = Input::get('description');
    $type               = Input::get('type');
    $orgId              = Input::get('organisation');
    $status             = Input::get('status');
    $initialLikelihood  = Input::get('initial_likelihood');
    $initialConsequence = Input::get('initial_consequence');
    $currentLikelihood  = Input::get('current_likelihood');
    $currentConsequence = Input::get('current_consequence');
    $linkedAction       = Input::get('linked_action');

    $insertArray = array(
        "dateOpened" => date('Y-m-d',strtotime($dateOpened)),
        "title" => $title,
        "description" => $description,
        "type" => $type,
        "orgId" => $orgId, 
        "submission" => '',
        "initialLikelihood" => $initialLikelihood, 
        "initialConsequence" => $initialConsequence,
        "currentLikelihood" => $currentLikelihood,
        "currentConsequence" => $currentConsequence,
        "linkedAction" => implode(',', $linkedAction),
        "themeStatus" => $status,
        "status" => 'Active',
        "created_at"=> date('Y-m-d H:i:s')
        );

    $themeId = DB::table('iot_themes')->insertGetId($insertArray);

    $themeArr = [];
    $actionThemes = [];
    foreach ($linkedAction as $linkedActionVal) {

        $actions = DB::table('actions')->where('id',$linkedActionVal)->first();
        
        if (!empty($actions)) {
            if (!empty($actions->themeId)) { //If themes exist

                $actionThemes = explode(',', $actions->themeId);
                array_push($actionThemes, "$themeId");

                DB::table('actions')->where('id',$linkedActionVal)->update([
                    'themeId' => implode(',', $actionThemes),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }else{ //No themes

                array_push($themeArr, "$themeId");

                DB::table('actions')->where('id',$linkedActionVal)->update([
                    'themeId' => implode(',', $themeArr),
                    'updated_at' => date('Y-m-d H:i:s')
                ]); 
            }
        }     
    }

    //redirect to following link
    return redirect('admin/theme-list/'.base64_encode($orgId).'/'.base64_encode($type))->with('message','Record Added Successfully.');

}

/*get edit page of custom theme*/
public function editCustomTheme(Request $request)
{
    $url = URL::previous();

    $last = explode("/", $url);

    if($last[6] != base64_encode(0))
    {

        $orgId = ($request->segment(3));

        $organisations = DB::table('organisations')->select('ImageURL')->where('id',$orgId)->first();

        if(!empty($organisations->ImageURL))
        {
            $org_detail = base64_encode($orgId)."-".$organisations->ImageURL;
            View::share('org_detail', $org_detail);
        }
        else
        {
            $org_detail = "";
            View::share('org_detail', $org_detail);
        }
    }

    $themeId = base64_decode($request->id);

    $themeTbl = DB::table('iot_themes')->where('id',$themeId)->first();

    $organisations = DB::table('organisations')->where('status','Active')->get();
    $types = DB::table('iot_theme_category')->where('status','Active')->get();

    $selectedActionArr = array();
    if(json_decode($themeTbl->linkedAction))
    {
        $selectedActionArr = json_decode($themeTbl->linkedAction);
    }

    $actionList = DB::table('actions')->select('id')->where('status','Active')->where('orgId',$themeTbl->orgId)->orderBy('id','DESC')->get();

    return view('admin.IOT.iotEditTheme',compact('actionList','selectedActionArr','themeTbl','organisations','types'));    
}

/*update custom theme*/
public function updateCustomTheme()
{
    // date_default_timezone_set('UTC');

    // $the_date = strtotime(date("Y-m-d H:i:s"));
    //echo(date_default_timezone_get() . "<br />");
    //echo(date("Y-d-mTG:i:sz",$the_date) . "<br />");
    // date_default_timezone_set("UTC");
    // $date_utc = date("Y-m-d H:i:s", $the_date);

    $title              = Input::get('title');
    $dateOpened         = Input::get('date_opened');
    $description        = Input::get('description');
    $themeCategory      = Input::get('themeCategory');
    $initialLikelihood  = Input::get('initial_likelihood');
    $initialConsequence = Input::get('initial_consequence');
    $currentLikelihood  = Input::get('current_likelihood');
    $currentConsequence = Input::get('current_consequence');
    $orgId              = Input::get('organisation');
    $linkedAction       = Input::get('actionTaken');
    $status             = Input::get('status');
    $comment            = trim(Input::get('comment'));
    $themeId            = base64_decode(Input::get('themeId'));
    $submissions        = Input::get('submissions');   

    // $dateOpened         = Input::get('date_opened');
    // $title              = Input::get('title');
    // $description        = Input::get('description');
    // $type               = Input::get('type');
    // $orgId              = Input::get('organisation');
    // $status             = Input::get('status');
    // $initialLikelihood  = Input::get('initial_likelihood');
    // $initialConsequence = Input::get('initial_consequence');
    // $currentLikelihood  = Input::get('current_likelihood');
    // $currentConsequence = Input::get('current_consequence');
    // $linkedAction       = Input::get('linked_action');
    // $themeId            = base64_decode(Input::get('themeId'));

    DB::table('iot_allocated_themes')->where('themeId',$themeId)->delete();

    foreach ($submissions as $submissionVal) {
        DB::table('iot_allocated_themes')->where('themeId',$themeId)->insertGetId([
            'feedbackId'    => $submissionVal,
            'themeId'       => $themeId,
            'created_at'    => date('Y-m-d H:i:s')
        ]);
    }

    $allActions = DB::table('actions')->select('id','themeId')->where('orgId',$orgId)->where('status','Active')->get();

    foreach ($allActions as $actionVals) {
        $themeids = explode(',', $actionVals->themeId);
        // $themeids = json_decode($actionVals->themeId);
        $mainThemeIds = '';
        if(!empty($themeids) && in_array($themeId, $themeids)){
            $mainThemeIds = array_diff($themeids, array($themeId));
            DB::table('actions')->where('id',$actionVals->id)->update([
                'themeId'       => implode(',', $mainThemeIds),
                // 'themeId'       => json_encode($mainThemeIds),
                'updated_at'    => date('Y-m-d H:i:s')
            ]);
        }
    }
    

    $updateArray = array(
        "title" => $title,
        "dateOpened" => date('Y-m-d',strtotime($dateOpened)),
        "description" => $description,
        "type" => $themeCategory,
        "orgId" => $orgId, 
        "submission" => '',
        "initialLikelihood" => $initialLikelihood, 
        "initialConsequence" => $initialConsequence,
        "currentLikelihood" => $currentLikelihood,
        "currentConsequence" => $currentConsequence,
        "linkedAction" => implode(',', $linkedAction),
        // "comment"       => $comment,
        "themeStatus" => $status,
        "updated_at"=> date('Y-m-d H:i:s')
        );

    DB::table('iot_themes')->where('id',$themeId)->update($updateArray);

    if (!empty($comment)) {
        $notesArray = array(
            'message'       => $comment,
            'themeId'       => $themeId,
            'userId'        => Auth::user()->id,
            'created_at'    => date('Y-m-d H:i:s') 
        );

        DB::table('iot_theme_notes')->insertGetId($notesArray);    
    }

    foreach ($linkedAction as $linkedActionVal) {

        $actions = DB::table('actions')->where('id',$linkedActionVal)->first();
        
        $themeArr = [];
        $actionThemes = [];        
        if (!empty($actions)) {
            if (!empty($actions->themeId)) { //If themes exist

                $actionThemes = explode(',', $actions->themeId);

                if (in_array($themeId, $actionThemes)) {
                    continue;
                }else{
                    array_push($actionThemes, $themeId);
                }

                DB::table('actions')->where('id',$linkedActionVal)->update([
                    'themeId' => implode(',', $actionThemes),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }else{ //No themes

                array_push($themeArr, $themeId);

                DB::table('actions')->where('id',$linkedActionVal)->update([
                    'themeId' => implode(',', $themeArr),
                    'updated_at' => date('Y-m-d H:i:s')
                ]); 
            }
        }     
    }

    //redirect to following link
    return redirect()->back();
    // return redirect('admin/theme-list/'.base64_encode($orgId).'/'.base64_encode($themeCategory))->with('message','Record updated successfully.');
}

/*get chat/msg screen*/
public function getChatMessages(Request $request)
{

    $url = URL::previous();

    $last = explode("/", $url);

    if($last[6] != base64_encode(0))
    {

        $orgId = ($request->segment(3));

        $organisations = DB::table('organisations')->select('ImageURL')->where('id',$orgId)->first();

        if(!empty($organisations->ImageURL))
        {
            $org_detail = base64_encode($orgId)."-".$organisations->ImageURL;
            View::share('org_detail', $org_detail);
        }
        else
        {
            $org_detail = "";
            View::share('org_detail', $org_detail);
        }
    }

    $feedbackId = base64_decode($request->feedbackId);
    
    $feedbackListQuery = DB::table('iot_feedbacks')
        ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.image','iot_feedbacks.created_at','iot_feedbacks.orgId','users.email','organisations.organisation','offices.office','all_department.department','iot_feedbacks.SWOT','iot_feedbacks.themeId','iot_feedbacks.feedbackSummary','iot_feedbacks.initialRiskScore','iot_feedbacks.actionTaken','iot_feedbacks.feedbackStatus','iot_theme_category.title','iot_feedbacks.userId','iot_feedbacks.status')
        ->leftJoin('users','users.id','iot_feedbacks.userId')
        ->leftJoin('iot_theme_category','iot_theme_category.id','iot_feedbacks.SWOT')
        ->leftJoin('organisations','organisations.id','users.orgId')
        ->leftJoin('offices','offices.id','users.officeId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('iot_feedbacks.id',$feedbackId);
    $feedbackListTbl = $feedbackListQuery->get();

    $objectArr = array();
    foreach ($feedbackListTbl as  $rValue)
    {
        $object    = new Controller();

        $object->id           = $rValue->id;
        $object->message      = $rValue->message;
        $object->image        = $rValue->image;
        $object->created_at   = $rValue->created_at;
        $object->email        = $rValue->email;
        $object->organisation = $rValue->organisation;
        $object->office       = $rValue->office;
        $object->department   = $rValue->department;
        $object->userId       = $rValue->userId;
        $object->status       = $rValue->status;
        $object->orgId        = $rValue->orgId;
        $object->SWOT        = $rValue->SWOT;
        $object->themeId        = $rValue->themeId;
        $object->feedbackSummary        = $rValue->feedbackSummary;
        $object->initialRiskScore = '';
        if (!empty($rValue->initialRiskScore)) {
            $riskScore = DB::table('iot_risk_priority')->select('title')->where('id',$rValue->initialRiskScore)->first();
            if (!empty($riskScore)) {
                $object->initialRiskScore = $riskScore->title;
            }else{
                $object->initialRiskScore = '';
            }
        }
        $object->actionTaken        = "";
        if (!empty($rValue->actionTaken)) {
            $actionIds = explode(',', $rValue->actionTaken);
            $actionArr = [];
            foreach ($actionIds as $actionVal) {
                $action = DB::table('actions')->select('description')->where('id',$actionVal)->where('status','Active')->first();
                array_push($actionArr, $action);
            }
            $object->actionTaken = $actionArr;
        }
        if (!empty($rValue->feedbackStatus) && $rValue->feedbackStatus == 1) {
            $object->feedbackStatus        = 'Open';
        }elseif (!empty($rValue->feedbackStatus) && $rValue->feedbackStatus == 2) {
            $object->feedbackStatus        = 'Close';
        }
        $object->swotTitle        = $rValue->title;

        if(!empty($rValue->SWOT)){
            $themeListTblQuery = DB::table('iot_themes')
                ->select('iot_themes.id','iot_themes.title')
                ->where('iot_themes.status','Active');
            if(!empty($rValue->SWOT)) {
                $themeListTblQuery->where('iot_themes.type',$rValue->SWOT);
            }
            if(!empty($rValue->orgId)){
                $themeListTblQuery->where('iot_themes.orgId',$rValue->orgId);
            }
            if(!empty($rValue->themeId)){
                $themeListTblQuery->where('iot_themes.id',$rValue->themeId);
            }
            $themeList = $themeListTblQuery->first();

            if (count($themeList)) {
                $object->swotThemes = $themeList->title;
            }else{
                $object->swotThemes = 'No Themes';    
            }
        }else{
            $object->swotThemes = '';
        }
        
        $themeListTbl = DB::table('iot_allocated_themes')
        ->select('iot_themes.title')
        ->leftJoin('iot_themes','iot_themes.id','iot_allocated_themes.themeId')
        ->where('feedbackId',$rValue->id)->where('iot_allocated_themes.status','Active')
        ->get();

        $themeListArr = array();
        foreach ($themeListTbl as $tvalue)
        {
            array_push($themeListArr, $tvalue->title);
        }

        $object->themes = $themeListArr;

        array_push($objectArr, $object);
    }

    $feedbackList = $objectArr;

    //get messages
    $messages = DB::table('iot_messages')
    ->select('iot_messages.sendTo','iot_messages.sendFrom','users.name','iot_messages.message','iot_messages.file','users.imageUrl','iot_messages.created_at')
    ->leftJoin('users','users.id','iot_messages.sendFrom')
    ->where('iot_messages.feedbackId',$feedbackId)
    ->where('iot_messages.status','Active')->orderBy('iot_messages.id','DESC')->get();

    return view('admin.IOT.iotChatbox',compact('feedbackList','messages'));
}

/*send msg */
public function sendChatMessages(Request $request)
{

    $sendFrom   = Input::get('sendFrom');
    $sendTo     = Input::get('sendTo');
    $message    = Input::get('message');
    $feedbackId = Input::get('feedbackId');

    if($request->hasfile('file'))
    {
        $image = $request->file('file'); 

        $imageName   = 'iot_'.time().'.'.$image->getClientOriginalExtension();
        $destination = public_path('uploads/iot_files/');
        $image->move($destination, $imageName);

        $insertmsgArray['sendFrom']   = $sendFrom;
        $insertmsgArray['sendTo']     = $sendTo;   
        $insertmsgArray['feedbackId'] = $feedbackId;
        $insertmsgArray['status']     = 'Active';
        $insertmsgArray['created_at'] = date('Y-m-d H:i:s');
        $insertmsgArray['file']       = $imageName;

        $insertGetId = DB::table('iot_messages')->insertGetId($insertmsgArray);

        //Notification part
        $user = DB::table('users')->select('fcmToken')->where('id', $sendTo)->where('status','Active')->first();

        $fcmToken = '';
        if($user)
        {
            $fcmToken = $user->fcmToken;
        }

        //get badge count 
        $totbadge = app('App\Http\Controllers\API\ApiDotController')->getIotNotificationBadgeCount(array('userId'=>$sendTo));

        $title    = 'Improving';
        $message  = 'You have received a new message';

        $notiArray = array('fcmToken'=>$fcmToken, 'title'=>$title ,'message'=>$message, 'totbadge'=>$totbadge, 'feedbackId'=>$feedbackId);

        $test = app('App\Http\Controllers\Admin\CommonController')->sendFcmNotify($notiArray);

        $notificationArray = array(
          'to_bubble_user_id'  => $sendTo,
          'from_bubble_user_id'=> $sendFrom,
          'title' => $title,
          'description' => $message,  
          'feedbackId' => $feedbackId, 
          'notificationType' => 'chat',
          'created_at' => date('Y-m-d H:i:s')
          );

        DB::table('iot_notifications')->insertGetId($notificationArray);
    //end notification
    }

    if($message)
    {
        $insertArray['sendFrom']   = $sendFrom;
        $insertArray['sendTo']     = $sendTo;   
        $insertArray['feedbackId'] = $feedbackId;
        $insertArray['status']     = 'Active';
        $insertArray['created_at'] = date('Y-m-d H:i:s');
        $insertArray['message']    = $message;

        $insertGetId = DB::table('iot_messages')->insertGetId($insertArray);


            //Notification part
        $user = DB::table('users')->select('fcmToken')->where('id', $sendTo)->where('status','Active')->first();

        $fcmToken = '';
        if($user)
        {
            $fcmToken = $user->fcmToken;
        }

        //get badge count 
        $totbadge = app('App\Http\Controllers\API\ApiDotController')->getIotNotificationBadgeCount(array('userId'=>$sendTo));

        $title    = 'Improving';
        $message  = 'You have received a new message';

        $notiArray = array('fcmToken'=>$fcmToken, 'title'=>$title ,'message'=>$message, 'totbadge'=>$totbadge, 'feedbackId'=>$feedbackId);

        $test = app('App\Http\Controllers\Admin\CommonController')->sendFcmNotify($notiArray);

        $notificationArray = array(
          'to_bubble_user_id'  => $sendTo,
          'from_bubble_user_id'=> $sendFrom,
          'title' => $title,
          'description' => $message,  
          'feedbackId' => $feedbackId, 
          'notificationType' => 'chat',
          'created_at' => date('Y-m-d H:i:s')
          );

        DB::table('iot_notifications')->insertGetId($notificationArray);
    ///end notification
    }




    $messages = DB::table('iot_messages')
    ->select('iot_messages.sendTo','iot_messages.sendFrom','users.name','iot_messages.message','iot_messages.file','users.imageUrl','iot_messages.created_at')->leftJoin('users','users.id','iot_messages.sendFrom')->where('iot_messages.feedbackId',$feedbackId)->where('iot_messages.status','Active')->orderBy('iot_messages.id','DESC')->get();

    $htmlArr = array();
    foreach($messages as $msg)
    {
        $html = '';
        if($msg->sendFrom !=1)
        {
            if($msg->message)
            {
                $html .= '<li class="sent"><div class="user-img"><img src="' .url("public/uploads/user_profile/".$msg->imageUrl).'"/></div><p>
                <span>'. $msg->message .'</span><time>'.date('H:i a',strtotime($msg->created_at)).'</time></p></li>';
            }

            if($msg->file)
            {
                $html .= '<li class="sent"><img src="'.url("public/uploads/iot_files/".$msg->file).'"/></li>';
            }
        }
        elseif($msg->sendFrom == 1)
        {
           if($msg->message)
           {        
             $html .= '<li class="replies"><div class="user-img"><img src="' .url("public/uploads/user_profile/".$msg->imageUrl).'"/></div><p>
             <span>'. $msg->message .'</span><time>'.date('H:i a',strtotime($msg->created_at)).'</time></p></li>';
         }

         if($msg->file)
         {
            $html .= '<li class="replies"><img src="'.url("public/uploads/iot_files/".$msg->file).'"/></li>';
        }
    }

    array_push($htmlArr, $html);
}

return $htmlArr;

}

/*get msgs by ajax call every time given*/
public function getChatMessagesByAjax()
{

    $feedbackId = Input::get('feedbackId');

    $messages = DB::table('iot_messages')
    ->select('iot_messages.sendTo','iot_messages.sendFrom','users.name','iot_messages.message','iot_messages.file','users.imageUrl','iot_messages.created_at')->leftJoin('users','users.id','iot_messages.sendFrom')->where('iot_messages.feedbackId',$feedbackId)->where('iot_messages.status','Active')->orderBy('iot_messages.id','DESC')->get();

    $htmlArr = array();
    foreach($messages as $msg)
    {
        $html = '';
        if($msg->sendFrom !=1)
        {
           if($msg->message)
           {
            $html .= '<li class="sent"><div class="user-img"><img src="' .url("public/uploads/user_profile/".$msg->imageUrl).'"/></div><p>
            <span>'. $msg->message .'</span><time>'.date('H:i a',strtotime($msg->created_at)).'</time></p></li>';
        }

        if($msg->file)
        {
            $html .= '<a href="'.url("public/uploads/iot_files/". $msg->file).'" target="blank"><li class="sent"><img src="'.url("public/uploads/iot_files/".$msg->file).'"/></li></a>';
        }
    }
    elseif($msg->sendFrom == 1)
    {
     if($msg->message)
     {

         $html .= '<li class="replies"><div class="user-img"><img src="' .url("public/uploads/user_profile/".$msg->imageUrl).'"/></div><p>
         <span>'. $msg->message .'</span><time>'.date('H:i a',strtotime($msg->created_at)).'</time></p></li>';
     }

     if($msg->file)
     {
        $html .= '<a href="'.url("public/uploads/iot_files/". $msg->file).'" target="blank"><li class="replies"><img src="'.url("public/uploads/iot_files/".$msg->file).'"/></li></a>';
    }
}

array_push($htmlArr, $html);
}

return $htmlArr;
}

/*update feedback theme */
public function updateFeedbackTheme()
{
    $feedbackId = Input::get('feedbackId');

    $themeIdArr = Input::get('themeId');

    DB::table('iot_allocated_themes')->where('feedbackId', $feedbackId)->update(['status'=>'Inactive']);
    foreach ($themeIdArr as $themeId)
    {
        $isThemeAllocated = DB::table('iot_allocated_themes')->where('feedbackId',$feedbackId)->where('themeId', $themeId)->first();

        if($isThemeAllocated)
        {
            $updateArray = array(               
                'status'=> 'Active',
                'updated_at'=> date('Y-m-d H:i:s')
                );
            DB::table('iot_allocated_themes')->where('feedbackId', $feedbackId)->where('themeId', $themeId)->update($updateArray);
        }
        else
        {
            $insertArray = array(
                'feedbackId'=> $feedbackId,
                'themeId'=> $themeId,               
                'status'=> 'Active',
                'updated_at'=> date('Y-m-d H:i:s')
                );
            DB::table('iot_allocated_themes')->insertGetId($insertArray);
        }
    }
}

/*get theme add modal */
public function getThemeModal()
{
    $feedbackId = Input::get('feedbackId');

    $feedback = DB::table('iot_feedbacks')->where('id', $feedbackId)->first();

    if($feedback)
    {
      $orgId = $feedback->orgId;   
  }

  $themeSelectedTbl = DB::table('iot_allocated_themes')->where('status','Active')->where('feedbackId',$feedbackId)->get();

  $selectedfeedbackArr = array();
  foreach ($themeSelectedTbl as $value)
  {
    array_push($selectedfeedbackArr, $value->themeId);
}

$themeList = DB::table('iot_themes')->where('status','Active')->where('orgId',$orgId)->orderBy('id','DESC')->get();

return view('admin.IOT.iotThemeModal',compact('themeList','feedbackId','selectedfeedbackArr'));

}


/*add new theme by ajax when modal is open*/
public function addNewThemeByAjax()
{

    $feedbackId = Input::get('feedbackId');

    $feedback = DB::table('iot_feedbacks')->where('id', $feedbackId)->first();

    $orgId = '';
    if($feedback)
    {
      $orgId = $feedback->orgId;   
  }

  $insertArray = array(
    "dateOpened" => date('Y-m-d'),
    "title" => Input::get('themeTitle'),
    "description" => '',
    "type" => 1,
    "orgId" => $orgId, 
    "submission" => '',
    "initialLikelihood" => '0', 
    "initialConsequence" => '0',
    "currentLikelihood" => '0',
    "currentConsequence" => '0',
    "linkedAction" => '',
    "themeStatus" => 'Closed',
    "status" => 'Active',
    "created_at"=> date('Y-m-d H:i:s')
    );

  if(Input::get('themeTitle'))
  {
    DB::table('iot_themes')->insertGetId($insertArray);
}

$themeList = DB::table('iot_themes')->select('id','title')->where('status','Active')->where('orgId',$orgId)->orderBy('id','DESC')->get();

$htmlContainer = '';
$htmlOption = '';
foreach($themeList as $value)
{
    $htmlContainer.='<li><a tabindex="0"><label class="checkbox" title="'.$value->title.'"><input type="checkbox" value="'.$value->id.'">'.$value->title.'</label></a></li>';

    $htmlOption.='<option value="'.$value->id.'">'.$value->title.'</option>'; 
}

$htmlArr = array('htmlContainer'=>$htmlContainer, 'htmlOption'=>$htmlOption);

print_r(json_encode($htmlArr));

}

/*get actions list*/
public function getActionsByAjax()
{

    $orgId = Input::get('orgId');

    $actionList = DB::table('actions')->select('id','description')->where('status','Active')->where('orgId',$orgId)->get();

    $htmlContainer = '';
    $htmlOption = '';
    foreach ($actionList as $value)
    {
        $htmlContainer.='<li><a tabindex="0"><label class="checkbox" title="'.$value->description.'"><input type="checkbox" value="'.$value->id.'">'.$value->description.'</label></a></li>';

        $htmlOption.='<option value="'.$value->id.'">'.$value->description.'</option>'; 
    }

    $htmlArr = array('htmlContainer'=>$htmlContainer, 'htmlOption'=>$htmlOption);

    print_r(json_encode($htmlArr));

}

/*get actions list edit*/
public function getActionsByAjaxEdit()
{

    $orgId = Input::get('orgId');

    $actionList = DB::table('actions')->select('id','description')->where('status','Active')->where('orgId',$orgId)->get();

    $feedbackList = DB::table('iot_feedbacks')->select('id','message')->where('status','Active')->where('orgId',$orgId)->get();

    $htmlOption = '';
    $feedbackOption = '';
    foreach ($actionList as $value) {
        $htmlOption.='<option value="'.$value->id.'">'.$value->description.'</option>'; 
    }
    foreach ($feedbackList as $feedbackVal) {
        $feedbackOption.='<option value="'.$feedbackVal->id.'">'.$feedbackVal->message.'</option>'; 
    }

    $htmlArr = array('status'=>200,'htmlOption'=>$htmlOption,'feedbackOption'=>$feedbackOption);

    print_r(json_encode($htmlArr));
}

public function deleteThemes()
{
    $orgId = base64_decode(Input::get('orgId'));
    $themeId = base64_decode(Input::get('themeId'));
    
    DB::table('iot_themes')->where('id',$themeId)->where('orgId',$orgId)->delete();

    DB::table('iot_theme_notes')->where('themeId',$themeId)->delete();

    //Data delete from theme allocated table
    $themeAllocatedFeedback = DB::table('iot_allocated_themes')->where('themeId',$themeId)->delete();

    //theme id delete from feedback table
    $themeFeedback = DB::table('iot_feedbacks')
        ->where('orgId',$orgId)
        ->where('status','Active')
        ->whereRaw("find_in_set(".$themeId.",themeId)")
        ->get();

    foreach ($themeFeedback as $feedbackVals) {
        $themeids1 = explode(',', $feedbackVals->themeId);
        $mainThemeIds1 = '';
        if(!empty($themeids1) && in_array($themeId, $themeids1)){
            $mainThemeIds1 = array_diff($themeids1, array($themeId));
            DB::table('iot_feedbacks')->where('id',$feedbackVals->id)->update([
                'themeId'       => implode(',', $mainThemeIds1),
                'updated_at'    => date('Y-m-d H:i:s')
            ]);
        }
    }

    //theme id delete from actions table
    $themeActions = DB::table('actions')
        ->where('orgId',$orgId)
        ->where('status','Active')
        ->whereRaw("find_in_set(".$themeId.",themeId)")
        ->get();

    foreach ($themeActions as $actionVals) {
        $themeids = explode(',', $actionVals->themeId);
        $mainThemeIds = '';
        if(!empty($themeids) && in_array($themeId, $themeids)){
            $mainThemeIds = array_diff($themeids, array($themeId));
            DB::table('actions')->where('id',$actionVals->id)->update([
                'themeId'       => implode(',', $mainThemeIds),
                'updated_at'    => date('Y-m-d H:i:s')
            ]);
        }
    }

    return json_encode(array('status'=>200,'message'=>'Theme Successfully Deleted'));
}

public function deleteFeedbacks()
{
    $orgId = base64_decode(Input::get('orgId'));
    $feedbackId = base64_decode(Input::get('feedbackId'));

    // Data delete from feedback table
    $feedback = DB::table('iot_feedbacks')->where('id',$feedbackId)->where('orgId',$orgId)->delete();

    //Data delete from feedback message table
    $feedbackMessage = DB::table('iot_messages')->where('feedbackId',$feedbackId)->delete();

    //Data delete from notifications table
    $feedbackNotifications = DB::table('iot_notifications')->where('feedbackId',$feedbackId)->delete();

    //Data delete from theme allocated table
    $feedbackAllocatedFeedback = DB::table('iot_allocated_themes')->where('feedbackId',$feedbackId)->delete();

    //feedback id delete from actions table
    $feedbackActions = DB::table('actions')
        ->where('orgId',$orgId)
        ->where('status','Active')
        ->whereRaw("find_in_set(".$feedbackId.",feedbackId)")
        ->get();

    foreach ($feedbackActions as $actionVals) {
        $feedbackids = explode(',', $actionVals->feedbackId);
        $mainFeedbackIds = '';
        if(!empty($feedbackids) && in_array($feedbackId, $feedbackids)){
            $mainFeedbackIds = array_diff($feedbackids, array($feedbackId));
            DB::table('actions')->where('id',$actionVals->id)->update([
                'feedbackId'       => implode(',', $mainFeedbackIds),
                'updated_at'    => date('Y-m-d H:i:s')
            ]);
        }
    }

    return json_encode(array('status'=>200,'message'=>'Feedbacks Successfully Deleted'));
}

//Risk register view
public function showRiskRegister(Request $request)
{
    $orgId    = $request->orgId;

    $themesCategory = DB::table('iot_theme_category')->where('status','Active')->get();

    $organisations = DB::table('organisations')->where('status','Active')->orderBy('organisation','ASC')->get();

    $feedbackList = array();
    foreach ($themesCategory as $themeCate) {
        $feedbackListQuery = DB::table('iot_feedbacks')
            ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.created_at','iot_feedbacks.orgId','iot_feedbacks.SWOT','iot_feedbacks.themeId','iot_feedbacks.feedbackSummary','iot_feedbacks.initialRiskScore','iot_feedbacks.actionTaken','iot_feedbacks.feedbackStatus','iot_feedbacks.mitigatedScore','iot_feedbacks.updatedText','organisations.organisation','iot_risk_priority.title as riskScore')
            ->leftJoin('users','users.id','iot_feedbacks.userId')
            ->leftJoin('organisations','organisations.id','users.orgId')
            ->leftJoin('iot_risk_priority','iot_risk_priority.id','iot_feedbacks.initialRiskScore');
        if (!empty($themeCate->id) && $themeCate->id == 1) {
            $feedbackListQuery->where('iot_feedbacks.SWOT','LIKE','%1%');
        }elseif (!empty($themeCate->id) && $themeCate->id == 2) {
            $feedbackListQuery->where('iot_feedbacks.SWOT','LIKE','%2%');
        }
        elseif (!empty($themeCate->id) && $themeCate->id == 3) {
            $feedbackListQuery->where('iot_feedbacks.SWOT','LIKE','%3%');
        }elseif (!empty($themeCate->id) && $themeCate->id == 4) {
            $feedbackListQuery->where('iot_feedbacks.SWOT','LIKE','%4%');
        }
        if(!empty($orgId)) {
            $feedbackListQuery->where('users.orgId',$orgId);
        }
        $feedbackListQuery->where('iot_feedbacks.status','!=','Inactive')
            ->orderBy('iot_feedbacks.id','DESC');
        if (!empty($themeCate->id) && $themeCate->id == 1) {
            $feedbackListTbl1 = $feedbackListQuery->paginate(10);
        }elseif (!empty($themeCate->id) && $themeCate->id == 2) {
            $feedbackListTbl2 = $feedbackListQuery->paginate(10);
        }
        elseif (!empty($themeCate->id) && $themeCate->id == 3) {
            $feedbackListTbl3 = $feedbackListQuery->paginate(10);
        }elseif (!empty($themeCate->id) && $themeCate->id == 4) {
            $feedbackListTbl4 = $feedbackListQuery->paginate(10);
        }
        

        // $objectArr = array();
        // foreach ($feedbackListTbl as  $rValue)
        // {
        //     $object    = new Controller();

        //     $object->id                 = $rValue->id;
        //     $object->created_at         = $rValue->created_at;
        //     $object->message            = $rValue->message;
        //     // $object->SWOT   = '-';
        //     // if (!empty($rValue->SWOT)) {
        //     //     $SWOTVal = DB::table('iot_theme_category')->where('id',$rValue->SWOT)->first();
        //     //     if (!empty($SWOTVal)) {
        //     //         $object->SWOT   = $SWOTVal->title;        
        //     //     }
        //     // }
        //     // $themes = [];
        //     // if (!empty($rValue->themeId)) {
        //     //     $themes = DB::table('iot_themes')->where('id',$rValue->themeId)->first();
        //     // }
        //     // $object->themeId            = '-';
        //     // $object->initialLikelihood  = '-';
        //     // $object->initialConsequence  = '-';
        //     // if (!empty($themes)) {
        //     //     $object->themeId            = $themes->title;
        //     //     $object->initialLikelihood  = $themes->initialLikelihood;
        //     //     $object->initialConsequence = $themes->initialConsequence;
        //     // }else{
        //     //     if (!empty($rValue->SWOT)) {
        //     //         $object->themeId            = 'No Themes';
        //     //         $object->initialLikelihood  = 'No Likelihood';
        //     //         $object->initialConsequence  = 'No Consequence';
        //     //     }
        //     // }
        //     // $object->initialRiskScore   = '';
        //     // if (!empty($rValue->initialRiskScore)) {
        //     //     $initRiskScore = DB::table('iot_risk_priority')->where('id',$rValue->initialRiskScore)->first();
        //     //     if (!empty($initRiskScore)) {
        //     //         $object->initialRiskScore   = $initRiskScore->title;        
        //     //     }
        //     // }
        //     // $object->actionTaken = '-';
        //     // if (!empty($rValue->actionTaken)) {
        //     //     $object->actionTaken        = $rValue->actionTaken;
        //     // }
        //     // $object->mitigatedScore     = $rValue->mitigatedScore;
        //     // $object->feedbackStatus   = '';
        //     // if (!empty($rValue->feedbackStatus) && $rValue->feedbackStatus == 1) {
        //     //     $object->feedbackStatus   = 'Open';
        //     // }elseif (!empty($rValue->feedbackStatus) && $rValue->feedbackStatus == 2) {
        //     //     $object->feedbackStatus   = 'Close';
        //     // }
        //     // $object->orgId              = $rValue->orgId;
        //     // $object->updatedText        = $rValue->updatedText;
        //     array_push($objectArr, $object);
        // }
        // $objectArr['themeId'] = $themeCate->id;

        if (!empty($themeCate->id) && $themeCate->id == 1) {
            $feedbackArr['feedbackArr'] = $feedbackListTbl1;
            $feedbackArr['themeId'] = $themeCate->id;
            array_push($feedbackList, $feedbackArr);
        }elseif (!empty($themeCate->id) && $themeCate->id == 2) {
            $feedbackArr['feedbackArr'] = $feedbackListTbl2;
            $feedbackArr['themeId'] = $themeCate->id;
            array_push($feedbackList, $feedbackArr);
        }
        elseif (!empty($themeCate->id) && $themeCate->id == 3) {
            $feedbackArr['feedbackArr'] = $feedbackListTbl3;
            $feedbackArr['themeId'] = $themeCate->id;
            array_push($feedbackList, $feedbackArr);
        }elseif (!empty($themeCate->id) && $themeCate->id == 4) {
            $feedbackArr['feedbackArr'] = $feedbackListTbl4;
            $feedbackArr['themeId'] = $themeCate->id;
            array_push($feedbackList, $feedbackArr);
        }

        
    }
    // echo "<pre>";print_r($feedbackList);
    // die();












    // $feedbackListQuery = DB::table('iot_feedbacks')
    //     ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.created_at','iot_feedbacks.orgId','iot_feedbacks.SWOT','iot_feedbacks.themeId','iot_feedbacks.feedbackSummary','iot_feedbacks.initialRiskScore','iot_feedbacks.actionTaken','iot_feedbacks.feedbackStatus','iot_feedbacks.mitigatedScore','iot_feedbacks.updatedText')
    //     ->leftJoin('users','users.id','iot_feedbacks.userId');
    // if(!empty($orgId)) {
    //     $feedbackListQuery->where('users.orgId',$orgId);
    // }
    // $feedbackListQuery->where('iot_feedbacks.status','!=','Inactive')
    //     ->orderBy('iot_feedbacks.id','DESC');
    // $feedbackListTbl = $feedbackListQuery->get();

    // echo "<pre>";print_r($feedbackListTbl);die();

    // $objectArr = array();
    // foreach ($feedbackListTbl as  $rValue)
    // {
    //     $object    = new Controller();

    //     $object->id                 = $rValue->id;
    //     $object->created_at         = $rValue->created_at;
    //     $object->message            = $rValue->message;
    //     $object->SWOT   = '-';
    //     if (!empty($rValue->SWOT)) {
    //         $SWOTVal = DB::table('iot_theme_category')->where('id',$rValue->SWOT)->first();
    //         if (!empty($SWOTVal)) {
    //             $object->SWOT   = $SWOTVal->title;        
    //         }
    //     }
    //     $themes = [];
    //     if (!empty($rValue->themeId)) {
    //         $themes = DB::table('iot_themes')->where('id',$rValue->themeId)->first();
    //     }
    //     $object->themeId            = '-';
    //     $object->initialLikelihood  = '-';
    //     $object->initialConsequence  = '-';
    //     if (!empty($themes)) {
    //         $object->themeId            = $themes->title;
    //         $object->initialLikelihood  = $themes->initialLikelihood;
    //         $object->initialConsequence = $themes->initialConsequence;
    //     }else{
    //         if (!empty($rValue->SWOT)) {
    //             $object->themeId            = 'No Themes';
    //             $object->initialLikelihood  = 'No Likelihood';
    //             $object->initialConsequence  = 'No Consequence';
    //         }
    //     }
    //     $object->initialRiskScore   = '';
    //     if (!empty($rValue->initialRiskScore)) {
    //         $initRiskScore = DB::table('iot_risk_priority')->where('id',$rValue->initialRiskScore)->first();
    //         if (!empty($initRiskScore)) {
    //             $object->initialRiskScore   = $initRiskScore->title;        
    //         }
    //     }
    //     $object->actionTaken = '-';
    //     if (!empty($rValue->actionTaken)) {
    //         $object->actionTaken        = $rValue->actionTaken;
    //     }
    //     $object->mitigatedScore     = $rValue->mitigatedScore;
    //     $object->feedbackStatus   = '';
    //     if (!empty($rValue->feedbackStatus) && $rValue->feedbackStatus == 1) {
    //         $object->feedbackStatus   = 'Open';
    //     }elseif (!empty($rValue->feedbackStatus) && $rValue->feedbackStatus == 2) {
    //         $object->feedbackStatus   = 'Close';
    //     }
    //     $object->orgId              = $rValue->orgId;
    //     $object->updatedText        = $rValue->updatedText;
    //     array_push($objectArr, $object);
    // }
    // $feedbackList = $objectArr;

    $priorityList = DB::table('iot_risk_priority')->select('id','title')->get();

    // $themesCategory = DB::table('iot_theme_category')->where('status','Active')->get();

    return view('admin.IOT.iotRiskRegisterView',compact('feedbackList','feedbackListTbl','priorityList','themesCategory','feedbackListTbl1','feedbackListTbl2','feedbackListTbl3','feedbackListTbl4','orgId','organisations'));
}
// public function showRiskRegister(Request $request)
// {
//     $orgId    = base64_decode($request->orgId);

//     $feedbackListQuery = DB::table('iot_feedbacks')
//         ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.created_at','iot_feedbacks.orgId','iot_feedbacks.SWOT','iot_feedbacks.themeId','iot_feedbacks.feedbackSummary','iot_feedbacks.initialRiskScore','iot_feedbacks.actionTaken','iot_feedbacks.feedbackStatus','iot_feedbacks.mitigatedScore','iot_feedbacks.updatedText')
//         ->leftJoin('users','users.id','iot_feedbacks.userId');
//     if(!empty($orgId)) {
//         $feedbackListQuery->where('users.orgId',$orgId);
//     }
//     $feedbackListQuery->where('iot_feedbacks.status','Completed')
//         ->orderBy('iot_feedbacks.id','DESC');
//     $feedbackListTbl = $feedbackListQuery->paginate(4);

//     $objectArr = array();
//     foreach ($feedbackListTbl as  $rValue)
//     {
//         $object    = new Controller();

//         $object->id                 = $rValue->id;
//         $object->created_at         = $rValue->created_at;
//         $object->message            = $rValue->message;
//         $object->SWOT   = '-';
//         if (!empty($rValue->SWOT)) {
//             $SWOTVal = DB::table('iot_theme_category')->where('id',$rValue->SWOT)->first();
//             if (!empty($SWOTVal)) {
//                 $object->SWOT   = $SWOTVal->title;        
//             }
//         }
//         $themes = [];
//         if (!empty($rValue->themeId)) {
//             $themes = DB::table('iot_themes')->where('id',$rValue->themeId)->first();
//         }
//         $object->themeId            = '-';
//         $object->initialLikelihood  = '-';
//         $object->initialConsequence  = '-';
//         if (!empty($themes)) {
//             $object->themeId            = $themes->title;
//             $object->initialLikelihood  = $themes->initialLikelihood;
//             $object->initialConsequence = $themes->initialConsequence;
//         }else{
//             if (!empty($rValue->SWOT)) {
//                 $object->themeId            = 'No Themes';
//                 $object->initialLikelihood  = 'No Likelihood';
//                 $object->initialConsequence  = 'No Consequence';
//             }
//         }
//         $object->initialRiskScore   = '';
//         if (!empty($rValue->initialRiskScore)) {
//             $initRiskScore = DB::table('iot_risk_priority')->where('id',$rValue->initialRiskScore)->first();
//             if (!empty($initRiskScore)) {
//                 $object->initialRiskScore   = $initRiskScore->title;        
//             }
//         }
//         $object->actionTaken = '-';
//         if (!empty($rValue->actionTaken)) {
//             $object->actionTaken        = $rValue->actionTaken;
//         }
//         $object->mitigatedScore     = $rValue->mitigatedScore;
//         $object->feedbackStatus   = '';
//         if (!empty($rValue->feedbackStatus) && $rValue->feedbackStatus == 1) {
//             $object->feedbackStatus   = 'Open';
//         }elseif (!empty($rValue->feedbackStatus) && $rValue->feedbackStatus == 2) {
//             $object->feedbackStatus   = 'Close';
//         }
//         $object->orgId              = $rValue->orgId;
//         $object->updatedText        = $rValue->updatedText;
//         array_push($objectArr, $object);
//     }
//     $feedbackList = $objectArr;

//     $priorityList = DB::table('iot_risk_priority')->select('id','title')->get();

//     return view('admin.IOT.iotRiskRegisterView',compact('feedbackList','feedbackListTbl','priorityList'));
// }

public function updateRiskRegister(Request $request)
{
    $feedbackId         = $request->feedbackId;
    $mitigatedScore     = $request->mitigatedScore;
    $updatedText        = $request->updatedText;

    DB::table('iot_feedbacks')->where('id',$feedbackId)->update([
        'mitigatedScore'    => $mitigatedScore,
        'updatedText'       => $updatedText,
        'updated_at'        => date('Y-m-d H:i:s')
    ]);
    return redirect()->back();
}

//Add actions from feedback list
public function addActionFromFeedback() {

    $orgId          = Input::get('orgId');
    $actionText     = trim(Input::get('actionText'));
    $userId         = Input::get('userId');
    $feedbackId     = Input::get('feedbackId');

    $actionId = DB::table('actions')->insertGetId([
        'userId'            => 1,
        'description'       => $actionText,
        'startedDate'       => date('Y-m-d'),
        'responsibleId'     => 1,
        'dueDate'           => date('Y-m-d',strtotime('+1 days')),
        'orgStatus'         =>'Not Started',
        'orgId'             => $orgId,
        'responsibleUserId' =>$userId
    ]);

    $actionTaken = DB::table('iot_feedbacks')->select('actionTaken')->where('id',$feedbackId)->first();

    $actionIds = [];
    if (!empty($actionTaken)) {
        $actionIds = explode(',', $actionTaken->actionTaken); 
        if (!empty($actionId)) {
            $actionIds[] = $actionId;
        }
    }
    DB::table('iot_feedbacks')->where('id',$feedbackId)->update([
        'actionTaken' => implode(',', $actionIds)
    ]);
    return json_encode(array('status'=>200,'actionText'=>$actionText,'actionId'=>$actionId));
}

//add risks from feedback list
public function addRisksFromFeedback() {

    $orgId          = Input::get('orgId');
    $riskText       = trim(Input::get('riskText'));
    $swotId         = Input::get('swotId');
    $feedbackId     = Input::get('feedbackId');
    $actionTaken    = Input::get('actionTaken');

    $themeId = DB::table('iot_themes')->insertGetId([
        'dateOpened'        => date('Y-m-d'),
        'title'             => $riskText,
        'type'              => $swotId,
        'orgId'             => $orgId,
        'initialLikelihood' => 1,
        'initialConsequence'=> 1,
        'initialConsequence'=> 1,
        'currentConsequence'=> 1,
        'themeStatus'       => 'Open',
        'created_at' => date('Y-m-d H:i:s')
    ]);

    //add submissions in allocated theme table
    DB::table('iot_allocated_themes')->insertGetId([
        'feedbackId' => $feedbackId,
        'themeId'    => $themeId,
        'created_at' => date('Y-m-d H:i:s')
    ]);


    $themes = DB::table('iot_feedbacks')->select('themeId')->where('id',$feedbackId)->where('status','Active')->first();

    $themeIds = '';
    if (!empty($themes)) {
        if (!empty($themes->themeId)) {
            $themeIds = $themes->themeId.",".$themeId;
        }else{
            $themeIds = $themeId;
        }
        if (!empty($themeIds)) {
            DB::table('iot_feedbacks')->where('id',$feedbackId)->update([
                'themeId' => $themeIds
            ]);
        }
    }
    
    return json_encode(array('status'=>200,'riskText'=>$riskText,'themeId'=>$themeId));
}

// Show submission feedback list
public function showSubmissionFeedback(Request $request,$id,$orgId)
{
    $themeId = base64_decode($id);
    $orgId = base64_decode($orgId);

    $allocatedFeedbackIds = DB::table('iot_allocated_themes')->select('feedbackId')->where('themeId',$themeId)->paginate(10);

    $feedbackList = [];
    if (!empty($allocatedFeedbackIds)) {
        foreach ($allocatedFeedbackIds as $feedbackId) {
            $feedbackListQuery = DB::table('iot_feedbacks')
                ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.feedbackSummary','iot_feedbacks.initialRiskScore','iot_feedbacks.actionTaken','iot_feedbacks.feedbackStatus','iot_risk_priority.title as riskScore')
                ->leftJoin('iot_risk_priority','iot_risk_priority.id','iot_feedbacks.initialRiskScore');
            if(!empty($feedbackId)){
                $feedbackListQuery->where('iot_feedbacks.id',$feedbackId->feedbackId);
            }
            if(!empty($orgId)) {
                $feedbackListQuery->where('iot_feedbacks.orgId',$orgId);
            }
            $feedbackListTbl =  $feedbackListQuery->orderBy('iot_feedbacks.id','DESC')->first();

            $object    = new Controller();

            $object->id                 = $feedbackListTbl->id;
            $object->message            = $feedbackListTbl->message;
            $object->feedbackSummary    = "-";
            if (!empty($feedbackListTbl->feedbackSummary)) {
                $object->feedbackSummary    = $feedbackListTbl->feedbackSummary;
            }
            $object->initialRiskScore   = $feedbackListTbl->initialRiskScore;
            $object->riskScore          = $feedbackListTbl->riskScore;
            $object->actionTaken        = "";
            if (!empty($feedbackListTbl->actionTaken)) {
                $actionIds = explode(',', $feedbackListTbl->actionTaken);
                $actionArr = [];
                foreach ($actionIds as $actionVal) {
                    $action = DB::table('actions')->select('description')->where('id',$actionVal)->where('status','Active')->first();
                    array_push($actionArr, $action);
                }
                $object->actionTaken = $actionArr;
            }
            if (!empty($feedbackListTbl->feedbackStatus) && $feedbackListTbl->feedbackStatus == 1) {
                $object->feedbackStatus = "Open";    
            }elseif (!empty($feedbackListTbl->feedbackStatus) && $feedbackListTbl->feedbackStatus == 2) {
                $object->feedbackStatus = "Close";
            }
            array_push($feedbackList,$object);
        }
    }
    //         echo "<pre>";print_r($feedbackList);
    // die();

    return view('admin.IOT.iotSubmissionFeedbackList',compact('feedbackList','allocatedFeedbackIds'));
}

public function addActionFromAddTheme()
{
    $orgId          = Input::get('orgId');
    $actionText     = trim(Input::get('actionText'));
    $userId         = trim(Input::get('userId'));
    $themeId        = base64_decode(Input::get('themeId'));

    $actionId = DB::table('actions')->insertGetId([
        'userId'            => 1,
        'description'       => $actionText,
        'startedDate'       => date('Y-m-d'),
        'responsibleId'     => 1,
        'dueDate'           => date('Y-m-d',strtotime('+1 days')),
        'orgStatus'         =>'Not Started',
        'orgId'             => $orgId,
        'responsibleUserId' => $userId
    ]);
    return json_encode(array('status'=>200,'actionText'=>$actionText,'actionId'=>$actionId));
}

public function responsiblePersonList()
{
    $orgId = Input::get('orgId');
    
    $resUsers = DB::table('users')
        ->select('id','name','lastName')
        ->where('status','Active')
        ->where('roleId',3)
        ->where('orgId',$orgId)->get();

    return json_encode(array('status'=>200,'resUsers'=>$resUsers));
}

}