<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetForeignKeysSecondTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('specificThreats',function(Blueprint $table)
        {
            $table->foreign('generalThreatId')->references('id')->on('generalThreats')->onDelete('cascade');
        });
        
        Schema::table('assets',function(Blueprint $table)
        {
            $table->foreign('orgId')->references('id')->on('organisations')->onDelete('cascade');
            $table->foreign('officeId')->references('id')->on('offices')->onDelete('cascade');
            $table->foreign('departmentId')->references('id')->on('departments')->onDelete('cascade');
            $table->foreign('assetCriticalId')->references('id')->on('assetsCriticalRating')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::dropIfExists('_set_foriegnkeys_second');
    }
}
