<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetForeignKeysThirdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('riskAssesments',function(Blueprint $table)
        {
            $table->foreign('generalThreatId')->references('id')->on('generalThreats')->onDelete('cascade');
            $table->foreign('specificThreatId')->references('id')->on('specificThreats')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('_set_foriegnkeys_third');
    }
}
