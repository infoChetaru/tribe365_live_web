<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiskAssesmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riskAssesments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('generalThreatId')->unsigned();
            $table->integer('specificThreatId')->unsigned();
            $table->integer('likeValue');
            $table->integer('consValue');
            $table->float('riskScore');
            $table->string('method');
            $table->integer('finalLikeValue');
            $table->integer('finalConsValue');
            $table->float('finalRiskScore');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riskAssesments');
    }
}
