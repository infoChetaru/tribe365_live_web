<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganisationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organisations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('organisation');
            $table->string('address1');
            $table->string('address2');
            $table->string('address3');
            $table->string('postcode');
            $table->string('industry');
            $table->string('phone');
            $table->string('turnover');
            $table->integer('numberOfEmployees');
            $table->integer('numberOfOffices');
            $table->integer('numberOfDepartments');
            $table->enum('status',array('Active','Inactive'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organisations');
    }
}
