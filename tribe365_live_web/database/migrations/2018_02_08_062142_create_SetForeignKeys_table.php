<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetForeignKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('clients',function(Blueprint $table)
        {
            $table->foreign('orgId')->references('id')->on('organisations')->onDelete('cascade');
            $table->foreign('officeId')->references('id')->on('offices')->onDelete('cascade');
            $table->foreign('departmentId')->references('id')->on('departments')->onDelete('cascade');
            $table->foreign('roleId')->references('id')->on('roles')->onDelete('cascade');
        });

        Schema::table('offices',function(Blueprint $table)
        {
            $table->foreign('orgId')->references('id')->on('organisations')->onDelete('cascade');
        });

        Schema::table('departments', function(Blueprint $table)
        {
            $table->foreign('orgId')->references('id')->on('organisations')->onDelete('cascade');
            $table->foreign('officeId')->references('id')->on('offices')->onDelete('cascade');
        });

        Schema::table('admins',function(Blueprint $table)
        {
            $table->foreign('orgId')->references('id')->on('organisations')->onDelete('cascade');
            $table->foreign('officeId')->references('id')->on('offices')->onDelete('cascade');
            $table->foreign('departmentId')->references('id')->on('departments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setforeignkeys');
    }
}
