<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
use View;

class EveryDayUpdate extends Command
{
        /**
     * The name and signature of the console command.
     *
     * @var string
     */
        protected $signature = 'notification:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification every day at 4:00 PM';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {   

        //Need only on 1 to 5 days
        if(date('D')=="Mon" || date('D')=="Tue" || date('D')=="Wed" || date('D')=="Thu" || date('D')=="Fri"){

            $title   = "Feedback";
            $message = "How's things at work today?";

            $users = DB::table('users')
            ->select('users.id','users.name','users.fcmToken','offices.office','offices.country','country.timezone')
            ->leftjoin('offices','offices.id','users.officeId')
            ->leftjoin('country','country.id','offices.country')
            ->where('users.roleId',3)
            ->where('users.fcmToken','!=','')
            ->where('users.status','Active')->get();

            foreach($users as $value){
                $notiArray = array('fcmToken'=> $value->fcmToken, 'title'=> $title ,'message'=> $message, 'totbadge'=>0, 'feedbackId'=>'');
                if($value->timezone){
                    date_default_timezone_set($value->timezone);
                    if(date('H:i')=='16:00') {
                        if (!empty($value->id)) {
                            $loggedInUser = DB::table('oauth_access_tokens')->where('user_id',$value->id)->where('revoked',0)->first();
                            if (!empty($loggedInUser)){
                                app('App\Http\Controllers\Admin\CommonController')->sendFcmNotify($notiArray);                   
                            }
                        }
                    }
                }
                $this->info(date('H:i'));
            }    

            /*
            Mail::send('welcome', [], function($message) {
                $message->to('ram@chetaru.com')->subject(date('D').'=== 111=> Testing mails at: '.date('Y-m-d H:i:s')); 
            });
            */

        }



        if(date('H:i')=='23:59'){
        //if(date('H:i')=='19:27'){
            
            /*
            Mail::send('welcome', [], function($message) {
                $message->to('ram@chetaru.com')->subject('Testing mails at: '.date('Y-m-d H:i:s')); 
            });
            */
            app('App\Http\Controllers\Admin\AdminReportController')->getIndexesOfAllOrganization();
        }         
    }

}
