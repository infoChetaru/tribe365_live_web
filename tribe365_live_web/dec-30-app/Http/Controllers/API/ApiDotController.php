<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Client;
use DB;
use Carbon\Carbon;
use DateTime;


class ApiDotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
    }

    public function getDotDetail()
    {

    	$resultArray = array();

    	$user = Auth::user();

    	$orgId = Input::get('orgId');
    	
    	$dot_data = DB::table('dots')
    	->where('orgId',$orgId)     
    	->latest()
    	->first();

    	$result = array();

    	if(!empty($dot_data)){  

         //dot data
    		$resultArray['id']                      = $dot_data->id;
    		$resultArray['vision']                  = $dot_data->vision;
    		$resultArray['mission']                 = $dot_data->mission;
    		$resultArray['focus']                   = $dot_data->focus;
    		$resultArray['orgId']                   = $dot_data->orgId;
    		$resultArray['dot_date']                = $dot_data->dot_date;
    		$resultArray['introductory_information']= $dot_data->introductory_information;

    		if($user->roleId==3)
    		{

    			$dot_evidence = DB::table('dot_evidence')
    			->where('dotId',$dot_data->id)    
    			->where('userId',$user->id)
    			->where('status','Active') 
    			->get();

    		}else{

    			$dot_evidence = DB::table('dot_evidence')
    			->where('dotId',$dot_data->id)    
    			->where('status','Active') 
    			->get();

    		}

    		$evidenceArr = array();

    		foreach ($dot_evidence as $evidence) {

    			$evd['id']          = (string)$evidence->id;
    			$evd['description'] = $evidence->description;
    			$evd['userId']      = $evidence->userId;
    			$evd['created_at']  = $evidence->created_at;
    			$evd['section']     = $evidence->section;
    			$evd['sectionId']   = $evidence->sectionId;
    			$evd['fileURL']     = '';

    			if($evidence->fileURL)
    			{
    				$evd['fileURL']     = url('/public/uploads/evidence_images').'/'. $evidence->fileURL;
    			}

    			array_push($evidenceArr, $evd);

    		}

    		$resultArray['evidence'] = $evidenceArr;

         //get beliefs data 
    		$beliefs_data = DB::table('dots_beliefs')
    		->where('dotId',$dot_data->id)     
    		->get();


    		$belief_arr = array();

    		foreach($beliefs_data as $value) {

    			$belief['id'] = $value->id;
    			$belief['name'] = $value->name;
    			$belief['dotId'] = $value->dotId;


    			$dots_value = DB::table('dots_values')
    			->where('beliefId',$value->id)    
    			->where('status','Active') 
    			->get();

    			$dotsValueArr = array();

    			$count = 0;
    			foreach ($dots_value as $Vvalue) {

    				$dotValueList = DB::table('dot_value_list')
    				->where('id',$Vvalue->name)     
    				->first();

    				$dValuesName = '';
    				if($dotValueList){

    					$dValuesName = $dotValueList->name;
    				}

    				$dotV["index"]     = $count;
    				$dotV["id"]        = $Vvalue->id;
    				$dotV['name']      = $dValuesName;
    				$dotV["valueId"]   = $Vvalue->name;
    				$dotV["beliefId"]  = $Vvalue->beliefId;
    				$dotV["status"]    = $Vvalue->status;
    				$dotV["created_at"]= $Vvalue->created_at;
    				$dotV["updated_at"]=$Vvalue->updated_at;
    				$dotV['dotId']     = $value->dotId;



    				$valuesRatings = db::table('dot_values_ratings')
    				->where('valueId', $Vvalue->id)
    				->where('userId',$user->id)
    				->first();

    				$dotV["ratings"]='';

    				if($valuesRatings){
    					$dotV["ratings"]= $valuesRatings->ratings;
    				}

    				array_push($dotsValueArr, $dotV);

    				$count++;
    			}

    			$belief['belief_value'] = $dotsValueArr;

    			array_push($belief_arr, $belief);
    		}

    		$resultArray['belief'] = $belief_arr;

    		return response()->json(['code'=>200,'status'=>true,'service_name'=>'dot-detail','message'=>'DOT Detail','data'=>$resultArray]);

    	} 
    	else
    	{
    		return response()->json(['code'=>400,'status'=>false,'service_name'=>'dot-detail','message'=>'DOT Detail not found','data'=>$resultArray]);
    	}

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addDot()
    {

    	$vision   = Input::get('vision');
    	$mission  = Input::get('mission');
    	$focus    = Input::get('focus');
    	$orgId    = Input::get('orgId');
    	$dotDate  = Input::get('dot_date');
    	$introductory_information = Input::get('introductory_information');

    	$resultArray = array();

    	$status = DB::table('dots')
    	->where('orgId',$orgId)    
    	->first();

    	if($status){

    		return response()->json(['code'=>400,'status'=>false,'service_name'=>'add-dot-detail','message'=>'DOT already exists for this organisation','data'=>$resultArray]);

    	} else {

    		$dotInsertArray = array(
    			'vision'=> $vision,
    			'mission'=> $mission,
    			'focus'=> $focus,
    			'orgId'=> $orgId,
    			'introductory_information'=> $introductory_information,
    			'dot_date'=> date('Y-m-d H:i:s', strtotime($dotDate)),
    			'created_at'=> date('Y-m-d H:i:s')
         );

    		$dotId = DB::table('dots')->insertGetId($dotInsertArray);

    		if(!empty($dotId)){ 

    			$dotsBeliefs = Input::get('belief');

           //add dot beliefs
    			foreach($dotsBeliefs as $belief) {

    				$dotBeliefInsertArray = array(
    					'name'=> $belief['name'],
    					'dotId' => $dotId,
    					'created_at'=> date('Y-m-d H:i:s')
                   );

    				$dotBeliefId = DB::table('dots_beliefs')->insertGetId($dotBeliefInsertArray);

               //add beliefs values
    				foreach ($belief['belief_value'] as $belief_value)
    				{

    					$dotBeliefValueInsertArray = array(
    						'name'=> $belief_value['id'],
    						'beliefId'=> $dotBeliefId,
    						'created_at'=> date('Y-m-d H:i:s')
                      );

    					$dotBeliefValueId = DB::table('dots_values')->insertGetId($dotBeliefValueInsertArray);            

            }//dot belief value foreach

            }//dot belief foreach


            $dot_data = DB::table('dots')
            ->where('orgId',$orgId)
            ->latest()     
            ->first();

            if(!empty($dot_data)){  

         //dot data
            	$resultArray['id']      = $dot_data->id;
            	$resultArray['vision']  = $dot_data->vision;
            	$resultArray['mission'] = $dot_data->mission;
            	$resultArray['focus']   = $dot_data->focus;
            	$resultArray['orgId']   = $dot_data->orgId;
            	$resultArray['dot_date']= $dot_data->dot_date;
            	$resultArray['introductory_information']= $dot_data->introductory_information;

         //get beliefs data 
            	$beliefs_data = DB::table('dots_beliefs')
            	->where('dotId',$dot_data->id)     
            	->get();


            	$belief_arr = array();

            	foreach($beliefs_data as $value) {

            		$belief['id'] = $value->id;
            		$belief['name'] = $value->name;
            		$belief['dotId'] = $value->dotId;


            		$dots_value = DB::table('dots_values')
            		->where('beliefId',$value->id)  
            		->where('status','Active')   
            		->get();

            		$dotsValueArr = array();

            		$count = 0;
            		foreach ($dots_value as $Vvalue) {

            			$dotValueList = DB::table('dot_value_list')
            			->where('id',$Vvalue->name)     
            			->first();

            			$dValuesName = '';
            			if($dotValueList){

            				$dValuesName = $dotValueList->name;
            			}

            			$dotV["index"]     = $count;
            			$dotV["id"]        = $Vvalue->id;
            			$dotV["name"]      = $dValuesName;
            			$dotV["beliefId"]  = $Vvalue->beliefId;
            			$dotV["status"]    = $Vvalue->status;
            			$dotV["created_at"]= $Vvalue->created_at;
            			$dotV["updated_at"]= $Vvalue->updated_at;
            			$dotV['dotId']     = $value->dotId;

            			array_push($dotsValueArr, $dotV);
            			$count++;
            		}


            		$belief['belief_value'] = $dotsValueArr;

            		array_push($belief_arr, $belief);
            	}

            	$resultArray['belief'] = $belief_arr;
            }

            return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-dot-detail','message'=>'Dot added successfully','data'=>$resultArray]);

        } else {

        	return response()->json(['code'=>400,'status'=>false,'service_name'=>'add-dot-detail','message'=>'Dot not added','data'=>$resultArray]);
        }
    }
}

/*add evidence*/
public function addDotEvidence(Request $request)
{

	$postData = Input::all();

	$user = Auth::user();

	$resultArray = array();

	if(!empty($postData['file']))
	{               

		$image = $request->file('file');

		$fileName   = 'evidence_'.time().'.'.$image->getClientOriginalExtension();

		$destinationPath = public_path('/uploads/evidence_images/');

		$status = $image->move($destinationPath, $fileName);

	}else{

		$fileName='';
	}
	/*one field is required*/
	if(empty($postData['description']) && empty($fileName)){

		return response()->json(['code'=>400,'status'=>false,'service_name'=>'add-evidence','message'=>'One field required','data'=>$resultArray]);

	}

	$insertArray = array(
		'description'=> $postData['description'],
		'fileURL'    => $fileName,
		'userId'     => $user->id,
		'dotId'      => $postData['dotId'],
		'section'    => $postData['section'],
		'sectionId'  => $postData['sectionId'],
		'created_at' => date('Y-m-d H:i:s')
  );

	$status = DB::table('dot_evidence')->insertGetId($insertArray);      

	if($fileName){

		$resultArray['file_name'] = url('/public/uploads/evidence_images').'/'. $fileName;
	} 
	else
	{
		$resultArray['file_name'] ="";
	}     
	$resultArray['id'] =$status;
	$resultArray['description'] =$insertArray['description'];
	$resultArray['created_at'] =$insertArray['created_at'];

       //send notification mail Pooja Nenava added a new evidence for mission/vision/focus of Organisation name

	$user = Auth::user();

	$organisation = DB::table('organisations')->select('organisation')->where('id',$user->orgId)->first();

	$orgName ='';
	if($organisation){
		$orgName = $organisation->organisation;
	}

	$msg =  '<strong>'.ucfirst($user->name).'</strong>'.' added a new evidence for ' .$postData['section']. ' of Organisation: <strong>'.$orgName.'</strong>';

	
	$notificationArray = array('msg'=>$msg);

	$controller = app('App\Http\Controllers\Admin\CommonController')->sendNotificationMail($notificationArray);

	return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-evidence','message'=>'Evidence added successfully','data'=>$resultArray]);
}


public function updateDotEvidence(Request $request)
{

	$postData = Input::all();
      // print_r($postData);

      // die();

	$user = Auth::user();

	$resultArray = array();

	if(!empty($postData['file']))
	{               

		$image = $request->file('file');

		$fileName   = 'evidence_'.time().'.'.$image->getClientOriginalExtension();

		$destinationPath = public_path('/uploads/evidence_images/');

		$status = $image->move($destinationPath, $fileName);

	} 
	else
	{
		$fileName='';
	}
	/*one field is required*/
	if(empty($postData['description']) && empty($fileName)){

		return response()->json(['code'=>400,'status'=>false,'service_name'=>'add-evidence','message'=>'One field required','data'=>$resultArray]);

	}

	$insertArray = array(
		'description'=> $postData['description'],
		'fileURL'    => $fileName,
		'updated_at' => date('Y-m-d H:i:s')
  );
	
	

	DB::table('dot_evidence')
	->where('id',$postData['id'])
	->update($insertArray);     

	if($fileName){

		$resultArray['file_name'] = url('/public/uploads/evidence_images').'/'. $fileName;
	} 
	else
	{
		$resultArray['file_name'] ="";
	}    
	$resultArray['description'] =  $insertArray['description'];
	$resultArray['updated_at']  =  $insertArray['updated_at'];
	$resultArray['id']          =  $postData['id'];

	

	return response()->json(['code'=>200,'status'=>true,'service_name'=>'update-evidence','message'=>'Evidence updated successfully','data'=>$resultArray]);
}


/*get belief values list*/
public function getBeliefValuesList()
{

	$resultArray  = array();

	$customValues = DB::table('dot_value_list')
	->where('status','Active')
	->orderBy('name','ASC') 
	->get();


	foreach ($customValues as $value) {

		$result['id']   = $value->id;
		$result['name'] = $value->name;

		array_push($resultArray, $result);
	}

	if($resultArray){

		return response()->json(['code'=>200,'status'=>true,'service_name'=>'get-belief-values','message'=>'Belief values list','data'=>$resultArray]);

	} else {

		return response()->json(['code'=>400,'status'=>false,'service_name'=>'get-belief-values','message'=>'Belief values not found','data'=>$resultArray]);
	}
}


public function updateDot()
{

	$vision   = Input::get('vision');
	$mission  = Input::get('mission');
	$focus    = Input::get('focus');
	$orgId    = Input::get('orgId');
	$dotDate  = Input::get('dot_date');
	$dotId  = Input::get('dot_id');
	$introductory_information = Input::get('introductory_information');

	$resultArray = array();

	$dotInsertArray = array(
		'vision'=> $vision,
		'mission'=> $mission,
		'focus'=> $focus,
		'orgId'=> $orgId,
		'introductory_information'=> $introductory_information,
		'dot_date'=> date('Y-m-d H:i:s', strtotime($dotDate)),
		'updated_at'=> date('Y-m-d H:i:s')
  );


	DB::table('dots')
	->where('id', $dotId)
	->update($dotInsertArray);

	
	$dotsBeliefs = Input::get('belief');

           //add dot beliefs
	foreach($dotsBeliefs as $belief) {

		$dotBeliefInsertArray = array(
			'name'=> $belief['name'],
			'updated_at'=> date('Y-m-d H:i:s')
     );


		DB::table('dots_beliefs')
		->where('id', $belief['belief_id'])
		->update($dotBeliefInsertArray);

               //add beliefs values
		foreach ($belief['belief_value'] as $belief_value)
		{

			$dotBeliefValueInsertArray = array(
				'name'=> $belief_value['id'],

				'updated_at'=> date('Y-m-d H:i:s')
            );


			DB::table('dots_values')
			->where('id', $belief_value['value_id'])
			->update($dotBeliefValueInsertArray);            

            }//dot belief value foreach

            }//dot belief foreach


            $dot_data = DB::table('dots')
            ->where('orgId',$orgId)
            ->latest()     
            ->first();

            if(!empty($dot_data)){  

         //dot data
            	$resultArray['id']      = $dot_data->id;
            	$resultArray['vision']  = $dot_data->vision;
            	$resultArray['mission'] = $dot_data->mission;
            	$resultArray['focus']   = $dot_data->focus;
            	$resultArray['orgId']   = $dot_data->orgId;
            	$resultArray['dot_date']= $dot_data->dot_date;
            	$resultArray['introductory_information']= $dot_data->introductory_information;

         //get beliefs data 
            	$beliefs_data = DB::table('dots_beliefs')
            	->where('dotId',$dot_data->id)     
            	->get();


            	$belief_arr = array();

            	foreach($beliefs_data as $value) {

            		$belief['id'] = $value->id;
            		$belief['name'] = $value->name;
            		$belief['dotId'] = $value->dotId;


            		$dots_value = DB::table('dots_values')
            		->where('beliefId',$value->id)    
            		->where('status','Active') 
            		->get();

            		$dotsValueArr = array();

            		$count = 0;
            		foreach ($dots_value as $Vvalue) {

            			$dotValueList = DB::table('dot_value_list')
            			->where('id',$Vvalue->name)     
            			->first();

            			$dValuesName = '';
            			if($dotValueList){

            				$dValuesName = $dotValueList->name;
            			}

            			$dotV["index"]     = $count;
            			$dotV["id"]        = $Vvalue->id;
            			$dotV["name"]      = $dValuesName;
            			$dotV["beliefId"]  = $Vvalue->beliefId;
            			$dotV["status"]    = $Vvalue->status;
            			$dotV["created_at"]= $Vvalue->created_at;
            			$dotV["updated_at"]= $Vvalue->updated_at;
            			$dotV['dotId']     = $value->dotId;

            			array_push($dotsValueArr, $dotV);
            			$count++;
            		}


            		$belief['belief_value'] = $dotsValueArr;

            		array_push($belief_arr, $belief);
            	}

            	$resultArray['belief'] = $belief_arr;
            }

            return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-dot-detail','message'=>'Dot added successfully','data'=>$resultArray]);



        }


        public function addBelief()
        {

        	$dotBeliefInsertArray = array(
        		'name'=> Input::get('name'),
        		'dotId'=> Input::get('dotId'),
        		'created_at'=> date('Y-m-d H:i:s')
          );

        	$beliefId = DB::table('dots_beliefs')->insertGetId($dotBeliefInsertArray);

        	$beliefValue = Input::get('belief_value');

        	foreach ($beliefValue as  $value)
        	{

        		$dotValueInsertArray = array(
        			'name'       => $value['id'],
        			'beliefId'   => $beliefId,
        			'status'     =>'Active',
        			'created_at' => date('Y-m-d H:i:s')
             );

        		$Value = DB::table('dots_values')->insertGetId($dotValueInsertArray); 

        	}

        	$dotBeliefInsertArray['id'] = $beliefId;

        	if($beliefId)
        	{
        		return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-dot-belief','message'=>'Belief added successfully','data'=>$dotBeliefInsertArray]);
        	}  

        }




        public function addValue()
        {
        	$value = Input::get('values');

        	$resultArray = array();

        	foreach ($value as $value)
        	{

        		$dotValueInsertArray = array(
        			'name'=> $value['valueId'],
        			'beliefId'=> Input::get('beliefId'),
        			'created_at'=> date('Y-m-d H:i:s')
             );

        		$Value = DB::table('dots_values')->insertGetId($dotValueInsertArray);

        		$valueId = $value['valueId'];
        		$beliefId= Input::get('beliefId');

        		$dotValueList = DB::table('dot_value_list')
        		->where('id',$valueId)     
        		->first();

        		$dValuesName = '';
        		if($dotValueList){

        			$dValuesName = $dotValueList->name;
        		}

        		$dotV["id"]        = $Value;
        		$dotV["valueId"]   = $valueId;
        		$dotV["name"]      = $dValuesName;
        		$dotV["beliefId"]  = $beliefId;
        		$dotV["beliefName"]  =Input::get('beliefName');

        		array_push($resultArray, $dotV);

        	}

        	if($Value)
        	{
        		return response()->json(['code'=>200,'status'=>true,'service_name'=>'add-belief-values','message'=>'Values added successfully.','data'=>$resultArray]);
        	}  

        }

        /*delete dot values*/
        public function deleteDotValue()
        {

        	$resultArray  = array();

        	$dotValueId   = Input::get('id');

        	$updatetArray = array('status'=>'Inactive');

        	DB::table('dots_values')
        	->where('id',$dotValueId)
        	->update($updatetArray);  

        	return response()->json(['code'=>200,'status'=>true,'service_name'=>'delete-dot-value','message'=>'DOT value deleted successfully','data'=>$resultArray]);
        }

        /*get evidence list*/
        public function getEvidenceList()
        {

        	$resultArray = array();

        	$dotId      = Input::get('dotId');
        	$sectionId  = Input::get('sectionId');
        	$section    = Input::get('section');

        	$wrCls = array();

        	if($sectionId){
        		$wrCls['sectionId']= $sectionId;
        	}

        	$wrCls['section']= $section;
        	$wrCls['dotId']= $dotId;

        	$dotEvidence = DB::table('dot_evidence')
        	->where('status','Active')
        	->where($wrCls)      
        	->orderBy('id','DESC')
        	->get();

        	foreach ($dotEvidence as $evidence)
        	{
        		$evd['id']          = $evidence->id;
        		$evd['description'] = $evidence->description;
        		$evd['userId']      = $evidence->userId;
        		$evd['created_at']  = $evidence->created_at;
        		$evd['section']     = $evidence->section;
        		$evd['fileURL']     = '';

        		if($evidence->fileURL)
        		{
        			$evd['fileURL']     = url('/public/uploads/evidence_images').'/'. $evidence->fileURL;
        		}

        		array_push($resultArray, $evd);

        	}

        	if($resultArray)
        	{

        		return response()->json(['code'=>200,'status'=>true,'service_name'=>'get-evidence-list','message'=>'Evidence list.','data'=>$resultArray]);
        	}

        	return response()->json(['code'=>400,'status'=>false,'service_name'=>'get-evidence-list','message'=>'Evidence not found.','data'=>$resultArray]);

        }

        public function updateBelief()
        {

        	$dotInsertArray = array(
        		'name'=> Input::get('name')
        		
          );
        	
        	DB::table('dots_beliefs')
        	->where('id', Input::get('id'))
        	->update($dotInsertArray);

        	return response()->json(['code'=>200,'status'=>true,'service_name'=>'update-belief','message'=>'Belief updated successfully','data'=>$dotInsertArray]); 

        }

        public function deleteBelief()
        {
        	$beliefId = Input::get('id');
        	

        	DB::table('dots_beliefs')
        	->where('id',$beliefId) 
        	->delete();  

        	DB::table('dots_values')
        	->where('beliefId',$beliefId) 
        	->delete();  

        	return response()->json(['code'=>200,'status'=>true,'service_name'=>'delete-belief','message'=>'Belief deleted successfully','data'=>""]); 


        }

      //add bubble rating to the user
        public function addbubbleRatings()
        {
        	$resultArray = array();
        	$user = Auth::user();

        	$toUserIdArray      = Input::get('toUserId'); 
        	$fromUserId         = $user->id;
        	$dotId              = Input::get('dotId');
        	$dotBeliefId        = Input::get('dotBeliefId');
        	$dotValueId         = Input::get('dotValueId');
        	$dotValueNameId     = Input::get('dotValueNameId');
        	$bubbleFlag         = Input::get('bubbleFlag');

        	foreach($toUserIdArray as $key => $ids)
        	{

        		$insertArray = array(
        			'to_bubble_user_id'  => $ids,
        			'from_bubble_user_id'=> $fromUserId,
        			'dot_id'             => $dotId,
        			'dot_belief_id'      => $dotBeliefId,
        			'dot_value_id'       => $dotValueId,
        			'dot_value_name_id'  => $dotValueNameId,
        			'bubble_flag'        => $bubbleFlag,
        			'status'             => 'Active',
        			'created_at'         => date('Y-m-d H:i:s')
             );

        		DB::table('dot_bubble_rating_records')->insertGetId($insertArray);


        		$user = DB::table('users')->select('fcmToken')->where('id', $ids)->where('status','Active')->first();

        		$value = DB::table('dot_value_list')->select('name')->where('id', $dotValueNameId)->first();

        		$dotValueName = '';
        		if($value)
        		{
        			$dotValueName = $value->name;
        		}

        		$fcmToken = '';
        		if($user)
        		{
        			$fcmToken = $user->fcmToken;
        		}

        		$title    = 'Thumbs Up: '.$dotValueName;
        		$message  = 'You have received a thumbs up in '.$dotValueName;
          //get badge count 
        		$totbadge = $this->getIotNotificationBadgeCount(array('userId'=>$ids));

        		$test = app('App\Http\Controllers\API\ApiCommonController')->sendFcmNotify($fcmToken, $title, $message, $totbadge);

        		$notificationArray = array(
        			'to_bubble_user_id'  => $ids,
        			'from_bubble_user_id'=> $fromUserId,
        			"title" => $title,
        			"description" => $message,            
        			'dot_value_id' => $dotValueId,
        			'dot_value_name_id' => $dotValueNameId,
        			'notificationType' => 'thumbsup',
        			"created_at" => date('Y-m-d H:i:s')
             );

        		DB::table('iot_notifications')->insertGetId($notificationArray);

        	}

        	return response()->json(['code'=>200,'status'=>true,'service_name'=>'DOT-add-bubble-ratings','message'=>'Bubble ratings provided successfully.','data'=>$resultArray]);
        	
        }

        /*get bubble rating list*/
        public function getBubbleRatingList(Request $request)
        {

        	$resultArray = array();
        	$beliefArr   = array();

        	$orgId      = $request->orgId;
        	$year       = $request->year;
        	$month      = $request->month;
        	$userId     = $request->userId;

        	$dots = DB::table('dots')->select('id')->where('orgId',$orgId)->first();

        	if(empty($dots->id))
        	{

        		if($request->reportStatus)
        		{
        			return $beliefArr;
        		}

        		return response()->json(['code'=>400,'status'=>false,'service_name'=>'DOT-bubble-ratings-list','message'=>'DOT not available.','data'=>$resultArray]);
        	}

        	$dotId = $dots->id;

        	$beliefsData = DB::table('dots_beliefs')->where('dotId',$dotId)->get();

        	
        	foreach($beliefsData as $bValue)
        	{

        		$belief['id']   = $bValue->id;
        		$belief['name'] = ucfirst($bValue->name);
        		
        		$dotsValue = DB::table('dots_values')->where('beliefId',$bValue->id)->where('status','Active')->get();

        		$dotsValueArr = array();

        		$count = 0;
        		foreach ($dotsValue as $Vvalue)
        		{

        			$dotValueList = DB::table('dot_value_list')->where('id',$Vvalue->name)->first();

        			$upVotTblQuery = DB::table('dot_bubble_rating_records')            
        			->where('dot_belief_id',$bValue->id)
        			->where('dot_value_name_id',$Vvalue->name)
        			->where('bubble_flag',"1")           
        			->whereYear('created_at', '=', $year)
        			->whereMonth('created_at', '=', $month);
        			if($userId)
        			{
        				$upVotTblQuery->where('to_bubble_user_id',$userId);
        			}

        			$upVotTbl = $upVotTblQuery->get();
        			
        			$dValuesName = '';
        			if($dotValueList)
        			{
        				$dValuesName = $dotValueList->name;
        			}

        			$dotV["index"]     = $count;
        			$dotV["id"]        = $Vvalue->id;
        			$dotV['name']      = $dValuesName;          
        			$dotV['upVotes']   = count($upVotTbl);

        			$valuesRatingsQuery = db::table('dot_values_ratings')
        			->where('valueId', $Vvalue->id);            
        			if($userId) 
        			{
        				$valuesRatingsQuery->where('userId',$userId);
        			}            

        			$valuesRatings = $valuesRatingsQuery->first();

        			$dotV["ratings"]='';

        			if($valuesRatings)
        			{
        				$dotV["ratings"]= $valuesRatings->ratings;
        			}

        			array_push($dotsValueArr, $dotV);

        			$count++;
        		}

        		$belief['beliefValueArr'] = $dotsValueArr;

        		array_push($beliefArr, $belief);
        	}

        	if($request->reportStatus)
        	{
        		return $beliefArr;
        	}


        	return response()->json(['code'=>200,'status'=>true,'service_name'=>'DOT-bubble-ratings-list','message'=>'Bubble ratings.','data'=>$beliefArr]);


        }

        /*get dot from user ratings*/
        public function getBubbleFromUserRating()
        {

        	$resultArray = array();

        	$user = Auth::user();

        	$fromUserId     = $user->id;
        	$toUserId       = Input::get('toUserId');
        	$dotBeliefId    = Input::get('dotBeliefId');
        	$dotValueNameId = Input::get('dotValueNameId');

        	$upVotTbl = DB::table('dot_bubble_rating_records')
        	->where('to_bubble_user_id',$toUserId)
        	->where('from_bubble_user_id',$fromUserId)->where('dot_belief_id',$dotBeliefId)
        	->where('dot_value_name_id',$dotValueNameId)->where('bubble_flag',"0")
        	->get();

        	$downVotTbl = DB::table('dot_bubble_rating_records')
        	->where('to_bubble_user_id',$toUserId)
        	->where('from_bubble_user_id',$fromUserId)->where('dot_belief_id',$dotBeliefId)
        	->where('dot_value_name_id',$dotValueNameId)->where('bubble_flag',"1")
        	->get();

        	$resultArray['upVotes']   = count($upVotTbl);
        	$resultArray['downVotes'] = count($downVotTbl);

        	return response()->json(['code'=>200,'status'=>true,'service_name'=>'DOT-bubble-from-user-ratings','message'=>'DOT bubble from user ratings.','data'=>$resultArray]);

        }

        /*get dot report*/
        public function getDOTreportGraph()
        {

        	$orgId = Input::get('orgId');

        	$dots = DB::table('dots')->where('orgId',$orgId)->first();

        	$dotValuesArray = array();
        	
        	if(!empty($dots))
        	{

        		$dotBeliefs = DB::table('dots_beliefs')
        		->where('status','Active')
        		->where('dotId',$dots->id)        
        		->orderBy('id','ASC')->get();
        		
        		foreach ($dotBeliefs as $key => $bValue)
        		{

        			$dotValues = DB::table('dots_values')
        			->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
        			->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
        			->where('dots_values.status','Active')
        			->where('dots_values.beliefId',$bValue->id)
        			->orderBy('dot_value_list.id','ASC')
        			->get();

        			$bRatings = DB::table('dot_values_ratings')
        			->leftjoin('users','users.id','=','dot_values_ratings.userId')
        			->where('users.status','Active') 
        			->where('beliefId', $bValue->id)->avg('ratings');

        			$valuesArray = array();
        			foreach ($dotValues as $key => $vValue) 
        			{
        				$vRatings = DB::table('dot_values_ratings')
        				->leftjoin('users','users.id','=','dot_values_ratings.userId')
        				->where('users.status','Active')
        				->where('valueId', $vValue->id)->avg('ratings');

        				$vResult['valueId']      = $vValue->id;
        				$vResult['valueName']    = ucfirst($vValue->name);

        				$vResult['valueRatings'] = 0;
        				if($vRatings)
        				{
                $vResult['valueRatings'] = round(($vRatings-1), 2); //number_format("1000000",2)
            }
            array_push($valuesArray, $vResult);
        }

        $result['beliefId']      = $bValue->id;
        $result['beliefName']    = ucfirst($bValue->name);           

        $result['beliefRatings'] = 0;
        if($bRatings)
        {
        	$result['beliefRatings'] = number_format(($bRatings-1), 2);
        }

        $result['beliefValues']  = $valuesArray;

        array_push($dotValuesArray, $result);
    }
}

return response()->json(['code'=>200,'status'=>true,'service_name'=>'DOT-report','message'=>'','data'=>$dotValuesArray]);
}



/*get dot valuse list*/
public function getDOTValuesList()
{

	$resultArray = array();

	$user = Auth::user();

	$orgId = Input::get('orgId');

	$dot_data = DB::table('dots')->where('orgId',$orgId)->latest()->first();

	$result = array();

	if(!empty($dot_data))
	{  

         //get beliefs data 
		$beliefs_data = DB::table('dots_beliefs')->where('dotId',$dot_data->id)->get();

		$belief_arr = array();

		foreach($beliefs_data as $value)
		{

			$belief['id'] = $value->id;
			$belief['name'] = $value->name;
			$belief['dotId'] = $value->dotId;

			$dots_value = DB::table('dots_values')->where('beliefId',$value->id)->where('status','Active')->get();

			$dotsValueArr = array();

			$count = 0;
			foreach($dots_value as $Vvalue)
			{

				$dotValueList = DB::table('dot_value_list')->where('id',$Vvalue->name)->first();

				$dValuesName = '';
				if($dotValueList)
				{
					$dValuesName = $dotValueList->name;
				}

				$dotV["index"]     = $count;
				$dotV["id"]        = $Vvalue->id;
				$dotV['name']      = $dValuesName;
				$dotV["valueId"]   = $Vvalue->name;
				$dotV["beliefId"]  = $Vvalue->beliefId;
				$dotV["status"]    = $Vvalue->status;
				$dotV["created_at"]= $Vvalue->created_at;
				$dotV["updated_at"]= (string)$Vvalue->updated_at;
				$dotV['dotId']     = $value->dotId;

				$valuesRatings = db::table('dot_values_ratings')->where('valueId', $Vvalue->id)->where('userId',$user->id)->first();

				$dotV["ratings"]='';

				if($valuesRatings){
					$dotV["ratings"]= $valuesRatings->ratings;
				}

				array_push($dotsValueArr, $dotV);

				$count++;
			}

			$belief['belief_value'] = $dotsValueArr;

			array_push($belief_arr, $belief);
		}

		$resultArray['belief'] = $belief_arr;

		return response()->json(['code'=>200,'status'=>true,'service_name'=>'dot-value-list','message'=>'DOT value list','data'=>$resultArray]);

	} 
	else
	{
		return response()->json(['code'=>400,'status'=>false,'service_name'=>'dot-value-list','message'=>'DOT values not found','data'=>$resultArray]);
	}

}


/*DOT bubble report by multiple department and values*/
public function addDOTBubbleRatingsToMultiDepartment()
{


	$resultArray = array();
	$user  = Auth::user();
	$orgId = $user->orgId;

	$toUserIdArray = array();

	$toUserIdArray      = Input::get('toUserId'); 
	$fromUserId         = $user->id;
	$dotId              = Input::get('dotId');
	$bubbleFlag         = Input::get('bubbleFlag');

	$departmentIdArray  = Input::get('departmentIdArray');

	$options = Input::get('options');
	
	foreach ($departmentIdArray as $deptId)
	{

		$users = DB::table('users')->select('id')                 
		->where('departmentId', $deptId)
		->where('orgId', $orgId)->where('status', 'Active')->get();

		foreach($users as $uValue)
		{
			if($user->id != $uValue->id)
			{
				array_push($toUserIdArray, $uValue->id);
			}            
		}
	}


	foreach($toUserIdArray as $key => $ids)
	{

		foreach ($options as $oValue)
		{

			$insertArray = array(
				'to_bubble_user_id'  => $ids,
				'from_bubble_user_id'=> $fromUserId,
				'dot_id'             => $dotId,
				'dot_belief_id'      => $oValue['dotBeliefId'],
				'dot_value_id'       => $oValue['dotValueId'],
				'dot_value_name_id'  => $oValue['dotValueNameId'],
				'bubble_flag'        => $bubbleFlag,
				'status'             => 'Active',
				'created_at'         => date('Y-m-d H:i:s')
            );

			DB::table('dot_bubble_rating_records')->insertGetId($insertArray);


			$user = DB::table('users')->select('fcmToken')->where('id', $ids)->where('status','Active')->first();

			$value = DB::table('dot_value_list')->select('name')->where('id', $oValue['dotValueNameId'])->first();

			$dotValueName = '';
			if($value)
			{
				$dotValueName = $value->name;
			}

			$fcmToken = '';
			if($user)
			{
				$fcmToken = $user->fcmToken;
			}

			$title    = 'Thumbs Up: '.$dotValueName;
			$message  = 'You have received a thumbs up in '.$dotValueName;
            // $totbadge = 1;

			$totbadge = $this->getIotNotificationBadgeCount(array('userId'=>$ids));
			
			$test = app('App\Http\Controllers\API\ApiCommonController')->sendFcmNotify($fcmToken, $title, $message, $totbadge);


			$notificationArray = array(
				'to_bubble_user_id'  => $ids,
				'from_bubble_user_id'=> $fromUserId,
				"title" => $title,
				"description" => $message,            
				'dot_value_id' => $oValue['dotValueId'],
				'dot_value_name_id' => $oValue['dotValueNameId'],
				'notificationType' => 'thumbsup',
				"created_at" => date('Y-m-d H:i:s')
            );

			DB::table('iot_notifications')->insertGetId($notificationArray);

		}

	}

	return response()->json(['code'=>200,'status'=>true,'service_name'=>'DOT-add-bubble-ratings','message'=>'Bubble ratings provided successfully.','data'=>$resultArray]);

}

/*get DOT bubble rating notification list*/
public function getBubbleRatingNotificationList()
{

	$resultArray = array();

	$userId         = Input::get('userId');
	$page           = Input::get('page');

	$recordLimit    = 10;
	$pageCount      = $page - 1; 
	$offset         = $recordLimit * $pageCount;
	$totalPageCount = 0;

	$wrclaue = array();
	
	$wrclaue['status']             = 'Active';
	$wrclaue['to_bubble_user_id']  = $userId;
	$wrclaue['isMsgRead']          = "1";
	
	$pageCount = app('App\Http\Controllers\API\ApiCommonController')->getPageCountQuery('iot_notifications', $wrclaue);   

	$totalPageCount = app('App\Http\Controllers\API\ApiCommonController')->getPageCount($pageCount,$recordLimit);

	if($page)
	{
		$bubbleNotifTbl = DB::table('iot_notifications')
		->where($wrclaue)
		->offset($offset)
		->limit($recordLimit)
		->orderBy('id','DESC')
		->get();
	}
	else
	{
		$bubbleNotifTbl = DB::table('iot_notifications')
		->where($wrclaue)        
		->orderBy('id','DESC')
		->get();
	}
	

	foreach($bubbleNotifTbl as $value)
	{
		$result["id"]               = $value->id;
		$result["title"]            = (string)$value->title;
		$result["description"]      = (string)$value->description;  
		$result["feedbackId"]       = (string)$value->feedbackId; 
		$result["notificationType"] = (string)$value->notificationType;      
		$result["created_at"]       = (string)$value->created_at;
		$result['lastMessage']      = '';
		$result['file']             = false;

		if($value->isMsgRead==0)
		{
			$result['isRead'] = false;
		}
		else
		{
			$result['isRead'] = true;
		}

		if($value->feedbackId)
		{
			$feedback = DB::table('iot_feedbacks')->select('message')->where('id',$value->feedbackId)->first();

			if($feedback)
			{
				$result["title"]  = $feedback->message;
			}
		}

		array_push($resultArray, $result);
	}

        // DB::table('iot_notifications')->where('to_bubble_user_id', $userId)->update(['isMsgRead'=>'1']);


	return response()->json(['code'=>200, 'status'=>true,'service_name'=>'DOT-bubble-ratings-notification','message'=>'','totalPageCount'=>$totalPageCount,'currentPage'=>$page,'data'=>$resultArray]);

}

/*get DOT bubble rating un read notification list*/
public function getBubbleRatingUnReadNotificationList()
{

	$resultArray = array();

	$userId         = Input::get('userId');

	$bubbleNotifTbl = DB::table('iot_notifications')
	->where('status', 'Active')
	->where('to_bubble_user_id', $userId)
	->where('isMsgRead',"0")
	->orderBy('id','DESC')
	->get();

	foreach($bubbleNotifTbl as $value)
	{
		$result["id"]               = $value->id;
		$result["title"]            = (string)$value->title;
		$result["description"]      = (string)$value->description;  
		$result["feedbackId"]       = (string)$value->feedbackId; 
		$result["notificationType"] = (string)$value->notificationType;      
		$result["created_at"]       = (string)$value->created_at;
		$result['lastMessage']      = '';
		$result['file']             = false;

		if($value->isMsgRead==0)
		{
			$result['isRead'] = false;
		}
		else
		{
			$result['isRead'] = true;
		}

		if($value->feedbackId)
		{
			$feedback = DB::table('iot_feedbacks')->select('message')->where('id',$value->feedbackId)->first();

			if($feedback)
			{
				$result["title"]  = $feedback->message;
			}
		}

		array_push($resultArray, $result);
	}

	$ansStatus   = array(); 
	$toDoList    = array();

	$month  = date('m');
	$year   = date('Y');
	$day    = date('d');

	$statusKey = app('App\Http\Controllers\API\ApiChecklistController')->getVar('dot_values_ratings');

	$ansStatus[$statusKey]= app('App\Http\Controllers\API\ApiChecklistController')->isDotValuecompleted($userId);

	$dataTableArr = array('dot_values_ratings','cot_functional_lens_answers','cot_answers','sot_answers','sot_motivation_answers','tribeometer_answers','diagnostic_answers');

	foreach($dataTableArr as $value)
	{

		$tableStatus = DB::table($value)->where('userId',$userId)->first(); 

		if($value !='dot_values_ratings')
		{
			$statusKey = app('App\Http\Controllers\API\ApiChecklistController')->getVar($value);

			if(!empty($tableStatus))
			{     
				$ansStatus[$statusKey]= true;
			}
			else
			{
				$ansStatus[$statusKey]= false;
			}
		}

          //Get month wise status
		$queryDot = DB::table($value)->where('userId',$userId); 
		$queryDot->whereYear('updated_at','=', $year);
		$queryDot->whereMonth('updated_at','=', $month);
		$dotvaluesMonthsWise = $queryDot->first();

		if (empty($dotvaluesMonthsWise))
		{
			$queryDot = DB::table($value)->where('userId',$userId); 
			$queryDot->whereYear('created_at','=', $year);
			$queryDot->whereMonth('created_at','=', $month);
			$dotvaluesMonthsWise = $queryDot->first();
		}   

		if(!empty($dotvaluesMonthsWise))
		{       
			$toDoList[$statusKey] = true;
		}
		else
		{
			$toDoList[$statusKey] = false;
		} 
	}    



       //tribe values
	$querybubble = DB::table('dot_bubble_rating_records')->where('from_bubble_user_id',$userId); 
	$querybubble->whereDate('updated_at','=', date('Y-m-d'));   
	$bubbleRatings = $querybubble->first();

	if (empty($bubbleRatings))
	{
		$querybubble = DB::table('dot_bubble_rating_records')->where('from_bubble_user_id',$userId);
		$querybubble->whereDate('created_at','=', date('Y-m-d'));
		$bubbleRatings = $querybubble->first();
	} 

	if(!empty($bubbleRatings))
	{
		$toDoList['bubbleRatings'] = true;
	}else{
		$toDoList['bubbleRatings'] = false;
	} 

	return response()->json(['code'=>200, 'status'=>true, 'service_name'=>'DOT-bubble-ratings-unread-notification', 'message'=>'', 'data'=>$resultArray, 'ansStatus'=>$ansStatus, 'toDoList'=>$toDoList]);

}

/*get un read notification count*/
public function getBubbleUnReadNotifications()
{

	$resultArray = array();

	$userId = Input::get('userId');
        //orWhere clause not woring here
	$bubbleNotifTblthumbsup = DB::table('iot_notifications')
	->where('isMsgRead','0')
	->where('status','Active')       
	->where('notificationType','thumbsup')
	->where('to_bubble_user_id',$userId)
	->count();

	$bubbleNotifTblchat = DB::table('iot_notifications')
	->where('isMsgRead','0')
	->where('status','Active')
	->Where('notificationType','chat') 
	->where('to_bubble_user_id',$userId)
	->count();

	

    $ansStatus   = array(); 
    $toDoList    = array();

    $month  = date('m');
    $year   = date('Y');
    $day    = date('d');

    $ansStatusCount = 0;
    $toDoListcount = 0;

    $statusKey = app('App\Http\Controllers\API\ApiChecklistController')->getVar('dot_values_ratings');

    $ansStatus[$statusKey]= app('App\Http\Controllers\API\ApiChecklistController')->isDotValuecompleted($userId);
    if (empty($ansStatus[$statusKey])) {
        $ansStatusCount += 1;
    }

    $dataTableArr = array('dot_values_ratings','cot_functional_lens_answers','cot_answers','sot_answers','sot_motivation_answers','tribeometer_answers','diagnostic_answers');

    foreach($dataTableArr as $value)
    {

        $tableStatus = DB::table($value)->where('userId',$userId)->first(); 

        if($value !='dot_values_ratings')
        {
            $statusKey = app('App\Http\Controllers\API\ApiChecklistController')->getVar($value);

            if(!empty($tableStatus))
            {     
                $ansStatus[$statusKey]= true;
            }
            else
            {
                $ansStatus[$statusKey]= false;
                    $ansStatusCount += 1;
            }
        }

          //Get month wise status
        $queryDot = DB::table($value)->where('userId',$userId); 
        $queryDot->whereYear('updated_at','=', $year);
        $queryDot->whereMonth('updated_at','=', $month);
        $dotvaluesMonthsWise = $queryDot->first();

        if (empty($dotvaluesMonthsWise))
        {
            $queryDot = DB::table($value)->where('userId',$userId); 
            $queryDot->whereYear('created_at','=', $year);
            $queryDot->whereMonth('created_at','=', $month);
            $dotvaluesMonthsWise = $queryDot->first();
        }   

        if(!empty($dotvaluesMonthsWise))
        {       
            $toDoList[$statusKey] = true;
        }
        else
        {
            $toDoList[$statusKey] = false;
            if ($ansStatusCount<1) {
                $toDoListcount += 1;
            }
        }
    }   
    //tribe values
    $querybubble = DB::table('dot_bubble_rating_records')->where('from_bubble_user_id',$userId); 
    $querybubble->whereDate('updated_at','=', date('Y-m-d'));   
    $bubbleRatings = $querybubble->first();

    if (empty($bubbleRatings))
    {
        $querybubble = DB::table('dot_bubble_rating_records')->where('from_bubble_user_id',$userId);
        $querybubble->whereDate('created_at','=', date('Y-m-d'));
        $bubbleRatings = $querybubble->first();
    } 

    if(!empty($bubbleRatings))
    {
        $toDoList['bubbleRatings'] = true;
    }else{
        $toDoList['bubbleRatings'] = false;
        if ($ansStatusCount<1) {
            $toDoListcount += 1;
        }
    }   
    
    if ($ansStatusCount>0) {
        $toDoListcount = 0;
    }
    $resultArray['notificationCount'] = ($bubbleNotifTblthumbsup + $bubbleNotifTblchat + $ansStatusCount + $toDoListcount);

	return response()->json(['code'=>200, 'status'=>true,'service_name'=>'DOT-bubble-ratings-notification','message'=>'','data'=>$resultArray,'ansStatus'=>$ansStatus,'toDoList'=>$toDoList]);

}


/*get badge notiication count*/
public function getIotNotificationBadgeCount($arr)
{

	$userId = $arr['userId'];

	$resultArray = array();
	$ansStatus   = array(); 
	$toDoList    = array();

	$month  = date('m');
	$year   = date('Y');
	$day    = date('d');

	$statusKey = app('App\Http\Controllers\API\ApiChecklistController')->getVar('dot_values_ratings');

	$ansStatus[$statusKey]= app('App\Http\Controllers\API\ApiChecklistController')->isDotValuecompleted($userId);

	$dataTableArr = array('dot_values_ratings','cot_functional_lens_answers','cot_answers','sot_answers','sot_motivation_answers','tribeometer_answers','diagnostic_answers');

	foreach($dataTableArr as $value)
	{

		$tableStatus = DB::table($value)->where('userId',$userId)->first(); 

		if($value !='dot_values_ratings')
		{
			$statusKey = app('App\Http\Controllers\API\ApiChecklistController')->getVar($value);

			if(!empty($tableStatus))
			{     
				$ansStatus[$statusKey]= true;
			}
			else
			{
				$ansStatus[$statusKey]= false;
			}
		}

          //Get month wise status
		$queryDot = DB::table($value)->where('userId',$userId); 
		$queryDot->whereYear('updated_at','=', $year);
		$queryDot->whereMonth('updated_at','=', $month);
		$dotvaluesMonthsWise = $queryDot->first();

		if (empty($dotvaluesMonthsWise))
		{
			$queryDot = DB::table($value)->where('userId',$userId); 
			$queryDot->whereYear('created_at','=', $year);
			$queryDot->whereMonth('created_at','=', $month);
			$dotvaluesMonthsWise = $queryDot->first();
		}   

		if(!empty($dotvaluesMonthsWise))
		{       
			$toDoList[$statusKey] = true;
		}
		else
		{
			$toDoList[$statusKey] = false;
		} 
	}    



       //tribe values
	$querybubble = DB::table('dot_bubble_rating_records')->where('from_bubble_user_id',$userId); 
	$querybubble->whereDate('updated_at','=', date('Y-m-d'));   
	$bubbleRatings = $querybubble->first();

	if (empty($bubbleRatings))
	{
		$querybubble = DB::table('dot_bubble_rating_records')->where('from_bubble_user_id',$userId);
		$querybubble->whereDate('created_at','=', date('Y-m-d'));
		$bubbleRatings = $querybubble->first();
	} 

	if(!empty($bubbleRatings))
	{
		$toDoList['bubbleRatings'] = true;
	}else{
		$toDoList['bubbleRatings'] = false;
	}

        //check false count of users answers status
	$counter = 0;
	foreach ($ansStatus as $key=> $value)
	{
		if($value===false)
		{
			$counter++;
		}          
	} 

       //if all the answers are filled then check in to do lists pending status
	if($counter==0)
	{
		foreach ($toDoList as $key => $value)
		{
			if($value===false)
			{
				$counter++;
			}
		}
	}

        //unread count of notification
	$bubbleNotifTbl = DB::table('iot_notifications')->where('isMsgRead','0')->where('status','Active')->where('notificationType','thumbsup')->orWhere('notificationType','chat')->where('to_bubble_user_id',$userId)->count();

	$totalNotification = (count($bubbleNotifTbl)+$counter+1);

	return $totalNotification;
}
/*change status of the unread notification*/
public function changeNotificationStatus()
{
	$resultArray = array();
	$notificationId = Input::get('notificationId');

	$updatetArray['isMsgRead'] = "1";

	DB::table('iot_notifications')->where('id',$notificationId)->update($updatetArray);

	return response()->json(['code'=>200, 'status'=>true,'service_name'=>'DOT-bubble-ratings-notification-status-change','message'=>'','data'=>$resultArray]);
}

/*change status of the unread notification*/
public function readAllNotification()
{
    $resultArray   = array();
    $userId        = Input::get('userId');
    $updatetArray['isMsgRead'] = "1";
    DB::table('iot_notifications')->where('to_bubble_user_id',$userId)->update($updatetArray);
    return response()->json(['code'=>200, 'status'=>true,'service_name'=>'DOT-bubble-ratings-notification-status-change','message'=>'','data'=>$resultArray]);
}

/*add happy index detail*/
public function addHappyIndex()
{
	$resultArray = array();

	$userId    = Input::get('userId');
	$moodValue = Input::get('moodStatus');

	$happyIndexInsertArray = array(
		'userId'=> $userId,
		'moodValue'=> $moodValue,
		'created_at'=> date('Y-m-d H:i:s'),
  );

	$happyIndexId = DB::table('happy_indexes')->insertGetId($happyIndexInsertArray);

	return response()->json(['code'=>200, 'status'=>true,'service_name'=>'dashboard-add-happy-index','message'=>'','data'=>$resultArray]);
}

/*get dashboard detail*/

public function getDashboardDetail()
{

    $resultArray = array();

    $orgId = Input::get('orgId');

    $dot = DB::table('dots')->where('orgId',$orgId)->first();

    $vision = '';
    if($dot)
    {
        $vision = $dot->vision;
    }

    $resultArray['vision'] = $vision;

    //get tribeometer detail
    $queCatTbl = DB::table('tribeometer_questions_category')->get();

    $users = DB::table('tribeometer_answers')
    ->leftjoin('users','users.id','tribeometer_answers.userId')
    ->select('tribeometer_answers.userId')
    ->where('users.status','Active')
    ->groupBy('tribeometer_answers.userId')
    ->where('users.orgId',$orgId)->get();

    $userCount = count($users);

    $optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
    $optCount = count($optionsTbl)-1;

    $tribeoResultArray = array();
    foreach ($queCatTbl as $value)
    {
        $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();

        $quecount = count($questionTbl);

        $perQuePercen = 0;
        foreach ($questionTbl as  $queValue)
        {
            $diaAnsTbl = DB::table('tribeometer_answers')     
            ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
            ->leftJoin('users','users.id','tribeometer_answers.userId')
            ->where('users.status','Active')
            ->where('tribeometer_answers.orgId',$orgId)
            ->where('tribeometer_questions.id',$queValue->id)
            ->where('tribeometer_questions.category_id',$value->id)
            ->sum('answer');

        //avg of all questions
            if ($userCount !=0)
            {
                $perQuePercen += ($diaAnsTbl/$userCount); 
            }
            
        }

        $score = ($perQuePercen/($quecount*$optCount));
        $totalPercentage = ($perQuePercen/($quecount*$optCount))*100;

        $value1['title']      =  $value->title;       
        $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');      

        array_push($tribeoResultArray, $value1);
    }

    $resultArray['tribeometerDetail'] = $tribeoResultArray;


 $user = Auth::user();

 $userId = $user->id;

 $isUser = DB::table('happy_indexes')->where('userId',$userId)->where('status','Active')->whereDate('created_at', date('Y-m-d'))->first();

 $weekDay = date('w', strtotime(date('Y-m-d')));

 $resultArray['userGivenfeedback'] = false;
//  if($isUser) 
//  {
//     $resultArray['userGivenfeedback'] = true;
// }
 if($isUser || $weekDay == 0 || $weekDay == 6) 
 {
    $resultArray['userGivenfeedback'] = true;
 }

/*get previous day happy index detail*/
$userTypeArr = array('3','2','1');

$totalIndexes = 0;
$totalUser    = 0;
foreach($userTypeArr as $type)
{
  $userCount = DB::table('happy_indexes')
  ->leftjoin('users','users.id','happy_indexes.userId')
  ->where('happy_indexes.status','Active')
  ->where('users.status','Active')
  ->where('happy_indexes.moodValue',$type)
  ->where('users.orgId',$orgId)        
  ->whereDate('happy_indexes.created_at', date('Y-m-d', strtotime('-1 days')))
  ->count();
  $totalUser +=$userCount;
  if($type==3) 
  {
     $totalIndexes += $userCount;
 }       
}


$happyIndexPer = 0;
if($totalIndexes !=0 && $totalUser !=0)
//if($totalUser !=0)
{ 
  $happyIndexPer = (($totalIndexes/($totalUser))*100);
}

$resultArray['happyIndex'] = round($happyIndexPer,2);

//get yesterday's previous day happy index
$totalIndexesOfyesterdaysPreviousday = 0;
$totalUserOfyesterdaysPreviousday    = 0;
$totalHappyUserYesterday = 0;
$totalIndexesOfyesterday = 0;
foreach($userTypeArr as $type)
{

    $userCount = DB::table('happy_indexes')
    ->leftjoin('users','users.id','happy_indexes.userId')
    ->where('happy_indexes.status','Active')
    ->where('users.status','Active')
    ->where('happy_indexes.moodValue',$type)
    ->where('users.orgId',$orgId)        
    ->whereDate('happy_indexes.created_at', date('Y-m-d', strtotime('-2 days')))
    ->count();

    $totalUserOfyesterdaysPreviousday +=$userCount;

    $yesterdayUserCount = DB::table('happy_indexes')
    ->leftjoin('users','users.id','happy_indexes.userId')
    ->where('happy_indexes.status','Active')
    ->where('users.status','Active')
    ->where('happy_indexes.moodValue',$type)
    ->where('users.orgId',$orgId)        
    ->whereDate('happy_indexes.created_at', date('Y-m-d', strtotime('-1 days')))
    ->count();

    $totalHappyUserYesterday +=$yesterdayUserCount;

    if($type==3) 
    {
        $totalIndexesOfyesterdaysPreviousday += $userCount;
        
        $totalIndexesOfyesterday += $yesterdayUserCount;
    }       
}

$happyIndexPerOfyesterdaysPreviousday = 0;
if($totalIndexesOfyesterdaysPreviousday !=0 && $totalUserOfyesterdaysPreviousday !=0)
{
    $happyIndexPerOfyesterdaysPreviousday = ($totalIndexesOfyesterdaysPreviousday/($totalUserOfyesterdaysPreviousday));
}

$happyIndexPerOfyesterday = 0;
if($totalIndexesOfyesterday !=0 && $totalHappyUserYesterday !=0)
{
    $happyIndexPerOfyesterday = ($totalIndexesOfyesterday/($totalHappyUserYesterday));       
}

//((-1*(day before yesterday H.I. value – yesterday's H.I. value)) /( day before yesterday H.I. value))*100

$happyIndexDD = 0;
if($happyIndexPerOfyesterdaysPreviousday !=0) 
{
    $happyIndexDD = ((-1*($happyIndexPerOfyesterdaysPreviousday - $happyIndexPerOfyesterday))/($happyIndexPerOfyesterdaysPreviousday))*100;
}

$resultArray['happyIndexDD'] =round($happyIndexDD,2);

  //M.A.(First calculate Moving Average:) = (Total number of happy faces from all previous days/number of days)
  //=-1(M.A – todays yesterday H.I. value) / M.A

$happyFacesCount = DB::table('happy_indexes')
->leftjoin('users','users.id','happy_indexes.userId')
->where('happy_indexes.status','Active')
->where('users.status','Active')
->where('happy_indexes.moodValue','3')
->where('users.orgId',$orgId)        
->whereDate('happy_indexes.created_at', '<', date('Y-m-d'))
//->whereDate('happy_indexes.created_at','<=', date('Y-m-d', strtotime('-1 days')))
->count();

//No of days from first staff created of organisation till yesterday
$firstStaffOfOrg = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
if (!empty($firstStaffOfOrg)) {
    $firstStaffOfOrgDate = strtotime($firstStaffOfOrg->created_at);
    $yesterdayDate = strtotime(Carbon::today());
    
    if ($firstStaffOfOrgDate < $yesterdayDate) {
        $firstStaffOfOrgDate = date_create(date('Y-m-d',strtotime($firstStaffOfOrg->created_at)));
        $yesterdayDate = date_create(Carbon::yesterday());
        
        $diff = date_diff($firstStaffOfOrgDate,$yesterdayDate);
        $noOfDays = $diff->format("%a")+1;
    }else{
        $noOfDays = 0;
    }
}else{
    $noOfDays = 0;
}

//$happyIndexFirstData = DB::table('happy_indexes')->select('created_at')->orderBy('created_at','asc')->first();

$movingAvgIndex = 0;

if($noOfDays !=0 && $happyFacesCount !=0)
//if($noOfDays !=0)
{
    $movingAvgIndex = ($happyFacesCount/$noOfDays);  
    //$movingAvgIndex = ($happyFacesCount/$noOfDays)*100;  
}

$maValue = 0;
//if($movingAvgIndex !=0 && $happyIndexPer !=0)
if($movingAvgIndex !=0)
{
    //-1(MA – yesterday's H.I. value)/ MA
    $maValue = ((-1*($movingAvgIndex - $happyIndexPerOfyesterday))/($movingAvgIndex))*100;  
    //$maValue = -1*($movingAvgIndex - $happyIndexPer)/$movingAvgIndex;  
}

$resultArray['happyIndexMA'] = round($maValue,2);


    //check DOT rating is given by user on every dot value if it is yes then true

    $userDetails = Auth::user();
    $departmentId = $userDetails->departmentId;

    $orgUsers = DB::table('users')->where('status','Active')->where('orgId',$orgId)->get();

    $departments = DB::table('departments')->select('departmentId')->where('id',$departmentId)->first();

    $departId = $departments->departmentId;

    // $deptUsers = DB::table('users')
    // ->select('users.id')
    // ->leftjoin('departments','departments.id','users.departmentId')
    // ->leftjoin('all_department','all_department.id','departments.departmentId')
    // ->where('users.status','Active')
    // ->where('departments.status','Active')
    // ->where('all_department.status','Active')
    // ->where('departments.departmentId',$departments->departmentId)
    // ->where('users.orgId',$orgId)
    // ->get();

    // $orgUserCount = count($orgUsers);
    // $deptUserCount = count($deptUsers);

    // $dotValueRatingCompletedOrgUserArr = array();
    // $dotValueRatingCompletedDeptUserArr = array();

    // foreach($orgUsers as $ouValue)
    // {
    //     $dotPerArr = array('orgId' => $orgId,'userId'=>$ouValue->id,'departmentId'=>'');

    //     $status = $this->indexReportForDotCompletedValues($dotPerArr);
    //     if($status)
    //     {
    //         array_push($dotValueRatingCompletedOrgUserArr, $ouValue->id);      
    //     }
    // }
    // foreach($deptUsers as $duValue)
    // {
    //     $dotPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'departmentId'=>$departments->departmentId);

    //     $status = $this->indexReportForDotCompletedValues($dotPerArr);
    //     if($status)
    //     {
    //         array_push($dotValueRatingCompletedDeptUserArr, $duValue->id);      
    //     }
    // }

    // $dotValueRatingCompletedOrgUserArrCount = count($dotValueRatingCompletedOrgUserArr);
    // $dotValueRatingCompletedDeptUserArrCount = count($dotValueRatingCompletedDeptUserArr);

    // $orgDotCompleted = "0";
    // $deptDotCompleted = "0";
    // if(!empty($dotValueRatingCompletedOrgUserArrCount) && !empty($orgUserCount))
    // //if(!empty($orgUserCount))
    // {
    //     $orgDotCompleted = ($dotValueRatingCompletedOrgUserArrCount/$orgUserCount)*100;
    // }
    // if(!empty($dotValueRatingCompletedDeptUserArrCount) && !empty($deptUserCount))
    // //if(!empty($deptUserCount))
    // {
    //     $deptDotCompleted = ($dotValueRatingCompletedDeptUserArrCount/$deptUserCount)*100;
    // }

    // //for team role map complete
    // $orgTeamPerArr = array('orgId' => $orgId,'departmentId'=>'');
    // $deptTeamPerArr = array('orgId' => $orgId,'departmentId'=>$departments->departmentId);

    // $orgTeamRoleStatus = $this->indexReportForTeamroleCompleted($orgTeamPerArr);
    // $deptTeamRoleStatus = $this->indexReportForTeamroleCompleted($deptTeamPerArr);

    // $orgTemRoleMapCount = count($orgTeamRoleStatus);
    // $deptTemRoleMapCount = count($deptTeamRoleStatus);

    // $orgTeamRoleMapCompleted = "0";
    // $deptTeamRoleMapCompleted = "0";
    // if (!empty($orgTemRoleMapCount) && !empty($orgUserCount))
    // //if (!empty($orgUserCount))
    // {
    //     $orgTeamRoleMapCompleted = ($orgTemRoleMapCount/$orgUserCount)*100;
    // }
    // if (!empty($deptTemRoleMapCount) && !empty($deptUserCount))
    // //if (!empty($deptUserCount))
    // {
    //     $deptTeamRoleMapCompleted = ($deptTemRoleMapCount/$deptUserCount)*100;
    // }

    // //for personality type completed
    // $orgPersonalityPerArr = array('orgId' => $orgId,'departmentId'=>'');
    // $deptPersonalityPerArr = array('orgId' => $orgId,'departmentId'=>$departments->departmentId);

    // $orgPersonalityTypeStatus = $this->indexReportForpersonalityTypeCompleted($orgPersonalityPerArr);
    // $deptPersonalityTypeStatus = $this->indexReportForpersonalityTypeCompleted($deptPersonalityPerArr);

    // $orgPersonalityTypeCount = count($orgPersonalityTypeStatus);
    // $deptPersonalityTypeCount = count($deptPersonalityTypeStatus);

    // $orgPersonalityTypeCompleted = "0";
    // $deptPersonalityTypeCompleted = "0";
    // if (!empty($orgPersonalityTypeCount) && !empty($orgUserCount))
    // //if (!empty($orgUserCount))
    // {
    //     $orgPersonalityTypeCompleted = ($orgPersonalityTypeCount/$orgUserCount)*100;
    // }
    // if (!empty($deptPersonalityTypeCount) && !empty($deptUserCount))
    // //if (!empty($deptUserCount))
    // {
    //     $deptPersonalityTypeCompleted = ($deptPersonalityTypeCount/$deptUserCount)*100;
    // }

    // //for culture structure comleted       
    // $orgCultureStructurePerArr = array('orgId' => $orgId,'departmentId'=>'');
    // $deptCultureStructurePerArr = array('orgId' => $orgId,'departmentId'=>$departments->departmentId);

    // $orgCultureStructureStatus = $this->indexReportForCultureStructureCompleted($orgCultureStructurePerArr);
    // $deptCultureStructureStatus = $this->indexReportForCultureStructureCompleted($deptCultureStructurePerArr);

    // $orgCultureStructureCount = count($orgCultureStructureStatus);
    // $deptCultureStructureCount = count($deptCultureStructureStatus);

    // $orgCultureStructureCompleted = "0";
    // $deptCultureStructureCompleted = "0";
    // if (!empty($orgCultureStructureCount) && !empty($orgUserCount))
    // //if (!empty($orgUserCount))
    // {
    //     $orgCultureStructureCompleted = ($orgCultureStructureCount/$orgUserCount)*100;
    // }
    // if (!empty($deptCultureStructureCount) && !empty($deptUserCount))
    // //if (!empty($deptUserCount))
    // {
    //     $deptCultureStructureCompleted = ($deptCultureStructureCount/$deptUserCount)*100;
    // }

    // //get all previous days culture structure record
    // $orgCultureStructureStatusForAllPreviousDays = $this->indexReportForCultureStructureCompletedAllPreviousDays($orgCultureStructurePerArr);
    // $deptCultureStructureStatusForAllPreviousDays = $this->indexReportForCultureStructureCompletedAllPreviousDays($deptCultureStructurePerArr);

    // $orgCultureStructureCountAllPreviousDay = count($orgCultureStructureStatusForAllPreviousDays);
    // $deptCultureStructureCountAllPreviousDay = count($deptCultureStructureStatusForAllPreviousDays);

    // $orgCultureStructureCompletedAllPreviousDay = "0";
    // $deptCultureStructureCompletedAllPreviousDay = "0";
    // if(!empty($orgUserCount))
    // {
    //     $orgCultureStructureCompletedAllPreviousDay = ($orgCultureStructureCountAllPreviousDay/$orgUserCount)*100;
    // }
    // if(!empty($deptUserCount))
    // {
    //     $deptCultureStructureCompletedAllPreviousDay = ($deptCultureStructureCountAllPreviousDay/$deptUserCount)*100;
    // }

    // //for motivation comleted
    // $orgMotivationPerArr = array('orgId' => $orgId,'departmentId'=>'');
    // $deptMotivationPerArr = array('orgId' => $orgId,'departmentId'=>$departments->departmentId);

    // $orgMotivationStatus = $this->indexReportForMotivationCompleted($orgMotivationPerArr);
    // $deptMotivationStatus = $this->indexReportForMotivationCompleted($deptMotivationPerArr);

    // $orgMotivationCount = count($orgMotivationStatus);
    // $deptMotivationCount = count($deptMotivationStatus);

    // $orgMotivationCompleted = "0";
    // $deptMotivationCompleted = "0";
    // if (!empty($orgMotivationCount) && !empty($orgUserCount))
    // //if(!empty($orgUserCount))
    // {
    //     $orgMotivationCompleted = ($orgMotivationCount/$orgUserCount)*100;
    // }
    // if (!empty($deptMotivationCount) && !empty($deptUserCount))
    // //if(!empty($deptUserCount))
    // {
    //     $deptMotivationCompleted = ($deptMotivationCount/$deptUserCount)*100;
    // }

    // //Thumbsup Completed for yesterday
    // $orgThumbsupPerArr = array('orgId' => $orgId,'departmentId'=>'');
    // $deptThumbsupPerArr = array('orgId' => $orgId,'departmentId'=>$departments->departmentId);

    // $orgThumbsupCount = $this->indexReportForThumbsupCompleted($orgThumbsupPerArr);
    // $deptThumbsupCount = $this->indexReportForThumbsupCompleted($deptThumbsupPerArr);

    // $orgThumbCompleted = "0";
    // $deptThumbCompleted = "0";
    // if(!empty($orgThumbsupCount) && !empty($orgUserCount))
    // //if(!empty($orgUserCount))
    // {
    //     //$orgThumbCompleted = number_format(($orgThumbsupCount/$orgUserCount), 2);
    //     $orgThumbCompleted = ($orgThumbsupCount/$orgUserCount)*100;
    // }
    // if(!empty($deptThumbsupCount) && !empty($deptUserCount))
    // //if(!empty($deptUserCount))
    // {
    //     //$deptThumbCompleted = number_format(($deptThumbsupCount/$deptUserCount), 2);
    //     $deptThumbCompleted = ($deptThumbsupCount/$deptUserCount)*100;
    // }

    // //Thumbsup completed Day before Yesterday
    // $orgThumbsupDayBeforeYesterdayCount = $this->indexReportForThumbsupCompletedDayBeforeYesterday($orgThumbsupPerArr);
    // $deptThumbsupDayBeforeYesterdayCount = $this->indexReportForThumbsupCompletedDayBeforeYesterday($deptThumbsupPerArr);

    // $orgThumbsupCountDayBeforeYesterday = "0";
    // $deptThumbsupCountDayBeforeYesterday = "0";
    // if(!empty($orgUserCount))
    // {
    //     $orgThumbsupCountDayBeforeYesterday = ($orgThumbsupDayBeforeYesterdayCount/$orgUserCount)*100;
    // }
    // if(!empty($deptUserCount))
    // {
    //     $deptThumbsupCountDayBeforeYesterday = ($deptThumbsupDayBeforeYesterdayCount/$deptUserCount)*100;
    // }

    // //Thumbsup completed All previous days
    // $orgThumbsupAllPreviousDaysCount = $this->indexReportForThumbsupCompletedAllPreviousDays($orgThumbsupPerArr);
    // $deptThumbsupAllPreviousDaysCount = $this->indexReportForThumbsupCompletedAllPreviousDays($deptThumbsupPerArr);

    // $orgThumbsupCountAllPreviousDaysCount = "0";
    // $deptThumbsupCountAllPreviousDaysCount = "0";
    // if(!empty($orgUserCount))
    // {
    //     $orgThumbsupCountAllPreviousDaysCount = ($orgThumbsupAllPreviousDaysCount/$orgUserCount)*100;
    // }
    // if(!empty($deptUserCount))
    // {
    //     $deptThumbsupCountAllPreviousDaysCount = ($deptThumbsupAllPreviousDaysCount/$deptUserCount)*100;
    // }

    // /*get previous day happy index detail*/

    // $orgHappyIndexPerArr = array('orgId' => $orgId,'departmentId'=>'');
    // $deptHappyIndexPerArr = array('orgId' => $orgId,'departmentId'=>$departments->departmentId);

    // $orgHappyIndexCompleted = $this->indexReportForHappyIndexCompleted($orgHappyIndexPerArr);
    // $deptHappyIndexCompleted = $this->indexReportForHappyIndexCompleted($deptHappyIndexPerArr);

    // /*get day before yesterday of happy index detail*/
    // $orgHappyIndexCompletedDayBeforeYesterday = $this->indexReportForHappyIndexCompletedDayBeforeYesterday($orgHappyIndexPerArr);
    // $deptHappyIndexCompletedDayBeforeYesterday = $this->indexReportForHappyIndexCompletedDayBeforeYesterday($deptHappyIndexPerArr);

    // /*get happy index detail of all previous days*/
    // $orgHappyIndexCompletedAllPreviousDays = $this->indexReportForHappyIndexCompletedAllPreviousDays($orgHappyIndexPerArr);
    // $deptHappyIndexCompletedAllPreviousDays = $this->indexReportForHappyIndexCompletedAllPreviousDays($deptHappyIndexPerArr);

    // //Tribeometer and diagnostics completed last month by users

    // $diagnosticAnsCompletedOrgUserArr = array();
    // $tribeometerAnsCompletedOrgUserArr = array();

    // $diagnosticAnsCompletedDeptUserArr = array();
    // $tribeometerAnsCompletedDeptUserArr = array();

    // $diagnosticAnsCompletedOrgUserArrForAllPreviousDays = array();
    // $tribeometerAnsCompletedOrgUserArrForAllPreviousDays = array();

    // $diagnosticAnsCompletedDeptUserArrForAllPreviousDays = array();
    // $tribeometerAnsCompleteddeptUserArrForAllPreviousDays = array();

    // foreach($orgUsers as $ouValue)
    // {
    //     $PerArr = array('orgId' => $orgId,'userId'=>$ouValue->id,'departmentId'=>'');

    //     $isDiagnosticAnsDone = $this->indexReportForDiagAnsCompletedValues($PerArr);
       
    //     if(!empty($isDiagnosticAnsDone))
    //     {
    //         array_push($diagnosticAnsCompletedOrgUserArr, $ouValue->id);      
    //     }

    //     $isDiagnosticAnsDoneForAllPreviousDays = $this->indexReportForDiagAnsCompletedValuesForAllPreviousDays($PerArr);
       
    //     if(!empty($isDiagnosticAnsDoneForAllPreviousDays))
    //     {
    //         array_push($diagnosticAnsCompletedOrgUserArrForAllPreviousDays, $ouValue->id);      
    //     }

    //     //$tribeAnsPerArr = array('orgId' => $orgId,'userId'=>$ouValue->id,'departmentId'=>'');

    //     $isTribeometerAnsDone = $this->indexReportForTribeAnsCompletedValues($PerArr);
       
    //     if($isTribeometerAnsDone)
    //     {
    //         array_push($tribeometerAnsCompletedOrgUserArr, $ouValue->id);      
    //     }

    //     $isTribeometerAnsDoneForAllPreviousDays = $this->indexReportForTribeAnsCompletedValuesForAllPreviousDays($PerArr);
       
    //     if($isTribeometerAnsDoneForAllPreviousDays)
    //     {
    //         array_push($tribeometerAnsCompletedOrgUserArrForAllPreviousDays, $ouValue->id);      
    //     }
    // }

    // foreach($deptUsers as $duValue)
    // {
    //     $PerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'departmentId'=>$departments->departmentId);

    //     $isDiagnosticAnsDone = $this->indexReportForDiagAnsCompletedValues($PerArr);
    //     if(!empty($isDiagnosticAnsDone))
    //     {
    //         array_push($diagnosticAnsCompletedDeptUserArr, $duValue->id);      
    //     }

    //     $isDiagnosticAnsDoneForAllPreviousDays = $this->indexReportForDiagAnsCompletedValuesForAllPreviousDays($PerArr);
       
    //     if(!empty($isDiagnosticAnsDoneForAllPreviousDays))
    //     {
    //         array_push($diagnosticAnsCompletedDeptUserArrForAllPreviousDays, $duValue->id);      
    //     }

    //     //$tribeAnsPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'departmentId'=>$departments->departmentId);

    //     $isTribeometerAnsDone = $this->indexReportForTribeAnsCompletedValues($PerArr);
       
    //     if($isTribeometerAnsDone)
    //     {
    //         array_push($tribeometerAnsCompletedDeptUserArr, $duValue->id);      
    //     }

    //     $isTribeometerAnsDoneForAllPreviousDays = $this->indexReportForTribeAnsCompletedValuesForAllPreviousDays($PerArr);
       
    //     if($isTribeometerAnsDoneForAllPreviousDays)
    //     {
    //         array_push($tribeometerAnsCompleteddeptUserArrForAllPreviousDays, $duValue->id);      
    //     }
    // }

    // $diagnosticAnsCompletedOrgUserArrCount = count($diagnosticAnsCompletedOrgUserArr);
    // $tribeometerAnsCompletedOrgUserArrCount = count($tribeometerAnsCompletedOrgUserArr);

    // $diagnosticAnsCompletedDeptUserArrCount = count($diagnosticAnsCompletedDeptUserArr);
    // $tribeometerAnsCompletedDeptUserArrCount = count($tribeometerAnsCompletedDeptUserArr);

    // $diagnosticAnsCompletedOrgUserArrCountForAllPreviousDays = count($diagnosticAnsCompletedOrgUserArrForAllPreviousDays);
    // $tribeometerAnsCompletedOrgUserArrCountForAllPreviousDays = count($tribeometerAnsCompletedOrgUserArrForAllPreviousDays);

    // $diagnosticAnsCompletedDeptUserArrCountForAllPreviousDays = count($diagnosticAnsCompletedDeptUserArrForAllPreviousDays);
    // $tribeometerAnsCompletedDeptUserArrCountForAllPreviousDays = count($tribeometerAnsCompleteddeptUserArrForAllPreviousDays);

    // $orgDiagnosticAnsCompleted = "0";
    // if(!empty($orgUserCount))
    // {
    //     $orgDiagnosticAnsCompleted = ($diagnosticAnsCompletedOrgUserArrCount/$orgUserCount)*100;
    // }
    // $orgTribeometerAnsCompleted = "0";
    // if(!empty($orgUserCount))
    // {
    //     $orgTribeometerAnsCompleted = ($tribeometerAnsCompletedOrgUserArrCount/$orgUserCount)*100;
    // }

    // $deptDiagnosticAnsCompleted = "0";
    // if(!empty($deptUserCount))
    // {
    //     $deptDiagnosticAnsCompleted = ($diagnosticAnsCompletedDeptUserArrCount/$deptUserCount)*100;
    // }
    // $deptTribeometerAnsCompleted = "0";
    // if(!empty($deptUserCount))
    // {
    //     $deptTribeometerAnsCompleted = ($tribeometerAnsCompletedDeptUserArrCount/$deptUserCount)*100;
    // }
    // // For All previous days record
    // $orgDiagnosticAnsCompletedForAllPreviousDays = "0";
    // if(!empty($orgUserCount))
    // {
    //     $orgDiagnosticAnsCompletedForAllPreviousDays = ($diagnosticAnsCompletedOrgUserArrCountForAllPreviousDays/$orgUserCount)*100;
    // }
    // $orgTribeometerAnsCompletedForAllPreviousDays = "0";
    // if(!empty($orgUserCount))
    // {
    //     $orgTribeometerAnsCompletedForAllPreviousDays = ($tribeometerAnsCompletedOrgUserArrCountForAllPreviousDays/$orgUserCount)*100;
    // }

    // $deptDiagnosticAnsCompletedForAllPreviousDays = "0";
    // if(!empty($deptUserCount))
    // {
    //     $deptDiagnosticAnsCompletedForAllPreviousDays = ($diagnosticAnsCompletedDeptUserArrCountForAllPreviousDays/$deptUserCount)*100;
    // }
    // $deptTribeometerAnsCompletedForAllPreviousDays = "0";
    // if(!empty($deptUserCount))
    // {
    //     $deptTribeometerAnsCompletedForAllPreviousDays = ($tribeometerAnsCompletedDeptUserArrCountForAllPreviousDays/$deptUserCount)*100;
    // }

    //  //All average ratings for values by users
    // $orgValuePerformanceCount = 0;
    // $deptValuePerformanceCount = 0;
    // if ($dot) {
    //     $orgValuePerformancePerArr = array('orgId' => $orgId,'departmentId'=>'', 'dotId'=>$dot->id);
    //     $deptValuePerformancePerArr = array('orgId' => $orgId,'departmentId'=>$departments->departmentId, 'dotId'=>$dot->id);

    //     $orgValuePerformanceCount = $this->indexReportForValuePerformanceCompleted($orgValuePerformancePerArr);
    //     $deptValuePerformanceCount = $this->indexReportForValuePerformanceCompleted($deptValuePerformancePerArr);
    // }

    // //no of month from the day of creation of first staff
    // // $firstStaffOrg = DB::table('offices')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();

    // // $date1 = new DateTime($firstStaffOrg->created_at);
    // // $date2 = $date1->diff(new DateTime(Carbon::today()));
    // // $totalMonthcount = ($date2->y*12) + $date2->m + 2;

    // $firstStaffOfOrg = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    // if($firstStaffOfOrg){
    //     $firstDateOfOrg = date('Y-m',strtotime($firstStaffOfOrg->created_at));
    //     $lastMonthDate = date('Y-m');

    //     $time1 = strtotime($firstDateOfOrg); // always small
    //     $time2 = strtotime($lastMonthDate);
    //     if(date('m', $time1)==date('m', $time2) && date('Y', $time1)==date('Y', $time2)){
    //         $noOfPreviousMonths = 1;
    //     }else{
    //         if ($time1 < $time2){
    //             $my = date('mY', $time2);
    //             $noOfPreviousMonths = array(date('F', $time1));
    //             while($time1 < $time2) {
    //                 $time1 = strtotime(date('Y-m-d', $time1).' +1 month');
    //                 if(date('mY', $time1) != $my && ($time1 < $time2))
    //                     $noOfPreviousMonths[] = date('F', $time1);
    //             }
    //             $noOfPreviousMonths[] = date('F', $time2);
    //             $noOfPreviousMonths = count($noOfPreviousMonths); 
    //         }else{
    //             $noOfPreviousMonths = 0;
    //         } 
    //     }
    // }else{
    //     $noOfPreviousMonths = 0; 
    // }
    
    // //Number of reported incidents and get details of feedback of last month
    // $orgFeedbackPerArr = array('orgId' => $orgId,'departmentId'=>'');
    // $deptFeedbackPerArr = array('orgId' => $orgId,'departmentId'=>$departments->departmentId);

    // $orgFeedbackCount = $this->indexReportForFeedbackCompleted($orgFeedbackPerArr);
    // $deptFeedbackCount = $this->indexReportForFeedbackCompleted($deptFeedbackPerArr);

    // $orgFeedbackCompleted = "0";
    // $deptFeedbackCompleted = "0";
    // if(!empty($orgFeedbackCount) && !empty($orgUserCount) && !empty($noOfPreviousMonths))
    // //if(!empty($orgUserCount))
    // {
    //     //((Total feedback count /All active users count of organisation) /(no of month from the day of creation of first staff till now)) *500
    //     $orgFeedbackCompleted = (($orgFeedbackCount/$orgUserCount)/($noOfPreviousMonths))*500;
    // }
    // if(!empty($deptFeedbackCount) && !empty($deptUserCount) && !empty($noOfPreviousMonths))
    // //if(!empty($deptUserCount))
    // {
    //     $deptFeedbackCompleted = (($deptFeedbackCount/$deptUserCount)/($noOfPreviousMonths))*500;
    // }

    // //for all previous days
    // $orgFeedbackCountForAllPreviousDays = $this->indexReportForFeedbackCompletedForAllPreviousDays($orgFeedbackPerArr);
    // $deptFeedbackCountForAllPreviousDays = $this->indexReportForFeedbackCompletedForAllPreviousDays($deptFeedbackPerArr);

    // $orgFeedbackCompletedForAllPreviousDays = "0";
    // $deptFeedbackCompletedForAllPreviousDays = "0";
    // if(!empty($orgUserCount))
    // {
    //     $orgFeedbackCompletedForAllPreviousDays = (($orgFeedbackCountForAllPreviousDays/$orgUserCount)/$noOfPreviousMonths*500);
    // }
    // if(!empty($deptUserCount))
    // {
    //     $deptFeedbackCompletedForAllPreviousDays = (($deptFeedbackCountForAllPreviousDays/$deptUserCount)/$noOfPreviousMonths*500);
    // }

    // //User count for tribeometer

    // $orgUserForTribePerArr = array('orgId' => $orgId,'departmentId'=>'');
    // $deptUserForTribePerArr = array('orgId' => $orgId,'departmentId'=>$departments->departmentId);

    // $orgTribeUserCount = $this->indexReportForUserCountForTribe($orgUserForTribePerArr);
    // $deptTribeUserCount = $this->indexReportForUserCountForTribe($deptUserForTribePerArr);

    // //Direction and connection of tribeometer

    // $orgTribeDnCCount = 0;
    // if (!empty($orgTribeUserCount))
    // {
    //     $orgTribePerArr = array('orgId' => $orgId,'departmentId'=>'','userCount'=> $orgTribeUserCount);

    //     $orgTribeDnCCount = $this->indexReportForTribeDnC($orgTribePerArr);
    // }
    // $deptTribeDnCCount = 0;
    // if (!empty($deptTribeUserCount))
    // {
    //     $deptTribePerArr = array('orgId' => $orgId,'departmentId'=>$departments->departmentId,'userCount'=> $deptTribeUserCount);

    //     $deptTribeDnCCount = $this->indexReportForTribeDnC($deptTribePerArr);
    // }

    // //User count for diagnstics

    // $orgUserForDiagPerArr = array('orgId' => $orgId,'departmentId'=>'');
    // $deptUserForDiagPerArr = array('orgId' => $orgId,'departmentId'=>$departments->departmentId);

    // $orgDiagUserCount = $this->indexReportForUserCountForDiagnostics($orgUserForDiagPerArr);
    // $deptDiagUserCount = $this->indexReportForUserCountForDiagnostics($deptUserForDiagPerArr);


    // //Diagnostic core 

    // $orgDiagCore = 0;
    // if (!empty($orgDiagUserCount))
    // {
    //     $orgDiagCorePerArr = array('orgId' => $orgId,'departmentId'=>'','userCount'=> $orgDiagUserCount);

    //     $orgDiagCore = $this->indexReportForDiagnosticCore($orgDiagCorePerArr);
    // }
    // $deptDiagCore = 0;
    // if (!empty($deptDiagUserCount))
    // {
    //     $deptDiagCorePerArr = array('orgId' => $orgId,'departmentId'=>$departments->departmentId,'userCount'=> $deptDiagUserCount);

    //     $deptDiagCore = $this->indexReportForDiagnosticCore($deptDiagCorePerArr);
    // }

    // //stress inverse of diagnostic

    // $orgDiagStress = 0;
    // if (!empty($orgDiagUserCount))
    // {
    //     $orgDiagstressPerArr = array('orgId' => $orgId,'departmentId'=>'','userCount'=> $orgDiagUserCount);

    //     $orgDiagStress = $this->indexReportForDiagnosticStress($orgDiagstressPerArr);
    // }
    // $deptDiagStress = 0;
    // if (!empty($deptDiagUserCount))
    // {
    //     $deptDiagstressPerArr = array('orgId' => $orgId,'departmentId'=>$departments->departmentId,'userCount'=> $deptDiagUserCount);

    //     $deptDiagStress = $this->indexReportForDiagnosticStress($deptDiagstressPerArr);
    // }


    // //index org for yesterday and all day
    // $releventOrganisationScores = $orgDotCompleted + $orgTeamRoleMapCompleted + $orgPersonalityTypeCompleted + $orgCultureStructureCompleted + $orgMotivationCompleted + $orgThumbCompleted + $orgHappyIndexCompleted + $orgDiagnosticAnsCompleted + $orgTribeometerAnsCompleted + $orgValuePerformanceCount + $orgFeedbackCompleted + $orgTribeDnCCount + $orgDiagCore + $orgDiagStress;

    // //index dept for yesterday and all day
    // $releventDepartmentScores = $deptDotCompleted + $deptTeamRoleMapCompleted + $deptPersonalityTypeCompleted + $deptCultureStructureCompleted + $deptMotivationCompleted + $deptThumbCompleted + $deptHappyIndexCompleted + $deptDiagnosticAnsCompleted + $deptTribeometerAnsCompleted + $deptValuePerformanceCount + $deptFeedbackCompleted + $deptTribeDnCCount + $deptDiagCore + $deptDiagStress;

    // //index org for day before yesterday and all day
    // $previousYesterdayOrganisationScores = $orgDotCompleted + $orgTeamRoleMapCompleted + $orgPersonalityTypeCompleted + $orgCultureStructureCompleted + $orgMotivationCompleted + $orgThumbsupCountDayBeforeYesterday + $orgHappyIndexCompletedDayBeforeYesterday + $orgDiagnosticAnsCompleted + $orgTribeometerAnsCompleted + $orgValuePerformanceCount + $orgFeedbackCompleted + $orgTribeDnCCount + $orgDiagCore + $orgDiagStress;

    // //index dept for day before yesterday and all day
    // $previousYesterdayDepartmentScores = $deptDotCompleted + $deptTeamRoleMapCompleted + $deptPersonalityTypeCompleted + $deptCultureStructureCompleted + $deptMotivationCompleted + $deptThumbsupCountDayBeforeYesterday + $deptHappyIndexCompletedDayBeforeYesterday + $deptDiagnosticAnsCompleted + $deptTribeometerAnsCompleted + $deptValuePerformanceCount + $deptFeedbackCompleted + $deptTribeDnCCount + $deptDiagCore + $deptDiagStress;

    // //index org for all previous days
    // $allPreviousDayOrganisationScores = $orgDotCompleted + $orgTeamRoleMapCompleted + $orgPersonalityTypeCompleted + $orgCultureStructureCompletedAllPreviousDay + $orgMotivationCompleted + $orgThumbsupCountAllPreviousDaysCount + $orgHappyIndexCompletedAllPreviousDays + $orgDiagnosticAnsCompletedForAllPreviousDays + $orgTribeometerAnsCompletedForAllPreviousDays + $orgValuePerformanceCount + $orgFeedbackCompletedForAllPreviousDays + $orgTribeDnCCount + $orgDiagCore + $orgDiagStress;

    // //index dept for all previous days
    // $allPreviousDayDepartmentScores = $deptDotCompleted + $deptTeamRoleMapCompleted + $deptPersonalityTypeCompleted + $deptCultureStructureCompletedAllPreviousDay + $deptMotivationCompleted + $deptThumbsupCountAllPreviousDaysCount + $deptHappyIndexCompletedAllPreviousDays + $deptDiagnosticAnsCompletedForAllPreviousDays + $deptTribeometerAnsCompletedForAllPreviousDays + $deptValuePerformanceCount + $deptFeedbackCompletedForAllPreviousDays + $deptTribeDnCCount + $deptDiagCore + $deptDiagStress;

    // $previousYesterdayOrganisationIndex = ($previousYesterdayOrganisationScores/2300)*1000;
    // $previousYesterdayDepartmentIndex = ($previousYesterdayDepartmentScores/2300)*1000;

 $resultArray['indexOrg'] = DB::table('indexReportRecords')
        ->where('orgId',$orgId)
        ->whereNull('officeId')
        ->whereNull('departmentId')
        ->whereNull('isAllDepId')
        ->whereDate('date',date('Y-m-d', strtotime('-1 days')))
        ->sum('total_count');
//print_r($resultArray['indexOrg']);die();
    // $resultArray['indexDept'] = DB::table('indexReportRecords')
    //     ->where('orgId',$orgId)
    //     ->where('departmentId',$departmentId)
    //     ->where('date',date('Y-m-d', strtotime('-1 days')))
    //     ->sum('total_count');

    $resultArray['indexDept'] = DB::table('indexReportRecords')
        // ->leftjoin('departments','departments.id','indexReportRecords.departmentId')
        // ->leftjoin('all_department','all_department.id','departments.departmentId')
        // ->where('departments.status','active')
        // ->where('all_department.status','active')
        ->where('orgId',$orgId)
        ->where('departmentId',$departId)
        ->whereNull('officeId')
        ->where('isAllDepId',1)
        // ->where('departmentId',$departmentId)
        ->whereDate('date',date('Y-m-d', strtotime('-1 days')))
        ->sum('total_count');
    //print_r($resultArray['indexDept']);die();
       
    $dayBeforeYesterdayIndexOrg = DB::table('indexReportRecords')
        ->where('orgId',$orgId)
        ->whereNull('officeId')
        ->whereNull('departmentId')
        ->whereNull('isAllDepId')
        ->whereDate('date',date('Y-m-d', strtotime('-2 days')))
        ->sum('total_count');

    // $dayBeforeYesterdayIndexDept = DB::table('indexReportRecords')
    //     ->where('orgId',$orgId)
    //     ->where('departmentId',$departmentId)
    //     ->where('date',date('Y-m-d', strtotime('-2 days')))
    //     ->sum('total_count');

    $dayBeforeYesterdayIndexDept = DB::table('indexReportRecords')
        // ->leftjoin('departments','departments.id','indexReportRecords.departmentId')
        // ->leftjoin('all_department','all_department.id','departments.departmentId')
        // ->where('departments.status','active')
        // ->where('all_department.status','active')
        ->where('orgId',$orgId)
        ->where('departmentId',$departId)
        ->whereNull('officeId')
        ->where('isAllDepId',1)
        // ->where('departmentId',$departmentId)
        ->whereDate('date',date('Y-m-d', strtotime('-2 days')))
        ->sum('total_count');

    $resultArray['indexOrgDD'] = 0;
    if (!empty($dayBeforeYesterdayIndexOrg)) {
        $resultArray['indexOrgDD'] = round((-1*($dayBeforeYesterdayIndexOrg - $resultArray['indexOrg'])/$dayBeforeYesterdayIndexOrg)*100,2);
    }

    $resultArray['indexDepDD'] = 0;
    if (!empty($dayBeforeYesterdayIndexDept)) {
        $resultArray['indexDepDD'] = round((-1*($dayBeforeYesterdayIndexDept - $resultArray['indexDept'])/$dayBeforeYesterdayIndexDept)*100,2);
    }

    $allPreviousDaysRecordOfOrg = DB::table('indexReportRecords')
        ->where('orgId',$orgId)
        ->whereNull('officeId')
        ->whereNull('departmentId')
        ->whereNull('isAllDepId')
        ->whereDate('date','<',date('Y-m-d'))
        ->sum('total_count');

    // $allPreviousDaysRecordOfDept = DB::table('indexReportRecords')
    //     ->where('orgId',$orgId)
    //     ->where('departmentId',$departmentId)
    //     ->where('date','<',date('Y-m-d'))
    //     ->sum('total_count');

    $allPreviousDaysRecordOfDept = DB::table('indexReportRecords')
        // ->leftjoin('departments','departments.id','indexReportRecords.departmentId')
        // ->leftjoin('all_department','all_department.id','departments.departmentId')
        // ->where('departments.status','active')
        // ->where('all_department.status','active')
        ->where('orgId',$orgId)
        ->where('departmentId',$departId)
        ->whereNull('officeId')
        ->where('isAllDepId',1)
        ->whereDate('date','<',date('Y-m-d'))
        ->sum('total_count');
        
        // ->where('departmentId',$departmentId)

    //No of days from first staff created of organisation till yesterday
    $firstStaffOfOrg = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    if (!empty($firstStaffOfOrg)) {
        $firstStaffOfOrgDate = strtotime($firstStaffOfOrg->created_at);
        $yesterdayDate = strtotime(Carbon::today());
        
        if ($firstStaffOfOrgDate < $yesterdayDate) {
            $firstStaffOfOrgDate = date_create(date('Y-m-d',strtotime($firstStaffOfOrg->created_at)));
            $yesterdayDate = date_create(Carbon::yesterday());
            
            $diff = date_diff($firstStaffOfOrgDate,$yesterdayDate);
            $noOfDays = $diff->format("%a")+1;
        }else{
            $noOfDays = 0;
        }
    }else{
        $noOfDays = 0;
    }

    //No of days from first staff created of organisation of department till yesterday
    $firstStaffOfDept = DB::table('users')
    ->select('users.created_at')
    ->leftjoin('departments','departments.id','users.departmentId')
    ->leftjoin('all_department','all_department.id','departments.departmentId')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->where('departments.departmentId',$departId)
    //->where('departments.departmentId',$departments->departmentId)
    ->where('users.orgId',$orgId)
    ->orderBy('users.created_at','ASC')
    ->first();

    if (!empty($firstStaffOfDept)) {
        $firstStaffOfDeptDate = strtotime($firstStaffOfDept->created_at);
        $yesterdayDate = strtotime(Carbon::today());
        
        if ($firstStaffOfDeptDate < $yesterdayDate) {
            $firstStaffOfDeptDate = date_create(date('Y-m-d',strtotime($firstStaffOfDept->created_at)));
            $yesterdayDate = date_create(Carbon::yesterday());
            
            $diff = date_diff($firstStaffOfDeptDate,$yesterdayDate);
            $noOfDaysDept = $diff->format("%a")+1;
        }else{
            $noOfDaysDept = 0;
        }
    }else{
        $noOfDaysDept = 0;
    }

    //First calculate Moving Average of organisation
    $movingAverageOrgIndex = 0;
    if (!empty($allPreviousDaysRecordOfOrg) && !empty($noOfDays)) 
    {
        $movingAverageOrgIndex = ($allPreviousDaysRecordOfOrg/$noOfDays);
    }
    //Now Calculate MA
    $resultArray['indexOrgMA'] = 0;
    if (!empty($movingAverageOrgIndex)) 
    {
        $resultArray['indexOrgMA'] = round((-1*($movingAverageOrgIndex - $resultArray['indexOrg'])/$movingAverageOrgIndex)*100,2);
    }

    //First calculate Moving Average of department
    $movingAverageDeptIndex = 0;
    if (!empty($allPreviousDaysRecordOfDept) && !empty($noOfDaysDept)) 
    {
        $movingAverageDeptIndex = ($allPreviousDaysRecordOfDept/$noOfDaysDept);
    }
    //Now Calculate MA
    $resultArray['indexDeptMA'] = 0;
    if (!empty($movingAverageDeptIndex)) {
        $resultArray['indexDeptMA'] = round((-1*($movingAverageDeptIndex - $resultArray['indexDept'])/$movingAverageDeptIndex)*100,2);
    }





    //->whereDate('dot_bubble_rating_records.updated_at',date('Y-m-d', strtotime('-2 days')));  


   /* $resultArray['indexOrg'] = round((($releventOrganisationScores/2300)*1000),2);

    $resultArray['indexDept'] = round((($releventDepartmentScores/2300)*1000),2);

    $allPreviousDayOrganisationIndex = ($allPreviousDayOrganisationScores/2300)*1000;

    $allPreviousDayDepartmentIndex = ($allPreviousDayDepartmentScores/2300)*1000;

    $orgCreatedDate = DB::table('organisations')->select('created_at')->where('id',$orgId)->first();
    $organisationDate = date_create(date('Y-m-d',strtotime($orgCreatedDate->created_at)));
    $yesterdayDate = date_create(Carbon::yesterday());
    $diff = date_diff($organisationDate,$yesterdayDate);

    $noOfDays = $diff->format("%a")+1;

    //First calculate Moving Average of organisation
    $movingAverageOrgIndex = 0;
    if (!empty($noOfDays)) 
    {
        $movingAverageOrgIndex = ($allPreviousDayOrganisationIndex/$noOfDays);
    }
    //Now Calculate MA
    $resultArray['indexOrgMA'] = 0;
    if (!empty($movingAverageOrgIndex)) 
    {
        $resultArray['indexOrgMA'] = round((-1*($movingAverageOrgIndex - $resultArray['indexOrg'])/$movingAverageOrgIndex),2);
    }

    //First calculate Moving Average of department
    $movingAverageDeptIndex = 0;
    if (!empty($noOfDays)) 
    {
        $movingAverageDeptIndex = ($allPreviousDayDepartmentIndex/$noOfDays);
    }
    //Now Calculate MA
    $resultArray['indexDeptMA'] = 0;
    if (!empty($movingAverageDeptIndex)) {
        $resultArray['indexDeptMA'] = round((-1*($movingAverageDeptIndex - $resultArray['indexDept'])/$movingAverageDeptIndex),2);
    }

    $resultArray['indexOrgDD'] = 0;
    if (!empty($previousYesterdayOrganisationIndex)) 
    {
        $indexOrgDD = round((-1*($previousYesterdayOrganisationIndex - $resultArray['indexOrg'])/$previousYesterdayOrganisationIndex),2);
        if ($indexOrgDD != 0) 
        {
            $resultArray['indexOrgDD'] = $indexOrgDD;
        }
        else
        {
            $resultArray['indexOrgDD'] = 0;
        }
    }

    $resultArray['indexDepDD'] = 0;
    if (!empty($previousYesterdayDepartmentIndex)) 
    {
        $indexDepDD = round((-1*($previousYesterdayDepartmentIndex - $resultArray['indexDept'])/$previousYesterdayDepartmentIndex),2);

        if ($indexDepDD != 0) 
        {
            $resultArray['indexDepDD'] = $indexDepDD;
        }
        else
        {
            $resultArray['indexDepDD'] = 0;
        }
    }*/

    //     print_r("previousYesterdayOrganisationIndex=> ".$previousYesterdayOrganisationIndex."<br>  ");
    //     print_r("indexOrg=> ".$resultArray['indexOrg']."<br>  ");
    //     print_r("indexOrgDD=> ".$resultArray['indexOrgDD']."<br>  ");
    //     print_r("previousYesterdayDepartmentIndex=> ".$previousYesterdayDepartmentIndex."<br>  ");
    //     print_r("indexDept=> ".$resultArray['indexDept']."<br>  ");
    //     print_r("indexDepDD=> ".$resultArray['indexDepDD']."<br>  ");

    //     print_r("movingAverageOrgIndex=> ".$movingAverageOrgIndex."<br>  ");
    // die;

    // if (!empty($releventOrganisationScores)) 
    // {
        //$resultArray['indexOrg'] = number_format((($releventOrganisationScores/2300)*1000),2);

        //$indexOrg = number_format((($releventOrganisationScores/2300)*1000),2);
    // }else
    // {
    //     $resultArray['indexOrg'] = 0;
    // }

    //if (!empty($releventDepartmentScores)) 
    //{
        //$resultArray['indexDept'] = number_format((($releventDepartmentScores/2300)*1000),2);
        //$indexDept = number_format((($releventDepartmentScores/2300)*1000),2);
    // }else
    // {
    //     $resultArray['indexDept'] = 0;
    // }

//     print_r("Final result Organisation => ".$resultArray['indexOrg']."<br>");
//     print_r("Final result Department=> ".$resultArray['indexDept']."<br>");

//     print_r("Dot Org=>".$orgDotCompleted."<br>");
//     print_r("Dot Dept=>".$deptDotCompleted."<br>");

//     print_r("team role Org=>".$orgTeamRoleMapCompleted."<br>");
//     print_r("team role dept=>".$deptTeamRoleMapCompleted."<br>");

//     print_r("PT Org=>".$orgPersonalityTypeCompleted."<br>");
//     print_r("PT dept=>".$deptPersonalityTypeCompleted."<br>");

//     print_r("CS org=>".$orgCultureStructureCompleted."<br>");
//     print_r("CS dept=>".$deptCultureStructureCompleted."<br>");

//     print_r("motivation org=>".$orgMotivationCompleted."<br>");
//     print_r("motivation dept=>".$deptMotivationCompleted."<br>");

//     print_r("Thumpsup org=>".$orgThumbCompleted."<br>");
//     print_r("Thumpsup dept=>".$deptThumbCompleted."<br>");

//     print_r("happy Index Org=>".$orgHappyIndexCompleted."<br>");
//     print_r("happy Index dept=>".$deptHappyIndexCompleted."<br>");

//     print_r("Diagnostic Ans Org=>".$orgDiagnosticAnsCompleted."<br>");
//     print_r("Diagnostic Ans dept=>".$deptDiagnosticAnsCompleted."<br>");

//     print_r("Tribeometer Ans=>org".$orgTribeometerAnsCompleted."<br>");
//     print_r("Tribeometer Ans dept=>".$deptTribeometerAnsCompleted."<br>");

//     print_r("Values Performance org=>".$orgValuePerformanceCount."<br>");
//     print_r("Values Performance dept=>".$deptValuePerformanceCount."<br>");

//     print_r("feedbacks org=>".$orgFeedbackCompleted."<br>");
//     print_r("feedbacks dept=>".$deptFeedbackCompleted."<br>");

//     print_r("Direction and connection org=>".$orgTribeDnCCount."<br>");
//     print_r("Direction and connection dept=>".$deptTribeDnCCount."<br>");

//     print_r("Diagnostic core org =>".$orgDiagCore."<br>");
//     print_r("Diagnostic core dept=>".$deptDiagCore."<br>");

//     print_r("Diagnostic stress org=>".$orgDiagStress);
//     print_r("Diagnostic stress dept=>".$deptDiagStress);

// die;


return response()->json(['code'=>200, 'status'=>true,'service_name'=>'dashboard-detail','message'=>'','data'=>$resultArray]);

}

// public function getDashboardDetail()
// {

// 	$resultArray = array();

// 	$orgId = Input::get('orgId');

// 	$dot = DB::table('dots')->where('orgId',$orgId)->first();

// 	$vision = '';
// 	if($dot)
// 	{
// 		$vision = $dot->vision;
// 	}

// 	$resultArray['vision'] = $vision;

//     //get tribeometer detail
// 	$queCatTbl = DB::table('tribeometer_questions_category')->get();

// 	$users = DB::table('tribeometer_answers')
// 	->leftjoin('users','users.id','tribeometer_answers.userId')
// 	->select('tribeometer_answers.userId')
// 	->where('users.status','Active')
// 	->groupBy('tribeometer_answers.userId')
// 	->where('users.orgId',$orgId)->get();

// 	$userCount = count($users);

// 	$optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
// 	$optCount = count($optionsTbl)-1;

// 	$tribeoResultArray = array();
// 	foreach ($queCatTbl as $value)
// 	{
// 		$questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();

// 		$quecount = count($questionTbl);

// 		$perQuePercen = 0;
// 		foreach ($questionTbl as  $queValue)
// 		{
// 			$diaAnsTbl = DB::table('tribeometer_answers')     
// 			->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
// 			->leftJoin('users','users.id','tribeometer_answers.userId')
// 			->where('users.status','Active')
// 			->where('tribeometer_answers.orgId',$orgId)
// 			->where('tribeometer_questions.id',$queValue->id)
// 			->where('tribeometer_questions.category_id',$value->id)
// 			->sum('answer');

//         //avg of all questions
// 			if ($userCount !=0)
// 			{
// 				$perQuePercen += ($diaAnsTbl/$userCount); 
// 			}
			
// 		}

// 		$score = ($perQuePercen/($quecount*$optCount));
// 		$totalPercentage = ($perQuePercen/($quecount*$optCount))*100;

// 		$value1['title']      =  $value->title;       
// 		$value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');      

// 		array_push($tribeoResultArray, $value1);
// 	}

// 	$resultArray['tribeometerDetail'] = $tribeoResultArray;


//  $user = Auth::user();

//  $userId = $user->id;

//  $isUser = DB::table('happy_indexes')->where('userId',$userId)->where('status','Active')->whereDate('created_at', date('Y-m-d'))->first();

//  $resultArray['userGivenfeedback'] = false;
//  if($isUser) 
//  {
//     $resultArray['userGivenfeedback'] = true;
// }


// /*get previous day happy index detail*/
// $userTypeArr = array('3','2','1');

// $totalIndexes = 0;
// $totalUser    = 0;
// foreach($userTypeArr as $type)
// {

//   $userCount = DB::table('happy_indexes')
//   ->leftjoin('users','users.id','happy_indexes.userId')
//   ->where('happy_indexes.status','Active')
//   ->where('happy_indexes.moodValue',$type)
//   ->where('users.orgId',$orgId)        
//   ->whereDate('happy_indexes.created_at', date('Y-m-d', strtotime('-1 days')))
//   ->count();

//   $totalUser +=$userCount;

//   if($type==3) 
//   {
//      $totalIndexes += $userCount;
//  }       
// }


// $happyIndexPer = 0;
// if($totalIndexes !=0 && $totalUser !=0)
// { 
//   $happyIndexPer = (($totalIndexes/($totalUser))*100);
// }

// $resultArray['happyIndex'] = round($happyIndexPer,2);

// //get yesterday's previous day happy index
// $totalIndexesOfyesterdaysPreviousday = 0;
// $totalUserOfyesterdaysPreviousday    = 0;
// foreach($userTypeArr as $type)
// {

//     $userCount = DB::table('happy_indexes')
//     ->leftjoin('users','users.id','happy_indexes.userId')
//     ->where('happy_indexes.status','Active')
//     ->where('happy_indexes.moodValue',$type)
//     ->where('users.orgId',$orgId)        
//     ->whereDate('happy_indexes.created_at', date('Y-m-d', strtotime('-2 days')))
//     ->count();

//     $totalUserOfyesterdaysPreviousday +=$userCount;

//     if($type==3) 
//     {
//         $totalIndexesOfyesterdaysPreviousday += $userCount;
//     }       
// }

// $happyIndexPerOfyesterdaysPreviousday = 0;
// if($totalIndexesOfyesterdaysPreviousday !=0 && $totalUserOfyesterdaysPreviousday !=0)
// {
//     $happyIndexPerOfyesterdaysPreviousday = ($totalIndexesOfyesterdaysPreviousday/($totalUserOfyesterdaysPreviousday))*100;       
// }

//     // =-1(previous day yesterday H.I. value – todays yesterday H.I. value) / previous day yesterday H.I. value 

// $happyIndexDD = 0;
// if($happyIndexPerOfyesterdaysPreviousday !=0) 
// {
//     $happyIndexDD = -1*($happyIndexPerOfyesterdaysPreviousday - $happyIndexPer)/$happyIndexPerOfyesterdaysPreviousday;
// }

// $resultArray['happyIndexDD'] =round($happyIndexDD,2);

//   //M.A.(First calculate Moving Average:) = (Total number of happy faces from all previous days/number of days)
//   //=-1(M.A – todays yesterday H.I. value) / M.A

// $happyFacesCount = DB::table('happy_indexes')
// ->leftjoin('users','users.id','happy_indexes.userId')
// ->where('happy_indexes.status','Active')
// ->where('happy_indexes.moodValue','3')
// ->where('users.orgId',$orgId)        
// ->whereDate('happy_indexes.created_at','<=', date('Y-m-d', strtotime('-1 days')))->count();


// $noOfDaysTbl = DB::table('happy_indexes')
// ->select(DB::raw('DATE(happy_indexes.created_at) as date'), DB::raw('count(*) as views'))
// ->groupBy('date')
// ->leftjoin('users','users.id','happy_indexes.userId')
// ->where('happy_indexes.status','Active')
// ->whereDate('happy_indexes.created_at','<=', date('Y-m-d', strtotime('-1 days')))
// ->where('users.orgId',$orgId)->get();

// $movingAvgIndex = 0;
// $noOfDays = count($noOfDaysTbl);

// if($noOfDays !=0 && $happyFacesCount !=0)
// {
//     $movingAvgIndex = ($happyFacesCount/$noOfDays);  
// }

// $maValue = 0;
// if($movingAvgIndex !=0 && $happyIndexPer !=0)
// {
//     $maValue = -1*($movingAvgIndex - $happyIndexPer)/$movingAvgIndex;  
// }

// $resultArray['happyIndexMA'] = round($maValue,2);

// return response()->json(['code'=>200, 'status'=>true,'service_name'=>'dashboard-detail','message'=>'','data'=>$resultArray]);

// }

//Index report for DOT completed Values
protected function indexReportForDotCompletedValues($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $userId   = $perArr["userId"];
    $departmentId   = $perArr["departmentId"];

    // print_r($orgId."<br>");
    // print_r($departmentId."<br>");
    $query = DB::table('dots_values')  
    ->select('dots_values.id AS id')     
    ->leftjoin('dots_beliefs','dots_beliefs.id','dots_values.beliefId')
    ->leftjoin('dots','dots.id','dots_beliefs.dotId')
    ->where('dots_beliefs.status','Active')
    ->where('dots.status','Active')
    ->where('dots_values.status','Active');
    if($orgId) 
    {
        $query->where('dots.orgId', $orgId);
    }
    $dotValues = $query->get();

    foreach($dotValues as $value)
    {
        $dvValueQuery = DB::table('dot_values_ratings')
        ->leftjoin('users','users.id','dot_values_ratings.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('dot_values_ratings.userId',$userId)
        ->where('dot_values_ratings.valueId',$value->id)
        ->where('dot_values_ratings.status','Active');
        if (!empty($departmentId)) {
            $dvValueQuery->where('departments.departmentId',$departmentId);
        }
        $dvValue = $dvValueQuery->first();
        if(!$dvValue)
        {
            return false;
        }
    }
    if (count($dotValues) == 0) {
      return false;
    }else{
      return true;
    }
    //return true;
}

//Index report for Team Role Completed Values
protected function indexReportForTeamroleCompleted($perArr = array())
{
    $orgId = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $query = DB::table('cot_answers') 
    ->select('cot_answers.userId')
    ->distinct()
    ->rightjoin('users','users.id','cot_answers.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->where('cot_answers.status','Active')
    ->where('cot_answers.orgId',$orgId);
    if (!empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }
    $result = $query->get();

    return $result;
}

//Index report for Personality Type Completed Values
protected function indexReportForpersonalityTypeCompleted($perArr = array())
{

    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $query = DB::table('cot_functional_lens_answers')
    ->select('cot_functional_lens_answers.userId')
    ->distinct()
    ->rightjoin('users','users.id','cot_functional_lens_answers.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->where('cot_functional_lens_answers.status','Active')
    ->where('cot_functional_lens_answers.orgId',$orgId);
    
    if (!empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }
    $result = $query->get();

    return $result;
}

//Index report for Culture structure Completed
protected function indexReportForCultureStructureCompleted($perArr = array())
{

    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $query = DB::table('sot_answers')   
    ->select('sot_answers.userId')
    ->distinct()      
    ->leftJoin('users','users.id','=','sot_answers.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('sot_answers.status','Active')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->where('sot_answers.updated_at','LIKE',date("Y-m", strtotime("-1 month"))."%")
    // ->where('sot_answers.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%")
    //->whereMonth('sot_answers.created_at', Carbon::now()->subMonth()->month)
    ->where('users.orgId',$orgId);

    if (!empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }  
    $result = $query->get();

    return $result;
}

//Index report for Culture structure Completed for all previous days
protected function indexReportForCultureStructureCompletedAllPreviousDays($perArr = array())
{

    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $query = DB::table('sot_answers')   
    ->select('sot_answers.userId')
    ->distinct()      
    ->leftJoin('users','users.id','sot_answers.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('sot_answers.status','Active')
    ->where('users.status','Active')
    // ->whereDate('sot_answers.created_at', '<', date('Y-m-d'))
    ->whereDate('sot_answers.updated_at', '<', date('Y-m-d'))
    ->where('users.orgId',$orgId);

    if (!empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }  
    $result = $query->get();

    return $result;
}

//Index report for motivation Completed
protected function indexReportForMotivationCompleted($perArr = array())
{
    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $query = DB::table('sot_motivation_answers')
    ->select('sot_motivation_answers.userId')
    ->distinct()      
    ->leftJoin('users','users.id','=','sot_motivation_answers.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('sot_motivation_answers.status','Active')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->where('users.orgId',$orgId);   

    if (!empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }  
    // if($orgId)
    // {
    //     $query->where('users.orgId',$orgId);
    // }
    $result = $query->get();

    return $result;
}

//Index report for Thumbsup Completed
protected function indexReportForThumbsupCompleted($perArr = array())
{
    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $query = DB::table('dot_bubble_rating_records')
    ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
    ->leftjoin('users','users.id','dot_bubble_rating_records.to_bubble_user_id')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('dots.status','Active')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->where('dot_bubble_rating_records.status','Active')
    //->whereDate('dot_bubble_rating_records.created_at',Carbon::today());                 
    ->whereDate('dot_bubble_rating_records.created_at',Carbon::yesterday());                 
    if(!empty($orgId))
    {
        $query->where('dots.orgId',$orgId);
    }
    if (!empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }  
    $bubbleCount = $query->count('bubble_flag');

    return $bubbleCount;
}

//Index report for Thumbsup Completed day before yesterday
protected function indexReportForThumbsupCompletedDayBeforeYesterday($perArr = array())
{
    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $dayBeforeYesterdayQuery = DB::table('dot_bubble_rating_records')
    ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
    ->leftjoin('users','users.id','dot_bubble_rating_records.to_bubble_user_id')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->whereDate('dot_bubble_rating_records.updated_at',date('Y-m-d', strtotime('-2 days')));                 
    //->whereDate('dot_bubble_rating_records.created_at',Carbon::yesterday());                 
    //->whereDate('dot_bubble_rating_records.created_at',date('Y-m-d', strtotime('-2 days')));                 
    if(!empty($orgId))
    {
        $dayBeforeYesterdayQuery->where('dots.orgId',$orgId);
    }
    if (!empty($departmentId)) {
        $dayBeforeYesterdayQuery->where('departments.departmentId',$departmentId);
    }  
    $dayBeforeYesterdayBubbleCount = $dayBeforeYesterdayQuery->count('bubble_flag');

    return $dayBeforeYesterdayBubbleCount;
}

//Index report for Thumbsup Completed all previous days
protected function indexReportForThumbsupCompletedAllPreviousDays($perArr = array())
{
    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $allPreviousDayQuery = DB::table('dot_bubble_rating_records')
    ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
    ->leftjoin('users','users.id','dot_bubble_rating_records.to_bubble_user_id')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->whereDate('dot_bubble_rating_records.updated_at', '<', date('Y-m-d'));
    // ->whereDate('dot_bubble_rating_records.created_at', '<', date('Y-m-d'));
    //->whereDate('dot_bubble_rating_records.created_at',date('Y-m-d', strtotime('-2 days')));                 
    if(!empty($orgId))
    {
        $allPreviousDayQuery->where('dots.orgId',$orgId);
    }
    if (!empty($departmentId)) {
        $allPreviousDayQuery->where('departments.departmentId',$departmentId);
    }  
    $allPreviousDayBubbleCount = $allPreviousDayQuery->count('bubble_flag');

    return $allPreviousDayBubbleCount;
}

//Index report for happy index Completed
protected function indexReportForHappyIndexCompleted($perArr = array())
{
    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $userTypeArr = array('3','2','1');//3 for happy and 2 for avrage and 1 for sad

    $totalIndexes = 0;
    $totalUser    = 0;
    foreach($userTypeArr as $type)
    {
        $happyUserCountQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('happy_indexes.status','Active')
        ->where('happy_indexes.moodValue',$type)
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('users.orgId',$orgId)        
        ->whereDate('happy_indexes.created_at', Carbon::yesterday());

        if (!empty($departmentId)) {
            $happyUserCountQuery->where('departments.departmentId',$departmentId);
        }  
        $happyUserCount = $happyUserCountQuery->count();

        $totalUser +=$happyUserCount;

        if($type==3) 
        {
            $totalIndexes += $happyUserCount;
        }       
    }

    $happyIndexcount = 0;
    if($totalIndexes !=0 && $totalUser !=0)
    { 
      $happyIndexcount = (($totalIndexes)/($totalUser))*100;
    }

    return $happyIndexcount;
}

//Index report for happy index Completed day before yesterday
protected function indexReportForHappyIndexCompletedDayBeforeYesterday($perArr = array())
{
    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $userTypeArr = array('3','2','1');

    $totalIndexes = 0;
    $totalUser    = 0;
    foreach($userTypeArr as $type)
    {
        $happyUserCountQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('happy_indexes.status','Active')
        ->where('happy_indexes.moodValue',$type)
        ->where('users.orgId',$orgId)        
        ->whereDate('happy_indexes.created_at', date('Y-m-d', strtotime('-2 days')));
        //->whereDate('happy_indexes.created_at', Carbon::yesterday());

        if (!empty($departmentId)) {
            $happyUserCountQuery->where('departments.departmentId',$departmentId);
        }  

        $happyUserCount = $happyUserCountQuery->count();

        $totalUser +=$happyUserCount;

        if($type==3) 
        {
            $totalIndexes += $happyUserCount;
        }       
    }

    $happyIndexcount = 0;
    if($totalUser !=0)
    { 
      $happyIndexcount = round((($totalIndexes/($totalUser))*100),2);
    }

    return $happyIndexcount;
}

//Index report for happy index Completed For all previous days
protected function indexReportForHappyIndexCompletedAllPreviousDays($perArr = array())
{
    $orgId    = $perArr['orgId'];
    $departmentId = $perArr['departmentId'];

    $userTypeArr = array('3','2','1');

    $totalIndexes = 0;
    $totalUser    = 0;
    foreach($userTypeArr as $type)
    {
        $happyUserCountQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('happy_indexes.status','Active')
        ->where('happy_indexes.moodValue',$type)
        ->where('users.orgId',$orgId)        
        ->whereDate('happy_indexes.created_at', '<', date('Y-m-d'));
       
        if (!empty($departmentId)) {
            $happyUserCountQuery->where('departments.departmentId',$departmentId);
        }  

        $happyUserCount = $happyUserCountQuery->count();

        $totalUser +=$happyUserCount;

        if($type==3) 
        {
            $totalIndexes += $happyUserCount;
        }       
    }

    $happyIndexcount = 0;
    if($totalUser !=0)
    { 
      $happyIndexcount = round((($totalIndexes/($totalUser))*100),2);
    }

    return $happyIndexcount;
}

//Index report for diagnostic answer
protected function indexReportForDiagAnsCompletedValues($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $userId   = $perArr["userId"];
    $departmentId   = $perArr["departmentId"];

    $isDiagnosticAnsDoneQuery  = DB::table('diagnostic_answers')
        ->leftjoin('users','users.id','diagnostic_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('diagnostic_answers.userId',$userId)
        ->where('diagnostic_answers.orgId',$orgId)
        ->where('diagnostic_answers.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('diagnostic_answers.updated_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        // ->where('diagnostic_answers.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        //->whereMonth('diagnostic_answers.created_at',Carbon::now()->subMonth()->month);

    if (!empty($departmentId)) {
        $isDiagnosticAnsDoneQuery->where('departments.departmentId',$departmentId);
    }  
    $isDiagnosticAnsDone = $isDiagnosticAnsDoneQuery->first();

    return $isDiagnosticAnsDone;
}

//Index report for diagnostic answer for all previous days
protected function indexReportForDiagAnsCompletedValuesForAllPreviousDays($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $userId   = $perArr["userId"];
    $departmentId   = $perArr["departmentId"];

    $isDiagnosticAnsDoneForAllPreviousDaysQuery  = DB::table('diagnostic_answers')
        ->leftjoin('users','users.id','diagnostic_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('diagnostic_answers.userId',$userId)
        ->where('diagnostic_answers.orgId',$orgId)
        ->where('diagnostic_answers.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->whereDate('diagnostic_answers.updated_at', '<', date('Y-m-d'));
        // ->whereDate('diagnostic_answers.created_at', '<', date('Y-m-d'));
       
    if (!empty($departmentId)) {
        $isDiagnosticAnsDoneForAllPreviousDaysQuery->where('departments.departmentId',$departmentId);
    }  
    $isDiagnosticAnsDoneForAllPreviousDays = $isDiagnosticAnsDoneForAllPreviousDaysQuery->first();

    return $isDiagnosticAnsDoneForAllPreviousDays;
}

//Index report for Tribe Ans Completed values
protected function indexReportForTribeAnsCompletedValues($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $userId   = $perArr["userId"];
    $departmentId   = $perArr["departmentId"];

    $isTribeometerAnsDoneQuery = DB::table('tribeometer_answers')
        ->leftjoin('users','users.id','tribeometer_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('tribeometer_answers.userId',$userId)
        ->where('tribeometer_answers.orgId',$orgId)
        ->where('tribeometer_answers.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('tribeometer_answers.updated_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        // ->where('tribeometer_answers.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        //->whereMonth('tribeometer_answers.created_at',Carbon::now()->subMonth()->month);

    if (!empty($departmentId)) {
        $isTribeometerAnsDoneQuery->where('departments.departmentId',$departmentId);
    } 
    $isTribeometerAnsDone = $isTribeometerAnsDoneQuery->first();
    return $isTribeometerAnsDone;
}

//Index report for Tribe Ans Completed values
protected function indexReportForTribeAnsCompletedValuesForAllPreviousDays($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $userId   = $perArr["userId"];
    $departmentId   = $perArr["departmentId"];

    $isTribeometerAnsDoneForAllPreviousDaysQuery = DB::table('tribeometer_answers')
        ->leftjoin('users','users.id','tribeometer_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('tribeometer_answers.userId',$userId)
        ->where('tribeometer_answers.orgId',$orgId)
        ->where('tribeometer_answers.status','Active')
        ->where('users.status','Active')
        ->whereDate('tribeometer_answers.updated_at', '<', date('Y-m-d'));
        // ->whereDate('tribeometer_answers.created_at', '<', date('Y-m-d'));

    if (!empty($departmentId)) {
        $isTribeometerAnsDoneForAllPreviousDaysQuery->where('departments.departmentId',$departmentId);
    } 
    $isTribeometerAnsDoneForAllPreviousDays = $isTribeometerAnsDoneForAllPreviousDaysQuery->first();
    return $isTribeometerAnsDoneForAllPreviousDays;
}

//Index report for value performance
protected function indexReportForValuePerformanceCompleted($perArr = array())
{
    $departmentId   = $perArr["departmentId"]; 
    $dotId   = $perArr["dotId"]; 

    $allDotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dotId)
        ->orderBy('id','ASC')
        ->get();
        $totalBeliefRatings = 0;
        $dotValuesCount = 0;
        foreach ($allDotBeliefs as $key => $bValue)
        {
            $dotValues = DB::table('dots_values')
            ->where('dots_values.status','Active')
            ->where('dots_values.beliefId',$bValue->id)           
            ->count();
            $dotValuesCount += $dotValues;

            $beliefRatingQuery = DB::table('dot_values_ratings')
            ->leftjoin('users','users.id','dot_values_ratings.userId')
            ->leftJoin('departments','departments.id','users.departmentId')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('dot_values_ratings.beliefId',$bValue->id)
            ->where('users.status','Active')
            ->where('departments.status','Active')
            ->where('all_department.status','Active')
            ->where('dot_values_ratings.status','Active');

            if (!empty($departmentId)) {
                $beliefRatingQuery->where('departments.departmentId',$departmentId);
            } 
            $beliefRating = $beliefRatingQuery->avg('ratings');
            if($beliefRating)
            {
                $totalBeliefRatings += $beliefRating-1;
            }
        }
        $avgRatings = $totalBeliefRatings;
        $valuesPerformance = ((($avgRatings/$dotValuesCount)/5)*100);

        return $valuesPerformance;
}

//Index report for Feedback completed
protected function indexReportForFeedbackCompleted($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $departmentId   = $perArr["departmentId"];

    $iotFeedbackQuery = DB::table('iot_feedbacks')
    ->select('iot_feedbacks.id')
    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->where('iot_feedbacks.status','!=','Inactive')
    ->where('iot_feedbacks.orgId',$orgId);
    //->where('iot_feedbacks.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
    //->whereMonth('iot_feedbacks.created_at',Carbon::now()->subMonth()->month);

    if (!empty($departmentId)) {
        $iotFeedbackQuery->where('departments.departmentId',$departmentId);
    } 
    $iotFeedback = $iotFeedbackQuery->count();

    return $iotFeedback;
}

//Index report for Feedback completed for all previous days
protected function indexReportForFeedbackCompletedForAllPreviousDays($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $departmentId   = $perArr["departmentId"];

    $iotFeedbackQuery = DB::table('iot_feedbacks')
    ->select('iot_feedbacks.id')
    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('users.status','Active')
    ->where('iot_feedbacks.status','!=','Inactive')
    ->where('iot_feedbacks.orgId',$orgId)
    ->whereDate('iot_feedbacks.created_at', '<', date('Y-m-d'));
    
    if (!empty($departmentId)) {
        $iotFeedbackQuery->where('departments.departmentId',$departmentId);
    } 
    $iotFeedback = $iotFeedbackQuery->count();

    return $iotFeedback;
}

//Index report for User count for tribeometer
protected function indexReportForUserCountForTribe($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $departmentId   = $perArr["departmentId"];

    $usersForTribeQuery = DB::table('tribeometer_answers')
    ->leftjoin('users','users.id','tribeometer_answers.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->select('tribeometer_answers.userId')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->groupBy('tribeometer_answers.userId')
    ->where('users.orgId',$orgId);

    if (!empty($departmentId)) {
        $usersForTribeQuery->where('departments.departmentId',$departmentId);
    } 
    $usersForTribe = $usersForTribeQuery->get();
    $userCountForTribe = count($usersForTribe);

    return $userCountForTribe;
}

protected function indexReportForTribeDnC($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $departmentId   = $perArr["departmentId"];
    $userCount   = $perArr["userCount"];

    $queCatTbl    = DB::table('tribeometer_questions_category')->get();
    $tribeDirectionnConnection = 0;
    if (!empty($userCountForTribe)){
        $optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
        $optCount = count($optionsTbl)-1; 

        foreach ($queCatTbl as $value){
            $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();
            $quecount = count($questionTbl);

            $perQuePercen = 0;
            foreach ($questionTbl as  $queValue){
                $diaQuery = DB::table('tribeometer_answers')           
                ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
                ->leftjoin('users','users.id','=','tribeometer_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.status','Active') 
                ->where('departments.status','Active') 
                ->where('all_department.status','Active') 
                ->where('tribeometer_answers.orgId',$orgId)
                ->where('tribeometer_questions.id',$queValue->id)
                ->where('tribeometer_questions.category_id',$value->id);

                if (!empty($departmentId)) {
                    $diaQuery->where('departments.departmentId',$departmentId);
                } 
                $diaAnsTbl = $diaQuery->sum('answer');

                //avg of all questions
                $perQuePercen += ($diaAnsTbl/$userCountForTribe); 
            }
            $score = ($perQuePercen/($quecount*$optCount));
            $totalPercentage = (($perQuePercen/($quecount*$optCount))*100)*4;
            $tribeDirectionnConnection += $totalPercentage;
        }
    }

    // $queCatTbl    = DB::table('tribeometer_questions_category')->get();

    // $optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
    //     $optCount = count($optionsTbl)-1; 
    //     $tribeDirectionnConnection = 0;
    //     foreach ($queCatTbl as $value)
    //     {
    //         $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();
    //         $quecount = count($questionTbl);

    //         $perQuePercen = 0;
    //         foreach ($questionTbl as  $queValue)
    //         {    
    //             $diaQuery = DB::table('tribeometer_answers')           
    //             ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
    //             ->leftjoin('users','users.id','=','tribeometer_answers.userId')
    //             ->leftJoin('departments','departments.id','users.departmentId')
    //             ->leftJoin('all_department','all_department.id','departments.departmentId')
    //             ->where('users.status','Active') 
    //             ->where('tribeometer_answers.orgId',$orgId)
    //             ->where('tribeometer_questions.id',$queValue->id)
    //             ->where('tribeometer_questions.category_id',$value->id);

    //             if (!empty($departmentId)) {
    //                 $diaQuery->where('departments.departmentId',$departmentId);
    //             } 
    //             $diaAnsTbl = $diaQuery->sum('answer');

    //                 //avg of all questions
    //             $perQuePercen += ($diaAnsTbl/$userCount); 
    //         }

    //         $score = ($perQuePercen/($quecount*$optCount));
    //         $totalPercentage = round(((($perQuePercen/($quecount*$optCount))*100)*4),2);

    //         $tribeDirectionnConnection += $totalPercentage;

    //     }

    return $tribeDirectionnConnection;
}

//Index report for User count for diagnostics
protected function indexReportForUserCountForDiagnostics($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $departmentId   = $perArr["departmentId"];

    $diagQuery = DB::table('diagnostic_answers')
    ->leftjoin('users','users.id','diagnostic_answers.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->select('diagnostic_answers.userId')
    ->where('users.status','Active')
    ->where('departments.status','Active') 
    ->where('all_department.status','Active') 
    ->groupBy('diagnostic_answers.userId')
    ->where('users.orgId',$orgId);

    if (!empty($departmentId)) {
        $diagQuery->where('departments.departmentId',$departmentId);
    } 
    $diagUsers = $diagQuery->get();
    $diagUserCount = count($diagUsers);

    return $diagUserCount;
}

protected function indexReportForDiagnosticCore($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $departmentId   = $perArr["departmentId"];
    $userCount   = $perArr["userCount"];
    //$diagnosticQueCatTbl = $perArr["diagnosticQueCatTbl"];

    $diagnosticQueCatTbl    = DB::table('diagnostic_questions_category')->where('id','!=',5)->get();

    $totalDiagnosticCore = 0;

    if (!empty($diagUserCount)){
        $diaOptionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
        $diaOptCount    = count($diaOptionsTbl); 

        $diagnosticCorePer = 0;
        foreach ($diagnosticQueCatTbl as $value){
            $diaQuestionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();
            $diaQuecount = count($diaQuestionTbl);

            $diaPerQuePercen = 0;
            foreach ($diaQuestionTbl as  $queValue){
                $diaQuery = DB::table('diagnostic_answers')            
                ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
                ->leftjoin('users','users.id','=','diagnostic_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.status','Active')   
                ->where('departments.status','Active') 
                ->where('all_department.status','Active') 
                ->where('diagnostic_answers.orgId',$orgId)
                ->where('diagnostic_questions.id',$queValue->id)
                ->where('diagnostic_questions.category_id',$value->id);

                if (!empty($departmentId)) {
                    $diaQuery->where('departments.departmentId',$departmentId);
                } 
                $diaAnsTbl = $diaQuery->sum('answer'); 
                $diaPerQuePercen += ($diaAnsTbl/$diagUserCount);               
            }
            $diaTotalPercentage = ($diaPerQuePercen/($diaQuecount*$diaOptCount))*100;
            $diagnosticCorePer += $diaTotalPercentage;
        }
        $totalDiagnosticCore = ($diagnosticCorePer/5);
    }
   
    // $diaOptionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
    // $diaOptCount = count($diaOptionsTbl); 

    // $diagnosticCorePer = 0;

    // foreach ($diagnosticQueCatTbl as $value)
    // {
    //     $diaQuestionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

    //     $diaQuecount = count($diaQuestionTbl);

    //     $diaPerQuePercen = 0;
    //     foreach ($diaQuestionTbl as  $queValue)
    //     {
    //         $diaQuery = DB::table('diagnostic_answers')            
    //         ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
    //         ->leftjoin('users','users.id','=','diagnostic_answers.userId')
    //         ->leftJoin('departments','departments.id','users.departmentId')
    //         ->leftJoin('all_department','all_department.id','departments.departmentId')
    //         ->where('users.status','Active')   
    //         ->where('diagnostic_answers.orgId',$orgId)
    //         ->where('diagnostic_questions.id',$queValue->id)
    //         ->where('diagnostic_questions.category_id',$value->id);

    //         if (!empty($departmentId)) {
    //             $diaQuery->where('departments.departmentId',$departmentId);
    //         } 

    //         $diaAnsTbl = $diaQuery->sum('answer'); 

    //         $diaPerQuePercen += ($diaAnsTbl/$userCount);               
    //     }

    //     $diaTotalPercentage = ($diaPerQuePercen/($diaQuecount*$diaOptCount))*100;

    //     $diagnosticCorePer += $diaTotalPercentage;
    // }

    // $totalDiagnosticCore = round(($diagnosticCorePer/5),2);

    return $totalDiagnosticCore;

}

protected function indexReportForDiagnosticStress($perArr = array())
{
    $orgId    = $perArr["orgId"];
    $departmentId   = $perArr["departmentId"];
    $userCount   = $perArr["userCount"];
    //$diagnosticQueCatTbl = $perArr["diagnosticQueCatTbl"];

    $diagnosticQueCatTbl    = DB::table('diagnostic_questions_category')->where('id',5)->get();

    $totalDiagnosticStress = 0;
    if (!empty($diagUserCount)){
        $diagOptionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
        $diagOptCount = count($diagOptionsTbl); 

        $diagnosticStress = 0;

        foreach ($diagnosticCatTbl as $value){
            $diagQuestionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();
            $diagQuecount = count($diagQuestionTbl);
            $diagPerQuePercen = 0;
            foreach ($diagQuestionTbl as  $queValue){
                $diaQuery = DB::table('diagnostic_answers')            
                ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
                ->leftjoin('users','users.id','=','diagnostic_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.status','Active')
                ->where('departments.status','Active') 
                ->where('all_department.status','Active')    
                ->where('diagnostic_answers.orgId',$orgId)
                ->where('diagnostic_questions.id',$queValue->id)
                ->where('diagnostic_questions.category_id',$value->id);

                if (!empty($departmentId)) {
                    $diaQuery->where('departments.departmentId',$departmentId);
                } 
                $diagAnsTbl = $diaQuery->sum('answer'); 

                $diagPerQuePercen += ($diagAnsTbl/$diagUserCount);               
            }

            // $diagScore = ($diagPerQuePercen/($diagQuecount*$diagOptCount));
            $diagTotalPercentage = ($diagPerQuePercen/($diagQuecount*$diagOptCount))*100;

            //$value1['title']      =  $value->title;
            //$value1['score']      =  number_format((float)$diagScore, 2, '.', '');
            // $value1['percentage'] =  number_format((float)$diagTotalPercentage, 2, '.', '');
            $diagnosticStress = (($diagTotalPercentage-100)*(-1))*3;    
            //array_push($diagnosticStressArray, $value1);
        }
        $totalDiagnosticStress = ($diagnosticStress);
    }
   
    // $diagOptionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
    //     $diagOptCount = count($diagOptionsTbl); 

    //     $diagnosticStress = 0;

    //     foreach ($diagnosticQueCatTbl as $value)
    //     {
    //         $diagQuestionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

    //         $diagQuecount = count($diagQuestionTbl);

    //         $diagPerQuePercen = 0;
    //         foreach ($diagQuestionTbl as  $queValue)
    //         {
    //             $diaQuery = DB::table('diagnostic_answers')            
    //             ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
    //             ->leftjoin('users','users.id','=','diagnostic_answers.userId')
    //             ->leftJoin('departments','departments.id','users.departmentId')
    //             ->leftJoin('all_department','all_department.id','departments.departmentId')
    //             ->where('users.status','Active')   
    //             ->where('diagnostic_answers.orgId',$orgId)
    //             ->where('diagnostic_questions.id',$queValue->id)
    //             ->where('diagnostic_questions.category_id',$value->id);

    //             if (!empty($officeId) && empty($departmentId)) {
    //                 $diaQuery->where('users.officeId',$officeId);
    //             }
    //             elseif (!empty($officeId) && !empty($departmentId)) {
    //                 $diaQuery->where('users.officeId',$officeId);
    //                 $diaQuery->where('users.departmentId',$departmentId);
    //             }
    //             elseif (empty($officeId) && !empty($departmentId)) {
    //                 $diaQuery->where('departments.departmentId',$departmentId);
    //             } 

    //             $diagAnsTbl = $diaQuery->sum('answer'); 

    //             $diagPerQuePercen += ($diagAnsTbl/$userCount);               
    //         }

    //         $diagTotalPercentage = ($diagPerQuePercen/($diagQuecount*$diagOptCount))*100;

    //         $diagnosticStress = (($diagTotalPercentage-100)*(-1))*3;    
    //     }
    //     $totalDiagnosticStress = round(($diagnosticStress),2);

    return $totalDiagnosticStress;

}


}
