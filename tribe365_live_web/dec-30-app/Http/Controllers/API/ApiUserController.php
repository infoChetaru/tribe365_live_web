<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Client;
use DB;

class ApiUserController extends Controller
{

    /* get user list */
    public function getUserList() {

        $postData = Input::get();

        $resultArray = array();

        $users = DB::table('users')
        ->select('users.id','users.name','users.email','users.officeId','users.departmentId','users.password2','all_department.department','all_department.id AS departmentPreDefineId','users.orgId','offices.office','organisations.organisation') 
        ->leftJoin('departments', 'departments.id', '=', 'users.departmentId')
        ->leftJoin('all_department','all_department.id', '=', 'departments.departmentId') 
        ->leftJoin('offices','offices.id','users.officeId')
        ->leftJoin('organisations','organisations.id','users.orgId')
        ->where('users.status','Active')
        ->where('users.officeId',$postData['officeId'])
        ->where('roleId',3)
        ->get();

        foreach ($users as $value)
        {

            $result['id'] = $value->id;
            $result['name'] = $value->name;
            $result['email'] = $value->email;
            $result['officeId'] = $value->officeId;
            $result['orgId'] = $value->orgId;
            $result['departmentId'] = $value->departmentId;
            $result['departmentPreDefineId'] = $value->departmentPreDefineId;
            $result['department'] = $value->department;
            $result['officeName'] = $value->office;
            $result['organisationName'] = $value->organisation;
            $result['password'] = base64_decode($value->password2);

            array_push($resultArray, $result);
        } 

        if(!empty($resultArray))
        {
            return response()->json(['code'=>200,'status'=>true,'service_name'=>'user-list','message'=>'User list','data'=>$resultArray]);
        }
        else
        {
            return response()->json(['code'=>400,'status'=>false,'service_name'=>'user-list','message'=>'User list not found','data'=>$resultArray]);
        }        
    }    
}
