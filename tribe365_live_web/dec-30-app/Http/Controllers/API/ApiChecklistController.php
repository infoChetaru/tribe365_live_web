<?php

namespace App\Http\Controllers\api;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use DB;
use Mail;
use View;

class ApiChecklistController extends Controller
{

	public function getAllAnswersStatus()
	{
		$resultArray = array();
		$ansStatus = array();	
		$toDoList = array();
		
		$userId = Input::get('userId');
		$month  = date('m');
		$year   = date('Y');
		$day    = date('d');

		$statusKey = $this->getVar('dot_values_ratings');
		$ansStatus[$statusKey]= $this->isDotValuecompleted($userId);

		$dataTableArr = array('dot_values_ratings','cot_functional_lens_answers','cot_answers','sot_answers','sot_motivation_answers','tribeometer_answers','diagnostic_answers');

		foreach ($dataTableArr as $value)
		{
			$tableStatus = DB::table($value)->where('userId',$userId)->first();	
			
			if($value !='dot_values_ratings')
			{
				$statusKey = $this->getVar($value);	
				if(!empty($tableStatus))
				{			
					$ansStatus[$statusKey]= true;
				}else{
					$ansStatus[$statusKey]= false;
				}
			}

			
			//get month wise status
			$queryDot = DB::table($value)->where('userId',$userId); 
			$queryDot->whereYear('updated_at','=', $year);
			$queryDot->whereMonth('updated_at','=', $month);
			$dotvaluesMonthsWise = $queryDot->first();

			if (empty($dotvaluesMonthsWise))
			{
				$queryDot = DB::table($value)->where('userId',$userId); 
				$queryDot->whereYear('created_at','=', $year);
				$queryDot->whereMonth('created_at','=', $month);
				$dotvaluesMonthsWise = $queryDot->first();
			}		

			if(!empty($dotvaluesMonthsWise))
			{				
				$toDoList[$statusKey]= true;
			}else{
				$toDoList[$statusKey]= false;
			}	
		}			
		//tribe values
		$querybubble = DB::table('dot_bubble_rating_records')->where('from_bubble_user_id',$userId); 
		$querybubble->whereDate('updated_at','=', date('Y-m-d'));		
		$bubbleRatings = $querybubble->first();

		if (empty($bubbleRatings))
		{
			$querybubble = DB::table('dot_bubble_rating_records')->where('from_bubble_user_id',$userId);
			$querybubble->whereDate('created_at','=', date('Y-m-d'));
			$bubbleRatings = $querybubble->first();
		}	

		if(!empty($bubbleRatings))
		{
			$toDoList['bubbleRatings'] = true;
		}else{
			$toDoList['bubbleRatings'] = false;
		}

		$resultArray['tribeStatus'] = $ansStatus;
		$resultArray['toDoList']    = $toDoList;

		return response()->json(['code'=>200,'status'=>true,'service_name'=>'check-list','message'=>'','data'=>$resultArray]);
	}

	public function getVar($value)
	{
		$statusKey='';
		if ($value =='dot_values_ratings')
		{
			$statusKey = 'tribeValue';
		}
		if ($value =='cot_functional_lens_answers')
		{
			$statusKey = 'functionalLens';
		}
		if ($value =='cot_answers')
		{
			$statusKey = 'teamRoleAnswers';
		}
		if ($value =='sot_answers')
		{
			$statusKey = 'cultureStructure';
		}
		if ($value =='sot_motivation_answers')
		{
			$statusKey = 'motivation';
		}
		if ($value =='diagnostic_answers')
		{
			$statusKey = 'diagnostic';
		}
		if ($value =='tribeometer_answers')
		{
			$statusKey = 'tribeometer';
		}
		return $statusKey;
	}

	//check dot value rating completed or not
	public function isDotValuecompleted($userId)
	{
		$user = DB::table('users')->where('id',$userId)->first();

		if (!empty($user))
		{
			$dotsValuesArr = DB::table('dots_values')  
			->select('dots_values.id','dots_values.beliefId')				
			->leftJoin('dots_beliefs', 'dots_beliefs.id', '=', 'dots_values.beliefId') 
			->leftJoin('dots', 'dots.id', '=', 'dots_beliefs.dotId')   		  		
			->where('dots.orgId',$user->orgId)
			->where('dots_beliefs.status','Active')
			->where('dots_values.status','Active')
			->get();

			foreach ($dotsValuesArr as $dotVal)
			{
				$dotValStatus = DB::table('dot_values_ratings')->where('valueId',$dotVal->id)->where('userId',$userId)->first();	

				if(empty($dotValStatus))
				{			
					return false;
				}
			}
			
			return true;
		}
		else
		{
			return false;
		}

		
	}

}