<?php
//tribe365
namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Client;
use DB;
use Mail;



class ApiLoginController extends Controller
{
  public $successStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {

      $email          = Input::get('email');
      $password       = Input::get('password');
      $role           = Input::get('role'); 
      $deviceId       = Input::get('deviceId');
      $fcmToken       = Input::get('fcmToken');

      $resultArray = array();

      switch ($role) {

       case 1:
       // "Supen admin";

       if(Auth::attempt(array('email'=>$email,'password'=>$password,'roleId'=>1,'status'=>'Active')))
       {

         $success = array();

         $user = Auth::user(); 

         DB::table('oauth_access_tokens')->where('user_id', $user->id)->update(['revoked'=>1]);

         //For push notification when user is logout
         // $test = $request->user('api')->token()->revoke();
         
         $success = DB::table('users')    
         ->select('users.imageUrl','users.id','users.name','users.email','users.officeId','users.departmentId','offices.office','all_department.department','organisations.ImageURL','organisations.organisation') 

         ->leftJoin('offices', 'users.officeId', '=', 'offices.id')   
         ->leftJoin('departments', 'users.departmentId', '=', 'departments.id')
         ->leftJoin('all_department','all_department.id', '=', 'departments.departmentId') 
         ->leftJoin('organisations','offices.orgId', '=', 'organisations.id')
         ->where('users.id',$user->id)           
         ->get();

         foreach ($success as  $value)
         {

          $result['id'] = $value->id;              
          $result['name'] = $value->name;
          $result['email'] = $value->email;
          $result['officeId'] = $value->officeId;
          $result['departmentId'] = $value->departmentId;
          $result['orgname']      = $value->organisation;
          $result['office'] = $value->office;
          $result['department'] = $value->department;
          $result['organisation_logo'] = url('/public/uploads/org_images').'/'. $value->ImageURL;
          $result['role'] = $role;

          $result['profileImage'] = '';
          if($value->imageUrl)
          {
            $result['profileImage'] = url('/public/uploads/user_profile').'/'. $value->imageUrl;
          }


          $result['token'] = $user->createToken('tribe365')->accessToken;

          array_push($resultArray, $result);
        }

        return response()->json(['code'=>200,'status'=>true,'service_name'=>'user-login','message'=>'User logged in successfully','data'=>$resultArray[0]]);
        

      } else {

        return response()->json(['code'=>400,'status'=>false,'service_name'=>'user-login','message'=>'Credential not match','data'=>$resultArray]);
        break;
      }

      break;

      case 2:
      return response()->json(['code'=>400,'status'=>false,'service_name'=>'user-login','message'=>'User type is Client.','data'=>$resultArray]);
      break;

      case 3:

      if(Auth::attempt(array('email'=>$email,'password'=>$password,'roleId'=>3,'status'=>'Active')))
      {

        $success = array();

        $user = Auth::user(); 

        DB::table('oauth_access_tokens')->where('user_id', $user->id)->update(['revoked'=>1]);

        //For push notification when user is logout
        // $test = $request->user('api')->token()->revoke();

        //update device token
        DB::table('users')->where('id',$user->id)->update(['fcmToken' => $fcmToken,'deviceId'=>$deviceId]);

        $success = DB::table('users')    
        ->select('users.imageUrl','users.id','users.name','users.orgId','users.email','users.officeId','users.departmentId','offices.office','all_department.department','organisations.ImageURL','organisations.organisation') 
        ->leftJoin('offices', 'users.officeId', '=', 'offices.id')   
        ->leftJoin('departments', 'users.departmentId', '=', 'departments.id')
        ->leftJoin('all_department','all_department.id', '=', 'departments.departmentId')
        ->leftJoin('organisations','offices.orgId', '=', 'organisations.id')
        ->where('users.id',$user->id)           
        ->get();

        foreach ($success as  $value)
        {

          $result['id']                = $value->id;              
          $result['name']              = $value->name;
          $result['email']             = $value->email;
          $result['officeId']          = $value->officeId;
          $result['departmentId']      = $value->departmentId;
          $result['orgname']           = $value->organisation;
          $result['office']            = $value->office;
          $result['department']        = $value->department;
          $result['organisation_logo'] = url('/public/uploads/org_images').'/'. $value->ImageURL;
          $result['role']              = $role;
          $result['orgId']             = $value->orgId;    

          $result['profileImage'] = '';
          if($value->imageUrl)
          {
            $result['profileImage'] = url('/public/uploads/user_profile').'/'. $value->imageUrl;
          }       

          $result['token']             = $user->createToken('tribe365')->accessToken;
          $dotStaus = DB::table('dots')->where('orgId',$value->orgId)->first();

          if($dotStaus)
          {
            $result['isDot']  = true; 
          } 
          else
          {
            $result['isDot']  =false;
          }

          array_push($resultArray, $result);

        }

        return response()->json(['code'=>200,'status'=>true,'service_name'=>'user-login','message'=>'User logged in successfully','data'=>$resultArray[0]]);
      }
      else
      {

        if(Auth::attempt(array('email'=>$email,'password'=>$password,'roleId'=>3,'status'=>'Inactive')))
        {
          return response()->json(['code'=>400,'status'=>false,'service_name'=>'user-login','message'=>'You are no longer the authorized user of this application.','data'=>$resultArray]);
        }

        return response()->json(['code'=>400,'status'=>false,'service_name'=>'user-login','message'=>'Credential not match','data'=>$resultArray]);
      }

      break;

      default:
      return response()->json(['code'=>400,'status'=>false,'service_name'=>'user-login','message'=>'Credential not match','data'=>$resultArray]);
    }

  }

   /** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
   public function details() 
   { 

    $user = Auth::user();

    // $resultArray =array(
    //   'id'=> $user->id,
    //   'name'=>$user->name,
    //   'email'=>$user->email,
    //   'orgId'=>$user->orgId,
    //   'officeId'=> $user->officeId,
    //   'departmentId'=> $user->departmentId,
    //   'status'=> $user->status,
    //   'roleId'=> $user->roleId,

    // );

    $value = DB::table('users')    
    ->select('users.id','users.roleId','users.status','users.contact','users.name','users.imageUrl AS profileImage','users.orgId','users.email','users.officeId','users.departmentId','offices.office','all_department.department','organisations.ImageURL','organisations.organisation') 
    ->leftjoin('offices', 'users.officeId', '=', 'offices.id')   
    ->leftjoin('departments', 'users.departmentId', '=', 'departments.id')
    ->leftJoin('all_department','all_department.id', '=', 'departments.departmentId') 
    ->leftjoin('organisations','offices.orgId', '=', 'organisations.id')
    ->where('users.id',$user->id)           
    ->first();


    if($value)
    {

      $resultArray['id']                 = $value->id;              
      $resultArray['name']               = $value->name;
      $resultArray['email']              = $value->email;
      $resultArray['officeId']           = $value->officeId;
      $resultArray['departmentId']       = $value->departmentId;               
      $resultArray['role']               = $value->roleId;
      $resultArray['status']             = $value->status;
      $resultArray['userContact']        = $value->contact;
      $resultArray['orgId']              = $value->orgId;
      $resultArray['organisationName']   = $value->organisation;
      $resultArray['officeName']         = $value->office;
      $resultArray['departmentName']     = $value->department;


      
      $resultArray['profileImage']       = '';
      if(!empty($value->profileImage))
      {
        $resultArray['profileImage']       = url('/public/uploads/user_profile').'/'.$value->profileImage;
      }

      $resultArray['organisation_logo']       = '';
      if(!empty($value->ImageURL))
      {
        $resultArray['organisation_logo']  = url('/public/uploads/org_images').'/'. $value->ImageURL;
      }
      
    }

    $resultArray['cotTeamRoleMap']      = '';
    $resultArray['sotDetail']           = '';
    $resultArray['sotMotivationDetail'] = '';

    //get COT team role map
    $cotTeamRoleResultArray = array();
    
    $mapers = DB::table('cot_role_map_options')->where('status','Active')->orderBy('id','ASC')->get();

    $users = DB::table('users')->select('id','name','orgId')->where('id',$user->id)->where('status','Active')->orderBy('id','DESC')
    ->get();

    $usersArray = array();

    foreach($users as $key => $value) 
    {

      foreach($mapers as $key => $maper)
      {
        $maperCount = DB::table('cot_answers')->where('orgId',$value->orgId)->where('userId',$value->id)->where('cot_role_map_option_id',$maper->id)->where('status','Active')->sum('answer');

        $cotTeamRoleResultArray = array();

        $Value[$maper->maper_key] = $maperCount;    

        array_push($cotTeamRoleResultArray, $Value);
      }   

      arsort($cotTeamRoleResultArray[0]);

      $i = 0;
      $prev = "";
      $j = 0;

      $new  = array();
      $new1 = array();

      foreach($cotTeamRoleResultArray[0] as $key => $val)
      { 
        if($val != $prev)
        { 
          $i++; 
        }

        $new1['title'] = $key; 
        $new1['value'] = $i;        
        $prev = $val;
        
        if ($key==$mapers[0]->maper_key) {
          $new1['priority'] = 1;
        }elseif ($key==$mapers[1]->maper_key) {
          $new1['priority'] = 2;
        }elseif ($key==$mapers[2]->maper_key) {
          $new1['priority'] = 3;
        }elseif ($key==$mapers[3]->maper_key) {
          $new1['priority'] = 4;
        }elseif ($key==$mapers[4]->maper_key) {
          $new1['priority'] = 5;
        }elseif ($key==$mapers[5]->maper_key) {
          $new1['priority'] = 6;
        }elseif ($key==$mapers[6]->maper_key) {
          $new1['priority'] = 7;
        }elseif ($key==$mapers[7]->maper_key) {
          $new1['priority'] = 8;
        }

        array_push($new,$new1);
      } 

      $value->new = $new;

      $value->totalKeyCount = $cotTeamRoleResultArray[0];
      
      foreach($mapers as $mapValue)
      {
        $mapersArray = array();
        
        $res[$mapValue->maper_key] = $mapValue->maper;

        array_push($mapersArray, $res);
      }

      $value->mapersArray = $mapersArray[0];

      array_push($usersArray, $value);
    }
    

    $usersArray = $this->sortarr($usersArray);
    // print_r($usersArray);
    // dd();
    //convert array to string
    foreach ($usersArray as $key => $finalValue);

    $teamArr = array();
    $i=1;
    foreach($finalValue as $key => $value)
    {

      $mapers = DB::table('cot_role_map_options')->select('maper')->where('maper_key',$key)->where('status','Active')->first();

      array_push($teamArr, $mapers->maper);

      if($i==3)
      {
        break;
      }
      $i++;
    }

    $resultArray['cotTeamRoleMap'] = implode(', ',$teamArr);

    //personality type

    $cotFunResultArray = array();

    $query = DB::table('cot_functional_lens_answers')
    ->select('cot_functional_lens_answers.userId AS id')
    ->leftjoin('users','users.id','cot_functional_lens_answers.userId')    
    ->where('users.status','Active')
    ->groupBy('cot_functional_lens_answers.userId')
    ->where('users.id',$user->id);

    $usersList = $query->get();

    foreach ($usersList as $userValue)
    {

      $variable = app('App\Http\Controllers\Admin\CommonController')->getCotFunLensInitialValue();

      $initialValEIArray = array();

      foreach ($variable as $value)
      {

        $value1 = $value[0];
        $value2 = $value[1];

        $countE = DB::table('cot_functional_lens_answers AS cfla')
        ->select('cflqo.option_name AS optionName')
        ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
        ->leftJoin('users','users.id','=','cfla.userId')
        ->where('users.status','Active')
        ->where('cfla.userId',$userValue->id)->where('cflqo.initial_value_id',$value1)->get();

        $countI = DB::table('cot_functional_lens_answers AS cfla')
        ->select('cflqo.option_name AS optionName')
        ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
        ->leftJoin('users','users.id','=','cfla.userId')
        ->where('users.status','Active')
        ->where('cfla.userId',$userValue->id)->where('cflqo.initial_value_id',$value2)->get();

        $initialValEI  = '';
        if(count($countE) > count($countI))
        {            
          $initialValEI  =  $value1;
        }
        elseif (count($countE) < count($countI))
        {           
          $initialValEI  =  $value2;
        }
        elseif(count($countE) == count($countI))
        {
         $initialValEI  =  $value1;
       }           
       array_push($initialValEIArray, $initialValEI);

     }


     $valueTypeArray = array();
     $valueTypeArray['value'] = array();

     if(!empty($initialValEIArray))
     {
      $valueTypeArray = array('value'=>$initialValEIArray);
    }

    $matchValueArr =array();
    for($i=0; $i < count($valueTypeArray['value']); $i++)
    {
      $valuesKey = '';
      $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('title',$valueTypeArray['value'][$i])->first();  

      if(!empty($table))
      {
        $valuesKey = $table->value;
      }

      array_push($matchValueArr, $valuesKey); 
    }

    $tribeMatchValue = json_encode(array_slice($valueTypeArray['value'], 1, -1));

    $tribeTipsList = DB::table('cot_functional_lens_tribe_tips_records')->where('value',$tribeMatchValue)->get();

    $tribeTipsArray = array();
    foreach ($tribeTipsList as $ttvalue)
    {
      $tTips['value']   = $ttvalue->value;       
      array_push($cotFunResultArray, $tTips);
    }
  }

  $ST =0;
  $SF =0;
  $NF =0;
  $NT =0;

  foreach ($cotFunResultArray as $finalArrayValue)
  { 
   if($finalArrayValue['value']=='["7","9"]'){

     $ST++;
   }elseif ($finalArrayValue['value']=='["7","10"]'){

     $SF++;
   }elseif ($finalArrayValue['value']=='["8","10"]'){

     $NF++;
   }elseif ($finalArrayValue['value']=='["8","9"]'){

     $NT++;
   }

 }

 $totalUser = count($usersList);

 if(!empty($totalUser))
 {
  $stPercent = number_format((($ST/$totalUser)*100));
  $sfPercent = number_format((($SF/$totalUser)*100));
  $nfPercent = number_format((($NF/$totalUser)*100));
  $ntPercent = number_format((($NT/$totalUser)*100));
}
else
{
  $stPercent = 0;
  $sfPercent = 0;
  $nfPercent = 0;
  $ntPercent = 0;
}

$funcLensPercentageArray = array('st'=>$stPercent,'sf'=>$sfPercent,'nf'=>$nfPercent,'nt'=>$ntPercent);

$tribeTipsListKey = DB::table('cot_functional_lens_tribe_tips_records')->where('status','Active')->orderBy('id','ASC')->get();

$keyNameArray = array();
foreach ($tribeTipsListKey as $value1)
{
  $reportKeyArray = json_decode($value1->value);

  $keyArr = array();
  foreach ($reportKeyArray as $value)
  {
    $valuesKey = '';
    $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$value)->first();  

    if(!empty($table))
    {
      $valuesKey = $table->value;
    }   
    array_push($keyArr, $valuesKey); 
  }
  array_push($keyNameArray, $keyArr);
}


//set values of ST
$sotStKey ='';
if(!empty($keyNameArray[0][0]) && !empty($keyNameArray[0][1])) 
{
  $sotStKey =  $keyNameArray[0][0].$keyNameArray[0][1];

  if(!empty($funcLensPercentageArray['st']))
  {
   $stValue =  $funcLensPercentageArray['st'];
 } 
 else
 {
  $stValue = '0%';
}
}

$sotSfKey = '';
if(!empty($keyNameArray[3][0]) && !empty($keyNameArray[3][1])) 
{
  $sotSfKey =  $keyNameArray[3][0].$keyNameArray[3][1];

  if(!empty($funcLensPercentageArray['sf']))
  {
   $sfValue =  $funcLensPercentageArray['sf'];
 } 
 else
 {
  $sfValue = '0';
}
}

$sotNtKey = '';
if(!empty($keyNameArray[1][0]) && !empty($keyNameArray[1][1])) 
{
  $sotNtKey =  $keyNameArray[1][0]. $keyNameArray[1][1];

  if(!empty($funcLensPercentageArray['nt']))
  {
   $ntValue =  $funcLensPercentageArray['nt'];
 } 
 else
 {
  $ntValue = '0';
}
}

$sotNfKey = '';
if(!empty($keyNameArray[2][0]) && !empty($keyNameArray[2][1])) 
{
  $sotNfKey =  $keyNameArray[2][0].$keyNameArray[2][1];

  if(!empty($funcLensPercentageArray['nf']))
  {
    $nfValue =  $funcLensPercentageArray['nf'];
  } 
  else
  {
    $nfValue = '0';
  }
}
//set value for sot values
$resultArray['sotDetail'] = $sotStKey.'('.$stValue.'), '.$sotSfKey.'('.$sfValue.'), '.$sotNtKey.'('.$ntValue.'), '.$sotNfKey.'('.$nfValue.')';



//for motivation

$categoryTbl = DB::table('sot_motivation_value_records')->where('status','Active')->get();

$motvArr = array();
foreach ($categoryTbl as $value)
{  

  $ansTbl = DB::table('sot_motivation_answers AS sotans')->leftJoin('sot_motivation_question_options AS qoption', 'qoption.id', '=', 'sotans.optionId')
  ->leftjoin('users','users.id','sotans.userId')->where('users.status','Active')
  ->where('sotans.orgId',$user->orgId)->where('sotans.status','Active')->where('qoption.category_id',$value->id)->where('sotans.userId',$user->id)->sum('sotans.answer'); 
  $resultArr[$value->title] = (string)$ansTbl;

}

arsort($resultArr);

//
$finalMotArr = array();
$j=1;
foreach ($resultArr as $key => $value)
{  
  array_push($finalMotArr, $key);

  if($j==3)
  {
    break;
  }
  $j++;
}

// print_r($result1);

$resultArray['sotMotivationDetail'] = implode(', ', $finalMotArr);


return response()->json(['code'=>200,'status'=>true,'service_name'=>'user-profile','message'=>'User Profile','data'=>$resultArray]);

} 


/*sorting array by priority*/
function sortarr($arr1)
{

  $arrUsers = array();
  $newArray = array();
  $tes = array();

  for($i=0; $i<count($arr1); $i++)
  {    

    $arrt = (array)$arr1[$i]->new;

    usort($arrt, function($x, $y)
    {

     if ($x['value']== $y['value'] ) 
     {
      if($x['priority']<$y['priority'])
      {
        return 0;
      }
      else
      {
        return 1;
      } 
    }
  }); 

    $tt = array();
    foreach($arrt as $key => $value)
    {         
      if($value['value']!=0)
      {
        $tes[$value['title']] = $key+1;
      } 
      else 
      {
        $tes[$value['title']] = 0;
      }
    }   

    array_push($newArray,(object)$tes);
  }

  return $newArray;

} 

/*For user logout*/
public function logout(request $request)
{

 $test = $request->user('api')->token()->revoke();

 return response()->json(['code'=>200,'status'=>true,'service_name'=>'user-logout','message'=>'successfully logout']);
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*update user profile*/
    public function updateUserProfile()
    {

     $resultArray = array();

     $name     = Input::get('name');
     $password = Input::get('password');
     $officeId = Input::get('officeId');
     $departmentId = Input::get('departmentId');
     
     $id       = Auth::user()->id;


     if(Input::get('profileImage'))
     {

      $image = Input::get('profileImage');
      $image = str_replace('data:image/png;base64,', '', $image);
      $image = str_replace(' ', '+', $image);
      $imageName = 'user_'.time().'.png'; 
      \File::put(public_path('uploads/user_profile/'). $imageName, base64_decode($image));

      $updateArray['imageUrl']  = $imageName;

    } 

    $updateArray['name']      = $name;
    $updateArray['contact']   = Input::get('contact');
    $updateArray['officeId']  = $officeId;
    $updateArray['departmentId'] = $departmentId;

    $status = DB::table('users')->where('id',$id)->update($updateArray);

    $value = DB::table('users')    
    ->select('users.id','users.roleId','users.status','users.contact','users.name','users.imageUrl AS profileImage','users.orgId','users.email','users.officeId','users.departmentId','offices.office','all_department.department','organisations.ImageURL','organisations.organisation') 
    ->leftjoin('offices', 'users.officeId', '=', 'offices.id')   
    ->leftjoin('departments', 'users.departmentId', '=', 'departments.id')
    ->leftJoin('all_department','all_department.id', '=', 'departments.departmentId') 
    ->leftjoin('organisations','offices.orgId', '=', 'organisations.id')
    ->where('users.id',$id)           
    ->first();


    if($value)
    {

      $resultArray['id']                 = $value->id;              
      $resultArray['name']               = $value->name;
      $resultArray['email']              = $value->email;
      $resultArray['officeId']           = $value->officeId;
      $resultArray['departmentId']       = $value->departmentId;               
      $resultArray['role']               = $value->roleId;
      $resultArray['status']             = $value->status;
      $resultArray['userContact']        = $value->contact;
      $resultArray['orgId']              = $value->orgId;
      $resultArray['organisationName']   = $value->organisation;
      $resultArray['officeName']         = $value->office;
      $resultArray['departmentName']     = $value->department;


      
      $resultArray['profileImage']       = '';
      if(!empty($value->profileImage))
      {
        $resultArray['profileImage']       = url('/public/uploads/user_profile').'/'.$value->profileImage;
      }

      $resultArray['organisation_logo']       = '';
      if(!empty($value->ImageURL))
      {
        $resultArray['organisation_logo']  = url('/public/uploads/org_images').'/'. $value->ImageURL;
      }
      
    }

    if($status)
    {
      return response()->json(['code'=>200,'status'=>true,'service_name'=>'update-user-profile','message'=>'Profile updated successfully.','data'=>$resultArray]);
    }


    return response()->json(['code'=>400,'status'=>false,'service_name'=>'user-login','message'=>'Profile not updated.','data'=>$resultArray]);

  }

  /*FORGOT PASSWORD FUNCTIONALITY*/

  /*For Reset Password Mail*/
  public function forgotPassword()
  {

    $email    = Input::get('email')?Input::get('email'):'';

    $result   = DB::table('users')->where('email',$email)->where('status','Active')->first();
// ->where('roleId',3)
    if($result){

      $token = array('token' => str_random(50),'created_at'=>date('Y-m-d :H-i-s'));

      $count   = DB::table('password_resets')->where('email',$email)->first();

      if($count){

        DB::table('password_resets')->where('email',$email)->update($token);

      }else{

        DB::table('password_resets')->insert(['email' => $result->email, 'token' => $token['token']]);
      }         

      $data  = array('name'=>$result->name ,'passwordLink'=>$token['token']);

      Mail::send('mail', $data, function($message) use($email) 
      {
        $message->from('notification@tribe365.co', 'Tribe365');
        $message->to($email);
        $message->subject('Reset Password');
      });

      return response()->json(['status'=>true,'message'=>'Mail has been sent. Please Check.']);

    }else{

      return response()->json(['status'=>false,'message'=>'Email not Found.']);
    }
  }

  /*To Check Password Link*/
  public function resetPassword()
  {

   $token      = request()->segment(2);

   $result     = DB::table('password_resets')
   ->where('token',$token)
   ->first();

   if($result)
   {        
     $time       = $result->created_at;
     $time       = date('Y-m-d H:i',strtotime('+10 minutes',strtotime($time)));
     $checkTime  = date('Y-m-d H:i');

     if($checkTime < $time )
     {

       $data = array('email'=>$result->email,'token'=>'updatepassword/'.$result->token);

       return view('resetPassword',compact('data'));

     }else{

      echo '<p style="text-align: center;background: #eb1c24;color: #fff; font-size: 25px;font-weight: bold; padding: 15px 5px;
      text-align: center;">Invalid Link</p>';
    }

  }else{

    echo '<p style="text-align: center;background: #eb1c24;color: #fff; font-size: 25px;font-weight: bold; padding: 15px 5px;
    text-align: center;">Invalid Link</p>';
  }     
}


/*To update password*/
public function updatePassword()
{

 $token            = request()->segment(2);
 $email            = Input::get('email')?Input::get('email'):'';
 $password         = Input::get('password')?Input::get('password'):'';
 $confirm_password = Input::get('confirm_password')?Input::get('confirm_password'):'';

 $result           = DB::table('password_resets')->where('token',$token)->where('email',$email)->first();

 $rules = array('email'=>'required','password'=>'required','confirm_password'=>'required|same:password'); 

 if($result)
 {                                             
   $Validator   = Validator::make(Input::all(),$rules);

   if($Validator->fails())
   {

     return redirect()->back()->withErrors($Validator->errors())->withInput(Input::all());

   }else { 

     $updateArray = array('password'=> str_replace("&nbsp;", '',bcrypt($password)),'password2'=> str_replace("&nbsp;", '',base64_encode($password)));

     DB::table('users')->where('email',$email)->update($updateArray);
     
     return redirect()->back()->with(['message'=>'Password updated successfully.']);
     
   }

 }else{

   echo '<p style="text-align: center;background: #eb1c24;color: #fff; font-size: 25px;font-weight: bold; padding: 15px 5px;
   text-align: center;">Invalid Link</p>';
 } 

}

/*update user password with current password*/

public function updatePasswordWithCurrentPassword()
{
  $resultArray = array();

  $currentPassword = Input::get('currentPassword');
  $newPassword     = Input::get('newPassword');
  $user            = Auth::user();
  $id              = $user->id;


  $result = DB::table('users')->where('status','Active')->where('id',$id)->where('password2',base64_encode($currentPassword))->first();

  if(!$result)
  {   
    return response()->json(['code'=>400,'status'=>false,'service_name'=>'update-password-with-current-password','message'=>'Your current password is not correct.','data'=>$resultArray]);
  }

  $updatetArray['password']   = str_replace("&nbsp;", '',bcrypt($newPassword));
  $updatetArray['password2']  = str_replace("&nbsp;", '',base64_encode($newPassword));       
  $updatetArray['updated_at'] = date('Y-m-d H:i:s');


  DB::table('users')->where('id',$id)->update($updatetArray); 

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'update-password-with-current-password','message'=>'Password updated successfully.','data'=>$resultArray]);


}



/*To Check Password Link*/
  public function resetPasswordImprovement(){

     $token      = request()->segment(2);
     $result     = DB::table('password_resets_improvement')
     ->where('token',$token)
     ->first();

     if($result){        
       $time       = $result->created_at;
       $time       = date('Y-m-d H:i',strtotime('+10 minutes',strtotime($time)));
       $checkTime  = date('Y-m-d H:i');

       if($checkTime < $time ){
         $data = array('email'=>$result->email,'token'=>'updatepasswordImprovement/'.$result->token);
         return view('resetPasswordImprovement',compact('data'));
       }else{

        echo '<p style="text-align: center;background: #eb1c24;color: #fff; font-size: 25px;font-weight: bold; padding: 15px 5px;
        text-align: center;">Invalid Link</p>';
      }
    }else{
      echo '<p style="text-align: center;background: #eb1c24;color: #fff; font-size: 25px;font-weight: bold; padding: 15px 5px;
      text-align: center;">Invalid Link</p>';
    }     
}



/*To update password*/
public function updatepasswordImprovement(){

   $token            = request()->segment(2);
   $email            = Input::get('email')?Input::get('email'):'';
   $password         = Input::get('password')?Input::get('password'):'';
   $confirm_password = Input::get('confirm_password')?Input::get('confirm_password'):'';
   $result           = DB::table('password_resets_improvement')
                          ->where('token',$token)
                          ->where('email',$email)->first();
   $rules            = array('email'=>'required','password'=>'required','confirm_password'=>'required|same:password'); 

   if($result) {                                             
       $Validator   = Validator::make(Input::all(),$rules);
       if($Validator->fails()){
          return redirect()->back()->withErrors($Validator->errors())->withInput(Input::all());
       }else { 

          //Update password
          $updateArray = array('password_md5'=> md5($password));
          DB::table('improvements_credentials')->where('email',$email)->update($updateArray);     
          return redirect()->back()->with(['message'=>'Password updated successfully.']);    
       }

   }else{
       echo '<p style="text-align: center;background: #eb1c24;color: #fff; font-size: 25px;font-weight: bold; padding: 15px 5px;
       text-align: center;">Invalid Link</p>';
   } 

}


}
