<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Client;
use DB;

class ApiRatingsController extends Controller
{

	/*to give ratings to the dot values*/
	public function ratingsToDotValues(){

		$postData = Input::all();
		
		$resultArray = array();

		$status = DB::table('dot_values_ratings')
		->where('userId',$postData['userId'])
		->where('dotId',$postData['dotId'])
		->where('beliefId',$postData['beliefId'])
		->where('valueId',$postData['valueId'])
		->first();


		$dotValues = DB::table('dots_values')
		->select('dots_values.id','dot_value_list.name','dots_values.beliefId','dots_beliefs.name AS beliefName')
		->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
		->leftjoin('dots_beliefs','dots_beliefs.id','dots_values.beliefId')
		->where('dots_values.id',$postData['valueId'])->first();

		$user = Auth::user();

		$organisation = DB::table('organisations')->select('organisation')->where('id',$user->orgId)->first();
		
		$orgName ='';
		if($organisation){
			$orgName = $organisation->organisation;
		}

		$valueName = '';
		$beliefName='';
		if($dotValues){
			$valueName = $dotValues->name;
			$beliefName= $dotValues->beliefName;
		}

		//send notification mail Pooja Nenava rated for value: value name in belief: belief of Organisation name
		$user = Auth::user();
		
		$msg = '<p><strong>'.ucfirst($user->name).'</strong>'.' rated for Value: '.$valueName.' in Belief: '.$beliefName.' of Organisation <strong>'.$orgName.'</strong>';

		$notificationArray = array('msg'=>$msg);

		//app('App\Http\Controllers\Admin\CommonController')->sendNotificationMail($notificationArray);

		/*notification part finish*/
		
		if($status){

			$updateArray = array(				
				'ratings'  => $postData['ratings'],
				'updated_at'=> date('Y-m-d H:i:s')
			);

			DB::table('dot_values_ratings')
			->where('valueId',$postData['valueId'])
			->where('userId',$postData['userId'])
			->update($updateArray);

			return response()->json(['code'=>200,'status'=>true,'service_name'=>'rating-dot-values','message'=>'Ratings updated successfully.','data'=>$resultArray]);

		}else{

			$insertArray = array(
				'userId'   => $postData['userId'],
				'dotId'    => $postData['dotId'],
				'beliefId' => $postData['beliefId'],
				'valueId'  => $postData['valueId'],
				'ratings'  => $postData['ratings'],
				'created_at'=> date('Y-m-d H:i:s')
			);

			$status = DB::table('dot_values_ratings')->insertGetId($insertArray);

			return response()->json(['code'=>200,'status'=>true,'service_name'=>'rating-dot-values','message'=>'DOT rating successfully done.','data'=>$resultArray]);			

		}

	}

}