<?php

namespace App\Http\Controllers\api;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use DB;
use Mail;
use View;
use Dompdf\Dompdf;
use Dompdf\Options;
use SnappyPDF;

class ApiReportController extends Controller
{

  /*get DOT report*/
  public function getDOTreportGraph(Request $request)
  {

    // $orgId     = Input::get('orgId');
    // $beliefId  = Input::get('beliefId');
    // $officeId  = Input::get('officeId');

    $orgId    = $request->orgId;
    $beliefId = $request->beliefId;
    $officeId = $request->officeId;

    $dots = DB::table('dots')->where('orgId',$orgId)->first();

    $dotValuesArray = array();

    if(!empty($dots))
    {

      $query = DB::table('dots_beliefs')
      ->where('status','Active')
      ->where('dotId',$dots->id);  

      if(!empty($beliefId))
      {
        $query->where('id',$beliefId);
      }

      $query->orderBy('id','ASC');
      $dotBeliefs = $query->get();

      foreach ($dotBeliefs as $key => $bValue)
      {

        $dotValues = DB::table('dots_values')
        ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
        ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
        ->where('dots_values.status','Active')
        ->where('dots_values.beliefId',$bValue->id)
        ->orderBy('dot_value_list.id','ASC')
        ->get();

        $bquery = DB::table('dot_values_ratings')
        ->leftjoin('users','users.id','=','dot_values_ratings.userId')
        ->where('users.status','Active')
        ->where('beliefId', $bValue->id); 

        $startDate = (string)Input::get('startDate');
        $endDate   = (string)Input::get('endDate');

        //filter acc. to start and end date
        if(!empty($startDate) && !empty($endDate)) 
        {

          $startDate = date_create($startDate);
          $startDateMonth = date_format($startDate,"Y-m-d");

          $endDate = date_create($endDate);

          $endDateYear = date_format($endDate,"Y-m-d");

          $bquery->where('users.created_at', '>=',$startDateMonth);
          $bquery->where('users.created_at', '<=',$endDateYear);
        }

        $bRatings = $bquery->avg('ratings');

        $valuesArray = array();
        foreach ($dotValues as $key => $vValue) 
        {
          $query = DB::table('dot_values_ratings')
          ->leftjoin('users','users.id','=','dot_values_ratings.userId')
          ->where('users.status','Active')
          ->where('valueId', $vValue->id);

          $startDate = (string)Input::get('startDate');
          $endDate   = (string)Input::get('endDate');

          //filter acc. to start and end date
          if(!empty($startDate) && !empty($endDate)) 
          {

            $startDate = date_create($startDate);
            $startDateMonth = date_format($startDate,"Y-m-d");

            $endDate = date_create($endDate);

            $endDateYear = date_format($endDate,"Y-m-d");

            $query->where('users.created_at', '>=',$startDateMonth);
            $query->where('users.created_at', '<=',$endDateYear);

          }

          $vRatings = $query->avg('ratings');

          $vResult['valueId']      = $vValue->id;
          $vResult['valueName']    = ucfirst($vValue->name);

          $vResult['valueRatings'] = 0;
          if($vRatings)
          {
            $vResult['valueRatings'] = number_format(($vRatings-1), 2);
          }
          array_push($valuesArray, $vResult);
        }

        $result['beliefId']      = $bValue->id;
        $result['beliefName']    = ucfirst($bValue->name);           

        $result['beliefRatings'] = 0;
        if($bRatings)
        {
          $result['beliefRatings'] = number_format(($bRatings-1), 2);
        }

        $result['beliefValues']  = $valuesArray;

        //filter by office 
        if(!empty($officeId)) 
        {

          $bRatingsQuery = DB::table('dot_values_ratings')
          ->leftjoin('users','users.id','=','dot_values_ratings.userId')
          ->where('users.status','Active')
          ->where('dot_values_ratings.status','Active')
          ->where('dot_values_ratings.beliefId', $bValue->id);
          $bRatingsQuery->where('users.officeId', $officeId);

          $startDate = (string)Input::get('startDate');
          $endDate   = (string)Input::get('endDate');

          //filter acc. to start and end date
          if(!empty($startDate) && !empty($endDate)) 
          {

            $startDate = date_create($startDate);
            $startDateMonth = date_format($startDate,"Y-m-d");

            $endDate = date_create($endDate);

            $endDateYear = date_format($endDate,"Y-m-d");

            $bRatingsQuery->where('users.created_at', '>=',$startDateMonth);
            $bRatingsQuery->where('users.created_at', '<=',$endDateYear);

          }

          $isofficeRating = $bRatingsQuery->avg('ratings');

          if($isofficeRating)
          {
            array_push($dotValuesArray, $result);
          }
        }
        else
        {
          array_push($dotValuesArray, $result);
        }  


      }

      $startDate = Input::get('startDate');
      $endDate   = Input::get('endDate');

      //filter acc. to start and end date
      if(!empty($startDate) && !empty($endDate)) 
      {
        $startDate = date_create($startDate);
        $endDate = date_create($endDate);

        $startDateMonth = date_format($startDate,"Y-m-d");
        $endDateYear = date_format($endDate,"Y-m-d");

        $isUser = DB::table('users')
        ->where('orgId',$orgId)
        ->where('status','Active')
        ->where('created_at', '>=', $startDateMonth)  
        ->where('created_at', '<=', $endDateYear)
        ->count();

        if(empty($isUser))
        {
          $dotValuesArray = array();
        }

      }
    }

    if($request->reportStatus)
    {
      return $dotValuesArray;
    }

    return response()->json(['code'=>200,'status'=>true,'service_name'=>'DOT-report','message'=>'','data'=>$dotValuesArray]);
  }



  /*get COT functional lens report*/
  public function getReportFunctionalLensGraph(Request $request)
  {

    // $orgId        = Input::get('orgId');  
    // $officeId     = Input::get('officeId');
    // $departmentId = Input::get('departmentId');

    $orgId        = $request->orgId;
    $officeId     = $request->officeId;
    $departmentId = $request->departmentId;

    $startDate = (string)Input::get('startDate');
    $endDate   = (string)Input::get('endDate');

    $flag = 0 ;
    $departments = [];
    if(!empty($officeId)) 
    {          
      //for new department
      $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }
    else
    {
      if(!empty($departmentId))
      {
        $departmentsNew = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.orgId',$orgId)
        ->where('departments.departmentId',$departmentId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();
      }
    }

    $cotFunResultArray = array();

    $query = DB::table('cot_functional_lens_answers')
    ->select('cot_functional_lens_answers.userId AS id')
    ->leftjoin('users','users.id','cot_functional_lens_answers.userId')    
    ->where('users.status','Active')
    ->groupBy('cot_functional_lens_answers.userId')
    ->where('users.orgId',$orgId);

    if(!empty($officeId))
    {
     $query->where('officeId',$officeId);
   }

    //filter acc. to start and end date
   if(!empty($startDate) && !empty($endDate)) 
   {

    $startDate = date_create($startDate);
    $startDateMonth = date_format($startDate,"Y-m-d");

    $endDate = date_create($endDate);

    $endDateYear = date_format($endDate,"Y-m-d");

    $query->where('users.created_at', '>=',$startDateMonth);
    $query->where('users.created_at', '<=',$endDateYear);

  }

  if(!empty($departmentId))
  {   
    $i = 0;
    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {         
      if($i == 0)
      {
        $query->where('users.departmentId',$departments1->id);
      }
      else
      {
        $query->orWhere('users.departmentId',$departments1->id);
      }
      $i++;
    }
  }

  $usersList = $query->get();

// reset if department is not in organisation
  if($flag==1)
  {
    $usersList = array();
  }

  foreach ($usersList as $userValue)
  {

    $variable = app('App\Http\Controllers\Admin\CommonController')->getCotFunLensInitialValue();

    $initialValEIArray = array();

    foreach ($variable as $value)
    {

      $value1 = $value[0];
      $value2 = $value[1];

      $countE = DB::table('cot_functional_lens_answers AS cfla')
      ->select('cflqo.option_name AS optionName')
      ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
      ->leftJoin('users','users.id','=','cfla.userId')
      ->where('users.status','Active')
      ->where('cfla.userId',$userValue->id)->where('cflqo.initial_value_id',$value1)->get();

      $countI = DB::table('cot_functional_lens_answers AS cfla')
      ->select('cflqo.option_name AS optionName')
      ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
      ->leftJoin('users','users.id','=','cfla.userId')
      ->where('users.status','Active')
      ->where('cfla.userId',$userValue->id)->where('cflqo.initial_value_id',$value2)->get();

      $initialValEI  = '';
      if(count($countE) > count($countI))
      {            
        $initialValEI  =  $value1;
      }
      elseif (count($countE) < count($countI))
      {           
        $initialValEI  =  $value2;
      }
      elseif(count($countE) == count($countI))
      {
       $initialValEI  =  $value1;
     }           
     array_push($initialValEIArray, $initialValEI);

   }


   $valueTypeArray = array();
   $valueTypeArray['value'] = array();

   if(!empty($initialValEIArray))
   {
    $valueTypeArray = array('value'=>$initialValEIArray);
  }

  $matchValueArr =array();
  for($i=0; $i < count($valueTypeArray['value']); $i++)
  {
    $valuesKey = '';
    $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('title',$valueTypeArray['value'][$i])->first();  

    if(!empty($table))
    {
      $valuesKey = $table->value;
    }

    array_push($matchValueArr, $valuesKey); 
  }

       // dd($matchValue);

    //remove last and first string
  // $tribeMatchValue = substr(implode('', $matchValueArr), 1, -1);
  $tribeMatchValue = json_encode(array_slice($valueTypeArray['value'], 1, -1));

  $tribeTipsList = DB::table('cot_functional_lens_tribe_tips_records')->where('value',$tribeMatchValue)->get();

  $tribeTipsArray = array();
  foreach ($tribeTipsList as $ttvalue)
  {
    $tTips['value']   = $ttvalue->value;       
    array_push($cotFunResultArray, $tTips);
  }
}

$ST =0;
$SF =0;
$NF =0;
$NT =0;

foreach ($cotFunResultArray as $finalArrayValue)
{ 
 if($finalArrayValue['value']=='["7","9"]'){

   $ST++;
 }elseif ($finalArrayValue['value']=='["7","10"]'){

   $SF++;
 }elseif ($finalArrayValue['value']=='["8","10"]'){

   $NF++;
 }elseif ($finalArrayValue['value']=='["8","9"]'){

   $NT++;
 }

}

$totalUser = count($usersList);


if (!empty($totalUser))
{
  $stPercent = number_format((($ST/$totalUser)*100),2);
  $sfPercent = number_format((($SF/$totalUser)*100),2);
  $nfPercent = number_format((($NF/$totalUser)*100),2);
  $ntPercent = number_format((($NT/$totalUser)*100),2);
}
else
{
  $stPercent = 0;
  $sfPercent = 0;
  $nfPercent = 0;
  $ntPercent = 0;
}

$tribeTipsListKey = DB::table('cot_functional_lens_tribe_tips_records')->where('status','Active')->orderBy('id','ASC')->get();

$keyNameArray = array();
foreach ($tribeTipsListKey as $value1)
{
  $reportKeyArray = json_decode($value1->value);

  $keyArr = array();
  foreach ($reportKeyArray as $value)
  {
    $valuesKey = '';
    $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$value)->first();  

    if(!empty($table))
    {
      $valuesKey = $table->value;
    }   
    array_push($keyArr, $valuesKey); 
  }
  array_push($keyNameArray, $keyArr);
}


//set values of ST
$sotStKey ='';
if(!empty($keyNameArray[0][0]) && !empty($keyNameArray[0][1])) 
{
  $sotStKey =  $keyNameArray[0][0].$keyNameArray[0][1];
}

$sotSfKey = '';
if(!empty($keyNameArray[3][0]) && !empty($keyNameArray[3][1])) 
{
  $sotSfKey =  $keyNameArray[3][0].$keyNameArray[3][1];
}

$sotNtKey = '';
if(!empty($keyNameArray[1][0]) && !empty($keyNameArray[1][1])) 
{
  $sotNtKey =  $keyNameArray[1][0].$keyNameArray[1][1]; 
}

$sotNfKey = '';
if(!empty($keyNameArray[2][0]) && !empty($keyNameArray[2][1])) 
{
  $sotNfKey =  $keyNameArray[2][0].$keyNameArray[2][1];
}


$funcLensPercentageArray = array('st'=>$stPercent,'sf'=>$sfPercent,'nf'=>$nfPercent,'nt'=>$ntPercent,'stValue'=>$sotStKey,'sfValue'=>$sotSfKey,'nfValue'=>$sotNfKey,'ntValue'=>$sotNtKey);


if($request->reportStatus)
{
  return $funcLensPercentageArray;
}

return response()->json(['code'=>200,'status'=>true,'service_name'=>'COT-functional-lens-report','message'=>'','data'=>$funcLensPercentageArray]);
}

/*get overpresented,underpresented user of COT*/
public function getCOTteamRoleMapReport(Request $request)
{

  $orgId        = $request->orgId;
  $officeId     = $request->officeId;
  $departmentId = $request->departmentId;

  $startDate    = (string)Input::get('startDate');
  $endDate      = (string)Input::get('endDate');

  $resultArray = array();
  $finalArray  = array();
  $rankArray   = array();
  
  $flag = 0 ;

  $departments = [];
  if (!empty($officeId)) 
  {
   //for new department
    $departmentsNew = DB::table('departments')
    ->select('id')->where('id',$departmentId)
    ->where('status','Active')->get();
  }
  else
  {
    if(!empty($departmentId))
    {
      $departmentsNew = DB::table('departments')
      ->select('departments.id','all_department.department')
      ->leftJoin('all_department','all_department.id','departments.departmentId')
      ->where('departments.orgId',$orgId)
      ->where('departments.departmentId',$departmentId) 
      ->where('departments.status','Active') 
      ->orderBy('all_department.department','ASC')->get();
    }
  }

  $query = DB::table('cot_answers')
  ->select('users.id','users.orgId','users.name')
  ->leftjoin('users','users.id','cot_answers.userId')
  ->where('users.status','Active')
  ->where('users.orgId',$orgId);

  if(!empty($officeId))
  {
    $query->where('users.officeId',$officeId);
  }

  //filter acc. to start and end date
  if(!empty($startDate) && !empty($endDate)) 
  {

    $startDate = date_create($startDate);
    $startDateMonth = date_format($startDate,"Y-m-d");

    $endDate = date_create($endDate);

    $endDateYear = date_format($endDate,"Y-m-d");

    $query->where('users.created_at', '>=',$startDateMonth);
    $query->where('users.created_at', '<=',$endDateYear);

  }

  if(!empty($departmentId))
  {   
    $i = 0;

    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {
      if($i == 0)
      {
        $query->where('users.departmentId',$departments1->id);
      }
      else
      {
        $query->orWhere('users.departmentId',$departments1->id);
      }
      $i++;
    }
  }

  $users = $query->get();

  $resultArray1 = array();

  $data['id']    = '';
  $data['orgId'] = '';
  $data['name']  = '';

  foreach ($users as  $value)
  {
    $data['id'] = $value->id;
    $data['orgId'] = $value->orgId;
    $data['name'] = $value->name;

    array_push($resultArray1, $data);
  }

  $users23 = array_unique($resultArray1,SORT_REGULAR);

  $finalUser = array();

  foreach ($users23 as  $value55)
  {
    $object = (object)array();    

    $object->id    = $value55['id'];
    $object->orgId = $value55['orgId'];
    $object->name  = $value55['name'];

    array_push($finalUser,$object);
  }
// reset if department is not in organisation
  if($flag==1)
  {
    $finalUser = array();
  }

  $cotRoleMapOptions = DB::table('cot_role_map_options')->where('status','Active')->orderBy('id','ASC')->get();

  $usersArray = array();
  foreach ($finalUser as $key => $value) 
  {

    foreach($cotRoleMapOptions as $key => $maper)
    {
      $maperCount = DB::table('cot_answers')
      ->where('orgId',$value->orgId)
      ->where('userId',$value->id)
      ->where('cot_role_map_option_id',$maper->id)->where('status','Active')->sum('answer');

      $resultArray = array();

      $Value[$maper->maper_key] = $maperCount;    

      array_push($resultArray, $Value);
    }   

    arsort($resultArray[0]);

    $i = 0;
    $prev = "";
    $j = 0;

    $new  = array();
    $new1 = array();

    foreach($resultArray[0] as $key => $val)
    { 
      if($val != $prev)
      { 
        $i++; 
      }

      $new1['title'] = $key; 
      $new1['value'] = $i;        
      $prev = $val;

      if ($key==$cotRoleMapOptions[0]->maper_key) {
        $new1['priority'] = 1;
      }elseif ($key==$cotRoleMapOptions[1]->maper_key) {
        $new1['priority'] = 2;
      }elseif ($key==$cotRoleMapOptions[2]->maper_key) {
        $new1['priority'] = 3;
      }elseif ($key==$cotRoleMapOptions[3]->maper_key) {
        $new1['priority'] = 4;
      }elseif ($key==$cotRoleMapOptions[4]->maper_key) {
        $new1['priority'] = 5;
      }elseif ($key==$cotRoleMapOptions[5]->maper_key) {
        $new1['priority'] = 6;
      }elseif ($key==$cotRoleMapOptions[6]->maper_key) {
        $new1['priority'] = 7;
      }elseif ($key==$cotRoleMapOptions[7]->maper_key) {
        $new1['priority'] = 8;
      }

      array_push($new,$new1);
    } 

    $value->new = $new;

    $value->totalKeyCount = $resultArray[0];

    array_push($usersArray, $value);

  }

  $usersArray = $this->sortarr($usersArray);

  $shaper = 0; 
  $coordinator = 0; 
  $completerFinisher = 0; 
  $teamworker = 0; 
  $implementer = 0; 
  $monitorEvaluator = 0; 
  $plant = 0; 
  $resourceInvestigator = 0; 

  foreach ($usersArray as $key1 => $value1)
  {

    if($value1->shaper==1 || $value1->shaper==2 || $value1->shaper==3)
    {
      $shaper++;     
    }
    if($value1->coordinator==1 || $value1->coordinator==2 || $value1->coordinator==3)
    {
      $coordinator++;
    }
    if($value1->completerFinisher==1 || $value1->completerFinisher==2 || $value1->completerFinisher==3)
    {
      $completerFinisher++;
    }
    if($value1->teamworker==1 || $value1->teamworker==2 || $value1->teamworker==3)
    {
      $teamworker++;
    }
    if($value1->implementer==1 || $value1->implementer==2 || $value1->implementer==3)
    {
      $implementer++;
    }
    if($value1->monitorEvaluator==1 || $value1->monitorEvaluator==2 || $value1->monitorEvaluator==3)
    {
      $monitorEvaluator++;
    }
    if($value1->plant==1 || $value1->plant==2 || $value1->plant==3)
    {
      $plant++;
    }
    if($value1->resourceInvestigator==1 || $value1->resourceInvestigator==2 || $value1->resourceInvestigator==3)
    {
      $resourceInvestigator++;
    }

  }

  $data['shaper']              = $shaper;
  $data['coordinator']         = $coordinator;
  $data['completerFinisher']   = $completerFinisher;
  $data['teamworker']          = $teamworker;
  $data['implementer']         = $implementer;
  $data['monitorEvaluator']    = $monitorEvaluator;
  $data['plant']               = $plant;
  $data['resourceInvestigator']= $resourceInvestigator;


  $totalUsers = count($finalUser);

/*  foreach ($data as $key => $value)
{*/

 if (!empty($data) && (!empty($totalUsers)))
 {

  $data['shaper']              = number_format((($shaper/$totalUsers)*100),2);
  $data['coordinator']         = number_format((($coordinator/$totalUsers)*100),2);
  $data['completerFinisher']   = number_format((($completerFinisher/$totalUsers)*100),2);
  $data['teamworker']          = number_format((($teamworker/$totalUsers)*100),2);
  $data['implementer']         = number_format((($implementer/$totalUsers)*100),2);
  $data['monitorEvaluator']    = number_format((($monitorEvaluator/$totalUsers)*100),2);
  $data['plant']               = number_format((($plant/$totalUsers)*100),2);
  $data['resourceInvestigator']= number_format((($resourceInvestigator/$totalUsers)*100),2);
}
else
{
  $data['shaper']              = 0;
  $data['coordinator']         = 0;
  $data['completerFinisher']   = 0;
  $data['teamworker']          = 0;
  $data['implementer']         = 0;
  $data['monitorEvaluator']    = 0;
  $data['plant']               = 0;
  $data['resourceInvestigator']= 0;
}

foreach($cotRoleMapOptions as $mapValue)
{
  $mapersArray = array();

  $res[$mapValue->maper_key] = $mapValue->maper;

  array_push($mapersArray, $res);
}

$data['mapersArray'] = $mapersArray[0];

// }

if($request->reportStatus)
{
  return $data;
}

return response()->json(['code'=>200,'status'=>true,'service_name'=>'COT-maper-rank','message'=>'COT maper rank','data'=>$data]);

}

/*sorting array by priority*/
function sortarr($arr1)
{

  $arrUsers = array();
  $newArray = array();
  $tes =array();

  for($i=0; $i<count($arr1); $i++)
  {    
    $tes['userId']    = $arr1[$i]->id;
    $tes['userName']  = $arr1[$i]->name;   

    $arrt = (array)$arr1[$i]->new;
    
    usort($arrt, function($x, $y)
    {

      if ($x['value']== $y['value'] ) 
      {
        if($x['priority']<$y['priority'])
        {
          return 0;
        }
        else
        {
          return 1;
        } 
      }
    }); 

    $tt = array();
    foreach($arrt as $key => $value)
    {         
      if ($value['value']!=0)
      {
        $tes[$value['title']] = $key+1;
      } else{
        $tes[$value['title']] = 0;
      }

    }   

    $tes['totalKeyCount'] = $arr1[$i]->totalKeyCount;

    $tes['mapersArray'] = array();
    if (!empty($arr1[$i]->mapersArray))
    {
      $tes['mapersArray']   = $arr1[$i]->mapersArray;
    }

    array_push($newArray,(object)$tes);
  }

  // print_r($newArray);
  return $newArray;

}


/*get SOT culture structure report */
public function getSOTcultureStructureReport(Request $request)
{

 $resultArray = array();
 $user        = Auth::user();

 // $orgId        = Input::get('orgId');
 // $officeId     = Input::get('officeId');
 // $departmentId = Input::get('departmentId');

 $orgId        = $request->orgId;
 $officeId     = $request->officeId;
 $departmentId = $request->departmentId;
 
 $flag = 0 ;
 $departments = [];
 if (!empty($officeId)) 
 {
  //for new department
  $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
}
else
{
  if(!empty($departmentId))
  {
    $departmentsNew = DB::table('departments')
    ->select('departments.id','all_department.department')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('departments.orgId',$orgId)
    ->where('departments.departmentId',$departmentId) 
    ->where('departments.status','Active') 
    ->orderBy('all_department.department','ASC')->get();
  }
}

$sotCultureStrTbl = DB::table('sot_culture_structure_records')->select('id','type','title','imgUrl')->where('status','Active')->get();

$sotCountArray = array();
$countArr      = array();
$resultArray['IsQuestionnaireAnswerFilled'] = false;

foreach ($sotCultureStrTbl as $value) 
{

  $query = DB::table('sot_answers')
  ->select(DB::raw('sum(score) AS count'))
  ->leftJoin('sot_questionnaire_records','sot_questionnaire_records.id','=','sot_answers.question_id')
  ->join('users','users.id','=','sot_answers.userId')
  ->where('users.status','Active')
  ->where('sot_questionnaire_records.type',$value->type)    
  ->where('users.orgId',$orgId);

  if(!empty($officeId))
  {
   $query->where('users.officeId',$officeId);
 }

 $startDate    = (string)Input::get('startDate');
 $endDate      = (string)Input::get('endDate');
 //filter acc. to start and end date
 if(!empty($startDate) && !empty($endDate)) 
 {

  $startDate = date_create($startDate);
  $startDateMonth = date_format($startDate,"Y-m-d");
  
  $endDate = date_create($endDate);

  $endDateYear = date_format($endDate,"Y-m-d");

  $query->where('users.created_at', '>=',$startDateMonth);
  $query->where('users.created_at', '<=',$endDateYear);


}

if(!empty($departmentId))
{   
  $i = 0;

  if($departmentsNew->isEmpty())
  {
    $flag = 1;
  }
  foreach ($departmentsNew as $departments1) 
  {       
    if($i == 0)
    {
      $query->where('users.departmentId',$departments1->id);
    }
    else
    {
      $query->orWhere('users.departmentId',$departments1->id);
    }
    $i++;
  }
}       

$SOTCount = $query->first();

$value->SOTCount = ($SOTCount->count)?(string)$SOTCount->count:"0";

// reset if department is not in organisation
if($flag==1)
{    
  $value->SOTCount = "0";
}


$value->imgUrl   = url('public/uploads/sot/').'/'.$value->imgUrl;

if ($SOTCount->count) {
  $resultArray['IsQuestionnaireAnswerFilled']   = true;
}


$IsUserFilledAnswer = DB::table('sot_answers')
->select(DB::raw('sum(score) AS count'))->where('userId',$user->id)->first();

if ($IsUserFilledAnswer->count) {
  $resultArray['IsUserFilledAnswer']   = true;
}
else
{
  $resultArray['IsUserFilledAnswer']   = false;
}

array_push($countArr, $SOTCount->count);
array_push($sotCountArray, $value);

}

if($flag==1)
{
  $sotCountArray  = array();
}

// find maximum value array
$sotCountArray1 = array();
foreach ($sotCultureStrTbl as $value) 
{

  $query = DB::table('sot_answers')
  ->select(DB::raw('sum(score) AS count'))
  ->leftJoin('sot_questionnaire_records','sot_questionnaire_records.id','=','sot_answers.question_id')
  ->leftJoin('users','users.id','=','sot_answers.userId')
  ->where('users.status','Active')
  ->where('sot_questionnaire_records.type',$value->type)    
  ->where('users.orgId',$orgId);

  if(!empty($officeId))
  {
   $query->where('users.officeId',$officeId);
 }

 $startDate    = (string)Input::get('startDate');
 $endDate      = (string)Input::get('endDate');
  //filter acc. to start and end date
 if(!empty($startDate) && !empty($endDate)) 
 {

  $startDate = date_create($startDate);
  $startDateMonth = date_format($startDate,"Y-m-d");

  $endDate = date_create($endDate);

  $endDateYear = date_format($endDate,"Y-m-d");

  $query->where('users.created_at', '>=',$startDateMonth);
  $query->where('users.created_at', '<=',$endDateYear);

}


if(!empty($departmentId))
{   

  if($departmentsNew->isEmpty())
  {
    $flag = 1;
  }
  $department3 = array();
  foreach ($departmentsNew as $departments1) 
  {       
    $departments2 = $departments1->id;
    array_push($department3, $departments2);
  }

  $query->whereIn('users.departmentId',$department3);
}            

$SOTCount = $query->first();

// print_r($SOTCount);

$value->SOTCount = ($SOTCount->count)?(string)$SOTCount->count:"0";

if(!empty($SOTCount->count) && max($countArr)==$SOTCount->count)
{
  array_push($sotCountArray1, $value);
}        

}



// reset if department is not in organisation
if($flag==1)
{
  $sotCountArray1 = array();
}

//detail of culture structure 
$sotStrDetailArr = array();
foreach($sotCountArray1 as $sValue)
{
  $sotStrDetail = DB::table('sot_culture_structure_records')->where('id',$sValue->id)->where('status','Active')->first();

  $summary = DB::table('sot_culture_structure_summary_records')->select('summary')->where('type',$sValue->id)->where('status','Active')->get();

  $sValue->summary = $summary;

  array_push($sotStrDetailArr, $sValue);
  break; 
}

$resultArray['sotDetailArray']         = $sotCountArray;
$resultArray['sotSummaryDetailArray']  = $sotStrDetailArr;

if($request->reportStatus)
{
  return $resultArray;
}

return response()->json(['code'=>200,'status'=>true,'service_name'=>'SOT-culture-structure-report','message'=>'','data'=>$resultArray]);

}

//get SOT motivation report
public function getSOTmotivationReport(Request $request)
{

  $resultArray = array();

  // $orgId        = Input::get('orgId');  
  // $officeId     = Input::get('officeId');
  // $departmentId = Input::get('departmentId');

  $orgId        = $request->orgId;
  $officeId     = $request->officeId;
  $departmentId = $request->departmentId;


  $startDate    = (string)Input::get('startDate');
  $endDate      = (string)Input::get('endDate');

  $flag = 0 ;
  $departments = [];
  if (!empty($officeId)) 
  {
  //for new department
    $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
  }
  else
  {
    if(!empty($departmentId))
    {
      $departmentsNew = DB::table('departments')
      ->select('departments.id','all_department.department')
      ->leftJoin('all_department','all_department.id','departments.departmentId')
      ->where('departments.orgId',$orgId)
      ->where('departments.departmentId',$departmentId) 
      ->where('departments.status','Active') 
      ->orderBy('all_department.department','ASC')->get();
    }
  }

  $query = DB::table('sot_motivation_answers')
  ->leftjoin('users','users.id','sot_motivation_answers.userId')
  ->select('sot_motivation_answers.userId')
  ->where('users.status','Active')
  ->groupBy('sot_motivation_answers.userId')
  ->where('users.orgId',$orgId);
  
  if(!empty($officeId))
  {
    $query->where('officeId',$officeId);
  }

  $startDate    = (string)Input::get('startDate');
  $endDate      = (string)Input::get('endDate');
     //filter acc. to start and end date
  if(!empty($startDate) && !empty($endDate)) 
  {

    $startDate = date_create($startDate);
    $startDateMonth = date_format($startDate,"Y-m-d");

    $endDate = date_create($endDate);

    $endDateYear = date_format($endDate,"Y-m-d");

    $query->where('users.created_at', '>=',$startDateMonth);
    $query->where('users.created_at', '<=',$endDateYear);

  }


  if(!empty($departmentId))
  {   
    $i = 0;

    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {       
      if($i == 0)
      {
        $query->where('users.departmentId',$departments1->id);
      }
      else
      {
        $query->orWhere('users.departmentId',$departments1->id);
      }
      $i++;
    }
  } 

  $usersList = $query->get();

  $totalUser = count($usersList);

  // reset if department is not in organisation
  if($flag==1)
  {
    $totalUser = 0;
  }


  if(empty($totalUser) && empty($request->reportStatus))
  {
    return response()->json(['code'=>400,'status'=>true,'service_name'=>'SOT-motivation-report','message'=>'SOT answers are not done by user.','data'=>$resultArray]);
  }

  $categoryTbl = DB::table('sot_motivation_value_records')->where('status','Active')->get();

  foreach ($categoryTbl as $value)
  {  

    $query = DB::table('sot_motivation_answers AS sotans')
    ->leftJoin('sot_motivation_question_options AS qoption', 'qoption.id', '=', 'sotans.optionId')
    ->leftjoin('users','users.id','=','sotans.userId')
    ->where('users.status','Active')                     
    ->where('sotans.orgId',$orgId)
    ->where('sotans.status','Active')
    ->where('qoption.category_id',$value->id);

    if(!empty($officeId))
    {
     $query->where('users.officeId',$officeId);
   }

   $startDate    = (string)Input::get('startDate');
   $endDate      = (string)Input::get('endDate');
 //filter acc. to start and end date
   if(!empty($startDate) && !empty($endDate)) 
   {

    $startDate = date_create($startDate);
    $startDateMonth = date_format($startDate,"Y-m-d");

    $endDate = date_create($endDate);

    $endDateYear = date_format($endDate,"Y-m-d");

    $query->where('users.created_at', '>=',$startDateMonth);
    $query->where('users.created_at', '<=',$endDateYear);

  }

  if(!empty($departmentId))
  {   


    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    $department3 = array();
    foreach ($departmentsNew as $departments1) 
    {       
      $departments2 = $departments1->id;
      array_push($department3, $departments2);

    }

    $query->whereIn('users.departmentId',$department3);
  } 

  $ansTbl = $query->sum('sotans.answer'); 
  
  if($totalUser)
  {
    $result['title'] = $value->title;    
    $result['score'] = number_format((float)($ansTbl/$totalUser), 2); 
    array_push($resultArray, $result);
  }


  
}

if($request->reportStatus)
{
  return $resultArray;
}

return response()->json(['code'=>200,'status'=>true,'service_name'=>'SOT-motivation-report','message'=>'','data'=>$resultArray]);
}


/*get diagnostic reports*/
public function getDiagnosticReportForGraph(Request $request)
{

  $resultArray = array();

  $orgId        = $request->orgId;
  $officeId     = $request->officeId;
  $departmentId = $request->departmentId;

  $startDate    = (string)Input::get('startDate');
  $endDate      = (string)Input::get('endDate');

  $flag = 0 ;
  $departments = [];
  if (!empty($officeId)) 
  {
  //for new department
    $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
  }
  else
  {
    if(!empty($departmentId))
    {
      $departmentsNew = DB::table('departments')
      ->select('departments.id','all_department.department')
      ->leftJoin('all_department','all_department.id','departments.departmentId')
      ->where('departments.orgId',$orgId)
      ->where('departments.departmentId',$departmentId) 
      ->where('departments.status','Active') 
      ->orderBy('all_department.department','ASC')->get();
    }
  }

  $diagnosticQueCatTbl = DB::table('diagnostic_questions_category')->get();

  $query = DB::table('diagnostic_answers')
  ->leftjoin('users','users.id','diagnostic_answers.userId')
  ->select('diagnostic_answers.userId')
  ->where('users.status','Active')
  ->groupBy('diagnostic_answers.userId')
  ->where('users.orgId',$orgId);

  if(!empty($officeId))
  {
    $query->where('users.officeId',$officeId);
  }

  $startDate    = (string)Input::get('startDate');
  $endDate      = (string)Input::get('endDate');
  //filter acc. to start and end date
  if(!empty($startDate) && !empty($endDate)) 
  {

    $startDate = date_create($startDate);
    $startDateMonth = date_format($startDate,"Y-m-d");

    $endDate = date_create($endDate);

    $endDateYear = date_format($endDate,"Y-m-d");

    $query->where('users.created_at', '>=',$startDateMonth);
    $query->where('users.created_at', '<=',$endDateYear);

  }


  if(!empty($departmentId))
  {   
    $i = 0;

    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {       
      if($i == 0)
      {
        $query->where('users.departmentId',$departments1->id);
      }
      else
      {
        $query->orWhere('users.departmentId',$departments1->id);
      }
      $i++;
    }
  }


  $users     = $query->get();
  $userCount = count($users);

 // reset if department is not in organisation
  if($flag==1)
  {
    $userCount = 0;
  }

  if(empty($userCount) && empty($request->reportStatus))
  {
    return response()->json(['code'=>400,'status'=>true,'service_name'=>'Report-diagnostic-report','message'=>'Diagnostic answers not done yet.','data'=>$resultArray]);
  }

  $optionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
  $diaOptCount = count($optionsTbl); 


  foreach ($diagnosticQueCatTbl as $value)
  {
    $questionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

    $quecount = count($questionTbl);

    $perQuePercen = 0;
    foreach ($questionTbl as  $queValue)
    {

      $diaQuery = DB::table('diagnostic_answers')            
      ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
      ->leftjoin('users','users.id','=','diagnostic_answers.userId')
      ->where('users.status','Active')   
      ->where('diagnostic_answers.orgId',$orgId)
      ->where('diagnostic_questions.id',$queValue->id)
      ->where('diagnostic_questions.category_id',$value->id);

      if (!empty($officeId))
      {
        $diaQuery->where('users.officeId',$officeId);                    
      }
      if(!empty($departmentId))
      {   
        if($departmentsNew->isEmpty())
        {
          $flag = 1;
        }
        $department3 = array();
        foreach ($departmentsNew as $departments1) 
        {       
          $departments2 = $departments1->id;
          array_push($department3, $departments2);
        }

        $diaQuery->whereIn('users.departmentId',$department3);
      }

      $diaAnsTbl = $diaQuery->sum('answer'); 
      
      if($userCount) 
      {
        $perQuePercen += ($diaAnsTbl/$userCount);   
      }
      

    }

    $score = ($perQuePercen/($quecount*$diaOptCount));
    $totalPercentage = ($perQuePercen/($quecount*$diaOptCount))*100;

    $value1['title']      =  $value->title;
    $value1['categoryId'] =  $value->id;
    $value1['score']      =  number_format((float)$score, 2, '.', '');
    $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');      

    array_push($resultArray, $value1);
  }

  if($request->reportStatus)
  {
    return $resultArray;
  }

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'Report-diagnostic-report','message'=>'','data'=>$resultArray]);

}


/*get diagnostic reports*/
public function getTribeometerReportForGraph(Request $request)
{

  $resultArray = array();

  $orgId        = $request->orgId;
  $officeId     = $request->officeId;
  $departmentId = $request->departmentId;

  $flag = 0 ;
  $departments = [];
  if (!empty($officeId)) 
  {
     //for new department
    $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
  }
  else
  {
    if(!empty($departmentId))
    {
      $departmentsNew = DB::table('departments')
      ->select('departments.id','all_department.department')
      ->leftJoin('all_department','all_department.id','departments.departmentId')
      ->where('departments.orgId',$orgId)
      ->where('departments.departmentId',$departmentId) 
      ->where('departments.status','Active') 
      ->orderBy('all_department.department','ASC')->get();
    }
  }

  $query = DB::table('tribeometer_answers')
  ->leftjoin('users','users.id','tribeometer_answers.userId')
  ->select('tribeometer_answers.userId')
  ->where('users.status','Active')
  ->groupBy('tribeometer_answers.userId')
  ->where('users.orgId',$orgId);

  if(!empty($officeId))
  {
    $query->where('users.officeId',$officeId);
  }

  $startDate    = (string)Input::get('startDate');
  $endDate      = (string)Input::get('endDate');

     //filter acc. to start and end date
  if(!empty($startDate) && !empty($endDate)) 
  {

    $startDate = date_create($startDate);
    $startDateMonth = date_format($startDate,"Y-m-d");

    $endDate = date_create($endDate);

    $endDateYear = date_format($endDate,"Y-m-d");

    $query->where('users.created_at', '>=',$startDateMonth);
    $query->where('users.created_at', '<=',$endDateYear);


  }

  if(!empty($departmentId))
  {   
    $i = 0;

    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {       
      if($i == 0)
      {
        $query->where('users.departmentId',$departments1->id);
      }
      else
      {
        $query->orWhere('users.departmentId',$departments1->id);
      }
      $i++;
    }
  }

  $users     = $query->get();
  $userCount = count($users);

   // reset if department is not in organisation
  if($flag==1)
  {
    $userCount = 0;
  }

  if(empty($userCount) && empty($request->reportStatus))
  {
    return response()->json(['code'=>400,'status'=>true,'service_name'=>'Report-tribeometer-report','message'=>'Tribeometer answers not done yet.','data'=>$resultArray]);
  }   

  $optionsTbl = DB::table('tribeometer_question_options')->where('status','Active')->get();
  $optCount   = count($optionsTbl)-1; 

  $queCatTbl    = DB::table('tribeometer_questions_category')->get();
  foreach ($queCatTbl as $value)
  {
    $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();

    $quecount = count($questionTbl);

    $perQuePercen = 0;
    foreach ($questionTbl as  $queValue)
    {
      $triQuery = DB::table('tribeometer_answers')           
      ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
      ->leftjoin('users','users.id','=','tribeometer_answers.userId')
      ->where('users.status','Active') 
      ->where('tribeometer_answers.orgId',$orgId)
      ->where('tribeometer_questions.id',$queValue->id)
      ->where('tribeometer_questions.category_id',$value->id);

      if (!empty($officeId))
      {
        $triQuery->where('users.officeId',$officeId);                    
      }

      $startDate    = (string)Input::get('startDate');
      $endDate      = (string)Input::get('endDate');

     //filter acc. to start and end date
      if(!empty($startDate) && !empty($endDate)) 
      {

        $startDate = date_create($startDate);
        $startDateMonth = date_format($startDate,"Y-m-d");

        $endDate = date_create($endDate);

        $endDateYear = date_format($endDate,"Y-m-d");

        $triQuery->where('users.created_at', '>=',$startDateMonth);
        $triQuery->where('users.created_at', '<=',$endDateYear);


      }

      if(!empty($departmentId))
      {   
        if($departmentsNew->isEmpty())
        {
          $flag = 1;
        }
        $department3 = array();
        foreach ($departmentsNew as $departments1) 
        {       
          $departments2 = $departments1->id;
          array_push($department3, $departments2);

        }

        $triQuery->whereIn('users.departmentId',$department3);
      }

      $triAnsTbl = $triQuery->sum('answer');
      if($userCount)
      {
        $perQuePercen += ($triAnsTbl/$userCount);
      }

    }

    $score           = ($perQuePercen/($quecount*$optCount));
    $totalPercentage = ($perQuePercen/($quecount*$optCount))*100;

    $value1['title']      =  $value->title;
    $value1['score']      =  number_format((float)$score, 2, '.', '');
    $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');      

    array_push($resultArray, $value1);
  }

  if($request->reportStatus)
  {
    return $resultArray;
  }

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'Report-tribeometer-report','message'=>'','data'=>$resultArray]);

}

/*get report pdf url */
public function getReportPdfUrl(Request $request)
{


  $resultArray = array();

  $orgId = $request->orgId;

  $request->orgId = base64_encode($orgId);
  $request->pdfStatus = true;
    //get data of DOT

  $dotObj = app('App\Http\Controllers\Admin\AdminReportController')->show($request,base64_encode($orgId)); 
  $org = DB::table('organisations')->where('id',$orgId)->first();
  $dotValuesArray = $dotObj->dotValuesArray;

    //get data of team role map percentage values
  $cotTeamRoleMapObj = app('App\Http\Controllers\Admin\AdminReportController')->getCOTteamRoleMapGraph($request);
  $cotTeamRoleMapGraphPercentage = $cotTeamRoleMapObj['cotTeamRoleMapGraphPercentage']['data'];
  $cotRoleMapOptions             = $cotTeamRoleMapObj['cotRoleMapOptions'];

    //get cot functional lens
  $cotFunLensObj = app('App\Http\Controllers\Admin\AdminReportController')->getReportFunctionalGraph($request);
  $funcLensPercentageArray = $cotFunLensObj['funcLensPercentageArray'];
  $keyNameArray            = $cotFunLensObj['keyNameArray'];
  

    //get sot culture structure
  $sotCulStrObj = app('App\Http\Controllers\Admin\AdminReportController')->getReportCultureGraph($request);
  $sotCountArray = $sotCulStrObj['sotCountArray'];
  $sotStrDetailArr = $sotCulStrObj['sotStrDetailArr'];

    //get sot motivatin data
  $sotMotObj = app('App\Http\Controllers\Admin\AdminReportController')->getReportMotivationalGraph($request);
  $SOTmotivationResultArray = $sotMotObj['SOTmotivationResultArray'];

    //get diagnostic data
  $diaObj = app('App\Http\Controllers\Admin\AdminReportController')->getReportDiagnosticGraph($request);
  $diagnosticResultArray = $diaObj['diagnosticResultArray'];

    //get tribeometer data
  $triObj = app('App\Http\Controllers\Admin\AdminReportController')->getReportTribeometerGraph($request);
  $tribeometerResultArray = $triObj['tribeometerResultArray'];


  $file_name = 'report'.'_'.time().'.pdf'; 

  set_time_limit(60*5);
  $pdf = SnappyPDF::loadView('admin/report/report_graph/pdfReportApi', compact('org','dotValuesArray','cotTeamRoleMapGraphPercentage','funcLensPercentageArray','keyNameArray','sotCountArray','sotStrDetailArr','SOTmotivationResultArray','diagnosticResultArray','tribeometerResultArray','cotRoleMapOptions'));

  $pdf->setOption('enable-javascript', true);
  $pdf->setOption('javascript-delay', 5000);
  $pdf->setOption('enable-smart-shrinking', true);  
  $pdf->setOption('page-height', '145');
  $pdf->setOption('page-width', '200');  
  $pdf->setOption('no-stop-slow-scripts', true);     
  $pdf->save('public/uploads/pdf_report/'.$file_name);
  $url = url('public/uploads/pdf_report/'.$file_name);

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'Report-pdf-url','message'=>'','data'=>$url]);
}

/* get diagnostic sub graph values*/
public function getDiagnsticReportSubGraph()
{

  $orgId        = Input::get('orgId');
  $departmentId = Input::get('departmentId');
  $officeId     = Input::get('officeId');
  $categoryId   = Input::get('categoryId');

  $diagnosticResultArray = array();

  $flag = 0 ;
  $departments = [];
  if (!empty($officeId)) 
  {
    $departments = DB::table('departments')
    ->select('departments.id','all_department.department')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('departments.officeId',$officeId) 
    ->where('departments.status','Active') 
    ->orderBy('all_department.department','ASC')->get();

        //for new department
    $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
  }
  else
  {
    if(!empty($departmentId))
    {
      $departmentsNew = DB::table('departments')
      ->select('departments.id','all_department.department')
      ->leftJoin('all_department','all_department.id','departments.departmentId')
      ->where('departments.orgId',$orgId)
      ->where('departments.departmentId',$departmentId) 
      ->where('departments.status','Active') 
      ->orderBy('all_department.department','ASC')->get();
    }
  }

  $query = DB::table('diagnostic_answers')
  ->leftjoin('users','users.id','diagnostic_answers.userId')
  ->select('diagnostic_answers.userId')
  ->where('users.status','Active')
  ->groupBy('diagnostic_answers.userId')
  ->where('users.orgId',$orgId);

  if(!empty($officeId))
  {
    $query->where('users.officeId',$officeId);
  }


  if(!empty($departmentId))
  {   
    $i = 0;

    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {       
      if($i == 0)
      {
        $query->where('users.departmentId',$departments1->id);
      }
      else
      {
        $query->orWhere('users.departmentId',$departments1->id);
      }
      $i++;
    }
  }


  $users = $query->get();
  $userCount = count($users);

  // dd($userCount);

 // reset if department is not in organisation
  if($flag==1)
  {
    $userCount = 0;
  }

  $questionTbl = DB::table('diagnostic_questions')->where('category_id',$categoryId)->where('status','Active')->get();

  $optionsTbl = DB::table('diagnostic_question_options')->where('status','Active')->get();

  $quecount    = count($questionTbl);
  $diaOptCount = count($optionsTbl);

  $perQuePercen = 0;

  if($userCount) 
  {
    foreach($questionTbl as $value)
    {

      $diaQuery = DB::table('diagnostic_answers')            
      ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
      ->leftjoin('users','users.id','=','diagnostic_answers.userId')
      ->where('users.status','Active')
      ->where('diagnostic_answers.orgId',$orgId)
      ->where('diagnostic_questions.id',$value->id)
      ->where('diagnostic_questions.category_id',$categoryId);

      if(!empty($officeId))
      {
        $diaQuery->where('users.officeId',$officeId);
      }

      if(!empty($departmentId))
      {
       $diaQuery->where('users.departmentId',$departmentId);
     }


     $diaAnsTbl = $diaQuery->sum('answer'); 

     $perQuePercen = ($diaAnsTbl/$userCount);               

     $score = ($perQuePercen/($quecount*$diaOptCount));
     $totalPercentage = ($perQuePercen/($quecount*$diaOptCount))*100;

     $val['title']      =  ucfirst($value->measure);
     $val['score']      =  number_format((float)$score, 2, '.', '');
     $val['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');     

     array_push($diagnosticResultArray, $val);
   }
 }

 return response()->json(['code'=>200,'status'=>true,'service_name'=>'Report-diagnostic-sub-graph','message'=>'','data'=>$diagnosticResultArray]);
}


/*get dashboard report filter detail*/



public function getOrgDashboardReportWithFilter(Request $request)
{

  $resultArray = array();

  $orgId        = $request->orgId;
  $officeId     = $request->officeId;
  $departmentId = $request->departmentId;
  $request->reportStatus = true;

  $resultArray['getDOTreportGraph']       = $this->getDOTreportGraph($request);
  $resultArray['getCOTteamRoleMapReport'] = $this->getCOTteamRoleMapReport($request);
  $resultArray['getCOTpersonalityType']   = $this->getReportFunctionalLensGraph($request);
  $resultArray['getSOTcultureStructureReport'] = $this->getSOTcultureStructureReport($request);
  $resultArray['getSOTmotivationReport']       = $this->getSOTmotivationReport($request);
  $resultArray['getDiagnosticReportForGraph']  = $this->getDiagnosticReportForGraph($request);
  $resultArray['getTribeometerReportForGraph'] = $this->getTribeometerReportForGraph($request);


  $motivationStatus    = false;
  $diagnosticStatus    = false;
  $tribeometerStatus   = false; 


  $sotMotivationTblq = DB::table('sot_motivation_answers')
  ->leftjoin('users','users.id','sot_motivation_answers.userId')
  ->leftjoin('departments','users.departmentId','departments.id')
  ->leftjoin('all_department','departments.departmentId','all_department.id');

  if($officeId && !$departmentId)
  {
    $sotMotivationTblq->where('users.officeId',$officeId);
  }
  else if($officeId && $departmentId)
  {
    $sotMotivationTblq->where('users.departmentId',$departmentId);
  }
  else if(!$officeId && $departmentId)
  {
    $sotMotivationTblq->where('departments.departmentId',$departmentId);
  }    
  $sotMotivationTbl = $sotMotivationTblq->where('sot_motivation_answers.status','Active')->where('sot_motivation_answers.orgId',$orgId)->first();

  if($sotMotivationTbl)
  {
    $motivationStatus = true;
  }



  $diagnosticTblq = DB::table('diagnostic_answers')
  ->leftjoin('users','users.id','diagnostic_answers.userId')
  ->leftjoin('departments','users.departmentId','departments.id')
  ->leftjoin('all_department','departments.departmentId','all_department.id');


  if($officeId && !$departmentId)
  {
    $diagnosticTblq->where('users.officeId',$officeId);
  }
  else if($officeId && $departmentId)
  {
    $diagnosticTblq->where('users.departmentId',$departmentId);
  }
  else if(!$officeId && $departmentId)
  {
    $diagnosticTblq->where('departments.departmentId',$departmentId);
  }    

  $diagnosticTbl = $diagnosticTblq->where('diagnostic_answers.status','Active')->where('diagnostic_answers.orgId',$orgId)->first();

  if($diagnosticTbl)
  {
    $diagnosticStatus = true;
  }



  $tribeometeTblq = DB::table('tribeometer_answers')
  ->leftjoin('users','users.id','tribeometer_answers.userId')
  ->leftjoin('departments','users.departmentId','departments.id')
  ->leftjoin('all_department','departments.departmentId','all_department.id');

  if($officeId && !$departmentId)
  {
    $tribeometeTblq->where('users.officeId',$officeId);
  }
  else if($officeId && $departmentId)
  {
    $tribeometeTblq->where('users.departmentId',$departmentId);
  }
  else if(!$officeId && $departmentId)
  {
    $tribeometeTblq->where('departments.departmentId',$departmentId);
  }    
  
  $tribeometeTbl = $tribeometeTblq->where('tribeometer_answers.status','Active')->where('tribeometer_answers.orgId',$orgId)->first();

  if($tribeometeTbl)
  {
    $tribeometerStatus = true;
  }

    $user = Auth::user();
    $userId = $user->id;

    $isDiagnosticAnsDone  = DB::table('diagnostic_answers')->where('userId',$userId)->first();

    $isTribeometerAnsDone = DB::table('tribeometer_answers')->where('userId',$userId)->first();

    $isDiagnosticAnsDone1 = false;
    if(!empty($isDiagnosticAnsDone))
    {
      $isDiagnosticAnsDone1 =true;
    } 

    $isTribeometerAnsDone1 = false;
    if(!empty($isTribeometerAnsDone))
    {
      $isTribeometerAnsDone1 =true;
    } 

  $resultArray['orgStatus'] = array('motivationStatus'=>$motivationStatus,'diagnosticStatus'=>$diagnosticStatus,'tribeometerStatus'=>$tribeometerStatus,'isTribeometerAnsDone'=>$isTribeometerAnsDone1,'isDiagnosticAnsDone'=>$isDiagnosticAnsDone1);

    $resultArray['getHappyIndexYearGraphCount'] = $this->getHappyIndexYearGraphCount($request);

    if (!empty($resultArray['getHappyIndexYearGraphCount'])) {
      $happyArray = max(array_column($resultArray['getHappyIndexYearGraphCount'], 'happy'));
      $avgArray = max(array_column($resultArray['getHappyIndexYearGraphCount'], 'average'));
      $sadArray = max(array_column($resultArray['getHappyIndexYearGraphCount'], 'sad'));
    }
    else
    {
      $happyArray = 0;
      $avgArray = 0;
      $sadArray = 0;
    }


    //$max = max(array($happyArray,$avgArray,$sadArray));

    $resultArray['happyMaxCount'] = $happyArray;
    $resultArray['avgMaxCount'] = $avgArray;
    $resultArray['sadMaxCount'] = $sadArray;

    // print_r($happyArray."<br>");
    // print_r($avgArray."<br>");
    // print_r($sadArray."<br>");
    // print_r($max);
    // print_r($happyIndexCountYear);die();

  //$resultArray['orgStatus'] = array('motivationStatus'=>$motivationStatus,'diagnosticStatus'=>$diagnosticStatus,'tribeometerStatus'=>$tribeometerStatus);

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'dashboard-org-report','message'=>'','data'=>$resultArray]);

}

public function getHappyIndexYearGraphCount(Request $request)
{
  $happyIndexCountYear = array();
  $orgId        = $request->orgId;
  $officeId     = $request->officeId;
  $departmentId = $request->departmentId;
  
  $happyUsersYearlyQuery = DB::table('happy_indexes')
        ->select(DB::raw('YEAR(happy_indexes.created_at) year'))
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.orgId',$orgId)
        ->where('users.status','Active')
        ->where('happy_indexes.status','Active')
        ->whereRaw('(happy_indexes.moodValue = 3 OR happy_indexes.moodValue = 2 OR happy_indexes.moodValue = 1)');

    if(!empty($officeId) && empty($departmentId))
    {
        $happyUsersYearlyQuery->where('users.officeId',$officeId);
    }
    elseif(!empty($officeId) && !empty($departmentId))
    {
        $happyUsersYearlyQuery->where('users.officeId',$officeId);
        $happyUsersYearlyQuery->where('users.departmentId',$departmentId);
    }
    elseif(empty($officeId) && !empty($departmentId))
    {
        $happyUsersYearlyQuery->where('departments.departmentId',$departmentId);
    }

    $happyUsersYearly = $happyUsersYearlyQuery->groupBy('year')->get();

    foreach ($happyUsersYearly as $key => $value) {

        $moodValue = array("sad"=>1,"average"=>2,"happy"=>3);
        //$usersCount['indexCount'] =array();
        $usersCount =array();

        $totalUsersQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.orgId',$orgId)
        ->where('users.status','Active')
        ->where('happy_indexes.status','Active')
        ->whereYear('happy_indexes.created_at',$value->year);

        if(!empty($officeId) && empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
        }
        elseif(!empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
            $totalUsersQuery->where('users.departmentId',$departmentId);
        }
        elseif(empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('departments.departmentId',$departmentId);
        }

        $totalUsers = $totalUsersQuery->count();

        foreach ($moodValue as $mKey => $mValue) {
            $usersCountQuery = DB::table('happy_indexes')
                ->leftjoin('users','users.id','happy_indexes.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.orgId',$orgId)
                ->where('users.status','Active')
                ->where('happy_indexes.status','Active')
                ->where('happy_indexes.moodValue',$mValue)
                ->whereYear('happy_indexes.created_at',$value->year);

            if(!empty($officeId) && empty($departmentId))
            {
                $usersCountQuery->where('users.officeId',$officeId);
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $usersCountQuery->where('users.officeId',$officeId);
                $usersCountQuery->where('users.departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $usersCountQuery->where('departments.departmentId',$departmentId);
            }

            $moodCount = $usersCountQuery->count();

            if (!empty($totalUsers)) 
            {
                $moodCountUsers = number_format((($moodCount/$totalUsers)*100),2);
            }
            else
            {
                $moodCountUsers = 0;   
            }
            // array_push($usersCount['indexCount'], $moodCount);
            $usersCount[$mKey]= $moodCountUsers;
        }
        
        $usersCount['year'] = $value->year;
        array_push($happyIndexCountYear, $usersCount);
    }
    return $happyIndexCountYear;
}

public function getHappyIndexMonthCount(Request $request)
{
    $orgId        = $request->orgId;
    $officeId     = $request->officeId;
    $departmentId = $request->departmentId;
    $year = $request->year;
    $happyIndexCountMonth = array();
    
    $months = array(1=>"Jan ".$year,2=>"Feb ".$year,3=>"Mar ".$year,4=>"Apr ".$year,5=>"May ".$year,6=>"Jun ".$year,7=>"Jul ".$year,8=>"Aug ".$year,9=>"Sep ".$year,10=>"Oct ".$year,11=>"Nov ".$year,12=>"Dec ".$year);

    $monthInccount = 0;

    foreach ($months as $key => $value) {

        //$moodValue = array(1,2,3);
       $moodValue = array("sad"=>1,"average"=>2,"happy"=>3);
        $monthCount =array();
        //$monthCount['monthCount'] =array();

        $totalUsersQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.orgId',$orgId)
        ->where('users.status','Active')
        ->where('happy_indexes.status','Active')
        ->whereMonth('happy_indexes.created_at',$key)
        ->whereYear('happy_indexes.created_at',$year);

        if(!empty($officeId) && empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
        }
        elseif(!empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
            $totalUsersQuery->where('users.departmentId',$departmentId);
        }
        elseif(empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('departments.departmentId',$departmentId);
        }
        $totalUsers = $totalUsersQuery->count();

        foreach ($moodValue as $mKey => $mValue) {
            $monthCountQuery = DB::table('happy_indexes')
                ->leftjoin('users','users.id','happy_indexes.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.orgId',$orgId)
                ->where('users.status','Active')
                ->where('happy_indexes.status','Active')
                ->where('happy_indexes.moodValue',$mValue)
                ->whereMonth('happy_indexes.created_at',$key)
                ->whereYear('happy_indexes.created_at',$year);
                
            if(!empty($officeId) && empty($departmentId))
            {
                $monthCountQuery->where('users.officeId',$officeId);
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $monthCountQuery->where('users.officeId',$officeId);
                $monthCountQuery->where('users.departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $monthCountQuery->where('departments.departmentId',$departmentId);
            }
            $moodCount = $monthCountQuery->count();

            if (!empty($totalUsers)) 
            {
                $moodCountUsers = number_format((($moodCount/$totalUsers)*100),2);
            }
            else
            {
                $moodCountUsers = 0;   
            }
            $monthCount[$mKey]= $moodCountUsers;
        }
        $monthCount['monthName'] = $value;

        array_push($happyIndexCountMonth, $monthCount);
    }
    return response()->json(['code'=>200,'status'=>true,'service_name'=>'happy-index-monthly-report','data'=>$happyIndexCountMonth]);
}

public function getHappyIndexWeeksCount(Request $request)
{
    $orgId        = $request->orgId;
    $officeId     = $request->officeId;
    $departmentId = $request->departmentId;
    $year = $request->year;
    $month = $request->month;
    $happyIndexCountWeek = array();

    $monthCount = cal_days_in_month(CAL_GREGORIAN,$month,$year);

    $weeks = $monthCount%7;
    if ($weeks) {
        $arrayCount = array("Week 1","Week 2","Week 3","Week 4","Week 5");
    }
    else
    {
        $arrayCount = array("Week 1","Week 2","Week 3","Week 4");
    }
    $lastDay = date('t',strtotime($year."-".$month."-1"));
    $weekInccount = 0;
    $i = 1;$j = 7;
    foreach ($arrayCount as $key => $value) {
        $moodValue = array("sad"=>1,"average"=>2,"happy"=>3);
        $happyUsersWeekly = array();

        $totalUsersQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.orgId',$orgId)
        ->where('users.status','Active')
        ->where('happy_indexes.status','Active')
        ->whereMonth('happy_indexes.created_at',$month)
        ->whereYear('happy_indexes.created_at',$year);

        if(!empty($officeId) && empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
        }
        elseif(!empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
            $totalUsersQuery->where('users.departmentId',$departmentId);
        }
        elseif(empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('departments.departmentId',$departmentId);
        }
        if ($i==29) {
            $totalUsersQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$lastDay." "."23:59:59"]);
        }
        else
        {
            $totalUsersQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$j." "."23:59:59"]);
        }
        $totalUsers = $totalUsersQuery->count();

        foreach ($moodValue as $mKey => $mValue) {
            $happyUsersWeeklyQuery = DB::table('happy_indexes')
                ->select('happy_indexes.created_at')
                ->leftjoin('users','users.id','happy_indexes.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.orgId',$orgId)
                ->where('users.status','Active')
                ->where('happy_indexes.status','Active')
                ->where('happy_indexes.moodValue',$mValue);
            if(!empty($officeId) && empty($departmentId))
            {
                $happyUsersWeeklyQuery->where('users.officeId',$officeId);
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $happyUsersWeeklyQuery->where('users.officeId',$officeId);
                $happyUsersWeeklyQuery->where('users.departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $happyUsersWeeklyQuery->where('departments.departmentId',$departmentId);
            }
            if ($i==29) {
                $happyUsersWeeklyQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$lastDay." "."23:59:59"]);
            }
            else
            {
                $happyUsersWeeklyQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$j." "."23:59:59"]);
            }
            $moodCount = $happyUsersWeeklyQuery->count();

            if (!empty($totalUsers)) 
            {
                $moodCountUsers = number_format((($moodCount/$totalUsers)*100),2);
            }
            else
            {
                $moodCountUsers = 0;   
            }

            $happyUsersWeekly[$mKey] = $moodCountUsers;
        }
        $i+=7;
        $j+=7;
        $happyUsersWeekly['week'] = $value;

        array_push($happyIndexCountWeek, $happyUsersWeekly);
    }
    return response()->json(['code'=>200,'status'=>true,'service_name'=>'happy-index-weekly-report','data'=>$happyIndexCountWeek]);
}

public function getHappyIndexDaysCount(Request $request)
{

    $orgId        = $request->orgId;
    $officeId     = $request->officeId;
    $departmentId = $request->departmentId;
    $year = $request->year;
    $month = $request->month;
    $week = $request->week;
    $happyIndexCountDay = array();
    $days = array();

    $monthCount = cal_days_in_month(CAL_GREGORIAN,$month,$year);

    $remainingWeeks = $monthCount%7;

    if ($month == 1) {
        $monthName = "Jan";
    }elseif ($month == 2) {
        $monthName = "Feb";
    }elseif ($month == 3) {
        $monthName = "Mar";
    }elseif ($month == 4) {
        $monthName = "Apr";
    }elseif ($month == 5) {
        $monthName = "May";
    }elseif ($month == 6) {
        $monthName = "Jun";
    }elseif ($month == 7) {
        $monthName = "Jul";
    }elseif ($month == 8) {
        $monthName = "Aug";
    }elseif ($month == 9) {
        $monthName = "Sep";
    }elseif ($month == 10) {
        $monthName = "Oct";
    }elseif ($month == 11) {
        $monthName = "Nov";
    }elseif ($month == 12) {
        $monthName = "Dec";
    }


    if ($week == 1) {
        $i=1;
        //$days = array($monthName." 1 ".$year,$monthName." 2 ".$year,$monthName." 3 ".$year,$monthName." 4 ".$year,$monthName." 5 ".$year,$monthName." 6 ".$year,$monthName." 7 ".$year);
        $days = array("1 ".$monthName." ".$year,"2 ".$monthName." ".$year,"3 ".$monthName." ".$year,"4 ".$monthName." ".$year,"5 ".$monthName." ".$year,"6 ".$monthName." ".$year,"7 ".$monthName." ".$year);
        //$days = array("Day 1","Day 2","Day 3","Day 4","Day 5","Day 6","Day 7");
    }
    else if ($week == 2) {
        $i=8;
        $days = array("8 ".$monthName." ".$year,"9 ".$monthName." ".$year,"10 ".$monthName." ".$year,"11 ".$monthName." ".$year,"12 ".$monthName." ".$year,"13 ".$monthName." ".$year,"14 ".$monthName." ".$year);
        //$days = array($monthName." 8 ".$year,$monthName." 9 ".$year,$monthName." 10 ".$year,$monthName." 11 ".$year,$monthName." 12 ".$year,$monthName." 13 ".$year,$monthName." 14 ".$year);
        //$days = array("Day 8","Day 9","Day 10","Day 11","Day 12","Day 13","Day 14");
    }
    else if ($week == 3) {
        $i=15;
        $days = array("15 ".$monthName." ".$year,"16 ".$monthName." ".$year,"17 ".$monthName." ".$year,"18 ".$monthName." ".$year,"19 ".$monthName." ".$year,"20 ".$monthName." ".$year,"21 ".$monthName." ".$year);
       // $days = array($monthName." 15 ".$year,$monthName." 16 ".$year,$monthName." 17 ".$year,$monthName." 18 ".$year,$monthName." 19 ".$year,$monthName." 20 ".$year,$monthName." 21 ".$year);
        //$days = array("Day 15","Day 16","Day 17","Day 18","Day 19","Day 20","Day 21");
    }
    else if ($week == 4) {
        $i=22;
        $days = array($monthName." 22 ".$year,$monthName." 23 ".$year,$monthName." 24 ".$year,$monthName." 25 ".$year,$monthName." 26 ".$year,$monthName." 27 ".$year,$monthName." 28 ".$year);
        //$days = array("Day 22","Day 23","Day 24","Day 25","Day 26","Day 27","Day 28");
    }
    else if ($week == 5) {
        $i=29;
        if ($remainingWeeks == 1) {
          $days = array("29 ".$monthName." ".$year);
          // $days = array($monthName." 29 ".$year);
            //$days = array("Day 29");
        }
        elseif ($remainingWeeks == 2) {
          $days = array("29 ".$monthName." ".$year,"30 ".$monthName." ".$year);
           //$days = array($monthName." 29 ".$year,$monthName." 30 ".$year);
            //$days = array("Day 29","Day 30");
        }elseif ($remainingWeeks == 3) {
          $days = array("29 ".$monthName." ".$year,"30 ".$monthName." ".$year,"31 ".$monthName." ".$year);
          //$days = array($monthName." 29 ".$year,$monthName." 30 ".$year,$monthName." 31 ".$year);
            //$days = array("Day 29","Day 30","Day 31");
        }
    }

    foreach ($days as $key => $value) {
        $moodValue = array("sad"=>1,"average"=>2,"happy"=>3);
        $dayCount =array();

        $totalUsersQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.orgId',$orgId)
        ->where('users.status','Active')
        ->where('happy_indexes.status','Active')
        ->whereDate('happy_indexes.created_at',$year."-".$month."-".$i);

        if(!empty($officeId) && empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
        }
        elseif(!empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
            $totalUsersQuery->where('users.departmentId',$departmentId);
        }
        elseif(empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('departments.departmentId',$departmentId);
        }
        $totalUsers = $totalUsersQuery->count();

        foreach ($moodValue as $mKey => $mValue) {
            $dayCountquery = DB::table('happy_indexes')
                ->select('happy_indexes.created_at')
                ->leftjoin('users','users.id','happy_indexes.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.orgId',$orgId)
                ->where('users.status','Active')
                ->where('happy_indexes.status','Active')
                ->where('happy_indexes.moodValue',$mValue)
                ->whereDate('happy_indexes.created_at',$year."-".$month."-".$i);
            if(!empty($officeId) && empty($departmentId))
            {
                $dayCountquery->where('users.officeId',$officeId);
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $dayCountquery->where('users.officeId',$officeId);
                $dayCountquery->where('users.departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $dayCountquery->where('departments.departmentId',$departmentId);
            }

            $moodCount = $dayCountquery->count();

            if (!empty($totalUsers)) 
            {
                $moodCountUsers = number_format((($moodCount/$totalUsers)*100),2);
            }
            else
            {
                $moodCountUsers = 0;   
            }

            $dayCount[$mKey] = $moodCountUsers;
        }
        $dayCount['dayName'] = $value; 

        array_push($happyIndexCountDay, $dayCount);
       
        $i++;
    }
    return response()->json(['code'=>200,'status'=>true,'service_name'=>'happy-index-daily-report','data'=>$happyIndexCountDay]);

}



//public function getOrgDashboardReportWithFilter(Request $request)
// {

//   $resultArray = array();

//   $orgId        = $request->orgId;
//   $officeId     = $request->officeId;
//   $departmentId = $request->departmentId;
//   $request->reportStatus = true;

//   $resultArray['getDOTreportGraph']       = $this->getDOTreportGraph($request);
//   $resultArray['getCOTteamRoleMapReport'] = $this->getCOTteamRoleMapReport($request);
//   $resultArray['getCOTpersonalityType']   = $this->getReportFunctionalLensGraph($request);
//   $resultArray['getSOTcultureStructureReport'] = $this->getSOTcultureStructureReport($request);
//   $resultArray['getSOTmotivationReport']       = $this->getSOTmotivationReport($request);
//   $resultArray['getDiagnosticReportForGraph']  = $this->getDiagnosticReportForGraph($request);
//   $resultArray['getTribeometerReportForGraph'] = $this->getTribeometerReportForGraph($request);


//   $motivationStatus    = false;
//   $diagnosticStatus    = false;
//   $tribeometerStatus   = false; 


//   $sotMotivationTblq = DB::table('sot_motivation_answers')
//   ->leftjoin('users','users.id','sot_motivation_answers.userId')
//   ->leftjoin('departments','users.departmentId','departments.id')
//   ->leftjoin('all_department','departments.departmentId','all_department.id');

//   if($officeId)
//   {
//     $sotMotivationTblq->where('users.officeId',$officeId);
//    // $sotMotivationTblq->where('users.departmentId',$departmentId);
//   }
//   if(empty($officeId) && !empty($departmentId))
//   {
//     $sotMotivationTblq->where('departments.departmentId',$departmentId);
//   }    
//   $sotMotivationTbl = $sotMotivationTblq->where('sot_motivation_answers.status','Active')->where('sot_motivation_answers.orgId',$orgId)->first();

//   if($sotMotivationTbl)
//   {
//     $motivationStatus = true;
//   }



//   // $diagnosticTblq = DB::table('diagnostic_answers')
//   // ->leftjoin('users','users.id','diagnostic_answers.userId');

//   // if($officeId)
//   // {
//   //   $diagnosticTblq->where('users.officeId',$officeId);
//   // }
//   // if($departmentId)
//   // {
//   //   $diagnosticTblq->where('users.departmentId',$departmentId);
//   // }    

//   // $diagnosticTbl = $diagnosticTblq->where('diagnostic_answers.status','Active')->where('diagnostic_answers.orgId',$orgId)->first();


//   $diagnosticTblq = DB::table('diagnostic_answers')
//   ->leftjoin('users','users.id','diagnostic_answers.userId')
//   ->leftjoin('departments','users.departmentId','departments.id')
//   ->leftjoin('all_department','departments.departmentId','all_department.id');

//   if($officeId)
//   {
//     $diagnosticTblq->where('users.officeId',$officeId);
//     //$diagnosticTblq->where('users.departmentId',$departmentId);
//   }
//   if(empty($officeId) && !empty($departmentId))
//   {
//     $diagnosticTblq->where('departments.departmentId',$departmentId);
//   }    
//   // if($departmentId)
//   // {
//   //   $diagnosticTblq->where('departments.departmentId',$departmentId);
//   // }    

//   $diagnosticTbl = $diagnosticTblq->where('diagnostic_answers.status','Active')->where('diagnostic_answers.orgId',$orgId)->first();

//   if($diagnosticTbl)
//   {
//     $diagnosticStatus = true;
//   }



//   // $tribeometeTblq = DB::table('tribeometer_answers')
//   // ->leftjoin('users','users.id','tribeometer_answers.userId');

//   // if($officeId)
//   // {
//   //   $tribeometeTblq->where('users.officeId',$officeId);
//   // }
//   // if($departmentId)
//   // {
//   //   $tribeometeTblq->where('users.departmentId',$departmentId);
//   // }    
  
//   // $tribeometeTbl = $tribeometeTblq->where('tribeometer_answers.status','Active')->where('tribeometer_answers.orgId',$orgId)->first();

//   $tribeometeTblq = DB::table('tribeometer_answers')
//   ->leftjoin('users','users.id','tribeometer_answers.userId')
//   ->leftjoin('departments','users.departmentId','departments.id')
//   ->leftjoin('all_department','departments.departmentId','all_department.id');

//   if($officeId)
//   {
//     $tribeometeTblq->where('users.officeId',$officeId);
//    // $tribeometeTblq->where('users.departmentId',$departmentId);
//   }
//   if(empty($officeId) && !empty($departmentId))
//   {
//     $tribeometeTblq->where('departments.departmentId',$departmentId);
//   }   
//   // if($departmentId)
//   // {
//   //   $tribeometeTblq->where('departments.departmentId',$departmentId);
//   // }    
  
//   $tribeometeTbl = $tribeometeTblq->where('tribeometer_answers.status','Active')->where('tribeometer_answers.orgId',$orgId)->first();

//   if($tribeometeTbl)
//   {
//     $tribeometerStatus = true;
//   }

//     $user = Auth::user();
//     $userId = $user->id;

//     $isDiagnosticAnsDone  = DB::table('diagnostic_answers')->where('userId',$userId)->first();

//     $isTribeometerAnsDone = DB::table('tribeometer_answers')->where('userId',$userId)->first();

//     $isDiagnosticAnsDone1 = false;
//     if(!empty($isDiagnosticAnsDone))
//     {
//       $isDiagnosticAnsDone1 =true;
//     } 

//     $isTribeometerAnsDone1 = false;
//     if(!empty($isTribeometerAnsDone))
//     {
//       $isTribeometerAnsDone1 =true;
//     } 

//   $resultArray['orgStatus'] = array('motivationStatus'=>$motivationStatus,'diagnosticStatus'=>$diagnosticStatus,'tribeometerStatus'=>$tribeometerStatus,'isTribeometerAnsDone'=>$isTribeometerAnsDone1,'isDiagnosticAnsDone'=>$isDiagnosticAnsDone1);

//   return response()->json(['code'=>200,'status'=>true,'service_name'=>'dashboard-org-report','message'=>'','data'=>$resultArray]);

//}



/*get user speciafic reports*/
public function getUserDashboardReport(Request $request)
{
  $request->reportStatus = true;

  $userId = $request->userId;
  $orgId  = $request->orgId; 

  $request->userId = $userId;
  $request->orgId  = $orgId; 

  $cotIndidualReport = app('App\Http\Controllers\API\ApiCOTController')->getCOTindividualSummary($request);
  $getCOTpersonalityType = app('App\Http\Controllers\API\ApiCOTController')->getCOTFunctionalLensDetail($request);
  $getSOTdetail = app('App\Http\Controllers\API\ApiSOTController')->getSOTdetail($request);
  $getSOTmotivationUserList = app('App\Http\Controllers\API\ApiSOTController')->getSOTmotivationReport($request);

  $resultArray['getDOTreportGraph']       = $this->getDOTuserReport($request);
  $resultArray['getCOTindividualSummary'] = $cotIndidualReport;
  $resultArray['getCOTpersonalityType']   = $getCOTpersonalityType;
  $resultArray['getSOTdetail']            = $getSOTdetail;
  $resultArray['getSOTmotivationUserList']= $getSOTmotivationUserList;

  $request->year    = date('Y', strtotime('-1 months'));
  $request->month   = date('m', strtotime('-1 months'));

  $getBubbleRating = app('App\Http\Controllers\API\ApiDotController')->getBubbleRatingList($request);

  $resultArray['getBubbleRatingList'] = $getBubbleRating;


  $dotStatus             = false;
  $cotTeamRoleStatus     = false;
  $cotPersonalityStatus  = false;
  $sotStatus             = false;
  $sotMotivationStatus   = false;
  $bubbleRatingStatus    = false;

  if(!empty($resultArray['getDOTreportGraph']))
  {
    $dotStatus = true;
    $bubbleRatingStatus = true;
  }

  $teamRoleTbl = DB::table('cot_answers')->where('status','Active')->where('userId',$userId)->where('orgId',$orgId)->first();

  if($teamRoleTbl)
  {
    $cotTeamRoleStatus = true;
  }

  $cotPersonalityTbl = DB::table('cot_functional_lens_answers')->where('status','Active')->where('userId',$userId)->where('orgId',$orgId)->first();

  if($cotPersonalityTbl)
  {
    $cotPersonalityStatus = true;
  }

  $sotTbl = DB::table('sot_answers')->where('status','Active')->where('userId',$userId)->first();

  if($sotTbl)
  {
    $sotStatus = true;
  }

  $sotMotivationTbl = DB::table('sot_motivation_answers')->where('status','Active')->where('userId',$userId)->where('orgId',$orgId)->first();

  if($sotMotivationTbl)
  {
    $sotMotivationStatus = true;
  }

  $resultArray['userStatus'] = array('dotStatus'=>$dotStatus,'cotTeamRoleStatus'=>$cotTeamRoleStatus,'cotPersonalityStatus'=>$cotPersonalityStatus,'sotStatus'=>$sotStatus,'sotMotivationStatus'=>$sotMotivationStatus,'bubbleRatingStatus'=>$bubbleRatingStatus);

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'dashboard-user-report','message'=>'','data'=>$resultArray]);
}

/*get DOT user report*/
public function getDOTuserReport(Request $request)
{

  $orgId   = $request->orgId;  
  $userId  = $request->userId;

  $dots = DB::table('dots')->where('orgId',$orgId)->first();

  $dotValuesArray = array();

  if(!empty($dots))
  {

    $dotBeliefs = DB::table('dots_beliefs')->where('status','Active')->where('dotId',$dots->id)->orderBy('id','ASC')->get();  

    foreach($dotBeliefs as $bValue)
    {

      $bRatings = DB::table('dot_values_ratings')
      ->leftjoin('users','users.id','=','dot_values_ratings.userId')
      ->where('users.status','Active')
      ->where('dot_values_ratings.userId', $userId)
      ->where('beliefId', $bValue->id)
      ->avg('ratings'); 

      $dotValues = DB::table('dots_values')
      ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
      ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
      ->where('dots_values.status','Active')      
      ->where('dots_values.beliefId',$bValue->id)
      ->orderBy('dot_value_list.id','ASC')->get();

      $valuesArray = array();
      foreach ($dotValues as $key => $vValue) 
      {

        $vRatings = DB::table('dot_values_ratings')
        ->leftjoin('users','users.id','=','dot_values_ratings.userId')
        ->where('users.status','Active')
        ->where('dot_values_ratings.userId', $userId)
        ->where('valueId', $vValue->id)->avg('ratings');
        
        $vResult['valueId']   = $vValue->id;
        $vResult['valueName'] = ucfirst($vValue->name);

        $vResult['valueRatings'] = 0;
        if($vRatings)
        {
          $vResult['valueRatings'] = number_format(($vRatings-1), 2);
        }
        array_push($valuesArray, $vResult);
      }

      $result['beliefId']      = $bValue->id;
      $result['beliefName']    = ucfirst($bValue->name);           

      $result['beliefRatings'] = 0;
      if($bRatings)
      {
        $result['beliefRatings'] = number_format(($bRatings-1), 2);
      }

      $result['beliefValues']  = $valuesArray;

      array_push($dotValuesArray, $result);
    }
  }

  if($request->reportStatus)
  {
    return $dotValuesArray;
  }

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'DOT-report','message'=>'','data'=>$dotValuesArray]);
}

}
