<?php

namespace App\Http\Controllers\api;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use DB;
use Mail;
use View;
use Dompdf\Dompdf;
use Dompdf\Options;
use SnappyPDF;

class ApiReportController extends Controller
{

  /*get DOT report*/
  public function getDOTreportGraph()
  {

    $orgId     = Input::get('orgId');
    $beliefId  = Input::get('beliefId');
    $officeId  = Input::get('officeId');

    $dots = DB::table('dots')->where('orgId',$orgId)->first();

    $dotValuesArray = array();

    if(!empty($dots))
    {

      $query = DB::table('dots_beliefs')
      ->where('status','Active')
      ->where('dotId',$dots->id);  

      if(!empty($beliefId))
      {
        $query->where('id',$beliefId);
      }

      $query->orderBy('id','ASC');
      $dotBeliefs = $query->get();

      foreach ($dotBeliefs as $key => $bValue)
      {

        $dotValues = DB::table('dots_values')
        ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
        ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
        ->where('dots_values.status','Active')
        ->where('dots_values.beliefId',$bValue->id)
        ->orderBy('dot_value_list.id','ASC')
        ->get();

        $bRatings = DB::table('dot_values_ratings')
        ->leftjoin('users','users.id','=','dot_values_ratings.userId')
        ->where('users.status','Active') 
        ->where('beliefId', $bValue->id)->avg('ratings');

        $valuesArray = array();
        foreach ($dotValues as $key => $vValue) 
        {
          $vRatings = DB::table('dot_values_ratings')
          ->leftjoin('users','users.id','=','dot_values_ratings.userId')
          ->where('users.status','Active')
          ->where('valueId', $vValue->id)->avg('ratings');

          $vResult['valueId']      = $vValue->id;
          $vResult['valueName']    = ucfirst($vValue->name);

          $vResult['valueRatings'] = 0;
          if($vRatings)
          {
            $vResult['valueRatings'] = number_format(($vRatings-1), 2);
          }
          array_push($valuesArray, $vResult);
        }

        $result['beliefId']      = $bValue->id;
        $result['beliefName']    = ucfirst($bValue->name);           

        $result['beliefRatings'] = 0;
        if($bRatings)
        {
          $result['beliefRatings'] = number_format(($bRatings-1), 2);
        }

        $result['beliefValues']  = $valuesArray;

        //filter by office 
        if(!empty($officeId)) 
        {

          $bRatingsQuery = DB::table('dot_values_ratings')
          ->leftjoin('users','users.id','=','dot_values_ratings.userId')
          ->where('users.status','Active')
          ->where('dot_values_ratings.status','Active')
          ->where('dot_values_ratings.beliefId', $bValue->id);
          $bRatingsQuery->where('users.officeId', $officeId);

          $isofficeRating = $bRatingsQuery->avg('ratings');

          if($isofficeRating)
          {
            array_push($dotValuesArray, $result);
          }
        }
        else
        {
          array_push($dotValuesArray, $result);
        }  


      }
    }

    return response()->json(['code'=>200,'status'=>true,'service_name'=>'DOT-report','message'=>'','data'=>$dotValuesArray]);
  }

  /*get COT functional lens report*/
  public function getReportFunctionalLensGraph()
  {

    $orgId        = Input::get('orgId');  
    $officeId     = Input::get('officeId');
    $departmentId = Input::get('departmentId');

    $flag = 0 ;
    $departments = [];
    if(!empty($officeId)) 
    {          
      //for new department
      $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }
    else
    {
      if(!empty($departmentId))
      {
        $departmentsNew = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.orgId',$orgId)
        ->where('departments.departmentId',$departmentId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();
      }
    }

    $cotFunResultArray = array();

    $query = DB::table('cot_functional_lens_answers')
    ->select('cot_functional_lens_answers.userId AS id')
    ->leftjoin('users','users.id','cot_functional_lens_answers.userId')    
    ->where('users.status','Active')
    ->groupBy('cot_functional_lens_answers.userId')
    ->where('users.orgId',$orgId);

    if(!empty($officeId))
    {
     $query->where('officeId',$officeId);
   }
   if(!empty($departmentId))
   {   
    $i = 0;
    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {         
      if($i == 0)
      {
        $query->where('users.departmentId',$departments1->id);
      }
      else
      {
        $query->orWhere('users.departmentId',$departments1->id);
      }
      $i++;
    }
  }

  $usersList = $query->get();

// reset if department is not in organisation
  if($flag==1)
  {
    $usersList = array();
  }

  foreach ($usersList as $userValue)
  {

    $variable = app('App\Http\Controllers\Admin\CommonController')->getCotFunLensInitialValue();

    $initialValEIArray = array();

    foreach ($variable as $value)
    {

      $value1 = $value[0];
      $value2 = $value[1];

      $countE = DB::table('cot_functional_lens_answers AS cfla')
      ->select('cflqo.option_name AS optionName')
      ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
      ->leftJoin('users','users.id','=','cfla.userId')
      ->where('users.status','Active')
      ->where('cfla.userId',$userValue->id)->where('cflqo.initial_value_id',$value1)->get();

      $countI = DB::table('cot_functional_lens_answers AS cfla')
      ->select('cflqo.option_name AS optionName')
      ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
      ->leftJoin('users','users.id','=','cfla.userId')
      ->where('users.status','Active')
      ->where('cfla.userId',$userValue->id)->where('cflqo.initial_value_id',$value2)->get();

      $initialValEI  = '';
      if(count($countE) > count($countI))
      {            
        $initialValEI  =  $value1;
      }
      elseif (count($countE) < count($countI))
      {           
        $initialValEI  =  $value2;
      }
      elseif(count($countE) == count($countI))
      {
       $initialValEI  =  $value1;
     }           
     array_push($initialValEIArray, $initialValEI);

   }


   $valueTypeArray = array();
   $valueTypeArray['value'] = array();

   if(!empty($initialValEIArray))
   {
    $valueTypeArray = array('value'=>$initialValEIArray);
  }

  $matchValueArr =array();
  for($i=0; $i < count($valueTypeArray['value']); $i++)
  {
    $valuesKey = '';
    $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('title',$valueTypeArray['value'][$i])->first();  

    if(!empty($table))
    {
      $valuesKey = $table->value;
    }

    array_push($matchValueArr, $valuesKey); 
  }

       // dd($matchValue);

    //remove last and first string
  // $tribeMatchValue = substr(implode('', $matchValueArr), 1, -1);
  $tribeMatchValue = json_encode(array_slice($valueTypeArray['value'], 1, -1));

  $tribeTipsList = DB::table('cot_functional_lens_tribe_tips_records')->where('value',$tribeMatchValue)->get();

  $tribeTipsArray = array();
  foreach ($tribeTipsList as $ttvalue)
  {
    $tTips['value']   = $ttvalue->value;       
    array_push($cotFunResultArray, $tTips);
  }
}

$ST =0;
$SF =0;
$NF =0;
$NT =0;

foreach ($cotFunResultArray as $finalArrayValue)
{ 
 if($finalArrayValue['value']=='["7","9"]'){

   $ST++;
 }elseif ($finalArrayValue['value']=='["7","10"]'){

   $SF++;
 }elseif ($finalArrayValue['value']=='["8","10"]'){

   $NF++;
 }elseif ($finalArrayValue['value']=='["8","9"]'){

   $NT++;
 }

}

$totalUser = count($usersList);


if (!empty($totalUser))
{
  $stPercent = number_format((($ST/$totalUser)*100),2);
  $sfPercent = number_format((($SF/$totalUser)*100),2);
  $nfPercent = number_format((($NF/$totalUser)*100),2);
  $ntPercent = number_format((($NT/$totalUser)*100),2);
}
else
{
  $stPercent = 0;
  $sfPercent = 0;
  $nfPercent = 0;
  $ntPercent = 0;
}

$funcLensPercentageArray = array('st'=>$stPercent,'sf'=>$sfPercent,'nf'=>$nfPercent,'nt'=>$ntPercent);

return response()->json(['code'=>200,'status'=>true,'service_name'=>'COT-functional-lens-report','message'=>'','data'=>$funcLensPercentageArray]);
}

/*get overpresented,underpresented user of COT*/
public function getCOTteamRoleMapReport()
{

  $orgId        = Input::get('orgId');
  $officeId     = Input::get('officeId');
  $departmentId = Input::get('departmentId');

  $resultArray = array();
  $finalArray  = array();
  $rankArray   = array();
  
  $flag = 0 ;

  $departments = [];
  if (!empty($officeId)) 
  {
   //for new department
    $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
  }
  else
  {
    if(!empty($departmentId))
    {
      $departmentsNew = DB::table('departments')
      ->select('departments.id','all_department.department')
      ->leftJoin('all_department','all_department.id','departments.departmentId')
      ->where('departments.orgId',$orgId)
      ->where('departments.departmentId',$departmentId) 
      ->where('departments.status','Active') 
      ->orderBy('all_department.department','ASC')->get();
    }

  }

  $query = DB::table('cot_answers')
  ->select('users.id','users.orgId','users.name')
  ->leftjoin('users','users.id','cot_answers.userId')
  ->where('users.status','Active')
  ->where('users.orgId',$orgId);

  if(!empty($officeId))
  {
    $query->where('users.officeId',$officeId);
  }
  if(!empty($departmentId))
  {   
    $i = 0;
    
    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {
      if($i == 0)
      {
        $query->where('users.departmentId',$departments1->id);
      }
      else
      {
        $query->orWhere('users.departmentId',$departments1->id);
      }
      $i++;
    }
  }

  $users = $query->get();

  $resultArray1 = array();

  foreach ($users as  $value)
  {
    $data['id'] = $value->id;
    $data['orgId'] = $value->orgId;
    $data['name'] = $value->name;

    array_push($resultArray1, $data);
  }

  $users23 = array_unique($resultArray1,SORT_REGULAR);

  $finalUser = array();

  foreach ($users23 as  $value55)
  {
    $object = (object)array();    
    
    $object->id    = $value55['id'];
    $object->orgId = $value55['orgId'];
    $object->name  = $value55['name'];

    array_push($finalUser,$object);
  }
// reset if department is not in organisation
  if($flag==1)
  {
    $finalUser = array();
  }

  $cotRoleMapOptions = DB::table('cot_role_map_options')->where('status','Active')->orderBy('id','ASC')->get();

  $usersArray = array();
  foreach ($finalUser as $key => $value) 
  {

    foreach($cotRoleMapOptions as $key => $maper)
    {
      $maperCount = DB::table('cot_answers')
      ->where('orgId',$value->orgId)
      ->where('userId',$value->id)
      ->where('cot_role_map_option_id',$maper->id)->where('status','Active')->sum('answer');

      $resultArray = array();

      $Value[$maper->maper_key] = $maperCount;    

      array_push($resultArray, $Value);
    }   

    arsort($resultArray[0]);

    $i = 0;
    $prev = "";
    $j = 0;

    $new  = array();
    $new1 = array();

    foreach($resultArray[0] as $key => $val)
    { 
      if($val != $prev)
      { 
        $i++; 
      }

      $new1['title'] = $key; 
      $new1['value'] = $i;        
      $prev = $val;

      if ($key==$cotRoleMapOptions[0]->maper_key) {
        $new1['priority'] = 1;
      }elseif ($key==$cotRoleMapOptions[1]->maper_key) {
        $new1['priority'] = 2;
      }elseif ($key==$cotRoleMapOptions[2]->maper_key) {
        $new1['priority'] = 3;
      }elseif ($key==$cotRoleMapOptions[3]->maper_key) {
        $new1['priority'] = 4;
      }elseif ($key==$cotRoleMapOptions[4]->maper_key) {
        $new1['priority'] = 5;
      }elseif ($key==$cotRoleMapOptions[5]->maper_key) {
        $new1['priority'] = 6;
      }elseif ($key==$cotRoleMapOptions[6]->maper_key) {
        $new1['priority'] = 7;
      }elseif ($key==$cotRoleMapOptions[7]->maper_key) {
        $new1['priority'] = 8;
      }

      array_push($new,$new1);
    } 

    $value->new = $new;

    $value->totalKeyCount = $resultArray[0];

    array_push($usersArray, $value);

  }

  $usersArray = $this->sortarr($usersArray);

  $shaper = 0; 
  $coordinator = 0; 
  $completerFinisher = 0; 
  $teamworker = 0; 
  $implementer = 0; 
  $monitorEvaluator = 0; 
  $plant = 0; 
  $resourceInvestigator = 0; 

  foreach ($usersArray as $key1 => $value1)
  {

    if($value1->shaper==1 || $value1->shaper==2 || $value1->shaper==3)
    {
      $shaper++;     
    }
    if($value1->coordinator==1 || $value1->coordinator==2 || $value1->coordinator==3)
    {
      $coordinator++;
    }
    if($value1->completerFinisher==1 || $value1->completerFinisher==2 || $value1->completerFinisher==3)
    {
      $completerFinisher++;
    }
    if($value1->teamworker==1 || $value1->teamworker==2 || $value1->teamworker==3)
    {
      $teamworker++;
    }
    if($value1->implementer==1 || $value1->implementer==2 || $value1->implementer==3)
    {
      $implementer++;
    }
    if($value1->monitorEvaluator==1 || $value1->monitorEvaluator==2 || $value1->monitorEvaluator==3)
    {
      $monitorEvaluator++;
    }
    if($value1->plant==1 || $value1->plant==2 || $value1->plant==3)
    {
      $plant++;
    }
    if($value1->resourceInvestigator==1 || $value1->resourceInvestigator==2 || $value1->resourceInvestigator==3)
    {
      $resourceInvestigator++;
    }
    
  }

  $data['shaper']              = $shaper;
  $data['coordinator']         = $coordinator;
  $data['completerFinisher']   = $completerFinisher;
  $data['teamworker']          = $teamworker;
  $data['implementer']         = $implementer;
  $data['monitorEvaluator']    = $monitorEvaluator;
  $data['plant']               = $plant;
  $data['resourceInvestigator']= $resourceInvestigator;


  $totalUsers = count($finalUser);

/*  foreach ($data as $key => $value)
{*/

 if (!empty($data) && (!empty($totalUsers)))
 {

  $data['shaper']              = number_format((($shaper/$totalUsers)*100),2);
  $data['coordinator']         = number_format((($coordinator/$totalUsers)*100),2);
  $data['completerFinisher']   = number_format((($completerFinisher/$totalUsers)*100),2);
  $data['teamworker']          = number_format((($teamworker/$totalUsers)*100),2);
  $data['implementer']         = number_format((($implementer/$totalUsers)*100),2);
  $data['monitorEvaluator']    = number_format((($monitorEvaluator/$totalUsers)*100),2);
  $data['plant']               = number_format((($plant/$totalUsers)*100),2);
  $data['resourceInvestigator']= number_format((($resourceInvestigator/$totalUsers)*100),2);
}
else
{
  $data['shaper']              = 0;
  $data['coordinator']         = 0;
  $data['completerFinisher']   = 0;
  $data['teamworker']          = 0;
  $data['implementer']         = 0;
  $data['monitorEvaluator']    = 0;
  $data['plant']               = 0;
  $data['resourceInvestigator']= 0;
}

// }

return response()->json(['code'=>200,'status'=>true,'service_name'=>'COT-maper-rank','message'=>'COT maper rank','data'=>$data]);

}

/*sorting array by priority*/
function sortarr($arr1)
{

//   print_r($arr1[0]->totalKeyCount);
// die();
  $arrUsers = array();
  $newArray = array();
  $tes =array();

  for($i=0; $i<count($arr1); $i++)
  {    
    $tes['userId']    = $arr1[$i]->id;
    $tes['userName']  = $arr1[$i]->name;   

    $arrt = (array)$arr1[$i]->new;
    
    usort($arrt, function($x, $y)
    {

      if ($x['value']== $y['value'] ) 
      {
        if($x['priority']<$y['priority'])
        {
          return 0;
        }
        else
        {
          return 1;
        } 
      }
    }); 

    $tt = array();
    foreach($arrt as $key => $value)
    {         
      if ($value['value']!=0)
      {
        $tes[$value['title']] = $key+1;
      } else{
        $tes[$value['title']] = 0;
      }

    }   

    $tes['totalKeyCount'] = $arr1[$i]->totalKeyCount;

    array_push($newArray,(object)$tes);
  }

  // print_r($newArray);
  return $newArray;

}


/*get SOT culture structure report */
public function getSOTcultureStructureReport()
{

 $resultArray = array();
 $user        = Auth::user();

 $orgId        = Input::get('orgId');
 $officeId     = Input::get('officeId');
 $departmentId = Input::get('departmentId');

 $flag = 0 ;
 $departments = [];
 if (!empty($officeId)) 
 {
  //for new department
  $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
}
else
{
  if(!empty($departmentId))
  {
    $departmentsNew = DB::table('departments')
    ->select('departments.id','all_department.department')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('departments.orgId',$orgId)
    ->where('departments.departmentId',$departmentId) 
    ->where('departments.status','Active') 
    ->orderBy('all_department.department','ASC')->get();
  }
}

$sotCultureStrTbl = DB::table('sot_culture_structure_records')->select('id','type','title','imgUrl')->where('status','Active')->get();

$sotCountArray = array();
$countArr      = array();
$resultArray['IsQuestionnaireAnswerFilled'] = false;

foreach ($sotCultureStrTbl as $value) 
{

  $query = DB::table('sot_answers')
  ->select(DB::raw('sum(score) AS count'))
  ->leftJoin('sot_questionnaire_records','sot_questionnaire_records.id','=','sot_answers.question_id')
  ->join('users','users.id','=','sot_answers.userId')
  ->where('users.status','Active')
  ->where('sot_questionnaire_records.type',$value->type)    
  ->where('users.orgId',$orgId);

  if(!empty($officeId))
  {
   $query->where('users.officeId',$officeId);
 }

 if(!empty($departmentId))
 {   
  $i = 0;

  if($departmentsNew->isEmpty())
  {
    $flag = 1;
  }
  foreach ($departmentsNew as $departments1) 
  {       
    if($i == 0)
    {
      $query->where('users.departmentId',$departments1->id);
    }
    else
    {
      $query->orWhere('users.departmentId',$departments1->id);
    }
    $i++;
  }
}       

$SOTCount = $query->first();

$value->SOTCount = ($SOTCount->count)?(string)$SOTCount->count:"0";

// reset if department is not in organisation
if($flag==1)
{    
  $value->SOTCount = "0";
}


$value->imgUrl   = url('public/uploads/sot/').'/'.$value->imgUrl;

if ($SOTCount->count) {
  $resultArray['IsQuestionnaireAnswerFilled']   = true;
}


$IsUserFilledAnswer = DB::table('sot_answers')
->select(DB::raw('sum(score) AS count'))->where('userId',$user->id)->first();

if ($IsUserFilledAnswer->count) {
  $resultArray['IsUserFilledAnswer']   = true;
}
else
{
  $resultArray['IsUserFilledAnswer']   = false;
}

array_push($countArr, $SOTCount->count);
array_push($sotCountArray, $value);

}

if($flag==1)
{
  $sotCountArray  = array();
}

// find maximum value array
$sotCountArray1 = array();
foreach ($sotCultureStrTbl as $value) 
{

  $query = DB::table('sot_answers')
  ->select(DB::raw('sum(score) AS count'))
  ->leftJoin('sot_questionnaire_records','sot_questionnaire_records.id','=','sot_answers.question_id')
  ->leftJoin('users','users.id','=','sot_answers.userId')
  ->where('users.status','Active')
  ->where('sot_questionnaire_records.type',$value->type)    
  ->where('users.orgId',$orgId);

  if(!empty($officeId))
  {
   $query->where('users.officeId',$officeId);
 }

 if(!empty($departmentId))
 {   


  if($departmentsNew->isEmpty())
  {
    $flag = 1;
  }
  $department3 = array();
  foreach ($departmentsNew as $departments1) 
  {       
    $departments2 = $departments1->id;
    array_push($department3, $departments2);

  }

  $query->whereIn('users.departmentId',$department3);
}            

$SOTCount = $query->first();

$value->SOTCount = ($SOTCount->count)?(string)$SOTCount->count:"0";

if (!empty($SOTCount->count) && max($countArr)==$SOTCount->count)
{
  array_push($sotCountArray1, $value);
}        

}

//detail of culture structure 
$sotStrDetailArr = array();

// reset if department is not in organisation
if($flag==1)
{
  $sotCountArray1 = array();
}

foreach($sotCountArray1 as $sValue)
{
  $sotStrDetail = DB::table('sot_culture_structure_records')->where('id',$sValue->id)->where('status','Active')->first();

  $summary = DB::table('sot_culture_structure_summary_records')->select('summary')->where('type',$sValue->id)->where('status','Active')->get();

  $sValue->summary = $summary;

  array_push($sotStrDetailArr, $sValue);
  break; 
}

$resultArray['sotDetailArray']         = $sotCountArray;
$resultArray['sotSummaryDetailArray']  = $sotStrDetailArr;


return response()->json(['code'=>200,'status'=>true,'service_name'=>'SOT-culture-structure-report','message'=>'','data'=>$resultArray]);

}

//get SOT motivation report
public function getSOTmotivationReport()
{

  $resultArray = array();

  $orgId        = Input::get('orgId');  
  $officeId     = Input::get('officeId');
  $departmentId = Input::get('departmentId');

  $flag = 0 ;
  $departments = [];
  if (!empty($officeId)) 
  {
  //for new department
    $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
  }
  else
  {
    if(!empty($departmentId))
    {
      $departmentsNew = DB::table('departments')
      ->select('departments.id','all_department.department')
      ->leftJoin('all_department','all_department.id','departments.departmentId')
      ->where('departments.orgId',$orgId)
      ->where('departments.departmentId',$departmentId) 
      ->where('departments.status','Active') 
      ->orderBy('all_department.department','ASC')->get();
    }
  }

  $query = DB::table('sot_motivation_answers')
  ->leftjoin('users','users.id','sot_motivation_answers.userId')
  ->select('sot_motivation_answers.userId')
  ->where('users.status','Active')
  ->groupBy('sot_motivation_answers.userId')
  ->where('users.orgId',$orgId);
  
  if(!empty($officeId))
  {
    $query->where('officeId',$officeId);
  }

  if(!empty($departmentId))
  {   
    $i = 0;

    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {       
      if($i == 0)
      {
        $query->where('users.departmentId',$departments1->id);
      }
      else
      {
        $query->orWhere('users.departmentId',$departments1->id);
      }
      $i++;
    }
  } 

  $usersList = $query->get();

  $totalUser = count($usersList);

  // reset if department is not in organisation
  if($flag==1)
  {
    $totalUser = 0;
  }

  if(empty($totalUser))
  {
    return response()->json(['code'=>400,'status'=>true,'service_name'=>'SOT-motivation-report','message'=>'SOT answers are not done by user.','data'=>$resultArray]);
  }

  $categoryTbl = DB::table('sot_motivation_value_records')->where('status','Active')->get();

  foreach ($categoryTbl as $value)
  {  

    $query = DB::table('sot_motivation_answers AS sotans')
    ->leftJoin('sot_motivation_question_options AS qoption', 'qoption.id', '=', 'sotans.optionId')
    ->leftjoin('users','users.id','=','sotans.userId')
    ->where('users.status','Active')                     
    ->where('sotans.orgId',$orgId)
    ->where('sotans.status','Active')
    ->where('qoption.category_id',$value->id);

    if(!empty($officeId))
    {
     $query->where('users.officeId',$officeId);
   }

   if(!empty($departmentId))
   {   


    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    $department3 = array();
    foreach ($departmentsNew as $departments1) 
    {       
      $departments2 = $departments1->id;
      array_push($department3, $departments2);

    }

    $query->whereIn('users.departmentId',$department3);
  } 

  $ansTbl = $query->sum('sotans.answer'); 

  $result['title'] = $value->title;    
  $result['score'] = number_format((float)($ansTbl/$totalUser), 2); 

  array_push($resultArray, $result);
}

return response()->json(['code'=>200,'status'=>true,'service_name'=>'SOT-motivation-report','message'=>'','data'=>$resultArray]);
}


/*get diagnostic reports*/
public function getDiagnosticReportForGraph()
{

  $resultArray = array();

  $orgId        = Input::get('orgId');
  $officeId     = Input::get('officeId');
  $departmentId = Input::get('departmentId');

  $flag = 0 ;
  $departments = [];
  if (!empty($officeId)) 
  {
  //for new department
    $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
  }
  else
  {
    if(!empty($departmentId))
    {
      $departmentsNew = DB::table('departments')
      ->select('departments.id','all_department.department')
      ->leftJoin('all_department','all_department.id','departments.departmentId')
      ->where('departments.orgId',$orgId)
      ->where('departments.departmentId',$departmentId) 
      ->where('departments.status','Active') 
      ->orderBy('all_department.department','ASC')->get();
    }
  }

  $diagnosticQueCatTbl = DB::table('diagnostic_questions_category')->get();

  $query = DB::table('diagnostic_answers')
  ->leftjoin('users','users.id','diagnostic_answers.userId')
  ->select('diagnostic_answers.userId')
  ->where('users.status','Active')
  ->groupBy('diagnostic_answers.userId')
  ->where('users.orgId',$orgId);

  if(!empty($officeId))
  {
    $query->where('users.officeId',$officeId);
  }

  if(!empty($departmentId))
  {   
    $i = 0;

    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {       
      if($i == 0)
      {
        $query->where('users.departmentId',$departments1->id);
      }
      else
      {
        $query->orWhere('users.departmentId',$departments1->id);
      }
      $i++;
    }
  }


  $users     = $query->get();
  $userCount = count($users);

 // reset if department is not in organisation
  if($flag==1)
  {
    $userCount = 0;
  }

  if (empty($userCount))
  {
    return response()->json(['code'=>400,'status'=>true,'service_name'=>'Report-diagnostic-report','message'=>'Diagnostic answers not done yet.','data'=>$resultArray]);
  }

  $optionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
  $diaOptCount = count($optionsTbl)-1; 

  foreach ($diagnosticQueCatTbl as $value)
  {
    $questionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

    $quecount = count($questionTbl);

    $perQuePercen = 0;
    foreach ($questionTbl as  $queValue)
    {
      $diaQuery = DB::table('diagnostic_answers')            
      ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
      ->leftjoin('users','users.id','=','diagnostic_answers.userId')
      ->where('users.status','Active')   
      ->where('diagnostic_answers.orgId',$orgId)
      ->where('diagnostic_questions.id',$queValue->id)
      ->where('diagnostic_questions.category_id',$value->id);

      if (!empty($officeId))
      {
        $diaQuery->where('users.officeId',$officeId);                    
      }

      if(!empty($departmentId))
      {   


        if($departmentsNew->isEmpty())
        {
          $flag = 1;
        }
        $department3 = array();
        foreach ($departmentsNew as $departments1) 
        {       
          $departments2 = $departments1->id;
          array_push($department3, $departments2);

        }

        $diaQuery->whereIn('users.departmentId',$department3);
      }

      $diaAnsTbl = $diaQuery->sum('answer'); 

      $perQuePercen += ($diaAnsTbl/$userCount);       
    }


    $score = ($perQuePercen/($quecount*$diaOptCount));
    $totalPercentage = ($perQuePercen/($quecount*$diaOptCount))*100;

    $value1['title']      =  $value->title;
    $value1['score']      =  number_format((float)$score, 2, '.', '');
    $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');      

    array_push($resultArray, $value1);
  }

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'Report-diagnostic-report','message'=>'','data'=>$resultArray]);

}


/*get diagnostic reports*/
public function getTribeometerReportForGraph()
{

  $resultArray = array();

  $orgId        = Input::get('orgId');
  $officeId     = Input::get('officeId');
  $departmentId = Input::get('departmentId');

  $flag = 0 ;
  $departments = [];
  if (!empty($officeId)) 
  {
     //for new department
    $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
  }
  else
  {
    if(!empty($departmentId))
    {
      $departmentsNew = DB::table('departments')
      ->select('departments.id','all_department.department')
      ->leftJoin('all_department','all_department.id','departments.departmentId')
      ->where('departments.orgId',$orgId)
      ->where('departments.departmentId',$departmentId) 
      ->where('departments.status','Active') 
      ->orderBy('all_department.department','ASC')->get();
    }
  }

  $query = DB::table('tribeometer_answers')
  ->leftjoin('users','users.id','tribeometer_answers.userId')
  ->select('tribeometer_answers.userId')
  ->where('users.status','Active')
  ->groupBy('tribeometer_answers.userId')
  ->where('users.orgId',$orgId);

  if(!empty($officeId))
  {
    $query->where('users.officeId',$officeId);
  }

  if(!empty($departmentId))
  {   
    $i = 0;

    if($departmentsNew->isEmpty())
    {
      $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {       
      if($i == 0)
      {
        $query->where('users.departmentId',$departments1->id);
      }
      else
      {
        $query->orWhere('users.departmentId',$departments1->id);
      }
      $i++;
    }
  }

  $users     = $query->get();
  $userCount = count($users);

   // reset if department is not in organisation
  if($flag==1)
  {
    $userCount = 0;
  }

  if(empty($userCount))
  {
    return response()->json(['code'=>400,'status'=>true,'service_name'=>'Report-tribeometer-report','message'=>'Tribeometer answers not done yet.','data'=>$resultArray]);
  }   

  $optionsTbl = DB::table('tribeometer_question_options')->where('status','Active')->get();
  $optCount   = count($optionsTbl)-1; 

  $queCatTbl    = DB::table('tribeometer_questions_category')->get();
  foreach ($queCatTbl as $value)
  {
    $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();

    $quecount = count($questionTbl);

    $perQuePercen = 0;
    foreach ($questionTbl as  $queValue)
    {
      $triQuery = DB::table('tribeometer_answers')           
      ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
      ->leftjoin('users','users.id','=','tribeometer_answers.userId')
      ->where('users.status','Active') 
      ->where('tribeometer_answers.orgId',$orgId)
      ->where('tribeometer_questions.id',$queValue->id)
      ->where('tribeometer_questions.category_id',$value->id);

      if (!empty($officeId))
      {
        $triQuery->where('users.officeId',$officeId);                    
      }

      if(!empty($departmentId))
      {   


        if($departmentsNew->isEmpty())
        {
          $flag = 1;
        }
        $department3 = array();
        foreach ($departmentsNew as $departments1) 
        {       
          $departments2 = $departments1->id;
          array_push($department3, $departments2);

        }

        $triQuery->whereIn('users.departmentId',$department3);
      }

      $triAnsTbl = $triQuery->sum('answer');

      $perQuePercen += ($triAnsTbl/$userCount);       
    }

    $score           = ($perQuePercen/($quecount*$optCount));
    $totalPercentage = ($perQuePercen/($quecount*$optCount))*100;

    $value1['title']      =  $value->title;
    $value1['score']      =  number_format((float)$score, 2, '.', '');
    $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');      

    array_push($resultArray, $value1);
  }

  return response()->json(['code'=>200,'status'=>true,'service_name'=>'Report-tribeometer-report','message'=>'','data'=>$resultArray]);

}

/*get report pdf url */
public function getReportPdfUrl(Request $request)
{

  // $orgId = $request->orgId;

  // $url = url('api/pdfview').'/'.base64_encode($orgId);
  $resultArray = array();

  $orgId = $request->orgId;

  $request->orgId = base64_encode($orgId);
  $request->pdfStatus = true;
    //get data of DOT

  $dotObj = app('App\Http\Controllers\Admin\AdminReportController')->show($request,base64_encode($orgId)); 
  $org = DB::table('organisations')->where('id',$orgId)->first();
  $dotValuesArray = $dotObj->dotValuesArray;

    //get data of team role map percentage values
  $cotTeamRoleMapObj = app('App\Http\Controllers\Admin\AdminReportController')->getCOTteamRoleMapGraph($request);
  $cotTeamRoleMapGraphPercentage = $cotTeamRoleMapObj['cotTeamRoleMapGraphPercentage']['data'];

    //get cot functional lens
  $cotFunLensObj = app('App\Http\Controllers\Admin\AdminReportController')->getReportFunctionalGraph($request);
  $funcLensPercentageArray = $cotFunLensObj['funcLensPercentageArray'];
  $keyNameArray            = $cotFunLensObj['keyNameArray'];
  

    //get sot culture structure
  $sotCulStrObj = app('App\Http\Controllers\Admin\AdminReportController')->getReportCultureGraph($request);
  $sotCountArray = $sotCulStrObj['sotCountArray'];
  $sotStrDetailArr = $sotCulStrObj['sotStrDetailArr'];

    //get sot motivatin data
  $sotMotObj = app('App\Http\Controllers\Admin\AdminReportController')->getReportMotivationalGraph($request);
  $SOTmotivationResultArray = $sotMotObj['SOTmotivationResultArray'];

    //get diagnostic data
  $diaObj = app('App\Http\Controllers\Admin\AdminReportController')->getReportDiagnosticGraph($request);
  $diagnosticResultArray = $diaObj['diagnosticResultArray'];

    //get tribeometer data
  $triObj = app('App\Http\Controllers\Admin\AdminReportController')->getReportTribeometerGraph($request);
  $tribeometerResultArray = $triObj['tribeometerResultArray'];


    // $getReportPdfUrl1 = view('admin/report/report_graph/pdfReport',compact('org','dotValuesArray','cotTeamRoleMapGraphPercentage','funcLensPercentageArray','sotCountArray','sotStrDetailArr','SOTmotivationResultArray','diagnosticResultArray','tribeometerResultArray'));

//$getReportPdfUrl1 = file_get_contents('http://production.chetaru.co.uk/tribe365/html.html');

  // return view('admin/report/report_graph/pdfReport', compact('org','dotValuesArray','cotTeamRoleMapGraphPercentage','funcLensPercentageArray','sotCountArray','sotStrDetailArr','SOTmotivationResultArray','diagnosticResultArray','tribeometerResultArray'));

  set_time_limit(60 * 5);
  $pdf =SnappyPDF::loadView('admin/report/report_graph/pdfReportApi', compact('org','dotValuesArray','cotTeamRoleMapGraphPercentage','funcLensPercentageArray','keyNameArray','sotCountArray','sotStrDetailArr','SOTmotivationResultArray','diagnosticResultArray','tribeometerResultArray'));

  $pdf->setOption('enable-javascript', true);
  $pdf->setOption('javascript-delay', 5000);
  $pdf->setOption('enable-smart-shrinking', true);
  $pdf->setOption('no-stop-slow-scripts', true);
  $pdf->setOption('page-height', '145');
  $pdf->setOption('page-width', '200');
  $file_name    = 'report'.'_'.time().'.pdf'; 
  $url = url('public/uploads/pdf_report/'.$file_name);
  $pdf->save('public/uploads/pdf_report/'.$file_name);
  

 //$getReportPdfUrl1 ='<!DOCTYPE html><html><head><title>PDF</title></head><body><h1>TEST</h1></body></html>';
  return response()->json(['code'=>200,'status'=>true,'service_name'=>'Report-pdf-url','message'=>'','data'=>$url]);
}



}
