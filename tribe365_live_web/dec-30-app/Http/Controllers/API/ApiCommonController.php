<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Client;
use DB;

class ApiCommonController extends Controller
{


	public function getDepaermentUserList()
	{

		$resultArray = array();

		$userDetail = Auth::user();

		$orgId = Input::get('orgId');

		$users = DB::table('users')
		->where('orgId',$orgId)
		->where('status','Active')
		->where('roleId',3)
		->get();

		$userArray = array();

		$departmentArray = array();

		foreach ($users as $key => $value) {

			$result['id']   = $value->id;
			$result['name'] = $value->name;

			array_push($userArray, $result);
		}
		$officeId = $userDetail->officeId;

		$departments = DB::table('departments')
		->select('departments.id','all_department.department')
		->leftJoin('all_department', 'all_department.id', '=', 'departments.departmentId')   
		->where('departments.orgId',$orgId)
		->where('departments.officeId',$officeId)
		->where('departments.status','Active')
		->get();

		$activeUsers = array();

		//print_r($departments);die();

		foreach ($departments as $deptValue) {

			// $dept['department_id']   = $deptValue->id;
			// $dept['name'] = $deptValue->department;

			// array_push($departmentArray, $dept);

			$checkUsersStatus = DB::table('users')
				->select('departments.id as department_id','all_department.department as name')
				->leftJoin('departments','departments.id','users.departmentId')
				->leftJoin('all_department','departments.departmentId','all_department.id')
				->where('users.departmentId',$deptValue->id)
				->where('users.status','Active')
				->where('departments.status','Active')
				->where('all_department.status','Active')
				->first();
				//print_r($checkUsersStatus);echo "<br>";
			if (count($checkUsersStatus)) {
				//print_r($checkUsersStatus);
				//$dept['department_id']   = $deptValue->id;
				//$dept['name'] = $checkUsersStatus->department;

				//array_push($departmentArray, $dept);

				array_push($activeUsers, $checkUsersStatus);
			}
		}
		// print_r($departmentArray);
		// print_r($activeUsers);
		// die;

		$resultArray['users'] = $userArray;

		$resultArray['departments'] = $activeUsers;


		return response()->json(['code'=>200,'status'=>true,'service_name'=>'department-user-list','message'=>'department-user-list','data'=>$resultArray]);

	}


	public function getOfficeCount($orgId){

		return $office = DB::table('offices')
		->where('status','Active')
		->where('orgId',$orgId)
		->get()
		->count();


	}

	public function getDepartmentCount($orgId){

		$departments = DB::table('departments')
		->select('departmentId')
		->where('status','Active')
		->where('orgId',$orgId)
		->groupBy('departmentId')
		->get();
		return count($departments);

	}

	/*get pagination page count*/
	function getPageCount($count,$limit)
	{

		if ($count <= $limit) {
			return 1;
		} else if ($count > $limit) {
			$page_count = intval($count / $limit);
			if ($count % $limit > 0) {
				$page_count = $page_count + 1;
			}
			return $page_count;
		}
	}
	/*get total record of the list*/
	function getPageCountQuery($table, $whrcls)
	{

		$count = DB::table($table)->where($whrcls)->count();

		return $count;
	}

	/*get all office and departments by organisation*/
	public function getAllOfficenDepartments()
	{
		$resultArray = array();

		$orgId =Input::get('orgId');

		$offices = DB::table('offices')
		->select('id','office')		
		->where('orgId',$orgId)
		->where('status','Active')
		->get();

		$officeArray = array();
		foreach ($offices as $key => $value)
		{
			$departments = DB::table('departments')
			->select('departments.id','all_department.department','departments.officeId')
			->leftJoin('all_department', 'all_department.id', '=', 'departments.departmentId')   
			->where('departments.orgId',$orgId)
			->where('departments.officeId',$value->id)
			->where('departments.status','Active')
			->get();

			$result['officeId']   = $value->id;
			$result['office']     = $value->office;			
			$result['department'] = $departments;
			
			array_push($officeArray, $result);

		}

		$department = DB::table('departments')
		->select('departments.id','all_department.department')
		->leftJoin('all_department', 'all_department.id', '=', 'departments.departmentId')   
		->where('departments.orgId',$orgId)
		->where('departments.status','Active')
		->get();


		$resultArray['offices']     = $officeArray;
		$resultArray['department']  = $department;

		return response()->json(['code'=>200,'status'=>true,'service_name'=>'get-all-office-and-departments','message'=>'All office and departments.','data'=>$resultArray]);

	}


	/*send notification*/
	function sendFcmNotify($fcmToken, $title, $message, $totbadge)
	{

		$fields = array(
			'to' => $fcmToken,
			'priority' => "high",
			'notification' => array("title" => $title, "body" => $message, "badge" => $totbadge, "sound" => "default"),
			'data'=> array("title" => $title, "body" => $message, "badge" => $totbadge, "sound" => "default") 
		);

		$headers = array(
			'https://fcm.googleapis.com/fcm/send',
			'Content-Type: application/json',
			'Authorization: key=AAAASSh9qJM:APA91bGFC09RB3dFkD9u7L-R-hNMA7l4MWinuhfI0HspKKrNcXMdpVyQ-UKEqk71_Je91dEOb4y80Y96siJ6Z5v5sTR0Iq-yUh779ZEOqGDar71tbts9oCw4xggscbBoR0D_jOSPF_xQ'
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

		$result = curl_exec($ch);
		if($result === FALSE)
		{
			die('Problem occurred: ' . curl_error($ch));
		}
		
		curl_close($ch);
		$msg = json_decode($result);		
		return $msg;
	}
}
