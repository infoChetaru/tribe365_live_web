<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

/*FORGOT PASSWORD*/

Route::post('forgotPassword','API\ApiLoginController@forgotPassword');

/*LOGIN FUNCTIONALITY*/

/*for user login process*/
Route::post('userLogin', 'API\ApiLoginController@login');
Route::get('pdfview/{id}','API\ApiReportController@pdfview');	

Route::group(['middleware' => 'auth:api'], function(){

	/*get user profile detail*/
	Route::get('userProfile', 'API\ApiLoginController@details');
	/*for logout*/
	Route::post('userLogout', 'API\ApiLoginController@logout');
	/*update user profile*/
	Route::post('updateUserProfile', 'API\ApiLoginController@updateUserProfile');
	/*update password with current password*/
	Route::post('updatePasswordWithCurrentPassword','API\ApiLoginController@updatePasswordWithCurrentPassword');

	/*DOT FUNCTIONALITY*/

	/*get dot detail*/
	Route::post('dotDetail','API\ApiDotController@getDotDetail');
	/*add new DOT detail*/
	Route::post('addDot','API\ApiDotController@addDot');
	/* update DOT */
	Route::post('updateDot','API\ApiDotController@updateDot');
	/* add Belief DOT */
	Route::post('addBelief','API\ApiDotController@addBelief');
	/* add value DOT */
	Route::post('addValue','API\ApiDotController@addValue');
	/*add evidence to the DOT*/
	Route::post('addDotEvidence','API\ApiDotController@addDotEvidence');
	/*update evidence to the DOT*/
	Route::post('updateDotEvidence','API\ApiDotController@updateDotEvidence');
	/*get belief values list*/
	Route::get('getBeliefValuesList','API\ApiDotController@getBeliefValuesList');
	/*delete DOT value*/
	Route::post('deleteDotValue','API\ApiDotController@deleteDotValue');
	/*get evidence list*/
	Route::post('getEvidenceList','API\ApiDotController@getEvidenceList');
	// update belief
	Route::post('updateBelief','API\ApiDotController@updateBelief');
	// delete belief
	Route::post('deleteBelief','API\ApiDotController@deleteBelief');
	/*add bubble ratings*/
	Route::post('addbubbleRatings','API\ApiDotController@addbubbleRatings');
	/*get bubble rating list*/
	Route::post('getBubbleRatingList','API\ApiDotController@getBubbleRatingList');
	/*get dot from user ratings*/
	Route::post('getBubbleFromUserRating','API\ApiDotController@getBubbleFromUserRating');
	/*get dot report*/
	// Route::post('getDOTreportGraph','API\ApiDotController@getDOTreportGraph');
	Route::post('getDOTValuesList','API\ApiDotController@getDOTValuesList')->name('getDOTValuesList');
	/*add DOT bubble ratings multiple user and multi department*/
	Route::post('addDOTBubbleRatingsToMultiDepartment','API\ApiDotController@addDOTBubbleRatingsToMultiDepartment');
	/*get bubble ratings notification*/
	Route::post('getBubbleRatingNotificationList','API\ApiDotController@getBubbleRatingNotificationList');
	/*get bubble un read msg notification count*/
	Route::post('getBubbleUnReadNotifications','API\ApiDotController@getBubbleUnReadNotifications');
	/*get unread notification list*/
	Route::post('getBubbleRatingUnReadNotificationList','API\ApiDotController@getBubbleRatingUnReadNotificationList'); 
	/*change status of the unread notificatio*/
	Route::post('changeNotificationStatus','API\ApiDotController@changeNotificationStatus');
	/*get dashabord detail*/
	Route::post('getDashboardDetail','API\ApiDotController@getDashboardDetail');
	/*add happy index */
	Route::post('addHappyIndex','API\ApiDotController@addHappyIndex');

	
	

	/*ORGANISATION FUNCTIONALITY*/

	/*add organisation*/
	Route::post('addOrganisation','API\ApiOrganisationController@addOrganisation');
	/*organisation list*/
	Route::get('getOrganisationList','API\ApiOrganisationController@getOrganisationList');

	Route::post('getOrganisationList_test','API\ApiOrganisationController@getOrganisationList_test');
	/*department list*/
	Route::get('getDepartmentList','API\ApiOrganisationController@getDepartmentList');
	/*organisation update*/
	Route::post('updateOrganisation','API\ApiOrganisationController@updateOrganisation');
	/*logo update*/
	Route::post('updateLogo','API\ApiOrganisationController@updateLogo');
	/*add office*/
	Route::post('addOffice','API\ApiOrganisationController@addOffice');
	/*add office users*/
	Route::post('addOfficeUser','API\ApiOrganisationController@addOfficeUser');
	/*get usrs by its type organisation/office/department */
	Route::post('getUserByType','API\ApiOrganisationController@getUserByType');
	/*update staff detail*/
	Route::post('updateStaffDetail','API\ApiOrganisationController@updateStaffDetail');
	/*delete staff*/
	Route::post('deleteStaff','API\ApiOrganisationController@deleteStaff');
	/*get country list*/
	Route::get('getCountryList','API\ApiOrganisationController@getCountryList');

	/*get user list*/
	Route::post('getUserList','API\ApiUserController@getUserList');	

	/*RATING FUNCTIONALITY*/


	/*to give ratings to the dot values*/
	Route::post('ratingsToDotValues','API\ApiRatingsController@ratingsToDotValues');
	
	/*delete evidence to the DOT*/
	Route::delete('deleteDotEvidence/{id}',function ($id) {
		
		DB::table('dot_evidence')->where('id',$id)->delete();
		return response()->json(['code'=>200,'status'=>false,'service_name'=>'delete-evidence','message'=>'Evidence deleted successfully','data'=>'']);
	});


	/*ACTION FUNCTIONALITY*/

	
	/*get actions tiers*/
	Route::get('actionTierList','API\ApiActionController@actionTierList');
	/*add new action */
	Route::post('addAction','API\ApiActionController@addAction');
	/*get actions list*/
	Route::post('getActionList','API\ApiActionController@getActionList');

	Route::post('addComment','API\ApiActionController@addComment');

	Route::post('listComment','API\ApiActionController@listComment');
	
	Route::post('updateStatus','API\ApiActionController@updateStatus');
	/*update action*/
	Route::post('updateAction','API\ApiActionController@updateAction');
	/*delet action*/
	Route::post('deleteAction','API\ApiActionController@deleteAction');
	/*get action count*/
	Route::post('getActionStatusCount','API\ApiActionController@getActionStatusCount');


	/*COMMON FUNCTIONALITY*/

	/*get deaprtments user list*/
	Route::post('getDepartmentUserList','API\ApiCommonController@getDepaermentUserList');	

	/*get office and departments by organisation*/
	Route::post('getAllOfficenDepartments','API\ApiCommonController@getAllOfficenDepartments');

	/*COT FUNCTIONALITY*/
	
	/*get COT questions*/
	Route::get('getCOTQuestions','API\ApiCOTController@getCOTQuestions');
	/*add COT answer*/
	Route::post('addCOTAnswer','API\ApiCOTController@addCOTAnswer');
	/*get COT indivisual summary*/
	Route::get('getCOTindividualSummary','API\ApiCOTController@getCOTindividualSummary');
	/*check cot answers submited by user or not*/
	Route::get('isCOTanswerDone','API\ApiCOTController@isCOTanswerDone');
	/*get team cot summary*/
	Route::post('getCOTteamSummary','API\ApiCOTController@getCOTteamSummary');
	/*get COt maper rank*/
	Route::post('getCOTmaperkey','API\ApiCOTController@getCOTmaperkey');
	/*get COt maper summary*/
	Route::get('getCOTMapperSummary','API\ApiCOTController@getCOTMapperSummary');
	/*get COT Team Role Map Values List*/
	Route::get('getCOTteamRoleMapValues','API\ApiCOTController@getCOTteamRoleMapValues');
	
	/*COT functional lens*/
	/*check questions assign to the user or not*/
	Route::get('isQuestionsDistributedToUser','API\ApiCOTController@isQuestionsDistributedToUser');
	/*send for request of questinare list*/
	Route::get('requestQuestionnaireList','API\ApiCOTController@requestQuestionnaireList');
	/*get value of the COT functional lens */
	Route::post('getCOTFunctionalLensDetail','API\ApiCOTController@getCOTFunctionalLensDetail');
	/*get COT functional lens question list*/
	Route::get('getCOTfunctionalLensQuestionsList','API\ApiCOTController@getCOTfunctionalLensQuestionsList');
	/*add COT Functional lens answers*/
	Route::post('addCOTfunctionalLensAnswer','API\ApiCOTController@addCOTfunctionalLensAnswer');
	/*get cot functional lens report*/
	// Route::post('getReportFunctionalLensGraph','API\ApiCOTController@getReportFunctionalLensGraph');
	/*get cot team role map answers*/
	Route::get('getCOTteamRoleCompletedAnswers','API\ApiCOTController@getCOTteamRoleCompletedAnswers');
	/*update cot team role map answers*/
	Route::post('updateCOTteamRoleMapAnswers','API\ApiCOTController@updateCOTteamRoleMapAnswers');
	/*get cot functional lens completed answes*/
	Route::get('getCOTfuncLensCompletedAnswers','API\ApiCOTController@getCOTfuncLensCompletedAnswers');
	/*update cot functional lens answers*/	
	Route::post('updateCOTfunLensAnswers','API\ApiCOTController@updateCOTfunLensAnswers');



	/*SOT FUNCTIONALITY*/
	/*get sot questinanaire list*/
	Route::get('getSotQuestionList','API\ApiSOTController@getSotQuestionList');
	/*add sot answers*/
	Route::post('addSOTanswers','API\ApiSOTController@addSOTanswers');
	/*get sot detail*/
	Route::post('getSOTdetail','API\ApiSOTController@getSOTdetail');
	/*get questions - answes of user*/
	Route::get('getSOTquestionAnswers','API\ApiSOTController@getSOTquestionAnswers');
	/*update sot answers*/
	Route::post('updateSOTquestionAnswer','API\ApiSOTController@updateSOTquestionAnswer');
	/**/
	Route::get('isSOTmotivationAnswersDone','API\ApiSOTController@isSOTmotivationAnswersDone');
	/*reqest mail sopt motivation*/
	Route::get('requestSOTmotivationScoreMail','API\ApiSOTController@requestSOTmotivationScoreMail');
	/*SOT-get Motivation user list*/
	Route::post('getSOTmotivationUserList','API\ApiSOTController@getSOTmotivationUserList');
	/*get SOT Motivation questions list*/
	Route::get('getSOTmotivationQuestions','API\ApiSOTController@getSOTmotivationQuestions');
	//add sot motivation answers
	Route::post('addSOTmotivationAnswer','API\ApiSOTController@addSOTmotivationAnswer');
	/*get sot motivation completed answers*/
	Route::get('getSOTmotivationCompletedAnswer','API\ApiSOTController@getSOTmotivationCompletedAnswer');
	/*update sot motivation answers*/
	Route::post('updateSOTmotivationAnswers','API\ApiSOTController@updateSOTmotivationAnswers');

	

	/*DIAGNOSTIC FUNCTIONALITY*/
	/*get diagnostic questions*/
	Route::get('getDiagnosticQuestionList','API\ApiDiagnosticController@getDiagnosticQuestionList');
	/*add diagnostic answers*/
	Route::post('addDiagnosticAnswers','API\ApiDiagnosticController@addDiagnosticAnswers');
	/*check tribeometer and diagnostic answers done or not*/
	Route::get('isDiagnosticTribeometerAnswerDone','API\ApiDiagnosticController@isDiagnosticTribeometerAnswerDone');
	/*get diagnostic completed answers*/
	Route::get('getDiagnosticCompletedAnswers','API\ApiDiagnosticController@getDiagnosticCompletedAnswers');
	/*update diagnostic answers*/
	Route::post('updateDiagnosticAnswers','API\ApiDiagnosticController@updateDiagnosticAnswers');
	/*REPORT*/
	Route::post('getDiagnosticReport','API\ApiDiagnosticController@getDiagnosticReport');


	/*TRIBEOMETER FUNCTIONALITY*/
	/*get diagnostic questions*/
	Route::get('getTribeometerQuestionList','API\ApiTribeometerController@getTribeometerQuestionList');
	/*add TRIBEOMETER answers*/
	Route::post('addTribeometerAnswers','API\ApiTribeometerController@addTribeometerAnswers');
	/*get tribeometer completed answers*/
	Route::get('getTribeometerCompletedAnswers','API\ApiTribeometerController@getTribeometerCompletedAnswers');
	/*update TRIBEOMETER answers*/
	Route::post('updateTribeometerAnswers','API\ApiTribeometerController@updateTribeometerAnswers');
	/*REPORT*/
	Route::post('getTribeometerReport','API\ApiTribeometerController@getTribeometerReport');


	/*REPORT FUNCTIONALITY*/
	/*Get DOT reports*/
	Route::post('getDOTreportGraph','API\ApiReportController@getDOTreportGraph');
	/*get COT functional lens report*/
	Route::post('getReportFunctionalLensGraph','API\ApiReportController@getReportFunctionalLensGraph');
	/*get COT Team Role Map report*/
	Route::post('getCOTteamRoleMapReport','API\ApiReportController@getCOTteamRoleMapReport');
	/*get SOT culture structure report*/
	Route::post('getSOTcultureStructureReport','API\ApiReportController@getSOTcultureStructureReport');
	/*get sot motivation report*/
	Route::post('getSOTmotivationReport','API\ApiReportController@getSOTmotivationReport');
	/*get diagnostic report*/
	Route::post('getDiagnosticReportForGraph','API\ApiReportController@getDiagnosticReportForGraph');
	/*get tribeometer reports*/
	Route::post('getTribeometerReportForGraph','API\ApiReportController@getTribeometerReportForGraph');
	/*get report pdf url */
	Route::post('getReportPdfUrl','API\ApiReportController@getReportPdfUrl');	
	Route::post('downloadPdf','API\ApiReportController@downloadPdf');	
	/*get diagnostic dub graph data*/
	Route::post('getDiagnsticReportSubGraph','API\ApiReportController@getDiagnsticReportSubGraph');
	/*get org report by filter api*/
	Route::post('getOrgDashboardReportWithFilter','API\ApiReportController@getOrgDashboardReportWithFilter');
	/*get user dashboard report*/
	Route::post('getUserDashboardReport','API\ApiReportController@getUserDashboardReport');
	/*get Happy Index Count by month*/
	Route::post('getHappyIndexMonthCount','API\ApiReportController@getHappyIndexMonthCount');
	/*get Happy Index Count by weeks*/
	Route::post('getHappyIndexWeeksCount','API\ApiReportController@getHappyIndexWeeksCount');
	/*get Happy Index Count by days*/
	Route::post('getHappyIndexDaysCount','API\ApiReportController@getHappyIndexDaysCount');


	/*CHECKLIST FUNCTIONALITY*/
	
	/*get status of answers*/
	Route::post('getAllAnswersStatus','API\ApiChecklistController@getAllAnswersStatus');	

	/*PERFORMANCE FUNCTIONALITY*/

	/*get performance detail*/
	Route::post('getPerformance','API\ApiPerformanceController@getPerformance');



	/*IOT FUNCTIONALITY*/
	
	/*send msg */
	Route::post('iotSendMsg','API\ApiIOTController@iotSendMsg');
	/*post feedback*/
	Route::post('postFeedback','API\ApiIOTController@postFeedback');
	/*get feedback detail*/
	Route::post('getFeedbackDetail','API\ApiIOTController@getFeedbackDetail');
	/*get chat msgs*/
	Route::post('getChatMessages','API\ApiIOTController@getChatMessages');
	/*get inbox chat list*/
	Route::post('getInboxChatList','API\ApiIOTController@getInboxChatList');
	/*get theme list*/
	Route::post('getThemeList','API\ApiIOTController@getThemeList');


});


