<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Validator;
use Hash;
use Illuminate\Support\Facades\Input;
use Config;
use View;
use Dompdf\Dompdf;
use Dompdf\Options;
use Carbon\Carbon;
use DateTime;
use Mail;

class AdminReportController extends Controller
{

  public function __construct(Request $request)
  {
    $orgid = base64_decode($request->segment(3));

    $organisations = DB::table('organisations')
    ->select('ImageURL')
    ->where('id',$orgid)
    ->first();

    if(!empty($organisations->ImageURL))
    {
      $org_detail = base64_encode($orgid)."-".$organisations->ImageURL;

      View::share('org_detail', $org_detail);
  }else {

      $org_detail = "";

      View::share('org_detail', $org_detail);
  }

}

/*get DOT reports*/
public function getDOTreports(Request $request,$id)
{
    $orgId = base64_decode($id);

    $dots = DB::table('dots')->where('orgId',$orgId)->first();

    if (empty($dots))
    {
        return redirect()->back()->with('error',"Reports not available.");
    }

    $organisations = DB::table('organisations')->where('id',$orgId)->first();

    $dotValuesArray = array();
    $beliefList = array();

    if(!empty($dots))
    {
             //for search
        $searchBeliefId = $request->beliefId;
        session()->put('searchBeliefId', $searchBeliefId);
        $beliefWr = array();
        if($searchBeliefId)
        {
            $beliefWr['id'] = $searchBeliefId;
        }

        $dotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->where($beliefWr)
        ->orderBy('id','ASC')
        ->get();

        $beliefList = DB::table('dots_beliefs')->where('status','Active')->where('dotId',$dots->id)->get();

        foreach ($dotBeliefs as $key => $bValue)
        {

            $dotValues = DB::table('dots_values')
            ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
            ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
            ->where('dots_values.status','Active')
            ->where('dots_values.beliefId',$bValue->id)
            ->orderBy('dot_value_list.id','ASC')
            ->get();

            $bRatings = DB::table('dot_values_ratings')
            ->leftjoin('users','users.id','=','dot_values_ratings.userId')
            ->where('users.status','Active')
            ->where('beliefId', $bValue->id)->avg('ratings');

            $valuesArray = array();
            foreach ($dotValues as $key => $vValue) 
            {
                $vRatings = DB::table('dot_values_ratings')
                ->leftjoin('users','users.id','=','dot_values_ratings.userId')
                ->where('users.status','Active')
                ->where('valueId', $vValue->id)->avg('ratings');

                $vResult['valueId']      = $vValue->id;
                $vResult['valueName']    = ucfirst($vValue->name);

                $vResult['valueRatings'] = 0;
                if($vRatings)
                {
                    $vResult['valueRatings'] = $vRatings-1;
                }
                array_push($valuesArray, $vResult);
            }

            $result['beliefId']      = $bValue->id;
            $result['beliefName']    = ucfirst($bValue->name);           

            $result['beliefRatings'] = 0;
            if($bRatings)
            {
                $result['beliefRatings'] =$bRatings-1;
            }

            $result['beliefValues']  = $valuesArray;

            array_push($dotValuesArray, $result);
        }
    }

        //show image of organisation
    if(!empty($organisations->ImageURL))
    {
        $org_detail = base64_encode($orgId)."-".$organisations->ImageURL;
        View::share('org_detail', $org_detail);
    }


    return view('admin/report/detailRatingReport',compact('dotValuesArray','orgId','beliefList','organisations'));
}


/*get list of average rating of belief and rating*/
public function getBeliefValueRatingList(Request $request, $id)
{

    $orgId = base64_decode($id);

    $dots = DB::table('dots')->where('orgId',$orgId)->first();

    if (empty($dots))
    {
        return redirect()->back()->with('error',"Reports not available.");
    }

    $organisations = DB::table('organisations')->where('id',$orgId)->first();

    $beliefList = DB::table('dots_beliefs')->where('status','Active')->where('dotId',$dots->id)->get();

    $dotValuesArray = array();

    if(!empty($dots))
    {
         //for search
        $searchBeliefId = $request->beliefId;
        session()->put('searchBeliefId', $searchBeliefId);
        $beliefWr = array();
        if($searchBeliefId)
        {
            $beliefWr['id'] = $searchBeliefId;
        }

        $dotBeliefs = DB::table('dots_beliefs')->where('status','Active')->where('dotId',$dots->id)
        ->where($beliefWr)->orderBy('id','ASC')->get();

        foreach($dotBeliefs as $key => $bValue)
        {

            $dotValues = DB::table('dots_values')
            ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
            ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
            ->where('dots_values.status','Active')
            ->where('dots_values.beliefId',$bValue->id)
            ->orderBy('dot_value_list.id','ASC')->get();

            $bRatings = DB::table('dot_values_ratings')
                ->leftjoin('users','users.id','=','dot_values_ratings.userId')
                ->where('dot_values_ratings.beliefId', $bValue->id)
                ->where('users.status','Active')
                ->avg('ratings');

            $valuesArray = array();
            foreach ($dotValues as $key => $vValue) 
            {
                $vRatings = DB::table('dot_values_ratings')
                ->leftjoin('users','users.id','=','dot_values_ratings.userId')
                ->where('users.status','Active')
                ->where('valueId', $vValue->id)
                ->where('beliefId',$bValue->id)->avg('ratings');

                $vResult['valueId']      = $vValue->id;
                $vResult['valueName']    = ucfirst($vValue->name);

                $vResult['valueRatings'] = 0;
                if($vRatings)
                {
                    $vResult['valueRatings'] = $vRatings-1;
                }
                array_push($valuesArray, $vResult);
            }

            $result['beliefId']      = $bValue->id;
            $result['beliefName']    = ucfirst($bValue->name);           

            $result['beliefRatings'] = 0;
            if($bRatings)
            {
                $result['beliefRatings'] = $bRatings-1;
            }

            $result['beliefValues']  = $valuesArray;

            array_push($dotValuesArray, $result);
        }
    }
         //show image of organisation
    if(!empty($organisations->ImageURL))
    {
        $org_detail = base64_encode($orgId)."-".$organisations->ImageURL;
        View::share('org_detail', $org_detail);
    }

    return view('admin/report/listBeliefValueRatingReport',compact('dotValuesArray','organisations','beliefList'));

}

/*get individual rating of values*/
public function getIndividualBeliefValueRatingList(Request $request,$id)
{

    $beliefId = base64_decode($id);

    $beliefs = DB::table('dots_beliefs')
    ->select('organisations.ImageURL','organisations.organisation','dots_beliefs.id','organisations.id AS orgId')
    ->leftjoin('dots','dots.id','dots_beliefs.dotId')
    ->leftjoin('organisations','organisations.id','dots.orgId')
    ->where('dots_beliefs.id',$beliefId)->first();

    if (empty($beliefs))
    {
        return redirect()->back()->with('error',"Reports not available.");
    }

    //search
    $valueWr =array();
    $customValueId = $request->custValueId;
    session()->put('customValueId', $customValueId);
    if ($customValueId) {
        $valueWr['dots_values.name'] = $customValueId;
    }

    $dotValues = DB::table('dots_values')   
    ->select('dots_values.id','dot_value_list.name')
    ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
    ->where('dots_values.beliefId',$beliefId)
    ->where($valueWr)
    ->where('dots_values.status','Active')
    ->get();

    $custValueList = DB::table('dot_value_list')->where('status','Active')->get();


    $valueRatingArray = array();
    foreach ($dotValues as $key => $bValue)
    {

        $ratings = DB::table('dot_values_ratings')
        ->select('users.name','dot_values_ratings.ratings','dot_values_ratings.id','dot_values_ratings.created_at')
        ->leftJoin('users','users.id','dot_values_ratings.userId')
        ->where('valueId',$bValue->id)->get();

        $vRatings = DB::table('dot_values_ratings')->where('valueId', $bValue->id)->avg('ratings');

        $bValue->valueRating = 0;
        if($vRatings)
        {
            $bValue->valueRating = $vRatings-1;
        }

        $bValue->ratingArray = $ratings;

        array_push($valueRatingArray, $bValue);
    }

        //show image of organisation
    $organisations = DB::table('organisations')->where('id',$beliefs->orgId)->first();
    if(!empty($organisations->ImageURL))
    {
        $org_detail = base64_encode($beliefs->orgId)."-".$organisations->ImageURL;
        View::share('org_detail', $org_detail);
    }

    return view('admin/report/listIndividualBeliefValueRatingReport',compact('valueRatingArray','beliefs','custValueList'));
}

/*get diagnostic reports organisations list*/
public function getDiagOrgList()
{
    $organisation = DB::table('organisations')->where('status','Active')->orderBy('id','DESC')->paginate(12);
    return View('admin/report/listDiagnosticOrganisation',compact('organisation'));
}
/*get diagnostic resport graph*/
public function getDiagGraph(Request $request)
{

    $resultArray = array();

    $orgId = base64_decode($request->id);

    $diagnosticQueCatTbl = DB::table('diagnostic_questions_category')->get();

    $orgName = DB::table('organisations')->select('organisation')->where('id',$orgId)->first();

        //get user list
    $users = DB::table('diagnostic_answers')->select('userId')->groupBy('userId')->where('orgId',$orgId)->get();
    $userCount = count($users);

    if (empty($userCount))
    {
        return redirect()->back()->with('error',"Diagnostic answers are not done yet.");
    }

    $optionsTbl= DB::table('diagnostic_question_options')->where('status','Active')->get();
    $diaOptCount = count($optionsTbl)-1; 

    foreach ($diagnosticQueCatTbl as $value)
    {
        $questionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

        $quecount = count($questionTbl);

        $perQuePercen = 0;
        foreach ($questionTbl as  $queValue)
        {
            $diaAnsTbl = DB::table('diagnostic_answers')            
            ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
            ->where('diagnostic_answers.orgId',$orgId)
            ->where('diagnostic_questions.id',$queValue->id)
            ->where('diagnostic_questions.category_id',$value->id)
            ->sum('answer');                
            $perQuePercen += ($diaAnsTbl/$userCount);               
        }


        $score = ($perQuePercen/($quecount*$diaOptCount));
        $totalPercentage = ($perQuePercen/($quecount*$diaOptCount))*100;

        $value1['title']      =  $value->title;
        $value1['score']      =  number_format((float)$score, 2, '.', '');
        $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');            

        array_push($resultArray, $value1);
    }

    return View('admin/report/detailDiagnosticReport',compact('resultArray','orgName'));
}

/*get tribeometer reports organisations list*/
public function getTribeoOrgList()
{
    $organisation = DB::table('organisations')->where('status','Active')->orderBy('id','DESC')->paginate(12);
    return View('admin/report/listTribeometerOrganisation',compact('organisation'));
}

/*get tribeometer resport graph*/
public function getTribeoGraph(Request $request)
{

    $resultArray = array();

    $orgId = base64_decode($request->id);

    $orgName  = DB::table('organisations')->select('organisation')->where('id',$orgId)->first();

    $queCatTbl = DB::table('tribeometer_questions_category')->get();

        //get user list
    $users = DB::table('tribeometer_answers')->select('userId')->groupBy('userId')->where('orgId',$orgId)->get();
    $userCount = count($users);

    if(empty($userCount))
    {
        return redirect()->back()->with('error',"Tribeometer answers are not done yet.");
    }       


    $optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
    $optCount = count($optionsTbl)-1; 

    foreach ($queCatTbl as $value)
    {
        $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();

        $quecount = count($questionTbl);

        $perQuePercen = 0;
        foreach ($questionTbl as  $queValue)
        {
            $diaAnsTbl = DB::table('tribeometer_answers')           
            ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
            ->where('tribeometer_answers.orgId',$orgId)
            ->where('tribeometer_questions.id',$queValue->id)
            ->where('tribeometer_questions.category_id',$value->id)
            ->sum('answer');

                //avg of all questions
            $perQuePercen += ($diaAnsTbl/$userCount);               
        }

        $score = ($perQuePercen/($quecount*$optCount));
        $totalPercentage = ($perQuePercen/($quecount*$optCount))*100;

        $value1['title']      =  $value->title;
        $value1['score']      =  number_format((float)$score, 2, '.', '');
        $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');            

        array_push($resultArray, $value1);
    }
    return View('admin/report/detailTribeometerReport',compact('resultArray','orgName'));
}


/*get cot functional lens detail*/
public function getCOTFunctionalLensGraph(Request $request)
{

   $orgId = base64_decode($request->orgId);

   $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get();

   $officeId     = $request->officeId;
   $departmentId = $request->departmentId;

   $customDept = DB::table('departments')
   ->select('departments.id','all_department.department')
   ->leftJoin('all_department','all_department.id','departments.departmentId')
   ->where('departments.officeId',$officeId)->get();

   session()->put('officeId', $officeId);
   session()->put('departmentId', $departmentId);
   session()->put('customDept',$customDept);

   $query = DB::table('users')
   ->select('users.id','users.name','offices.office','all_department.department')
   ->leftJoin('offices', 'offices.id', '=', 'users.officeId')   
   ->leftJoin('departments', 'departments.id', '=', 'users.departmentId')
   ->leftJoin('all_department','all_department.id', '=', 'departments.departmentId')
   ->where('users.orgId',$orgId)
   ->where('users.roleId',3)
   ->where('users.status','Active')
   ->orderBy('users.id','DESC');

   if($officeId)
   {
    $query->where('users.officeId', $officeId);
}

if($departmentId)
{
    $query->where('users.departmentId', $departmentId);
}

$userTbl = $query->paginate(10);

$variable = array('EI','SN','TF','JP');

$cotFuncUserArray = array();

foreach($userTbl as $userValue)
{

    $scoreArray   = array();

    foreach ($variable as $value)
    {

    $value1 = substr($value, -2,1);//get first char
    $value2 = substr($value, 1);//get last char

    $countE = DB::table('cot_functional_lens_answers AS cfla')
    ->select('cflqo.option_name AS optionName')
    ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
    ->where('cfla.userId',$userValue->id)->where('cflqo.initial',$value1)->get();

    $countI = DB::table('cot_functional_lens_answers AS cfla')
    ->select('cflqo.option_name AS optionName')
    ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
    ->where('cfla.userId',$userValue->id)->where('cflqo.initial',$value2)->get();

    if(count($countE) > count($countI))
    {
      $countValEI = count($countE)-count($countI);
      $initialValEI  =  $value1;
  }
  elseif (count($countE) < count($countI))
  {
      $countValEI = count($countI)-count($countE);
      $initialValEI  =  $value2;

  }elseif(count($countE) == count($countI))
  {
      $countValEI    = count($countE)-count($countI);
      $initialValEI  =  $value1;
  }
  else
  {
      $countValEI    = '-';
      $initialValEI  = '-';
  }

  array_push($scoreArray, array('key'=>$initialValEI,'score'=>$countValEI));

}

$userValue->scoreArray   = $scoreArray;

array_push($cotFuncUserArray, $userValue);

}

return view('admin/report/report_graph/cotFunctionalLensUserListGraph',compact('cotFuncUserArray','offices','orgId'));

}

/*get sot motivation data for graph*/
public function getSOTmotivationGraph(Request $request)
{

    $sotMotivationUserArray = array();

    $orgId = base64_decode($request->orgId);
    
    $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get();

    $officeId     = $request->officeId;
    $departmentId = $request->departmentId;

    $customDept1 = DB::table('departments')
    ->select('departments.id','all_department.department')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('departments.officeId',$officeId)->get();

    $customDept = array();
    foreach ($customDept1 as $value)
    {
      $users = DB::table('users')->where('status','Active')->where('departmentId',$value->id)->first();

      if($users)
      {
        array_push($customDept, $value);
    }
}

session()->put('officeId', $officeId);
session()->put('departmentId', $departmentId);
session()->put('customDept', $customDept);

$query = DB::table('users')
->select('users.id AS userId', 'users.name', 'offices.office', 'all_department.department')
->leftJoin('offices', 'offices.id', '=', 'users.officeId')   
->leftJoin('departments', 'departments.id', '=', 'users.departmentId')
->leftJoin('all_department','all_department.id', '=', 'departments.departmentId') 
->where('users.orgId',$orgId)
->where('users.roleId',3)
->where('users.status','Active')
->orderBy('users.id','DESC');

if($officeId) 
{ 
  $query->where('users.officeId',$officeId);
}
if($departmentId)
{ 
  $query->where('users.departmentId',$departmentId);
}

$userTbl = $query->get();

$userArray = array();
foreach ($userTbl as $key => $userValue)
{

  $categoryTbl = DB::table('sot_motivation_value_records')->where('status','Active')->get();

  $catArray = array();
  foreach ($categoryTbl as $value)
  {  

    $ansTbl = DB::table('sot_motivation_answers AS sotans')         

    ->leftJoin('sot_motivation_question_options AS qoption', 'qoption.id', '=', 'sotans.optionId')
    ->where('sotans.userId',$userValue->userId)
    ->where('sotans.orgId',$orgId)
    ->where('sotans.status','Active')
    ->where('qoption.category_id',$value->id)
    ->sum('sotans.answer');

    $result['title'] = $value->title;
    $result['score'] = $ansTbl;

    array_push($catArray, $result);

}

$userValue->sotMotivationValues = $catArray;

array_push($sotMotivationUserArray, $userValue);

}

return view('admin/report/report_graph/sotMotivationUserListGraph',compact('sotMotivationUserArray','offices','orgId'));

}

//get all reports organisation
public function index()
{
    $organisation = DB::table('organisations')->where('status','Active')->orderBy('id','DESC')->paginate(12);
    return View('admin/report/listOrganisation',compact('organisation'));
}

/*public function show(Request $request,$id)
{
    $orgId    = base64_decode($id);
    $officeId = $request->officeId;

    $org = DB::table('organisations')->select('id','organisation','ImageURL')->where('id',$orgId)->first();

    $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get();

    $dots = DB::table('dots')->where('orgId',$orgId)->first();

    //DOT report
   $dotValuesArray = array();
    $beliefList     = array();

    if(!empty($dots))
    {
        //for search
        $searchBeliefId = $request->beliefId;
        session()->put('searchBeliefId', $searchBeliefId);
        $beliefWr = array();
        if($searchBeliefId)
        {
            $beliefWr['id'] = $searchBeliefId;
        }

        $dotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->where($beliefWr)
        ->orderBy('id','ASC')
        ->get();

        $beliefList = DB::table('dots_beliefs')->where('status','Active')->where('dotId',$dots->id)->get();

        foreach ($dotBeliefs as $key => $bValue)
        {

            $dotValues = DB::table('dots_values')
            ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
            ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
            ->where('dots_values.status','Active')
            ->where('dots_values.beliefId',$bValue->id)
            ->orderBy('dot_value_list.id','ASC')
            ->get();

            $bRatings = DB::table('dot_values_ratings')
            ->leftjoin('users','users.id','=','dot_values_ratings.userId')
            ->where('users.status','Active') 
            ->where('beliefId', $bValue->id)->avg('ratings');

            $valuesArray = array();
            foreach ($dotValues as $key => $vValue) 
            {
                $vRatings = DB::table('dot_values_ratings')
                ->leftjoin('users','users.id','=','dot_values_ratings.userId')
                ->where('users.status','Active')
                ->where('valueId', $vValue->id)->avg('ratings');

                $vResult['valueId']      = $vValue->id;
                $vResult['valueName']    = ucfirst($vValue->name);

                $vResult['valueRatings'] = 0;
                if($vRatings)
                {
                    $vResult['valueRatings'] = $vRatings-1;
                }
                array_push($valuesArray, $vResult);
            }

            $result['beliefId']      = $bValue->id;
            $result['beliefName']    = ucfirst($bValue->name);           

            $result['beliefRatings'] = 0;
            if($bRatings)
            {
                $result['beliefRatings'] =$bRatings-1;
            }

            $result['beliefValues']  = $valuesArray;

            //filter by office 
            if(!empty($officeId)) 
            {

                $bRatingsQuery = DB::table('dot_values_ratings')
                ->leftjoin('users','users.id','=','dot_values_ratings.userId')
                ->where('users.status','Active')
                ->where('dot_values_ratings.status','Active')
                ->where('dot_values_ratings.beliefId', $bValue->id);
                $bRatingsQuery->where('users.officeId', $officeId);

                $isofficeRating = $bRatingsQuery->avg('ratings');

                if($isofficeRating)
                {
                    array_push($dotValuesArray, $result);
                }
            }
            else
            {
                array_push($dotValuesArray, $result);
            }  
        }
    }

     //for pdf
    if (empty($resquest->pdfStatus))
    {
        return View('admin/report/manageReport',compact('offices','beliefList','dotValuesArray','org','officeId'));
    }else{

        return compact('dotValuesArray','org','officeId');
    }
}
*/

// public function show(Request $request,$id)
// {

//     $orgId     = base64_decode($id);
//     $officeId  = $request->officeId;
    
//     $org = DB::table('organisations')->select('id','organisation','ImageURL')->where('id',$orgId)->first();

//     $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get();

//     $dots = DB::table('dots')->where('orgId',$orgId)->first();

//     /*DOT report*/
//     $dotValuesArray = array();
//     $beliefList     = array();

//     if(!empty($dots))
//     {
//         //for search
//         $searchBeliefId = $request->beliefId;
//         session()->put('searchBeliefId', $searchBeliefId);
//         $beliefWr = array();
//         if($searchBeliefId)
//         {
//             $beliefWr['id'] = $searchBeliefId;
//         }

//         $dotBeliefs = DB::table('dots_beliefs')
//         ->where('status','Active')
//         ->where('dotId',$dots->id)
//         ->where($beliefWr)
//         ->orderBy('id','ASC')
//         ->get();

//         $beliefList = DB::table('dots_beliefs')->where('status','Active')->where('dotId',$dots->id)->get();

//         $startDate = Input::get('startDate');
//         $endDate   = Input::get('endDate');

//         foreach ($dotBeliefs as $key => $bValue)
//         {

//             $dotValues = DB::table('dots_values')
//             ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
//             ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
//             ->where('dots_values.status','Active')
//             ->where('dots_values.beliefId',$bValue->id)           
//             ->orderBy('dot_value_list.name','ASC')
//             ->get();

//             $bquery = DB::table('dot_values_ratings')
//             ->leftjoin('users','users.id','=','dot_values_ratings.userId')
//             ->where('dot_values_ratings.beliefId',$bValue->id)
//             ->where('users.status','Active');
            
//             $startDate = Input::get('startDate');
//             $endDate   = Input::get('endDate');
//             //filter acc. to start and end date
//             if(!empty($startDate) && !empty($endDate)) 
//             {

//               $startDate = date_create($startDate);
//               $startDateMonth = date_format($startDate,"Y-m-d");
//               $endDate = date_create($endDate);

//               $endDateYear = date_format($endDate,"Y-m-d");

//               $bquery->where('users.created_at', '>=',$startDateMonth);
//               $bquery->where('users.created_at', '<=',$endDateYear);

//           }
//             //belief rating
//           $bRatings = $bquery->avg('ratings');
          
//           $valuesArray = array();
//           foreach ($dotValues as $key => $vValue) 
//           {
//             $query = DB::table('dot_values_ratings')
//             ->leftjoin('users','users.id','=','dot_values_ratings.userId')
//             ->where('users.status','Active')
//             ->where('valueId', $vValue->id)
//             ->where('beliefId',$bValue->id)
//             ->where('dotId', $dots->id);

//             $startDate = Input::get('startDate');
//             $endDate   = Input::get('endDate');

//             //filter acc. to start and end date
//             if(!empty($startDate) && !empty($endDate)) 
//             {

//               $startDate = date_create($startDate);
//               $startDateMonth = date_format($startDate,"Y-m-d");

//               $endDate = date_create($endDate);

//               $endDateYear = date_format($endDate,"Y-m-d");

//               $query->where('users.created_at', '>=',$startDateMonth);
//               $query->where('users.created_at', '<=',$endDateYear);
//           }
          
//           /*print_r($query->toSql());
//           print_r($query->getBindings());
//           die();*/
//           $vRatings = $query->avg('ratings');

//          /* echo " V ";
//          print_r($vRatings);*/

//          $vResult['valueId']      = $vValue->id;
//          $vResult['valueName']    = ucfirst($vValue->name);

//          $vResult['valueRatings'] = 0;
//          if($vRatings)
//          {
//             $vResult['valueRatings'] = $vRatings-1;
//         }
//         array_push($valuesArray, $vResult);
//     }
//     // die();

//     $result['beliefId']      = $bValue->id;
//     $result['beliefName']    = ucfirst($bValue->name);           

//     $result['beliefRatings'] = 0;
//     if($bRatings)
//     {
//         $result['beliefRatings'] = $bRatings-1;
//     }

//     $result['beliefValues']  = $valuesArray;

//             //filter by office 
//     if(!empty($officeId)) 
//     {

//         $bRatingsQuery = DB::table('dot_values_ratings')
//         ->leftjoin('users','users.id','=','dot_values_ratings.userId')
//         ->where('users.status','Active')
//         ->where('dot_values_ratings.status','Active')
//         ->where('dot_values_ratings.beliefId', $bValue->id);
//         $bRatingsQuery->where('users.officeId', $officeId);

//         $startDate = Input::get('startDate');
//         $endDate   = Input::get('endDate');

//                 //filter acc. to start and end date
//         if(!empty($startDate) && !empty($endDate)) 
//         {

//           $startDate = date_create($startDate);
//           $startDateMonth = date_format($startDate,"Y-m-d");

//           $endDate = date_create($endDate);

//           $endDateYear = date_format($endDate,"Y-m-d");

//           $bRatingsQuery->where('users.created_at', '>=',$startDateMonth);
//           $bRatingsQuery->where('users.created_at', '<=',$endDateYear);

//       }

//       $isofficeRating = $bRatingsQuery->avg('ratings');

//       if($isofficeRating)
//       {
//         array_push($dotValuesArray, $result);
//     }
// }
// else
// {
//     array_push($dotValuesArray, $result);
// }

// }
//       //belief for loop

// $startDate = Input::get('startDate');
// $endDate   = Input::get('endDate');

//             //filter acc. to start and end date
// if(!empty($startDate) && !empty($endDate)) 
// {
//     $startDate = date_create($startDate);
//     $startDateMonth = date_format($startDate,"Y-m-d");            
//     $endDate = date_create($endDate);
//     $endDateYear = date_format($endDate,"Y-m-d");

//     $isUser = DB::table('users')
//     ->where('orgId',$orgId)
//     ->where('status','Active')
//     ->where('created_at', '>=', $startDateMonth)           
//     ->where('created_at', '<=', $endDateYear)
//     ->count();

//     if(empty($isUser))
//     {
//         $dotValuesArray = array();
//     }
// }


// }

//      //for pdf
// if (empty($resquest->pdfStatus))
// {
//     return View('admin/report/manageReport',compact('offices','beliefList','dotValuesArray','org','officeId'));
// }else{

//     return compact('dotValuesArray','org','officeId');
// }
// }

// public function show(Request $request,$id)
// {

//     $orgId     = base64_decode($id);
//     $officeId  = $request->officeId;
//     $departmentId = $request->departmentId;

//     // print_r($officeId);
//     // print_r($departmentId);
//     // die();
    
//     $org = DB::table('organisations')->select('id','organisation','ImageURL')->where('id',$orgId)->first();

//     $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get();

//     $dots = DB::table('dots')->where('orgId',$orgId)->first();

//     $all_department = DB::table('all_department')->where('status','Active')->get();

//     $departments = [];

//     if (!empty($officeId)) 
//     {
//         $departments = DB::table('departments')
//         ->select('departments.id','all_department.department')
//         ->leftJoin('all_department','all_department.id','departments.departmentId')
//         ->where('departments.officeId',$officeId) 
//         ->where('departments.status','Active') 
//         ->orderBy('all_department.department','ASC')->get();

//         //for new department
//         $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
//     }
//     else
//     {
//         if(!empty($departmentId))
//         {
//             $departmentsNew = DB::table('departments')
//             ->select('departments.id','all_department.department')
//             ->leftJoin('all_department','all_department.id','departments.departmentId')
//             ->where('departments.orgId',$orgId)
//             ->where('departments.departmentId',$departmentId) 
//             ->where('departments.status','Active') 
//             ->orderBy('all_department.department','ASC')->get();
//         }
//     }

//     /*DOT report*/
//     $dotValuesArray = array();
//     $beliefList     = array();

//     if(!empty($dots))
//     {
//         //for search
//         $searchBeliefId = $request->beliefId;
//         session()->put('searchBeliefId', $searchBeliefId);
//         $beliefWr = array();
//         if($searchBeliefId)
//         {
//             $beliefWr['id'] = $searchBeliefId;
//         }

//         $dotBeliefs = DB::table('dots_beliefs')
//         ->where('status','Active')
//         ->where('dotId',$dots->id)
//         ->where($beliefWr)
//         ->orderBy('id','ASC')
//         ->get();

//         $beliefList = DB::table('dots_beliefs')->where('status','Active')->where('dotId',$dots->id)->get();

//         $startDate = Input::get('startDate');
//         $endDate   = Input::get('endDate');

//         foreach ($dotBeliefs as $key => $bValue)
//         {

//             $dotValues = DB::table('dots_values')
//             ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
//             ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
//             ->where('dots_values.status','Active')
//             ->where('dots_values.beliefId',$bValue->id)           
//             ->orderBy('dot_value_list.name','ASC')
//             ->get();

//             $bquery = DB::table('dot_values_ratings')
//             ->leftjoin('users','users.id','=','dot_values_ratings.userId')
//             ->where('dot_values_ratings.beliefId',$bValue->id)
//             ->where('users.status','Active');
            
//             $startDate = Input::get('startDate');
//             $endDate   = Input::get('endDate');
//             //filter acc. to start and end date
//             if(!empty($startDate) && !empty($endDate)) 
//             {

//               $startDate = date_create($startDate);
//               $startDateMonth = date_format($startDate,"Y-m-d");
//               $endDate = date_create($endDate);

//               $endDateYear = date_format($endDate,"Y-m-d");

//               $bquery->where('users.created_at', '>=',$startDateMonth);
//               $bquery->where('users.created_at', '<=',$endDateYear);

//             }
//             //belief rating
//               $bRatings = $bquery->avg('ratings');
              
//               $valuesArray = array();
//               foreach ($dotValues as $key => $vValue) 
//               {
//                 $query = DB::table('dot_values_ratings')
//                 ->leftjoin('users','users.id','=','dot_values_ratings.userId')
//                 ->where('users.status','Active')
//                 ->where('valueId', $vValue->id)
//                 ->where('beliefId',$bValue->id)
//                 ->where('dotId', $dots->id);

//                 $startDate = Input::get('startDate');
//                 $endDate   = Input::get('endDate');

//                 //filter acc. to start and end date
//                 if(!empty($startDate) && !empty($endDate)) 
//                 {

//                   $startDate = date_create($startDate);
//                   $startDateMonth = date_format($startDate,"Y-m-d");

//                   $endDate = date_create($endDate);

//                   $endDateYear = date_format($endDate,"Y-m-d");

//                   $query->where('users.created_at', '>=',$startDateMonth);
//                   $query->where('users.created_at', '<=',$endDateYear);
//               }
              
//               /*print_r($query->toSql());
//               print_r($query->getBindings());
//               die();*/
//               $vRatings = $query->avg('ratings');

//              /* echo " V ";
//              print_r($vRatings);*/

//              $vResult['valueId']      = $vValue->id;
//              $vResult['valueName']    = ucfirst($vValue->name);

//              $vResult['valueRatings'] = 0;
//              if($vRatings)
//              {
//                 $vResult['valueRatings'] = $vRatings-1;
//             }
//             array_push($valuesArray, $vResult);
//         }
//         // die();

//         $result['beliefId']      = $bValue->id;
//         $result['beliefName']    = ucfirst($bValue->name);           

//         $result['beliefRatings'] = 0;
//         if($bRatings)
//         {
//             $result['beliefRatings'] = $bRatings-1;
//         }

//         $result['beliefValues']  = $valuesArray;

//                 //filter by office 
//         if(!empty($officeId)) 
//         {

//             $bRatingsQuery = DB::table('dot_values_ratings')
//             ->leftjoin('users','users.id','=','dot_values_ratings.userId')
//             ->where('users.status','Active')
//             ->where('dot_values_ratings.status','Active')
//             ->where('dot_values_ratings.beliefId', $bValue->id);
//             $bRatingsQuery->where('users.officeId', $officeId);

//             $startDate = Input::get('startDate');
//             $endDate   = Input::get('endDate');

//                     //filter acc. to start and end date
//             if(!empty($startDate) && !empty($endDate)) 
//             {

//               $startDate = date_create($startDate);
//               $startDateMonth = date_format($startDate,"Y-m-d");

//               $endDate = date_create($endDate);

//               $endDateYear = date_format($endDate,"Y-m-d");

//               $bRatingsQuery->where('users.created_at', '>=',$startDateMonth);
//               $bRatingsQuery->where('users.created_at', '<=',$endDateYear);

//           }

//           $isofficeRating = $bRatingsQuery->avg('ratings');

//           if($isofficeRating)
//           {
//             array_push($dotValuesArray, $result);
//         }
//     }
//     else
//     {
//         array_push($dotValuesArray, $result);
//     }

//     }
//           //belief for loop

//     $startDate = Input::get('startDate');
//     $endDate   = Input::get('endDate');

//                 //filter acc. to start and end date
//     if(!empty($startDate) && !empty($endDate)) 
//     {
//         $startDate = date_create($startDate);
//         $startDateMonth = date_format($startDate,"Y-m-d");            
//         $endDate = date_create($endDate);
//         $endDateYear = date_format($endDate,"Y-m-d");

//         $isUser = DB::table('users')
//         ->where('orgId',$orgId)
//         ->where('status','Active')
//         ->where('created_at', '>=', $startDateMonth)           
//         ->where('created_at', '<=', $endDateYear)
//         ->count();

//         if(empty($isUser))
//         {
//             $dotValuesArray = array();
//         }
//     }


// }
   
//    //check DOT rating is given by user on every dot value if it is yes then true
//     $usersQuery = DB::table('users')->where('status','Active');
//     if(!empty($orgId))
//     {
//         $usersQuery->where('orgId',$orgId);
//     }
//     $users = $usersQuery->get();

//     $userCount = count($users);

//     $dotValueRatingCompletedUserArr = array();

//     foreach($users as $duValue)
//     {
//         $dotPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'officeId'=>$officeId,'departmentId'=>$departmentId);

//         //$dotPerArr = array('orgId' => $orgId, 'userId' => $duValue->id, 'updateAt' => '', 'currentDate' => '', 'month' => '', 'year' => '');
//         $status = $this->indexReportForDotCompletedValues($dotPerArr);
//         //$status = app('App\Http\Controllers\Admin\AdminPerformanceController')->getDotPerformance($dotPerArr);
//         if($status)
//         {
//             array_push($dotValueRatingCompletedUserArr, $duValue->id);      
//         }

//     }
//     $dotValueRatingCompletedUserArrCount = count($dotValueRatingCompletedUserArr);

//     $dotCompleted = "0";
//     if(!empty($dotValueRatingCompletedUserArrCount) && !empty($userCount))
//     {
//         $dotCompleted = number_format(($dotValueRatingCompletedUserArrCount/$userCount)*100,2);
//     }

//     //for team role map complete
//     $teamPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId);
//     //$teamPerArr = array('orgId' => $orgId,'updateAt'=>'','currentDate'=>'','month'=>'','year'=>'');
//     $teamRoleStatus = $this->indexReportForTeamroleCompleted($teamPerArr);
//     //$teamRoleStatus = app('App\Http\Controllers\Admin\AdminPerformanceController')->getTeamRoleMap($teamPerArr);

//     $temRoleMapCount = count($teamRoleStatus);

//     $tealRoleMapCompleted = "0";
//     if (!empty($temRoleMapCount) && !empty($userCount))
//     {
//         $tealRoleMapCompleted = number_format(($temRoleMapCount/$userCount)*100,2);
//     }

//     //for personality type completed
//     $personalityPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId);
//     //$personalityPerArr = array('orgId' => $orgId,'updateAt'=>'','currentDate'=>'','month'=>'','year'=>'');
//     $personalityTypeStatus = $this->indexReportForpersonalityTypeCompleted($personalityPerArr);
//     //$personalityTypeStatus = app('App\Http\Controllers\Admin\AdminPerformanceController')->getPersonalityType($personalityPerArr);

//     $personalityTypeCount = count($personalityTypeStatus);

//     $personalityTypeCompleted = "0";
//     if (!empty($personalityTypeCount) && !empty($userCount))
//     {
//         $personalityTypeCompleted = number_format(($personalityTypeCount/$userCount)*100,2);
//     }

//     //for culture structure comleted        
//     $cultureStructurePerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId);
//     $cultureStructureStatus = $this->indexReportForCultureStructureCompleted($cultureStructurePerArr);
//     //$cultureStructureStatus = app('App\Http\Controllers\Admin\AdminPerformanceController')->getCultureStructure($cultureStructurePerArr);

//     $cultureStructureCount = count($cultureStructureStatus);

//     $cultureStructureCompleted = "0";
//     if (!empty($cultureStructureCount) && !empty($userCount))
//     {
//         $cultureStructureCompleted = number_format(($cultureStructureCount/$userCount)*100,2);
//     }

//     //for motivation comleted       
//     $motivationPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId);
//     //$motivationPerArr = array('orgId' => $orgId,'updateAt'=>'','currentDate'=>'','month'=>'','year'=>'');
//     $motivationStatus = $this->indexReportForMotivationCompleted($motivationPerArr);
//     //$motivationStatus = app('App\Http\Controllers\Admin\AdminPerformanceController')->getMotivation($motivationPerArr);

//     $motivationCount = count($motivationStatus);

//     $motivationCompleted = "0";
//     if (!empty($motivationCount) && !empty($userCount))
//     {
//         $motivationCompleted = number_format(($motivationCount/$userCount)*100,2);
//     }

//     //Thumbsup Complted for yesterday
//     $query = DB::table('dot_bubble_rating_records')
//     ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
//     ->leftjoin('users','users.id','dot_bubble_rating_records.to_bubble_user_id')
//     ->leftJoin('departments','departments.id','users.departmentId')
//     ->leftJoin('all_department','all_department.id','departments.departmentId')
//     ->whereDate('dot_bubble_rating_records.created_at',Carbon::yesterday());                 
//     if(!empty($orgId))
//     {
//         $query->where('dots.orgId',$orgId);
//     }
//     if (!empty($officeId) && empty($departmentId)) {
//         $query->where('users.officeId',$officeId);
//     }
//     elseif (!empty($officeId) && !empty($departmentId)) {
//         $query->where('users.officeId',$officeId);
//         $query->where('users.departmentId',$departmentId);
//     }
//     elseif (empty($officeId) && !empty($departmentId)) {
//         $query->where('departments.departmentId',$departmentId);
//     }  
//     $bubbleCount = $query->count('bubble_flag');

//     $thumbCompleted = "0";
//     if(!empty($bubbleCount) && !empty($userCount))
//     {
//         $thumbCompleted = number_format(($bubbleCount/$userCount), 2);
//     }

//     /*get previous day happy index detail*/
//     $userTypeArr = array('3','2','1');

//     $totalIndexes = 0;
//     $totalUser    = 0;
//     foreach($userTypeArr as $type)
//     {
//         $happyUserCountQuery = DB::table('happy_indexes')
//         ->leftjoin('users','users.id','happy_indexes.userId')
//         ->leftJoin('departments','departments.id','users.departmentId')
//         ->leftJoin('all_department','all_department.id','departments.departmentId')
//         ->where('happy_indexes.status','Active')
//         ->where('happy_indexes.moodValue',$type)
//         ->where('users.orgId',$orgId)        
//         ->whereDate('happy_indexes.created_at', Carbon::yesterday());

//         if (!empty($officeId) && empty($departmentId)) {
//             $happyUserCountQuery->where('users.officeId',$officeId);
//         }
//         elseif (!empty($officeId) && !empty($departmentId)) {
//             $happyUserCountQuery->where('users.officeId',$officeId);
//             $happyUserCountQuery->where('users.departmentId',$departmentId);
//         }
//         elseif (empty($officeId) && !empty($departmentId)) {
//             $happyUserCountQuery->where('departments.departmentId',$departmentId);
//         }  

//         $happyUserCount = $happyUserCountQuery->count();

//         $totalUser +=$happyUserCount;

//         if($type==3) 
//         {
//             $totalIndexes += $happyUserCount;
//         }       
//     }

//     $happyIndexCompleted = 0;
//     if($totalIndexes !=0 && $totalUser !=0)
//     { 
//       $happyIndexCompleted = number_format((($totalIndexes/($totalUser))*100),2);
//     }

//     //Tribeometer and diagnostics completed last month by users
//     $diagnosticAnsCompletedUserArr = array();
//     $tribeometerAnsCompletedUserArr = array();

//     foreach($users as $duValue)
//     {
//         $isDiagnosticAnsDoneQuery  = DB::table('diagnostic_answers')
//         ->leftjoin('users','users.id','diagnostic_answers.userId')
//         ->leftJoin('departments','departments.id','users.departmentId')
//         ->leftJoin('all_department','all_department.id','departments.departmentId')
//         ->where('diagnostic_answers.userId',$duValue->id)
//         ->where('diagnostic_answers.orgId',$orgId)
//         ->where('diagnostic_answers.status','Active')
//         ->where('users.status','Active')
//         ->whereMonth('diagnostic_answers.created_at',Carbon::now()->subMonth()->month);

//         if (!empty($officeId) && empty($departmentId)) {
//             $isDiagnosticAnsDoneQuery->where('users.officeId',$officeId);
//         }
//         elseif (!empty($officeId) && !empty($departmentId)) {
//             $isDiagnosticAnsDoneQuery->where('users.officeId',$officeId);
//             $isDiagnosticAnsDoneQuery->where('users.departmentId',$departmentId);
//         }
//         elseif (empty($officeId) && !empty($departmentId)) {
//             $isDiagnosticAnsDoneQuery->where('departments.departmentId',$departmentId);
//         }  
//         $isDiagnosticAnsDone = $isDiagnosticAnsDoneQuery->first();

//         $isTribeometerAnsDoneQuery = DB::table('tribeometer_answers')
//         ->leftjoin('users','users.id','tribeometer_answers.userId')
//         ->leftJoin('departments','departments.id','users.departmentId')
//         ->leftJoin('all_department','all_department.id','departments.departmentId')
//         ->where('tribeometer_answers.userId',$duValue->id)
//         ->where('tribeometer_answers.orgId',$orgId)
//         ->where('tribeometer_answers.status','Active')
//         ->where('users.status','Active')
//         ->whereMonth('tribeometer_answers.created_at',Carbon::now()->subMonth()->month);

//         if (!empty($officeId) && empty($departmentId)) {
//             $isTribeometerAnsDoneQuery->where('users.officeId',$officeId);
//         }
//         elseif (!empty($officeId) && !empty($departmentId)) {
//             $isTribeometerAnsDoneQuery->where('users.officeId',$officeId);
//             $isTribeometerAnsDoneQuery->where('users.departmentId',$departmentId);
//         }
//         elseif (empty($officeId) && !empty($departmentId)) {
//             $isTribeometerAnsDoneQuery->where('departments.departmentId',$departmentId);
//         } 
//         $isTribeometerAnsDone = $isTribeometerAnsDoneQuery->first();

//         if(!empty($isDiagnosticAnsDone))
//         {
//             array_push($diagnosticAnsCompletedUserArr, $duValue->id);      
//         }
//         if($isTribeometerAnsDone)
//         {
//             array_push($tribeometerAnsCompletedUserArr, $duValue->id);      
//         }
//     }

//     $diagnosticAnsCompletedUserArrCount = count($diagnosticAnsCompletedUserArr);
//     $tribeometerAnsCompletedUserArrCount = count($tribeometerAnsCompletedUserArr);


//     $diagnosticAnsCompleted = "0";
//     if(!empty($diagnosticAnsCompletedUserArrCount) && !empty($userCount))
//     {
//         $diagnosticAnsCompleted = number_format(($diagnosticAnsCompletedUserArrCount/$userCount)*100,2);
//     }
//     $tribeometerAnsCompleted = "0";
//     if(!empty($tribeometerAnsCompletedUserArrCount) && !empty($userCount))
//     {
//         $tribeometerAnsCompleted = number_format(($tribeometerAnsCompletedUserArrCount/$userCount)*100,2);
//     }
   
//     //All average ratings for values by users
//     $valuesPerformance = 0;
//     if ($dots) {
//         $allDotBeliefs = DB::table('dots_beliefs')
//         ->where('status','Active')
//         ->where('dotId',$dots->id)
//         ->orderBy('id','ASC')
//         ->get();
//         $totalBeliefRatings = 0;
//         $dotValuesCount = 0;
//         foreach ($allDotBeliefs as $key => $bValue)
//         {
//             $dotValues = DB::table('dots_values')
//             ->where('dots_values.status','Active')
//             ->where('dots_values.beliefId',$bValue->id)           
//             ->count();
//             $dotValuesCount += $dotValues;

//             $beliefRatingQuery = DB::table('dot_values_ratings')
//             ->leftjoin('users','users.id','dot_values_ratings.userId')
//             ->leftJoin('departments','departments.id','users.departmentId')
//             ->leftJoin('all_department','all_department.id','departments.departmentId')
//             ->where('dot_values_ratings.beliefId',$bValue->id)
//             ->where('users.status','Active')
//             ->where('dot_values_ratings.status','Active');

//             if (!empty($officeId) && empty($departmentId)) {
//                 $beliefRatingQuery->where('users.officeId',$officeId);
//             }
//             elseif (!empty($officeId) && !empty($departmentId)) {
//                 $beliefRatingQuery->where('users.officeId',$officeId);
//                 $beliefRatingQuery->where('users.departmentId',$departmentId);
//             }
//             elseif (empty($officeId) && !empty($departmentId)) {
//                 $beliefRatingQuery->where('departments.departmentId',$departmentId);
//             } 
//             $beliefRating = $beliefRatingQuery->avg('ratings');
//             if($beliefRating)
//             {
//                 $totalBeliefRatings += $beliefRating-1;
//             }
//         }
//         $avgRatings = $totalBeliefRatings;
//         $valuesPerformance = number_format(((($avgRatings/$dotValuesCount)/5)*100),2);
//     }

//     //Number of reported incidents and get details of feedback of last month
//     $iotFeedbackQuery = DB::table('iot_feedbacks')
//     ->select('iot_feedbacks.id')
//     ->leftJoin('users','users.id','iot_feedbacks.userId')
//     ->leftJoin('departments','departments.id','users.departmentId')
//     ->leftJoin('all_department','all_department.id','departments.departmentId')
//     ->where('users.status','Active')
//     ->where('iot_feedbacks.status','!=','Inactive')
//     ->where('iot_feedbacks.orgId',$orgId)
//     ->whereMonth('iot_feedbacks.created_at',Carbon::now()->subMonth()->month);

//     if (!empty($officeId) && empty($departmentId)) {
//         $iotFeedbackQuery->where('users.officeId',$officeId);
//     }
//     elseif (!empty($officeId) && !empty($departmentId)) {
//         $iotFeedbackQuery->where('users.officeId',$officeId);
//         $iotFeedbackQuery->where('users.departmentId',$departmentId);
//     }
//     elseif (empty($officeId) && !empty($departmentId)) {
//         $iotFeedbackQuery->where('departments.departmentId',$departmentId);
//     } 
//     $iotFeedback = $iotFeedbackQuery->count();
//     $feedbacks = 0;
//     if (!empty($iotFeedback) && !empty($userCount)) {
//         $feedbacks = number_format((($iotFeedback/$userCount)*500),2);
//     }

//     //Direction and connection of tribeometer
//     $usersForTribeQuery = DB::table('tribeometer_answers')
//     ->leftjoin('users','users.id','tribeometer_answers.userId')
//     ->leftJoin('departments','departments.id','users.departmentId')
//     ->leftJoin('all_department','all_department.id','departments.departmentId')
//     ->select('tribeometer_answers.userId')
//     ->where('users.status','Active')
//     ->groupBy('tribeometer_answers.userId')
//     ->where('users.orgId',$orgId);

//     if (!empty($officeId) && empty($departmentId)) {
//         $usersForTribeQuery->where('users.officeId',$officeId);
//     }
//     elseif (!empty($officeId) && !empty($departmentId)) {
//         $usersForTribeQuery->where('users.officeId',$officeId);
//         $usersForTribeQuery->where('users.departmentId',$departmentId);
//     }
//     elseif (empty($officeId) && !empty($departmentId)) {
//         $usersForTribeQuery->where('departments.departmentId',$departmentId);
//     } 
//     $usersForTribe = $usersForTribeQuery->get();
//     $userCountForTribe = count($usersForTribe);

//     //$tribeometerResultArray = array();
//     $queCatTbl    = DB::table('tribeometer_questions_category')->get();
//     $tribeDirectionnConnection = 0;
//     if (!empty($userCountForTribe))
//     {
//         $optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
//         $optCount = count($optionsTbl)-1; 

//         foreach ($queCatTbl as $value)
//         {
//             $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();
//             $quecount = count($questionTbl);

//             $perQuePercen = 0;
//             foreach ($questionTbl as  $queValue)
//             {
//                 $diaQuery = DB::table('tribeometer_answers')           
//                 ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
//                 ->leftjoin('users','users.id','=','tribeometer_answers.userId')
//                 ->leftJoin('departments','departments.id','users.departmentId')
//                 ->leftJoin('all_department','all_department.id','departments.departmentId')
//                 ->where('users.status','Active') 
//                 ->where('tribeometer_answers.orgId',$orgId)
//                 ->where('tribeometer_questions.id',$queValue->id)
//                 ->where('tribeometer_questions.category_id',$value->id);

//                 if (!empty($officeId) && empty($departmentId)) {
//                     $diaQuery->where('users.officeId',$officeId);
//                 }
//                 elseif (!empty($officeId) && !empty($departmentId)) {
//                     $diaQuery->where('users.officeId',$officeId);
//                     $diaQuery->where('users.departmentId',$departmentId);
//                 }
//                 elseif (empty($officeId) && !empty($departmentId)) {
//                     $diaQuery->where('departments.departmentId',$departmentId);
//                 } 
//                 $diaAnsTbl = $diaQuery->sum('answer');

//                     //avg of all questions
//                 $perQuePercen += ($diaAnsTbl/$userCountForTribe); 
//             }

//             $score = ($perQuePercen/($quecount*$optCount));
//             $totalPercentage = number_format(((($perQuePercen/($quecount*$optCount))*100)*4),2);

//             $tribeDirectionnConnection += $totalPercentage;

//             // $value1['title']      =  $value->title;
//             // $value1['score']      =  number_format((float)$score, 2, '.', '');
//             // $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');            

//             //array_push($tribeometerResultArray, $value1);
//         }
//     }

//     //Diagnostic core 

//     //$diagnosticResultArray = array();

//     $diagnosticQueCatTbl    = DB::table('diagnostic_questions_category')->where('id','!=',5)->get();

//     $diagQuery = DB::table('diagnostic_answers')
//     ->leftjoin('users','users.id','diagnostic_answers.userId')
//     ->leftJoin('departments','departments.id','users.departmentId')
//     ->leftJoin('all_department','all_department.id','departments.departmentId')
//     ->select('diagnostic_answers.userId')
//     ->where('users.status','Active')
//     ->groupBy('diagnostic_answers.userId')
//     ->where('users.orgId',$orgId);

//     if (!empty($officeId) && empty($departmentId)) {
//         $diagQuery->where('users.officeId',$officeId);
//     }
//     elseif (!empty($officeId) && !empty($departmentId)) {
//         $diagQuery->where('users.officeId',$officeId);
//         $diagQuery->where('users.departmentId',$departmentId);
//     }
//     elseif (empty($officeId) && !empty($departmentId)) {
//         $diagQuery->where('departments.departmentId',$departmentId);
//     } 
//     $diagUsers = $diagQuery->get();
//     $diagUserCount = count($diagUsers);
//     $totalDiagnosticCore = 0;

//     if (!empty($diagUserCount))
//     {
//         $diaOptionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
//         $diaOptCount = count($diaOptionsTbl); 

//         $diagnosticCorePer = 0;

//         foreach ($diagnosticQueCatTbl as $value)
//         {
//             $diaQuestionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

//             $diaQuecount = count($diaQuestionTbl);

//             $diaPerQuePercen = 0;
//             foreach ($diaQuestionTbl as  $queValue)
//             {
//                 $diaQuery = DB::table('diagnostic_answers')            
//                 ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
//                 ->leftjoin('users','users.id','=','diagnostic_answers.userId')
//                 ->leftJoin('departments','departments.id','users.departmentId')
//                 ->leftJoin('all_department','all_department.id','departments.departmentId')
//                 ->where('users.status','Active')   
//                 ->where('diagnostic_answers.orgId',$orgId)
//                 ->where('diagnostic_questions.id',$queValue->id)
//                 ->where('diagnostic_questions.category_id',$value->id);

//                 if (!empty($officeId) && empty($departmentId)) {
//                     $diaQuery->where('users.officeId',$officeId);
//                 }
//                 elseif (!empty($officeId) && !empty($departmentId)) {
//                     $diaQuery->where('users.officeId',$officeId);
//                     $diaQuery->where('users.departmentId',$departmentId);
//                 }
//                 elseif (empty($officeId) && !empty($departmentId)) {
//                     $diaQuery->where('departments.departmentId',$departmentId);
//                 } 

//                 $diaAnsTbl = $diaQuery->sum('answer'); 

//                 $diaPerQuePercen += ($diaAnsTbl/$diagUserCount);               
//             }


//             //$diaScore = ($diaPerQuePercen/($diaQuecount*$diaOptCount));
//             $diaTotalPercentage = ($diaPerQuePercen/($diaQuecount*$diaOptCount))*100;

//             // $value1['title']      =  $value->title;
//             // $value1['score']      =  number_format((float)$diaScore, 2, '.', '');
//             // $value1['percentage'] =  number_format((float)$diaTotalPercentage, 2, '.', '');

//             $diagnosticCorePer += $diaTotalPercentage;

//             //array_push($diagnosticResultArray, $value1);
//         }

//         $totalDiagnosticCore = number_format(($diagnosticCorePer/5),2);
//     }

//     //stress inverse of diagnostic
//    // $diagnosticStressArray = array();
//     $diagnosticCatTbl = DB::table('diagnostic_questions_category')->where('id',5)->get();
//     $totalDiagnosticStress = 0;
//     if (!empty($diagUserCount))
//     {
//         $diagOptionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
//         $diagOptCount = count($diagOptionsTbl); 

//         $diagnosticStress = 0;

//         foreach ($diagnosticCatTbl as $value)
//         {
//             $diagQuestionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

//             $diagQuecount = count($diagQuestionTbl);

//             $diagPerQuePercen = 0;
//             foreach ($diagQuestionTbl as  $queValue)
//             {
//                 $diaQuery = DB::table('diagnostic_answers')            
//                 ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
//                 ->leftjoin('users','users.id','=','diagnostic_answers.userId')
//                 ->leftJoin('departments','departments.id','users.departmentId')
//                 ->leftJoin('all_department','all_department.id','departments.departmentId')
//                 ->where('users.status','Active')   
//                 ->where('diagnostic_answers.orgId',$orgId)
//                 ->where('diagnostic_questions.id',$queValue->id)
//                 ->where('diagnostic_questions.category_id',$value->id);

//                 if (!empty($officeId) && empty($departmentId)) {
//                     $diaQuery->where('users.officeId',$officeId);
//                 }
//                 elseif (!empty($officeId) && !empty($departmentId)) {
//                     $diaQuery->where('users.officeId',$officeId);
//                     $diaQuery->where('users.departmentId',$departmentId);
//                 }
//                 elseif (empty($officeId) && !empty($departmentId)) {
//                     $diaQuery->where('departments.departmentId',$departmentId);
//                 } 

//                 $diagAnsTbl = $diaQuery->sum('answer'); 

//                 $diagPerQuePercen += ($diagAnsTbl/$diagUserCount);               
//             }


//            // $diagScore = ($diagPerQuePercen/($diagQuecount*$diagOptCount));
//             $diagTotalPercentage = ($diagPerQuePercen/($diagQuecount*$diagOptCount))*100;


//             //$value1['title']      =  $value->title;
//             //$value1['score']      =  number_format((float)$diagScore, 2, '.', '');
//            // $value1['percentage'] =  number_format((float)$diagTotalPercentage, 2, '.', '');
//             $diagnosticStress = (($diagTotalPercentage-100)*(-1))*3;    

//             //array_push($diagnosticStressArray, $value1);
//         }
//         $totalDiagnosticStress = number_format(($diagnosticStress/5),2);
//     }
//    // print_r($totalDiagnosticStress);die();

//     $releventCompanyScores = $dotCompleted + $tealRoleMapCompleted + $personalityTypeCompleted + $cultureStructureCompleted + $motivationCompleted + $thumbCompleted + $happyIndexCompleted + $diagnosticAnsCompleted + $tribeometerAnsCompleted + $valuesPerformance + $feedbacks + $tribeDirectionnConnection + $totalDiagnosticCore + $totalDiagnosticStress;

//     if (!empty($releventCompanyScores)) {
//         $indexOrg = number_format((($releventCompanyScores/2300)*1000),2);
//     }else
//     {
//         $indexOrg = 0;
//     }

//     // print_r($indexOrg);die();

//     // print_r("Dot=>".$dotCompleted."<br>");
//     // print_r("team role=>".$tealRoleMapCompleted."<br>");
//     // print_r("PT=>".$personalityTypeCompleted."<br>");
//     // print_r("CS=>".$cultureStructureCompleted."<br>");
//     // print_r("motivation=>".$motivationCompleted."<br>");
//     // print_r("Thumpsup=>".$thumbCompleted."<br>");
//     // print_r("happy Index=>".$happyIndexCompleted."<br>");
//     // print_r("Diagnostic Ans=>".$diagnosticAnsCompleted."<br>");
//     // print_r("Tribeometer Ans=>".$tribeometerAnsCompleted."<br>");
//     // print_r("Values Performance=>".$valuesPerformance."<br>");
//     // print_r("feedbacks=>".$feedbacks."<br>");
//     // print_r("Direction and connection=>".$tribeDirectionnConnection."<br>");
//     // print_r("Diagnostic core =>".$totalDiagnosticCore."<br>");
//     // print_r("Diagnostic stress =>".$totalDiagnosticStress);
//     // die;

    
   
//     //for pdf
//     if (empty($resquest->pdfStatus))
//     {
//         return View('admin/report/manageReport',compact('offices','beliefList','dotValuesArray','org','officeId','indexOrg','all_department','orgId','departments','departmentId'));
//     }else{

//         return compact('dotValuesArray','org','officeId','indexOrg','orgId');
//     }
// }


public function getIndexesOfAllOrganizationByPastDate(){
    $year  = '2019';
    $month = '11';  
    //Get total days of month and year
    //$totalDays = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    $totalDays = 20;
    for ($day = 16; $day <= $totalDays; $day++) {
        $makeDate = $year."-".$month."-".$day;
        $this->getIndexesOfAllOrganization($makeDate);
    }
    echo "Done for :".$month." - ".$year." days ".$totalDays;
    die('<br /> Finally Done.');
}




//This function will generate index of org by date
public function getIndexesOfAllOrganization($date=false){
    
    //Send test email
    /*
    Mail::send('welcome', [], function($message) {
        $message->to('ram@chetaru.com')->subject('Testing mails at: '.date('Y-m-d H:i:s')); 
    });
    */

    if($date){
        $date = $date;
    }else{
        $date =date('Y-m-d');
    }


    //Get all organization
    $allOrganizations = DB::table('organisations')
                            ->where('status','Active') 
                            //->where('id','8') 
                            ->select('id')
                            ->whereDate('created_at', '<=',$date)
                            ->get();
    if($allOrganizations){
        foreach($allOrganizations as $organization){
            $orgId = $organization->id;

            $indexOrg = $this->calculationIndex(base64_encode($orgId),'','',$date);
            // $indexOrg = $this->calculationIndex(base64_encode('9'),'','',$date);

            //Chk duplicate record for same day
            $orgRecords = DB::table('indexReportRecords')
                    ->select('id')
                    ->where('orgId',$orgId)
                    ->whereNull('officeId')
                    ->whereNull('departmentId')
                    ->whereDate('date',$date)
                    ->first();

            if($orgRecords){
                //No need to insert
            }else{
                $insertOrgArray = array(
                    'orgId'         => $orgId,              
                    //'officeId'      => '',    
                    //'departmentId'  => '', 
                    'date'          => $date,
                    'total_count'   => $indexOrg,
                    'created_at'    => date('Y-m-d H:i:s')
                );
                $indexOrgReportRecords = DB::table('indexReportRecords')->insertGetId($insertOrgArray);
            } 
       
            //Get offices of organization
            $offices = DB::table('offices')
                        ->select('id')
                        ->where('status','Active') 
                        ->where('orgId',$orgId)->get();
            if($offices){
                foreach($offices as $office){
                    $officeId = $office->id;
                    $indexOffice = $this->calculationIndex(base64_encode($orgId),$officeId,'',$date);
                    //$indexOffice = $this->calculationIndex(base64_encode('9'),'7','',$date);
            
                    //Chk duplicate record for same day
                    $officeRecords = DB::table('indexReportRecords')
                            ->select('id')
                            ->where('orgId',$orgId)
                            ->where('officeId',$officeId)
                            ->whereNull('departmentId')
                            ->whereDate('date',$date)
                            ->first();

                    if($officeRecords){
                        //No need to insert
                    }else{
                        $insertOfficeArray = array(
                            'orgId'         => $orgId,              
                            'officeId'      => $officeId,    
                            //'departmentId'  => '', 
                            'date'          => $date,
                            'total_count'   => $indexOffice,
                            'created_at'    => date('Y-m-d H:i:s')
                        );
                        $indexOfficeReportRecords = DB::table('indexReportRecords')->insertGetId($insertOfficeArray);
                    } 
                    //Get departments
                    $departments = DB::table('departments')
                        //->select('id')
                        ->where('status','Active') 
                        ->where('officeId',$officeId)
                        ->where('orgId',$orgId)->get();
                    if($departments){
                        foreach ($departments as $department) {
                            $departmentId = $department->id;
                            $allDepartmentId = $department->departmentId;
                           
                            //Get index
                            $deptindex = $this->calculationIndex(base64_encode($orgId),'',$allDepartmentId,$date);
                            //$deptindex = $this->calculationIndex(base64_encode('9'),'','6',$date);
                            
                            //Chk duplicate record for same day
                            $deptRecords = DB::table('indexReportRecords')
                                    ->select('id')
                                    ->where('orgId',$orgId)
                                    ->whereNull('officeId')
                                    ->where('departmentId',$allDepartmentId)
                                    ->whereDate('date',$date)
                                    // ->where(DB::raw("(DATE_FORMAT(date,'%Y-%m-%d'))"),$date)
                                    ->first();
                            if($deptRecords){
                                //No need to insert
                            }else{
                                $insertDeptArray = array(
                                    'orgId'         => $orgId,              
                                    //'officeId'      => '',    
                                    'departmentId'  => $allDepartmentId, 
                                    'isAllDepId'    => "1",
                                    'date'          => $date,
                                    'total_count'   => $deptindex,
                                    'created_at'    => date('Y-m-d H:i:s')
                                );
                                $indexDeptReportRecords = DB::table('indexReportRecords')->insertGetId($insertDeptArray);
                            }

                            $index = $this->calculationIndex(base64_encode($orgId),$officeId,$departmentId,$date);
                            //$index = $this->calculationIndex(base64_encode('9'),'7','9',$date);

                            //Chk duplicate record for same day
                            $indexrecords = DB::table('indexReportRecords')
                                    ->select('id')
                                    ->where('orgId',$orgId)
                                    ->where('officeId',$officeId)
                                    ->where('departmentId',$departmentId)
                                    ->whereDate('date',$date)
                                    // ->where(DB::raw("(DATE_FORMAT(date,'%Y-%m-%d'))"),$date)
                                    ->first();

                            if($indexrecords){
                                //No need to insert
                            }else{
                                $insertArray = array(
                                    'orgId'         => $orgId,              
                                    'officeId'      => $officeId,    
                                    'departmentId'  => $departmentId, 
                                    'isAllDepId'    => "2",
                                    'date'          => $date,
                                    'total_count'   => $index,
                                    'created_at'    => date('Y-m-d H:i:s')
                                );
                                $indexReportRecords = DB::table('indexReportRecords')->insertGetId($insertArray);
                            }        
                        }   
                    }
                }
            }
        }
    }
}


public function calculationIndex($id,$officeId,$departmentId,$date){

    //$date='2018-08-20';
    //$date='2019-11-05';

    $orgId = base64_decode($id);
    $departmentId = $departmentId;
    $officeId = $officeId;

     //Get dots of org            
    $dots = DB::table('dots')
            ->where('orgId',$orgId)->first();
            
//------------------------------------Calculation Start---------------------------------------

   //check DOT g is given by user on every dot value if it is yes then true
    $usersQuery = DB::table('users')
    ->select('users.id as id')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId');
    if(!empty($orgId)){
        $usersQuery->where('users.orgId',$orgId);
    }

    if($date){
        $usersQuery->whereDate('users.created_at','<=',$date);
        // $usersQuery->where(DB::raw("(DATE_FORMAT(users.created_at,'%Y-%m-%d'))"),"<=",$date);
    }

    if (!empty($officeId) && empty($departmentId)) {
        $usersQuery->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $usersQuery->where('users.officeId',$officeId);
        $usersQuery->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $usersQuery->where('departments.departmentId',$departmentId);
    }

    $users      = $usersQuery->get();
    $userCount  = count($users);

    $dotValueRatingCompletedUserArr = array();
    foreach($users as $duValue){
        $dotPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);

        //$dotPerArr = array('orgId' => $orgId, 'userId' => $duValue->id, 'updateAt' => '', 'currentDate' => '', 'month' => '', 'year' => '');
        $status = $this->indexReportForDotCompletedValues($dotPerArr);
        //$status = app('App\Http\Controllers\Admin\AdminPerformanceController')->getDotPerformance($dotPerArr);
        if($status){
            array_push($dotValueRatingCompletedUserArr, $duValue->id);      
        }
    }
    $dotValueRatingCompletedUserArrCount = count($dotValueRatingCompletedUserArr);
    $dotCompleted = "0";
    if(!empty($dotValueRatingCompletedUserArrCount) && !empty($userCount)){
        //echo $dotValueRatingCompletedUserArrCount." = ".$userCount;die;
        $dotCompleted = round(($dotValueRatingCompletedUserArrCount/$userCount)*100,2);
    }

    //for team role map complete
    $teamPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);
    //$teamPerArr = array('orgId' => $orgId,'updateAt'=>'','currentDate'=>'','month'=>'','year'=>'');
    $teamRoleStatus = $this->indexReportForTeamroleCompleted($teamPerArr);
    //$teamRoleStatus = app('App\Http\Controllers\Admin\AdminPerformanceController')->getTeamRoleMap($teamPerArr);
    $temRoleMapCount        = count($teamRoleStatus);
    $tealRoleMapCompleted   = "0";
    if (!empty($temRoleMapCount) && !empty($userCount)){
        $tealRoleMapCompleted = round(($temRoleMapCount/$userCount)*100,2);
    }

    //for personality type completed
    $personalityPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);
    //$personalityPerArr = array('orgId' => $orgId,'updateAt'=>'','currentDate'=>'','month'=>'','year'=>'');
    $personalityTypeStatus = $this->indexReportForpersonalityTypeCompleted($personalityPerArr);
    //$personalityTypeStatus = app('App\Http\Controllers\Admin\AdminPerformanceController')->getPersonalityType($personalityPerArr);

    $personalityTypeCount       = count($personalityTypeStatus);
    $personalityTypeCompleted   = "0";
    if (!empty($personalityTypeCount) && !empty($userCount)){
        $personalityTypeCompleted = round(($personalityTypeCount/$userCount)*100,2);
    }

    //for culture structure comleted        
    $cultureStructurePerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);
    $cultureStructureStatus = $this->indexReportForCultureStructureCompleted($cultureStructurePerArr);
    //$cultureStructureStatus = app('App\Http\Controllers\Admin\AdminPerformanceController')->getCultureStructure($cultureStructurePerArr);
    $cultureStructureCount      = count($cultureStructureStatus);
    $cultureStructureCompleted  = "0";
    if (!empty($cultureStructureCount) && !empty($userCount)){
        $cultureStructureCompleted = round(($cultureStructureCount/$userCount)*100,2);
    }

    //for motivation comleted       
    $motivationPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);
    //$motivationPerArr = array('orgId' => $orgId,'updateAt'=>'','currentDate'=>'','month'=>'','year'=>'');
    $motivationStatus = $this->indexReportForMotivationCompleted($motivationPerArr);
    //$motivationStatus = app('App\Http\Controllers\Admin\AdminPerformanceController')->getMotivation($motivationPerArr);

    $motivationCount = count($motivationStatus);
    $motivationCompleted = "0";
    if (!empty($motivationCount) && !empty($userCount)){
        $motivationCompleted = round(($motivationCount/$userCount)*100,2);
    }

    //Kudos/Thumbsup Completed for yesterday
    $thumbsupPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);
    $thumbsupCount = $this->indexReportForThumbsupCompleted($thumbsupPerArr);

    $thumbCompleted = "0";
    if(!empty($thumbsupCount) && !empty($userCount)){
        $thumbCompleted = round(($thumbsupCount/$userCount), 2);
    }

    /*get previous day Good/happy index detail*/
    $happyIndexPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate' => $date);
    $happyIndexCompleted = $this->indexReportForHappyIndexCompleted($happyIndexPerArr);

    //Tribeometer and diagnostics completed last month by users
    $diagnosticAnsCompletedUserArr = array();
    $tribeometerAnsCompletedUserArr = array();
    foreach($users as $duValue){


        if($date){
            //Get Previos month of date
            $timestamp = strtotime ("-1 month",strtotime ($date));
            //Convert into 'Y-m' format
            $previosMonthFromDate  =  date("Y-m",$timestamp);

            $isDiagnosticAnsDoneQuery  = DB::table('diagnostic_answers')
                ->leftjoin('users','users.id','diagnostic_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('diagnostic_answers.userId',$duValue->id)
                ->where('diagnostic_answers.orgId',$orgId)
                ->where('diagnostic_answers.status','Active')
                ->where('users.status','Active')
                ->where('departments.status','Active')
                ->where('all_department.status','Active')
                ->where('diagnostic_answers.updated_at','LIKE',$previosMonthFromDate."%");
                // ->where('diagnostic_answers.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
                //->whereMonth('diagnostic_answers.created_at',Carbon::now()->subMonth()->month);
        }else{
            $lastMonth = date("Y-m", strtotime("-1 month"));

            $isDiagnosticAnsDoneQuery  = DB::table('diagnostic_answers')
                ->leftjoin('users','users.id','diagnostic_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('diagnostic_answers.userId',$duValue->id)
                ->where('diagnostic_answers.orgId',$orgId)
                ->where('diagnostic_answers.status','Active')
                ->where('users.status','Active')
                ->where('departments.status','Active')
                ->where('all_department.status','Active')
                ->where('diagnostic_answers.updated_at','LIKE',$lastMonth."%");
                // ->where('diagnostic_answers.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
                //->whereMonth('diagnostic_answers.created_at',Carbon::now()->subMonth()->month);
        } 

        if (!empty($officeId) && empty($departmentId)) {
            $isDiagnosticAnsDoneQuery->where('users.officeId',$officeId);
        }
        elseif (!empty($officeId) && !empty($departmentId)) {
            $isDiagnosticAnsDoneQuery->where('users.officeId',$officeId);
            $isDiagnosticAnsDoneQuery->where('users.departmentId',$departmentId);
        }
        elseif (empty($officeId) && !empty($departmentId)) {
            $isDiagnosticAnsDoneQuery->where('departments.departmentId',$departmentId);
        }  
        $isDiagnosticAnsDone = $isDiagnosticAnsDoneQuery->first();


        if($date){
            //Get Previos month of date
            $timestamp = strtotime ("-1 month",strtotime ($date));
            //Convert into 'Y-m' format
            $previosMonthFromDate  =  date("Y-m",$timestamp);

            $isTribeometerAnsDoneQuery = DB::table('tribeometer_answers')
                ->leftjoin('users','users.id','tribeometer_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('tribeometer_answers.userId',$duValue->id)
                ->where('tribeometer_answers.orgId',$orgId)
                ->where('tribeometer_answers.status','Active')
                ->where('users.status','Active')
                ->where('tribeometer_answers.updated_at','LIKE',$previosMonthFromDate."%");
                //->where(DB::raw("(DATE_FORMAT(tribeometer_answers.updated_at,'%Y-%m-%d'))"),$date);
                //->where('tribeometer_answers.updated_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
                // ->where('tribeometer_answers.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
                //->whereMonth('tribeometer_answers.created_at',Carbon::now()->subMonth()->month);
        }else{
            $lastMonth = date("Y-m", strtotime("-1 month"));

            $isTribeometerAnsDoneQuery = DB::table('tribeometer_answers')
                ->leftjoin('users','users.id','tribeometer_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('tribeometer_answers.userId',$duValue->id)
                ->where('tribeometer_answers.orgId',$orgId)
                ->where('tribeometer_answers.status','Active')
                ->where('users.status','Active')
                ->where('tribeometer_answers.updated_at','LIKE',$lastMonth."%");
                //->where(DB::raw("(DATE_FORMAT(tribeometer_answers.updated_at,'%Y-%m-%d'))"),$date);
                //->where('tribeometer_answers.updated_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
                // ->where('tribeometer_answers.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
                //->whereMonth('tribeometer_answers.created_at',Carbon::now()->subMonth()->month);
        }



        if (!empty($officeId) && empty($departmentId)) {
            $isTribeometerAnsDoneQuery->where('users.officeId',$officeId);
        }elseif (!empty($officeId) && !empty($departmentId)) {
            $isTribeometerAnsDoneQuery->where('users.officeId',$officeId);
            $isTribeometerAnsDoneQuery->where('users.departmentId',$departmentId);
        }elseif (empty($officeId) && !empty($departmentId)) {
            $isTribeometerAnsDoneQuery->where('departments.departmentId',$departmentId);
        } 
        $isTribeometerAnsDone = $isTribeometerAnsDoneQuery->first();

        if(!empty($isDiagnosticAnsDone)){
            array_push($diagnosticAnsCompletedUserArr, $duValue->id);      
        }
        if($isTribeometerAnsDone){
            array_push($tribeometerAnsCompletedUserArr, $duValue->id);      
        }
    }

    $diagnosticAnsCompletedUserArrCount  = count($diagnosticAnsCompletedUserArr);
    $tribeometerAnsCompletedUserArrCount = count($tribeometerAnsCompletedUserArr);


    $diagnosticAnsCompleted = "0";
    if(!empty($diagnosticAnsCompletedUserArrCount) && !empty($userCount)){
        $diagnosticAnsCompleted = ($diagnosticAnsCompletedUserArrCount/$userCount)*100;
    }
    $tribeometerAnsCompleted = "0";
    if(!empty($tribeometerAnsCompletedUserArrCount) && !empty($userCount)){
        $tribeometerAnsCompleted = ($tribeometerAnsCompletedUserArrCount/$userCount)*100;
    }

//echo $diagnosticAnsCompleted;die;
//echo $tribeometerAnsCompleted;die;   




    //All average ratings for values by users
    $valuesPerformance = 0;
    if ($dots) {
        $allDotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->orderBy('id','ASC')
        ->get();
        $totalBeliefRatings = 0;
        $dotValuesCount = 0;
        foreach ($allDotBeliefs as $key => $bValue){
            if($date){
                $dotValues = DB::table('dots_values')
                ->where('dots_values.status','Active')
                ->where('dots_values.beliefId',$bValue->id)   
                ->whereDate('dots_values.created_at','<=',$date)
                // ->where(DB::raw("(DATE_FORMAT(dots_values.created_at,'%Y-%m-%d'))"),"<=",$date)    
                ->count();    
            }else{
                $dotValues = DB::table('dots_values')
                ->where('dots_values.status','Active')
                ->where('dots_values.beliefId',$bValue->id)       
                ->count();   
            }            
            $dotValuesCount += $dotValues;

            $beliefRatingQuery = DB::table('dot_values_ratings')
                ->leftjoin('users','users.id','dot_values_ratings.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('dot_values_ratings.beliefId',$bValue->id)
                ->where('users.status','Active')
                //->where(DB::raw("(DATE_FORMAT(dot_values_ratings.created_at,'%Y-%m-%d'))"),"<=",$date)
                ->where('dot_values_ratings.status','Active');
            if($date){
                $beliefRatingQuery->whereDate('dot_values_ratings.created_at','<=',$date);
                // $beliefRatingQuery->where(DB::raw("(DATE_FORMAT(dot_values_ratings.created_at,'%Y-%m-%d'))"),"<=",$date);
            }
            if (!empty($officeId) && empty($departmentId)) {
                $beliefRatingQuery->where('users.officeId',$officeId);
            }elseif (!empty($officeId) && !empty($departmentId)) {
                $beliefRatingQuery->where('users.officeId',$officeId);
                $beliefRatingQuery->where('users.departmentId',$departmentId);
            }elseif (empty($officeId) && !empty($departmentId)) {
                $beliefRatingQuery->where('departments.departmentId',$departmentId);
            } 
            $beliefRating = $beliefRatingQuery->avg('ratings');
            if($beliefRating){
                $totalBeliefRatings += $beliefRating-1;
            }
        }
        $avgRatings = $totalBeliefRatings;
        if($dotValuesCount>0){
            $valuesPerformance = (($avgRatings/$dotValuesCount)/5)*100;
        }
    }



  //(Total feedback count /All active users count of organisation) /(no of month from the day of creation of first staff till now) *500

    $firstStaffOfOrg = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    if($firstStaffOfOrg){
        $firstDateOfOrg  = date('Y-m',strtotime($firstStaffOfOrg->created_at));
        $lastMonthDate   = date('Y-m');

       // $firstDateOfOrg  = "2019-02-10"; // always small
       // $lastMonthDate   = "2019-11-12";



        $time1 = strtotime($firstDateOfOrg);  // always small
        $time2 = strtotime($lastMonthDate);
        if(date('m', $time1)==date('m', $time2) && date('Y', $time1)==date('Y', $time2)){
          $noOfPreviousMonths = 1;
        }else{
          if ($time1 < $time2){
            $my = date('mY', $time2);
            $noOfPreviousMonths = array(date('F', $time1));
            while($time1 < $time2) {
              $time1 = strtotime(date('Y-m-d', $time1).' +1 month');
              if(date('mY', $time1) != $my && ($time1 < $time2))
                $noOfPreviousMonths[] = date('F', $time1);
            }
            $noOfPreviousMonths[] = date('F', $time2);
            //print_r($noOfPreviousMonths);die;
            $noOfPreviousMonths   = count($noOfPreviousMonths);                    
          }else{
            $noOfPreviousMonths = 0;
          } 
        }
    }else{
        $noOfPreviousMonths = 0;
    }


    //echo "D1->".$firstDateOfOrg." <br />D2->".$lastMonthDate." <br />Total ".$noOfPreviousMonths;die;

    //$firstStaffOrg = DB::table('offices')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    //$date1 = new DateTime($firstStaffOrg->created_at);
    //$date2 = $date1->diff(new DateTime(Carbon::today()));
    //$totalMonthcount = ($date2->y*12) + $date2->m + 2;

    //Number of reported incidents and get details of feedback of last month



    //Number of reported incidents and get details of feedback of last month
     $iotFeedbackQuery = DB::table('iot_feedbacks')
        ->select('iot_feedbacks.id')
        ->leftJoin('users','users.id','iot_feedbacks.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('iot_feedbacks.status','!=','Inactive')
        ->where('iot_feedbacks.orgId',$orgId);
        //->where('iot_feedbacks.updated_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        // ->where('iot_feedbacks.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        //->whereMonth('iot_feedbacks.created_at',Carbon::now()->subMonth()->month);
    if($date){
        $iotFeedbackQuery->whereDate('iot_feedbacks.created_at','<=',$date);
        // $iotFeedbackQuery->where(DB::raw("(DATE_FORMAT(iot_feedbacks.created_at,'%Y-%m-%d'))"),'<=',$date);
    }
    if (!empty($officeId) && empty($departmentId)) {
        $iotFeedbackQuery->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $iotFeedbackQuery->where('users.officeId',$officeId);
        $iotFeedbackQuery->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $iotFeedbackQuery->where('departments.departmentId',$departmentId);
    } 
    $iotFeedback = $iotFeedbackQuery->count();
    $feedbacks = 0;
    if (!empty($iotFeedback) && !empty($userCount)) {
        if($userCount>0 && $noOfPreviousMonths>0){
            //$feedbacks = round((($iotFeedback/$userCount)/$totalMonthcount*500),2);

            //(Total number of feedback / Number of staff)/number of months client live *500
            //feedbacks = ($iotFeedback/$userCount)/($noOfPreviousMonths*500);
            $feedbacks = (($iotFeedback/$userCount)/($noOfPreviousMonths))*500;
        }
    }

    //Direction and connection of tribeometer
     $usersForTribeQuery = DB::table('tribeometer_answers')
        ->leftjoin('users','users.id','tribeometer_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->select('tribeometer_answers.userId')
        ->where('users.status','Active')
        ->where('departments.status','Active') 
        ->where('all_department.status','Active') 
        ->groupBy('tribeometer_answers.userId')
        ->where('users.orgId',$orgId);
    if($date){
        $usersForTribeQuery->whereDate('tribeometer_answers.created_at','<=',$date);
        // $usersForTribeQuery->where(DB::raw("(DATE_FORMAT(tribeometer_answers.created_at,'%Y-%m-%d'))"),'<=',$date);
    }
    if (!empty($officeId) && empty($departmentId)) {
        $usersForTribeQuery->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $usersForTribeQuery->where('users.officeId',$officeId);
        $usersForTribeQuery->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $usersForTribeQuery->where('departments.departmentId',$departmentId);
    } 
    $usersForTribe = $usersForTribeQuery->get();
    $userCountForTribe = count($usersForTribe);

    //$tribeometerResultArray = array();
    $queCatTbl    = DB::table('tribeometer_questions_category')->get();
    $tribeDirectionnConnection = 0;
    if (!empty($userCountForTribe)){
        $optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
        $optCount = count($optionsTbl)-1; 

        foreach ($queCatTbl as $value){
            $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();
            $quecount = count($questionTbl);

            $perQuePercen = 0;
            foreach ($questionTbl as  $queValue){
                
                $diaQuery = DB::table('tribeometer_answers')           
                ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
                ->leftjoin('users','users.id','=','tribeometer_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.status','Active') 
                ->where('departments.status','Active') 
                ->where('all_department.status','Active') 
                ->where('tribeometer_answers.orgId',$orgId)
                ->where('tribeometer_questions.id',$queValue->id)
                ->where('tribeometer_questions.category_id',$value->id);

                if($date){
                    $diaQuery->whereDate('tribeometer_answers.created_at','<=',$date);
                    // $diaQuery->where(DB::raw("(DATE_FORMAT(tribeometer_answers.created_at,'%Y-%m-%d'))"),'<=',$date);
                }

                if (!empty($officeId) && empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                }elseif (!empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                    $diaQuery->where('users.departmentId',$departmentId);
                }elseif (empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('departments.departmentId',$departmentId);
                } 
                $diaAnsTbl = $diaQuery->sum('answer');

                //avg of all questions
                $perQuePercen += ($diaAnsTbl/$userCountForTribe); 
            }

            $score = ($perQuePercen/($quecount*$optCount));
            $totalPercentage = (($perQuePercen/($quecount*$optCount))*100)*4;

            $tribeDirectionnConnection += $totalPercentage;

            // $value1['title']      =  $value->title;
            // $value1['score']      =  number_format((float)$score, 2, '.', '');
            // $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');            

            //array_push($tribeometerResultArray, $value1);
        }
    }

    //Diagnostic core 

    //$diagnosticResultArray = array();

    $diagnosticQueCatTbl    = DB::table('diagnostic_questions_category')
                            ->where('id','!=',5)->get();

    $diagQuery = DB::table('diagnostic_answers')
        ->leftjoin('users','users.id','diagnostic_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->select('diagnostic_answers.userId')
        ->where('users.status','Active')
        ->groupBy('diagnostic_answers.userId')
        ->where('users.orgId',$orgId);
    if($date){
        $diagQuery->whereDate('diagnostic_answers.created_at','<=',$date);
        // $diagQuery->where(DB::raw("(DATE_FORMAT(diagnostic_answers.created_at,'%Y-%m-%d'))"),'<=',$date);
    }

    if (!empty($officeId) && empty($departmentId)) {
        $diagQuery->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $diagQuery->where('users.officeId',$officeId);
        $diagQuery->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $diagQuery->where('departments.departmentId',$departmentId);
    } 
    $diagUsers = $diagQuery->get();
    $diagUserCount = count($diagUsers);
    $totalDiagnosticCore = 0;

    if (!empty($diagUserCount)){
        $diaOptionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
        $diaOptCount    = count($diaOptionsTbl); 

        $diagnosticCorePer = 0;

        foreach ($diagnosticQueCatTbl as $value){
            $diaQuestionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

            $diaQuecount = count($diaQuestionTbl);

            $diaPerQuePercen = 0;
            foreach ($diaQuestionTbl as  $queValue){
                $diaQuery = DB::table('diagnostic_answers')            
                    ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
                    ->leftjoin('users','users.id','=','diagnostic_answers.userId')
                    ->leftJoin('departments','departments.id','users.departmentId')
                    ->leftJoin('all_department','all_department.id','departments.departmentId')
                    ->where('users.status','Active')   
                    ->where('diagnostic_answers.orgId',$orgId)
                    ->where('diagnostic_questions.id',$queValue->id)
                    ->where('diagnostic_questions.category_id',$value->id);
                if($date){
                    $diaQuery->whereDate('diagnostic_answers.created_at','<=',$date);
                    // $diaQuery->where(DB::raw("(DATE_FORMAT(diagnostic_answers.created_at,'%Y-%m-%d'))"),'<=',$date);
                }
                if (!empty($officeId) && empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                }
                elseif (!empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                    $diaQuery->where('users.departmentId',$departmentId);
                }
                elseif (empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('departments.departmentId',$departmentId);
                } 

                $diaAnsTbl = $diaQuery->sum('answer'); 

                $diaPerQuePercen += ($diaAnsTbl/$diagUserCount);               
            }


            //$diaScore = ($diaPerQuePercen/($diaQuecount*$diaOptCount));
            $diaTotalPercentage = ($diaPerQuePercen/($diaQuecount*$diaOptCount))*100;

            // $value1['title']      =  $value->title;
            // $value1['score']      =  number_format((float)$diaScore, 2, '.', '');
            // $value1['percentage'] =  number_format((float)$diaTotalPercentage, 2, '.', '');

            $diagnosticCorePer += $diaTotalPercentage;

            //array_push($diagnosticResultArray, $value1);
        }

        $totalDiagnosticCore = round(($diagnosticCorePer/5),2);
    }

    //stress inverse of diagnostic
   // $diagnosticStressArray = array();
    $diagnosticCatTbl = DB::table('diagnostic_questions_category')->where('id',5)->get();
    $totalDiagnosticStress = 0;
    if (!empty($diagUserCount)) {
        $diagOptionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
        $diagOptCount = count($diagOptionsTbl); 

        $diagnosticStress = 0;

        foreach ($diagnosticCatTbl as $value) {
            $diagQuestionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

            $diagQuecount = count($diagQuestionTbl);

            $diagPerQuePercen = 0;
            foreach ($diagQuestionTbl as  $queValue){
                $diaQuery = DB::table('diagnostic_answers')            
                ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
                ->leftjoin('users','users.id','=','diagnostic_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.status','Active')   
                ->where('diagnostic_answers.orgId',$orgId)
                ->where('diagnostic_questions.id',$queValue->id)
                ->where('diagnostic_questions.category_id',$value->id);
                if($date){
                    $diaQuery->whereDate('diagnostic_answers.created_at','<=',$date);
                    // $diaQuery->where(DB::raw("(DATE_FORMAT(diagnostic_answers.created_at,'%Y-%m-%d'))"),'<=',$date);
                }
                if (!empty($officeId) && empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                }elseif (!empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                    $diaQuery->where('users.departmentId',$departmentId);
                }elseif (empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('departments.departmentId',$departmentId);
                } 

                $diagAnsTbl = $diaQuery->sum('answer'); 

                $diagPerQuePercen += ($diagAnsTbl/$diagUserCount);               
            }


           // $diagScore = ($diagPerQuePercen/($diagQuecount*$diagOptCount));
            $diagTotalPercentage = ($diagPerQuePercen/($diagQuecount*$diagOptCount))*100;

            //$value1['title']      =  $value->title;
            //$value1['score']      =  number_format((float)$diagScore, 2, '.', '');
           // $value1['percentage'] =  number_format((float)$diagTotalPercentage, 2, '.', '');
            $diagnosticStress = (($diagTotalPercentage-100)*(-1))*3;    

            //array_push($diagnosticStressArray, $value1);
        }
        $totalDiagnosticStress = round(($diagnosticStress),2);
    }
//    print_r($totalDiagnosticStress);die();

    $releventCompanyScores = $dotCompleted + $tealRoleMapCompleted + $personalityTypeCompleted + $cultureStructureCompleted + $motivationCompleted + $thumbCompleted + $happyIndexCompleted + $diagnosticAnsCompleted + $tribeometerAnsCompleted + $valuesPerformance + $feedbacks + $tribeDirectionnConnection + $totalDiagnosticCore + $totalDiagnosticStress;

    if (!empty($releventCompanyScores)) {
        $indexOrg = round((($releventCompanyScores/2300)*1000),2);
    }else{
        $indexOrg = 0;
    }


/*
     print_r("Final result => ".$indexOrg."<br>");
     print_r("Dot=>".$dotCompleted."<br>");
     print_r("team role=>".$tealRoleMapCompleted."<br>");
     print_r("PT=>".$personalityTypeCompleted."<br>");
     print_r("CS=>".$cultureStructureCompleted."<br>");
     print_r("motivation=>".$motivationCompleted."<br>");
     print_r("Thumpsup=>".$thumbCompleted."<br>");
     print_r("happy Index=>".$happyIndexCompleted."<br>");
     print_r("Diagnostic Ans=>".$diagnosticAnsCompleted."<br>");
     print_r("Tribeometer Ans=>".$tribeometerAnsCompleted."<br>");
     print_r("Values Performance=>".$valuesPerformance."<br>");
     print_r("feedbacks=>".$feedbacks."<br>");
     print_r("Direction and connection=>".$tribeDirectionnConnection."<br>");
     print_r("Diagnostic core =>".$totalDiagnosticCore."<br>");
     print_r("Diagnostic stress =>".$totalDiagnosticStress);
     die;
*/   

   return $indexOrg;
}


function sumOfIndex($year,$month=false,$getArr = array(),$day=false){
    $orgId          = $getArr["orgId"];
    $officeId       = $getArr["officeId"];
    $departmentId   = $getArr["departmentId"];

    $totalIndex=0;


    $indexReportRecords = DB::table('indexReportRecords')
            ->select('indexReportRecords.total_count')
            ->where('indexReportRecords.orgId',$orgId);


    if($officeId=='' && $departmentId==''){
        
        $indexReportRecords->whereNull('indexReportRecords.officeId');
        $indexReportRecords->whereNull('indexReportRecords.departmentId');

        if($year!='' && $day!='' && $month!=''){
            $indexReportRecords->whereDate('indexReportRecords.date',$year.'-'.$month.'-'.$day);
        }else if($year!='' && $day=='' && $month!=''){
            $indexReportRecords->where('indexReportRecords.date','LIKE',$year.'-'.$month."%");
        }else{
            $indexReportRecords->whereYear('indexReportRecords.date',$year);
        }

    }else{
        
        if($year!='' && $day!='' && $month!=''){
            $indexReportRecords->whereDate('indexReportRecords.date',$year.'-'.$month.'-'.$day);
        }else if($year!='' && $day=='' && $month!=''){
            $indexReportRecords->where('indexReportRecords.date','LIKE',$year.'-'.$month."%");
        }else{
            $indexReportRecords->whereYear('indexReportRecords.date',$year);
        }

        if($orgId!='' && $officeId!=''){  
            if($departmentId!=''){
                //match with dep tbl id
                $indexReportRecords->where('indexReportRecords.isAllDepId','2');
                $indexReportRecords->where('indexReportRecords.officeId',$officeId);
                $indexReportRecords->where('indexReportRecords.departmentId',$departmentId);
            }else{
                $indexReportRecords->whereNull('indexReportRecords.isAllDepId');
                $indexReportRecords->whereNull('indexReportRecords.departmentId');
                $indexReportRecords->where('indexReportRecords.officeId',$officeId);
            }
        }else if($orgId!='' && $officeId=='' && $departmentId!=''){   //match with all dep tbl id
             $indexReportRecords->where('indexReportRecords.isAllDepId','1');
             $indexReportRecords->where('indexReportRecords.departmentId',$departmentId);
        }
    }        


/*
    if($year!='' && $day!='' && $month!=''){
        $indexReportRecords = DB::table('indexReportRecords')
            ->select('indexReportRecords.total_count')
            ->where('indexReportRecords.orgId',$orgId) 
            ->whereDate('indexReportRecords.date',$year.'-'.$month.'-'.$day);
            // ->where(DB::raw("(DATE_FORMAT(indexReportRecords.date,'%Y-%m-%d'))"),$year.'-'.$month.'-'.$day);

    }else if($year!='' && $day=='' && $month!=''){
        $indexReportRecords = DB::table('indexReportRecords')
            ->select('indexReportRecords.total_count')
            ->where('indexReportRecords.orgId',$orgId)   
            ->where('indexReportRecords.date','LIKE',$year.'-'.$month."%");
            // ->where(DB::raw("(DATE_FORMAT(indexReportRecords.date,'%Y-%m'))"),$year.'-'.$month);
    }else{  //all
        $indexReportRecords = DB::table('indexReportRecords')
            ->select('indexReportRecords.total_count')
            ->where('indexReportRecords.orgId',$orgId)   
            ->whereYear('indexReportRecords.date',$year);
            // ->where(DB::raw("(DATE_FORMAT(indexReportRecords.date,'%Y'))"),$year);
    }

 
    if (!empty($officeId) && empty($departmentId)) {
        $indexReportRecords->Join('departments','departments.id','indexReportRecords.departmentId');
        $indexReportRecords->Join('all_department','all_department.id','departments.departmentId');
        $indexReportRecords->where('all_department.status','Active');
        $indexReportRecords->where('departments.status','Active');
        $indexReportRecords->where('indexReportRecords.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $indexReportRecords->Join('departments','departments.id','indexReportRecords.departmentId');
        $indexReportRecords->Join('all_department','all_department.id','departments.departmentId');
        $indexReportRecords->where('all_department.status','Active');
        $indexReportRecords->where('departments.status','Active');
        $indexReportRecords->where('indexReportRecords.officeId',$officeId);
        $indexReportRecords->where('indexReportRecords.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $indexReportRecords->where('all_department.status','Active');
        $indexReportRecords->where('departments.status','Active');
        $indexReportRecords->Join('departments','departments.id','indexReportRecords.departmentId');
        $indexReportRecords->Join('all_department','all_department.id','departments.departmentId');
        $indexReportRecords->where('departments.departmentId',$departmentId);
    }
*/




    $indexRecords = $indexReportRecords->get()->toArray();  
    $c=0;
    if($indexRecords){
        foreach($indexRecords as $indexRecord){
             if($year!='' && $day!='' && $month!=''){
                //
             }else{
                if($indexRecord->total_count>0){
                    $c++;
                }
             }
            $totalIndex+=$indexRecord->total_count;
        }
    }
    if($c>0){
        $totalIndex = number_format($totalIndex/$c);    
        return $totalIndex;
    }else{
         return $totalIndex;
    }
    
}



// function sumOfIndex($year,$month=false,$getArr = array(),$day=false){
//     $orgId          = $getArr["orgId"];
//     $officeId       = $getArr["officeId"];
//     $departmentId   = $getArr["departmentId"];

//     $totalIndex=0;

//     if($year!='' && $day!='' && $month!=''){
//         $indexReportRecords = DB::table('indexReportRecords')
//             ->select('indexReportRecords.total_count')
//             ->where('indexReportRecords.orgId',$orgId)   
//             ->where(DB::raw("(DATE_FORMAT(indexReportRecords.date,'%Y-%m-%d'))"),$year.'-'.$month.'-'.$day);
//     }else if($year!='' && $day=='' && $month!=''){
//         $indexReportRecords = DB::table('indexReportRecords')
//             ->select('indexReportRecords.total_count')
//             ->where('indexReportRecords.orgId',$orgId)   
//             ->where(DB::raw("(DATE_FORMAT(indexReportRecords.date,'%Y-%m'))"),$year.'-'.$month);
//     }else{
//         $indexReportRecords = DB::table('indexReportRecords')
//             ->select('indexReportRecords.total_count')
//             ->where('indexReportRecords.orgId',$orgId)   
//             ->where(DB::raw("(DATE_FORMAT(indexReportRecords.date,'%Y'))"),$year);
//     }

//     /*
//     if($officeId){
//         $indexReportRecords->where('officeid',$officeId);
//     }   
//     if($departmentId){
//         $indexReportRecords->where('depid',$departmentId);
//     } 
//     */      
//     if (!empty($officeId) && empty($departmentId)) {
//         $indexReportRecords->Join('departments','departments.id','indexReportRecords.departmentId');
//         $indexReportRecords->Join('all_department','all_department.id','departments.departmentId');
//         $indexReportRecords->where('all_department.status','Active');
//         $indexReportRecords->where('departments.status','Active');
//         $indexReportRecords->where('indexReportRecords.officeId',$officeId);
//     }elseif (!empty($officeId) && !empty($departmentId)) {
//         $indexReportRecords->Join('departments','departments.id','indexReportRecords.departmentId');
//         $indexReportRecords->Join('all_department','all_department.id','departments.departmentId');
//         $indexReportRecords->where('all_department.status','Active');
//         $indexReportRecords->where('departments.status','Active');
//         $indexReportRecords->where('indexReportRecords.officeId',$officeId);
//         $indexReportRecords->where('indexReportRecords.departmentId',$departmentId);
//     }elseif (empty($officeId) && !empty($departmentId)) {
//         $indexReportRecords->where('all_department.status','Active');
//         $indexReportRecords->where('departments.status','Active');
//         $indexReportRecords->Join('departments','departments.id','indexReportRecords.departmentId');
//         $indexReportRecords->Join('all_department','all_department.id','departments.departmentId');
//         $indexReportRecords->where('departments.departmentId',$departmentId);
//     }

//     $indexRecords = $indexReportRecords->get()->toArray();  
//     $c=0;
//     if($indexRecords){
//         foreach($indexRecords as $indexRecord){
//              if($year!='' && $day!='' && $month!=''){
//                 //
//              }else{
//                 if($indexRecord->total_count>0){
//                     $c++;
//                 }
//              }
//             $totalIndex+=$indexRecord->total_count;
//         }
//     }
//     if($c>0){
//         $totalIndex = number_format($totalIndex/$c);    
//         if($totalIndex>1000){
//             return 1000;
//         }else{
//              return $totalIndex;
//         }
//     }else{
//         if($totalIndex>1000){
//             return 1000;
//         }else{
//              return $totalIndex;
//         }
//     }
    
// }


public function show(Request $request,$id){
    $orgId     = base64_decode($id);
    $officeId  = $request->officeId;
    $departmentId = $request->departmentId;

    $org = DB::table('organisations')->select('id','organisation','ImageURL')->where('id',$orgId)->first();

    $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get();

    $dots = DB::table('dots')->where('orgId',$orgId)->first();

    $all_department = DB::table('all_department')->where('status','Active')->get();

    $departments = [];

    if (!empty($officeId)) {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }else{
        if(!empty($departmentId)){
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    //---------------------------------Index Graph-------------------------------------
    //Get Year used
    $yearUsed = DB::table('indexReportRecords')
                ->select(DB::raw('YEAR(date) usingyear'))    
                ->where('orgId',$orgId)
                ->whereNotNull('date')->distinct()
                ->get()->toArray();

    //Get index graph values
    $currentMonth = '';
    $currentYear  = '';
    $graphVal     = '';

    $currentMonth = $request->currentMonth;
    $currentYear  = $request->selectYear;  
  
    //make var
    $orgArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId);


    //Month of Year wise
    if($currentYear!='' && $currentMonth==''){    //Month of Year wise
        $showGraph=1;
        //$month = ['Index','Index'];
        $jan = ['Jan',$this->sumOfIndex($currentYear,'01',$orgArr,'')];  
        $feb = ['Feb',$this->sumOfIndex($currentYear,'02',$orgArr,'')];  
        $mar = ['March',$this->sumOfIndex($currentYear,'03',$orgArr,'')];  
        $apr = ['Apr',$this->sumOfIndex($currentYear,'04',$orgArr,'')];    
        $may = ['May',$this->sumOfIndex($currentYear,'05',$orgArr,'')];   
        $jun = ['Jun',$this->sumOfIndex($currentYear,'06',$orgArr,'')];    
        $jul = ['July',$this->sumOfIndex($currentYear,'07',$orgArr,'')];   
        $aug = ['Aug',$this->sumOfIndex($currentYear,'08',$orgArr,'')];     
        $sep = ['Sep',$this->sumOfIndex($currentYear,'09',$orgArr,'')];    
        $oct = ['Oct',$this->sumOfIndex($currentYear,'10',$orgArr,'')];  
        $nov = ['Nov',$this->sumOfIndex($currentYear,'11',$orgArr,'')];      
        $dec = ['Dec',$this->sumOfIndex($currentYear,'12',$orgArr,'')];  

        if ($currentYear.'-01' <= date('Y-m')){              
            $mainArray  = [$jan];
        }   
        if ($currentYear.'-02' <= date('Y-m')){              
            $mainArray  = [$jan,$feb];
        }  
        if ($currentYear.'-03' <= date('Y-m')){              
            $mainArray  = [$jan,$feb,$mar];
        }
        if ($currentYear.'-04' <= date('Y-m')){            
            $mainArray  = [$jan,$feb,$mar,$apr];
        }  
        if ($currentYear.'-05' <= date('Y-m')){            
            $mainArray  = [$jan,$feb,$mar,$apr,$may];
        }  
        if ($currentYear.'-06' <= date('Y-m')){            
            $mainArray  = [$jan,$feb,$mar,$apr,$may,$jun];
        }   
        if ($currentYear.'-07' <= date('Y-m')){           
            $mainArray  = [$jan,$feb,$mar,$apr,$may,$jun,$jul];
        }  
        if ($currentYear.'-08' <= date('Y-m')){             
            $mainArray  = [$jan,$feb,$mar,$apr,$may,$jun,$jul,$aug];
        }    
        if ($currentYear.'-09' <= date('Y-m')){           
            $mainArray  = [$jan,$feb,$mar,$apr,$may,$jun,$jul,$aug,$sep];
        } 
        if ($currentYear.'-10' <= date('Y-m')){            
            $mainArray  = [$jan,$feb,$mar,$apr,$may,$jun,$jul,$aug,$sep,$oct];
        }           
        if ($currentYear.'-11' <= date('Y-m')){           
            $mainArray  = [$jan,$feb,$mar,$apr,$may,$jun,$jul,$aug,$sep,$oct,$nov];
        }
        if ($currentYear.'-12' <= date('Y-m')){             
            $mainArray  = [$jan,$feb,$mar,$apr,$may,$jun,$jul,$aug,$sep,$oct,$nov,$dec];
        }
        $graphVal = $mainArray;
        //print_r($graphVal);die;
        //$graphVal  = json_encode($mainArray);
        //$graphVal = str_replace('"', "'", $graphVal);
        //echo $graphVal;die;

    }else if($currentYear!='' && $currentMonth!=''){   //Days wise
        $showGraph=2;
        //Days Graph shows
        $no_of_days = date('t',mktime(0,0,0,$currentMonth,1,$currentYear));
        $graphVal=[];
        for ($day = 1; $day <= $no_of_days; $day++) {

            //$rowDate = $currentYear."-".$currentMonth."-".$day;
            if($day>10 || $day==10){
                $rowDate = $currentYear."-".$currentMonth."-".$day;
            }else{
                $rowDate = $currentYear."-".$currentMonth."-0".$day;
            }
            $dateFormat = date('M/d',strtotime($rowDate));            
            // if($day>=10){
            //     $day= $day;
            // }else{
            //     $day = "0".$day;
            // }

            if ($rowDate < date('Y-m-d')){    
                $graphVal[$dateFormat] = $this->sumOfIndex($currentYear,$currentMonth,$orgArr,$day);
            }

        }
        //print_r($graphVal);die;
    }else{    //Year wise
        $showGraph=3;
        $graphVal=[];
        if($yearUsed){
            foreach($yearUsed as $allyears){
                $year = $allyears->usingyear;
                $year = $year;
                $graphVal[$year] = $this->sumOfIndex($year,'',$orgArr,'');
            }
        }
        //print_r($graphVal);die;   
    }
    //--------------------------------Index Graph Close---------------------------------    


    /*DOT report*/
    $dotValuesArray = array();
    $beliefList     = array();

    if(!empty($dots))
    {
        //for search
        $searchBeliefId = $request->beliefId;
        session()->put('searchBeliefId', $searchBeliefId);
        $beliefWr = array();
        if($searchBeliefId)
        {
            $beliefWr['id'] = $searchBeliefId;
        }

        $dotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->where($beliefWr)
        ->orderBy('id','ASC')
        ->get();

        $beliefList = DB::table('dots_beliefs')->where('status','Active')->where('dotId',$dots->id)->get();

        $startDate = Input::get('startDate');
        $endDate   = Input::get('endDate');

        foreach ($dotBeliefs as $key => $bValue)
        {

            $dotValues = DB::table('dots_values')
            ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
            ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
            ->where('dots_values.status','Active')
            ->where('dots_values.beliefId',$bValue->id)           
            ->orderBy('dot_value_list.name','ASC')
            ->get();

            $bquery = DB::table('dot_values_ratings')
            ->leftjoin('users','users.id','=','dot_values_ratings.userId')
            ->where('dot_values_ratings.beliefId',$bValue->id)
            ->where('users.status','Active');
            
            $startDate = Input::get('startDate');
            $endDate   = Input::get('endDate');
            //filter acc. to start and end date
            if(!empty($startDate) && !empty($endDate)) 
            {

              $startDate = date_create($startDate);
              $startDateMonth = date_format($startDate,"Y-m-d");
              $endDate = date_create($endDate);

              $endDateYear = date_format($endDate,"Y-m-d");

              $bquery->where('users.created_at', '>=',$startDateMonth);
              $bquery->where('users.created_at', '<=',$endDateYear);

            }
            //belief rating
              $bRatings = $bquery->avg('ratings');
              
              $valuesArray = array();
              foreach ($dotValues as $key => $vValue) 
              {
                $query = DB::table('dot_values_ratings')
                ->leftjoin('users','users.id','=','dot_values_ratings.userId')
                ->where('users.status','Active')
                ->where('valueId', $vValue->id)
                ->where('beliefId',$bValue->id)
                ->where('dotId', $dots->id);

                $startDate = Input::get('startDate');
                $endDate   = Input::get('endDate');

                //filter acc. to start and end date
                if(!empty($startDate) && !empty($endDate)) 
                {

                  $startDate = date_create($startDate);
                  $startDateMonth = date_format($startDate,"Y-m-d");

                  $endDate = date_create($endDate);

                  $endDateYear = date_format($endDate,"Y-m-d");

                  $query->where('users.created_at', '>=',$startDateMonth);
                  $query->where('users.created_at', '<=',$endDateYear);
              }
              
              /*print_r($query->toSql());
              print_r($query->getBindings());
              die();*/
              $vRatings = $query->avg('ratings');

             /* echo " V ";
             print_r($vRatings);*/

             $vResult['valueId']      = $vValue->id;
             $vResult['valueName']    = ucfirst($vValue->name);

             $vResult['valueRatings'] = 0;
             if($vRatings)
             {
                $vResult['valueRatings'] = $vRatings-1;
            }
            array_push($valuesArray, $vResult);
        }
        // die();

        $result['beliefId']      = $bValue->id;
        $result['beliefName']    = ucfirst($bValue->name);           

        $result['beliefRatings'] = 0;
        if($bRatings)
        {
            $result['beliefRatings'] = $bRatings-1;
        }

        $result['beliefValues']  = $valuesArray;

                //filter by office 
        if(!empty($officeId)) {

            $bRatingsQuery = DB::table('dot_values_ratings')
            ->leftjoin('users','users.id','=','dot_values_ratings.userId')
            ->where('users.status','Active')
            ->where('dot_values_ratings.status','Active')
            ->where('dot_values_ratings.beliefId', $bValue->id);
            $bRatingsQuery->where('users.officeId', $officeId);

            $startDate = Input::get('startDate');
            $endDate   = Input::get('endDate');

                    //filter acc. to start and end date
            if(!empty($startDate) && !empty($endDate)) 
            {

              $startDate = date_create($startDate);
              $startDateMonth = date_format($startDate,"Y-m-d");

              $endDate = date_create($endDate);

              $endDateYear = date_format($endDate,"Y-m-d");

              $bRatingsQuery->where('users.created_at', '>=',$startDateMonth);
              $bRatingsQuery->where('users.created_at', '<=',$endDateYear);

          }

          $isofficeRating = $bRatingsQuery->avg('ratings');

          if($isofficeRating)
          {
            array_push($dotValuesArray, $result);
        }
    }else{
        array_push($dotValuesArray, $result);
    }

    }
          //belief for loop

    $startDate = Input::get('startDate');
    $endDate   = Input::get('endDate');

                //filter acc. to start and end date
    if(!empty($startDate) && !empty($endDate)) {
        $startDate = date_create($startDate);
        $startDateMonth = date_format($startDate,"Y-m-d");            
        $endDate = date_create($endDate);
        $endDateYear = date_format($endDate,"Y-m-d");

        $isUser = DB::table('users')
        ->where('orgId',$orgId)
        ->where('status','Active')
        ->where('created_at', '>=', $startDateMonth)           
        ->where('created_at', '<=', $endDateYear)
        ->count();

        if(empty($isUser)){
            $dotValuesArray = array();
        }
    }
}



   //check DOT rating is given by user on every dot value if it is yes then true

    $usersQuery = DB::table('users')->where('users.status','Active')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId');
    if(!empty($orgId)){
        $usersQuery->where('users.orgId',$orgId);
    }

    if (!empty($officeId) && empty($departmentId)) {
        $usersQuery->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $usersQuery->where('users.officeId',$officeId);
        $usersQuery->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $usersQuery->where('departments.departmentId',$departmentId);
    }

    $users = $usersQuery->get();
    $userCount = count($users);


    $dotValueRatingCompletedUserArr = array();
    foreach($users as $duValue){
        $dotPerArr = array('orgId' => $orgId,'userId'=>$duValue->id,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate'=>'');

        //$dotPerArr = array('orgId' => $orgId, 'userId' => $duValue->id, 'updateAt' => '', 'currentDate' => '', 'month' => '', 'year' => '');
        $status = $this->indexReportForDotCompletedValues($dotPerArr);
        //$status = app('App\Http\Controllers\Admin\AdminPerformanceController')->getDotPerformance($dotPerArr);
        if($status){
            array_push($dotValueRatingCompletedUserArr, $duValue->id);      
        }

    }
    $dotValueRatingCompletedUserArrCount = count($dotValueRatingCompletedUserArr);

    $dotCompleted = "0";
    if(!empty($dotValueRatingCompletedUserArrCount) && !empty($userCount)){
        $dotCompleted = ($dotValueRatingCompletedUserArrCount/$userCount)*100;
    }

    //for team role map complete
    $teamPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate'=>'');
    //$teamPerArr = array('orgId' => $orgId,'updateAt'=>'','currentDate'=>'','month'=>'','year'=>'');
    $teamRoleStatus = $this->indexReportForTeamroleCompleted($teamPerArr);
    //$teamRoleStatus = app('App\Http\Controllers\Admin\AdminPerformanceController')->getTeamRoleMap($teamPerArr);

    $temRoleMapCount = count($teamRoleStatus);

    $tealRoleMapCompleted = "0";
    if (!empty($temRoleMapCount) && !empty($userCount)){
        $tealRoleMapCompleted = ($temRoleMapCount/$userCount)*100;
    }

    //for personality type completed
    $personalityPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate'=>'');
    //$personalityPerArr = array('orgId' => $orgId,'updateAt'=>'','currentDate'=>'','month'=>'','year'=>'');
    $personalityTypeStatus = $this->indexReportForpersonalityTypeCompleted($personalityPerArr);
    //$personalityTypeStatus = app('App\Http\Controllers\Admin\AdminPerformanceController')->getPersonalityType($personalityPerArr);

    $personalityTypeCount = count($personalityTypeStatus);

    $personalityTypeCompleted = "0";
    if (!empty($personalityTypeCount) && !empty($userCount)){
        $personalityTypeCompleted = ($personalityTypeCount/$userCount)*100;
    }

    //for culture structure comleted        
    $cultureStructurePerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate'=>'');
    $cultureStructureStatus = $this->indexReportForCultureStructureCompleted($cultureStructurePerArr);
    //$cultureStructureStatus = app('App\Http\Controllers\Admin\AdminPerformanceController')->getCultureStructure($cultureStructurePerArr);

    $cultureStructureCount = count($cultureStructureStatus);

    $cultureStructureCompleted = "0";
    if (!empty($cultureStructureCount) && !empty($userCount)){
        $cultureStructureCompleted = ($cultureStructureCount/$userCount)*100;
    }

    //for motivation comleted       
    $motivationPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate'=>'');
    //$motivationPerArr = array('orgId' => $orgId,'updateAt'=>'','currentDate'=>'','month'=>'','year'=>'');
    $motivationStatus = $this->indexReportForMotivationCompleted($motivationPerArr);
    //$motivationStatus = app('App\Http\Controllers\Admin\AdminPerformanceController')->getMotivation($motivationPerArr);

    $motivationCount = count($motivationStatus);

    $motivationCompleted = "0";
    if (!empty($motivationCount) && !empty($userCount)){
        $motivationCompleted = ($motivationCount/$userCount)*100;
    }

    //Thumbsup Completed for yesterday
    $thumbsupPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate'=>'');
    $thumbsupCount = $this->indexReportForThumbsupCompleted($thumbsupPerArr);

    $thumbCompleted = "0";
    if(!empty($thumbsupCount) && !empty($userCount)){
        $thumbCompleted = ($thumbsupCount/$userCount)*100;
    }

    /*get previous day happy index detail*/

    $happyIndexPerArr = array('orgId' => $orgId,'officeId'=>$officeId,'departmentId'=>$departmentId,'getByDate'=>'');
    $happyIndexCompleted = $this->indexReportForHappyIndexCompleted($happyIndexPerArr);



    //Tribeometer and diagnostics completed last month by users
    $diagnosticAnsCompletedUserArr = array();
    $tribeometerAnsCompletedUserArr = array();
    foreach($users as $duValue){

        $isDiagnosticAnsDoneQuery  = DB::table('diagnostic_answers')
        ->leftjoin('users','users.id','diagnostic_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('diagnostic_answers.userId',$duValue->id)
        ->where('diagnostic_answers.orgId',$orgId)
        ->where('diagnostic_answers.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('diagnostic_answers.updated_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        // ->where('diagnostic_answers.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        //->whereMonth('diagnostic_answers.created_at',Carbon::now()->subMonth()->month);

        if (!empty($officeId) && empty($departmentId)) {
            $isDiagnosticAnsDoneQuery->where('users.officeId',$officeId);
        }elseif (!empty($officeId) && !empty($departmentId)) {
            $isDiagnosticAnsDoneQuery->where('users.officeId',$officeId);
            $isDiagnosticAnsDoneQuery->where('users.departmentId',$departmentId);
        }elseif (empty($officeId) && !empty($departmentId)) {
            $isDiagnosticAnsDoneQuery->where('departments.departmentId',$departmentId);
        }  
        $isDiagnosticAnsDone = $isDiagnosticAnsDoneQuery->first();



        $isTribeometerAnsDoneQuery = DB::table('tribeometer_answers')
        ->leftjoin('users','users.id','tribeometer_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('tribeometer_answers.userId',$duValue->id)
        ->where('tribeometer_answers.orgId',$orgId)
        ->where('tribeometer_answers.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('tribeometer_answers.updated_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        // ->where('tribeometer_answers.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
        //->whereMonth('tribeometer_answers.created_at',Carbon::now()->subMonth()->month);


        if (!empty($officeId) && empty($departmentId)) {
            $isTribeometerAnsDoneQuery->where('users.officeId',$officeId);
        }elseif (!empty($officeId) && !empty($departmentId)) {
            $isTribeometerAnsDoneQuery->where('users.officeId',$officeId);
            $isTribeometerAnsDoneQuery->where('users.departmentId',$departmentId);
        }elseif (empty($officeId) && !empty($departmentId)) {
            $isTribeometerAnsDoneQuery->where('departments.departmentId',$departmentId);
        } 
        $isTribeometerAnsDone = $isTribeometerAnsDoneQuery->first();

        if(!empty($isDiagnosticAnsDone)){
            array_push($diagnosticAnsCompletedUserArr, $duValue->id);      
        }
        if($isTribeometerAnsDone)
        {
            array_push($tribeometerAnsCompletedUserArr, $duValue->id);      
        }
    }

    $diagnosticAnsCompletedUserArrCount  = count($diagnosticAnsCompletedUserArr);
    $tribeometerAnsCompletedUserArrCount = count($tribeometerAnsCompletedUserArr);


    $diagnosticAnsCompleted = "0";
    if(!empty($diagnosticAnsCompletedUserArrCount) && !empty($userCount))
    {
        $diagnosticAnsCompleted = ($diagnosticAnsCompletedUserArrCount/$userCount)*100;
    }
    $tribeometerAnsCompleted = "0";
    if(!empty($tribeometerAnsCompletedUserArrCount) && !empty($userCount))
    {
        $tribeometerAnsCompleted = ($tribeometerAnsCompletedUserArrCount/$userCount)*100;
    }

   
    //All average ratings for values by users
    $valuesPerformance = 0;
    if ($dots) {
        $allDotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->orderBy('id','ASC')
        ->get();
        $totalBeliefRatings = 0;
        $dotValuesCount     = 0;
        foreach ($allDotBeliefs as $key => $bValue){
            $dotValues = DB::table('dots_values')
            ->where('dots_values.status','Active')
            ->where('dots_values.beliefId',$bValue->id)           
            ->count();
            $dotValuesCount += $dotValues;

            $beliefRatingQuery = DB::table('dot_values_ratings')
            ->leftjoin('users','users.id','dot_values_ratings.userId')
            ->leftJoin('departments','departments.id','users.departmentId')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('dot_values_ratings.beliefId',$bValue->id)
            ->where('users.status','Active')
            ->where('departments.status','Active')
            ->where('all_department.status','Active')
            ->where('dot_values_ratings.status','Active');

            if (!empty($officeId) && empty($departmentId)) {
                $beliefRatingQuery->where('users.officeId',$officeId);
            }elseif (!empty($officeId) && !empty($departmentId)) {
                $beliefRatingQuery->where('users.officeId',$officeId);
                $beliefRatingQuery->where('users.departmentId',$departmentId);
            }elseif (empty($officeId) && !empty($departmentId)) {
                $beliefRatingQuery->where('departments.departmentId',$departmentId);
            } 
            $beliefRating = $beliefRatingQuery->avg('ratings');
            if($beliefRating){
                $totalBeliefRatings += $beliefRating-1;
            }
        }
        $avgRatings         = $totalBeliefRatings;
        $valuesPerformance  = (($avgRatings/$dotValuesCount)/5)*100;
    }


    //(Total feedback count /All active users count of organisation) /(no of month from the day of creation of first staff till now) *500

    $firstStaffOfOrg = DB::table('users')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    if($firstStaffOfOrg){
        $firstDateOfOrg  = date('Y-m',strtotime($firstStaffOfOrg->created_at));
        $lastMonthDate   = date('Y-m');

       // $firstDateOfOrg  = "2019-02-10"; // always small
       // $lastMonthDate   = "2019-11-12";



        $time1 = strtotime($firstDateOfOrg);  // always small
        $time2 = strtotime($lastMonthDate);
        if(date('m', $time1)==date('m', $time2) && date('Y', $time1)==date('Y', $time2)){
          $noOfPreviousMonths = 1;
        }else{
          if ($time1 < $time2){
            $my = date('mY', $time2);
            $noOfPreviousMonths = array(date('F', $time1));
            while($time1 < $time2) {
              $time1 = strtotime(date('Y-m-d', $time1).' +1 month');
              if(date('mY', $time1) != $my && ($time1 < $time2))
                $noOfPreviousMonths[] = date('F', $time1);
            }
            $noOfPreviousMonths[] = date('F', $time2);
            //print_r($noOfPreviousMonths);die;
            $noOfPreviousMonths   = count($noOfPreviousMonths);                    
          }else{
            $noOfPreviousMonths = 0;
          } 
        }
    }else{
        $noOfPreviousMonths = 0;     
    }

    //echo "D1->".$firstDateOfOrg." <br />D2->".$lastMonthDate." <br />Total ".$noOfPreviousMonths;die;

    //$firstStaffOrg = DB::table('offices')->where('orgId',$orgId)->orderBy('created_at','ASC')->first();
    //$date1 = new DateTime($firstStaffOrg->created_at);
    //$date2 = $date1->diff(new DateTime(Carbon::today()));
    //$totalMonthcount = ($date2->y*12) + $date2->m + 2;

    //Number of reported incidents and get details of feedback of last month
    $iotFeedbackQuery = DB::table('iot_feedbacks')
    ->select('iot_feedbacks.id')
    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->where('iot_feedbacks.status','!=','Inactive')
    ->where('iot_feedbacks.orgId',$orgId);
    //->where('iot_feedbacks.updated_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
    // ->where('iot_feedbacks.created_at','LIKE',date("Y-m", strtotime("-1 month"))."%");
    //->whereMonth('iot_feedbacks.created_at',Carbon::now()->subMonth()->month);

    if (!empty($officeId) && empty($departmentId)) {
        $iotFeedbackQuery->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $iotFeedbackQuery->where('users.officeId',$officeId);
        $iotFeedbackQuery->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $iotFeedbackQuery->where('departments.departmentId',$departmentId);
    } 
    $iotFeedback = $iotFeedbackQuery->count();
    $feedbacks = 0;
    if (!empty($iotFeedback) && !empty($userCount)) {
        //$feedbacks = round((($iotFeedback/$userCount)/$totalMonthcount*500),2);

        //(Total number of feedback / Number of staff)/number of months client live *500
        //$feedbacks = ($iotFeedback/$userCount)/($noOfPreviousMonths*500);
        $feedbacks = (($iotFeedback/$userCount)/($noOfPreviousMonths))*500;
    }






    // /print_r($feedbacks);die();

    //Direction and connection of tribeometer
    $usersForTribeQuery = DB::table('tribeometer_answers')
    ->leftjoin('users','users.id','tribeometer_answers.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->select('tribeometer_answers.userId')
    ->where('users.status','Active')
    ->where('departments.status','Active')
    ->where('all_department.status','Active')
    ->groupBy('tribeometer_answers.userId')
    ->where('users.orgId',$orgId);

    if (!empty($officeId) && empty($departmentId)) {
        $usersForTribeQuery->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $usersForTribeQuery->where('users.officeId',$officeId);
        $usersForTribeQuery->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $usersForTribeQuery->where('departments.departmentId',$departmentId);
    } 
    $usersForTribe = $usersForTribeQuery->get();
    $userCountForTribe = count($usersForTribe);

    //$tribeometerResultArray = array();
    $queCatTbl    = DB::table('tribeometer_questions_category')->get();
    $tribeDirectionnConnection = 0;
    if (!empty($userCountForTribe)){
        $optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
        $optCount = count($optionsTbl)-1; 

        foreach ($queCatTbl as $value){
            $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();
            $quecount = count($questionTbl);

            $perQuePercen = 0;
            foreach ($questionTbl as  $queValue){
                $diaQuery = DB::table('tribeometer_answers')           
                ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
                ->leftjoin('users','users.id','=','tribeometer_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.status','Active') 
                ->where('departments.status','Active') 
                ->where('all_department.status','Active') 
                ->where('tribeometer_answers.orgId',$orgId)
                ->where('tribeometer_questions.id',$queValue->id)
                ->where('tribeometer_questions.category_id',$value->id);

                if (!empty($officeId) && empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                }
                elseif (!empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                    $diaQuery->where('users.departmentId',$departmentId);
                }
                elseif (empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('departments.departmentId',$departmentId);
                } 
                $diaAnsTbl = $diaQuery->sum('answer');

                //avg of all questions
                $perQuePercen += ($diaAnsTbl/$userCountForTribe); 
            }
            $score = ($perQuePercen/($quecount*$optCount));
            $totalPercentage = (($perQuePercen/($quecount*$optCount))*100)*4;
            $tribeDirectionnConnection += $totalPercentage;

            // $value1['title']      =  $value->title;
            // $value1['score']      =  number_format((float)$score, 2, '.', '');
            // $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');            //array_push($tribeometerResultArray, $value1);
        }
    }
    //print_r($tribeDirectionnConnection);die;


    //Diagnostic core 
    //$diagnosticResultArray = array();
    $diagnosticQueCatTbl    = DB::table('diagnostic_questions_category')->where('id','!=',5)->get();

    $diagQuery = DB::table('diagnostic_answers')
    ->leftjoin('users','users.id','diagnostic_answers.userId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->select('diagnostic_answers.userId')
    ->where('users.status','Active')
    ->where('departments.status','Active') 
    ->where('all_department.status','Active') 
    ->groupBy('diagnostic_answers.userId')
    ->where('users.orgId',$orgId);

    if (!empty($officeId) && empty($departmentId)) {
        $diagQuery->where('users.officeId',$officeId);
    }elseif (!empty($officeId) && !empty($departmentId)) {
        $diagQuery->where('users.officeId',$officeId);
        $diagQuery->where('users.departmentId',$departmentId);
    }elseif (empty($officeId) && !empty($departmentId)) {
        $diagQuery->where('departments.departmentId',$departmentId);
    } 
    $diagUsers      = $diagQuery->get();
    $diagUserCount  = count($diagUsers);
    $totalDiagnosticCore = 0;

    if (!empty($diagUserCount)){
        $diaOptionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
        $diaOptCount    = count($diaOptionsTbl); 

        $diagnosticCorePer = 0;
        foreach ($diagnosticQueCatTbl as $value){
            $diaQuestionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

            $diaQuecount = count($diaQuestionTbl);

            $diaPerQuePercen = 0;
            foreach ($diaQuestionTbl as  $queValue){
                $diaQuery = DB::table('diagnostic_answers')            
                ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
                ->leftjoin('users','users.id','=','diagnostic_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.status','Active')   
                ->where('departments.status','Active') 
                ->where('all_department.status','Active') 
                ->where('diagnostic_answers.orgId',$orgId)
                ->where('diagnostic_questions.id',$queValue->id)
                ->where('diagnostic_questions.category_id',$value->id);

                if (!empty($officeId) && empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                }elseif (!empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                    $diaQuery->where('users.departmentId',$departmentId);
                }elseif (empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('departments.departmentId',$departmentId);
                } 

                $diaAnsTbl = $diaQuery->sum('answer'); 
                $diaPerQuePercen += ($diaAnsTbl/$diagUserCount);               
            }
            //$diaScore = ($diaPerQuePercen/($diaQuecount*$diaOptCount));
            $diaTotalPercentage = ($diaPerQuePercen/($diaQuecount*$diaOptCount))*100;
            // $value1['title']      =  $value->title;
            // $value1['score']      =  number_format((float)$diaScore, 2, '.', '');
            // $value1['percentage'] =  number_format((float)$diaTotalPercentage, 2, '.', '');

            $diagnosticCorePer += $diaTotalPercentage;
            //array_push($diagnosticResultArray, $value1);
        }
        $totalDiagnosticCore = ($diagnosticCorePer/5);
    }
//print_r($totalDiagnosticCore);die;



   //Stress inverse of diagnostic
   // $diagnosticStressArray = array();
    $diagnosticCatTbl = DB::table('diagnostic_questions_category')->where('id',5)->get();
    $totalDiagnosticStress = 0;
    if (!empty($diagUserCount)){
        $diagOptionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
        $diagOptCount = count($diagOptionsTbl); 

        $diagnosticStress = 0;

        foreach ($diagnosticCatTbl as $value){
            $diagQuestionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

            $diagQuecount = count($diagQuestionTbl);
            $diagPerQuePercen = 0;
            foreach ($diagQuestionTbl as  $queValue){
                $diaQuery = DB::table('diagnostic_answers')            
                ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
                ->leftjoin('users','users.id','=','diagnostic_answers.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.status','Active')
                ->where('departments.status','Active') 
                ->where('all_department.status','Active')    
                ->where('diagnostic_answers.orgId',$orgId)
                ->where('diagnostic_questions.id',$queValue->id)
                ->where('diagnostic_questions.category_id',$value->id);

                if (!empty($officeId) && empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                }elseif (!empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('users.officeId',$officeId);
                    $diaQuery->where('users.departmentId',$departmentId);
                }elseif (empty($officeId) && !empty($departmentId)) {
                    $diaQuery->where('departments.departmentId',$departmentId);
                } 

                $diagAnsTbl = $diaQuery->sum('answer'); 

                $diagPerQuePercen += ($diagAnsTbl/$diagUserCount);               
            }

            // $diagScore = ($diagPerQuePercen/($diagQuecount*$diagOptCount));
            $diagTotalPercentage = ($diagPerQuePercen/($diagQuecount*$diagOptCount))*100;

            //$value1['title']      =  $value->title;
            //$value1['score']      =  number_format((float)$diagScore, 2, '.', '');
            // $value1['percentage'] =  number_format((float)$diagTotalPercentage, 2, '.', '');
            $diagnosticStress = (($diagTotalPercentage-100)*(-1))*3;    
            //array_push($diagnosticStressArray, $value1);
        }
        $totalDiagnosticStress = ($diagnosticStress);
    }
//print_r($totalDiagnosticStress);die();





    $releventCompanyScores = $dotCompleted + $tealRoleMapCompleted + $personalityTypeCompleted + $cultureStructureCompleted + $motivationCompleted + $thumbCompleted + $happyIndexCompleted + $diagnosticAnsCompleted + $tribeometerAnsCompleted + $valuesPerformance + $feedbacks + $tribeDirectionnConnection + $totalDiagnosticCore + $totalDiagnosticStress;

    if (!empty($releventCompanyScores)) {
        $indexOrg = round((($releventCompanyScores/2300)*1000),2);
    }else{
        $indexOrg = 0;
    }
    //echo $indexOrg;die;

    // print_r("Final result => ".$indexOrg."<br>");

    // print_r("Dot=>".$dotCompleted."<br>");
    // print_r("team role=>".$tealRoleMapCompleted."<br>");
    // print_r("PT=>".$personalityTypeCompleted."<br>");
    // print_r("CS=>".$cultureStructureCompleted."<br>");
    // print_r("motivation=>".$motivationCompleted."<br>");
    // print_r("Thumpsup=>".$thumbCompleted."<br>");
    // print_r("happy Index=>".$happyIndexCompleted."<br>");
    // print_r("Diagnostic Ans=>".$diagnosticAnsCompleted."<br>");
    // print_r("Tribeometer Ans=>".$tribeometerAnsCompleted."<br>");
    // print_r("Values Performance=>".$valuesPerformance."<br>");
    // print_r("feedbacks=>".$feedbacks."<br>");
    // print_r("Direction and connection=>".$tribeDirectionnConnection."<br>");
    // print_r("Diagnostic core =>".$totalDiagnosticCore."<br>");
    // print_r("Diagnostic stress =>".$totalDiagnosticStress);
    //die;

    
   
    //for pdf
    if (empty($resquest->pdfStatus)){
        return View('admin/report/manageReport',compact('offices','beliefList','dotValuesArray','org','officeId','indexOrg','all_department','orgId','departments','departmentId','yearUsed','currentYear','currentMonth','graphVal','showGraph'));
    }else{

        return compact('dotValuesArray','org','officeId','indexOrg','orgId','yearUsed','currentYear','currentMonth','graphVal','showGraph');
    }
}


public function dotTabData(Request $request){

    //$date='2018-08-20';
    //$date='2019-11-05';

    $orgId          = base64_decode($request->orgId);
    $departmentId   = Input::get('departmentId');
    $officeId       = Input::get('officeId');
    $beliefId       = Input::get('beliefId');
    //$officeId  = $request->officeId;
    //$departmentId = $request->departmentId;

    //echo $orgId." = ".$officeId." = ".$officeId." = ".$departmentId;die;

    //Get info of org
    $org = DB::table('organisations')
                ->select('id','organisation','ImageURL')
                ->where('id',$orgId)->first();

    //Get offices of org            
    $offices = DB::table('offices')
                ->where('status','Active')
                ->where('orgId',$orgId)->get();

    //Get dots of org            
    $dots = DB::table('dots')
            ->where('orgId',$orgId)->first();

    //Get all deps        
    $all_department = DB::table('all_department')->where('status','Active')->get();

    $departments = [];
    if (!empty($officeId)){
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')
                            ->where('id',$departmentId)
                            ->where('status','Active')->get();
    }else{
        if(!empty($departmentId)){
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }   


      /*DOT report*/
    $dotValuesArray = array();
    $beliefList     = array();

    if(!empty($dots)){
        //for search
        $searchBeliefId = $beliefId;
        session()->put('searchBeliefId', $searchBeliefId);
        $beliefWr = array();
        if($searchBeliefId){
            $beliefWr['id'] = $searchBeliefId;
        }

        $dotBeliefs = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->where($beliefWr)
        ->orderBy('id','ASC')
        ->get();

        $beliefList = DB::table('dots_beliefs')
        ->where('status','Active')
        ->where('dotId',$dots->id)
        ->get();

        $startDate = Input::get('startDate');
        $endDate   = Input::get('endDate');


        foreach ($dotBeliefs as $key => $bValue){
            $dotValues = DB::table('dots_values')
                ->select('dots_values.id','dot_value_list.name','dots_values.beliefId')
                ->leftjoin('dot_value_list','dot_value_list.id','dots_values.name')
                ->where('dots_values.status','Active')
                ->where('dots_values.beliefId',$bValue->id)           
                ->orderBy('dot_value_list.name','ASC')
                ->get();

            $bquery = DB::table('dot_values_ratings')
                ->leftjoin('users','users.id','=','dot_values_ratings.userId')
                ->where('dot_values_ratings.beliefId',$bValue->id)
                ->where('users.status','Active');

            //filter acc. to start and end date
            if(!empty($startDate) && !empty($endDate)){
              $startDate        = date_create($startDate);
              $startDateMonth   = date_format($startDate,"Y-m-d");
              $endDate          = date_create($endDate);
              $endDateYear      = date_format($endDate,"Y-m-d");
              $bquery->where('users.created_at', '>=',$startDateMonth);
              $bquery->where('users.created_at', '<=',$endDateYear);
            }
            //belief rating
            $bRatings = $bquery->avg('ratings');              
            $valuesArray = array();
            foreach ($dotValues as $key => $vValue) {
                $query = DB::table('dot_values_ratings')
                    ->leftjoin('users','users.id','=','dot_values_ratings.userId')
                    ->where('users.status','Active')
                    ->where('valueId', $vValue->id)
                    ->where('beliefId',$bValue->id)
                    ->where('dotId', $dots->id);
                    //filter acc. to start and end date
                    if(!empty($startDate) && !empty($endDate)){
                        $startDate        = date_create($startDate);
                        $startDateMonth   = date_format($startDate,"Y-m-d");
                        $endDate          = date_create($endDate);
                        $endDateYear      = date_format($endDate,"Y-m-d");

                        $query->where('users.created_at', '>=',$startDateMonth);
                        $query->where('users.created_at', '<=',$endDateYear);
                    }
              
                  //print_r($query->toSql()); print_r($query->getBindings());  die();
                  $vRatings = $query->avg('ratings');

                 /* echo " V ";
                 print_r($vRatings);*/

                 $vResult['valueId']      = $vValue->id;
                 $vResult['valueName']    = ucfirst($vValue->name);
                 $vResult['valueRatings'] = 0;
                 if($vRatings){
                    $vResult['valueRatings'] = $vRatings-1;
                 }
                 array_push($valuesArray, $vResult);
            }

            $result['beliefId']      = $bValue->id;
            $result['beliefName']    = ucfirst($bValue->name); 
            $result['beliefRatings'] = 0;
            if($bRatings){
                $result['beliefRatings'] = $bRatings-1;
            }
            $result['beliefValues']  = $valuesArray;

        //filter by office 
        if(!empty($officeId)){
            $bRatingsQuery = DB::table('dot_values_ratings')
                ->leftjoin('users','users.id','=','dot_values_ratings.userId')
                ->where('users.status','Active')
                ->where('dot_values_ratings.status','Active')
                ->where('dot_values_ratings.beliefId', $bValue->id);
                $bRatingsQuery->where('users.officeId', $officeId);


            $startDate = Input::get('startDate');
            $endDate   = Input::get('endDate');

                    //filter acc. to start and end date
            if(!empty($startDate) && !empty($endDate)) {
                  $startDate        = date_create($startDate);
                  $startDateMonth   = date_format($startDate,"Y-m-d");
                  $endDate          = date_create($endDate);
                  $endDateYear      = date_format($endDate,"Y-m-d");
                  $bRatingsQuery->where('users.created_at', '>=',$startDateMonth);

            }

          $isofficeRating = $bRatingsQuery->avg('ratings');
          if($isofficeRating){
            array_push($dotValuesArray, $result);
          }
        }else{
            array_push($dotValuesArray, $result);
        }

    }
          //belief for loop

        $startDate = Input::get('startDate');
        $endDate   = Input::get('endDate');

        //filter acc. to start and end date
        if(!empty($startDate) && !empty($endDate)){
            $startDate = date_create($startDate);
            $startDateMonth = date_format($startDate,"Y-m-d");            
            $endDate = date_create($endDate);
            $endDateYear = date_format($endDate,"Y-m-d");

            $isUser = DB::table('users')
            ->where('orgId',$orgId)
            ->where('status','Active')
            ->where('created_at', '>=', $startDateMonth)           
            ->where('created_at', '<=', $endDateYear)
            ->count();

            if(empty($isUser)){
                $dotValuesArray = array();
            }
        }
    }

   
    //for pdf
    if (empty($resquest->pdfStatus)){
        return View('admin/report/report_graph/directingGraph',compact('offices','beliefList','dotValuesArray','org','officeId','indexOrg','all_department','orgId','departments','departmentId'));
    }else{
        return compact('dotValuesArray','org','officeId','indexOrg','orgId');
    }
}





//Index report for DOT completed Values
protected function indexReportForDotCompletedValues($perArr = array()){

    $orgId          = $perArr["orgId"];
    $userId         = $perArr["userId"];
    $officeId       = $perArr["officeId"];
    $departmentId   = $perArr["departmentId"];

    $date = $perArr["getByDate"];

    $query = DB::table('dots_values')  
        ->select('dots_values.id AS id')     
        ->leftjoin('dots_beliefs','dots_beliefs.id','dots_values.beliefId')
        ->leftjoin('dots','dots.id','dots_beliefs.dotId')
        ->where('dots_beliefs.status','Active')
        ->where('dots.status','Active')
        ->where('dots_values.status','Active');
    if($orgId){
        $query->where('dots.orgId', $orgId);
    }
    if($date){
        $query->whereDate('dots_values.created_at','<=',$date);
         // $query->where(DB::raw("(DATE_FORMAT(dots_values.created_at,'%Y-%m-%d'))"),"<=",$date);
    }
    $dotValues = $query->get();
    foreach($dotValues as $value){
        $dvValueQuery = DB::table('dot_values_ratings')
            ->rightjoin('users','users.id','dot_values_ratings.userId')
            ->leftJoin('departments','departments.id','users.departmentId')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('users.status','Active')
            ->where('departments.status','Active')
            ->where('all_department.status','Active')
            ->where('dot_values_ratings.userId',$userId)
            ->where('dot_values_ratings.valueId',$value->id)
            ->where('dot_values_ratings.status','Active');
        if($date){
            $dvValueQuery->whereDate('dot_values_ratings.created_at','<=',$date);
            // $dvValueQuery->where(DB::raw("(DATE_FORMAT(dot_values_ratings.created_at,'%Y-%m-%d'))"),"<=",$date);
        }
        if (!empty($officeId) && empty($departmentId)) {
            $dvValueQuery->where('users.officeId',$officeId);
        }
        elseif (!empty($officeId) && !empty($departmentId)) {
            $dvValueQuery->where('users.officeId',$officeId);
            $dvValueQuery->where('users.departmentId',$departmentId);
        }
        elseif (empty($officeId) && !empty($departmentId)) {
            $dvValueQuery->where('departments.departmentId',$departmentId);
        }
        $dvValue = $dvValueQuery->first();
        if(!$dvValue){
            return false;
        }
    }
    //return true;
    if (count($dotValues) == 0) {
      return false;
    }else{
      return true;
    }
    //return true;
}





//Index report for Team Role Completed Values
protected function indexReportForTeamroleCompleted($perArr = array()){
    $orgId          = $perArr['orgId'];
    $officeId       = $perArr['officeId'];
    $departmentId   = $perArr['departmentId'];

    $date = $perArr["getByDate"];
    

    $query = DB::table('cot_answers') 
        ->select('cot_answers.userId')
        ->distinct()
        ->rightjoin('users','users.id','cot_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('cot_answers.status','Active')
        ->where('cot_answers.orgId',$orgId);

    if ($date) {
        $query->whereDate('cot_answers.created_at','<=',$date);
        // $query->where(DB::raw("(DATE_FORMAT(cot_answers.created_at,'%Y-%m-%d'))"),"<=",$date);
    }

    if (!empty($officeId) && empty($departmentId)) {
        $query->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
        $query->where('users.officeId',$officeId);
        $query->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }
    // if($orgId)
    // {
    //     $query->where('cot_answers.orgId',$orgId);
    // }
    $result = $query->get();
    return $result;
}

//Index report for Personality Type Completed Values
protected function indexReportForpersonalityTypeCompleted($perArr = array()){

    $orgId          = $perArr['orgId'];
    $officeId       = $perArr['officeId'];
    $departmentId   = $perArr['departmentId'];

    $date = $perArr["getByDate"];

    $query = DB::table('cot_functional_lens_answers')
        ->select('cot_functional_lens_answers.userId')
        ->distinct()
        ->rightjoin('users','users.id','cot_functional_lens_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('cot_functional_lens_answers.status','Active')
        ->where('cot_functional_lens_answers.orgId',$orgId);

    if($date){
        $query->whereDate('cot_functional_lens_answers.created_at','<=',$date);
        // $query->where(DB::raw("(DATE_FORMAT(cot_functional_lens_answers.created_at,'%Y-%m-%d'))"),"<=",$date);
    }
    if(!empty($officeId) && empty($departmentId)) {
        $query->where('users.officeId',$officeId);
    }elseif(!empty($officeId) && !empty($departmentId)) {
        $query->where('users.officeId',$officeId);
        $query->where('users.departmentId',$departmentId);
    }elseif(empty($officeId) && !empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }
    // if($orgId)
    // {
    //     $query->where('cot_functional_lens_answers.orgId',$orgId);
    // }
    $result = $query->get();
    return $result;
}

//Index report for Culture structure Completed
protected function indexReportForCultureStructureCompleted($perArr = array()){
    $orgId          = $perArr['orgId'];
    $officeId       = $perArr['officeId'];
    $departmentId   = $perArr['departmentId'];

    $date = $perArr["getByDate"];

    //$date='2019-11-08';
    if($date){
        //Get Previos month of date
        $timestamp = strtotime ("-1 month",strtotime ($date));
        //Convert into 'Y-m' format
        $previosMonthFromDate  =  date("Y-m",$timestamp);

        $query = DB::table('sot_answers')   
            ->select('sot_answers.userId')
            ->distinct()      
            ->leftJoin('users','users.id','=','sot_answers.userId')
            ->leftJoin('departments','departments.id','users.departmentId')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('sot_answers.status','Active')
            ->where('users.status','Active')
            ->where('departments.status','Active')
            ->where('all_department.status','Active')
            ->where('sot_answers.updated_at','LIKE',$previosMonthFromDate."%")
            ->where('users.orgId',$orgId);
    }else{
        //Get last month
        $lastMonth = date("Y-m", strtotime("-1 month"));
            //echo $lastMonth;die;
            $query = DB::table('sot_answers')   
            ->select('sot_answers.userId')
            ->distinct()      
            ->leftJoin('users','users.id','=','sot_answers.userId')
            ->leftJoin('departments','departments.id','users.departmentId')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('sot_answers.status','Active')
            ->where('users.status','Active')
            ->where('departments.status','Active')
            ->where('all_department.status','Active')
            ->where('sot_answers.updated_at','LIKE',$lastMonth."%")
            ->where('users.orgId',$orgId);
    }

    if (!empty($officeId) && empty($departmentId)) {
        $query->where('users.officeId',$officeId);
    }
    elseif (!empty($officeId) && !empty($departmentId)) {
        $query->where('users.officeId',$officeId);
        $query->where('users.departmentId',$departmentId);
    }
    elseif (empty($officeId) && !empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }  
    // if($orgId)
    // {
    //     $query->where('users.orgId',$orgId);
    // }
    $result = $query->get();
    return $result;
}

//Index report for motivation Completed
protected function indexReportForMotivationCompleted($perArr = array()){
    $orgId    = $perArr['orgId'];
    $officeId = $perArr['officeId'];
    $departmentId = $perArr['departmentId'];


    $date = $perArr["getByDate"];
    //$date='2019-01-10';

    $query = DB::table('sot_motivation_answers')
        ->select('sot_motivation_answers.userId')
        ->distinct()      
        ->leftJoin('users','users.id','=','sot_motivation_answers.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('sot_motivation_answers.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('users.orgId',$orgId);   

    if($date){
        $query->whereDate('sot_motivation_answers.created_at','<=',$date);
        // $query->where(DB::raw("(DATE_FORMAT(sot_motivation_answers.created_at,'%Y-%m-%d'))"),"<=",$date);
    }

    if (!empty($officeId) && empty($departmentId)) {
        $query->where('users.officeId',$officeId);
    }
    elseif (!empty($officeId) && !empty($departmentId)) {
        $query->where('users.officeId',$officeId);
        $query->where('users.departmentId',$departmentId);
    }
    elseif (empty($officeId) && !empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }  
    // if($orgId)
    // {
    //     $query->where('users.orgId',$orgId);
    // }
    $result = $query->get();
    return $result;
}

//Index report for Thumbsup Completed
protected function indexReportForThumbsupCompleted($perArr = array()){
    $orgId          = $perArr['orgId'];
    $officeId       = $perArr['officeId'];
    $departmentId   = $perArr['departmentId'];

    $date   = $perArr["getByDate"];
    //$date='2019-01-10';

    //echo Carbon::yesterday();die;
    //echo Carbon::yesterday()->format('Y-m-d');die;
    if($date){
        $query = DB::table('dot_bubble_rating_records')
        ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
        ->leftjoin('users','users.id','dot_bubble_rating_records.to_bubble_user_id')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('dots.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('dot_bubble_rating_records.status','Active')
        ->whereDate('dot_bubble_rating_records.created_at',$date);
        // ->where(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),$date);
        //->whereDate('dot_bubble_rating_records.updated_at',Carbon::yesterday());                 
        // ->whereDate('dot_bubble_rating_records.created_at',Carbon::yesterday());                 
    }else{
         $query = DB::table('dot_bubble_rating_records')
        ->leftjoin('dots','dots.id','dot_bubble_rating_records.dot_id')
        ->leftjoin('users','users.id','dot_bubble_rating_records.to_bubble_user_id')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('dots.status','Active')
        ->where('users.status','Active')
        ->where('departments.status','Active')
        ->where('all_department.status','Active')
        ->where('dot_bubble_rating_records.status','Active')
        ->whereDate('dot_bubble_rating_records.created_at',Carbon::today());
        // ->where(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.created_at,'%Y-%m-%d'))"),Carbon::today()->format('Y-m-d'));
        //->where(DB::raw("(DATE_FORMAT(dot_bubble_rating_records.updated_at,'%Y-%m-%d'))"),Carbon::yesterday()->format('Y-m-d'));
        //->whereDate('dot_bubble_rating_records.updated_at',Carbon::yesterday());                 
        // ->whereDate('dot_bubble_rating_records.created_at',Carbon::yesterday());  
    }
    if(!empty($orgId)){
        $query->where('dots.orgId',$orgId);
    }
    if (!empty($officeId) && empty($departmentId)) {
        $query->where('users.officeId',$officeId);
    }
    elseif (!empty($officeId) && !empty($departmentId)) {
        $query->where('users.officeId',$officeId);
        $query->where('users.departmentId',$departmentId);
    }
    elseif (empty($officeId) && !empty($departmentId)) {
        $query->where('departments.departmentId',$departmentId);
    }  
    $bubbleCount = $query->count('bubble_flag');
    return $bubbleCount;
}

//Index report for happy index Completed
protected function indexReportForHappyIndexCompleted($perArr = array()){
    $orgId          = $perArr['orgId'];
    $officeId       = $perArr['officeId'];
    $departmentId   = $perArr['departmentId'];
    $userTypeArr    = array('3','2','1');  //3 for happy and 2 for avrage and 1 for sad

    $totalIndexes = 0;
    $totalUser    = 0;

    $date = $perArr["getByDate"];

    foreach($userTypeArr as $type){
        if($date){
             $happyUserCountQuery = DB::table('happy_indexes')
            ->leftjoin('users','users.id','happy_indexes.userId')
            ->leftJoin('departments','departments.id','users.departmentId')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('happy_indexes.status','Active')
            ->where('happy_indexes.moodValue',$type)
            ->where('departments.status','Active')
            ->where('all_department.status','Active')
            ->where('users.orgId',$orgId)        
            ->whereDate('happy_indexes.created_at',$date);
            // ->where(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),$date);
            //->where(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),Carbon::yesterday()->format('Y-m-d'));
            //->whereDate('happy_indexes.created_at', Carbon::yesterday());
        }else{
            $happyUserCountQuery = DB::table('happy_indexes')
            ->leftjoin('users','users.id','happy_indexes.userId')
            ->leftJoin('departments','departments.id','users.departmentId')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('happy_indexes.status','Active')
            ->where('happy_indexes.moodValue',$type)
            ->where('departments.status','Active')
            ->where('all_department.status','Active')
            ->where('users.orgId',$orgId)      
            ->whereDate('happy_indexes.created_at',Carbon::today());  
            // ->where(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),Carbon::today()->format('Y-m-d'));  
            //->where(DB::raw("(DATE_FORMAT(happy_indexes.created_at,'%Y-%m-%d'))"),Carbon::yesterday()->format('Y-m-d'));
            //->whereDate('happy_indexes.created_at', Carbon::yesterday());
        }

        if (!empty($officeId) && empty($departmentId)) {
            $happyUserCountQuery->where('users.officeId',$officeId);
        }elseif (!empty($officeId) && !empty($departmentId)) {
            $happyUserCountQuery->where('users.officeId',$officeId);
            $happyUserCountQuery->where('users.departmentId',$departmentId);
        }elseif (empty($officeId) && !empty($departmentId)) {
            $happyUserCountQuery->where('departments.departmentId',$departmentId);
        }  

        $happyUserCount = $happyUserCountQuery->count();
        $totalUser +=$happyUserCount;
        if($type==3){
            $totalIndexes += $happyUserCount;
        }       
    }

    $happyIndexcount = 0;
    if($totalIndexes !=0 && $totalUser !=0){ 
      $happyIndexcount = (($totalIndexes/($totalUser))*100);
    }
    return $happyIndexcount;
}

/*get new graph data for Tribeometer*/
public function getReportTribeometerGraph(Request $request){

    $tribeometerResultArray = array();

    $orgId        = base64_decode($request->orgId); 

    $departmentId = Input::get('departmentId');
    $startDate    = Input::get('startDate');
    $endDate      = Input::get('endDate');
    
    $queCatTbl    = DB::table('tribeometer_questions_category')->get();
    $offices      = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
    $officeId     = Input::get('officeId');
    $all_department = DB::table('all_department')->where('status','Active')->get();
    $flag = 0 ;
    $departments  = [];
    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }
    else
    {
        if(!empty($departmentId))
        {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    $query = DB::table('tribeometer_answers')
    ->leftjoin('users','users.id','tribeometer_answers.userId')
    ->select('tribeometer_answers.userId')
    ->where('users.status','Active')
    ->groupBy('tribeometer_answers.userId')
    ->where('users.orgId',$orgId);

    if(!empty($officeId))
    {
        $query->where('users.officeId',$officeId);
    }
    
    //filter acc. to start and end date
    if(!empty($startDate) && !empty($endDate)) 
    {

      $startDate = date_create($startDate);
      $startDateMonth = date_format($startDate,"Y-m-d");

      $endDate = date_create($endDate);

      $endDateYear = date_format($endDate,"Y-m-d");

      $query->where('users.created_at', '>=',$startDateMonth);
      $query->where('users.created_at', '<=',$endDateYear);


  }

  if(!empty($departmentId))
  {   
    $i = 0;

    if($departmentsNew->isEmpty())
    {
        $flag = 1;
    }


    foreach ($departmentsNew as $departments1) 
    {       
        if($i == 0)
        {
            $query->where('users.departmentId',$departments1->id);
        }
        else
        {
            $query->orWhere('users.departmentId',$departments1->id);
        }
        $i++;
    }
}

    // print_r($query->toSql());
    // print_r($query->getBindings());
    // die();
$users = $query->get();
$userCount = count($users);



           // reset if department is not in organisation
if($flag==1)
{
    $userCount = 0;
}

if(!empty($userCount))
{
    $optionsTbl= DB::table('tribeometer_question_options')->where('status','Active')->get();
    $optCount = count($optionsTbl)-1; 

    foreach ($queCatTbl as $value)
    {
        $questionTbl = DB::table('tribeometer_questions')->where('category_id',$value->id)->where('status','Active')->get();

        $quecount = count($questionTbl);

        $perQuePercen = 0;
        foreach ($questionTbl as  $queValue)
        {
            $diaQuery = DB::table('tribeometer_answers')           
            ->leftJoin('tribeometer_questions','tribeometer_questions.id','tribeometer_answers.questionId')
            ->leftjoin('users','users.id','=','tribeometer_answers.userId')
            ->where('users.status','Active') 
            ->where('tribeometer_answers.orgId',$orgId)
            ->where('tribeometer_questions.id',$queValue->id)
            ->where('tribeometer_questions.category_id',$value->id);

            if (!empty($officeId))
            {
                $diaQuery->where('users.officeId',$officeId);                    
            }

            if(!empty($departmentId))
            {   


                if($departmentsNew->isEmpty())
                {
                    $flag = 1;
                }



                $department3 = array();
                foreach ($departmentsNew as $departments1) 
                {       
                    $departments2 = $departments1->id;
                    array_push($department3, $departments2);

                }

                $diaQuery->whereIn('users.departmentId',$department3);
            }

            $diaAnsTbl = $diaQuery->sum('answer');

                //avg of all questions
            $perQuePercen += ($diaAnsTbl/$userCount); 


        }

        $score = ($perQuePercen/($quecount*$optCount));
        $totalPercentage = ($perQuePercen/($quecount*$optCount))*100;


        $value1['title']      =  $value->title;
        $value1['score']      =  number_format((float)$score, 2, '.', '');
        $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');            

        array_push($tribeometerResultArray, $value1);
    }
}


if (empty($request->pdfStatus))
{
 return view('admin/report/report_graph/reportTribeometerGraph',compact('tribeometerResultArray','offices','orgId','officeId','departmentId','departments','all_department'));
} else {
   return compact('tribeometerResultArray');
}
}

/*get doagnostic report graph*/
public function getReportDiagnosticGraph(Request $request)
{

    $diagnosticResultArray = array();

    $orgId        = base64_decode($request->orgId); 
    $departmentId = Input::get('departmentId');
    $startDate    = Input::get('startDate');
    $endDate      = Input::get('endDate');

    $queCatTbl    = DB::table('diagnostic_questions_category')->get();
    $offices      = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
    $officeId     = Input::get('officeId');
    $all_department = DB::table('all_department')->where('status','Active')->get();
    $flag = 0 ;
    $departments = [];
    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

              //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }
    else
    {
        if(!empty($departmentId))
        {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }


    $diagnosticQueCatTbl = DB::table('diagnostic_questions_category')->get();

// $query = DB::table('users')->where('orgId',$orgId)->where('status','Active');

    $query = DB::table('diagnostic_answers')
    ->leftjoin('users','users.id','diagnostic_answers.userId')
    ->select('diagnostic_answers.userId')
    ->where('users.status','Active')
    ->groupBy('diagnostic_answers.userId')
    ->where('users.orgId',$orgId);

    if(!empty($officeId))
    {
        $query->where('users.officeId',$officeId);
    }

    //filter acc. to start and end date
    if(!empty($startDate) && !empty($endDate)) 
    {

      $startDate = date_create($startDate);
      $startDateMonth = date_format($startDate,"Y-m-d");

      $endDate = date_create($endDate);

      $endDateYear = date_format($endDate,"Y-m-d");

      $query->where('users.created_at', '>=',$startDateMonth);
      $query->where('users.created_at', '<=',$endDateYear);


  }

  if(!empty($departmentId))
  {   
    $i = 0;

    if($departmentsNew->isEmpty())
    {
        $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {       
        if($i == 0)
        {
            $query->where('users.departmentId',$departments1->id);
        }
        else
        {
            $query->orWhere('users.departmentId',$departments1->id);
        }
        $i++;
    }
}


$users = $query->get();
$userCount = count($users);

 // reset if department is not in organisation
if($flag==1)
{
    $userCount = 0;
}

if (!empty($userCount))
{

    $optionsTbl  = DB::table('diagnostic_question_options')->where('status','Active')->get();
    $diaOptCount = count($optionsTbl); 

    foreach ($diagnosticQueCatTbl as $value)
    {
        $questionTbl = DB::table('diagnostic_questions')->where('category_id',$value->id)->where('status','Active')->get();

        $quecount = count($questionTbl);

        $perQuePercen = 0;
        foreach ($questionTbl as  $queValue)
        {
            $diaQuery = DB::table('diagnostic_answers')            
            ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
            ->leftjoin('users','users.id','=','diagnostic_answers.userId')
            ->where('users.status','Active')   
            ->where('diagnostic_answers.orgId',$orgId)
            ->where('diagnostic_questions.id',$queValue->id)
            ->where('diagnostic_questions.category_id',$value->id);

            if (!empty($officeId))
            {
                $diaQuery->where('users.officeId',$officeId);                    
            }

            if(!empty($departmentId))
            {   
                $i = 0;

                if($departmentsNew->isEmpty())
                {
                    $flag = 1;
                }
                $department3 = array();
                foreach ($departmentsNew as $departments1) 
                {       
                    $departments2 = $departments1->id;
                    array_push($department3, $departments2);

                }

                $diaQuery->whereIn('users.departmentId',$department3);
            }

            $diaAnsTbl = $diaQuery->sum('answer'); 

            $perQuePercen += ($diaAnsTbl/$userCount);               
        }


        $score = ($perQuePercen/($quecount*$diaOptCount));
        $totalPercentage = ($perQuePercen/($quecount*$diaOptCount))*100;

        $value1['title']      =  $value->title;
        $value1['score']      =  number_format((float)$score, 2, '.', '');
        $value1['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');            

        array_push($diagnosticResultArray, $value1);
    }
}

if (empty($request->pdfStatus)) {

    return view('admin/report/report_graph/reportDiagnosticGraph',compact('diagnosticResultArray','offices','orgId','officeId','departmentId','departments','all_department'));
} else {
    return compact('diagnosticResultArray');
}

}

/*CHART MOTIVATION */
public function getReportMotivationalGraph(Request $request)
{

    $SOTmotivationResultArray = array();

    $orgId        = base64_decode($request->orgId);   
    $officeId     = Input::get('officeId');
    $departmentId = Input::get('departmentId');
    $startDate    = Input::get('startDate');
    $endDate      = Input::get('endDate');


    $categoryTbl   = DB::table('sot_motivation_value_records')->where('status','Active')->get();
    $offices       = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
    $all_department= DB::table('all_department')->where('status','Active')->get();
    $flag = 0 ;
    $departments = [];
    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();


    //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();

    }
    else
    {
        if(!empty($departmentId))
        {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    // $query = DB::table('users')->where('status','Active')->where('orgId',$orgId);

    $query = DB::table('sot_motivation_answers')
    ->leftjoin('users','users.id','sot_motivation_answers.userId')
    ->select('sot_motivation_answers.userId')
    ->where('users.status','Active')
    ->groupBy('sot_motivation_answers.userId')
    ->where('users.orgId',$orgId);

    if(!empty($officeId))
    {
        $query->where('officeId',$officeId);
    }

    $startDate    = (string)Input::get('startDate');
    $endDate      = (string)Input::get('endDate');
//filter acc. to start and end date
    if(!empty($startDate) && !empty($endDate)) 
    {

      $startDate = date_create($startDate);
      $startDateMonth = date_format($startDate,"Y-m-d");

      $endDate = date_create($endDate);

      $endDateYear = date_format($endDate,"Y-m-d");

      $query->where('users.created_at', '>=',$startDateMonth);
      $query->where('users.created_at', '<=',$endDateYear);
  }

  if(!empty($departmentId))
  {   
    $i = 0;

    if($departmentsNew->isEmpty())
    {
        $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {       
        if($i == 0)
        {
            $query->where('users.departmentId',$departments1->id);
        }
        else
        {
            $query->orWhere('users.departmentId',$departments1->id);
        }
        $i++;
    }
} 

$usersList = $query->get();

$totalUser = count($usersList);

    // reset if department is not in organisation
if($flag==1)
{
    $totalUser = 0;
}

if (!empty($totalUser))
{
    foreach ($categoryTbl as $sotCvalue)
    {  
        $query = DB::table('sot_motivation_answers AS sotans')
        ->leftJoin('sot_motivation_question_options AS qoption', 'qoption.id', '=', 'sotans.optionId')
        ->leftjoin('users','users.id','=','sotans.userId')
        ->where('users.status','Active')                     
        ->where('sotans.orgId',$orgId)
        ->where('sotans.status','Active')
        ->where('qoption.category_id',$sotCvalue->id);

        if(!empty($officeId))
        {
         $query->where('users.officeId',$officeId);
     }

     $startDate    = (string)Input::get('startDate');
     $endDate      = (string)Input::get('endDate');
        //filter acc. to start and end date
     if(!empty($startDate) && !empty($endDate)) 
     {

      $startDate = date_create($startDate);
      $startDateMonth = date_format($startDate,"Y-m-d");

      $endDate = date_create($endDate);

      $endDateYear = date_format($endDate,"Y-m-d");

      $query->where('users.created_at', '>=',$startDateMonth);
      $query->where('users.created_at', '<=',$endDateYear);
  }

  if(!empty($departmentId))
  {   

    if($departmentsNew->isEmpty())
    {
        $flag = 1;
    }
    $department3 = array();
    foreach ($departmentsNew as $departments1) 
    {       
        $departments2 = $departments1->id;
        array_push($department3, $departments2);

    }

    $query->whereIn('users.departmentId',$department3);
} 

$ansTbl = $query->sum('sotans.answer'); 

$result1['title']      = utf8_encode($sotCvalue->title);
$result1['percentage'] = number_format((float)($ansTbl/$totalUser), 2, '.', '');          

array_push($SOTmotivationResultArray, $result1);
}
}

if (empty($request->pdfStatus))
{ 
    return view('admin/report/report_graph/sotMotivationUserListGraph',compact('SOTmotivationResultArray','offices','orgId','officeId','departmentId','departments','all_department'));
} else {
    return compact('SOTmotivationResultArray');
}
}

public function getReportCultureGraph(Request $request)
{

    // dd(Input::all());

    $orgId        = base64_decode($request->orgId);  
    $officeId     = Input::get('officeId');
    $departmentId = Input::get('departmentId');

    $all_department = DB::table('all_department')->where('status','Active')->get();
    $flag = 0 ;
    $departments = [];
    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

           //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();

    }
    else
    {
        if(!empty($departmentId))
        {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    $sotCultureStrTbl = DB::table('sot_culture_structure_records')->select('id','type','title','imgUrl')->where('status','Active')->get();
    $offices      = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();

    $sotCountArray = array();
    $countArr = array();

    foreach ($sotCultureStrTbl as $value) 
    {

        $query = DB::table('sot_answers')
        ->select(DB::raw('sum(score) AS count'))
        ->leftJoin('sot_questionnaire_records','sot_questionnaire_records.id','=','sot_answers.question_id')
        ->join('users','users.id','=','sot_answers.userId')
        ->where('users.status','Active')
        ->where('sot_questionnaire_records.type',$value->type)    
        ->where('users.orgId',$orgId);

        if(!empty($officeId))
        {
         $query->where('users.officeId',$officeId);
     }

     $startDate    = Input::get('startDate');
     $endDate      = Input::get('endDate');

     //filter acc. to start and end date
     if(!empty($startDate) && !empty($endDate)) 
     {

      $startDate = date_create($startDate);
      $startDateMonth = date_format($startDate,"Y-m-d");

      $endDate = date_create($endDate);

      $endDateYear = date_format($endDate,"Y-m-d");

      $query->where('users.created_at', '>=',$startDateMonth);
      $query->where('users.created_at', '<=',$endDateYear);

  }


  if(!empty($departmentId))
  {   
    $i = 0;

    if($departmentsNew->isEmpty())
    {
        $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {       
        if($i == 0)
        {
            $query->where('users.departmentId',$departments1->id);
        }
        else
        {
            $query->orWhere('users.departmentId',$departments1->id);
        }
        $i++;
    }
}       

$SOTCount = $query->first();

$value->SOTCount = $SOTCount->count;

// reset if department is not in organisation
if($flag==1)
{    
    $value->SOTCount = 0;
}

array_push($countArr, $SOTCount->count);
array_push($sotCountArray, $value);

}

if($flag==1)
{
    $sotCountArray  = array();
}

// find maximum value array
$sotCountArray1 = array();

foreach ($sotCultureStrTbl as $value) 
{

    $query = DB::table('sot_answers')
    ->select(DB::raw('sum(score) AS count'))
    ->leftJoin('sot_questionnaire_records','sot_questionnaire_records.id','=','sot_answers.question_id')
    ->leftJoin('users','users.id','=','sot_answers.userId')
    ->where('users.status','Active')
    ->where('sot_questionnaire_records.type',$value->type)    
    ->where('users.orgId',$orgId);

    if(!empty($officeId))
    {
     $query->where('users.officeId',$officeId);
 }

 $startDate    = (string)Input::get('startDate');
 $endDate      = (string)Input::get('endDate');
    //filter acc. to start and end date
 if(!empty($startDate) && !empty($endDate)) 
 {

  $startDate = date_create($startDate);
  $startDateMonth = date_format($startDate,"Y-m-d");

  $endDate = date_create($endDate);

  $endDateYear = date_format($endDate,"Y-m-d");

  $query->where('users.created_at', '>=',$startDateMonth);
  $query->where('users.created_at', '<=',$endDateYear);

}

if(!empty($departmentId))
{   
    $i = 0;
    
    if($departmentsNew->isEmpty())
    {
        $flag = 1;
    }
    $department3 = array();
    foreach ($departmentsNew as $departments1) 
    {       
        $departments2 = $departments1->id;
        array_push($department3, $departments2);

    }

    $query->whereIn('users.departmentId',$department3);
}            

$SOTCount = $query->first();

$value->SOTCount = $SOTCount->count;

if (max($countArr)==$SOTCount->count)
{
  array_push($sotCountArray1, $value);
}        
}

  //detail of culture structure 
$sotStrDetailArr = array();

// reset if department is not in organisation
if($flag==1)
{
    $sotCountArray1 = array();
}

foreach ($sotCountArray1 as $value)
{
    $sotStrDetail = DB::table('sot_culture_structure_records')
    ->where('id',$value->id)
    ->where('status','Active')
    ->first();

    $summary = DB::table('sot_culture_structure_summary_records')
    ->where('type',$value->id)
    ->where('status','Active')
    ->get();

    $value->summary = $summary;

    array_push($sotStrDetailArr, $value);
}

if (empty($request->pdfStatus))
{
    return view('admin/report/report_graph/sotCultureGraph',compact('sotCountArray','sotStrDetailArr','offices','orgId','officeId','departmentId','departments','all_department'));
} else {
    return compact('sotCountArray','sotStrDetailArr');
}

}

/*get percentage of cot functional lens*/
public function getReportFunctionalGraph(Request $request)
{

    $orgId        = base64_decode($request->orgId);    
    $officeId     = Input::get('officeId');
    $departmentId = Input::get('departmentId');
    $startDate    = Input::get('startDate');
    $endDate      = Input::get('endDate');

    $offices        = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
    $all_department = DB::table('all_department')->where('status','Active')->get();
    $flag = 0 ;
    $departments = [];
    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

           //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }
    else
    {
        if(!empty($departmentId))
        {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    $cotFunResultArray = array();

    // $query = DB::table('users')->where('status','Active')->where('orgId',$orgId);

    $query = DB::table('cot_functional_lens_answers')
    ->select('cot_functional_lens_answers.userId AS id')
    ->leftjoin('users','users.id','cot_functional_lens_answers.userId')    
    ->where('users.status','Active')
    ->groupBy('cot_functional_lens_answers.userId')
    ->where('users.orgId',$orgId);

    if(!empty($officeId))
    {
     $query->where('officeId',$officeId);
 }

 //filter acc. to start and end date
 if(!empty($startDate) && !empty($endDate)) 
 {

  $startDate = date_create($startDate);
  $startDateMonth = date_format($startDate,"Y-m-d");

  $endDate = date_create($endDate);

  $endDateYear = date_format($endDate,"Y-m-d");

  $query->where('users.created_at', '>=',$startDateMonth);
  $query->where('users.created_at', '<=',$endDateYear);


}


if(!empty($departmentId))
{   
    $i = 0;
    
    if($departmentsNew->isEmpty())
    {
        $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {
        if($i == 0)
        {
            $query->where('users.departmentId',$departments1->id);
        }
        else
        {
            $query->orWhere('users.departmentId',$departments1->id);
        }
        $i++;
    }
}

$usersList = $query->get();

// reset if department is not in organisation
if($flag==1)
{
    $usersList = array();
}

foreach ($usersList as $key => $userValue)
{

    $variable = app('App\Http\Controllers\Admin\CommonController')->getCotFunLensInitialValue();
    
    $initialValEIArray = array();

    foreach ($variable as $value)
    {

        $value1 = $value[0];
        $value2 = $value[1];

        $countE = DB::table('cot_functional_lens_answers AS cfla')
        ->select('cflqo.option_name AS optionName')
        ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
        ->where('cfla.userId',$userValue->id)->where('cflqo.initial_value_id',$value1)->get();

        $countI = DB::table('cot_functional_lens_answers AS cfla')
        ->select('cflqo.option_name AS optionName')
        ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
        ->where('cfla.userId',$userValue->id)->where('cflqo.initial_value_id',$value2)->get();


        $initialValEI  ='';
        if(count($countE) > count($countI))
        {
          $initialValEI  =  $value1;
      }
      else if (count($countE) < count($countI))
      {
          $initialValEI  =  $value2;    
      }
      else if(count($countE) == count($countI))
      {     
          $initialValEI  =  $value1;
      } 

      array_push($initialValEIArray, $initialValEI);

  }

  $valueTypeArray = array();
  $valueTypeArray['value'] = array();

  if(!empty($initialValEIArray))
  {
      $valueTypeArray = array('value'=>$initialValEIArray);
  }

  $matchValueArr =array();
  for($i=0; $i < count($valueTypeArray['value']); $i++)
  {
    $valuesKey = '';
    $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$valueTypeArray['value'][$i])->first();  

    if(!empty($table))
    {
        $valuesKey = $table->value;
    }

    array_push($matchValueArr, $valuesKey); 
}

//remove last and first string
$tribeMatchValue = json_encode(array_slice($valueTypeArray['value'], 1, -1));

$tribeTipsList = DB::table('cot_functional_lens_tribe_tips_records')->where('value',$tribeMatchValue)->get();

$tribeTipsArray = array();
foreach ($tribeTipsList as $ttvalue)
{
    $tTips['value']   = $ttvalue->value;       
    array_push($cotFunResultArray, $tTips);
}

}

$ST =0;
$SF =0;
$NF =0;
$NT =0;


foreach ($cotFunResultArray as $finalArrayValue)
{ 
 if ($finalArrayValue['value']=='["7","9"]'){

     $ST++;
 }elseif ($finalArrayValue['value']=='["7","10"]'){

     $SF++;
 }elseif ($finalArrayValue['value']=='["8","10"]'){

     $NF++;
 }elseif ($finalArrayValue['value']=='["8","9"]'){

     $NT++;
 }
}

$totalUser = count($usersList);
if (!empty($totalUser))
{
    $stPercent = ($ST/$totalUser)*100;
    $sfPercent = ($SF/$totalUser)*100;
    $nfPercent = ($NF/$totalUser)*100;
    $ntPercent = ($NT/$totalUser)*100;
}
else
{
    $stPercent = 0;
    $sfPercent = 0;
    $nfPercent = 0;
    $ntPercent = 0;
}

$funcLensPercentageArray = array('st'=>round($stPercent, 2),'sf'=>round($sfPercent, 2),'nf'=>round($nfPercent, 2),'nt'=>round($ntPercent, 2));



$tribeTipsListKey = DB::table('cot_functional_lens_tribe_tips_records')->where('status','Active')->orderBy('id','ASC')->get();

$keyNameArray = array();
foreach ($tribeTipsListKey as $value1)
{
    $reportKeyArray = json_decode($value1->value);

    $keyArr = array();
    foreach ($reportKeyArray as $value)
    {
        $valuesKey = '';
        $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$value)->first();  

        if(!empty($table))
        {
          $valuesKey = $table->value;
      }   
      array_push($keyArr, $valuesKey); 
  }
  array_push($keyNameArray, $keyArr);
}

if (empty($request->pdfStatus))
{

    return view('admin/report/report_graph/cotFunctionalLensUserListGraph',compact('funcLensPercentageArray','offices','orgId','officeId','departmentId','departments','all_department','keyNameArray'));
} else {
    return compact('funcLensPercentageArray','keyNameArray');
}

}

public function getFunName($value)
{

  $valuesKey = '';
  $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$value)->first();  
  if(!empty($table))
  {
    $valuesKey = $table->value;
}
return $valuesKey;
}



/*get COT team role mape for report graph*/
public function getCOTteamRoleMapGraph(Request $request)
{

    $finalArray  = array();
    $orgId = base64_decode($request->orgId);

    $offices = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();

    $all_department = DB::table('all_department')->where('status','Active')->get();

    $mapers = DB::table('cot_role_map_options')->where('status','Active')->get();

    $officeId     = Input::get('officeId');
    $departmentId = Input::get('departmentId');
    $startDate    = Input::get('startDate');
    $endDate      = Input::get('endDate');

    $flag = 0 ;

    $departments = [];
    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

   //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }
    else
    {
        if(!empty($departmentId))
        {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    $query = DB::table('cot_answers')
    ->select('users.id','users.orgId','users.name')
    ->leftjoin('users','users.id','cot_answers.userId')
    ->where('users.status','Active')
    ->where('users.orgId',$orgId);

    if(!empty($officeId))
    {
        $query->where('users.officeId',$officeId);
    }

//filter acc. to start and end date
    if(!empty($startDate) && !empty($endDate)) 
    {

      $startDate = date_create($startDate);
      $startDateMonth = date_format($startDate,"Y-m-d");

      $endDate = date_create($endDate);

      $endDateYear = date_format($endDate,"Y-m-d");

      $query->where('users.created_at', '>=',$startDateMonth);
      $query->where('users.created_at', '<=',$endDateYear);


  }

  if(!empty($departmentId))
  {   
    $i = 0;

    if($departmentsNew->isEmpty())
    {
        $flag = 1;
    }
    foreach ($departmentsNew as $departments1) 
    {        
        if($i == 0)
        {
            $query->where('users.departmentId',$departments1->id);
        }
        else
        {
            $query->orWhere('users.departmentId',$departments1->id);
        }

        $i++;
    }
}


$users = $query->get();

$resultArray1 = array();

foreach($users as $value)
{
    $data['id']    = $value->id;
    $data['orgId'] = $value->orgId;
    $data['name']  = $value->name;

    array_push($resultArray1, $data);
}

$users23 = array_unique($resultArray1,SORT_REGULAR);

$finalUser = array();

foreach($users23 as  $value55)
{
    $object = (object)array();    

    $object->id = $value55['id'];
    $object->orgId=$value55['orgId'];
    $object->name=$value55['name'];

    array_push($finalUser,$object);

}
// reset if department is not in organisation
if($flag==1)
{
    $finalUser = array();
}

$cotRoleMapOptions = DB::table('cot_role_map_options')->where('status','Active')->get();

$usersArray = array();
foreach ($finalUser as $key => $value) 
{

  foreach($cotRoleMapOptions as $key => $maper)
  {
    $maperCount = DB::table('cot_answers')
    ->where('orgId',$value->orgId)
    ->where('userId',$value->id)
    ->where('cot_role_map_option_id',$maper->id)->where('status','Active')->sum('answer');

    $resultArray = array();

    $Value[$maper->maper_key] = $maperCount;    

    array_push($resultArray, $Value);
}   

arsort($resultArray[0]);

$i = 0;
$prev = "";
$j = 0;

$new  = array();
$new1 = array();

foreach($resultArray[0] as $key => $val)
{ 
    if($val != $prev)
    { 
      $i++; 
  }

  $new1['title'] = $key; 
  $new1['value'] = $i;        
  $prev = $val;

  if ($key==$cotRoleMapOptions[0]->maper_key) {
      $new1['priority'] = 1;
  }elseif ($key==$cotRoleMapOptions[1]->maper_key) {
      $new1['priority'] = 2;
  }elseif ($key==$cotRoleMapOptions[2]->maper_key) {
      $new1['priority'] = 3;
  }elseif ($key==$cotRoleMapOptions[3]->maper_key) {
      $new1['priority'] = 4;
  }elseif ($key==$cotRoleMapOptions[4]->maper_key) {
      $new1['priority'] = 5;
  }elseif ($key==$cotRoleMapOptions[5]->maper_key) {
      $new1['priority'] = 6;
  }elseif ($key==$cotRoleMapOptions[6]->maper_key) {
      $new1['priority'] = 7;
  }elseif ($key==$cotRoleMapOptions[7]->maper_key) {
      $new1['priority'] = 8;
  }

  array_push($new,$new1);

} 

$value->new = $new;

$value->totalKeyCount = $resultArray[0];

array_push($usersArray, $value);

}

$usersArray = $this->sortarr($usersArray);

$cotTeamRoleMapUserArray = $usersArray;

/*get percentage values*/
$shaper = 0; 
$coordinator = 0; 
$completerFinisher = 0; 
$teamworker = 0; 
$implementer = 0; 
$monitorEvaluator = 0; 
$plant = 0; 
$resourceInvestigator = 0; 

foreach ($usersArray as $key1 => $value1)
{

    if($value1->shaper==1 || $value1->shaper==2 || $value1->shaper==3)
    {
        $shaper++;     
    }
    if($value1->coordinator==1 || $value1->coordinator==2 || $value1->coordinator==3)
    {
        $coordinator++;
    }
    if($value1->completerFinisher==1 || $value1->completerFinisher==2 || $value1->completerFinisher==3)
    {
        $completerFinisher++;
    }
    if($value1->teamworker==1 || $value1->teamworker==2 || $value1->teamworker==3)
    {
        $teamworker++;
    }
    if($value1->implementer==1 || $value1->implementer==2 || $value1->implementer==3)
    {
        $implementer++;
    }
    if($value1->monitorEvaluator==1 || $value1->monitorEvaluator==2 || $value1->monitorEvaluator==3)
    {
        $monitorEvaluator++;
    }
    if($value1->plant==1 || $value1->plant==2 || $value1->plant==3)
    {
        $plant++;
    }
    if($value1->resourceInvestigator==1 || $value1->resourceInvestigator==2 || $value1->resourceInvestigator==3)
    {
        $resourceInvestigator++;
    }
    
}

$data['shaper']              = $shaper;
$data['coordinator']         = $coordinator;
$data['completerFinisher']   = $completerFinisher;
$data['teamworker']          = $teamworker;
$data['implementer']         = $implementer;
$data['monitorEvaluator']    = $monitorEvaluator;
$data['plant']               = $plant;
$data['resourceInvestigator']= $resourceInvestigator;

$totalUsers = count($finalUser);
//get presented values
$cotTeamRoleMapGraphPercentage = array();

if (!empty($data) && (!empty($totalUsers)))
{

    $data['shaper']              = number_format((($shaper/$totalUsers)*100), 2, '.', '');
    $data['coordinator']         = number_format((($coordinator/$totalUsers)*100), 2, '.', '');
    $data['completerFinisher']   = number_format((($completerFinisher/$totalUsers)*100), 2, '.', '');
    $data['teamworker']          = number_format((($teamworker/$totalUsers)*100), 2, '.', '');
    $data['implementer']         = number_format((($implementer/$totalUsers)*100), 2, '.', '');
    $data['monitorEvaluator']    = number_format((($monitorEvaluator/$totalUsers)*100), 2, '.', '');
    $data['plant']               = number_format((($plant/$totalUsers)*100), 2, '.', '');
    $data['resourceInvestigator']= number_format((($resourceInvestigator/$totalUsers)*100), 2, '.', '');

    $cotTeamRoleMapGraphPercentage['data'] = $data;

}
else
{

    $data['shaper']              = 0;
    $data['coordinator']         = 0;
    $data['completerFinisher']   = 0;
    $data['teamworker']          = 0;
    $data['implementer']         = 0;
    $data['monitorEvaluator']    = 0;
    $data['plant']               = 0;
    $data['resourceInvestigator']= 0;

    $cotTeamRoleMapGraphPercentage['data'] = $data;

}

/*print_r($cotTeamRoleMapGraphPercentage);
die();*/
if (empty($request->pdfStatus))
{
    return view('admin/report/report_graph/cotTeamRoleMapGraph',compact('cotTeamRoleMapGraphPercentage','cotTeamRoleMapUserArray','offices','orgId','officeId','departmentId','departments','all_department','cotRoleMapOptions'));
} 
else 
{
    return compact('cotTeamRoleMapGraphPercentage','cotRoleMapOptions');
}

}

// Get happy index report yearly

public function getReportHappyIndexGraphYear(Request $request)
{
    $happyIndexCountYear = array();

    $orgId = base64_decode($request->orgId); 
    $departmentId = Input::get('departmentId');
    $officeId     = Input::get('officeId');

    $offices = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
    $all_department = DB::table('all_department')->where('status','Active')->get();
    //$flag = 0 ;
    $departments = [];

    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }
    else
    {
        if(!empty($departmentId))
        {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }
  //  echo "<pre>";print_r($departmentsNew);die();
    $happyUsersYearlyQuery = DB::table('happy_indexes')
        ->select(DB::raw('YEAR(happy_indexes.created_at) year'))
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.orgId',$orgId)
        ->where('users.status','Active')
        ->where('happy_indexes.status','Active')
        ->whereRaw('(happy_indexes.moodValue = 3 OR happy_indexes.moodValue = 2 OR happy_indexes.moodValue = 1)');
        // ->where('happy_indexes.moodValue',3)
        // ->orWhere('happy_indexes.moodValue',2)
        // ->orWhere('happy_indexes.moodValue',1);

    if(!empty($officeId) && empty($departmentId))
    {
        $happyUsersYearlyQuery->where('users.officeId',$officeId);
    }
    elseif(!empty($officeId) && !empty($departmentId))
    {
        $happyUsersYearlyQuery->where('users.officeId',$officeId);
        $happyUsersYearlyQuery->where('users.departmentId',$departmentId);
    }
    elseif(empty($officeId) && !empty($departmentId))
    {
        $happyUsersYearlyQuery->where('departments.departmentId',$departmentId);
    }

    $happyUsersYearly = $happyUsersYearlyQuery->groupBy('year')->get();

    foreach ($happyUsersYearly as $key => $value) {

        $moodValue = array(1,2,3);
        $usersCount['indexCount'] =array();

        $totalUsersQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.orgId',$orgId)
        ->where('users.status','Active')
        ->where('happy_indexes.status','Active')
        //->where('happy_indexes.moodValue',$mValue)
        ->whereYear('happy_indexes.created_at',$value->year);

        if(!empty($officeId) && empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
        }
        elseif(!empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
            $totalUsersQuery->where('users.departmentId',$departmentId);
        }
        elseif(empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('departments.departmentId',$departmentId);
        }

        $totalUsers = $totalUsersQuery->count();

        foreach ($moodValue as $mValue) {
            $usersCountQuery = DB::table('happy_indexes')
                ->leftjoin('users','users.id','happy_indexes.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.orgId',$orgId)
                ->where('users.status','Active')
                ->where('happy_indexes.status','Active')
                ->where('happy_indexes.moodValue',$mValue)
                ->whereYear('happy_indexes.created_at',$value->year);

            if(!empty($officeId) && empty($departmentId))
            {
                $usersCountQuery->where('users.officeId',$officeId);
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $usersCountQuery->where('users.officeId',$officeId);
                $usersCountQuery->where('users.departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $usersCountQuery->where('departments.departmentId',$departmentId);
            }

            $moodCount = $usersCountQuery->count();

            if (!empty($totalUsers)) 
            {
                $moodCountUsers = number_format((($moodCount/$totalUsers)*100),2);
            }
            else
            {
                $moodCountUsers = 0;   
            }

            array_push($usersCount['indexCount'], $moodCountUsers);
        }
        
        $usersCount['year'] = $value->year;
        array_push($happyIndexCountYear, $usersCount);
    }

   // print_r($happyIndexCountYear);die();
    if (empty($request->pdfStatus))
    {
        return view('admin/report/report_graph/happyIndexGraph',compact('happyIndexCountYear','orgId','offices','all_department','departments','officeId','departmentId'));
    } 
    else 
    {
        return compact('happyIndexCountYear');
    }
}

// Get happy index report monthly

public function getReportHappyIndexGraphMonth(Request $request)
{
    $year = $request->year;
    $happyIndexCountMonth = array();
    $orgId = base64_decode($request->orgId);
    $departmentId = Input::get('departmentId');
    $officeId     = Input::get('officeId');

    $offices = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
    $all_department = DB::table('all_department')->where('status','Active')->get();
    //$flag = 0 ;
    $departments = [];

    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }
    else
    {
        if(!empty($departmentId))
        {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    $happyUsersYearlistQuery = DB::table('happy_indexes')
        ->select(DB::raw('YEAR(happy_indexes.created_at) year'))
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.orgId',$orgId)
        ->where('users.status','Active')
        ->where('happy_indexes.status','Active')
        ->whereRaw('(happy_indexes.moodValue = 3 OR happy_indexes.moodValue = 2 OR happy_indexes.moodValue = 1)');
        // ->where('happy_indexes.moodValue',3)
        // ->orWhere('happy_indexes.moodValue',2)
        // ->orWhere('happy_indexes.moodValue',1)
        if(!empty($officeId) && empty($departmentId))
        {
            $happyUsersYearlistQuery->where('users.officeId',$officeId);
        }
        elseif(!empty($officeId) && !empty($departmentId))
        {
            $happyUsersYearlistQuery->where('users.officeId',$officeId);
            $happyUsersYearlistQuery->where('users.departmentId',$departmentId);
        }
        elseif(empty($officeId) && !empty($departmentId))
        {
            $happyUsersYearlistQuery->where('departments.departmentId',$departmentId);
        }
        $happyUsersYearlist = $happyUsersYearlistQuery->groupBy('year')->get();
    
    //$months = array(1=>"Jan",2=>"Feb",3=>"Mar",4=>"Apr",5=>"May",6=>"Jun",7=>"Jul",8=>"Aug",9=>"Sep",10=>"Oct",11=>"Nov",12=>"Dec");
    $months = array(1=>"Jan ".$year,2=>"Feb ".$year,3=>"Mar ".$year,4=>"Apr ".$year,5=>"May ".$year,6=>"Jun ".$year,7=>"Jul ".$year,8=>"Aug ".$year,9=>"Sep ".$year,10=>"Oct ".$year,11=>"Nov ".$year,12=>"Dec ".$year);

    $monthInccount = 0;

    foreach ($months as $key => $value) {

        $moodValue = array(1,2,3);
        $monthCount['monthCount'] =array();

        $totalUsersQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.orgId',$orgId)
        ->where('users.status','Active')
        ->where('happy_indexes.status','Active')
        ->whereMonth('happy_indexes.created_at',$key)
        ->whereYear('happy_indexes.created_at',$year);

        if(!empty($officeId) && empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
        }
        elseif(!empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
            $totalUsersQuery->where('users.departmentId',$departmentId);
        }
        elseif(empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('departments.departmentId',$departmentId);
        }
        $totalUsers = $totalUsersQuery->count();

        foreach ($moodValue as $mValue) {
            $monthCountQuery = DB::table('happy_indexes')
                ->leftjoin('users','users.id','happy_indexes.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.orgId',$orgId)
                ->where('users.status','Active')
                ->where('happy_indexes.status','Active')
                ->where('happy_indexes.moodValue',$mValue)
                ->whereMonth('happy_indexes.created_at',$key)
                ->whereYear('happy_indexes.created_at',$year);
                
            if(!empty($officeId) && empty($departmentId))
            {
                $monthCountQuery->where('users.officeId',$officeId);
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $monthCountQuery->where('users.officeId',$officeId);
                $monthCountQuery->where('users.departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $monthCountQuery->where('departments.departmentId',$departmentId);
            }
            $moodCount = $monthCountQuery->count();

            if (!empty($totalUsers)) 
            {
                $moodCountUsers = number_format((($moodCount/$totalUsers)*100),2);
            }
            else
            {
                $moodCountUsers = 0;   
            }
            array_push($monthCount['monthCount'], $moodCountUsers);
        }
        $monthCount['monthName'] = $value;

        array_push($happyIndexCountMonth, $monthCount);

        if ($monthCount['monthCount'] != 0) {
            $monthInccount++;
        }
    }

    if (empty($monthInccount)) {
        $happyIndexCountMonth = array();
    }

    //get the maximum value of the happy index graph
    // $happyIndexYearMaxCount = $this->getReportHappyIndexGraphYear($request);

    // $maxValueArray = array();
    // foreach($happyIndexYearMaxCount['happyIndexCountYear'] as $value)
    // {
    //     foreach ($value['indexCount'] as $key => $value1) 
    //     {
    //         array_push($maxValueArray, $value1);
    //     }
    // }
    // if ($maxValueArray) {
    //     $max = max($maxValueArray);
    // }else
    // {
    //     $max = array();
    // }
   
    return view('admin/report/report_graph/happyIndexGraphMonth',compact('happyIndexCountMonth','year','orgId','offices','officeId','departmentId','departments','all_department','happyUsersYearlist'/*,'max'*/));
}

// Get happy index report weekly

public function getReportHappyIndexGraphWeek(Request $request)
{    
    $year = $request->year;
    $monthName = $request->month;
    $orgId = base64_decode($request->orgId); 
    $departmentId = Input::get('departmentId');
    $officeId     = Input::get('officeId');
    $happyIndexCountWeek = array();

    $offices = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
    $all_department = DB::table('all_department')->where('status','Active')->get();

    $departments = [];

    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }
    else
    {
        if(!empty($departmentId))
        {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    if ($monthName == "Jan ".$year) {
        $month = 1;
    }elseif ($monthName == "Feb ".$year) {
        $month = 2;
    }elseif ($monthName == "Mar ".$year) {
        $month = 3;
    }elseif ($monthName == "Apr ".$year) {
        $month = 4;
    }elseif ($monthName == "May ".$year) {
        $month = 5;
    }elseif ($monthName == "Jun ".$year) {
        $month = 6;
    }elseif ($monthName == "Jul ".$year) {
        $month = 7;
    }elseif ($monthName == "Aug ".$year) {
        $month = 8;
    }elseif ($monthName == "Sep ".$year) {
        $month = 9;
    }elseif ($monthName == "Oct ".$year) {
        $month = 10;
    }elseif ($monthName == "Nov ".$year) {
        $month = 11;
    }elseif ($monthName == "Dec ".$year) {
        $month = 12;
    }

    $monthCount = cal_days_in_month(CAL_GREGORIAN,$month,$year);

    $weeks = $monthCount%7;

    if ($weeks) {
        $arrayCount = array("Week 1","Week 2","Week 3","Week 4","Week 5");
    }
    else
    {
        $arrayCount = array("Week 1","Week 2","Week 3","Week 4");
    }
    $lastDay = date('t',strtotime($year."-".$month."-1"));
    $weekInccount = 0;
    $i = 1;$j = 7;
    foreach ($arrayCount as $key => $value) {
        $moodValue = array(1,2,3);
        $happyUsersWeekly['weekcount'] =array();

        $totalUsersQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.orgId',$orgId)
        ->where('users.status','Active')
        ->where('happy_indexes.status','Active')
        ->whereMonth('happy_indexes.created_at',$month)
        ->whereYear('happy_indexes.created_at',$year);

        if(!empty($officeId) && empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
        }
        elseif(!empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
            $totalUsersQuery->where('users.departmentId',$departmentId);
        }
        elseif(empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('departments.departmentId',$departmentId);
        }
        if ($i==29) {
            $totalUsersQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$lastDay." "."23:59:59"]);
        }
        else
        {
            $totalUsersQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$j." "."23:59:59"]);
        }
        $totalUsers = $totalUsersQuery->count();

        foreach ($moodValue as $mValue) {
            $happyUsersWeeklyQuery = DB::table('happy_indexes')
                ->select('happy_indexes.created_at')
                ->leftjoin('users','users.id','happy_indexes.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.orgId',$orgId)
                ->where('users.status','Active')
                ->where('happy_indexes.status','Active')
                ->where('happy_indexes.moodValue',$mValue);
            if(!empty($officeId) && empty($departmentId))
            {
                $happyUsersWeeklyQuery->where('users.officeId',$officeId);
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $happyUsersWeeklyQuery->where('users.officeId',$officeId);
                $happyUsersWeeklyQuery->where('users.departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $happyUsersWeeklyQuery->where('departments.departmentId',$departmentId);
            }
            if ($i==29) {
                $happyUsersWeeklyQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$lastDay." "."23:59:59"]);
            }
            else
            {
                $happyUsersWeeklyQuery->whereBetween('happy_indexes.created_at',[$year."-".$month."-".$i,$year."-".$month."-".$j." "."23:59:59"]);
            }
            $moodCount = $happyUsersWeeklyQuery->count();

            if (!empty($totalUsers)) 
            {
                $moodCountUsers = number_format((($moodCount/$totalUsers)*100),2);
            }
            else
            {
                $moodCountUsers = 0;   
            }

            array_push($happyUsersWeekly['weekcount'], $moodCountUsers);
        }

        
        $i+=7;
        $j+=7;
        $happyUsersWeekly['week'] = $value;

        array_push($happyIndexCountWeek, $happyUsersWeekly);
        if ($happyUsersWeekly['weekcount'] != 0) {
            $weekInccount++;
        }
    }
    if (empty($weekInccount)) {
        $happyIndexCountWeek = array();
    }

    $monthsList = array(1=>"Jan ".$year,2=>"Feb ".$year,3=>"Mar ".$year,4=>"Apr ".$year,5=>"May ".$year,6=>"Jun ".$year,7=>"Jul ".$year,8=>"Aug ".$year,9=>"Sep ".$year,10=>"Oct ".$year,11=>"Nov ".$year,12=>"Dec ".$year);

    //get the maximum value of the happy index graph

    // $happyIndexYearMaxCount = $this->getReportHappyIndexGraphYear($request);

    // $maxValueArray = array();
    // foreach($happyIndexYearMaxCount['happyIndexCountYear'] as $value)
    // {
    //     foreach ($value['indexCount'] as $key => $value1) 
    //     {
    //         array_push($maxValueArray, $value1);
    //     }
    // }
    // if ($maxValueArray) {
    //     $max = max($maxValueArray);
    // }else
    // {
    //     $max = array();
    // }
    return view('admin/report/report_graph/happyIndexGraphWeek',compact('happyIndexCountWeek','year','orgId','month'/*,'max'*/,'monthsList','offices','officeId','departmentId','departments','all_department'));
}

// Get happy index report day wise

public function getReportHappyIndexGraphDay(Request $request)
{
    $year = $request->year;
    $month = $request->month;
    $week = $request->week;
    $happyIndexCountDay = array();
    $orgId = base64_decode($request->orgId); 
    $departmentId = Input::get('departmentId');
    $officeId     = Input::get('officeId');

    $offices = DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();
    $all_department = DB::table('all_department')->where('status','Active')->get();

    $departments = [];

    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }
    else
    {
        if(!empty($departmentId))
        {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    $days = array();

    $weekList =array();

    $daysInccount = 0;

    $monthCount = cal_days_in_month(CAL_GREGORIAN,$month,$year);

    $remainingWeeks = $monthCount%7;

    $totalUsers = 0;

    if ($month == 1) {
        $monthName = "Jan";
    }elseif ($month == 2) {
        $monthName = "Feb";
    }elseif ($month == 3) {
        $monthName = "Mar";
    }elseif ($month == 4) {
        $monthName = "Apr";
    }elseif ($month == 5) {
        $monthName = "May";
    }elseif ($month == 6) {
        $monthName = "Jun";
    }elseif ($month == 7) {
        $monthName = "Jul";
    }elseif ($month == 8) {
        $monthName = "Aug";
    }elseif ($month == 9) {
        $monthName = "Sep";
    }elseif ($month == 10) {
        $monthName = "Oct";
    }elseif ($month == 11) {
        $monthName = "Nov";
    }elseif ($month == 12) {
        $monthName = "Dec";
    }

    $chopYear = substr($year, -2);

    if ($week == "Week 1") {
        $i=1;
        $days = array($monthName." 1 ".$year,$monthName." 2 ".$year,$monthName." 3 ".$year,$monthName." 4 ".$year,$monthName." 5 ".$year,$monthName." 6 ".$year,$monthName." 7 ".$year);
    }
    else if ($week == "Week 2") {
        $i=8;
        $days = array($monthName." 8 ".$year,$monthName." 9 ".$year,$monthName." 10 ".$year,$monthName." 11 ".$year,$monthName." 12 ".$year,$monthName." 13 ".$year,$monthName." 14 ".$year);
        //$days = array("Day 8","Day 9","Day 10","Day 11","Day 12","Day 13","Day 14");
    }
    else if ($week == "Week 3") {
        $i=15;
        $days = array($monthName." 15 ".$year,$monthName." 16 ".$year,$monthName." 17 ".$year,$monthName." 18 ".$year,$monthName." 19 ".$year,$monthName." 20 ".$year,$monthName." 21 ".$year);
        //$days = array("Day 15","Day 16","Day 17","Day 18","Day 19","Day 20","Day 21");
    }
    else if ($week == "Week 4") {
        $i=22;
        $days = array($monthName." 22 ".$year,$monthName." 23 ".$year,$monthName." 24 ".$year,$monthName." 25 ".$year,$monthName." 26 ".$year,$monthName." 27 ".$year,$monthName." 28 ".$year);
        //$days = array("Day 22","Day 23","Day 24","Day 25","Day 26","Day 27","Day 28");
    }
    else if ($week == "Week 5") {
        $i=29;
        if ($remainingWeeks == 1) {
            $days = array($monthName." 29 ".$year);
            //$days = array("Day 29");
        }
        elseif ($remainingWeeks == 2) {
            $days = array($monthName." 29 ".$year,$monthName." 30 ".$year);
            //$days = array("Day 29","Day 30");
        }elseif ($remainingWeeks == 3) {
            $days = array($monthName." 29 ".$year,$monthName." 30 ".$year,$monthName." 31 ".$year);
            //$days = array("Day 29","Day 30","Day 31");
        }
    }

    foreach ($days as $key => $value) {
        $moodValue = array(1,2,3);
        $dayCount['dayCount'] =array();

        $totalUsersQuery = DB::table('happy_indexes')
        ->leftjoin('users','users.id','happy_indexes.userId')
        ->leftJoin('departments','departments.id','users.departmentId')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('users.orgId',$orgId)
        ->where('users.status','Active')
        ->where('happy_indexes.status','Active')
        ->whereDate('happy_indexes.created_at',$year."-".$month."-".$i);

        if(!empty($officeId) && empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
        }
        elseif(!empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('users.officeId',$officeId);
            $totalUsersQuery->where('users.departmentId',$departmentId);
        }
        elseif(empty($officeId) && !empty($departmentId))
        {
            $totalUsersQuery->where('departments.departmentId',$departmentId);
        }
        $totalUsers = $totalUsersQuery->count();

        foreach ($moodValue as $mValue) {
            $dayCountquery = DB::table('happy_indexes')
                ->select('happy_indexes.created_at')
                ->leftjoin('users','users.id','happy_indexes.userId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId')
                ->where('users.orgId',$orgId)
                ->where('users.status','Active')
                ->where('happy_indexes.status','Active')
                ->where('happy_indexes.moodValue',$mValue)
                ->whereDate('happy_indexes.created_at',$year."-".$month."-".$i);
            if(!empty($officeId) && empty($departmentId))
            {
                $dayCountquery->where('users.officeId',$officeId);
            }
            elseif(!empty($officeId) && !empty($departmentId))
            {
                $dayCountquery->where('users.officeId',$officeId);
                $dayCountquery->where('users.departmentId',$departmentId);
            }
            elseif(empty($officeId) && !empty($departmentId))
            {
                $dayCountquery->where('departments.departmentId',$departmentId);
            }

            $moodCount = $dayCountquery->count();

            if (!empty($totalUsers)) 
            {
                $moodCountUsers = number_format((($moodCount/$totalUsers)*100),2);
            }
            else
            {
                $moodCountUsers = 0;   
            }
            array_push($dayCount['dayCount'], $moodCountUsers);
        }
        $dayCount['dayName'] = $value; 

        array_push($happyIndexCountDay, $dayCount);
       
        $i++;
        if ($dayCount['dayCount'] != 0) {
            $daysInccount++;
        }
    }
    if (empty($daysInccount)) {
        $happyIndexCountDay = array();
    }

    if ($remainingWeeks) {
        $weekList = array("Week 1","Week 2","Week 3","Week 4","Week 5");
    }
    else
    {
        $weekList = array("Week 1","Week 2","Week 3","Week 4");
    }

    //get the maximum value of the happy index graph
    // $happyIndexYearMaxCount = $this->getReportHappyIndexGraphYear($request);

    // $maxValueArray = array();
    // $maxValueArray = array();
    // foreach($happyIndexYearMaxCount['happyIndexCountYear'] as $value)
    // {
    //     foreach ($value['indexCount'] as $key => $value1) 
    //     {
    //         array_push($maxValueArray, $value1);
    //     }
    // }
    // if ($maxValueArray) {
    //     $max = max($maxValueArray);
    // }else
    // {
    //     $max = array();
    // }

    if ($month == 1) {
        $monthName = "Jan ".$year;
    }elseif ($month == 2) {
        $monthName = "Feb ".$year;
    }elseif ($month == 3) {
        $monthName = "Mar ".$year;
    }elseif ($month == 4) {
        $monthName = "Apr ".$year;
    }elseif ($month == 5) {
        $monthName = "May ".$year;
    }elseif ($month == 6) {
        $monthName = "Jun ".$year;
    }elseif ($month == 7) {
        $monthName = "Jul ".$year;
    }elseif ($month == 8) {
        $monthName = "Aug ".$year;
    }elseif ($month == 9) {
        $monthName = "Sep ".$year;
    }elseif ($month == 10) {
        $monthName = "Oct ".$year;
    }elseif ($month == 11) {
        $monthName = "Nov ".$year;
    }elseif ($month == 12) {
        $monthName = "Dec ".$year;
    }

    return view('admin/report/report_graph/happyIndexGraphDay',compact('happyIndexCountDay','year','orgId','monthName','month'/*,'max'*/,'weekList','offices','officeId','departmentId','departments','all_department','week'));
}


/*sorting array by priority*/
function sortarr($arr1)
{

  $arrUsers = array();
  $newArray = array();
  $tes      = array();

  for($i=0; $i<count($arr1); $i++)
  {    
    $tes['userId']    = $arr1[$i]->id;
    $tes['userName']  = $arr1[$i]->name;   

    $arrt = (array)$arr1[$i]->new;
    
    usort($arrt, function($x, $y)
    {

      if ($x['value']== $y['value'] ) 
      {
        if($x['priority']<$y['priority'])
        {
          return 0;
      }
      else
      {
          return 1;
      } 
  }
}); 

    $tt = array();
    foreach($arrt as $key => $value)
    {         
      if($value['value']!=0)
      {
        $tes[$value['title']] = $key+1;
    }
    else
    {
        $tes[$value['title']] = 0;
    }

}   

$tes['totalKeyCount'] = $arr1[$i]->totalKeyCount;

array_push($newArray,(object)$tes);
}

return $newArray;

}

/*generate pdf*/
public function showPdfReport(Request $request)
{
    $orgId = $request->id;
    $request->orgId = $orgId;
    $request->pdfStatus = true;
    //get data of DOT
    $dotObj = $this->show($request,$orgId);
    $org    = $dotObj->org;
    $dotValuesArray = $dotObj->dotValuesArray;

    //For index
    $indexOrg = $dotObj->indexOrg;
    $yearUsed = $dotObj->yearUsed;
    $currentYear = $dotObj->currentYear;
    $currentMonth = $dotObj->currentMonth;
    $graphVal = $dotObj->graphVal;
    $showGraph = $dotObj->showGraph;

    //get data of team role map percentage values
    $cotTeamRoleMapObj = $this->getCOTteamRoleMapGraph($request);
    $cotTeamRoleMapGraphPercentage = $cotTeamRoleMapObj['cotTeamRoleMapGraphPercentage']['data'];
    $cotRoleMapOptions             = $cotTeamRoleMapObj['cotRoleMapOptions'];
    
    //get cot functional lens
    $cotFunLensObj = $this->getReportFunctionalGraph($request);
    $funcLensPercentageArray = $cotFunLensObj['funcLensPercentageArray']; 
    $keyNameArray            = $cotFunLensObj['keyNameArray'];
    //get sot culture structure
    $sotCulStrObj = $this->getReportCultureGraph($request);
    $sotCountArray = $sotCulStrObj['sotCountArray'];
    $sotStrDetailArr = $sotCulStrObj['sotStrDetailArr'];

    //get sot motivatin data
    $sotMotObj = $this->getReportMotivationalGraph($request);
    $SOTmotivationResultArray = $sotMotObj['SOTmotivationResultArray'];

    //get diagnostic data
    $diaObj = $this->getReportDiagnosticGraph($request);
    $diagnosticResultArray = $diaObj['diagnosticResultArray'];

    

    //get tribeometer data
    $triObj = $this->getReportTribeometerGraph($request);
    $tribeometerResultArray = $triObj['tribeometerResultArray'];

    //get happy index data
    $hapIndexObj = $this->getReportHappyIndexGraphYear($request);
    $happyIndexCountYear = $hapIndexObj['happyIndexCountYear'];

    return view('admin/report/report_graph/pdfReport',compact('org','dotValuesArray','cotTeamRoleMapGraphPercentage','funcLensPercentageArray','keyNameArray','sotCountArray','sotStrDetailArr','SOTmotivationResultArray','diagnosticResultArray','tribeometerResultArray','cotRoleMapOptions','happyIndexCountYear','indexOrg','yearUsed','currentYear','currentMonth','graphVal','showGraph'));
}

public function exportPdf()
{
    $orgId  = base64_decode(Input::get('orgId'));

    $file_name    = 'report'.$orgId.'_'.time().'.pdf'; 
    $hidden_html  = Input::get('hidden_html');

    $dompdf = new Dompdf();     
    $dompdf->loadHtml($hidden_html);
    $dompdf->setPaper('A4', 'landscape');
    $dompdf->render();
    $dompdf->stream($file_name,array("Attachment"=>false));
    $output = $dompdf->output();   
    file_put_contents(public_path('uploads/pdf_report/').$file_name, $output);

    DB::table('organisations')->where('id',$orgId)->update(['PdfReportUrl'=>$file_name]);
}


/* get diagnostic sub graph values*/
public function getDiagnsticSubGraph(Request $request)
{
    $indexId      = Input::get('barIndex');
    $orgId        = Input::get('orgId');
    $departmentId = Input::get('departmentId');
    $officeId     = Input::get('officeId');
    $categoryId   = Input::get('categoryId');

    if(!$categoryId)
    {
        $categoryId = $indexId+1;
    }
    

    $diagnosticResultArray = array();

    $offices=DB::table('offices')->where('orgId',$orgId)->where('status','Active')->get();  
    $all_department = DB::table('all_department')->where('status','Active')->get();

    $diaQueCategoryList = DB::table('diagnostic_questions_category')->get();

    $flag = 0 ;
    $departments = [];
    if (!empty($officeId)) 
    {
        $departments = DB::table('departments')
        ->select('departments.id','all_department.department')
        ->leftJoin('all_department','all_department.id','departments.departmentId')
        ->where('departments.officeId',$officeId) 
        ->where('departments.status','Active') 
        ->orderBy('all_department.department','ASC')->get();

        //for new department
        $departmentsNew = DB::table('departments')->select('id')->where('id',$departmentId)->where('status','Active')->get();
    }
    else
    {
        if(!empty($departmentId))
        {
            $departmentsNew = DB::table('departments')
            ->select('departments.id','all_department.department')
            ->leftJoin('all_department','all_department.id','departments.departmentId')
            ->where('departments.orgId',$orgId)
            ->where('departments.departmentId',$departmentId) 
            ->where('departments.status','Active') 
            ->orderBy('all_department.department','ASC')->get();
        }
    }

    $query = DB::table('diagnostic_answers')
    ->leftjoin('users','users.id','diagnostic_answers.userId')
    ->select('diagnostic_answers.userId')
    ->where('users.status','Active')
    ->groupBy('diagnostic_answers.userId')
    ->where('users.orgId',$orgId);

    if(!empty($officeId))
    {
        $query->where('users.officeId',$officeId);
    }


    if(!empty($departmentId))
    {   
        $i = 0;

        if($departmentsNew->isEmpty())
        {
            $flag = 1;
        }
        foreach ($departmentsNew as $departments1) 
        {       
            if($i == 0)
            {
                $query->where('users.departmentId',$departments1->id);
            }
            else
            {
                $query->orWhere('users.departmentId',$departments1->id);
            }
            $i++;
        }
    }


    $users = $query->get();
    $userCount = count($users);

 // reset if department is not in organisation
    if($flag==1)
    {
        $userCount = 0;
    }

    $questionTbl = DB::table('diagnostic_questions')->where('category_id',$categoryId)->where('status','Active')->get();

    $optionsTbl = DB::table('diagnostic_question_options')->where('status','Active')->get();

    $quecount    = count($questionTbl);
    $diaOptCount = count($optionsTbl);
    
    $perQuePercen = 0;

    if($userCount) 
    {
        foreach($questionTbl as $value)
        {

            $diaQuery = DB::table('diagnostic_answers')            
            ->leftJoin('diagnostic_questions','diagnostic_questions.id','diagnostic_answers.questionId')
            ->leftjoin('users','users.id','=','diagnostic_answers.userId')
            ->where('users.status','Active')   
            ->where('diagnostic_answers.orgId',$orgId)
            ->where('diagnostic_questions.id',$value->id)
            ->where('diagnostic_questions.category_id',$categoryId);

            if(!empty($officeId))
            {
                $diaQuery->where('users.officeId',$officeId);
            }
            
            if(!empty($departmentId))
            {   
                $i = 0;

                if($departmentsNew->isEmpty())
                {
                    $flag = 1;
                }
                $department3 = array();
                foreach ($departmentsNew as $departments1) 
                {       
                    $departments2 = $departments1->id;
                    array_push($department3, $departments2);
                }
                
                $diaQuery->whereIn('users.departmentId',$department3);
            }

            $diaAnsTbl = $diaQuery->sum('answer'); 

            $perQuePercen = ($diaAnsTbl/$userCount);               

            $score = ($perQuePercen/($quecount*$diaOptCount));
            $totalPercentage = ($perQuePercen/($quecount*$diaOptCount))*100;

            $val['title']      =  ucfirst($value->measure);
            $val['score']      =  number_format((float)$score, 2, '.', '');
            $val['percentage'] =  number_format((float)$totalPercentage, 2, '.', '');     

            array_push($diagnosticResultArray, $val);
        }
    }


    //get the maximum value of the diagnostic graph
    $request->orgId = base64_encode($orgId);
    $test = $this->getReportDiagnosticGraph($request);

    $maxValueArray = array();
    foreach($test->diagnosticResultArray as $value1)
    {
        array_push($maxValueArray, $value1['percentage']);
    }

    $max = 100;
    if(!empty($maxValueArray))
    {
        $max = max($maxValueArray);
        $max = ceil($max / 10) * 10;

        if($max >= 100)
        {
            $max = 100;
        }
    }


    return view('admin/report/report_graph/reportDiagnosticSubGraph',compact('diagnosticResultArray','offices','orgId','officeId','departmentId','departments','all_department','diaQueCategoryList','categoryId','max'));
}

/*get DOT bubble list for report*/
public function getDotThumbsUpBubbleList()
{

   $resultArray = array();
   $beliefArr = array();

   $userId     = Input::get('userId');
   $date       = Input::get('date');
   $orgId      = base64_decode(Input::get('orgId'));

   $month = '';
   $year  = '';
   if($date)
   {
     $dateObj = date_create($date);
     $year  = date_format($dateObj,"Y");
     $month = date_format($dateObj,"m");
 }

 $dots = DB::table('dots')->select('id')->where('orgId',$orgId)->first();

 $userList = DB::table('users')->select('id','name')->where('orgId',$orgId)->where('status','Active')->get(); 

 if(!empty($dots))
 {

    $dotId = $dots->id;

    $beliefsData = DB::table('dots_beliefs')->where('dotId',$dotId)->get();

    foreach($beliefsData as $bValue)
    {

      $belief['id']   = $bValue->id;
      $belief['name'] = ucfirst($bValue->name);

      $dotsValue = DB::table('dots_values')->where('beliefId',$bValue->id)->where('status','Active')->get();

      $dotsValueArr = array();

      foreach ($dotsValue as $Vvalue)
      {

        $dotValueList = DB::table('dot_value_list')->where('id',$Vvalue->name)->first();

        $upVotTblQuery = DB::table('dot_bubble_rating_records')            
        ->where('dot_belief_id',$bValue->id)
        ->where('dot_value_name_id',$Vvalue->name)
        ->where('bubble_flag',"1");

        if($month && $year) 
        {
            $upVotTblQuery->whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month);
        }
        
        if($userId)
        {
          $upVotTblQuery->where('to_bubble_user_id',$userId);
      }

      $dotV['upVotes']   = $upVotTblQuery->count();

      $dValuesName = '';
      if($dotValueList)
      {
          $dValuesName = $dotValueList->name;
      }
      $dotV["id"]        = $Vvalue->id;
      $dotV['name']      = $dValuesName;          


      $valuesRatingsQuery = db::table('dot_values_ratings')->where('valueId', $Vvalue->id);            
      if($userId) 
      {
          $valuesRatingsQuery->where('userId',$userId);
      }            

      $valuesRatings = $valuesRatingsQuery->first();

      $dotV["ratings"]='';

      if($valuesRatings)
      {
          $dotV["ratings"]= $valuesRatings->ratings;
      }

      array_push($dotsValueArr, $dotV);      
  }

  $belief['beliefValueArr'] = $dotsValueArr;

  array_push($beliefArr, $belief);
  
}

}

return view('admin/report/report_graph/dotThumbsuplist',compact('beliefArr','orgId','userList','userId','date'));

}

}