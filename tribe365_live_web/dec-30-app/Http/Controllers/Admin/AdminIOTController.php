<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;
use Validator;
use Hash;
use Config;
use View;
use Illuminate\Pagination\LengthAwarePaginator;

class AdminIOTController extends Controller
{

   public function __construct(Request $request)
   {

    $orgId = base64_decode($request->segment(3));

    $organisations = DB::table('organisations')->select('ImageURL')->where('id',$orgId)->first();

    if(!empty($organisations->ImageURL))
    {
        $org_detail = base64_encode($orgId)."-".$organisations->ImageURL;
        View::share('org_detail', $org_detail);
    }
    else
    {
        $org_detail = "";
        View::share('org_detail', $org_detail);
    }

}


public function show($id)
{
    $orgId = base64_decode($id);

    return view('admin/IOT/listIOTdetail',compact('orgId'));
}

//get iot feedback dashboard


public function getIotDeshboard(Request $request)
{ 

    $orgId    = base64_decode($request->orgId);
    $officeId = $request->officeId;

    //when user come from organisation
    $isFromOrg = base64_decode($request->segment(3));

    if(!$officeId)
    {
        $officeId = 0;
    }
    if(!$orgId)
    {
        $orgId = 0;
    }

    $organisations = DB::table('organisations')->where('status','Active')->get(); 

    $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get(); 

    //new feedbacks
    $iotNewFeedbackQuery = DB::table('iot_feedbacks')
    ->leftJoin('iot_messages','iot_messages.feedbackId','iot_feedbacks.id')
    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->where('iot_feedbacks.status','Active')
    ->where('iot_feedbacks.status','!=','Completed')
    ->whereNull('iot_messages.feedbackId');

    if($orgId)
    {
        $iotNewFeedbackQuery->where('iot_feedbacks.orgId',$orgId);
    }
    if($officeId)
    {
        $iotNewFeedbackQuery->where('users.officeId',$officeId);
    }

    $iotNewFeedback = $iotNewFeedbackQuery->get();

    //on hold feedback count
    $iotOnHoldFeedbackArrayQuery = DB::table('iot_feedbacks')->select('*','iot_feedbacks.id as feedId')
    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->where('iot_feedbacks.status','Active')->where('iot_feedbacks.status','!=','Completed')

    ->whereIn('iot_feedbacks.id', function($query)
    {
        $query->select('feedbackId')->from('iot_messages');
    });


    if($orgId)
    {
        $iotOnHoldFeedbackArrayQuery->where('users.orgId',$orgId);
    }
    if($officeId)
    {
        $iotOnHoldFeedbackArrayQuery->where('users.officeId',$officeId);
    }

    $iotOnHoldFeedbackArray = $iotOnHoldFeedbackArrayQuery->get();

    $coun = 0;
    foreach ($iotOnHoldFeedbackArray as $key => $value) {


        $iot = DB::table('iot_messages')
        ->where('feedbackId',$value->feedId)
        ->orderBy('id','desc')
        ->first();
        if ($iot->sendFrom != 1) {
            $coun++;
        }
    }
    $iotOnHoldFeedbackArray = $coun;

    //on completed feedbacks
    $iotCompletedFeedbackQuery = DB::table('iot_feedbacks')
    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->where('iot_feedbacks.status','Completed');

    if($orgId)
    {
        $iotCompletedFeedbackQuery->where('iot_feedbacks.orgId',$orgId);
    }
    if($officeId)
    {
        $iotCompletedFeedbackQuery->where('users.officeId',$officeId);
    }

    $iotCompletedFeedback = $iotCompletedFeedbackQuery->get();

    // avaiting response
    $avaitingResponseArray = array();

    $iotFeedTblQuery = DB::table('iot_feedbacks')->select('*','iot_feedbacks.id as feedId')
    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->where('iot_feedbacks.status','Active')->where('iot_feedbacks.status','!=','Completed')

    ->whereIn('iot_feedbacks.id', function($query)
    {
        $query->select('feedbackId')->from('iot_messages');
    });

    if($orgId)
    {
        $iotFeedTblQuery->where('iot_feedbacks.orgId',$orgId);
    }
    if($officeId)
    {
        $iotFeedTblQuery->where('users.officeId',$officeId);
    }

    $iotFeedTbl = $iotFeedTblQuery->get();
   // echo "<pre>";print_r($iotFeedTbl);die();

    $coun1 = 0;
    foreach ($iotFeedTbl as $key => $value) {


        $iot = DB::table('iot_messages')
        ->where('feedbackId',$value->feedId)
        ->orderBy('id','desc')
        ->first();
          //  echo "<pre>";print_r($iot);
        if ($iot->sendFrom == 1) {
            $coun1++;
        }
    }
    $avaitingResponseArray = $coun1;

    //get themes count for dashbaord
    $themeListTbl = DB::table('iot_theme_category')->select('id','title')->where('status','Active')->get();

    $themeList = array();
    foreach($themeListTbl as $value)
    {        
        $themeCountQuery = DB::table('iot_themes')->where('type',$value->id)->where('status','Active');

        if($orgId)
        {
            $themeCountQuery->where('orgId',$orgId);
        }

        $themeCount = $themeCountQuery->count();

        $result['id']         = $value->id;
        $result['title']      = $value->title;
        $result['themeCount'] = $themeCount;

        array_push($themeList, $result);
    }

    $fromOrg = false;
    if($isFromOrg !=0 && !$officeId) 
    {
        $fromOrg = true;
    }

    return view('admin.IOT.iotDashboard',compact('organisations','iotNewFeedback','iotOnHoldFeedbackArray','iotCompletedFeedback','avaitingResponseArray','themeList','orgId','officeId','offices','fromOrg'));
}  


// public function getIotDeshboard(Request $request)
// { 

//     $orgId    = base64_decode($request->orgId);
//     $officeId = $request->officeId;

//     //when user come from organisation
//     $isFromOrg = base64_decode($request->segment(3));

//     if(!$officeId)
//     {
//         $officeId = 0;
//     }
//     if(!$orgId)
//     {
//         $orgId = 0;
//     }

//     $organisations = DB::table('organisations')->where('status','Active')->get(); 

//     $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get(); 

//     //new feedbacks
//     $iotNewFeedbackQuery = DB::table('iot_feedbacks')
//     ->leftJoin('iot_messages','iot_messages.feedbackId','iot_feedbacks.id')
//     ->leftJoin('users','users.id','iot_feedbacks.userId')
//     ->where('iot_feedbacks.status','!=','Completed')
//     ->whereNull('iot_messages.feedbackId');

//     if($orgId)
//     {
//         $iotNewFeedbackQuery->where('iot_feedbacks.orgId',$orgId);
//     }
//     if($officeId)
//     {
//         $iotNewFeedbackQuery->where('users.officeId',$officeId);
//     }

//     $iotNewFeedback = $iotNewFeedbackQuery->get();

//     //on hold feedback count
//     $iotOnHoldFeedbackArrayQuery = DB::table('iot_feedbacks')
//     ->leftJoin('users','users.id','iot_feedbacks.userId')
//     ->where('iot_feedbacks.status','Active')->where('iot_feedbacks.status','!=','Completed')

//     ->whereIn('iot_feedbacks.id', function($query)
//     {
//         $query->select('feedbackId')->from('iot_messages');
//     });


//     if($orgId)
//     {
//         $iotOnHoldFeedbackArrayQuery->where('users.orgId',$orgId);
//     }
//     if($officeId)
//     {
//         $iotOnHoldFeedbackArrayQuery->where('users.officeId',$officeId);
//     }

//     $iotOnHoldFeedbackArray = $iotOnHoldFeedbackArrayQuery->get();


//     //on completed feedbacks
//     $iotCompletedFeedbackQuery = DB::table('iot_feedbacks')
//     ->leftJoin('users','users.id','iot_feedbacks.userId')
//     ->where('iot_feedbacks.status','Completed');

//     if($orgId)
//     {
//         $iotCompletedFeedbackQuery->where('iot_feedbacks.orgId',$orgId);
//     }
//     if($officeId)
//     {
//         $iotCompletedFeedbackQuery->where('users.officeId',$officeId);
//     }

//     $iotCompletedFeedback = $iotCompletedFeedbackQuery->get();

//     // avaiting response
//     $avaitingResponseArray = array();

//     $iotFeedTblQuery = DB::table('iot_feedbacks')
//     ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.image','iot_feedbacks.created_at','users.email','organisations.organisation','offices.office','all_department.department')    
//     ->leftJoin('users','users.id','iot_feedbacks.userId')
//     ->leftJoin('organisations','organisations.id','users.orgId')
//     ->leftJoin('offices','offices.id','users.officeId')
//     ->leftJoin('departments','departments.id','users.departmentId')
//     ->leftJoin('all_department','all_department.id','departments.departmentId')    
//     ->where('iot_feedbacks.status','Active')
//     ->where('iot_feedbacks.status','!=','Completed');

//     if($orgId)
//     {
//         $iotFeedTblQuery->where('iot_feedbacks.orgId',$orgId);
//     }
//     if($officeId)
//     {
//         $iotFeedTblQuery->where('users.officeId',$officeId);
//     }

//     $iotFeedTblQuery->whereIn('iot_feedbacks.id', function($query)
//     {
//         $query->select('feedbackId')
//         ->from('iot_messages')
//         ->leftJoin('users','users.id','iot_messages.sendFrom')
//         ->where('iot_messages.status','Active')
//         ->where('users.roleId',1)
//         ->orderBy('iot_messages.id','DESC');

//     });

//     $avaitingResponseArray = $iotFeedTblQuery->get();

//     // foreach($iotFeedTbl as $fvalue)
//     // {
//     //     $iotMsgQuery = DB::table('iot_messages')
//     //     ->select('iot_messages.id')
//     //     ->leftJoin('users','users.id','iot_messages.sendFrom')
//     //     ->where('users.roleId',1)
//     //     ->where('iot_messages.feedbackId',$fvalue->id)
//     //     ->where('iot_messages.status','Active')
//     //     ->orderBy('iot_messages.id','DESC');

//     //     if($orgId)
//     //     {
//     //         $iotMsgQuery->where('users.orgId',$orgId);
//     //     }
//     //     if($officeId)
//     //     {
//     //         $iotMsgQuery->where('users.officeId',$officeId);
//     //     }

//     //     $iotMsg = $iotMsgQuery->first();

//     //     if(!$iotMsg)
//     //     {
//     //         array_push($avaitingResponseArray, $fvalue->id);
//     //     }
//     // }


//     //get themes count for dashbaord
//     $themeListTbl = DB::table('iot_theme_category')->select('id','title')->where('status','Active')->get();

//     $themeList = array();
//     foreach($themeListTbl as $value)
//     {        
//         $themeCountQuery = DB::table('iot_themes')->where('type',$value->id)->where('status','Active');

//         if($orgId)
//         {
//             $themeCountQuery->where('orgId',$orgId);
//         }

//         $themeCount = $themeCountQuery->count();

//         $result['id']         = $value->id;
//         $result['title']      = $value->title;
//         $result['themeCount'] = $themeCount;

//         array_push($themeList, $result);
//     }

//     $fromOrg = false;
//     if($isFromOrg !=0 && !$officeId) 
//     {
//         $fromOrg = true;
//     }

//     return view('admin.IOT.iotDashboard',compact('organisations','iotNewFeedback','iotOnHoldFeedbackArray','iotCompletedFeedback','avaitingResponseArray','themeList','orgId','officeId','offices','fromOrg'));
// }  




/*get feedback list*/



public function getFeedbackList(Request $request)
{

    $status   = base64_decode($request->status);
    $orgId    = base64_decode($request->orgId);
    $officeId = base64_decode($request->officeId);

    $feedbackListQuery = DB::table('iot_feedbacks')
    ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.image','iot_feedbacks.created_at','iot_feedbacks.orgId','users.email','organisations.organisation','offices.office','all_department.department')

    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->leftJoin('organisations','organisations.id','users.orgId')
    ->leftJoin('offices','offices.id','users.officeId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId');    

    if($orgId)
    {
        $feedbackListQuery->where('users.orgId',$orgId);
    }
    if($officeId)
    {
        $feedbackListQuery->where('users.officeId',$officeId);
    }

    if($status=='new')
    {
        $feedbackListQuery->where('iot_feedbacks.status','!=','Completed')
        ->whereNull('iot_messages.feedbackId')
        ->leftJoin('iot_messages','iot_messages.feedbackId','iot_feedbacks.id')
        ->orderBy('iot_feedbacks.id','DESC');
        $feedbackListTbl =  $feedbackListQuery->paginate(6);
    }
    elseif($status=='on_hold')
    {
        $feedbackListQuery->where('iot_feedbacks.status','!=','Completed');
        $feedbackListQuery->where('iot_feedbacks.status','Active');

        $feedbackListQuery->whereIn('iot_feedbacks.id', function($query)
        {
            $query->select('feedbackId')
            ->from('iot_messages');
        });

        $feedbackListQuery->orderBy('iot_feedbacks.id','DESC');
        $feedbackOnholdListTbl = $feedbackListQuery->get();
        //echo "<pre>";print_r($feedbackOnholdListTbl);die();
        $iotData = array();

        foreach ($feedbackOnholdListTbl as $key => $value) {
            $iot = DB::table('iot_feedbacks')
            //->select('iot_feedbacks.id','iot_messages.message','iot_messages.sendTo','iot_messages.sendFrom')
            //->select('iot_feedbacks.id','iot_messages.message','iot_feedbacks.image','iot_feedbacks.created_at','iot_feedbacks.orgId','users.email','organisations.organisation','offices.office','all_department.department','iot_messages.sendTo','iot_messages.sendFrom')


            ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.image','iot_feedbacks.created_at','iot_feedbacks.orgId','users.email','organisations.organisation','offices.office','all_department.department','iot_messages.sendTo','iot_messages.sendFrom')

            ->leftJoin('users','users.id','iot_feedbacks.userId')
            ->leftJoin('iot_messages','iot_feedbacks.id','iot_messages.feedbackId')
            ->leftJoin('organisations','organisations.id','users.orgId')
            ->leftJoin('offices','offices.id','users.officeId')
            ->leftJoin('departments','departments.id','users.departmentId')
            ->leftJoin('all_department','all_department.id','departments.departmentId');

            if($orgId)
            {
                $iot->where('users.orgId',$orgId);
            }
            if($officeId)
            {
                $iot->where('users.officeId',$officeId);
            }
            $iot->where('iot_feedbacks.id',$value->id)
            ->where('iot_feedbacks.status','Active')
            ->where('iot_feedbacks.status','!=','Completed')
            ->orderBy('iot_messages.id','DESC');
            $iotMsg = $iot->first();
       //     echo "<pre>";print_r($iotMsg);
            if($iotMsg)
            {
                if($iotMsg->sendFrom != 1)
                {
                    array_push($iotData, $iotMsg);
                }            
            }
        }
        //echo "<pre>";print_r($iotData);
        //die();

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $perPage = 6;

        // $iotData = array_reverse(array_sort($iotData , function ($value) {
        //     return $value->created_at;
        // }));
        $currentItems = array_slice($iotData, $perPage * ($currentPage - 1), $perPage);

        $paginator = new LengthAwarePaginator($currentItems, count($iotData), $perPage, $currentPage);
        $feedbackListTbl = $paginator;
    }
    elseif($status=='completed')
    {

        $feedbackListQuery->where('iot_feedbacks.status','Completed')
        ->orderBy('iot_feedbacks.id','DESC');
        $feedbackListTbl = $feedbackListQuery->paginate(6);
    }
    else if($status=='awaiting')
    {

       $feedbackListQuery->where('iot_feedbacks.status','!=','Completed');

       $feedbackListQuery->whereIn('iot_feedbacks.id', function($query)
       {
        $query->select('feedbackId')
        ->from('iot_messages');
    });

       $feedbackListQuery->orderBy('iot_feedbacks.id','DESC');
       $feedbackAwaitingListTbl = $feedbackListQuery->get();
       $iotData = array();

       foreach ($feedbackAwaitingListTbl as $key => $value) {
            $iot = DB::table('iot_feedbacks')
                //->select('iot_feedbacks.id','iot_messages.message','iot_messages.sendTo','iot_messages.sendFrom')
                //->select('iot_feedbacks.id','iot_messages.message','iot_feedbacks.image','iot_feedbacks.created_at','iot_feedbacks.orgId','users.email','organisations.organisation','offices.office','all_department.department','iot_messages.sendTo','iot_messages.sendFrom')


                ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.image','iot_feedbacks.created_at','iot_feedbacks.orgId','users.email','organisations.organisation','offices.office','all_department.department','iot_messages.sendTo','iot_messages.sendFrom')
                ->leftJoin('users','users.id','iot_feedbacks.userId')
                ->leftJoin('iot_messages','iot_feedbacks.id','iot_messages.feedbackId')
                ->leftJoin('organisations','organisations.id','users.orgId')
                ->leftJoin('offices','offices.id','users.officeId')
                ->leftJoin('departments','departments.id','users.departmentId')
                ->leftJoin('all_department','all_department.id','departments.departmentId');

                if($orgId)
                {
                    $iot->where('users.orgId',$orgId);
                }
                if($officeId)
                {
                    $iot->where('users.officeId',$officeId);
                }
                $iot->where('iot_feedbacks.id',$value->id)
                ->where('iot_feedbacks.status','Active')
                ->where('iot_feedbacks.status','!=','Completed')
                ->orderBy('iot_messages.id','DESC');
                $iotMsg = $iot->first();
           //     echo "<pre>";print_r($iotMsg);
                if($iotMsg)
                {
                    if($iotMsg->sendFrom == 1)
                    {
                        array_push($iotData, $iotMsg);
                    }            
                }
        }
       // echo "<pre>";print_r($iotData);die();

    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    $perPage = 6;

        // $iotData = array_reverse(array_sort($iotData , function ($value) {
        //     return $value->created_at;
        // }));
    $currentItems = array_slice($iotData, $perPage * ($currentPage - 1), $perPage);

    $paginator = new LengthAwarePaginator($currentItems, count($iotData), $perPage, $currentPage);

    $feedbackListTbl = $paginator;

        //print_r($feedbackListTbl);DIE

}
else
{      
    $feedbackListTbl = $feedbackListQuery->paginate(6);
}


$objectArr = array();
foreach ($feedbackListTbl as  $rValue)
{
    $object    = new Controller();

    $object->id           = $rValue->id;
    $object->message      = $rValue->message;
    $object->image        = $rValue->image;
    $object->created_at   = $rValue->created_at;
    $object->email        = $rValue->email;
    $object->organisation = $rValue->organisation;
    $object->office       = $rValue->office;
    $object->department   = $rValue->department;
    $object->orgId        = $rValue->orgId;


    $themeListTbl = DB::table('iot_allocated_themes')
    ->select('iot_themes.title')
    ->leftJoin('iot_themes','iot_themes.id','iot_allocated_themes.themeId')
    ->where('feedbackId',$rValue->id)->where('iot_allocated_themes.status','Active')
    ->get();

    $themeListArr = array();
    foreach ($themeListTbl as $tvalue)
    {
        array_push($themeListArr, $tvalue->title);
    }

    $object->themes = $themeListArr;

    array_push($objectArr, $object);
}

$feedbackList = $objectArr;

rsort($feedbackList);

//echo "<pre />"; print_r($feedbackList);die;

$themeList = DB::table('iot_themes')->where('status','Active')->get();

return view('admin.IOT.iotFeedbackList',compact('iotFeedTbl','feedbackListTbl','feedbackList','themeList'));

}  



/*public function getFeedbackList(Request $request)
{

    $status   = base64_decode($request->status);
    $orgId    = base64_decode($request->orgId);
    $officeId = base64_decode($request->officeId);

    $feedbackListQuery = DB::table('iot_feedbacks')
    ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.image','iot_feedbacks.created_at','iot_feedbacks.orgId','users.email','organisations.organisation','offices.office','all_department.department')

    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->leftJoin('organisations','organisations.id','users.orgId')
    ->leftJoin('offices','offices.id','users.officeId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId');    

    $iotFeedTbl = '';
    if($orgId)
    {
        $feedbackListQuery->where('users.orgId',$orgId);
    }
    if($officeId)
    {
        $feedbackListQuery->where('users.officeId',$officeId);
    }

    if($status=='new')
    {
        $feedbackListQuery->where('iot_feedbacks.status','!=','Completed')
        ->whereNull('iot_messages.feedbackId')
        ->leftJoin('iot_messages','iot_messages.feedbackId','iot_feedbacks.id');

        $feedbackListTbl =  $feedbackListQuery->paginate(6);
    }
    elseif($status=='on_hold')
    {

        $feedbackListQuery->where('iot_feedbacks.status','!=','Completed');

        $feedbackListQuery->whereIn('iot_feedbacks.id', function($query)
        {
            $query->select('feedbackId')
            ->from('iot_messages');
        });

        $feedbackListTbl = $feedbackListQuery->paginate(6);

    }
    elseif($status=='completed')
    {

        $feedbackListQuery->where('iot_feedbacks.status','Completed');
        $feedbackListTbl = $feedbackListQuery->paginate(6);
    }
    else if($status=='awaiting')
    {

        // avaiting response
        $avaitingResponseArray = array();

        $feedbackListQuery->where('iot_feedbacks.status','Active');    

        if($orgId)
        {
            $feedbackListQuery->where('users.orgId',$orgId);
        }
        if($officeId)
        {
            $feedbackListQuery->where('users.officeId',$officeId);
        }  

        $feedbackListQuery->whereIn('iot_feedbacks.id', function($query)
        {
            $query->select('feedbackId')
            ->from('iot_messages')
            ->leftJoin('users','users.id','iot_messages.sendFrom')
            ->where('iot_messages.status','Active')
            ->where('users.roleId',1)
            ->orderBy('iot_messages.id','DESC');
            
        });

        $feedbackListTbl = $feedbackListQuery->paginate(6);

        // print_r($iotFeedTbl);
        // dd();

        // foreach($iotFeedTbl as $fvalue)
        // {
        //     $iotMsgQuery = DB::table('iot_messages')->select('iot_messages.id')
        //     ->leftJoin('users','users.id','iot_messages.sendFrom')
        //     ->where('users.roleId',1)
        //     ->where('iot_messages.feedbackId',$fvalue->id)
        //     ->where('iot_messages.status','Active')
        //     ->orderBy('iot_messages.id','DESC');

        //     if($orgId)
        //     {
        //         $iotMsgQuery->where('users.orgId',$orgId);
        //     }
        //     if($officeId)
        //     {
        //         $iotMsgQuery->where('users.officeId',$officeId);
        //     }

        //     $iotMsg = $iotMsgQuery->first();

        //     if(!$iotMsg)
        //     {
        //         array_push($avaitingResponseArray, $fvalue);
        //     }
        // }

        // $feedbackListTbl = $avaitingResponseArray;

    }
    else
    {      
        $feedbackListTbl = $feedbackListQuery->paginate(6);
    }


    $objectArr = array();
    foreach ($feedbackListTbl as  $rValue)
    {
        $object    = new Controller();

        $object->id           = $rValue->id;
        $object->message      = $rValue->message;
        $object->image        = $rValue->image;
        $object->created_at   = $rValue->created_at;
        $object->email        = $rValue->email;
        $object->organisation = $rValue->organisation;
        $object->office       = $rValue->office;
        $object->department   = $rValue->department;
        $object->orgId        = $rValue->orgId;
        

        $themeListTbl = DB::table('iot_allocated_themes')
        ->select('iot_themes.title')
        ->leftJoin('iot_themes','iot_themes.id','iot_allocated_themes.themeId')
        ->where('feedbackId',$rValue->id)->where('iot_allocated_themes.status','Active')
        ->get();

        $themeListArr = array();
        foreach ($themeListTbl as $tvalue)
        {
            array_push($themeListArr, $tvalue->title);
        }

        $object->themes = $themeListArr;

        array_push($objectArr, $object);
    }

    $feedbackList = $objectArr;

    $themeList = DB::table('iot_themes')->where('status','Active')->get();

    return view('admin.IOT.iotFeedbackList',compact('iotFeedTbl','feedbackListTbl','feedbackList','themeList'));

}  */


/*do complete feedback*/
public function completeFeedback(Request $request)
{
    $feedbackId = base64_decode($request->id);

    DB::table('iot_feedbacks')->where('id', $feedbackId)->update(['status'=>'Completed']);

    return redirect()->back();
}


/*get create theme page*/
public function createTheme()
{
    $organisations = DB::table('organisations')->where('status','Active')->get();

    $types = DB::table('iot_theme_category')->where('status','Active')->get();

    return view('admin.IOT.iotAddTheme',compact('organisations','types'));
}

/*get theme list*/
public function getThemeList(Request $request)
{

    $id = base64_decode($request->id);
    $orgId = base64_decode($request->orgId);

    $themeListTblQuery = DB::table('iot_themes')
    ->select('iot_themes.id','iot_themes.dateOpened','iot_themes.title','iot_themes.description','iot_themes.submission','iot_themes.initialLikelihood','iot_themes.initialConsequence','iot_themes.currentLikelihood','iot_themes.currentConsequence','iot_themes.themeStatus','iot_themes.linkedAction','organisations.organisation','iot_theme_category.title AS categoryTitle','iot_themes.orgId')

    ->leftJoin('organisations','organisations.id','iot_themes.orgId')
    ->leftJoin('iot_theme_category','iot_theme_category.id','iot_themes.type')    
    ->where('iot_themes.status','Active');

    if($id) 
    {
        $themeListTblQuery->where('iot_themes.type',$id);
    }
    if($orgId)
    {
        $themeListTblQuery->where('iot_themes.orgId',$orgId);
    }

    $themeListTblpagi = $themeListTblQuery->orderBy('id','DESC')->paginate(6);

    $objectArr = array();
    foreach ($themeListTblpagi as  $rValue)
    {

        $object                     = new Controller();

        $object->id                 = $rValue->id;
        $object->dateOpened         = $rValue->dateOpened;
        $object->title              = $rValue->title;
        $object->description        = $rValue->description;
        $object->initialLikelihood  = $rValue->initialLikelihood;        
        $object->initialConsequence = $rValue->initialConsequence;
        $object->currentLikelihood  = $rValue->currentLikelihood;
        $object->currentConsequence = $rValue->currentConsequence;
        $object->themeStatus        = $rValue->themeStatus;
        $object->linkedAction       = $rValue->linkedAction;
        $object->organisation       = $rValue->organisation;
        $object->categoryTitle      = $rValue->categoryTitle;
        $object->orgId              = $rValue->orgId;

        $themeListTbl = DB::table('iot_allocated_themes')->select('feedbackId')->where('themeId',$rValue->id)->where('status','Active')->get();

        $feedbackIdArr = array();
        foreach ($themeListTbl as $tvalue)
        {
            array_push($feedbackIdArr, $tvalue->feedbackId);
        }

        $object->submission = $feedbackIdArr;

        //action list
        $allocatedActionArr = json_decode($rValue->linkedAction);

        $actionTitleArr = array();
        if($allocatedActionArr)
        {
            foreach ($allocatedActionArr as $actionId)
            {
                $action = DB::table('actions')->select('id')->where('status','Active')->where('id',$actionId)->first();

                if($action)
                {
                  array_push($actionTitleArr, $action->id);
              }

          }

      }

      $object->actions  = implode(', ', $actionTitleArr);

      array_push($objectArr, $object);
  }

  $themeListTbl = $objectArr;
  
  return view('admin.IOT.iotThemeList',compact('themeListTbl','themeListTblpagi'));

} 

/*store theme*/
public function storeCustomTheme()
{

    $dateOpened         = Input::get('date_opened');
    $title              = Input::get('title');
    $description        = Input::get('description');
    $type               = Input::get('type');
    $orgId              = Input::get('organisation');
    $status             = Input::get('status');
    $initialLikelihood  = Input::get('initial_likelihood');
    $initialConsequence = Input::get('initial_consequence');
    $currentLikelihood  = Input::get('current_likelihood');
    $currentConsequence = Input::get('current_consequence');
    $linkedAction       = Input::get('linked_action');

    $insertArray = array(
        "dateOpened" => date('Y-m-d',strtotime($dateOpened)),
        "title" => $title,
        "description" => $description,
        "type" => $type,
        "orgId" => $orgId, 
        "submission" => '',
        "initialLikelihood" => $initialLikelihood, 
        "initialConsequence" => $initialConsequence,
        "currentLikelihood" => $currentLikelihood,
        "currentConsequence" => $currentConsequence,
        "linkedAction" => json_encode($linkedAction),
        "themeStatus" => $status,
        "status" => 'Active',
        "created_at"=> date('Y-m-d H:i:s')
        );

    DB::table('iot_themes')->insertGetId($insertArray);
    //redirect to following link
    return redirect('admin/theme-list/'.base64_encode($orgId).'/'.base64_encode($type))->with('message','Record Added Successfully.');

}

/*get edit page of custom theme*/
public function editCustomTheme(Request $request)
{


    $url = URL::previous();

    $last = explode("/", $url);

    if($last[6] != base64_encode(0))
    {

        $orgId = ($request->segment(3));

        $organisations = DB::table('organisations')->select('ImageURL')->where('id',$orgId)->first();

        if(!empty($organisations->ImageURL))
        {
            $org_detail = base64_encode($orgId)."-".$organisations->ImageURL;
            View::share('org_detail', $org_detail);
        }
        else
        {
            $org_detail = "";
            View::share('org_detail', $org_detail);
        }
    }

    $themeId = base64_decode($request->id);

    $themeTbl = DB::table('iot_themes')->where('id',$themeId)->first();

    $organisations = DB::table('organisations')->where('status','Active')->get();
    $types = DB::table('iot_theme_category')->where('status','Active')->get();

    $selectedActionArr = array();
    if(json_decode($themeTbl->linkedAction))
    {
        $selectedActionArr = json_decode($themeTbl->linkedAction);
    }

    $actionList = DB::table('actions')->select('id')->where('status','Active')->where('orgId',$themeTbl->orgId)->orderBy('id','DESC')->get();

    return view('admin.IOT.iotEditTheme',compact('actionList','selectedActionArr','themeTbl','organisations','types'));    
}

/*update custom theme*/
public function updateCustomTheme()
{

    $dateOpened         = Input::get('date_opened');
    $title              = Input::get('title');
    $description        = Input::get('description');
    $type               = Input::get('type');
    $orgId              = Input::get('organisation');
    $status             = Input::get('status');
    $initialLikelihood  = Input::get('initial_likelihood');
    $initialConsequence = Input::get('initial_consequence');
    $currentLikelihood  = Input::get('current_likelihood');
    $currentConsequence = Input::get('current_consequence');
    $linkedAction       = Input::get('linked_action');

    $themeId = base64_decode(Input::get('themeId'));

    $updateArray = array(
        "dateOpened" => date('Y-m-d',strtotime($dateOpened)),
        "title" => $title,
        "description" => $description,
        "type" => $type,
        "orgId" => $orgId, 
        "submission" => '',
        "initialLikelihood" => $initialLikelihood, 
        "initialConsequence" => $initialConsequence,
        "currentLikelihood" => $currentLikelihood,
        "currentConsequence" => $currentConsequence,
        "linkedAction" => json_encode($linkedAction),
        "themeStatus" => $status,       
        "updated_at"=> date('Y-m-d H:i:s')
        );

    DB::table('iot_themes')->where('id',$themeId)->update($updateArray);

    //redirect to following link
    return redirect('admin/theme-list/'.base64_encode($orgId).'/'.base64_encode($type))->with('message','Record updated successfully.');
}

/*get chat/msg screen*/
public function getChatMessages(Request $request)
{

    $url = URL::previous();

    $last = explode("/", $url);

    if($last[6] != base64_encode(0))
    {

        $orgId = ($request->segment(3));

        $organisations = DB::table('organisations')->select('ImageURL')->where('id',$orgId)->first();

        if(!empty($organisations->ImageURL))
        {
            $org_detail = base64_encode($orgId)."-".$organisations->ImageURL;
            View::share('org_detail', $org_detail);
        }
        else
        {
            $org_detail = "";
            View::share('org_detail', $org_detail);
        }
    }

    $feedbackId = base64_decode($request->feedbackId);
    
    $feedbackListQuery = DB::table('iot_feedbacks')
    ->select('iot_feedbacks.id','iot_feedbacks.message','iot_feedbacks.image','iot_feedbacks.userId','iot_feedbacks.created_at','iot_feedbacks.status','users.email','organisations.organisation','offices.office','all_department.department')
    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->leftJoin('organisations','organisations.id','users.orgId')
    ->leftJoin('offices','offices.id','users.officeId')
    ->leftJoin('departments','departments.id','users.departmentId')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('iot_feedbacks.id',$feedbackId);
    $feedbackListTbl = $feedbackListQuery->get();

    $objectArr = array();
    foreach ($feedbackListTbl as  $rValue)
    {
        $object    = new Controller();

        $object->id           = $rValue->id;
        $object->message      = $rValue->message;
        $object->image        = $rValue->image;
        $object->created_at   = $rValue->created_at;
        $object->email        = $rValue->email;
        $object->organisation = $rValue->organisation;
        $object->office       = $rValue->office;
        $object->department   = $rValue->department;
        $object->userId       = $rValue->userId;
        $object->status       = $rValue->status;
        
        $themeListTbl = DB::table('iot_allocated_themes')
        ->select('iot_themes.title')
        ->leftJoin('iot_themes','iot_themes.id','iot_allocated_themes.themeId')
        ->where('feedbackId',$rValue->id)->where('iot_allocated_themes.status','Active')
        ->get();

        $themeListArr = array();
        foreach ($themeListTbl as $tvalue)
        {
            array_push($themeListArr, $tvalue->title);
        }

        $object->themes = $themeListArr;

        array_push($objectArr, $object);
    }

    $feedbackList = $objectArr;

    //get messages
    $messages = DB::table('iot_messages')
    ->select('iot_messages.sendTo','iot_messages.sendFrom','users.name','iot_messages.message','iot_messages.file','users.imageUrl','iot_messages.created_at')
    ->leftJoin('users','users.id','iot_messages.sendFrom')
    ->where('iot_messages.feedbackId',$feedbackId)
    ->where('iot_messages.status','Active')->orderBy('iot_messages.id','DESC')->get();

    return view('admin.IOT.iotChatbox',compact('feedbackList','messages'));
}

/*send msg */
public function sendChatMessages(Request $request)
{

    $sendFrom   = Input::get('sendFrom');
    $sendTo     = Input::get('sendTo');
    $message    = Input::get('message');
    $feedbackId = Input::get('feedbackId');

    if($request->hasfile('file'))
    {
        $image = $request->file('file'); 

        $imageName   = 'iot_'.time().'.'.$image->getClientOriginalExtension();
        $destination = public_path('uploads/iot_files/');
        $image->move($destination, $imageName);

        $insertmsgArray['sendFrom']   = $sendFrom;
        $insertmsgArray['sendTo']     = $sendTo;   
        $insertmsgArray['feedbackId'] = $feedbackId;
        $insertmsgArray['status']     = 'Active';
        $insertmsgArray['created_at'] = date('Y-m-d H:i:s');
        $insertmsgArray['file']       = $imageName;

        $insertGetId = DB::table('iot_messages')->insertGetId($insertmsgArray);

        //Notification part
        $user = DB::table('users')->select('fcmToken')->where('id', $sendTo)->where('status','Active')->first();

        $fcmToken = '';
        if($user)
        {
            $fcmToken = $user->fcmToken;
        }

        //get badge count 
        $totbadge = app('App\Http\Controllers\API\ApiDotController')->getIotNotificationBadgeCount(array('userId'=>$sendTo));

        $title    = 'Improving';
        $message  = 'You have received a new message';

        $notiArray = array('fcmToken'=>$fcmToken, 'title'=>$title ,'message'=>$message, 'totbadge'=>$totbadge, 'feedbackId'=>$feedbackId);

        $test = app('App\Http\Controllers\Admin\CommonController')->sendFcmNotify($notiArray);

        $notificationArray = array(
          'to_bubble_user_id'  => $sendTo,
          'from_bubble_user_id'=> $sendFrom,
          'title' => $title,
          'description' => $message,  
          'feedbackId' => $feedbackId, 
          'notificationType' => 'chat',
          'created_at' => date('Y-m-d H:i:s')
          );

        DB::table('iot_notifications')->insertGetId($notificationArray);
    //end notification
    }

    if($message)
    {
        $insertArray['sendFrom']   = $sendFrom;
        $insertArray['sendTo']     = $sendTo;   
        $insertArray['feedbackId'] = $feedbackId;
        $insertArray['status']     = 'Active';
        $insertArray['created_at'] = date('Y-m-d H:i:s');
        $insertArray['message']    = $message;

        $insertGetId = DB::table('iot_messages')->insertGetId($insertArray);


            //Notification part
        $user = DB::table('users')->select('fcmToken')->where('id', $sendTo)->where('status','Active')->first();

        $fcmToken = '';
        if($user)
        {
            $fcmToken = $user->fcmToken;
        }

        //get badge count 
        $totbadge = app('App\Http\Controllers\API\ApiDotController')->getIotNotificationBadgeCount(array('userId'=>$sendTo));

        $title    = 'Improving';
        $message  = 'You have received a new message';

        $notiArray = array('fcmToken'=>$fcmToken, 'title'=>$title ,'message'=>$message, 'totbadge'=>$totbadge, 'feedbackId'=>$feedbackId);

        $test = app('App\Http\Controllers\Admin\CommonController')->sendFcmNotify($notiArray);

        $notificationArray = array(
          'to_bubble_user_id'  => $sendTo,
          'from_bubble_user_id'=> $sendFrom,
          'title' => $title,
          'description' => $message,  
          'feedbackId' => $feedbackId, 
          'notificationType' => 'chat',
          'created_at' => date('Y-m-d H:i:s')
          );

        DB::table('iot_notifications')->insertGetId($notificationArray);
    ///end notification
    }




    $messages = DB::table('iot_messages')
    ->select('iot_messages.sendTo','iot_messages.sendFrom','users.name','iot_messages.message','iot_messages.file','users.imageUrl','iot_messages.created_at')->leftJoin('users','users.id','iot_messages.sendFrom')->where('iot_messages.feedbackId',$feedbackId)->where('iot_messages.status','Active')->orderBy('iot_messages.id','DESC')->get();

    $htmlArr = array();
    foreach($messages as $msg)
    {
        $html = '';
        if($msg->sendFrom !=1)
        {
            if($msg->message)
            {
                $html .= '<li class="sent"><div class="user-img"><img src="' .url("public/uploads/user_profile/".$msg->imageUrl).'"/></div><p>
                <span>'. $msg->message .'</span><time>'.date('H:i a',strtotime($msg->created_at)).'</time></p></li>';
            }

            if($msg->file)
            {
                $html .= '<li class="sent"><img src="'.url("public/uploads/iot_files/".$msg->file).'"/></li>';
            }
        }
        elseif($msg->sendFrom == 1)
        {
           if($msg->message)
           {        
             $html .= '<li class="replies"><div class="user-img"><img src="' .url("public/uploads/user_profile/".$msg->imageUrl).'"/></div><p>
             <span>'. $msg->message .'</span><time>'.date('H:i a',strtotime($msg->created_at)).'</time></p></li>';
         }

         if($msg->file)
         {
            $html .= '<li class="replies"><img src="'.url("public/uploads/iot_files/".$msg->file).'"/></li>';
        }
    }

    array_push($htmlArr, $html);
}

return $htmlArr;

}

/*get msgs by ajax call every time given*/
public function getChatMessagesByAjax()
{

    $feedbackId = Input::get('feedbackId');

    $messages = DB::table('iot_messages')
    ->select('iot_messages.sendTo','iot_messages.sendFrom','users.name','iot_messages.message','iot_messages.file','users.imageUrl','iot_messages.created_at')->leftJoin('users','users.id','iot_messages.sendFrom')->where('iot_messages.feedbackId',$feedbackId)->where('iot_messages.status','Active')->orderBy('iot_messages.id','DESC')->get();

    $htmlArr = array();
    foreach($messages as $msg)
    {
        $html = '';
        if($msg->sendFrom !=1)
        {
           if($msg->message)
           {
            $html .= '<li class="sent"><div class="user-img"><img src="' .url("public/uploads/user_profile/".$msg->imageUrl).'"/></div><p>
            <span>'. $msg->message .'</span><time>'.date('H:i a',strtotime($msg->created_at)).'</time></p></li>';
        }

        if($msg->file)
        {
            $html .= '<a href="'.url("public/uploads/iot_files/". $msg->file).'" target="blank"><li class="sent"><img src="'.url("public/uploads/iot_files/".$msg->file).'"/></li></a>';
        }
    }
    elseif($msg->sendFrom == 1)
    {
     if($msg->message)
     {

         $html .= '<li class="replies"><div class="user-img"><img src="' .url("public/uploads/user_profile/".$msg->imageUrl).'"/></div><p>
         <span>'. $msg->message .'</span><time>'.date('H:i a',strtotime($msg->created_at)).'</time></p></li>';
     }

     if($msg->file)
     {
        $html .= '<a href="'.url("public/uploads/iot_files/". $msg->file).'" target="blank"><li class="replies"><img src="'.url("public/uploads/iot_files/".$msg->file).'"/></li></a>';
    }
}

array_push($htmlArr, $html);
}

return $htmlArr;
}

/*update feedback theme */
public function updateFeedbackTheme()
{
    $feedbackId = Input::get('feedbackId');

    $themeIdArr = Input::get('themeId');

    DB::table('iot_allocated_themes')->where('feedbackId', $feedbackId)->update(['status'=>'Inactive']);
    foreach ($themeIdArr as $themeId)
    {
        $isThemeAllocated = DB::table('iot_allocated_themes')->where('feedbackId',$feedbackId)->where('themeId', $themeId)->first();

        if($isThemeAllocated)
        {
            $updateArray = array(               
                'status'=> 'Active',
                'updated_at'=> date('Y-m-d H:i:s')
                );
            DB::table('iot_allocated_themes')->where('feedbackId', $feedbackId)->where('themeId', $themeId)->update($updateArray);
        }
        else
        {
            $insertArray = array(
                'feedbackId'=> $feedbackId,
                'themeId'=> $themeId,               
                'status'=> 'Active',
                'updated_at'=> date('Y-m-d H:i:s')
                );
            DB::table('iot_allocated_themes')->insertGetId($insertArray);
        }
    }
}

/*get theme add modal */
public function getThemeModal()
{
    $feedbackId = Input::get('feedbackId');

    $feedback = DB::table('iot_feedbacks')->where('id', $feedbackId)->first();

    if($feedback)
    {
      $orgId = $feedback->orgId;   
  }

  $themeSelectedTbl = DB::table('iot_allocated_themes')->where('status','Active')->where('feedbackId',$feedbackId)->get();

  $selectedfeedbackArr = array();
  foreach ($themeSelectedTbl as $value)
  {
    array_push($selectedfeedbackArr, $value->themeId);
}

$themeList = DB::table('iot_themes')->where('status','Active')->where('orgId',$orgId)->orderBy('id','DESC')->get();

return view('admin.IOT.iotThemeModal',compact('themeList','feedbackId','selectedfeedbackArr'));

}


/*add new theme by ajax when modal is open*/
public function addNewThemeByAjax()
{

    $feedbackId = Input::get('feedbackId');

    $feedback = DB::table('iot_feedbacks')->where('id', $feedbackId)->first();

    $orgId = '';
    if($feedback)
    {
      $orgId = $feedback->orgId;   
  }

  $insertArray = array(
    "dateOpened" => date('Y-m-d'),
    "title" => Input::get('themeTitle'),
    "description" => '',
    "type" => 1,
    "orgId" => $orgId, 
    "submission" => '',
    "initialLikelihood" => '0', 
    "initialConsequence" => '0',
    "currentLikelihood" => '0',
    "currentConsequence" => '0',
    "linkedAction" => '',
    "themeStatus" => 'Closed',
    "status" => 'Active',
    "created_at"=> date('Y-m-d H:i:s')
    );

  if(Input::get('themeTitle'))
  {
    DB::table('iot_themes')->insertGetId($insertArray);
}

$themeList = DB::table('iot_themes')->select('id','title')->where('status','Active')->where('orgId',$orgId)->orderBy('id','DESC')->get();

$htmlContainer = '';
$htmlOption = '';
foreach($themeList as $value)
{
    $htmlContainer.='<li><a tabindex="0"><label class="checkbox" title="'.$value->title.'"><input type="checkbox" value="'.$value->id.'">'.$value->title.'</label></a></li>';

    $htmlOption.='<option value="'.$value->id.'">'.$value->title.'</option>'; 
}

$htmlArr = array('htmlContainer'=>$htmlContainer, 'htmlOption'=>$htmlOption);

print_r(json_encode($htmlArr));

}

/*get actions list*/
public function getActionsByAjax()
{

    $orgId = Input::get('orgId');

    $actionList = DB::table('actions')->select('id')->where('status','Active')->where('orgId',$orgId)->get();

    $htmlContainer = '';
    $htmlOption = '';
    foreach ($actionList as $value)
    {
        $htmlContainer.='<li><a tabindex="0"><label class="checkbox" title="'.$value->id.'"><input type="checkbox" value="'.$value->id.'">'.$value->id.'</label></a></li>';

        $htmlOption.='<option value="'.$value->id.'">'.$value->id.'</option>'; 
    }

    $htmlArr = array('htmlContainer'=>$htmlContainer, 'htmlOption'=>$htmlOption);

    print_r(json_encode($htmlArr));

}

public function deleteThemes()
{
    $orgId = base64_decode(Input::get('orgId'));
    $themeId = base64_decode(Input::get('themeId'));
    
    DB::table('iot_themes')->where('id',$themeId)->where('orgId',$orgId)->delete();

    return json_encode(array('status'=>200,'message'=>'Theme Successfully Deleted'));
}

public function deleteFeedbacks()
{
    $orgId = base64_decode(Input::get('orgId'));
    $feedbackId = base64_decode(Input::get('feedbackId'));

    $feedback = DB::table('iot_feedbacks')->where('id',$feedbackId)->where('orgId',$orgId)->delete();

    $feedbackMessage = DB::table('iot_messages')->where('feedbackId',$feedbackId)->delete();
    return json_encode(array('status'=>200,'message'=>'Feedbacks Successfully Deleted'));
}

}