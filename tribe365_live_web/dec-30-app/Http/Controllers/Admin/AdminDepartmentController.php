<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Validator;
use Hash;
use Illuminate\Support\Facades\Input;
use Config;
use Image;
use App\Organisation;

class AdminDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = DB::table('all_department')
        ->where('status','Active')
        ->orderBy('department','asc')
        ->paginate(10);

        return view('admin/department/getCustomDepartments',compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/department/addDepartment');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postData = Input::get();

        $department_name = Input::get('department');
        $orgId = Input::get('orgId') ? Input::get('orgId'):0;

        $rules = array('department'=>'required|max:100');

        $validator =  Validator::make(Input::all(),$rules);

        if($validator->fails())
        {
          return redirect()->back()->withInput()->withErrors($validator->errors());
      }

      $department = DB::table('all_department')
      ->where('department',$department_name)
      ->where('status','Active')
      ->get();
      

      if (count($department))
      {
        return redirect()->back()->withErrors('Department already exists.');
     
    }
    else
    {

        $insertArray = array(
          'department'=> $department_name,              
          'orgId'=> $orgId,    
          'status'=>'Active', 
          'created_at'=>date('Y-m-d H:i:s')
      );

        $status = DB::table('all_department')->insertGetId($insertArray);

        return redirect('admin/departments')->with(['message'=>'Department added successfully.']);

    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $deptId = base64_decode($id);

        $departments = DB::table('all_department')
        ->where('id',$deptId)  
        ->first();

        return view('admin/department/editDepartment',compact('departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $department_name = Input::get('department');

        $rules = array('department'=>'required|max:100');

        $validator =  Validator::make(Input::all(),$rules);

        if($validator->fails())
        {
          return redirect()->back()->withInput()->withErrors($validator->errors());
      }

      $department = DB::table('all_department')
      ->where('department',$department_name)
      ->where('status','Active')
      ->get();
      

      if (count($department))
      {
        return redirect()->back()->withErrors('Department already exists.');
    }

    $updatetArray = array(
        'department'=> $department_name,         
        'updated_at'=>date('Y-m-d H:i:s')
    );

    DB::table('all_department')
    ->where('id',base64_decode($id))
    ->update($updatetArray); 

    return redirect('admin/departments')->with(['message'=>'Department Updated successfully.']);

}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $updatetArray = array('status'=>'Inactive');

        DB::table('all_department')
        ->where('id',base64_decode($id)) 
        ->update($updatetArray);  

        return redirect('admin/departments')->with('message','Department Deleted Successfully.');
    }
}
