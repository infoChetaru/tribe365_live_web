<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Validator;
use Hash;
use Illuminate\Support\Facades\Input;
use Config;
use View;

class AdminActionController extends Controller
{

 public function __construct(Request $request)
 {
  $orgid = base64_decode($request->segment(3));

  $organisations = DB::table('organisations')
  ->select('ImageURL')
  ->where('id',$orgid)
  ->first();
  

  if(!empty($organisations->ImageURL))
  {
    $org_detail = base64_encode($orgid)."-".$organisations->ImageURL;

    View::share('org_detail', $org_detail);
  }
  else
  {
    $org_detail = "";

    View::share('org_detail', $org_detail);

  }


}


public function addAction($id)
{

  if(empty($id)){

    return view('admin/admin-organisation');
  }

  $actionResponsibles = DB::table('actionResponsibleStatus')->where('status','Active')->get();

  $resUsers = DB::table('users')
    // ->where('status','Active')
  ->where('roleId',3)->where('orgId',base64_decode($id))->get();

  $themeList = DB::table('iot_themes')->where('status','Active')->where('orgId',base64_decode($id))->orderBy('id','DESC')->get();

  return view('admin/actions/addAction',compact('themeList','actionResponsibles','id','resUsers'));
}

/*get DOT action list*/
public function getActionList($id)
{

  if(empty($id))
  {
    return view('admin/admin-organisation');
  }

  $orgId = base64_decode($id);

  $actionTbl = DB::table('actions')
  ->select('actions.themeId','actions.id','actions.userId','actions.description','actions.startedDate','actions.dueDate','actions.responsibleId','actions.orgStatus','actions.orgId','cuser.name AS cname','offices.office','all_department.department','ruser.name AS rname','actionResponsibleStatus.name AS tier')
  ->leftJoin('users AS cuser','cuser.id', '=', 'actions.userId')   
  ->leftJoin('users AS ruser','ruser.id', '=', 'actions.responsibleUserId')   
  ->leftJoin('offices','offices.id', '=', 'actions.officeId')         
  ->leftJoin('departments','departments.id', '=', 'actions.departmentId') 
  ->leftJoin('all_department','all_department.id', '=', 'departments.departmentId')   
  ->leftJoin('actionResponsibleStatus','actionResponsibleStatus.id', '=', 'actions.responsibleId')   
  ->where('actions.orgId', $orgId)
  ->where('actions.status', 'Active')         
  ->orderBy('actions.id','DESC')
  ->get();

  $actions = array();
  foreach ($actionTbl as $value)
  {
    $obj = new Controller();

    $obj->id            = $value->id;
    $obj->userId        = $value->userId;
    $obj->description   = $value->description;
    $obj->startedDate   = $value->startedDate;
    $obj->dueDate       = $value->dueDate;
    $obj->responsibleId = $value->responsibleId;
    $obj->orgStatus     = $value->orgStatus;
    $obj->orgId         = $value->orgId;
    $obj->cname         = $value->cname;
    $obj->office        = $value->office;
    $obj->department    = $value->department;
    $obj->rname         = $value->rname;
    $obj->tier          = $value->tier;

    $allocatedThemearr = json_decode($value->themeId);

    $themeTitleArr = array();
    if ($allocatedThemearr)
    {

      foreach ($allocatedThemearr as $themeId)
      {

        $theme = DB::table('iot_themes')->select('title')->where('status','Active')->where('id',$themeId)->first();

        if($theme)
        {
          array_push($themeTitleArr, $theme->title);
        }

      }

    }
    
    $obj->themeTitle  = implode(', ', $themeTitleArr);

    array_push($actions, $obj);
  }

  return view('admin/actions/actionList',compact('id','actions'));

}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $orgId = Input::get('orgId');

      $user = Auth::user();

      $insertArray['startedDate']    = date('Y-m-d H:i:s', strtotime(Input::get('start_date')));
      $insertArray['dueDate']        = date('Y-m-d H:i:s', strtotime(Input::get('end_date')));
      $insertArray['description']    = Input::get('description');
      $insertArray['responsibleId']  = Input::get('responiblityValue');
      $insertArray['orgId']          = base64_decode($orgId);
      $insertArray['orgStatus']      = Input::get('status');
      $insertArray['status']         = 'Active';    
      $insertArray['created_at']     = date('Y-m-d H:i:s');
      $insertArray['userId']         = $user->id;

      $insertArray['forUserId']      = Input::get('userId');
      $insertArray['officeId']       = Input::get('officeId');   
      $insertArray['departmentId']   = Input::get('departmentId');
      $insertArray['responsibleUserId'] = Input::get('resUsers');
      $insertArray['themeId']           = json_encode(Input::get('themeId'));

      DB::table('actions')->insertGetId($insertArray);

      $actionName = DB::table('actionResponsibleStatus')
      ->where('id',$insertArray['responsibleId'])
      ->first();

      return redirect("admin/action-list/$orgId")->with('message', $actionName->name.' - Action added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($actid)
    {
      $actionId = base64_decode($actid);

      $actions = DB::table('actions')->where('id',$actionId)->first();

      $id = "";
      $selectedThemeArr = array();
      if($actions)
      {
        $id = base64_encode($actions->orgId);
        if(json_decode($actions->themeId))
        {
         $selectedThemeArr = json_decode($actions->themeId);
       }

     }

     $actionResponsibles = DB::table('actionResponsibleStatus')->where('status','Active')->get();

     $resUsers = DB::table('users')->where('roleId',3)->where('orgId',base64_decode($id))->get();

     $themeList = DB::table('iot_themes')->where('status','Active')->where('orgId',base64_decode($id))->orderBy('id','DESC')->get();

     return view('admin/actions/editAction',compact('themeList','actionResponsibles','id','resUsers','actions','selectedThemeArr'));
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $actionId = base64_decode($id);

      $orgId = Input::get('orgId');

      $insertArray['startedDate']    = date('Y-m-d H:i:s', strtotime(Input::get('start_date')));
      $insertArray['dueDate']        = date('Y-m-d H:i:s', strtotime(Input::get('end_date')));
      $insertArray['description']    = Input::get('description');
      $insertArray['responsibleId']  = Input::get('responiblityValue');      
      $insertArray['orgStatus']      = Input::get('status');       
      $insertArray['updated_at']     = date('Y-m-d H:i:s');

      if(Input::get('themeUpdate'))
      {
        $insertArray['themeId'] = json_encode(Input::get('themeId'));
      }

      if(Input::get('userId')) {
       $insertArray['forUserId']      = Input::get('userId');
     }

     if(Input::get('officeId')) {
      $insertArray['officeId']        = Input::get('officeId'); 
    }

    if (Input::get('departmentId')) {
      $insertArray['departmentId']    = Input::get('departmentId');
    }

    if(Input::get('resUsers')) {
      $insertArray['responsibleUserId'] = Input::get('resUsers');
    }

    DB::table('actions')->where('id', $actionId)->update($insertArray);

    $actionName = DB::table('actionResponsibleStatus')->where('id',$insertArray['responsibleId'])->first();

    return redirect("admin/action-list/$orgId")->with('message', $actionName->name.' - Action updated successfully.');
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCommentsList($id)
    {
      $actionId = base64_decode($id);

      $actionTbl = DB::table('actions')
      ->select('actions.themeId','actions.id','actions.userId','actions.description','actions.startedDate','actions.dueDate','actions.responsibleId','actions.orgStatus','actions.orgId','cuser.name AS cname','offices.office','all_department.department','ruser.name AS rname')
      
      ->leftJoin('users AS cuser','cuser.id', '=', 'actions.userId')   
      ->leftJoin('users AS ruser','ruser.id', '=', 'actions.responsibleUserId')  
      ->leftJoin('offices','offices.id', '=', 'actions.officeId')         
      ->leftJoin('departments','departments.id', '=', 'actions.departmentId') 
      ->leftJoin('all_department','all_department.id', '=', 'departments.departmentId')   
      ->where('actions.id',$actionId)
      ->get();

      $actions = array();
      foreach ($actionTbl as $value)
      {
        $obj = new Controller();

        $obj->id            = $value->id;
        $obj->userId        = $value->userId;
        $obj->description   = $value->description;
        $obj->startedDate   = $value->startedDate;
        $obj->dueDate       = $value->dueDate;
        $obj->responsibleId = $value->responsibleId;
        $obj->orgStatus     = $value->orgStatus;
        $obj->orgId         = $value->orgId;
        $obj->cname         = $value->cname;
        $obj->office        = $value->office;
        $obj->department    = $value->department;
        $obj->rname         = $value->rname;
        

        $allocatedThemearr = json_decode($value->themeId);

        $themeTitleArr = array();
        if ($allocatedThemearr)
        {

          foreach ($allocatedThemearr as $themeId)
          {

            $theme = DB::table('iot_themes')->select('title')->where('status','Active')->where('id',$themeId)->first();

            if($theme)
            {
              array_push($themeTitleArr, $theme->title);
            }

          }

        }

        $obj->themeTitle  = implode(', ', $themeTitleArr);

        array_push($actions, $obj);
      }

      $actionsComments = DB::table('actions_comment as AC')
      ->select('AC.id','AC.userId','AC.comment','AC.created_at','users.name')
      ->leftJoin('users','users.id', '=', 'AC.userId')        
      ->where('AC.actionId',$actionId)
      ->orderBy('AC.id','DESC')
      ->get();  

      return view('admin/actions/actionComments',compact('actions','actionsComments'));
    }

    /*add new comment in action comment list*/
    public function addComment()
    {       
      date_default_timezone_set('Europe/London');
      $user = Auth::user();
      $insertArray['actionId']       = Input::get('actionId');  
      $insertArray['userId']         = $user->id;  
      $insertArray['comment']        = Input::get('comment');
      $insertArray['created_at']     = date('Y-m-d H:i:s');

      $status = DB::table('actions_comment')->insertGetId($insertArray);

      $actionId = base64_encode(Input::get('actionId'));

      return redirect("admin/action-comments/$actionId"); 
    }

    /*update status of the action*/
    public function updateActionStatus()
    {
      $actionId = Input::get('actionId');

      $insertArray['orgStatus'] = Input::get('status');  

      DB::table('actions')
      ->where('id', $actionId)
      ->update($insertArray);

      echo "success";
    }

    public function getResUser()
    {

      $orgId = base64_decode(Input::get('orgId'));

      if(Input::get('section')=="office")
      {
       $users = DB::table('users')
       ->where('orgId',$orgId) 
       ->where('officeId',Input::get('id'))
       ->where('status','Active')
       ->where('roleId',3)
       ->get();
     }
     elseif(Input::get('section')=="department")
     {
      $users = DB::table('users')
      ->where('orgId',$orgId) 
      ->where('departmentId',Input::get('id'))
      ->where('status','Active')
      ->where('roleId',3)
      ->get();
    }
    else
    {
     $users = DB::table('users')
     ->where('orgId',$orgId) 
     ->where('status','Active')
     ->where('roleId',3)
     ->get();

   }
   
   
   
   $select = '<select id="resUsers" name="resUsers" class="form-control" required>';
   $select .=' <option value="" disabled="" selected="">Responsible Person</option>';

   foreach ($users as $value) {
    $select .='<option value="'.$value->id.'">'.ucfirst($value->name).'</option>';
  }

  $select .='</select>';

  return $select;

}
/*delete action*/
public function deleteAction($id)
{

  DB::table('actions')->where('id',base64_decode($id))->update(['status'=>'Inactive']);  

  return redirect()->back()->with('message','Action Deleted Successfully.');
}

/*get theme add modal*/
public function getActionThemeModal()
{

  $selectedThemeArr = array();
  $orgId = base64_decode(Input::get('orgId'));

  if(Input::get('selectedThemeArr'))
  {
    $selectedThemeArr = Input::get('selectedThemeArr');
  }
  

  $themeList = DB::table('iot_themes')->where('status','Active')->where('orgId',$orgId)->orderBy('id','DESC')->get();

  return view('admin.actions.actionThemeModal',compact('themeList','orgId','selectedThemeArr'));

}

/*add new theme by ajax when modal is open*/
public function addNewActionThemeByAjax()
{

  $orgId = Input::get('orgId');

  $insertArray = array(
    "dateOpened" => date('Y-m-d'),
    "title" => Input::get('themeTitle'),
    "description" => '',
    "type" => 1,
    "orgId" => $orgId, 
    "submission" => '',
    "initialLikelihood" => '0', 
    "initialConsequence" => '0',
    "currentLikelihood" => '0',
    "currentConsequence" => '0',
    "linkedAction" => '',
    "themeStatus" => 'Closed',
    "status" => 'Active',
    "created_at"=> date('Y-m-d H:i:s')
  );

  if(Input::get('themeTitle'))
  {
    DB::table('iot_themes')->insertGetId($insertArray);
  }

  $themeList = DB::table('iot_themes')->select('id','title')->where('status','Active')->where('orgId',$orgId)->orderBy('id','DESC')->get();

  $htmlContainer = '';
  $htmlOption    = '';
  foreach ($themeList as $value)
  {
    $htmlContainer.='<li><a tabindex="0"><label class="checkbox" title="'.$value->title.'"><input type="checkbox" value="'.$value->id.'">'.$value->title.'</label></a></li>';

    $htmlOption.='<option value="'.$value->id.'">'.$value->title.'</option>'; 
  }

  $htmlArr = array('htmlContainer'=>$htmlContainer, 'htmlOption'=>$htmlOption);

  print_r(json_encode($htmlArr));

}
}
