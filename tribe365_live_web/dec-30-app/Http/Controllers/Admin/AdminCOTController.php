<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Validator;
use Hash;
use Illuminate\Support\Facades\Input;
use Config;
use View;

class AdminCOTController extends Controller
{
  public function __construct(Request $request)
  {

    $orgid = base64_decode($request->segment(3));

    if(!empty($orgid))
    {

      $organisations = DB::table('organisations')->select('ImageURL')->where('id',$orgid)->first();

      if(!empty($organisations->ImageURL))
      {
        $org_detail = base64_encode($orgid)."-".$organisations->ImageURL;

        View::share('org_detail', $org_detail);
      }
      else
      {
        $org_detail = "";

        View::share('org_detail', $org_detail);
      }
    }
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('admin/COT/cot1');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin/COT/Cot1');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

     $postData = Input::all();

     $cotFunId = Input::get('cotFunId');
     $userId   = Input::get('userId');

     $updateArray['EI']       = Input::get('ei_value');
     $updateArray['EI_score'] = Input::get('ei_score');
     $updateArray['SN']       = Input::get('sn_value');
     $updateArray['SN_score'] = Input::get('sn_score');
     $updateArray['TF']       = Input::get('tn_value');
     $updateArray['TF_score'] = Input::get('tn_score');
     $updateArray['JP']       = Input::get('jp_value');
     $updateArray['JP_score'] = Input::get('jp_score');


     $status = DB::table('cot_functional_lens_records')->where('id',$cotFunId)->first();

     if($status)
     {

       $updateArray['updated_at'] = date('Y-m-d H:i:s');

       DB::table('cot_functional_lens_records')->where('id',$cotFunId)->update($updateArray);

     }else{

       $updateArray['status']     = 'Active';
       $updateArray['userId']     = $userId;
       $updateArray['created_at'] = date('Y-m-d H:i:s');

       DB::table('cot_functional_lens_records')->insertGetId($updateArray);
     }

     return redirect()->back();

   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /*get COT detail page*/
    public function getCotDetail($id)
    {
      $id = base64_decode($id);

      $organisations = DB::table('organisations')       
      ->where('id',$id)
      ->first();

      return view('admin/COT/detailCOT',compact('organisations'));

    }
    
    /*get team role map*/
    public function getTeamRoleMap(Request $request, $id)
    {
      $postData =Input::all();

      $id = base64_decode($id);
     /* echo "<pre>";
      print_r(Input::all());
      die();*/

      $organisations = DB::table('organisations')->where('id',$id)->first();
       //get offices
      $offices = DB::table('offices')->where('orgId',$id)->where('status','Active')->get();
       //get maper key 
      $mapers = DB::table('cot_role_map_options')->where('status','Active')->get();
      //when request comes from post
      $searchKey    = Input::get('searchKey');
      $officeId     = Input::get('officeId');
      $departmentId = Input::get('departmentId');
      $maperKey     = Input::get('maperKey');
      $preferenceKey= Input::get('preferenceKey');

     //set value in session
      $customDept = DB::table('departments')
      ->select('departments.id','all_department.department')
      ->leftJoin('all_department','all_department.id','departments.departmentId')
      ->where('departments.officeId',$officeId)->get();
      
      session()->put('searchKey', $searchKey);
      session()->put('officeId', $officeId);
      session()->put('departmentId', $departmentId);
      session()->put('customDept', $customDept);
      session()->put('maperKey', $maperKey);
      session()->put('preferenceKey', $preferenceKey);
      //end session

      if(!empty($officeId))
      {
        $userWhr['officeId'] = $officeId;
      }

      if(!empty($departmentId))
      {
        $userWhr['departmentId'] = $departmentId;
      }

      $userWhr['orgId'] = $id;
      $userWhr['roleId']=3;
      $userWhr['status']='Active';

      $users  = DB::table('users')
      ->select('id','name','orgId')
      ->where('name','LIKE',"%{$searchKey}%")
      ->where($userWhr)->orderBy('id','DESC')->get();

      $cotRoleMapOptions = DB::table('cot_role_map_options')->where('status','Active')->get();

       
      
      $usersArray = array();

      foreach ($users as $key => $value) 
      {

        foreach($cotRoleMapOptions as $key => $maper)
        {
          $maperCount = DB::table('cot_answers')
          ->where('orgId',$value->orgId)
          ->where('userId',$value->id)
          ->where('cot_role_map_option_id',$maper->id)->where('status','Active')->sum('answer');

          $resultArray = array();

          $Value[$maper->maper_key] = $maperCount;    
            
            
      
          array_push($resultArray, $Value);
        }   

        arsort($resultArray[0]);
        
        $i = 0;
        $prev = "";
        $j = 0;

        $new  = array();
        $new1 = array();

        foreach($resultArray[0] as $key => $val)
        { 
            
         
          if($val != $prev)
          { 
            $i++; 
          }

          $new1['title'] = $key; 
          $new1['value'] = $i;        
          $prev = $val;

          if ($key==$cotRoleMapOptions[0]->maper_key) {
            $new1['priority'] = 1;
          }elseif ($key==$cotRoleMapOptions[1]->maper_key) {
            $new1['priority'] = 2;
          }elseif ($key==$cotRoleMapOptions[2]->maper_key) {
            $new1['priority'] = 3;
          }elseif ($key==$cotRoleMapOptions[3]->maper_key) {
            $new1['priority'] = 4;
          }elseif ($key==$cotRoleMapOptions[4]->maper_key) {
            $new1['priority'] = 5;
          }elseif ($key==$cotRoleMapOptions[5]->maper_key) {
            $new1['priority'] = 6;
          }elseif ($key==$cotRoleMapOptions[6]->maper_key) {
            $new1['priority'] = 7;
          }elseif ($key==$cotRoleMapOptions[7]->maper_key) {
            $new1['priority'] = 8;
          }

          array_push($new,$new1);

        }
        
         //set value in object
        $value->new = $new;
        
    
        if($maperKey)
        {
          foreach ($new as $key => $valueKey)
          {
              
            
            if($valueKey['title']==$maperKey)
            {
                
             if ($preferenceKey=='secondary')
             {
              if($new[1]['priority']==2)
              {
               array_push($usersArray, $value);
             }

           }
           elseif ($preferenceKey=='tertiary')
           {

            if($new[2]['priority']==3)
            {
             array_push($usersArray, $value);
           }

         }
         else if($preferenceKey=='primary')
         {

          if($new[0]['priority']==1)
          {
           array_push($usersArray, $value);
         }

       }
       else
       {
           
        //    echo"<pre>";
        // print_r($new);
        die();
          
         if($new[0]['priority']==1)
         {
           array_push($usersArray, $value);
         }
       }                 
     }
   }
 }
 else
 {
  array_push($usersArray, $value);
}
   
  

}

$usersArray = $this->sortarr($usersArray);
// print_r($usersArray);
// die();
return view('admin/COT/teamRoleMap',compact('organisations','usersArray','offices','mapers','cotRoleMapOptions'));
}

/*sorting array by priority*/
function sortarr($arr1)
{

  $arrUsers = array();
  $newArray = array();
  $tes =array();
  for($i=0; $i<count($arr1); $i++)
  {    
    $tes['id']    = $arr1[$i]->id;
    $tes['name']  = $arr1[$i]->name;
    $tes['orgId'] = $arr1[$i]->orgId;

    $arrt = (array)$arr1[$i]->new;
    usort($arrt, function($x, $y)
    {
      if ($x['value']== $y['value'] ) 
      {
        if($x['priority']<$y['priority'])
        {
          return 0;
        }
        else
        {
          return 1;
        } 
      }
    }); 

    $tt = array();
    foreach($arrt as $key => $value)
    {         
      if ($value['value']!=0)
      {
        $tt[$value['title']] = $key+1;
      } else{
        $tt[$value['title']] = 0;
      }

    }   

    $tes['new'] = $tt;

    array_push($newArray,(object)$tes);

  }
  
  return $newArray;

}



/*update status of has_cot that COT is assigned to organisation*/
public function updateCOTstatus($baseId)
{
  $id = base64_decode($baseId);

  DB::table('organisations')
  ->where('id',$id)
  ->update(['has_cot'=>'1','COT_fun_lens_start_date'=>date('Y-m-d H:i:s')]);

  return redirect("admin/cot-team-role-map/$baseId"); 
}

/*get functional lens list*/
public function getFunctinalLensList(Request $request,$id)
{

  $orgId = base64_decode($id);

  $officeId = $request->officeId;
  $departmentId = $request->departmentId;

  $customDept = DB::table('departments')
  ->select('departments.id','all_department.department')
  ->leftJoin('all_department','all_department.id','departments.departmentId')
  ->where('departments.officeId',$officeId)->get();

  session()->put('officeId', $officeId);
  session()->put('departmentId', $departmentId);
  session()->put('customDept',$customDept);

  $userWhr = array();
  if($officeId)
  {
    $userWhr['users.officeId'] = $officeId;
  }

  if($departmentId)
  {
    $userWhr['users.departmentId'] = $departmentId;
  }

  $organisations = DB::table('organisations')->where('id',$orgId)->first();

  $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get();

  $userTbl = DB::table('users')
  ->select('users.id','users.name','offices.office','all_department.department')
  ->leftJoin('offices', 'offices.id', '=', 'users.officeId')   
  ->leftJoin('departments', 'departments.id', '=', 'users.departmentId')
  ->leftJoin('all_department','all_department.id', '=', 'departments.departmentId') 
  ->where($userWhr)
  ->where('users.orgId',$orgId)
  ->where('users.roleId',3)
  ->where('users.status','Active')
  ->orderBy('users.id','DESC')
  ->paginate(10);

  //get array of key of cot initial values
  
  $variable = app('App\Http\Controllers\Admin\CommonController')->getCotFunLensInitialValue();
  
  $userArray = array();

  foreach ($userTbl as $userValue)
  {

    $scoreArray   = array();

    foreach ($variable as $value)
    {

      $value1 = $value[0];
      $value2 = $value[1];

      $countE = DB::table('cot_functional_lens_answers AS cfla')
      ->select('cflqo.option_name AS optionName')
      ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
      ->where('cfla.userId',$userValue->id)->where('cflqo.initial_value_id',$value1)->get();

      $countI = DB::table('cot_functional_lens_answers AS cfla')
      ->select('cflqo.option_name AS optionName')
      ->leftJoin('cot_functional_lens_question_options AS cflqo', 'cflqo.id', '=', 'cfla.optionId')
      ->where('cfla.userId',$userValue->id)->where('cflqo.initial_value_id',$value2)->get();

      if(count($countE) > count($countI))
      {
        $countValEI = count($countE)-count($countI);
        $initialValEI  = $this->getFunName($value1);
      }
      else if (count($countE) < count($countI))
      {
        $countValEI = count($countI)-count($countE);
        $initialValEI  = $this->getFunName($value2);
      }
      else if(count($countE) == count($countI))
      {
        $countValEI    = count($countE)-count($countI);
        $initialValEI  = $this->getFunName($value1);
      }
      else
      {
        $countValEI    = '-';
        $initialValEI  = '-';
      }

      $keyNameArray = array('value1' => $this->getFunName($value1),'value2'=> $this->getFunName($value2));

      array_push($scoreArray, array('keyNameArray'=>$keyNameArray,'key'=>$initialValEI,'score'=>$countValEI));
    }

    $userValue->scoreArray   = $scoreArray;

    array_push($userArray, $userValue);

  }

  //heading for functional lens
  $keyNameArray = array();
  foreach ($variable as $key => $value)
  {

   $value1 = $value[0];
   $value2 = $value[1];

   $key = array('value1' => $this->getFunName($value1),'value2'=> $this->getFunName($value2));

   array_push($keyNameArray, $key);
 }

 /*print_r($keyNameArray);
 die();*/

 return view('admin/COT/listCOTfunctionalLens',compact('userArray','userTbl','organisations','offices','keyNameArray'));
}

public function getFunName($value)
{

  $valuesKey = '';
  $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$value)->first();  
  if(!empty($table))
  {
    $valuesKey = $table->value;
  }
  return $valuesKey;
}

//manage functinal lens detail page
public function manageFunctionalLensList(Request $request)
{

  $initialValueListTable = DB::table('cot_functional_lens_initial_value_records')->where('status','Active')->get();

  $insightForStrengthValueListTable = DB::table('cot_functional_lens_insight_for_strength_value_records')
  ->where('status','Active')  
  ->get();

  //get tribe tips list
  $tribeTipsArray = DB::table('cot_functional_lens_tribe_tips_records')->where('status','Active')->get();

  $tribeTipsList = array();
  foreach ($tribeTipsArray as $key => $value) 
  {
    $seekArray = DB::table('cot_functional_lens_tribe_tips_seek_persuade_value_records')
    ->select('id','value')
    ->where('lens_tribe_tips_id',$value->id)
    ->where('status','Active')
    ->where('value_type','seek')
    ->get();

    $persuadeArray = DB::table('cot_functional_lens_tribe_tips_seek_persuade_value_records')
    ->select('id','value')
    ->where('lens_tribe_tips_id',$value->id)
    ->where('status','Active')
    ->where('value_type','persuade')
    ->get();

    //for values 
    $idArray  = json_decode($value->value);
    $keyArray = array();
    foreach($idArray as $vid)
    {
      $key = $this->getFunNameById($vid);
      array_push($keyArray, $key);
    }
    
    //for opposite
    $idOppArray = json_decode($value->opposite);
    $oppKeyArray = array();
    foreach($idOppArray as $oid)
    {
      $key = $this->getFunNameById($oid);
      array_push($oppKeyArray, $key);
    }

    $value->opposite = implode($oppKeyArray);
    $value->value = implode($keyArray);
    $value->title = implode($keyArray);

    $value->seek_value_list     = $seekArray;
    $value->persuade_value_list = $persuadeArray;

    array_push($tribeTipsList, $value);
  }
/*
  print_r($tribeTipsList);
  die();
*/

//initiale value 
  $initialValueList = array();
  foreach($initialValueListTable as $ivalue) 
  {

    if($ivalue->id > 12)
    {      
      $idArray = json_decode($ivalue->value);
      
      if($idArray)
      {
        $keyArray = array();
        foreach($idArray as $vid)
        {
          $key = $this->getFunNameById($vid);
          array_push($keyArray, $key);
        }

        $ivalue->value = implode($keyArray);
        $ivalue->title = implode($keyArray);

        array_push($initialValueList, $ivalue);
      }
    }
    else
    {
      array_push($initialValueList, $ivalue);
    }   

  }


  $insightForStrengthValueList = array();
  foreach($insightForStrengthValueListTable as $isightValue)
  {
   $table = DB::table('cot_functional_lens_initial_value_records')
   ->select('title')->where('id',$isightValue->section)->first(); 

   if($table)
   {
    $isightValue->title_key = $table->title;
  } 

  $key = $this->getFunNameById($isightValue->section);

  $isightValue->section = $key;
  array_push($insightForStrengthValueList, $isightValue);
}


 /* print_r($insightForStrengthValueList);
 die();*/

 return view('admin/COT/manualFunctionalLensDetail',compact('initialValueList','insightForStrengthValueList','tribeTipsList'));
}

public function getFunNameById($id)
{

  $valuesKey = '';
  $table = DB::table('cot_functional_lens_initial_value_records')->select('value')->where('id',$id)->first();  
  if(!empty($table))
  {
    $valuesKey = $table->value;
  }

  return $valuesKey;
}

//update cot functional initiative values
public function updateCOTinitiatialValue(Request $request)
{

  $id = Input::get('initialValueId');
  $initialValueId = base64_decode($id);

  if(trim(Input::get('description')))
  {       
    $updateArray['description'] = Input::get('description');
  }

  if(Input::get('iValue')) 
  {
   $updateArray['value'] = Input::get('iValue');
 }

 if(Input::get('iTitle'))
 {
  $updateArray['title'] = Input::get('iTitle');
}

$updateArray['updated_at']  = date('Y-m-d H:i:s');

DB::table('cot_functional_lens_initial_value_records')->where('id',$initialValueId)->update($updateArray);

return redirect()->back();
}
//update cot functional lens insight value 
public function updateCOTfunctionalLensInsightForStrenghtValue(Request $request)
{

  $id = Input::get('insightForStrengthValueId');
  $initialValueId = base64_decode($id);

  $updateArray['positives']            = Input::get('positives'); 
  $updateArray['allowable_weaknesses'] = Input::get('allowableWeaknesses');
  $updateArray['start']                = Input::get('start');
  $updateArray['end']                  = Input::get('end'); 
  $updateArray['updated_at']           = date('Y-m-d H:i:s');

  DB::table('cot_functional_lens_insight_for_strength_value_records')
  ->where('id',$initialValueId)
  ->update($updateArray);

  return redirect()->back();

}

//edit tribe tips values
public function editCOTfunTribeTipsSeekPersuadeValues(Request $request)
{

  $tribeTipsValueId = base64_decode($request->id);
  $valueType = base64_decode($request->type);

  $seekPersuadeList = DB::table('cot_functional_lens_tribe_tips_seek_persuade_value_records')
  ->select('id','value','lens_tribe_tips_id')
  ->where('lens_tribe_tips_id',$tribeTipsValueId)
  ->where('status','Active')
  ->where('value_type',$valueType)
  ->get();
  
  return view('admin/COT/editCOTfunLensTribeTipsValue',compact('seekPersuadeList','tribeTipsValueId','valueType'));

}
//update cot functional lens seek-persuade values or add new
public function updateCOTtribeSeekPersuadevalues(Request $request)
{

  $newValueArray = Input::get('newValue');

  if(!empty($newValueArray))
  {
    foreach ($newValueArray as $value) {

      if(!empty($value)){

        $updateArray['value']              = $value;
        $updateArray['lens_tribe_tips_id'] = Input::get('tribeTipsValueId');
        $updateArray['value_type']         = Input::get('valueType');
        $updateArray['created_at']         = date('Y-m-d H:i:s');

        DB::table('cot_functional_lens_tribe_tips_seek_persuade_value_records')->insertGetId($updateArray);

      }else{

        echo "empty";
      }
    }
  }

  $id = Input::get('seekPersuadeValueId');
  $seekPersuadeValueId = base64_decode($id);

  if(trim(Input::get('seekPersuadeValue')) != '')
  {   
    $updateArray['value']     = Input::get('seekPersuadeValue'); 
  }
  
  $updateArray['updated_at']  = date('Y-m-d H:i:s');

  DB::table('cot_functional_lens_tribe_tips_seek_persuade_value_records')
  ->where('id',$seekPersuadeValueId)
  ->update($updateArray);

  return redirect()->back();
}

//delete cot functional lense tribe tips value 
public function deleteCOTfunLensTribeTipsvalue(Request $request)
{

  $id = $request->id;
  $seekPersuadeValueId = base64_decode($id);
  
  $updateArray['status']      = ('Inactive'); 
  $updateArray['updated_at']  = date('Y-m-d H:i:s');

  DB::table('cot_functional_lens_tribe_tips_seek_persuade_value_records')
  ->where('id',$seekPersuadeValueId)
  ->update($updateArray);

  return redirect()->back();

}

//update cot questions starting date
public function updateCOTfuncLensStartingDate(Request $request)
{

  $orgId = $request->orgId;
  $cotDate = date('Y-m-d', strtotime($request->cotDate));

  DB::table('organisations')->where('id',$orgId)->update(['COT_fun_lens_start_date' => $cotDate]);
  
}
/*update cot tribe tips summary*/
public function updateTribeTipsSummary(Request $request)
{

 $tribeTipsId = base64_decode($request->tribeTipsId);

 if(trim(Input::get('summary')) != '')
 {  
   $updateArray['summary']     = Input::get('summary'); 
 }

 $updateArray['updated_at']  = date('Y-m-d H:i:s');

 DB::table('cot_functional_lens_tribe_tips_records')->where('id',$tribeTipsId)->update($updateArray);

 return redirect()->back();

}

/*get cot team role map description list*/
public function getTeamRoleMapDescriptionList()
{

  $teamRoleMap = DB::table('cot_role_map_options')->where('status','Active')->get();

  return view('admin/COT/listTeamRoleMapDescription',compact('teamRoleMap'));

}
/*update cot team role map description values*/
public function updateCOTteamroleMapDescription()
{

  $maperId     = base64_decode(Input::get('maperId'));
  $description = Input::get('longDescription');

  if(trim($description))
  {
    $updateArray['long_description']  = $description;
    $updateArray['maper']             = Input::get('maperName');
    $updateArray['short_description'] = Input::get('sortDescription');
  }

  $updateArray['updated_at'] = date(('Y-m-d H:i:s'));

  DB::table('cot_role_map_options')->where('id',$maperId)->update($updateArray);

  return redirect()->back();
}


}
