<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Validator;
use Hash;
use Illuminate\Support\Facades\Input;
use Config;
use Mail;

class CommonController extends Controller
{

  public function addNewDepartment()
  {
    $postData = Input::get();

    $department_name = Input::get('department');
    $orgId = Input::get('orgId') ? Input::get('orgId'):0;

    $department = DB::table('departments')
    ->where('department',$department_name)
    ->get();


    if (count($department)) {

      $data = array('response'=>'error','message'=>'Department already exists.');
      return $data;

    } else {

      $insertArray = array(
        'department'=> $department_name,              
        'orgId'=> $orgId,    
        'status'=>'Active', 
        'created_at'=>date('Y-m-d H:i:s')
      );

      $status = DB::table('all_department')->insertGetId($insertArray);

      $data['response'] = 'success';
      $data['message']  = 'Department added successfully';


      $controller = new CommonController();

      $department_array = $controller->callAction('getAllDepartments',array());

      $data['department_array'] = $department_array;

      return $data;
    }


  }


    //get all departments
  public function getAllDepartments() {

    $departments = DB::table('all_department')
    ->select('id','department')
    ->where('status','Active')
    ->orderBy('department','ASC')
    ->get();

    $departmentArray = array();

    foreach ($departments as $value) {

      $dept_opt = '<option value="'.$value->id.'">'.$value->department.'</option>';

      array_push($departmentArray, $dept_opt);
    }

    return $departmentArray;
  }
    //get custom values list
  // public function getCustomDotValues() {

  //   $dotValues = DB::table('dot_value_list')
  //   ->select('id','name')
  //   ->where('status','Active')
  //   ->orderBy('id','asc')
  //   ->get();

  //   $dotValuesArray = array();

  //   foreach ($dotValues as $key => $value) {

  //     $dotValueOpt = '<option value="'.$value->id.'">'.$value->name.'</option>';

  //     array_push($dotValuesArray, $dotValueOpt);
  //   }

  //   return $dotValuesArray;
  // }

  public function getCustomDotValues(Request $request) {

    $getData = Input::all();
    if(!empty($getData['variable']) && !empty($getData))
    {

      $var = $getData['variable'];
    }
    else
    {
      $var = 0;
    }
    $dotValues = DB::table('dot_value_list')
    ->select('id','name')
    ->where('status','Active')
    ->orderBy('id','asc')
    ->get();

    $dotValuesArray = array();

    foreach ($dotValues as $key => $value) {

      $dotValueOpt = '<li><input type="checkbox" name="value_name'.$var.'[]" value="'.$value->id.'"><label>'.$value->name.'</label></li>';

      array_push($dotValuesArray, $dotValueOpt);
    }

    return $dotValuesArray;
  }
  /*get office count*/

  public function getOfficeCount($orgId){

   return $office = DB::table('offices')
   ->where('status','Active')
   ->where('orgId',$orgId)
   ->get()
   ->count();
 }

 public function getDepartmentCount($orgId){

  // return $departments = DB::table('departments')
  // ->where('status','Active')
  // ->where('orgId',$orgId)
  // ->get()
  // ->count();

  $departments = DB::table('departments')
  ->select('departmentId')
  ->where('status','Active')
  ->where('orgId',$orgId)
  ->groupBy('departmentId')
  ->get();
  return count($departments);
  

}

/*check email*/
public function getEmail()
{

  $email = Input::get('email');
  
  $user = DB::table('users')->where('email',$email)->where('status','Active')->first();

  if($user)
  {
    echo true;
  }
  else
  {
    echo false;
  }
}


/*check email*/
public function getOrgName()
{

  $orgName = Input::get('org_name');
  
  $org = DB::table('organisations')
  ->where('organisation',$orgName)
  ->first();

  if($org)
  {
    echo true;
  }
  else
  {
    echo false;
  }
}

/*get offices*/
public function getOfficeList()
{

  $orgId = base64_decode(Input::get('orgId'));

  $offices = DB::table('offices')->where('orgId',$orgId)->get();

  $select = '<select id="office" name="officeId" type="" class="form-control required">';
  $select .='<option value="" disabled="" selected="">Office</option>';
  foreach ($offices as $value) {
    $select .='<option value="'.$value->id.'">'.ucfirst($value->office).'</option>';
  }

  $select .='</select>';

  return $select;

}
/*get all departments*/
public function getDepartmentList()
{

  $orgId = base64_decode(Input::get('orgId'));
  
  $departments = DB::table('departments')
  ->select('departments.id','all_department.department')
  ->leftJoin('all_department','all_department.id', '=', 'departments.departmentId')
  ->where('departments.orgId',$orgId)  
  ->get();

  $select  = '<select id="department" name="departmentId" type="" class="form-control required">';
  $select .='<option value="" disabled="" selected="">Department</option>';

  foreach ($departments as $value) {
    $select .='<option value="'.$value->id.'">'.ucfirst($value->department).'</option>';
  }

  $select .='</select>';

  return $select;

}
/*get user */
public function getUsers()
{

  $orgId = base64_decode(Input::get('orgId'));
  
  $users = DB::table('users')
  ->where('orgId',$orgId)  
  ->where('status','Active')
  ->where('roleId',3)
  ->get();

  $select = '<select id="office" name="userId" type="" class="form-control required">';
  $select .='<option value="" disabled="" selected="">Individual</option>';

  foreach ($users as $value) {
    $select .='<option value="'.$value->id.'">'.ucfirst($value->name).'</option>';
  }

  $select .='</select>';

  return $select;

}

public function inArray($needle, $haystack, $strict = false) {
  foreach ($haystack as $item) {
    if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
      return true;
    }
  }

  return false;
}

/*get search data of the dot values*/
public function getDotValueSearchData(){

  $searchKey = Input::get('search_key');
  $beliefId  = Input::get('beliefId');
  $dotId     = Input::get('dotId');
  // $wrClouse array('dot_value_list.name''like', '%' . $searchKey . '%');

  $dotValuesList = DB::table('dots_values')
  ->select('dot_value_list.name','dots_values.id')
  ->leftJoin('dot_value_list', 'dots_values.name', '=', 'dot_value_list.id')
  ->where('dots_values.status','Active')
  ->where('dots_values.beliefId',$beliefId)
  ->where('dot_value_list.name', 'like', '%' . $searchKey . '%')
  ->orderBy('dot_value_list.name','ASC')
  ->get();
  
  $html ='';
  foreach ($dotValuesList as $key => $value){

    $url = url("admin/get-evidence-list\/").base64_encode($dotId).'/'.base64_encode("value")."/".base64_encode($value->id);
    $name = $value->name;

    $html ='<li><a href="'.$url.'"><label>'.$name.'</label></a></li>';
    
  }
  return $html;
}

/*send notification mail */
public function sendNotificationMail($data)
{
  $email ='team@tribe365.co';
  // $email ='yatendra@chetaru.com';

  Mail::send('layouts/notificationMail', $data, function($message) use($email) 
  {
    $message->from('notification@tribe365.co', 'Tribe365');
    $message->to($email);
    $message->subject('Notification Tribe365');
  });

}

/*get offices */
public function getOfficesByOrgId()
{

  $orgId = Input::get('orgId');

  $offices = DB::table('offices')
  ->where('status','Active')
  ->orderBy('office','ASC')
  ->where('orgId',$orgId)
  ->get();

  $selectArr = array();
  $select ='<option value="" disabled="" selected="">Select Office</option>';
  array_push($selectArr, $select);
  
  foreach ($offices as $value) {

    $select = '<option value="'.$value->id.'">'.ucfirst($value->office).'</option>';

    array_push($selectArr, $select);
  }

  return $selectArr;

}

/*get all departments by office id*/
public function getDepartmentByOfficecId()
{

 $officeId = Input::get('officeId');

 $departments = DB::table('departments')
 ->select('departments.id','all_department.department')
 ->leftJoin('all_department','all_department.id','departments.departmentId')
 ->where('departments.officeId',$officeId) 
 ->where('departments.status','Active') 
 ->orderBy('all_department.department','ASC')->get();

 $select='';
 foreach ($departments as $value)
 {
  $select .='<option value="'.$value->id.'" data-weapons="laserI laserII">'.ucfirst($value->department).'</option>';
}

return $select;
}

/*search organisations */
public function searchOrganisations()
{
  $orgName = Input::get('orgName');

  $controller = new CommonController();

  $organisations = DB::table('organisations')->where('organisation', 'like', '%' . $orgName . '%')->where('status','Active')->get();

  $htmlArray = array();
  foreach ($organisations as $value) {

    $orgLink   = url('admin/organisation-detail/'.base64_encode($value->id));
    $imgUrl    = asset('public/uploads/org_images').'/'.$value->ImageURL;
    $orgName   = ucfirst($value->organisation);
    $indType   = ucfirst($value->industry);
    $numOffice = $controller->callAction('getOfficeCount',array($value->id));
    $numDept   = $controller->callAction('getDepartmentCount',array($value->id));

    $html = '<div class="col-md-6"><div class="company-section"><div class="company-logo"><a href="'.$orgLink.'"><img src="'.$imgUrl.'"></a></div><div class="company-detale"><a href="'.$orgLink.'"><h2>'.$orgName.'</h2></a><div class="company-category"><ul><li><span class="category-tital">IndustryType</span><span class="category-number">'.$indType.'</span></li><li><span class="category-tital">Offices</span><span class="category-number">'.$numOffice.'</span></li><li><span class="category-tital">Departments</span><span class="category-number">'.$numDept.'</span></li></ul></div></div></div></div>';

    array_push($htmlArray, $html);;
  }

  return $htmlArray;

}

/*get seach offces results*/
public function searchOffice()
{

  $officeName = Input::get('office');
  $orgId      = Input::get('orgId');

  $controller = new CommonController();

  //$offices = DB::table('offices')->where('office', 'like', '%' . $officeName . '%')->where('status','Active')->where('orgId',$orgId)->get();
  $offices = DB::table('offices')
     ->select('offices.office','offices.address','offices.city','offices.country','offices.phone','offices.numberOfEmployees','country.id','country.nicename')
     ->leftJoin('country','country.id','offices.country')
     ->where('offices.status','Active')
     ->where('office', 'like', '%' . $officeName . '%')
     ->where('orgId',$orgId)
     ->get();

  $htmlArray = array();
  $html1 = '<tr><th> Name</th><th> Address</th><th> Phone</th><th> No. of Employee</th></tr>';
  array_push($htmlArray, $html1);
  foreach ($offices as $value)
  {

    $html = '<tr><td>'.ucfirst($value->office).'</td><td>'.$value->address .' '. $value->city .' '.$value->nicename.'</td><td>'.$value->phone.'</td><td>'.$value->numberOfEmployees.'</td></tr>';

    array_push($htmlArray, $html);;
  }

  return $htmlArray;
}

/*get list of staff*/
public function searchStaff()
{

  $staffName = Input::get('staff');
  $orgId     = Input::get('orgId');

  $controller = new CommonController();
  
  $users = DB::table('users')    
  ->select('users.id','users.name','users.email','users.orgId','users.officeId','users.departmentId','organisations.organisation','offices.office','all_department.department') 
  ->leftJoin('offices', 'users.officeId', '=', 'offices.id')           
  ->leftJoin('departments', 'users.departmentId', '=', 'departments.id')
  ->leftJoin('all_department','departments.departmentId', '=', 'all_department.id')
  ->leftJoin('organisations','users.orgId', '=', 'organisations.id')  
  ->where('users.roleId',3)
  ->where('users.status','Active')
  ->where('organisations.status','Active')
  ->where('offices.status','Active')
  ->where('departments.status','Active')
  ->where('all_department.status','Active')
  ->where(function($query) use ($staffName){
        $query->where('users.name', 'like', '%' . $staffName . '%');
        $query->orWhere('all_department.department', 'like', '%' . $staffName . '%');
        $query->orWhere('offices.office', 'like', '%' . $staffName . '%');
    })
  ->where('users.orgId',$orgId)        
  ->get();

  $htmlArray = array();
  $html1 = '<tr><th> Name </th><th> Email </th><th> Office </th><th> Department </th><th> Action </th></tr>';
  array_push($htmlArray, $html1);
  foreach ($users as $value)
  {

   $html = '<tr><td>'.ucfirst($value->name).'</td><td>'.$value->email.'</td><td>'. ucfirst($value->office).'</td><td>'.ucfirst($value->department).'</td><td><div class="editable"><a href="'.route("admin-user.edit",["id"=>base64_encode($value->id)]).'"><button type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button></a><form method="POST" action="'.route("admin-user.destroy",["id"=>base64_encode($value->id)]).'" accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="'. csrf_token().'"><button type="submit" onclick="return confirm("Are you sure you want to delete ?")"></button></form></div></td></tr>';

   array_push($htmlArray, $html);
 }

 return $htmlArray;
}

/*get departments search list*/
public function searchDepartment()
{

  $department = Input::get('department');

  $departments = DB::table('all_department')->where('department', 'like', '%' . $department . '%')->where('status','Active')->orderBy('department','ASC')->get();

  $htmlArray = array();
  $html1 = '<tr><th> Name </th></tr>';
  array_push($htmlArray, $html1);  

  foreach ($departments as $value)
  {

    $html = '<tr><td>'.ucfirst($value->department).'<div class="editable"><a href="'.route("departments.edit",["id"=>base64_encode($value->id)]).'"><button type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button></a><form method="POST" action="'.route("departments.destroy",["id"=>base64_encode($value->id)]).'" accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="'. csrf_token().'"><button type="submit" onclick="return confirm("Are you sure you want to delete ?")"></button></form></div></td></tr>';

    array_push($htmlArray, $html);
  }

  return $htmlArray;
}


/*search organisations */
public function searchReportOrganisations()
{
  $orgName = Input::get('orgName');

  $organisations = DB::table('organisations')->where('organisation', 'like', '%' . $orgName . '%')->where('status','Active')->get();

  $htmlArray = array();
  foreach ($organisations as $value)
  {
    $orgLink   = route('reports.show',base64_encode($value->id));
    $imgUrl    = asset('public/uploads/org_images').'/'.$value->ImageURL;
    $orgName   = ucfirst($value->organisation);
    
    $html = '<div class="col-md-3"> <div class="company-section"> <div class="company-detale"> <a href="'.$orgLink.'"> <img src="'.$imgUrl.'"> </a> <h2>'.$orgName.'</h2> </div><div class="company-ovel-text"> <span>generate report </span> </div></div></div>';

    array_push($htmlArray, $html);;
  }

  return $htmlArray;
}

/*get all departments by office id*/
public function getDepartmentByOfficecIdforGraph()
{

 $officeId = Input::get('officeId');

 if (!empty($officeId))
 {
  $departments = DB::table('departments')
  ->select('departments.id','all_department.department')
  ->leftJoin('all_department','all_department.id','departments.departmentId')
  ->where('departments.officeId',$officeId) 
  ->where('departments.status','Active') 
  ->orderBy('all_department.department','ASC')->get();
}
else
{
  $departments = DB::table('all_department')->where('status','Active')->orderBy('department','ASC')->get();
}

$html = '';
foreach ($departments as $value)
{
  $html .='<option value="'.$value->id.'" data-weapons="laserI laserII">'.ucfirst($value->department).'</option>';
}

return $html;
}


/*get cot functional lens initial values*/
public function getCotFunLensInitialValue()
{

  $cotFunArray = DB::table('cot_functional_lens_initial_value_records')->select('id','title')->take(8)->get();

  $newArray = array();
  foreach ($cotFunArray as $value)
  {
   array_push($newArray,(string)$value->id);
 }
//split array in small arrays
 return array_chunk($newArray, 2);

}

/*send notification*/
function sendFcmNotify($notiArr)
{

  $fcmToken = $notiArr['fcmToken'];
  $title    = $notiArr['title'];
  $message  = $notiArr['message'];
  $totbadge = $notiArr['totbadge'];
  $feedbackId = $notiArr['feedbackId'];
  
  $fields = array(
    'to' => $fcmToken,
    'priority' => "high",
    'notification' => array("title" => $title, "body" => $message, "badge" => $totbadge, "sound" => "default","feedbackId"=>$feedbackId),
    'data'=> array("title" => $title, "body" => $message, "badge" => $totbadge, "sound" => "default","feedbackId"=>$feedbackId) 
  );

  $headers = array(
    'https://fcm.googleapis.com/fcm/send',
    'Content-Type: application/json',
    'Authorization: key=AAAASSh9qJM:APA91bGFC09RB3dFkD9u7L-R-hNMA7l4MWinuhfI0HspKKrNcXMdpVyQ-UKEqk71_Je91dEOb4y80Y96siJ6Z5v5sTR0Iq-yUh779ZEOqGDar71tbts9oCw4xggscbBoR0D_jOSPF_xQ'
  );

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

  $result = curl_exec($ch);
  if($result === FALSE)
  {
    die('Problem occurred: ' . curl_error($ch));
  }

  curl_close($ch);
  $msg = json_decode($result);    
  return $msg;
}

}
