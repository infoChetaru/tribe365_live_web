<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;

class AdminDiagnosticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {     

        $categoryId = $request->categoryId;

        $query = DB::table('diagnostic_questions')
        ->select('diagnostic_questions.id','diagnostic_questions.question','dqc.title','dqc.id AS category_id','diagnostic_questions.measure')
        ->leftjoin('diagnostic_questions_category AS dqc','dqc.id','diagnostic_questions.category_id')
        ->where('status','Active')
        ->orderBy('dqc.id','ASC');

        if($categoryId)
        {
            $query->where('diagnostic_questions.category_id',$categoryId);
        }

        $questionTbl = $query->paginate(10);

        $diaQueCategoryList = DB::table('diagnostic_questions_category')->get();

        return view('admin/diagnostic/listDiagnosticQuestions',compact('questionTbl','diaQueCategoryList','categoryId'));
    }

    public function update(Request $request, $id)
    {

        $queId    = base64_decode($id);        
        $question = Input::get('question');
        $diaCategoryId = Input::get('diaCategoryId');
        $pageNo = Input::get('page_no');
        $measure = Input::get('measure');

        if(trim($question))
        {
            $updateArray['question']    = $question; 
            $updateArray['category_id'] = $diaCategoryId;
            $updateArray['measure']     = strtolower($measure);
        }

        $updateArray['updated_at'] = date('Y-m-d H:i:s');

        DB::table('diagnostic_questions')->where('id',$queId)->update($updateArray);

        return redirect('admin/diagnostic?categoryId='.$diaCategoryId);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    public function destroy($id)
    {
        //
    }

    /*diagnostic option list*/
    public function getDiagnosticOptionList()
    {
        $diaOptTbl = DB::table('diagnostic_question_options')->where('status','Active')->get();

        return view('admin/diagnostic/listDiagnosticQuestionOption',compact('diaOptTbl'));
    }
    /*update option value*/
    public function upadateDiagnosticOption(Request $request)
    {
        $optionId = base64_decode($request->optionId);        
        $option   = Input::get('option');

        if(trim($option))
        {
            $updateArray['option_name'] = $option;
        }

        $updateArray['updated_at'] = date('Y-m-d H:i:s');

        DB::table('diagnostic_question_options')->where('id',$optionId)->update($updateArray);

        return redirect()->back(); 
    }

    /*get diagnostic categories values list*/
    public function getDiagnosticValuesList()
    {
        $categoryTbl = DB::table('diagnostic_questions_category')->get();

        return view('admin/diagnostic/listDiagnosticCategoryValuesList',compact('categoryTbl'));
    }
    /*update diagnosctic category values*/
    public function updateDiagnosticCategoryValues()
    {
        $titleId = base64_decode(Input::get('titleId'));        

        $updateArray = array();
        if(trim(Input::get('title')))
        {
            $updateArray['title'] = Input::get('title');
        }

        DB::table('diagnostic_questions_category')->where('id',$titleId)->update($updateArray);

        return redirect()->back(); 
    }
}
