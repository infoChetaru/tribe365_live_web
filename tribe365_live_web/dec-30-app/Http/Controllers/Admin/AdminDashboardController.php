<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;

class AdminDashboardController extends Controller
{
  public function index()
  {
    $orgId             = Auth::guard('web')->User()->orgId;

    $organisation      = DB::table('organisations')->where('status','Active')->count();

    $user_count        = DB::table('users')->where('status','Active')->where('roleId',3)->count();

    $dot_count         = DB::table('dots')->count();

    $departmentCount   = DB::table('all_department')->where('status','Active')->count();

    // $iotFeedbackCount  = DB::table('iot_feedbacks')->where('status','Active')->count();
    
    $iotNewFeedbackQuery = DB::table('iot_feedbacks')
    ->leftJoin('iot_messages','iot_messages.feedbackId','iot_feedbacks.id')
    ->leftJoin('users','users.id','iot_feedbacks.userId')
    ->where('iot_feedbacks.status','!=','Completed')
    ->whereNull('iot_messages.feedbackId');

    $iotFeedbackCount = $iotNewFeedbackQuery->count();

    $data = array(
      'organisation'=> $organisation,
      'user_count'=> $user_count,
      'dot_count'=> $dot_count,
      'departmentCount'=> $departmentCount,
      'iotFeedbackCount'=> $iotFeedbackCount,
    );
    return view('admin/adminDashboard',compact('data'));
  }
}
