<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;
use Config;
use View;


class AdminSOTController extends Controller
{

  public function __construct(Request $request)
  {

    $orgid = base64_decode($request->segment(3));

    if(!empty($orgid))
    {

      $organisations = DB::table('organisations')
      ->select('ImageURL')
      ->where('id',$orgid)
      ->first();

      if(!empty($organisations->ImageURL))
      {
        $org_detail = base64_encode($orgid)."-".$organisations->ImageURL;

        View::share('org_detail', $org_detail);
      }
      else
      {
        $org_detail = "";

        View::share('org_detail', $org_detail);

      }
    }

  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSOTquestionnaireList(Request $request)
    {        

      $sotQuesList = DB::table('sot_questionnaire_records')->where('status','Active')->get();

      $sotCultureTbl = DB::table('sot_culture_structure_records')->where('status','Active')->orderBy('type','ASC')->get();

      $sotCultureStructureList =array();

      foreach ($sotCultureTbl as $value){

        $sotCultureSummaryTbl = DB::table('sot_culture_structure_summary_records')->where('status','Active')->where('type',$value->id)->get();

        $value->cultureSummary = $sotCultureSummaryTbl;

        array_push($sotCultureStructureList, $value);

      }

      return view('admin/SOT/listSOTQuestionnaire',compact('sotQuesList','sotCultureStructureList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
      $orgId = base64_decode($id);

      $sotCultureStrTbl = DB::table('sot_culture_structure_records')->select('id','type','title','imgUrl')->where('status','Active')->get();

      $sotCountArray = array();
      $countArr = array();
      foreach ($sotCultureStrTbl as $value) 
      {

        $SOTCount = DB::table('sot_answers')
        ->select(DB::raw('sum(score) AS count'))
        ->leftJoin('sot_questionnaire_records','sot_questionnaire_records.id','=','sot_answers.question_id')
        ->join('users','users.id','=','sot_answers.userId')
        ->where('users.status','Active')
        ->where('sot_questionnaire_records.type',$value->type)    
        ->where('users.orgId',$orgId)        
        ->first();

        $value->SOTCount = $SOTCount->count;

        array_push($countArr, $SOTCount->count);
        array_push($sotCountArray, $value);

      }


        // find maximum value array
      $sotCountArray1 = array();
      foreach ($sotCultureStrTbl as $value) 
      {

        $SOTCount = DB::table('sot_answers')
        ->select(DB::raw('sum(score) AS count'))
        ->leftJoin('sot_questionnaire_records','sot_questionnaire_records.id','=','sot_answers.question_id')
        ->leftJoin('users','users.id','=','sot_answers.userId')
        ->where('users.status','Active')
        ->where('sot_questionnaire_records.type',$value->type)    
        ->where('users.orgId',$orgId)        
        ->first();

        $value->SOTCount = $SOTCount->count;

        if (max($countArr)==$SOTCount->count)
        {
          array_push($sotCountArray1, $value);

        }        

      }


        //detail of culture structure 
      $sotStrDetailArr = array();

      foreach ($sotCountArray1 as $value)
      {
        $sotStrDetail = DB::table('sot_culture_structure_records')
        ->where('id',$value->id)
        ->where('status','Active')
        ->first();

        $summary = DB::table('sot_culture_structure_summary_records')
        ->where('type',$value->id)
        ->where('status','Active')
        ->get();

        $value->summary = $summary;

        array_push($sotStrDetailArr, $value);
      }
      $organisations = DB::table('organisations')->where('id',$orgId)->first();

      return view('admin/SOT/detailSOTcultureStructure',compact('sotCountArray','sotStrDetailArr','orgId','organisations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*sot cultural structure summary edit*/
    public function edit(Request $request,$id)
    {
      $cultureId = base64_decode($id);

      $summaryList = DB::table('sot_culture_structure_summary_records')->where('type',$cultureId)->where('status','Active')->get();

      $cultureStrType = '';
      if($cultureId==1){

        $cultureStrType = 'Person';
      }elseif ($cultureId==2){

       $cultureStrType = 'Power';
     }elseif ($cultureId==3){

       $cultureStrType = 'Role';
     }elseif ($cultureId==4){

      $cultureStrType = 'Tribe';
    }

    return view('admin/SOT/editSOTCultureSummaryValue',compact('summaryList','cultureStrType'));   
  }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function sotHomeDetail(Request $request)
    {

      $orgId = base64_decode($request->id);

      $organisations = DB::table('organisations')->where('id',$orgId)->where('status','Active')->first();

      return view('admin/SOT/detailSOT',compact('organisations'));
    }


    /*update sot culture questions list */
    public function updateCultureInfoQuestion(Request $request)
    {

      $tribeTipsId = base64_decode($request->sotQuestionId);

      if(trim(Input::get('question')) != '')
      { 
        $updateArray['question']   = Input::get('question'); 
      }

      $updateArray['updated_at'] = date('Y-m-d H:i:s');

      DB::table('sot_questionnaire_records')->where('id',$tribeTipsId)->update($updateArray);  

      return redirect()->back(); 
    }
    /*update  culture structure summary value*/
    public function updateCultureSummaryValue(Request $request)
    {

      $summaryId = base64_decode($request->summaryId);

      if(trim(Input::get('summary')) != '')
      { 
        $updateArray['summary']   = Input::get('summary'); 
      }

      $updateArray['updated_at'] = date('Y-m-d H:i:s');

      DB::table('sot_culture_structure_summary_records')->where('id',$summaryId)->update($updateArray);  

      return redirect()->back(); 
    }

    /*update culture structure image*/
    public function updateCultureStructureImag(Request $request)
    {

      $cultureSrtId = base64_decode($request->cultureSrtId);

      if($request->hasfile('file'))
      {

       $file            = $request->file('file');
       $fileName        = 'sot_'.time().'.'.$file->getClientOriginalExtension();
       $destinationPath = public_path('/uploads/sot/');

       $file->move($destinationPath, $fileName);   

       DB::table('sot_culture_structure_records')->where('id',$cultureSrtId)->update(['imgUrl'=>$fileName]);       
     }     

     return redirect()->back(); 
   }


   /*get SOT Motivation structure detail*/
   public function getSOTmotivationUserList(Request $request)
   {

/*    $orgId = base64_decode($request->id);

    $officeId = $request->officeId;
    $departmentId = $request->departmentId;

    $customDept1 = DB::table('departments')
    ->select('departments.id','all_department.department')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('departments.officeId',$officeId)->get();

    $customDept = array();
    foreach ($customDept1 as $value)
    {
      $users = DB::table('users')->where('status','Active')->where('departmentId',$value->id)->first();

      if($users)
      {
        array_push($customDept, $value);
      }
    }

    session()->put('officeId', $officeId);
    session()->put('departmentId', $departmentId);
    session()->put('customDept',$customDept);

    $userWhr = array();
    if($officeId)
    {
      $userWhr['users.officeId'] = $officeId;
    }

    if($departmentId)
    {
      $userWhr['users.departmentId'] = $departmentId;
    }
    $userWhr['users.orgId']  = $orgId;
    $userWhr['users.roleId'] = 3;
    $userWhr['users.status'] = 'Active';

    //if user search result then pagination not shows
    if($officeId)
    {

      $users  = DB::table('users')
      ->select('users.id AS userId', 'users.name', 'offices.office', 'all_department.department', 'sot_motivation_user_records.id', 'sot_motivation_user_records.money_score', 'sot_motivation_user_records.stress_avoid_score', 'sot_motivation_user_records.risk_avoidance_score', 'sot_motivation_user_records.job_structure_score', 'sot_motivation_user_records.avoid_working_alone_score', 'sot_motivation_user_records.identify_with_team_score', 'sot_motivation_user_records.recognition_score', 'sot_motivation_user_records.power_score', 'sot_motivation_user_records.autonomy_variety_score', 'sot_motivation_user_records.personal_growth_score')

      ->leftJoin('offices', 'offices.id', '=', 'users.officeId')   
      ->leftJoin('departments', 'departments.id', '=', 'users.departmentId')
      ->leftJoin('all_department','all_department.id', '=', 'departments.departmentId') 
      ->leftJoin('sot_motivation_user_records','sot_motivation_user_records.userId', '=', 'users.id')
      ->where($userWhr)
      ->orderBy('users.id','DESC')
      ->get();
    }else{

      $users  = DB::table('users')
      ->select('users.id AS userId', 'users.name', 'offices.office', 'all_department.department', 'sot_motivation_user_records.id', 'sot_motivation_user_records.money_score', 'sot_motivation_user_records.stress_avoid_score', 'sot_motivation_user_records.risk_avoidance_score', 'sot_motivation_user_records.job_structure_score', 'sot_motivation_user_records.avoid_working_alone_score', 'sot_motivation_user_records.identify_with_team_score', 'sot_motivation_user_records.recognition_score', 'sot_motivation_user_records.power_score', 'sot_motivation_user_records.autonomy_variety_score', 'sot_motivation_user_records.personal_growth_score')

      ->leftJoin('offices', 'offices.id', '=', 'users.officeId')   
      ->leftJoin('departments', 'departments.id', '=', 'users.departmentId')
      ->leftJoin('all_department','all_department.id', '=', 'departments.departmentId') 
      ->leftJoin('sot_motivation_user_records','sot_motivation_user_records.userId', '=', 'users.id')
      ->where($userWhr)
      ->orderBy('users.id','DESC')
      ->paginate(10);

    }


    $organisations = DB::table('organisations')->where('id',$orgId)->first();

    $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get();

    return view('admin/SOT/listSOTMotivationUsers',compact('users','organisations','offices'));*/

    $resultArray = array();

    $orgId = base64_decode($request->id);

    $officeId = $request->officeId;
    $departmentId = $request->departmentId;

    $customDept1 = DB::table('departments')
    ->select('departments.id','all_department.department')
    ->leftJoin('all_department','all_department.id','departments.departmentId')
    ->where('departments.officeId',$officeId)->get();

    $customDept = array();
    foreach ($customDept1 as $value)
    {
      $users = DB::table('users')
      ->where('status','Active')
      ->where('departmentId',$value->id)
      ->first();

      if($users)
      {
        array_push($customDept, $value);
      }
    }

    session()->put('officeId', $officeId);
    session()->put('departmentId', $departmentId);
    session()->put('customDept',$customDept);


    $userWhr = array();
    if($officeId) 
    { 
      $userWhr['users.officeId'] = $officeId;
    }
    if($departmentId)
    { 
      $userWhr['users.departmentId'] = $departmentId;
    }

    $userWhr['users.orgId']  = $orgId;
    $userWhr['users.roleId'] = 3;
    $userWhr['users.status'] = 'Active';


    $userTbl = DB::table('users')
    ->select('users.id AS userId', 'users.name', 'offices.office', 'all_department.department')
    ->leftJoin('offices', 'offices.id', '=', 'users.officeId')   
    ->leftJoin('departments', 'departments.id', '=', 'users.departmentId')
    ->leftJoin('all_department','all_department.id', '=', 'departments.departmentId') 
    ->where($userWhr)
    ->orderBy('users.id','DESC')
    ->get();

    $userArray = array();
    foreach ($userTbl as $key => $userValue)
    {

      $categoryTbl = DB::table('sot_motivation_value_records')->where('status','Active')->get();

      $catArray = array();
      foreach ($categoryTbl as $value)
      {  

        $ansTbl = DB::table('sot_motivation_answers AS sotans')         

        ->leftJoin('sot_motivation_question_options AS qoption', 'qoption.id', '=', 'sotans.optionId')
        ->where('sotans.userId',$userValue->userId)
        ->where('sotans.orgId',$orgId)
        ->where('sotans.status','Active')
        ->where('qoption.category_id',$value->id)
        ->sum('sotans.answer');

        $result['title'] = $value->title;
        $result['score'] = $ansTbl;

        array_push($catArray, $result);

      }

      $userValue->sotMotivationValues = $catArray;

      array_push($userArray, $userValue);

    }

    $organisations = DB::table('organisations')->where('id',$orgId)->first();

    $offices = DB::table('offices')->where('status','Active')->where('orgId',$orgId)->get();

    return view('admin/SOT/listSOTMotivationUsers',compact('userArray','organisations','offices'));
    

  }

  /*update SOT motivation detail*/
  public function updateSOTmotivationDetail()
  {

    $sotMotId = Input::get('sotMotId');
    $userId   = Input::get('userId');

    $updateArray['money_score']               = Input::get('money_score');
    $updateArray['stress_avoid_score']        = Input::get('stress_avoid_score');
    $updateArray['risk_avoidance_score']      = Input::get('risk_avoidance_score');
    $updateArray['job_structure_score']       = Input::get('job_structure_score');
    $updateArray['avoid_working_alone_score'] = Input::get('avoid_working_alone_score');
    $updateArray['identify_with_team_score']  = Input::get('identify_with_team_score');
    $updateArray['recognition_score']         = Input::get('recognition_score');
    $updateArray['power_score']               = Input::get('power_score');
    $updateArray['autonomy_variety_score']    = Input::get('autonomy_variety_score');
    $updateArray['personal_growth_score']     = Input::get('personal_growth_score');

    $status = DB::table('sot_motivation_user_records')->where('id',$sotMotId)->first();

    if($status)
    {

     $updateArray['updated_at'] = date('Y-m-d H:i:s');

     DB::table('sot_motivation_user_records')->where('id',$sotMotId)->update($updateArray);

   }else{

     $updateArray['status']     = 'Active';
     $updateArray['userId']     = $userId;
     $updateArray['created_at'] = date('Y-m-d H:i:s');

     DB::table('sot_motivation_user_records')->insertGetId($updateArray);
   }

   return redirect()->back();
 }
 /*update sot starting time*/
 public function updateSOTmotivationStartingDate(Request $request)
 {
   $orgId = $request->orgId;
   $sotDate = date('Y-m-d', strtotime($request->sotDate));

   DB::table('organisations')->where('id',$orgId)->update(['SOT_motivation_start_date' => $sotDate]);
 }

 /*get sot defination list*/
 public function getSOTmotivationValuesList(Request $request)
 {

  $motivationValues = DB::table('sot_motivation_value_records')->where('status','Active')->get();

  return view('admin/SOT/listSOTmotivationValues',compact('motivationValues'));
}
/**/
public function updateSOTmotivationValue()
{

  $motValueId  = base64_decode(Input::get('motValueId'));

  if(trim(Input::get('description')) != '' && trim(Input::get('motValTitle')) != '')
  {   
    $updateArray['description'] = Input::get('description'); 
    $updateArray['title']       = Input::get('motValTitle');
  }

  $updateArray['updated_at'] = date('Y-m-d H:i:s');

  DB::table('sot_motivation_value_records')->where('id',$motValueId)->update($updateArray);

  return redirect()->back();
}


/*get sot motivation questions list*/
public function getSOTmotivationQuestionnaireList()
{

  $resultArray = array();

  $sotQuestions = DB::table('sot_motivation_questions')->where('status','Active')->paginate(10);

  return view('admin/SOT/listSOTmotivationQuestions',compact('sotQuestions'));
}

/*sot motivation questions list*/
public function getSOTmotivationQuesOptionList(Request $request)
{
  $questionsId  = base64_decode($request->id);

  $sotMotQueOptList = DB::table('sot_motivation_questions AS q')
  ->select('o.id','o.question_id','q.question','o.option_name','cat.title')
  ->leftJoin('sot_motivation_question_options AS o', 'q.id', '=', 'o.question_id')
  ->leftJoin('sot_motivation_value_records AS cat', 'cat.id', '=', 'o.category_id')
  ->where('q.id',$questionsId)
  ->where('o.status','Active')
  ->get();

  return view('admin/SOT/listSOTmotivationQuestionOptions',compact('sotMotQueOptList'));
}

/*update sot motivation question and option*/
public function updateSOTmotivationQueOpt()
{      

  $questionsId= Input::get('questionId');
  $question   = Input::get('question');
  $option     = Input::get('optionName');
  $optionId   = Input::get('optionId');
  
  $updateArray = array('question'=>$question,'updated_at'=>date('Y-m-d H:i:s'));

  DB::table('sot_motivation_questions')->where('id',$questionsId)->update($updateArray);

  for ($i=0; $i <count($optionId); $i++) 
  { 
    $optUpdateArray = array('option_name'=>$option[$i],'updated_at'=>date('Y-m-d H:i:s'));

    DB::table('sot_motivation_question_options')->where('id',$optionId[$i])->update($optUpdateArray);
  }

  return redirect()->back()->with('message','Record Updated Successfully.');
}

/*update sot culture structure title (person...)*/
public function updateCultureStructureTitle(Request $request)
{

  $sotCulStrId    = base64_decode(Input::get('sotCulStrId'));
  
  if(trim(Input::get('sotCulStrTitle')) != '')
  {   
    $updateArray['title'] = Input::get('sotCulStrTitle'); 
    
  }

  $updateArray['updated_at'] = date('Y-m-d H:i:s');

  DB::table('sot_culture_structure_records')->where('id',$sotCulStrId)->update($updateArray);

  return redirect()->back();
}

}
