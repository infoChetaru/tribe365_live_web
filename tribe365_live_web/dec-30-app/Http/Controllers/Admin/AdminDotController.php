<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Validator;
use Hash;
use Illuminate\Support\Facades\Input;
use Config;
use View;

/*DOT: directing of tribes*/
class AdminDotController extends Controller
{

  public function __construct(Request $request)
  {

    if($request->segment(2)=="dot-create")
    {
      $orgid = base64_decode($request->segment(3));

    }
    else
    {
      $dotid = base64_decode($request->segment(3));


      $dot = DB::table('dots')
      ->select('orgId')
      ->where('id',$dotid)
      ->first();
      if(!empty($dot))
      {
       $orgid = $dot->orgId;
     }
   }

   if(!empty($orgid))
   {


    $organisations = DB::table('organisations')
    ->select('ImageURL')
    ->where('id',$orgid)
    ->first();

    if(!empty($organisations->ImageURL))
    {
      $org_detail = base64_encode($orgid)."-".$organisations->ImageURL;

      View::share('org_detail', $org_detail);
    }
    else
    {
      $org_detail = "";

      View::share('org_detail', $org_detail);

    }
  }

}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $dot  = DB::table('dots')->get();

      return view('admin/dot/dot',compact('dot'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {       

    }

    public function addDot($id){

     $orgId =base64_decode($id);

     $data['orgId']  = $orgId;

     $dotValuesList = DB::table('dot_value_list')
     ->where('status','Active')
     ->orderBy('name','ASC')
     ->get();

      // print_r($offices);
     return view('admin/dot/addDot',$data,compact('dotValuesList'));

   }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $postData = Input::get();


      $orgId       = Input::get('orgId');
      $date_of_dot = Input::get('dot_date');
      $vision      = Input::get('vision');
      $mission     = Input::get('mission');
      $focus       = Input::get('focus');
      $officeId    = Input::get('office');
      $introductory_information = Input::get('introductory_information');

        // echo "<pre>";
        // print_r($postData);

        // insert data in dot table
      $insertArray = array(
        'vision'     => $vision,
        'mission'    => $mission,
        'focus'      => $focus,
        'officeId'   =>$officeId,
        'orgId'      => $orgId,
        'introductory_information'=> $introductory_information,
        'dot_date'   => date('Y-m-d', strtotime($date_of_dot)),
        'created_at' => date('Y-m-d H:i:s')
      );

      $get_dot_id = DB::table('dots')->insertGetId($insertArray);

        //check if dot not inserted
      if(!empty($get_dot_id)){

          //get counts of the beliefsf
        $belief_count = Input::get('belief_count');

        for($i=0; $i < $belief_count; $i++) {

           //get values variable
          $value_var = 'value_name'.$i;

          $belief_var = Input::get('belief_name'.$i);

            //insert data in belief table
          $beliefInsertArray = array(
            'name'=> $belief_var,
            'dotId'=> $get_dot_id,                
            'created_at'=> date('Y-m-d H:i:s')
          );


          $dot_belief_id = DB::table('dots_beliefs')->insertGetId($beliefInsertArray);

            //check if belief is not inserted 
          if(!empty($dot_belief_id)){

            if(!empty($postData[$value_var] )) {



              foreach ($postData[$value_var] as $value) {


                if(!empty($value)) {

                            //insert data in vaue table
                  $valuesInsertArray = array(
                    'name'=> $value,
                    'beliefId'=> $dot_belief_id,                
                    'created_at'=> date('Y-m-d H:i:s')
                  );

                  $dot_value_id = DB::table('dots_values')->insertGetId($valuesInsertArray);
                }
              } 
            }
            }//check dot belief inserted
          }
    }//check dot not inserted
    
    return redirect('admin/dot-edit/'. base64_encode($get_dot_id).'/edit')->with('message','DOT Added Successfully');

  }

  /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function show($id)
  {
        //
  }

  /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function edit($id)
  {

   $id = base64_decode($id);

   $dots = DB::table('dots')
   ->where('id',$id)
   ->first();

   $belief_qr = DB::table('dots_beliefs')
   ->where('dotId',$id)
   ->get();

   $beliefs = array();

   foreach ($belief_qr as $value) {

     $belief['id'] = $value->id;
     $belief['name'] = $value->name;
     $belief['dotId'] = $value->dotId;

     $dots_value = DB::table('dots_values')
     ->where('beliefId',$value->id) 
     ->where('status','Active')    
     ->get();

     $belief['belief_value'] = $dots_value;

     array_push($beliefs, $belief);
   }

   $dotValues = DB::table('dot_value_list')
   ->select('id','name')
   ->where('status','Active')
   ->orderBy('id','asc')
   ->get();



   return view('admin/dot/detailDot',compact('dots','beliefs','dotValues'));
 }

 public function deleteDot()
 {

  $dotValueId   = Input::get('id');


  $dotValues = DB::table('dots_values')
  ->select('beliefId')
  ->where('id',$dotValueId)
  ->first();

  $dotAllValues = DB::table('dots_values')
  ->where('beliefId',$dotValues->beliefId)
  ->where('status','Active')
  ->get();


  if($dotAllValues->count() > 1)
  {

    $updatetArray = array('status'=>'Inactive');

    DB::table('dots_values')
    ->where('id',$dotValueId)
    ->update($updatetArray);  
  }

}

  /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function update(Request $request, $id)
  {
        //
  }

  /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function destroy($id)
  {
        //
  }

   //add custome values for DOTs
  public function createCustomValues()
  {

    return view('admin/dot/addCustomValues');
  }

//store custome values
  public function storeCustomValues(){

    $rules = array('name'=>'required|max:100');

    $validator =  Validator::make(Input::all(),$rules);

    if($validator->fails())
    {
      return redirect()->back()->withInput()->withErrors($validator->errors());
    }

    $value = DB::table('dot_value_list')
    ->where('name',Input::get('name'))
    ->where('status','Active')
    ->get();


    if (count($value))
    {
     return redirect()->back()->withErrors('Value already exists.');
   }

   $insertArray = array(
    'name'=>Input::get('name'),
    'status'=>'Active',
    'created_at'=>date('Y-m-d H:i:s')
  );

   $status = DB::table('dot_value_list')->insertGetId($insertArray);

   return redirect('admin/dot-custom-values')->with('message','Values Added Successfully');
 }

 /*update DOT detail*/
 public function updateDotDetail(){

  $postData = Input::all();

  $tableData['dot_date'] = date('Y-m-d H:i:s', strtotime($postData['dot_date']));
  $tableData['vision']   = $postData['vision'];
  $tableData['mission']  = $postData['mission'];
  $tableData['focus']    = $postData['focus'];
  $tableData['updated_at']= date('Y-m-d H:i:s');

  $status = DB::table('dots')
  ->where('id',$postData['dotId'])
  ->update($tableData);

  if($status){

    echo"SUCCESS";
  }
}

public function updateDotBeliefDetail(){

  $postData = Input::all();

    // print_r($postData);
    // die(); 
  $counter = Input::get('counter');

  $beliefName   = $postData['belief_name'.$counter];
  $beliefId     = $postData['belief_id'.$counter];
  $valueNameArr = $postData['value_name'.$counter];
  $newValuesArr = Input::get('value_name_new'.$counter);

  $updateBeliefArray = array('name' => $beliefName);

  $beliefStatus = DB::table('dots_beliefs')
  ->where('id',$beliefId)
  ->update($updateBeliefArray);


  $dotsValues = DB::table('dots_values')
  ->where('beliefId',$beliefId)
  ->first();

  /*update values*/
  $valueId =0;
  if(!empty($dotsValues))
  {
    $valueId = $dotsValues->id;
  }

  foreach ($valueNameArr as $value) {
    $var = explode(",",$value);

    if(!empty($var[0]) && !empty($var[1]))
    {

      $updateArray = array('name'=>$var[1]);

      $status = DB::table('dots_values')
      ->where('id',$var[0])
      ->update($updateArray);      
      $valueId++;
    }
    else{


      $insertArray['name']= $value;
      $insertArray['beliefId']= $beliefId;
      $insertArray['status']= 'Active';
      $insertArray['created_at']= date('Y-m-d H:i:s');

      $status = DB::table('dots_values')->insertGetId($insertArray);


    }
  }

  /*add new values*/
  if($newValuesArr){
    foreach ($newValuesArr as $value) {

      $valuesInsertArray = array(
        'name'=> $value,
        'beliefId'=> $beliefId,                
        'created_at'=> date('Y-m-d H:i:s')
      );

      DB::table('dots_values')->insertGetId($valuesInsertArray);
    }
  }
  echo"SUCCESS";

}

public function addBelief()
{

  $postData = Input::all();

    //check if dot not inserted
  $get_dot_id = Input::get('dotId');

  if(!empty($get_dot_id))
  {

      //get counts of the beliefsf
    $belief_count = Input::get('belief_count');

    for($i=1; $i < $belief_count; $i++)
    {

        //get values variable
      $value_var = 'value_name'.$i;

      $belief_var = Input::get('belief_name_new'.$i);

      // if(empty($belief_var))
      // {
      //   return 'Please enter Belief Name.';
      // }else if(empty($postData[$value_var])){

      //   return 'Please select values.';
      // }

      if(!empty($belief_var))
      {
        $beliefInsertArray = array(
          'name'=> $belief_var,
          'dotId'=> $get_dot_id,                
          'created_at'=> date('Y-m-d H:i:s')
        );

        $dot_belief_id = DB::table('dots_beliefs')->insertGetId($beliefInsertArray);
      }
         //check if belief is not inserted 
      if(!empty($dot_belief_id))
      {

        if(!empty($postData[$value_var] ))
        {

          foreach ($postData[$value_var] as $value)
          {

            if(!empty($value))
            {
                //insert data in vaue table
              $valuesInsertArray = array(
                'name'=> $value,
                'beliefId'=> $dot_belief_id,                
                'created_at'=> date('Y-m-d H:i:s')
              );

              $dot_value_id = DB::table('dots_values')->insertGetId($valuesInsertArray);
            }
          } 
        }
        }//check dot belief inserted

      }
    }//check dot not inserted
    if(!empty($dot_belief_id))
    {
      echo "SUCCESS";
    }    
    
  }//main function

  //get custome values list
  public function beliefCustomeValues()
  {

   $dotValuesList = DB::table('dot_value_list')
   ->where('status','Active')
   ->orderBy('name','ASC')
   ->paginate(10);

   return view('admin/dot/customValues',compact('dotValuesList'));
 } 
 /*edit page of custom value*/
 public function editCustomValue($id)
 {

  $dotValueId = base64_decode($id);

  $dotValue = DB::table('dot_value_list')
  ->where('id',$dotValueId)  
  ->first();

  return view('admin/dot/editCustomValues',compact('dotValue'));

}

/*update custom value*/
public function updateCustomValues()
{

  $rules = array('name'=>'required|max:100');

  $validator =  Validator::make(Input::all(),$rules);

  if($validator->fails())
  {

    return redirect()->back()->withInput()->withErrors($validator->errors());
  }

  $value = DB::table('dot_value_list')
  ->where('name',Input::get('name'))
  ->where('status','Active')
  ->first();


  if (count($value))
  {

   $value1 = DB::table('dot_value_list')
   ->where('id',base64_decode(Input::get('dotValueId')))
   ->where('status','Active')
   ->first();



   if($value1->name != Input::get('name'))
   {
    return redirect()->back()->withErrors('Value already exists.');
  }else{
    return redirect()->back();
  }    

}

$updatetArray = array(
  'name'       => Input::get('name'),   
  'updated_at' => date('Y-m-d H:i:s')
);

DB::table('dot_value_list')
->where('id',base64_decode(Input::get('dotValueId'))) 
->update($updatetArray);  

return redirect('admin/dot-custom-values')->with('message','Values Updated Successfully.');
}

/*delete custom value*/
public function deleteCustomValue($id)
{

  $updatetArray = array('status'=>'Inactive');

  DB::table('dot_value_list')
  ->where('id',base64_decode($id)) 
  ->update($updatetArray);  

  return redirect('admin/dot-custom-values')->with('message','Value Deleted Successfully.');

}

public function getEvidenceList($dotId,$type,$id)
{

  $type = base64_decode($type);
  $id   = base64_decode($id);


  if($type=='mission'){

    $wrClouse = array('section'=>'mission','dotId'=>$id);
    $tableName='dots';
    $select ='mission as name';

  }else if($type=='vision'){

    $wrClouse = array('section'=>'vision','dotId'=>$id);
    $tableName='dots';
    $select ='vision as name';

  }else if($type=='focus'){

    $wrClouse = array('section'=>'focus','dotId'=>$id);
    $tableName='dots';
    $select ='focus as name';

  }else if($type=='belief'){

    $wrClouse = array('section'=>'belief','sectionId'=>$id);
    $tableName='dots_beliefs';
    $select ='name';


  }else if($type=='value'){

    $wrClouse = array('section'=>'value','sectionId'=>$id);
    $tableName='dots_values';
    $select ='name';
    $sectionId = DB::table($tableName)
    ->select($select)
    ->where('id',$id)
    ->first();

    $sectionName = DB::table('dot_value_list')
    ->select('name')
    ->where('id',$sectionId->name)
    ->first();




  }else{
    $wrClouse = array('dotId'=>$id);
  }

  $evidences = DB::table('dot_evidence')
  ->where('status','Active')
  ->where($wrClouse)
  ->paginate(4);
  
  if($type!='value')
  {
    $sectionName = DB::table($tableName)
    ->select($select)
    ->where('id',$id)
    ->first();
  }


  return view('admin/dot/listEvidence',compact('evidences','type','dotId','sectionName'));
}

/*get DOT custom value list for  */
public function getDotCustomValueList(){

  $beliefId = Input::get('beliefId');
  $dotId = Input::get('dotId');

  $dotValuesList = DB::table('dots_values')
  ->select('dot_value_list.name','dots_values.id')
  ->leftJoin('dot_value_list', 'dots_values.name', '=', 'dot_value_list.id')
  ->where('dots_values.status','Active')
  ->where('dots_values.beliefId',$beliefId)
  ->get();

  $belief = DB::table('dots_beliefs')
  ->where('id',$beliefId)
  ->first();

  return view('admin/dot/dotValueEvidenceSelectModal',compact('dotValuesList','belief','dotId'));

}

public function deleteBelief()
{
  $beliefId = Input::get('id');
  $updatetArray = array('status'=>'Inactive');

  DB::table('dots_beliefs')
  ->where('id',$beliefId) 
  ->delete();  

  DB::table('dots_values')
  ->where('beliefId',$beliefId) 
  ->delete();  

  echo "SUCCESS";
 // return redirect('admin/dot-custom-values')->with('message','Belief Deleted Successfully.');


}


}
